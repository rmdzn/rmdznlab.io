# Hai, Rmdzn di sini!

Dunia terlalu luas. Terlalu melelahkan untuk dilalui.

Sebuah bidang yang membentang dari rumah hingga ruang-ruang di sekitarnya adalah satu-satunya tempat berharga. Yang membuat lelah tak terasa. Amarah tak merusak. Sedih tak menggema. Bahagia bersahaja.

Saya menyebut bidang itu jembatan khayali. Satu dari sebagian bidang jembatan yang lebih luas yang tak mesti sepi. Penuh orang berisik. Saling bersahutan. Saling merisak. Bagian jembatan yang saya sebut pertama berwarna sedikit lebih terang, melenggang.

Itulah blog ini.

Alih-alih bercerita langsung kepada orang lain, saya lebih suka menuliskannya dalam catatan-catatan kecil. Blog ini adalah salah satu medianya. Menjadikannya sebagai ruang curahan pikiran dan curahan hati yang kerap berseliweran dari pojokan hingga riuh rendah jalanan.

Seperti manusia pada umumnya, curahan pikiran tidaklah aktual. Gaya berpikir saya dahulu tidak selalu sama seperti sekarang. Begitulah esai-esai di tempat ini. Yang tak lekang oleh zaman justru dapat ditemukan pada ulasan buku dan cerita-cerita pendeknya.

Jadi ... selamat datang!

---

✉️ ridanmaza.1gycn@slmail.me
