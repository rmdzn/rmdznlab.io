---
title: "Isi Survei DuckDuckGo Demi Konten Tentang Privasi"
date: "2016-09-24"
categories: 
  - "privasi"
tags: 
  - "privasi"
coverImage: "2016-09-25-003314_maim.png"
aliases:
  - /post/2016-09-24-isi-survei-duckduckgo-demi-konten-tentang-privasi/
---

![ddg](/posts/2016/09/images/2016-09-25-003314_maim.png)

Saya termasuk [pengguna efektif](http://rafeyu.github.io/kenapa-pakai-duckduckgo/) mesin pencari DuckDuckGo (DDG) sejak tahun 2014 lalu. Jadi saya pasti tertarik dengan perkembangan mesin pencari penjaga privasi ini.

Hari ini, melalui [Reddit.com](https://www.reddit.com/r/linux/comments/548ute/duckduckgo_is_doing_a_great_anonymous_online/), diberitahukan bahwa DDG sedang sibuk mengumpulkan survei yang berhubungan dengan privasi. Kalau saya perhatikan, DDG memang sedang mengumpulkan "dukungan" untuk menyediakan konten yang fokus pada privasi; dari teori hingga praktek. Misalnya, penjelasan mengenai TOR, VPN, bagaimana cara korporasi memanfaatkan "kepribadian" penggunanya, mengapa iklan selalu tahu apa yang dimau pengguna, dan sebagainya.

Bahasan privasi itu sangat luas! Oleh karena itu, saya hormati keputusan tim DDG untuk menyediakan konten yang berorientasi pada privasi.

Sedikit mengingat tulisan saya di [Kompasiana](http://www.kompasiana.com/rafeyu), penyelewengan privasi yang bisa kita temukan dengan mudah adalah iklan. Di Amerika Serikat, masalah ini menimbulkan kemunculan media sosial baru seperti Ello atau TPO (The People Operator).

Namun, di Indonesia, apakah privasi itu penting? Antara iya dan tidak. Idealnya, privasi itu penting. Tapi itu kembali lagi ke orang-orang untuk menjawab pertanyaan tersebut. _Ngomong-ngomong_ saya termasuk pengguna ponsel pintar, yang notabene adalah gawai pembuka tabir privasi paling populer.

Kembali ke DDG, bagi yang ingin ikut mengisi surveinya bisa menuju ke tautan: [https://www.surveymonkey.com/r/T7J2H2R](https://www.surveymonkey.com/r/T7J2H2R).
