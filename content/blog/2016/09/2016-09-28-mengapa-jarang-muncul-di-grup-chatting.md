---
title: "Mengapa Jarang Muncul di Grup Chatting?"
date: "2016-09-28"
categories: 
  - "curhat"
tags: 
  - "cuap"
coverImage: "grup-telegram-rmdz.jpg"
aliases:
  - /post/2016-09-28-mengapa-jarang-muncul-di-grup-chatting/
---

Kalau dibilang, saya termasuk orang yang jarang bicara di grup _chat_, bahkan di grup bertemakan hal yang super saya sukai.

Misalnya di grup Telegram [Belajar GNU/Linux Indonesia (BGLI)](http://s.id/BGLI), setiap [statistik yang di _generate_](http://s.id/BGLIStat) oleh combot, saya jarang sekali masuk 30 besar orang yang berbincang di grup tersebut.

Berikut tangkapan layar statistik grup Telegram BGLI tanggal 28 September 2016 jam 18:05 WIB.

[![Statistik grup BGLI](/blog/2016/09/images/statistik-grup-bgli-rmdz.jpg)](/posts/2016/09/2016-09-28-mengapa-jarang-muncul-di-grup-chatting/) Statistik grup BGLI via Combot

Saya jarang masuk daftar atas. Mengapa begitu?

## 1\. Tidak punya waktu

Bukan sombong. Sekitar 16 jam setiap hari, saya "berprofesi" sebagai penjual di sebuah warung kelontong atau kios yang ada di rumah saya sendiri. Nah, "ruang kerja" saya tepat di samping etalase kios.

Untuk menulis satu kabar di situs [Kabar Linux (situs mati)](https://kabarlinux.web.id) saja butuh perjuangan yang sangat keras karena trafik di kios orang tua saya memang cukup ramai. Tetangga, penghuni kos, orang-orang lewat, pun mampir sekadarnya untuk membeli permen hingga bahan-bahan sembako. Sehingga ide penulisan harus saya kunci rapat, karena setiap kata/kalimat yang saya tulis, selalu saja "diganggu" oleh para pembeli.

Apalagi kalau tema tulisan yang hendak saya terbitkan belum saya kuasai sepenuhnya. Saya harus belajar, membaca dan membaca, mencari referensi, hingga saya paham dengan apa yang saya sedang/akan tulis.

Ikut berbincang di grup _chat_? Sudah tidak ada waktu lagi.

## 2\. Cenderung menyendiri

Saya adalah orang [ambivert](https://en.wikipedia.org/wiki/Ambivert), tapi lebih condong ke introvert. Berbincang terlalu lama dalam sebuah forum adalah hal yang melelahkan. Saya lebih suka mengulik sendiri dengan hal-hal yang saya sukai.

Walaupun dunia daring, bagi saya, itu sudah sangat ramai. Orang-orang mengetik (_typing..._) setiap saat, orang-orang menulis sesuatu di grup setiap detik, yang lain menanggapi, dan terus berulang. Begitu ribut bukan? (Saya paham tentang notifikasi, tapi bukan itu yang saya maksud).

Saya tidak menghindari "keributan" ini, kalau ada bahasan yang menurut saya menarik, saya pasti ikut berdiskusi. Tetapi bila diskusi sudah ditutup dan ada diskusi lain yang masuk, saya undur diri.

Ini tidak hanya terjadi di grup Telegram bertema Linux, grup WhatsApp alumni sekolah saja, saya jarang muncul.

## 3\. Tidak suka aplikasi _chatting_

Kalau saya diberi pilihan antara aplikasi _chatting_ atau forum, saya akan lebih memilih forum.

Aplikasi _chatting_ yang mengusung sistem _realtime_ terasa meributkan (silakan baca poin 2 di atas). Saya lebih sering mengunjungi diskusi di Reddit, grup Facebook, [forum VoidLinux](https://forum.voidlinux.eu/), [forum Gentoo](https://forums.gentoo.org/), laman _pull request_/_issue_ dalam sebuah proyek GitHub, dibandingkan berbincang via kanal IRC #VoidLinux, #Gentoo, dan #silakan\_masukkan\_kanal\_favorit\_anda.

Saya pun lebih sering jadi Pembaca Senyap (_Silent Reader_) di grup-grup _chatting_ yang saya ikuti.

## Penutup

Sedikit _cuap_\-_cuap_ hari ini hanya untuk mengeluarkan beban pikiran saya. Bisa saja bermanfaat -- bagi yang _ngepoin_ saya dan yang ingin bacaan ringan -- bisa saja tidak bermanfaat bagi yang memang beranggapan bahasan ini tidak menarik. 😇 😇
