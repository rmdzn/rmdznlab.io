---
title: "Macam-Macam Cacat Logika (Logical Fallacy)"
date: "2016-10-11"
categories: 
  - "ngelmu"
tags: 
  - "cuap"
coverImage: "kesalahan-logis-rmdzn.png"
aliases:
  - /2016/10/macam-macam-cacat-logika-logical-fallacy/
  - /post/2016-10-11-macam-macam-kesalahan-logis-logical-fallacy/
---

![cacat logika](/blog/2016/10/images/kesalahan-logis-rmdzn.png)

**Edit: sebelumnya judul artikel ini _kesalahan logis_ kemudian saya ganti menjadi _cacat logika (_sering juga disebut sebagai _sesat pikir)_ ;)**

Anda pernah mengikuti sebuah debat profesional maupun debat kecil-kecilan di dunia maya? Pasti pernah. Mungkin saja saat Anda ikut merumuskan pendapat, Anda disangkal dengan bahasa-bahasa latin nan keren semacam _argumentum ad hominem_ oleh lawan debat? Jika belum, berarti debat Anda kurang jauh. ;p

_Argumentum ad hominem_ adalah salah satu cacat logika (_logical fallacy_) paling populer, bahkan yang sering kita lakukan saat mengikuti debat mini di sebuah forum daring.

Nah .. hari ini sepertinya asik kalau saya sedikit mengulas macam-macam cacat logika dengan bahasa latinnya. Sebelumnya, sebagai sangkalan, info seputar cacat logika ini hanya untuk debat yang benar-benar logis, yang bisa dinalar dan dibuktikan dengan otak--rasional.

## 1\. _Argumentum ad antiquitatem_

Hal yang sering dilakukan sebelumnya dianggap sebagai hal yang benar. Ini termasuk cacat logika yang populer.

Misalnya, untuk menginstal Windows, kita bisa meminjam CD instalasi Windows dari rental CD dan menginstalnya dengan bantuan _crack_ atau serial number yang sudah disediakan.

## 2\. _Argumentum ad hominem_

Saya jamin, cacat logika ini sering kita dengar dan mungkin kita lakukan. _Argumentum ad hominem_ merupakan pendapat yang menyerang kepribadian orang atau motif orang alih-alih menyerang ide atau gagasannya.

Misalnya, ide bu menteri untuk meledakkan kapal-kapal pencuri ikan adalah hal yang konyol dan bodoh karena dia hanya lulusan SMP.

## 3\. _Argumentum ad ignoratium_

Ketika ada orang yang membenarkan sesuatu karena sesuatu itu belum terbukti keberadaannya, maka ia telah melakukan cacat logika ini.

Misalnya, ketika guru mengatakan di depan kelas, "karena tidak ada yang bertanya mengenai materi tadi, besok kita ujian."

## 4\. _Argumentum ad logicam_

Sedikit berhubungan dengan cacat logika sebelumnya, _argumentum ad logicam_ adalah anggapan bahwa sesuatu itu salah karena hal yang menyebabkan sesuatu itu salah.

Misalnya, presiden Sukarno hebat karena telah mengurangi depresi masyarakat Indonesia.

Presiden mengurangi depresi masyarakat Indonesia? Belum tentu. Namun tidak berarti pernyataan "presiden Sukarno hebat" salah.

## 5\. _Argumentum ad misericordiam_

Ketika ada orang yang memaksakan kebenarannya dengan rasa kasihan, maka dia telah melakukan cacat logika ini.

Misalnya, ketika ibu Miriam menyalahkan tindakan gegabah para Avengers atas meninggalnya sang anak akibat pertempuran Avengers dengan kelompok Crossbones.

## 6\. _Argumentum ad nauseam_

Cacat logika ini terjadi saat sebuah argumen yang belum tentu benar atau bahkan salah, yang selalu diulang-ulang, sehingga akan dianggap benar secara otomatis. Atau argumen yang sudah benar, tapi terus diulang-ulang sehingga menutupi objek debat sebenarnya.

Misalnya, anggapan bahwa _global warming_ itu tidak ada dan hanya akal-akalan.

## 7\. _Argumentum ad numerum_

Cacat logika bisa timbul saat seseorang berusaha membuktikan kebenaran argumennya dengan cara menunjukkan jumlah orang yang percaya dengan argumennya.

Misalnya, pernyataan "paling tidak ada 70 persen masyarakat yang muak dengan presidennya."

## 8\. _Argumentum ad populum_

Cacat logika ini identik dengan _argumentum ad numerum_, yang berusaha membuktikan bahwa masyarakat umum setuju dengan kita.

## 9\. _Argumentum ad verecundiam_

Ada dua kasus untuk menggambarkan cacat logika ini. _Argumentum ad verecundiam_ (atau bisa disebut sebagai _appeal of authority_) akan aktif ketika:

1. kita ingin membuktikan sesuatu dengan cara mengutip pernyataan seseorang yang tidak ahli di bidangnya.
2. kita selalu membenarkan otoritas (orang yang dianggap tahu) tanpa mencari tahu apakah argumen yang disampaikan memang benar.

Contoh:

- "Pemanasan global tidak benar, menurut Kak Seto, pencairan di beberapa titik di benua Antartika adalah hal normal, yang akan pulih sekitar delapan bulan sekali."
- "Pemerintah pasti memiliki alasan mengapa pada titik tertentu jalanan selalu macet. Untuk mencegah itu, kita harus segera membangun jalan tol sepanjang 100Km lagi di wilayah tersebut."

## 10\. _Strawman fallacy_

Cacat logika yang terjadi ketika seseorang melakukan kesalahan interpretasi (mendistorsi pesan) terhadap lawan bicaranya hanya karena satu dua argumen atau ciri khas tertentu yang disampaikan sang lawan.

Contohnya,

**A:** Menurut antum bentuk pemerintahan apa yang memang patut diterapkan di Indonesia?

**B:** Jelas, demokrasi Pancasila! Demokrasi Pancasila itu sudah harga mati, tak bisa ditawar lagi. Lalu, menurutmu sistem pemerintahan Islam atau khilafah menjamin keadilan maupun kemakmuran negara?

**A:** Anda menuduh saya sebagai penganut sistem khilafah karena panggilan "antum"?

B telah melakukan _strawman fallacy_.

## 11\. _False Equivalence_

Sebuah cacat logika yang bermaksud menyamakan dua hal, padahal faktanya keduanya berbeda.

Contoh, "Pistol dan nuklir adalah senjata. Selama ini warga negara Amerika Serikat diizinkan memiliki pistol sehingga seharusnya mereka juga diizinkan memiliki nuklir".

## 12\. _Red Herring Fallacy_

Saat seseorang mengeluarkan argumen untuk mengalihkan topik utama yang sedang diperbincangkan, dia telah berhasil melakukan cacat logika ini.

Contoh, seorang murid tertangkap tangan telah mencontek saat ujian. Murid tersebut berkata, "saya tahu saya salah, Bu. Tapi, Bu, tolong bayangkan bagaimana perasaan orang tua ketika mereka melihat saya duduk di sini. Mereka akan patah hati, Bu. Sumpah."

Ibu guru: Yhaa~

Murid tersebut melakukan _Red herring fallacy_.

## _13\. Argumentum ad baculum_

Cacat logika ini terjadi ketika seseorang memaksa orang lain agar menyetujui pendapatnya dengan cara mengancam, mengintimidasi, dan cara buruk lainnya.

Contoh kasusnya,

Pendukung Prabowo: _coblos pak Prabowo! Kalau tidak mau, masjid tidak akan ada azan lagi_.

Pendukung Jokowi: _coblos pak Jokowi! Kalau tidak mau, Indonesia akan dikuasai HTI_.

Kedua pendukung calon presiden sama-sama melakukan cacat logika ini.

## _14\. Argumentum ad temperantiam_

Jika seseorang menyebutkan argumen yang bermaksud menengahi dua argumen ekstrem, orang tersebut telah melakukan cacat logika ini. Pernah dengar pernyataan "kebenaran berada di tengah-tengah" atau semacam itu? Naah ... _argumentum ad temperantiam_ bermain di pernyataan tersebut.

Contohnya dapat dilihat saat masa krisis ini, ketika kurva wabah masih terus naik. Belum turun-turun.

A bilang, "Indonesia harus _lockdown"._

B bilang, "Indonesia harus _herd immunity"_.

C bilang, "Indonesia harus mulai hidup berdampingan dengan korona."

(Catatan: ini hanyalah ilustrasi, tidak bermaksud menyinggung siapa pun. Karena, siapa pula yang benar-benar berkata seperti yang B katakan? Tidak ada, kan? Kan kan kan?)

## 15\. _Slippery Slope Fallacy_

Apabila seseorang mencontohkan kejadian A akan memicu kejadian B, C, D, E, sampai Z dan Z dianggap tidak akan terjadi jika kejadian A juga tidak terjadi. Orang tersebut telah menyampaikan cacat logika ini.

Contohnya,

Jangan sekali-kali mengirim pesan WhatsApp dengan 'P' saja. Itu akan menjadi kebiasaan. Suatu waktu kalian mengirim pesan 'P' saat melamar kerja. Tempat kerja kalian menolak. Kemudian kalian meminta bantuan teman, ternyata mereka juga menolak. Kalian bingung mengapa tempat kerja dan teman-teman tidak menerima kalian. Kalian bengong di jalan. Tertabrak mobil pikap lalu mati.

Mengirim pesan 'P' = mati muda.

## 16\. _Tu quoque_

Cacat logika ini mirip seperti _Red Herring_. Ketika seseorang dikritik tapi kemudian malah menjawabnya dengan kritikan lain ke lawan bicaranya sehingga bahasan utama bergeser.

Contoh, saat Anji dikritik atas konten videonya yang penuh disinformasi oleh warganet, tetapi Anji balik mengkritik warganet karena menganggap merekalah yang justru mempopulerkan video miliknya. Video Anji lain yang memiliki konten bermutu malah sedikit sekali yang menonton.

## 17\. _Petitio Principii_

Petitio Principii atau kalau dalam bahasa Inggris disebut sebagai c_ircular argument_ merupakan cacat logika saat seseorang menyampaikan argumennya secara berulang-ulang tanpa sampai ke kesimpulan.

Kesimpulannya muncul di premis. Begitu. Berulang-ulang.

Contoh argumen, kitab X benar karena kitab X menyatakan hal tersebut. Pemerintah pasti benar karena mereka bilang seperti itu.

## 18\. _Genetic Fallacy_

_Genetic fallacy_ terjadi saat seseorang memaparkan argumen yang berhubungan dengan genetik, orang, ras, bangsa, karakter, institusi, dsb. Yang jika argumennya digabung tidak nyambung.

Contoh, ayo kita razia buku apapun yang di judulnya terdapat kata "komunis"! Buku yang seperti itu pasti mendukung PKI!

## 19\. _Hasty Generalization_

Cacat logika ini terjadi ketika seseorang menyimpulkan sesuatu berdasarkan bukti yang sangat kurang maupun bukti yang bias.

Contoh, seseorang berkata pelatihan sepanjang pekan nanti pasti membosankan (padahal, dia baru mengikuti satu hari pertama pelatihan. Pelatihan berikutnya pasti tergantung guru dan materi lain, kan?).

## 20. _False Dilemma_

Cacat logika ini terjadi saat menyederhanakan masalah kompleks hanya dengan dua pilihan yang bertentangan. Padahal masalah tersebut memiliki solusi yang spektrum, bisa ke kanan, ke kiri, ke atas, ke bawah, bahkan juga bisa serong kanan-kiri, dsb.

Contoh:

- Kita ikuti rencana Pak Samsuri atau membiarkan proyek ini batal. Tak ada opsi lain.
- Kamu pilih main PUBG atau kita putus? (padahal masih ada jalan untuk berbincang lebih dalam, komitmen ulang, tanting, dll)

## 21\. Dll

Dan masih banyak lagi, yang akan saya teruskan di sela waktu-waktu tertentu.

Referensi:

- http://www.csun.edu/%7Edgw61315/fallacies.html
- https://everbosity.wordpress.com/2009/06/26/argumentum-ad-logicam/
- http://rationalwiki.org/wiki/Argumentum\_ad\_nauseam
- https://www.logicallyfallacious.com/tools/lp/Bo/LogicalFallacies/169/Strawman-Fallacy
- https://www.logicallyfallacious.com/tools/lp/Bo/LogicalFallacies/245/False-Equivalence
- https://literarydevices.net/red-herring/
- https://rationalwiki.org/wiki/Argumentum\_ad\_baculum
- https://religions.wiki/index.php/Argumentum\_ad\_temperantiam
- https://rationalwiki.org/wiki/Argument\_from\_authority
