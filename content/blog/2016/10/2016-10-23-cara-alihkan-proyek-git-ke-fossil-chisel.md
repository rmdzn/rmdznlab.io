---
title: "Cara Alihkan Proyek Git ke Fossil (Chisel)"
date: "2016-10-23"
categories: 
  - "tutorial"
tags: 
  - "tutorial"
coverImage: "2016-10-24-000721_maim.png"
aliases:
  - /post/2016-10-23-cara-alihkan-proyek-git-ke-fossil-chisel/
---

Iseng-iseng, saya mencoba untuk mengalihkan repositori _dotfiles_ git saya ke Fossil di Chisel.

### 1\. Daftar akun di chiselapp.com, lalu buat repositori baru

### 2\. Ekspor dulu proyek git ke fossil

```
$ cd dotfiles
$ git fast-export --all | fossil import --git dotfiles.fossil
```

### 3\. Buka dotfiles.fossil lalu masukkan remote-url repositori chiselapp.com

```
$ fossil open dotfiles.fossil
$ fossil remote-url https://ramdziana@chiselapp.com/user/ramdziana/repository/dotfiles
```

### 4\. Sesuaikan project-code repositori lokal dengan repositori Chisel

Lihat _project-code_ di Chisel, dengan cara masuk ke repositori, lalu menu Admin, lalu ke SQL (http://chiselapp.com/user/ramdziana/repository/dotfiles/admin_sql\).

Jalankan,

```
SELECT * FROM config WHERE name = 'project-code';
```

Setelah menyalin _project-code_, masuk ke repositori lokal,

```
$ cd dotfiles
$ fossil sql -R dotfiles.fossil
```

Ganti _project-code_ lokal dengan _project-code_ yang sudah disalin tadi,

```
UPDATE config SET value = 'project-code-yang-disalin' WHERE name = 'project-code';
```

### 5\. Sinkronkan repositori di Chisel dengan repositori lokal

```
$ fossil sync
```
