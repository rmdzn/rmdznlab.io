---
title: "Vim Stuck di Slackware"
date: "2016-11-09"
categories: 
  - "linux"
tags: 
  - "linux"
aliases:
  - /post/2016-11-09-vim-stuck-di-slackware/
---

Di Slackware 14.2, saya sempat merasakan _stuck_ pada Vim. Masalah ini terlihat ketika saya membuka `vim` via terminal, editor teks ini sama sekali tidak terbuka. _Stuck_, _freeze_, atau beku.

Untuk menutup vim-yang-stuck ini tidak cukup mengandalkan `Ctrl+C`, saya harus menutup terminal atau membunuhnya dengan `kill -9 $(pidof vim)`.

Ternyata ini memang salah satu _bug_ pada gpm. Baca [bugzilla RedHat ini](https://bugzilla.redhat.com/show_bug.cgi?id=168076).

Jadi, untuk menyelesaikan masalah Vim yang _stuck_, saya harus membunuh gpm terlebih dulu.

```
$ sudo kill $(pidof gpm)
```