---
title: "Setop Kagumi Orang Lain, Fokuslah pada Kemampuan dan Kekerenan Diri"
date: "2016-12-31"
categories: 
  - "cuap"
tags: 
  - "cuap"
aliases:
  - /post/2016-12-31-setop-kagumi-orang-lain-fokuslah-pada-kemampuan-dan-kekerenan-diri/
---

Saya punya teman yang mempunyai sifat negatif. Misalnya, lihat video orang bermain alat musik, jago, dia bilang, "keren, coba kalau bisa gitu, blabla…​." Lihat video olahraga ekstrem, dia juga bilang, "keren _coy_ kalau bisa gitu." Lihat video keren lain, dia akan terus mengatakan hal serupa.

Oke, mungkin contoh yang saya sampaikan masih terasa wajar dan belum menunjukkan sisi negatif. Namun …​

Bagi saya sebagai pendengar, rasa sebal selalu muncul tiba-tiba layaknya saat teman saya tiba-tiba mengagumi orang-orang yang ia lihat lewat video dan ingin melakukan hal sama. Mengapa dia tidak fokus saja dengan apa yang dia lakukan? Mengapa ia begitu peduli, ingin apapun yang ia lihat melalui mata kepala sendiri?

Saya tidak menyampaikan bahwa kagum dengan seseorang itu tidak boleh. Jelas boleh! Siapa yang ingin melawan kemanusiawian manusia? Namun, saya ingin melawan rasa kagum dengan apapun di mana si pengagum belum tentu (atau yakin) bisa melakukan hal serupa dan sebenarnya ia sudah melakukan hal hebat miliknya sendiri.

Pertama kali, ego saya merasa bahwa saya merupakan satu-satunya orang yang memiliki rasa sebal seperti itu. Ternyata tidak, beberapa hari yang lalu, salah satu tulisan Boris Veldhuijen di TheNextWeb secara tidak langsung mengiyakan tulisan saya hari ini.

Di tulisan berjudul [_Stop Coveting The Skilss You Don’t Have_](http://thenextweb.com/insider/2016/12/26/stop-coveting-skills-dont/), Boris ditegur oleh manajernya gara-gara ia kagum/iri dengan rekannya yang mempresentasikan dokumen Microsoft Word dengan sangat rapi dan apik, yang sebetulnya dia tidak bisa melakukannya.

Jadi, seperti yang saya bilang tadi. Saya tidak melawan sisi kemanusiawian, tapi ada hal urgen yang harus kita prioritaskan: fokus pada kemampuan dan kekerenan diri sendiri. Jangan biarkan kekagumanmu akan sesuatu justru menghapus kemampuan dan kekerenanmu.
