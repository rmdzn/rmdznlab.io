---
title: "Mengaktifkan USB Card Reader Realtek di Kernel Linux"
date: "2017-02-05"
categories: 
  - "gentoo"
  - "linux"
tags: 
  - "linux"
coverImage: "2017-02-05-221858_maim.png"
aliases:
  - /post/2017-02-05-mengaktifkan-usb-card-reader-realtek-di-kernel-linux/
---

![](/blog/2017/02/images/2017-02-05-221858_maim.png)

> Sudah "tak zaman" mengatur konfigurasi kernel secara manual, tapi siapa peduli?

Saya menggunakan laptop MSI CR41 dengan USB Card Reader Realtek, bagaimana cara saya mengaktifkan peranti untuk membaca _card reader_ ini?

Berikut bukti saya memiliki USB Card Reader Realtek:

$ lsusb | grep -i "card reader"
Bus 001 Device 003: ID 0bda:0139 Realtek Semiconductor Corp. RTS5139 Card Reader Controller

Untuk mengaktifkannya, kita perlu mengaktifkan konfigurasi MFD\_RTSX\_USB dan MMC\_REALTEK\_USB. Silakan kunjungi direktori `/usr/src/linux` lalu jalankan `make menuconfig`.

Aktifkan MFD\_RTSX\_USB di:

```
Device Drivers --->
       Multifunction Device Drivers --->
                  <M> Realtek USB Card Reader
```

Kemudian aktifkan MMC\_REALTEK\_USB:

```
Device Drivers --->
       MMC/SD/SDIO Card Support --->
                <M> Realtek USB SD/MMC Card Interface Reader
```

Silakan _compile_ ulang kernel Anda. Jika kompilasi berhasil, Anda akan mempunyai modul kernel baru; `mfd-core`, `rtsx_usb`, dan `rtsx_usb_sdmmc.`.

```
CC      drivers/mfd/mfd-core.mod.o
LD [M]  drivers/mfd/mfd-core.ko
CC      drivers/mfd/rtsx\_usb.mod.o
LD [M]  drivers/mfd/rtsx\_usb.ko
CC      drivers/mmc/host/rtsx\_usb\_sdmmc.mod.o
LD [M]  drivers/mmc/host/rtsx\_usb\_sdmmc.ko
```

_Walaa_ Anda telah berhasil mengaktifkan peranti tersebut! ;)
