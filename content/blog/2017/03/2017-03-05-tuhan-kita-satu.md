---
title: "Tuhan Kita Sama"
date: "2017-03-05"
categories: 
  - "cuap"
tags: 
  - "cuap"
aliases:
  - /post/2017-03-05-tuhan-kita-satu/
---

Setiap agama memiliki Tuhan atau sesembahannya sendiri. Misalnya, orang Islam meyakini bahwa Allah (Alloh, kalau mengikuti tulisan Aa Gym) adalah satu-satunya Tuhan, sesembahan, dan satu-satunya zat yang patut dipuja dengan segala kekuasaannya.

Dalam Kamus Bahasa Besar Bahasa Indonesia, [Tuhan](https://kbbi.kemdikbud.go.id/entri/tuhan) berarti:

1. sesuatu yang diyakini, dipuja, dan disembah oleh manusia sebagai yang Mahakuasa, Mahaperkasa, dan sebagainya.
2. sesuatu yang dianggap sebagai Tuhan.

Tak terbantahkan, arti dari kata Tuhan di atas tentu cocok dengan keyakinan umat Islam dan umat beragama lain atas sesembahannya. Lalu berapa "banyak" Tuhan di dunia ini? Sangat banyak! Jika kita mengikuti data banyaknya agama yang dianut oleh milyaran penduduk dunia.

Coba kita lihat persentase banyaknya penganut agama di dunia tahun 2010, di bawah ini:

![Tabel persentase penganut agama tahun 2010](/blog/2017/03/images/tabel-penganut-agama-2010-rmdz.jpg)

Menurut data tersebut, minimal, sudah ada 5 Tuhan yang berbeda di dunia ini; Tuhan orang Kristen, Islam, Hindu, Budha, dan Yahudi. Tuhan agama lain ada di bagian _unaffilliated_, _Folk Religions_, dan _Other Religions_.

Namun, sepertinya keadaan ini mulai bergeser.

Di media sosial bahkan di dunia nyata, kita dengan mudah melihat orang menghujat, membenci, mencaci, memfitnah, membunuh, menghantam, menyakiti, menuduh, menjegal, dan (masukkan kata buruk di sini) yang hanya mengikuti keinginan sendiri.

Saya takut menyatakan hal ini, ternyata kita telah bersatu, berpadu, kompak, dan seia sekata secara sadar atau tidak bahwa kita sudah memiliki satu Tuhan: Hawa Nafsu.

Referensi tabel agama di dunia: [Pew Research Center](https://www.weforum.org/agenda/2016/05/fastest-growing-major-religion/)
