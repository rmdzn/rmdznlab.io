---
title: "Percayalah! Komunikasi Itu Sangat Penting"
date: "2017-03-11"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "keluarga-rmdz.jpg"
aliases:
  - /post/2017-03-11-percayalah-komunikasi-itu-sangat-penting/
---

![keluarga](/blog/2017/03/images/keluarga-rmdz.jpg)

Komunikasi itu sangat penting. Lihat saja, masalah apapun sebenarnya tidak perlu timbul jika ada komunikasi, komunikasi yang benar-benar terbuka dan jujur.

Saya punya seorang sahabat karib sejak SD (lebih tepatnya sejak TPA di usia SD), panggil saja Uham. Laki-laki [introvert](https://en.wikipedia.org/wiki/Extraversion_and_introversion) super parah. Mirip _sih_ kayak saya, bedanya saya adalah seorang [ambivert](https://en.wikipedia.org/wiki/Extraversion_and_introversion#Ambiversion) (tengah-tengah antara ekstrovert dan introvert). Jadi, saya sebenarnya bangga punya sahabat introvert, mengingat orang yang benar-benar introvert dan ekstrovert sangat sedikit jika dibandingan dengan milyaran penduduk dunia yang sebagian besar adalah ambivert.

Seorang introvert hanya punya sedikit teman, tetapi dia pasti punya kedekatan emosional yang sangat tinggi dengan temannya dan mampu mengonversi mereka sebagai sahabat. Berbanding terbalik dengan seorang ekstrovert yang populer dan punya banyak teman di mana teman-teman tersebut belum tentu menjadi sahabat.

Saya dan Uham percaya komunikasi merupakan hal yang sangat penting, meski Uham sendiri merupakan seorang yang "sulit berkomunikasi" dengan orang lain, termasuk **keluarganya**, tetapi tidak dengan sahabat karibnya. Anda lihat _font_ tebal tersebut? _Yak_, Anda tidak salah.

Dalam satu waktu, Uham pernah _curcol_ (curhat colongan) ke saya tentang hal tersebut. Saya pun menyaut (dalam bahasa Jawa, tapi saya terjemahkan ke bahasa Indonesia), "Ham, _kok_ bisa? Kamu enteng bisa _ngobrol_ _ngalor-ngidul_ sama _temen_ sendiri, _kok_ malah ribet _ngomong_ sama keluarga?"

"Gini zi .."

## Uham dan keluarga

Uham adalah anak terakhir dari tiga bersaudara. Dua kakaknya laki-laki semua. Kakak pertamanya tinggal dan kerja di Ngawi sedangkan kakak kedua masih tinggal seatap dengan Uham dan bapak ibunya.Kata Uham, kesulitan komunikasi dengan keluarga baru ia rasakan sejak kuliah.

"Zi, dulu waktu sekolah kayaknya gampang _banget_ _ngobrol_ sama bapak ibu. Sampai-sampai aku heran sama kak Deri yang sering uring-uringan usai _ngomongin_ sesuatu sama bapak ibu. Akhirnya, aku sadar apa yang bikin kakakku panas kalau habis _ngomong_ sama bapak ibu."

Uham termasuk orang yang peduli, berbeda dari kakak-kakaknya yang relatif cuek bebek dengan keadaan sekitar.

"Ternyata sulit _banget_ _ngobrol_ sama bapak ibu. Bukan 'ngomongnya' _sih_ yang sulit, tapi tentang menyampaikan dan mengusulkan sesuatu."

Menurut Uham, bapak ibunya cenderung membebaskan anaknya untuk melakukan apa saja, asal positif, termasuk saat dia bergabung dengan organisasi Rohis di SMA-nya, tapi tidak dengan "bebas berpendapat".

Seperti yang saya sampaikan tadi, Uham termasuk orang yang peduli. Dia benar-benar peduli dengan kebaikan keluarganya. Tapi, sepertinya ini sulit diterima oleh orang tuanya yang sulit terbuka.

"Sewaktu saya usul ke bapak, 'pak, mungkin ini bisa diginiin nanti biar bisa gini', mentah! Respon maksimal yang aku _dapetin_ cuma 'gak perlu Ham, nanti malah gini dan gitu'."

"Aku sebenernya gak masalah kalau usulan ditolak, tapi yang aku sebelin adalah gak ada ruang diskusi di situ. Kalau 'tidak' ya 'tidak'. Jangan usul lagi."

Uham sering curhat ke kakaknya, Deri, tentang bagaimana sikap bapaknya sewaktu mendapatkan usulan. Kak Deri mengiyakan, karena memang beliau sudah sering mengalaminya. Kata Uham, mungkin gara-gara ini kak Deri jadi lebih cuek. "Terserah bapak ibu mau _ngapain_, toh kita gak punya suara di sini," ujar Uham menirukan kakaknya.

Berbanding lurus dengan ibu Uham. Kata Uham, ibunya sebenarnya lebih terbuka, bisa menerima usulan lebih mudah. Tapi, usulan ini sering mentah, tidak ada tindak lanjutnya.

"Kalau ibu seringnya ngambek kalau diberi saran sama anak-anaknya, apalagi ngambeknya sambil pakai intonasi tinggi. Sekali nerima pendapat, tidak ada tindak lanjutnya. Aku lama-lama bosen ngobrol serius sama bapak ibu kalau gini terus," tutur Uham sambil berekspresi lesu.

## Uham jadi 'korban' curhatan

Kata Uham, cukup sering bapak ibunya _berantem_ gara-gara masalah sepele. Usai _berantem_, bapak ibunya sering curhat ke Uham dan kakaknya sambil menegaskan bahwa, "bapak yang bener, karena ibu …" dan "ibu yang bener, karena bapak …" Uham mengaku stres, "kan bisa ya masalahnya di_obrolin_ dan diselesaikan tanpa perlu bawa anak-anaknya. Apalagi masalahnya kan gak berhubungan sama aku dan kak Deri."

Saya pun menimpali, "kan bapak sama ibumu pengin curhat ke kamu Ham."

"Iya sih, tapi gimana nasib curhatku ke bapak ibu? Sejak aku kesulitan _ngobrol_ serius sama bapak ibu, aku gak mikirin yang sepele (maksudnya masalah bapak ibunya yang sepele -ramdzi) kayak gitu. Berat di kepala."

Uham adalah orang yang benar-benar bisa jaga rahasia. Menurut teman-teman lainnya, dia sering jadi obyek curhatan, dan tidak ada satupun rahasia teman-temannya yang meluber ke jalanan. Saya rasa ini yang membuat Uham tidak peduli dengan masalah sepele yang mondar-mandir di rumahnya.

"Gara-gara bapak atau ibu sering curhat kalau mereka _berantem_, aku jadi _ngumpulin_ pendapat bapak tentang ibu dan pendapat ibu tentang bapak, yang aku jamin berakibat buruk kalau ini aku sampaikan ke mereka," kata Uham sambil bermuka masam.

"Otakmu gak pusing Ham jadi obyek curhatan temen-temenmu sama bapak ibumu?" tanya saya.

"Gak lah zi, _ngatasinnya_ gampang kok. Berselang waktu, karena curhatan itu gak berhubungan denganku, yang berarti tidak penting, lama-lama bakal hilang layaknya butiran debu. Otak siap menerima curhatan lain lagi, haha."

Terbukti, Uham memang tipikal orang introvert — melankolis plegmatis. Hehe.

## Komunikasi itu penting

"Zi, komunikasi itu benar-benar penting. Ada masalah, sampaikan. Ada yang kesel, sampaikan. Ada yang ini itu, sampaikan. Aku jamin zi, selama komunikasi itu terbuka dan jujur, segala masalah bisa diatasi."

Saya mengangguk mengiyakan.

"Jangan kayak aku zi. Masalah di keluarga terlihat rumit bahkan _stresful_ gara-gara komunikasi antar keluarga yang buruk. Ironi sih, aku penghamba 'komunikasi', eh dikeluargaku sendiri justru sulit berkomunikasi," tutur Uham.

"Ham, kalau kamu kesulitan _ngobrol_ serius sama bapak ibumu, gimana kalau mau _khitbah_ anak orang nanti?" Saya menyambung dengan gaya sok keren.

"Wkwk, asem kamu zi. Kecuali itulah, mau gak mau masalah itu harus kusampaikan ke bapak ibuku."

## Konklusi

Saya setuju 100% Ham. Komunikasi adalah hal yang sangat penting. Banyak orang salah paham gara-gara komunikasi, tapi setelah dikomunikasikan lagi pasti menjadi 'benar paham'.

Orang yang berkomunikasi saja bisa salah paham apalagi yang tidak berkomunikasi? Isinya hanya berupa kemarahan, _nggrundel_ yang memancing hipertensi.

Terima kasih Uham. Komunikasi menjadi kunci yang akan saya pegang di lingkungan keluarga sekarang dan keluarga nanti. #ea.
