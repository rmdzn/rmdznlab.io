---
title: "Perjalanan Peramban Favorit, dari Opera Sampai Pale Moon"
date: "2017-03-26"
categories: 
  - "tekno"
tags: 
  - "software"
aliases:
  - /post/2017-03-26-perjalanan-peramban-favorit-dari-opera-sampai-pale-moon/
---

**Update:** Sekarang saya kembali ke Firefox. Tidak lagi menggunakan Pale Moon.

**Update:** Sekitar satu tahun terakhir (2020 Q3), saya menjadi pengguna [Vivaldi](https://vivaldi.com/id).

Selamat datang Pale Moon!

Beberapa hari terakhir ini, saya resmi menjadi pengguna [Pale Moon](http://palemoon.org/), setelah cukup lama menggunakan [Firefox](https://www.mozilla.org/en-US/firefox/new/). Kasus ini mirip seperti saat saya meninggalkan [Opera](http://www.opera.com/) dan beralih ke Firefox.

Dulu, Pale Moon sempat saya coba ketika mencari peramban favorit baru pengganti Opera. Namun, karena beberapa hal saya putuskan untuk beralih ke Firefox. Beberapa tahun kemudian, saya mulai tidak menyukai Firefox, dan — terima kasih kepada pak foxy — saya diingatkan kembali dengan Pale Moon. :)

## Opera yang gagal menjaga loyalisnya

Sejak saya memegang komputer pertama kali, Opera adalah peramban favorit. Ringan dan kaya fitur, itulah alasan sederhana yang saya miliki saat memilih peramban berlogo 'O merah' ini. Saking loyalnya, di mana-mana saya menggunakan Opera, baik di komputer PC desktop milik kakak hingga di komputer warnet dengan Opera Portable.

Saya pun salah satu anggota Opera Fans Club di Kaskus, meski bukan seorang sesepuh. Kecintaan kepada Opera juga saya wujudkan dengan aktif di forum Opera untuk membantu pengguna lain yang mengalami masalah hingga berkontribusi dengan mengembangkan [_addons_ Jogja Streamers](https://addons.opera.com/en/extensions/details/jogja-streamers/?display=en-US) (bahkan sudah saya perbarui ke WebExtension).

![Peramban Opera](/blog/2017/03/images/opera-12-17-rmdz.jpg)

Saat itu, Opera membawa fitur yang saya sukai. Pengembangnya pun paham keinginan pengguna. Suara pengguna adalah hal segalanya!

Namun, bencana itu datang. Pada bulan Februari 2013, Opera memutuskan bergabung dengan keluarga WebKit dan segera membuang mesin _layout_ Prestonya. Opera 12.17 merupakan versi Opera terakhir yang saya pakai, ia meluncur tanggal 23 April 2014 sedangkan Opera 12.18 adalah Opera presto terakhir yang hadir dua tahun kemudian.

Ini keputusan yang sangat kontroversial. Banyak pengguna loyal yang kecewa. Meski begitu, Opera tetap _kekeuh_ menggunakan Webkit (yang selanjutnya Blink) dengan alasan bahwa mereka ingin fokus mengembangkan fitur alih-alih fokus membenahi masalah inti kode Presto (mesin _layout_ Opera 12.18 ke bawah).

Kekecewaan tidak hanya datang dari pengguna, tetapi juga dari pengembang Mozilla, Robert O’Callahan, [yang mengatakan bahwa](http://robert.ocallahan.org/2013/02/and-then-there-were-three.html) ini merupakan hari buruk bagi dunia Web. Opera berpotensi mengurangi standar web dengan beralih ke Webkit karena semua fitur-fitur hanya akan fokus ke Webkit. Dengan kata lain, "standar web" akan tergantikan dengan "standar Webkit".

Pernyataan seputar "standar web" yang berpotensi menjadi "standar Webkit" [juga disampaikan oleh Daniel Glazman](http://www.glazman.org/weblog/dotclear/index.php?post/2012/02/09/CALL-FOR-ACTION%3A-THE-OPEN-WEB-NEEDS-YOU-NOW) dari W3C CSS Working Group.

Saya cinta Presto dan segala fitur-fitur yang dia miliki di samping saya sendiri juga tidak begitu suka dengan Webkit. Jadi, saya beralih ke Firefox ..

## Firefox keren, tapi …

_Yap!_ Saya meloncat ke Firefox. Cukup berat beralih ke peramban dari Mozilla ini karena saya harus mengubah alur kerja (_workflow_) saat berselancar di internet, mengingat banyak fitur Opera presto yang tidak ada di sini.

Menggunakan Firefox, saya jadi mulai tidak begitu peduli dengan fitur. Cukup idealis memang. Karena saya jadi paham tentang konsep Open Web dan kebebasan pengguna. Apalagi, apa yang dilakukan para pengembang Firefox cocok seperti apa yang dilakukan pengembang Opera kepada pengguna (dulu).

Namun, hal ini berubah sejak Firefox mulai beralih ke tampilan Australis di versi 29 (29 April 2014). Tampilan yang mirip Google Chrome, _meh_ ..

Untuk satu ini, saya tidak terlalu kecewa, karena saya bisa beradaptasi dengan mudah, dan ini merupakan pembaruan yang menyegarkan bukan? ;)

![Peramban Firefox](/blog/2017/03/images/firefox-ss-fix-rmdz.jpg)

Namun, Firefox semakin aneh. Mereka justru menjadi antitesis prinsip mereka sendiri dengan menerapkan komponen proprietari DRM melalui EME di [Firefox 38](https://www.mozilla.org/en-US/firefox/38.0/releasenotes/). Meskipun, di balik itu, mereka tetap menghormati konsep _open-source_, kebebasan, dan privasi dengan menolak implementasi _Node Locking_ pada DRM. _Node Locking_ adalah konsep di mana untuk mengakses berkas digital harus melalui perangkat keras yang sama ketika pengguna membeli berkas tersebut. Misalnya, untuk melihat satu episode TV yang baru dibeli melalui ponsel, ya harus melalui ponsel, tidak boleh melalui PC. Tentang hal ini, Anda juga bisa membaca [tulisan Stephen Shankland dari CNet](https://www.cnet.com/news/mozilla-holds-its-nose-and-supports-drm-video-in-firefox/).

Penerapan ini ditujukan untuk meningkatkan pangsa pasar Firefox karena Mozilla terlalu khawatir jika tidak menerapkan teknologi kekinian, pengguna akan beralih ke peramban lain, di mana ini sebenarnya sudah terjadi sekian lama.

Anggap saja hal ini tak seserius yang dibayangkan, jadi saya beralih ke masalah terbaru.

Puncak kekecewaan saya terhadap Firefox adalah [implementasi PulseAudio yang menggantikan ALSA (situs mati)](https://kabarlinux.web.id/2017/sebagai-backend-sound-utama-firefox-gantikan-alsa-dengan-pulseaudio/) mulai Firefox 52.

Bukan .. bukan, saya bukannya menolak PulseAudio sebagai pengganti ALSA, tetapi menolak reaksi pengembang Mozilla atas hal ini. Mengenai PulseAudio, pengembang "melakukannya secara paksa", tak ada keterangan apapun di catatan rilis atau di tempat lain. Tiba-tiba saja pengguna GNU/Linux dengan ALSA sebagai _backend sound default_ tidak bisa menampilkan suara di Firefox 52.\[[1](#_footnote_1 "View footnote.")\].

Sebagai informasi, peramban sekelas Chromium saja masih mendukung ALSA. :)

Kata pengembang, selain hal teknis, penghapusan ALSA dilakukan karena mayoritas pengguna sudah tidak menggunakan teknologi ini. Informasi ini diakui didapat melalui fitur Telemetry pada Firefox. Jelas, demi privasi, tak semua pengguna mengaktifkan Telemetry. Cocok, sesuai prinsip Firefox yang menghargai privasi pengguna. Tetapi, para penjaga privasi ini sepertinya tak punya tempat. Mengenai ini, Anthony Jones, pengembang yang ditugasi menghapus ALSA di Firefox justru mengatakan,

"Telemetry adalah hambatan terendah untuk ikut memberikan umpan balik. Tak ada satupun di tim saya yang memiliki telepati."

(Setuju pak Jones. Bagus, jika ada orang yang mengaktifkan Telemetry. Tapi, saya pilih untuk tidak melakukannya. Bukankah itu pilihan untuk menjaga privasi seperti yang kalian gaungkan? -ramdzi)

"\[..\] bagaimanapun saya tidak merekomendasikan (pengguna) untuk menonaktifkan telemetry."

(Kejutan! Secara _default_ fitur Telemetry justru [tidak aktif di versi stabil](https://www.mozilla.org/en-US/privacy/firefox/#health-report). Telemetry aktif secara _default_ di Nightly dan Beta. -ramdzi)

"Telemetry mempengaruhi keputusan kami. Menonaktifkannya bukannya tanpa kerugian."

(Setiap pilihan memiliki konsekuensi dan resiko. Saya menghargai itu. Terima kasih Firefox! -ramdzi)

Komentar-komentar ini bisa dibaca di [laman Bugzilla Mozilla](https://bugzilla.mozilla.org/show_bug.cgi?id=1345661#c122).

Jadi, selamat tinggal Firefox …

## Halo Pale Moon!

Seperti yang saya sampaikan di atas, saya kembali mencoba Pale Moon dan akhirnya cocok, meski beberapa _addons_ favorit di Firefox tidak bisa dipasang di peramban ini.

Pale Moon memiliki semangat layaknya Firefox jaman dulu. Tampilannya pun ala Firefox lawas, dengan fitur Tabs on Top untuk memindah Tab ke atas layaknya Firefox baru dan Google Chrome. Bagi saya, fitur ini sebenarnya tidak berguna, karena saya lebih menyukai "Firefox dulu".

Tidak hanya itu, Pale Moon juga mempunyai semangat layaknya pengembang Opera jaman dulu: peduli dengan pengguna.

![Peramban Pale Moon](/blog/2017/03/images/palemoon-ss-fix-rmdz.jpg)

Di sisi teknis, Pale Moon berusaha menyajikan peramban dengan kebutuhan dasar (_basic needs_) yang "sempurna", tak perlu ada fitur-fitur baru yang sok-sok kekinian yang justru mengorbankan keamanan dan kestabilan fitur-fitur kebutuhan dasar (_basic needs_). Misalnya, Pale Moon tidak akan mendukung DRM dan _gamepad_.\[[2](#_footnote_2 "View footnote.")\]

Hingga saat ini, dibandingkan Firefox, Pale Moon super stabil, sama sekali tidak gampang _crash_ apalagi _hang_.

Buat yang pengin instal Pale Moon, silakan unduh _installer_\-nya di [palemoon.org](http://linux.palemoon.org/) mengingat tak banyak distro populer yang menyediakan peramban ini di repositori resminya.

Terima kasih Pale Moon! (Ini bukan pesan hasil _endorse_ resmi) :D

* * *

[1](#_footnoteref_1). Tentang ini, silakan baca [http://web.ncf.ca/fn352/ubuntu3.html#09Mar17](http://web.ncf.ca/fn352/ubuntu3.html#09Mar17)

[2](#_footnoteref_2). Survei Pale Moon yang layak dibaca [http://www.palemoon.org/survey2017/](http://www.palemoon.org/survey2017/)
