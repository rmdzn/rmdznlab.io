---
title: "Ternyata, Adblock Memblokir Tulisan Advertorial"
date: "2017-04-01"
categories: 
  - "blog"
tags: 
  - "cuap"
aliases:
  - /post/2017-04-01-ternyata-adblock-memblokir-tulisan-advertorial/
---

Saya bukanlah penggemar _adblock_ \[[1](#_footnote_1 "View footnote.")\] ([AdBlock](https://getadblock.com/), [AdBlock Plus](https://adblockplus.org/), [uBlock](https://www.ublock.org/), maupun [uBlock Origin](https://github.com/gorhill/uBlock)). Saya biasa menggunakan [Privacy Badger dari EFF](https://www.eff.org/privacybadger) untuk menghalau _tracker-tracker_ yang berseliweran di dunia maya di peramban Firefox.

Namun, sejak saya beralih ke [Pale Moon](/post/2017/03/2017-03-26-perjalanan-peramban-favorit-dari-opera-sampai-pale-moon/), saya juga beralih ke uBlock Origin mengingat _addons_ Privacy Badger memang tidak mendukung Pale Moon.

Menggunakan uBlock Origin, saya jadi tahu, ternyata keluarga _adblock_ sangat "kejam", selain memblokir iklan-iklan _banner_ yang muncul di _post_, sidebar, dll, mereka juga memblokir tulisan yang berada di kategori **Advertorial**. Sisi "kejam" lain, _adblock_ juga akan memblokir tulisan Adertorial di laman administrator (wp-admin kalau di Wordpress).

Ada dua sifat pemblokiran. AdBlock, uBlock, dan uBlock Origin, hanya memblokir tulisan Advertorial, yang berarti hanya judul dan konten _post_ yang diblokir mereka. Lihat di bawah ini:

![Halaman advertorial yang terblokir](/post/2017/04/images/adblock-blokir-advertorial-1-rmdz.jpg)

Sedangkan AdBlock Plus akan memblokir seluruh isi laman Advertorial tersebut. Halaman pun jadi kosong total. Lihat bawah ini:

![Halaman advertorial lain yang terblokir](/blog/2017/04/images/adblock-blokir-advertorial-2-rmdz.jpg) Tulisan advertorial yang diblokir AdBlock Plus (Taufik Mulyana)

## Apa yang dilakukan adblock?

Untuk memblokir konten Advertorial, _adblock_ mendeteksi kata "advertorial" pada URL. Oleh sebab itu, segala tulisan blog yang berada di dalam _contoh.com/ini/itu/terserah/deh/advertorial_ akan terblokir.

## Bagaimana cara mengatasi pemblokiran Advertorial?

Sebenarnya mudah, kamu hanya cukup membuat kategori dengan nama "Advertorial" lalu buat _slug_ (URL) nya dengan nama selain "advertorial", misalnya bermain _typo_ "advertoria" atau dengan kata lain; "iklan", "tulisan-bersponsor", dll.

## Dari tadi ngomongin Advertorial melulu, sebenarnya apa sih "advertorial" itu?

Disebutkan oleh Kamus Besar Bahasa Indonesia, [advertorial](https://kbbi.kemdikbud.go.id/entri/advertorial) adalah iklan yang berupa berita (bukan gambar atau poster). Arti yang sudah cukup mendeskripsikan maksud dari "advertorial" ini.

Jangan salah, "advertorial" ini ada peraturannya lho .. Peraturan penggunaan "advertorial" sebagai pembeda dari tulisan-tulisan berita termaktub di Peraturan Dewan Pers Nomor1/Peraturan-DB/III/2012 tentang Pemberitaan Media Siber.\[[2](#_footnote_2 "View footnote.")\]

Lihat peraturan 6 tentang Iklan. Disebutkan:

1. Media siber wajib membedakan dengan tegas antara produk berita dan iklan.
2. Setiap berita/artikel/isi yang merupakan iklan dan atau isi berbayar wajib mencantumkan keterangan "advertorial", "iklan", "ads", "sponsored", atau kata lain yang menjelaskan bahwa berita/artikel/isi tersebut adalah iklan.

* * *

[1](#_footnoteref_1). Tolong bedakan adblock dengan AdBlock :)

[2](#_footnoteref_2). [http://dewanpers.or.id/assets/media/file/kebijakan/667072\_peraturan%20Dewan%20Pers%20tentang%20pedoman%20pemberitaan%20media%20siber\_final.pdf](http://dewanpers.or.id/assets/media/file/kebijakan/667072_peraturan%20Dewan%20Pers%20tentang%20pedoman%20pemberitaan%20media%20siber_final.pdf)
