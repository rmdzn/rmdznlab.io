---
title: "Siapakah Troll?"
date: "2017-04-18"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "troll-face-rmdz.jpg"
aliases:
  - /post/2017-04-18-siapakah-troll/
---

![troll](/blog/2017/04/images/troll-face-rmdz.jpg)

Internet memang kejam. Dia "melahirkan" beragam sikap manusia yang di dunia nyata sebenarnya sulit ditemukan. Salah satu sikap tersebut adalah _trolling_ yang dilakukan oleh tukang _troll_.

## Jadi, siapakah Troll?

Saya tidak menemukan asal muasal pasti kata "troll". Apakah makhluk Skandinavia yang mengerikan dan jahat? Atau berasal dari kata "_trolling_" yang berarti memancing (ikan) dengan tali?

Yang jelas, kata "_troll_" yang saya maksud adalah orang yang mengirim pesan (atau juga pesan itu sendiri) di internet dengan tujuan untuk membangkitkan tanggapan emosional atau kemarahan dari pengguna lainnya. Saya mengambil arti ini dari Wikipedia.\[[1](#_footnote_1 "View footnote.")\]

Keterangan di atas terlalu umum, sehingga saya mengelompokkan siapa saja _troll_ yang dimaksud. Berikut pengelompokannya:

1. **Troll adalah orang yang memaksakan kultur komunitas mereka masuk ke kultur komunitas lain**Troll punya komunitas sendiri. Namun bukan komunitas yang secara implisit menamakan diri sebagai " _Komunitas Troll_", bukan, tetapi komunitas tertentu yang anggota-anggotanya membiasakan diri bertindak sebagai _troll_.
    
    Para _troll_ ini bukanlah _troll_ ketika mereka membahas sesuatu di komunitas mereka sendiri. Namun, mereka akan menjadi _troll_ ketika mereka memaksakan kultur _troll_ komunitas mereka ke komunitas lain.
    
    Contohnya, anak Yimyam. Mereka bukanlah _troll_ saat mereka mendiskusikan suatu peristiwa dengan candaan khas mereka di komunitas Yimyam. Namun, ketika mereka membawa-bawa kultur candaan mereka ke komunitas lain, saya akan menamakan mereka seorang _troll_.
    
    Sikap _troll_ ini sepertinya sulit dibenahi, karena sudah mengakar kuat dan mengasumsikan semua orang disekitarnya adalah orang _troll_ atau orang yang harus mendapatkan sikap _trolling_.
2. **Troll adalah orang ngeyel** Ketika sedang mendiskusikan sesuatu, apakah kamu pernah menemukan orang ngeyel? Pasti pernah, saya pun pernah.Kata "ngeyel" yang saya maksud merujuk pada sikap "diberitahu tapi tidak mendengarkan dan melakukan". Kalau dalam budaya Jawa disebut _Blungentuwa_ atau _Mlebu tengen metu kiwa_ (Masuk telinga kanan, keluar telinga kiri).
    
    Contohnya, ketika seseorang bernama X mengaku masih pemula ikut nimbrung di komunitas tertentu. Si X tidak mengindahkan ketika orang-orang di komunitas tersebut sudah memberitahu dirinya bahwa ia harus melakukan ini itu untuk mendapatkan ini itu.
    
    _Troll_ seperti ini sulit dijinakkan karena mereka sudah dari sananya ngeyel dan dengan mudah meremehkan sesuatu.
3. **Troll adalah orang yang sedang emosional dan memaksa diri untuk terjun dalam sebuah diskusi** Untuk kategori ini, sikap _trolling_ muncul sesuai situasi dan kondisi. Kita mungkin pernah menjadi _troll_ secara sadar atau tidak dan masuk di kategori ini.\[[2](#_footnote_2 "View footnote.")\]_Troll_ jenis ini hanya angin lalu, yang tidak terlalu membahayakan komunitas, karena ia hanya menjadi _troll_ satu dua kali dan tidak berminat untuk melakukannya lagi.

## Mengatasi troll di internet

Untuk mengatasi _troll_ di internet atau komunitas daring cukup sederhana.

1. **Diamkan**Jangan tanggapi seorang _troll_. Kalau dalam bahasa Inggris populer dengan: "_Don’t feed the trolls_". Seorang _troll_ akan bosan sendiri ketika mereka didiamkan karena _trolling_ yang mereka lakukan tidak berhasil. [Jangan keburu jadi orang ini](https://www.xkcd.com/386/).
    
    Lalu bagaimana kita tahu kalau dia seorang _troll_ jika tidak ditanggapi?
    
    Tergantung situasi. Jika ada penggemar sepeda BMX masuk ke komunitas daring Polygon, kemudian menjelek-jelekkan Polygon dan mengunggulkan sepeda BMX. Apa iya kita harus menanggapi mereka?
    
    Selain situasi tersebut. Kita boleh menanggapi mereka dengan kalimat-kalimat tertentu untuk memastikan bahwa mereka _troll_ atau bukan. Tak ada rumus kalimat yang digunakan di langkah ini. Itu tergantung kita.
2. **Tanggapi dan peringatkan**Jika sudah ditanggapi beberapa kali, ada potensi _trolling_ yang disampaikan oleh orang terduga _troll_, diamkan dulu. Jika mereka kembali mengulanginya beberapa kali, peringatkan.
3. **Keluarkan**Jika peringatan sudah tidak mereka indahkan dan mereka berulang kali melakukan hal sama serta membuat gaduh komunitas, keluarkan. Tak perlu ragu.

_Troll_ sangat mengganggu. Saya akui itu. Jadi, semoga informasi di atas cukup berguna. Jika ada tips atau informasi lain jangan ragu untuk menghubungi saya atau berikan komentar di bawah ini. :)

Saya punya wejangan untuk para _troll_:

> Jadilah _troll_ bermartabat dan baik hati dengan cara menutup mulut.

Oiya, saya adalah anggota dan administrator/moderator beberapa komunitas daring. Pandangan di atas adalah "riset" saya saat memegang dua "jabatan" tersebut.

* * *

[1](#_footnoteref_1). [https://id.wikipedia.org/wiki/Troll\_internet](https://id.wikipedia.org/wiki/Troll_internet), diakses tanggal 18 April 2017, pukul 12.43

[2](#_footnoteref_2). [http://internetsehat.id/2017/02/siapapun-bisa-menjadi-troll-internet/](http://internetsehat.id/2017/02/siapapun-bisa-menjadi-troll-internet/), diakses 18 April 2017, pukul 13.51
