---
title: "[Update] Situs Menyeramkan Ini Pamerkan Data Pribadi Kita, Hati-Hati!"
date: "2017-04-25"
categories: 
  - "privasi"
tags:
  - "privasi"
aliases:
  - /post/2017-04-25-situs-menyeramkan-ini-pamerkan-data-pribadi-kita-hati-hati/
---

Privasi itu penting, apalagi di era digital ini di mana orang dengan mudah melanggar privasi orang lain maupun menyerahkan data pribadi mereka secara "sukarela" kepada orang lain.

Awal tahun ini, orang-orang di Amerika _sono_ heboh dengan situs [FamilyTreeNow.com](http://FamilyTreeNow.com), situs untuk melacak silsilah keluarga dengan hanya memasukkan nama depan, nama belakang, dan negara bagian. Situs ini juga menyediakan fitur untuk ikut berkontribusi, dengan cara mengisi data supaya kelak ada keluarga yang menemukan kita berdasarkan nama depan dan nama belakang kita. Menakuuuutkaaaann …​ Lebih menakutkan lagi, hal ini bukanlah tindakan ilegal.\[[1](#_footnote_1 "View footnote.")\]

Ooh itu hanya di Amerika kok, di Indonesia berarti aman.

Benar, kita aman dari situs tersebut tetapi secara bersamaan kita ternyata tidak aman dari situs lain yang bertingkah serupa.

Situs lain tersebut saya temukan saat iseng-iseng mencari nama saya di Google. Saya langsung kaget, ternyata di situ ada nama saya, alamat, beserta nomor telepon/HP. Sebenarnya data pribadi saya yang ada di situs ini cukup aman, karena nama dan alamat saya tidak cukup lengkap. Tapi, banyak data orang lain sangat lengkap muncul, dari nama hingga alamat (nomor rumah, jalan, kecamatan, kabupaten, dst) di laman ini.

Lebih seramnya, nama belakang berupa tautan ke laman yang menunjukkan daftar nama orang dengan nama belakang sama. Misalnya, nama belakang saya "Yustitianto", nama ini bisa diklik menuju laman yang berisi daftar orang dengan nama "Yustitianto". Bagi orang yang menerapkan nama keluarga, fitur ini sangat menyeramkan.

Situs yang saya maksud adalah **[www.locatefamily.com](http://www.locatefamily.com)**.

![Situs LocateFamily.com](/blog/2017/04/images/locatefamily-site-rmdz.jpg)

## Bagaimana LocateFamily.com mendapatkan data saya?

Menurut saya data-data ini diambil dari hasil _grab_ atau _scrap_ situs atau layanan lain. Dari pola nama dan alamat yang muncul, khusus untuk data saya sepertinya diambil dari whois domain yang saya beli.

## Cara hapus data dari LocateFamily.com

Sebelumnya, pastikan nama kita muncul di situs ini dengan cara mencari data kita dengan kata kunci "**locatefamily.com nama kita**" di [Google](http://encrypted.google.com/) (DuckDuckGo tak melakukannya dengan baik sehingga saya sarankan kamu pakai Google -- maaf). Atau cari langsung lewat situs locatefamily.com.

Beruntung, seperti FamilyTreeNow.com yang menyediakan fitur _optout_, LocateFamily.com juga menyediakan laman khusus yang mengijinkan kita menghapus data kita dari situs tersebut. Laman ini ada di [www.locatefamily.com/removal.html](http://www.locatefamily.com/removal.html).

Untuk menghapus data silakan isi **nama** dan **nomor telepon/HP** (syarat yang menyebalkan, tapi demi data yang terhapus kita perlu melakukannya). Masukkan **posel (_email_)**, **URL atau tautan** yang menampilkan data kita, masukkan **nomor** urut data kita di tautan tersebut, dan centang pada "_Check here to be notified when LocateFamily.com is updated_" untuk mengetahui jika data kita sudah terhapus atau muncul lagi akibat situs galat. Kemudian Submit.

Saya sedang mengirimkan permintaan penghapusan data pribadi saya. Bila sudah terhapus, saya akan mengabarkan lagi via artikel ini.

**Update (27/4):**

Beberapa menit sebelum saya menulis artikel ini, saya meminta mereka untuk menghapus data saya lewat laman _removal_. Mereka berjanji menghapusnya dalam waktu 24 sampai 48 jam.

Jam 23.00 WIB malam kemarin (26/4), data saya sudah terhapus sepenuhnya.

Sebagai catatan, jika data kita masih terlihat di hasil pencarian Google, itu normal, mengingat Google masih menyimpan _cache_ laman locatefamily.com yang menampilkan data kita.

* * *

[1](#_footnoteref_1). [consumerist.com/2017/01/13/its-creepy-but-not-illegal-for-this-website-to-provide-all-your-public-info-to-anyone/](https://consumerist.com/2017/01/13/its-creepy-but-not-illegal-for-this-website-to-provide-all-your-public-info-to-anyone/), diakses 25 April, jam 22.00
