---
title: "Panggil Sesuai Usia atau Silsilah?"
date: "2017-06-23"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "keluarga-siluet-rmdz.jpg"
aliases:
  - /post/2017-06-23-panggil-sesuai-usia-atau-silsilah/
---

![keluarga](/blog/2017/06/images/keluarga-siluet-rmdz.jpg)

Apa yang Anda rasakan saat harus memanggil orang belasan tahun lebih tua dengan 'dik'? Atau sebaliknya, memanggil orang dengan silisih usia belasan tahun lebih muda dengan 'mas'?

Canggung? Tidak enak? Tidak nyaman? Atau justru terbiasa dan menikmati?

Lalu, bagaimana perasaan Anda saat melihat orang seperti di atas?

Bingung? Merasa aneh sambil berteriak, "ada apa dengan planet ini?!!"

Omong-omong keluarga saya adalah salah satu dari keluarga yang menerapkan panggilan sesuai silsilah keluarga. Sebelum mas/mbak pembaca sekalian memincingkan mata, saya akan sedikit mendongeng tentang silsilah keluarga dalam satu blok tanah, di mana salah satu pembagian blok tanah itu adalah rumah yang saya tinggali saat ini.

## Silsilah keluarga

Simbah buyut saya (generasi 1) adalah pemilik satu blok tanah yang saat ini terbagi dalam lima blok tanah. Beruntung, bapak dari bapak saya (generasi 2) adalah putra pertama dari simbah buyut sehingga keluarga yang menempati empat blok tanah lainnya secara otomatis akan memanggil bapak saya (generasi 3) dengan 'kang' atau 'mas'.

> Terasa membingungkan? Tak apa, tak perlu dipikir keras kecuali situ berminat jadi menantu bapak/ibu saya.

Secara otomatis pula, saya (generasi 4) adalah putra "tertua" dari para putra/i yang menempati empat tanah lainnya. Berarti, saya adalah kakak (mas) dari putra/i tersebut.

Dari sini terlihat, saya "harus" memanggil mereka dengan 'dik'.

## Panggil sesuai silsilah

Sejak kecil, saya dan kakak saya diajari untuk memanggil para saudara generasi 4 ini dengan 'dik', minimal untuk satu keluarga yang kami sangat akrab dengannya. Generasi 4, yang berjumlah empat orang, dari keluarga tersebut memiliki usia yang sangat jauh di atas kami, belasan tahunlah.

Dulu ada kejadian menarik. Sewaktu kakak saya pulang sekolah, entah TK atau SD, dia melewati sawah di mana salah satu putri dari keluarga tersebut sedang menanam/memanen padi. Secara spontan, kakak saya menyapa, "dik!"

Rekan yang disapa pun heran, "lho, kok kamu dipanggil 'dik'?"

"Lah, tepatnya dia (kakak saya -pen) itu memang kakak saya," timpal yang disapa.

Sampai saat ini pun kami biasa memanggil mereka dengan sebutan 'dik', meski sering terdistraksi dengan menyebut 'bro' atau menyebut 'bapak/ibunya si anu'.

Di sisi keluarga ibu, bulik (adik ibu) dan kakak saya usianya tak terpaut jauh. Zaman kecil, meski bulik saya masih SD/SMP sudah dipanggil sebagai 'bulik' oleh kakak saya, di mana saat itu bulik saya cukup risih, "masih mudah kok dipanggil bulik??". Dan juga, silsilah dari simbah ibu saya jika diturut hingga ke bawah, ada seorang pakde yang usianya juga tak jauh dari saya dan kakak saya, bahkan lebih muda dari ibu saya. Tebak, dia juga kami panggil "pakde!" 😏

## Karena pembiasaan

Untuk mengajarkan pemanggilan sesuai silsilah keluarga ini sebenarnya sangat sederhana.

Layaknya orang tua lain yang mengajar anaknya untuk memanggil si anu dengan 'mas anu', 'dik anu', 'om anu', dsb, sejak dulu bapak/ibu saya memanggilkan saudara kami dengan 'dik A', 'dik B', 'dik C', dll. Sehingga hal itu menjadi kebiasaan, bahkan menjadi "keharusan" ketika kami diperkenalkan dengan orang baru yang ternyata saudara jauh dengan mengatakan, "_nah, mas/mbak kui prenahe <dik, mas, mbak, pakde, paklik, mbah dll>_" (nah, mas/mbak itu tepatnya adalah <..>).

## Jadi?

Saya berbagi tentang hal ini karena penasaran, apakah keluarga Anda menerapkan hal yang sama? Atau menerapkan panggilan sesuai usia; 'mas' untuk yang lebih tua, 'dik' untuk yang lebih muda, 'mas' untuk yang lebih tua sedikit padahal sesuai silsilah adalah 'pakde' (kakak dari bapak), dan kasus-kasus panggilan lainnya?

Omong-omong kalau saat ini keluarga pembaca tidak menerapkan panggilan sesuai silsilah, apakah di masa mendatang akan menerapkannya?

Saya tidak menspesialkan orang-orang yang menerapkan hal yang sama seperti keluarga saya, tetapi saya tertarik melihat orang lain dengan "kecenderungan yang sama", mengingat di lingkungan saya pembiasaan untuk memanggil sesuai silsilah ini sudah sulit ditemukan, lebih banyak orang tua yang mengajarkan anaknya untuk memanggil sesuai umur.

Boleh _nih_ isi jajak pendapat di bawah ini~

\[polldaddy poll="9775090"\]

_Featured image_: Oleh Eric Ward \[[CC BY-SA 2.0](http://creativecommons.org/licenses/by-sa/2.0)\], [via Wikimedia Commons](https://commons.wikimedia.org/wiki/File%3AFamily_Portrait.jpg)
