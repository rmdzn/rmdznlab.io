---
title: "Aksi Menentang Obesistuff, Setuju!"
date: "2017-08-10"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "baju-baju-maxpixel-1-rmdz.jpg"
aliases:
  - /post/2017-08-10-aksi-menentang-obsesistuff-setuju/
---

![baju-baju](/blog/2017/08/images/baju-baju-maxpixel-1-rmdz.jpg)

Beberapa hari yang lalu, di media sosial viral tentang seorang ibu rumah tangga yang sadar bahwa selama ini beliau salah "mengoleksi" banyak barang yang belum tentu dipakai dan bermanfaat bagi dirinya.

Tulisan ini berjudul [_Obesistuff: Penyakit Muslimah Kekinian?_](http://rumahketjilku.blogspot.co.id/2017/08/obesistuff-penyakit-muslimah-kekinian_10.html), cocok dibaca buat akhwat/mbak/emak yang punya hobi "beli ini beli itu".

Dalam tulisan tersebut, si ibu yang menyebut blognya sebagai "Rumah Ketjil" mereferensikan buku berjudul _Life Changing Magic Of Tidying Up_ yang ditulis oleh Marie Kondo. Di situ, bu Marie Kondo mengajarkan kita untuk benar-benar menyimpan apa saja yang bermanfaat bagi kita dan menyingkirkan apa saja yang hanya menghabiskan ruang lemari. Saya belum membaca buku tersebut, tapi sudah saya masukkan radar untuk saya beli nanti, terima kasih buat mbak [Dee Lestari atas ulasannya](http://deelestari.com/review-the-life-changing-magic-of-tidying-up/)!

Saya pernah mendengar judul buku ini, entah di mana saya lupa, tapi itu hanya angin lalu, dan baru _ngeh_ setelah bu Rumah Ketjil menyinggungnya pada tulisan di atas. Gara-gara itu saya pun baru sadar, ternyata selama ini saya meyakini apa yang disampaikan bu Kondo meskipun saya belum pernah membaca buku beliau. Saya berprinsip, saya hanya beli yang memang saya perlukan.

Contohnya kaos. Oke lah, belum banyak kaos yang saya beli sendiri, kebanyakan adalah kaos lawas pemberian orang tua atau _lungsuran_ mas, dan kaos yang relatif baru hasil "hadiah" sebagai peserta/panitia acara tertentu. Namun, dari sekian banyak kaos tersebut, saya bisa hidup hanya dengan 3-4 kaos dalam seminggu. _Ringgo_ (_garing dinggo_) kuncinya, cuci sebentar, kering sehari, dipakai lagi. Begitu juga dengan baju, jaket, dan celana panjang yang cukup satu saja bisa dipakai dalam seminggu, karena saya memang jarang berpakaian formal saat keluar rumah.

Saya laki-laki, sehingga mungkin saya dianggap mudah melakukan hal itu. Jadi, maap buat mbak-mbak di luar sana …​ ;p

Gara-gara sikap seperti ini pun, saya termasuk orang ngeyel kalau disuruh pilih atau beli pakaian baru. Jadi, harap maklum. Kalaupun ada uang, mending beli buku baru dan bayar tagihan _hostingan_ blog sebelah. Iya toh :3

Mari tentang Obesistuff!
