---
title: "Mengakali Video Embed YouTube yang Tak Responsif"
date: "2017-09-23"
categories: 
  - "tutorial"
tags:
  - "tutorial"
aliases:
  - /post/2017-09-23-mengakali-video-embed-youtube-yang-tak-responsif/
---

Saya baru tahu, ternyata selama ini video YouTube hasil _embed_ tidak responsif. Jadi, meskipun tema blog yang kita gunakan sangat keren dan responsif, hasil video YouTube yang tertanam (_embed_) akan "tumpah" layaknya perut gendut di atas ikat pinggang ketika blog kita dibuka melalui ponsel atau perangkat dengan resolusi kecil.

Lalu bagaimana cara mengatasinya?

Setelah [_mbebek_](http://duckduckgo.com/), ketemulah salah satu artikel untuk menjadikan video YouTube tampil secara responsif. Omong-omong, tak hanya satu dua orang narablog saja yang mengalami masalah serupa mengingat tips atau tutorial yang muncul di mesin pencari juga banyak dan solusi paling banyak adalah memanfaatkan CSS.

Langsung saja, kita cukup _copas_ skrip di bawah ini ke berkas _stylesheet_ CSS.

```
.video-container {
  position:relative;
  padding-bottom:56.25%;
  padding-top:30px;
  height:0;
  overflow:hidden;
}

.video-container iframe, .video-container object, .video-container embed {
  position:absolute;
  top:0;
  left:0;
  width:100%;
  height:100%;
}
```

Di kemudian hari, video YouTube akan menjadi responsif saat kita memasukkan skrip _embed_ ke dalam _div_ dengan _class_ _video-container_.

Saya sudah bereksperimen dengan cara tersebut melalui [Kabar Linux (situs mati)](https://kabarlinux.web.id/2017/efek-typewriter-dan-fitur-pengaturan-properti-dasar-video-kdenlive-sukses-dibenamkan/), sayangnya video YouTube memenuhi lebar (_width_) artikel dan tidak menghormati pengaturan `.. width="700" height="400" ..` yang sudah saya set sebelumnya.
