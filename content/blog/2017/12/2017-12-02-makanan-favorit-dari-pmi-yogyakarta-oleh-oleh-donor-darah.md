---
title: "Makanan Favorit dari PMI Yogyakarta, Oleh-oleh Donor Darah"
date: "2017-12-02"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "pmi-oleh-oleh-makanan-rmdz.jpg"
aliases:
  - /post/2017-12-02-makanan-favorit-dari-pmi-yogyakarta-oleh-oleh-donor-darah/
---

Dua tahun menjadi pendonor darah WB (_Whole Blood_), saya baru sekali ini memfavoritkan oleh-oleh cemilan dari PMI Yogyakarta. Ueenaaak!

Sekitar tahun 2014 sampai 2015 awal, PMI mengoleh-olehi dua cemilan dalam tas donor: Pocari Sweat ukuran kecil dan Mie Sedaap Cup. Saya sekadar suka Pocari Sweat, tidak menjadi penggemar garis keras yang ke mana-mana harus minum minuman yang saat ini semakin populer gara-gara Yuki Sasou-nya. Sedangkan Mie Sedaap Cup, saya suka karena mengenyangkan. Tekstur minya pun lebih tebal menyenangkan dari pada Pop Mie. Namun, kalau dipikir-pikir lagi, makanan tidak sehat seperti ini _kok_ menjadi menu utama untuk pendonor darah.

Sekitar pertengahan tahun 2015 sampai 2016 akhir, Mie Sedaap Cup diganti dengan Malkist Roma rasa original. Kalau Malkist coklat boleh lah, tapi kalau rasa original (gurih manis karena ada gulanya) sepertinya tidak cocok dengan lidah saya. Kecuali kalau dimakan sambil minum kopi susu atau susu. Sedangkan untuk minumnya, Pocari Sweat diganti dengan susu sereal Milko rasa coklat. Entahlah, susu macam apa ini, saya sama sekali belum pernah melihat iklan di internet atau TV, bahkan di minimarket Alfarames dan Indorames.

Kepopuleran sebuah makanan atau minuman tidak selalu sebanding dengan rasa, paling tidak menurut saya sendiri. Seperti Milko. Ia punya cita rasa seperti susu Dancow coklat, tapi dengan tekstur yang lebih kental. Jelas, kekentalan yang dimaksud merujuk pada _sereal_ dalam _branding_ _susu sereal_. Favorit!

Tahun 2017 akhir, cemilan berganti lagi. Malkist Roma diganti dengan Malkist Crackers HATARI dengan krim kacang. _Hooo_ .. makanan apa lagi ini? Setelah menghabiskan satu lembar, saya sadar ternyata ini menjadi makanan terfavorit selama saya rutin donor darah WB di PMI Yogyakarta. Bentuk _cracker_ pada umumnya sama. Dua _cracker_ dilapisi gula lalu ditelangkupkan dengan krim kacang pada bagian tengah. Tidak tahu apakah krim kacang ini seenak selai mahal Nutella :3

![Makanan dari PMI Yogyakarta](/blog/2017/12/images/pmi-oleh-oleh-makanan-rmdz.jpg) Makanan dari PMI Yogyakarta (Pocari dan madu yang sudah tergiling perut)

Setengah bulan lalu saya kembali donor dan mendapatkan lebih banyak oleh-oleh cemilan! Selain suplemen makanan Fermia yang sejak dahulu pasti diberikan kepada pendonor, ada minuman Pocari Sweat botol kecil dan susu sereal Milko rasa kacang hijau (saya belum minum ini, _eman-eman_ ;p), ditambah madu saset dan HATARI, yooot.

Tunggu bulan Januari 2018 nanti, apakah PMI Yogyakarta masih memberikan oleh-oleh cemilan sebanyak dan sefavorit seperti itu X)
