---
title: "Konverter Teks ke Sandi Morse dengan Bahasa Java"
date: "2017-12-10"
categories: 
  - "ngelmu"
tags: 
  - "tutorial"
aliases:
  - /post/2017-12-10-konverter-teks-ke-sandi-morse-dengan-bahasa-java/
---

**Catatan:** saya sedang menguji fitur pos otomatis via umpan RSS blog ini ke kanal Telegram @ramdzianachan (privat) menggunakan IFTTT, jadi maafkan jika pos ini tidak begitu bermanfaat.

Omong-omong, program ini saya tulis sebagai salah satu tugas matkul Pemrograman Berorientasi Obyek (OOP) beberapa tahun lalu.

```
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import org.apache.commons.lang3.StringUtils;
    public class Morse extends JFrame implements ActionListener{
        JPanel container;
        JTextField textKata,textMorse;
        JButton btnConvert1,btnAbout;
        JMenuBar menu;
        JMenu menuFile;
        JMenuItem submenuExit;
        JLabel textKataLabel,textMorseLabel;
        Font fontMorse,fontKata;

        public String morse\[\]=new String\[100\];
        public char huruf\[\]=new char\[300\];
        public int panjangKata;
        public String kataMorse;
        
        public Morse(){         
            GridBagLayout gridBagLayout = new GridBagLayout();
            
            gridBagLayout.columnWidths = new int\[3\];
            gridBagLayout.rowHeights = new int\[5\];
            container=new JPanel();
            getContentPane().add(container);
            container.setLayout(gridBagLayout);
            
            textKataLabel=new JLabel("Teks: ");
            GridBagConstraints gbctextKataLabel=new GridBagConstraints();
            gbctextKataLabel.insets = new Insets(5, 5, 5, 5);
            gbctextKataLabel.fill = GridBagConstraints.HORIZONTAL;
            gbctextKataLabel.gridx=0;
            gbctextKataLabel.gridy=0;
            gbctextKataLabel.anchor=GridBagConstraints.NORTHWEST;
            container.add(textKataLabel,gbctextKataLabel);

            fontKata = new Font("Arial", Font.BOLD,13);
            textKata=new JTextField("",10);
            textKata.setFont(fontKata);
            GridBagConstraints gbctextKata=new GridBagConstraints();
            gbctextKata.insets = new Insets(5, 5, 5, 5);
            gbctextKata.ipady=20;
            gbctextKata.gridwidth=2;
            gbctextKata.gridx=0;
            gbctextKata.gridy=1;
            gbctextKata.weightx=0.5;
            gbctextKata.fill=GridBagConstraints.HORIZONTAL;
            container.add(textKata,gbctextKata);

            textMorseLabel=new JLabel("Sandi Morse: ");
            GridBagConstraints gbctextMorseLabel=new GridBagConstraints();
            gbctextMorseLabel.insets = new Insets(5, 5, 5, 5);
            gbctextMorseLabel.fill = GridBagConstraints.HORIZONTAL;
            gbctextMorseLabel.gridx=0;
            gbctextMorseLabel.gridy=2;
            gbctextMorseLabel.anchor=GridBagConstraints.WEST;
            container.add(textMorseLabel,gbctextMorseLabel);

            fontMorse = new Font("Arial", Font.BOLD,13);
            textMorse=new JTextField("",10);
            textMorse.setFont(fontMorse);
            GridBagConstraints gbctextMorse=new GridBagConstraints();
            gbctextMorse.insets = new Insets(5, 5, 5, 5);
            gbctextMorse.fill = GridBagConstraints.HORIZONTAL;
            gbctextMorse.gridx=0;
            gbctextMorse.gridy=3;
            gbctextMorse.gridwidth=2;
            gbctextMorse.ipady=20;
            gbctextMorse.weightx=0.5;
            gbctextMorse.anchor=GridBagConstraints.WEST;
            container.add(textMorse,gbctextMorse);

            btnConvert1=new JButton("Convert");
            GridBagConstraints gbcbtnConvert1=new GridBagConstraints();
            gbcbtnConvert1.fill = GridBagConstraints.WEST;
            gbcbtnConvert1.anchor=GridBagConstraints.SOUTHWEST;
            gbcbtnConvert1.insets = new Insets(5, 5, 5, 5);
            gbcbtnConvert1.gridx=0;
            gbcbtnConvert1.gridy=4;
            container.add(btnConvert1,gbcbtnConvert1);

            btnAbout=new JButton("About");
            GridBagConstraints gbcbtnAbout=new GridBagConstraints();
            gbcbtnAbout.fill = GridBagConstraints.EAST;
            gbcbtnAbout.anchor=GridBagConstraints.SOUTHWEST;
            gbcbtnAbout.insets = new Insets(5, 5, 5, 5);
            gbcbtnAbout.gridx=1;
            gbcbtnAbout.gridy=4;
            container.add(btnAbout,gbcbtnAbout);

            menu=new JMenuBar();
            menuFile=new JMenu("File");
            submenuExit=new JMenuItem("Exit");
        
            setVisible(true);
            setTitle("Morse Code Converter V1.0");
            setSize(400,250);
            setResizable(false);
            setJMenuBar(menu);
            setIconImage(new ImageIcon("img/icon.ico").getImage());
            setDefaultCloseOperation(DO\_NOTHING\_ON\_CLOSE);
            
            menu.add(menuFile);
            menuFile.add(submenuExit);

            submenuExit.addActionListener(this);
            btnAbout.addActionListener(this);
            btnConvert1.addActionListener(this);
            textKata.addActionListener(this);
        }
                
            public void actionPerformed(ActionEvent e){
                panjangKata=textKata.getText().length();
                if(e.getSource()==btnConvert1 || e.getSource()==textKata){
                    huruf=textKata.getText().toLowerCase().toCharArray();
                    for(int i=0;i<panjangKata;i++){
                        switch(huruf\[i\]){
                            case 'a': morse\[i\]=".- /"; break;
                            case 'b': morse\[i\]="-... /"; break;
                            case 'c': morse\[i\]="-.-. /"; break;
                            case 'd': morse\[i\]="-.. /"; break;
                            case 'e': morse\[i\]=". /"; break;
                            case 'f': morse\[i\]="..-. /"; break;
                            case 'g': morse\[i\]="- -. /"; break;
                            case 'h': morse\[i\]=".... /"; break;
                            case 'i': morse\[i\]=".. /"; break;
                            case 'j': morse\[i\]=".- - - /"; break;
                            case 'k': morse\[i\]="-.- /"; break;
                            case 'l': morse\[i\]=".-.. /"; break;
                            case 'm': morse\[i\]="- - /"; break;
                            case 'n': morse\[i\]="-. /"; break;
                            case 'o': morse\[i\]="- - - /"; break;
                            case 'p': morse\[i\]=".- -. /"; break;
                            case 'q': morse\[i\]="- -.- /"; break;
                            case 'r': morse\[i\]=".-. /"; break;
                            case 's': morse\[i\]="... /"; break;
                            case 't': morse\[i\]="- /"; break;
                            case 'u': morse\[i\]="..- /"; break;
                            case 'v': morse\[i\]="...- /"; break;
                            case 'w': morse\[i\]=".- - /"; break;
                            case 'x': morse\[i\]="-..- /"; break;
                            case 'y': morse\[i\]="-.- - /"; break;
                            case 'z': morse\[i\]="- -.. /"; break;
                            case '.': morse\[i\]=".-.-.- /"; break;
                            case ',': morse\[i\]="- -..- - /"; break;
                            case ':': morse\[i\]="- - -... /"; break;
                            case '-': morse\[i\]="-....- /"; break;
                            case '/': morse\[i\]="-..-. /"; break;
                            case '1': morse\[i\]=".- - - - /"; break;
                            case '2': morse\[i\]="..- - - /"; break;
                            case '3': morse\[i\]="...- - /"; break;
                            case '4': morse\[i\]="....- /"; break;
                            case '5': morse\[i\]="..... /"; break;
                            case '6': morse\[i\]="-.... /"; break;
                            case '7': morse\[i\]="- -... /"; break;
                            case '8': morse\[i\]="- - -.. /"; break;
                            case '9': morse\[i\]="- - - -. /"; break;
                            case '0': morse\[i\]="- - - - - /"; break;
                                                        case ' ': morse\[i\]="%"; break;
                            default : morse\[i\]="  /";
                        }
                                                
                    }
                    
                    kataMorse=StringUtils.join(morse);
                    textMorse.setText(kataMorse);
                }
                if (e.getSource()==submenuExit) {
                    System.exit(0);
                }
                if (e.getSource()==btnAbout){
                    JOptionPane.showMessageDialog(null, "Morse Code Converter V1.0\\n\\nDeveloped by:\\nMuh Faiz Mughni #5354\\n\\n�Copyright 2012", "About", JOptionPane.PLAIN\_MESSAGE);
                }
            }
            
            public static void main(String\[\] args){
                JFrame.setDefaultLookAndFeelDecorated(true);
                Morse b=new Morse();
            }   
    }
```

Dadaa ..
