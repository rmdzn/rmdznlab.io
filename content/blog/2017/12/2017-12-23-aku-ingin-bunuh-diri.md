---
title: "\"Aku Ingin Bunuh Diri\""
date: "2017-12-23"
categories: 
  - "cerpen"
tags: 
  - "cerpen"
coverImage: "keluarga-siluet-rmdz.jpg"
aliases:
  - /post/2017-12-23-aku-ingin-bunuh-diri/
---

![keluarga](/blog/2017/12/images/keluarga-siluet-rmdz.jpg)

Di sebuah siang hari Sabtu, Oti, teman semasa SMP menghampiriku.

"Jet, aku punya _uneg-uneg_."

_Jet_ begitu dia memanggilku, sebuah plesetan dari huruf 'zet' yang ada pada nama kerenku.

"Kenapa, Ti, tumben?" Tanyaku sambil mengernyitkan dahi dan menyeruput teh hangat.

"Begini, aku pernah berpikir untuk 'bunuh diri'." Katanya dengan isyarat dua jari tangan naik turun pada kata _bunuh diri_. "Entah, apakah aku memang berpikir demikian. Aku takut mati dengan cara seperti orang bunuh diri pada umumnya. Aku takut loncat dari jembatan, aku takut menggantung diriku, aku takut menembak kepalaku sendiri dengan pistol .."

"_Ssst_, itu tak mungkin. Kau tak pernah dan tak mungkin memiliki pistol."

"_Iih_, jangan potong perkataanku, Jet. Itu hanya perumpamaan!"

"Oke, oke, baiklah."

"Aku lanjutkan. Aku takut menabrakkan diri ke mobil yang melaju kencang. Aku takut merobek perutku dengan pisau, dan cara klasik apapun yang mungkin terpikir olehmu. Aku merasa ingin bunuh diri dengan cara mati sekarang juga, entah ada pohon roboh menimpaku, tersambar petir, mobil yang tidak sengaja menabrakku, pesawat yang tiba-tiba jatuh di kamarku, dan .. entahlah."

Ini adalah _uneg-uneg_ serius. Aku kemudian melihat ke wajahnya setelah beberapa saat menunduk.

"Bagaimana, Jet? Apakah aku memang sedang berpikir _suicidal_?" tanyanya.

"Aku kaget kau bertanya seperti itu, Ti. Aku tak ingin menghakimi. Kita akan obrolkan nanti kenapa kau berpikiran seperti itu," jawabku meyakinkan temanku.

"Aku pun tak tahu pasti. Tetapi, dari keinginanmu yang berharap semua itu terjadi, bisa saja kau memang sedang berpikir untuk 'bunuh diri'. Banyak orang ingin mati dengan cara wajar, tapi justru kau berpikir ingin mati dengan cara seperti itu secepatnya. Benarkah kau tidak ingin mati dengan tanganmu sendiri? Dengan cara klasik yang kau sebutkan tadi?"

"Tidak sama sekali," tutur Oti setelah menggeleng berulang kali.

"Ada alasan selain 'takut'?" tanyaku.

"Sebenarnya tidak. Ada dua takut yang ku alami. Takut merasa sakit dan takut melakukan dosa besar."

"Itu yang membuatmu terlihat aneh, Ti." Tuturku dengan gaya berpikir ala detektif. "Kau tahu, aku bukan psikolog. Aku pun takut perkataan ngawurku akan mencederaimu."

"Aku mengerti, Jet," Oti menunduk lesu. " Aku hanya butuh teman curhat, setelah apa yang terjadi pada diriku."

"Siap ke psikolog? Siap telpon _hotline_ bunuh diri?" Ajakku.

"Ayuklah!"
