---
title: "Datang untuk Pergi"
date: "2018-03-13"
categories: 
  - "puisi"
tags:
  - "puisi"
aliases:
  - /post/2018-03-13-datang-untuk-pergi/
---

Manusia datang untuk pergi

Pergi untuk kembali

Bertemu untuk pamit

Berpetualang untuk merindu

Berdiri tegak untuk duduk

Bersusah untuk bahagia

Membentuk untuk merusak

Menyapa untuk memalingkan muka

Memuji untuk menghina

Membuka untuk menutup

Dan hidup untuk mati

~ Manusia (13/3/18)
