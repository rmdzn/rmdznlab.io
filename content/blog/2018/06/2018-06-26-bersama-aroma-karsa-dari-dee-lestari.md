---
title: "Bersama 'Aroma Karsa' dari Dee Lestari"
date: "2018-06-26"
categories: 
  - "buku"
tags: 
  - "buku"
coverImage: "aroma-karsa-rmdzn.jpg"
aliases:
  - /post/2018-06-26-bersama-aroma-karsa-dari-dee-lestari/
---

> Peringatan, mungkin ada bocoran (spoiler).

Pada sebuah malam, hampir mengakhiri bulan Ramadan, saya memilih untuk jalan-jalan ke toko buku. Ada satu buku yang sudah mengawang untuk saya beli, sehingga saya tak berencana berlama-lama di tempat menyenangkan tersebut.

_Aroma Karsa_. Buku yang ditulis oleh Dewi Lestari alias [Dee Lestari](http://deelestari.com) sudah menjadi target untuk saya beli beberapa pekan lalu. Saya bukan atau mungkin belum menjadi [adDEEction](https://twitter.com/addeection), tetapi saya merasa perlu menikmati novel ini. Entah karena apa …​

![Aroma Karsa](/blog/2018/06/images/aroma-karsa-rmdzn.jpg)

Sebagai _jujugan_, saya pergi ke Toga Mas di jalan Affandi (Gejayan), untuk mencari bonus sampul dan, siapa tahu, diskon. Namun sayang, setelah mengitari rak novel, saya tidak menemukan buku ini. Rasa penasaran menggelayuti untuk mencari judul novel melalui komputer pencarian, ternyata saat itu _Aroma Karsa_ sedang dalam proses pengiriman dari _supplier_ ke Toga Mas. Wah …​ wah …​ wah …​

Kemudian saya pergi ke Gramedia. Tak perlu wara-wiri, _Aroma Karsa_ sudah tersaji di rak novel lokal dan dia atas "meja" yang memamerkan buku-buku terkini (atau mungkin terpopuler). Pengalaman membeli buku yang tidak terlalu baik karena buku yang baru diterbitkan bulan Maret 2018 sudah terlihat "lecek", sampul plastik yang sudah robek di sana dan di situ.

Seratus halaman _Aroma Karsa_ saya nikmati langsung di Indomaret, sekitar UPN Veteran Yogyakarta dengan "memesan" satu meja beserta bangkunya di antara meja kursi yang sudah terisi penuh oleh pasangan _ngobrol_.

**AROMA KARSA**

Novel _Aroma Karsa_ menceritakan dua orang manusia yang memiliki indra penciuman yang luar biasa tajam tetapi memiliki nasib berbeda. Keduanya dipersiapkan untuk melakukan ekspedisi pencarian tanaman yang konon membuat seorang raja Majapahit terhapus dari sejarah. Konspirasi ekspedisi menjadi bumbu yang tak kalah menarik, begitu juga dengan ilmu mengenai aroma dan parfum yang asyik gila.

Novel ini membuat saya berusaha menghargai semua aroma yang masuk ke dalam hidung dan membuat saya berusaha membaui benda atau lingkungan sekitar dengan saksama. Bahasa yang dipakai oleh ibu Dee nyaman dan menyenangkan. Saya belum pernah menyimak novel lainnya, seperti serial Supernova, Perahu Kertas, Rectoverso, dll, sehingga saya belum bisa membandingkan _Aroma Karsa_ dengan novel-novel tersebut. Bahasa yang terkesan tidak manusiawi ketika ibu Dee menyebutkan nama ilmiah untuk menjabarkan aroma harum dan busuk …​ haha.

Saya memiliki kekaguman sendiri terhadap _Aroma Karsa_. Risetnya begitu mendalam, _cuy_! Saya membayangkan bagaimana ibu Dee berdiskusi dengan Ananda Mikola sambil berjalan di sirkuit balap dan _paddock_. Aroma apa yang menguar di sirkuit dan mobil rusak untuk dimasukkan ke dalam _scene_ Arya. Terbayang juga bagaimana ibu Dee melakukan riset di Bantar Gebang dan meminta izin kepada pak Nurdin untuk meminjam namanya sebagai salah satu nama tokoh novel.

Saya adalah pengagum cerita yang tidak mudah ditebak. _Aroma Karsa_ menjembatani hal tersebut. Beberapa plot terkesan mengarahkan saya untuk menduga titik tertentu, tetapi plot berikutnya justru membantah dugaan itu.

Merespon kemungkinan sekuel _Aroma Karsa_, saya tidak berharap banyak (daripada kecewa :p). Saya setuju bahwa cerita _Aroma Karsa_ sudah sesuai target yang diinginkan penulis. Kata ibu Dee, _Aroma Karsa_ sudah sesuai desain selengkung busur cerita yang ditutup pada sebuah titik dan titik tersebut akhir yang terbaik. Biarkan akhir menjadi misteri dan imajinasi tersendiri. Namun, jika ibu Dee memilih untuk melanjutkan cerita, saya pun tak ragu untuk menikmatinya.

**Terima kasih Dee Lestari!**

Banyak pencuit di Twitter yang berharap novel ini diadopsi ke layar lebar seperti karya-karya Dee Lestari sebelumnya. Namun, sebagai nonpenikmat film adopsi novel, saya tak begitu tertarik jika novel ini benar-benar akan ditayangkan di layar lebar. Biarkan cerita dan tokoh-tokohnya mengalun sebagai imajinasi tanpa batas di kepala ini ...
