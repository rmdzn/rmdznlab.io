---
title: "Gaung Asian Games 2018, dari Baliho sampai Wi-Fi Gratis"
date: "2018-07-04"
categories: 
  - "cuap"
tags:
  - "cuap"
aliases:
  - /post/2018-07-04-gaung-asian-games-2018-dari-baliho-sampai-wi-fi-gratis/
---

Pemerintah benar-benar serius mempromosikan [Asian Games](https://en.wikipedia.org/wiki/Asian_Games) 2018, _ya iya lah_, ini adalah kesempatan kedua Indonesia menjadi tuan rumah ajang olahraga akbar se-Asia setelah tahun 1962. Asian Games 2018 ini spesial karena akan diselenggarakan di dua kota sekaligus—​Jakarta dan Palembang—​berbeda dengan Asian Games sebelumnya yang hanya diadakan di satu kota, termasuk Asian Games 1962 yang hanya diadakan di Jakarta.

Beberapa bulan sebelumnya, gaung Asian Games 2018 sangat lemah, Eric Thohir, ketua INASGOC selaku penyelenggara acara [mengakui hal tersebut](https://sport.detik.com/sport-lain/4031793/erick-thohir-ungkap-sebab-promosi-asian-games-belum-maksimal). Namun semakin kesini, penyelenggara gencar menyembur informasi Asian Games 2018 ke penjuru Indonesia meskipun [terganggu dengan gelaran Piala Dunia](https://www.bola.com/pesta-bola-rusia/read/3500487/erick-thohir-piala-dunia-ganggu-promosi-asian-games-2018).

Bukan berlebihan saya menyebut proses promosi dengan cara "menyembur". Di Sleman, Yogyakarta (bukan kota, bukan pula pelosok) menjadi area promosi Asian Games 2018. Kantor Kementerian Pekerjaan Umum Direktorat Jenderal Sumber Daya Air di Sopalan Maguwoharjo menjadi tempat hingar bingar baliho, spanduk, dan umbul-umbul Asian Games 2018. Pertama lihat warna-warni ala Asian Games di situ, jujur, saya takjub, _wuuuuh …​ gak nyangka promosi sampai sini_.

![Baliho Asian Games 2018](/blog/2018/07/images/promosi-asian-games-2-rmdzn.jpg) Baliho Asian Games 2018 di Kementerian Pekerjaan Umum

Ramainya materi promosi gelaran tersebut juga terlihat di timur Balai Sabo, di kantor kementerian yang sama.

![Spanduk dan Baliho Asian Games 2018](/blog/2018/07/images/promosi-asian-games-1-rmdzn.jpg)

Itu mungkin biasa, di sekitar RS Hermina Yogya, materi promosi bukan berupa materi klasik seperti spanduk dan kawan-kawannya, tetapi Wi-Fi gratis! _Yep_, akses Wi-Fi itu dapat ditemukan pada _access point_ "AsianGames2018".

![Daftar Access Point Asian Games 2018](/blog/2018/07/images/screenshot_2018-07-02-20-33-49.png) Access Point Asian Games 2018

Setelah terkoneksi dengan "AsianGames2018", akan muncul halaman seperti gambar di bawah.

![Halaman login Asian Games 2018](/blog/2018/07/images/img-20180702-wa0000.jpg)

Saya salut dengan promosi yang dilakukan INASGOC dan pemerintah, menjalar hingga "pelosok" dan dalam bentuk yang tidak biasa. Saya sebenarnya penasaran untuk keliling ke kantor pemerintahan lain apakah materi promosi juga ada di situ. Bila sempat berkeliling, saya pastikan akan menyunting tulisan ini.
