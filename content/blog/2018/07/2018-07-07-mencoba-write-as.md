---
title: "Mencoba 'Write.as'"
date: "2018-07-07"
categories: 
  - "blog"
tags:
  - "blog"
coverImage: "home-office-rmdzn.jpg"
aliases:
  - /post/2018-07-07-mencoba-write-as/
---

![home office](/blog/2018/07/images/home-office-rmdzn.jpg)

Daftar harga di paragraf bawah sudah tidak akurat. Silakan cek langsung ke situs web [write.as](https://write.as).

> Artikel ini terbit pertama kali di [write.as/ramdziana/mencoba-write-as](https://write.as/ramdziana/mencoba-write-as) dengan sedikit suntingan.

Bertelusur di jagat per-blog-an adalah salah satu aktivitas favorit. Bercengkerama dengan platform arus utama adalah hal biasa, tetapi menemukan platform baru yang patut untuk dijajal adalah hal yang luar biasa.

**Write.as** merupakan platform blog yang belum lama ini saya temukan. Ia didirikan sekitar tahun 2015. Platformnya sungguh sederhana, semirip Telegraph-nya Telegram. [Kata Matt Baer](https://write.as/matt/who-is-this), Write.as adalah implementasi atas ketidakberdayaan dan ketidakbebasan penulis untuk tidak diintip privasinya. Di sini, calon penulis dibebaskan untuk menulis secara anonim maupun secara bernama (_login_ dahulu).

![Laman Write.as](/blog/2018/07/images/bcc01583-43e9-474d-a4c9-4e019298238c.jpg)

Penulisan anonim dan bernama, dan bentuk kolom editornya adalah fitur yang membuat Write.as mirip seperti [Telegraph](https://telegra.ph). Kalau dilihat secara keseluruhan, platform ini juga mirip seperti [Roon.io](https://roon.io) (sudah diakuisisi oleh Ghost) karena mementingkan layanan ketiga sebagai sarana berkomentar. Dalam kasus ini, layanan ketiga yang dimaksud seperti Twitter, Hacker News, dsb.

Write.as menggunakan [Markdown](https://daringfireball.net/projects/markdown/) sebagai pemformat teks. Markdown memang sangat populer. Selain dipakai Write.as, ia juga dipakai oleh Wordpress.com, Roon.io, Ghost, [Slvrback.com](https://silvrback.com), hingga platform SSG (Static Site Generator). Saya adalah penggemar berat Asciidoc(tor) sehingga kurang nyaman ketika berusaha menggunakan bahasa _markup_ karya John Gruber tersebut untuk menulis artikel ini.

## Kustomisasi dan paket berbayar

Secara bawaan, Write.as membuat URL untuk pengguna dengan format **write.as/namapengguna**. Format pilihan lain adalah **namapengguna.writeas.com**. Jika mempertahankan domain **write.as** dan **writeas.com** adalah hal cupu, tenang, mereka juga menyediakan dukungan domain kustom yang bisa diperoleh dengan berlangganan minimal paket Casual.

Write.as tersedia secara gratis dengan fitur yang terbatas. Versi gratis, blog hanya dapat diakses bila calon pengakses memiliki URL-nya atau diistilahkan dengan _Unlisted_. Dengan demikian, artikel atau blog kita, termasuk artikel ini yang dipublikasikan secara gratis, tidak dapat diramban oleh mesin pencari. Pilihan hasil publikasi lain seperti Private (hanya dapat dibaca saya sendiri ketika _login_), Password-protected (terlindungi kata sandi), dan Public harus berlangganan paket Pro. Untuk Public, paket Casual sudah membawa fitur tersebut. Di keterangan paket Public dituliskan bahwa blog kita akan terindeks oleh read.write.as. Saya kurang tahu pasti apakah itu berarti blog dan artikel kita juga akan teramban mesin pencari atau tidak. Tidak ada penjelasan implisit seputar ini.

Dengan Write.as gratis, format tampilan bawaannya hanyalah Blog (tanggal terbit terlihat, artikel terbaru tampil pertama kali). Saya suka ini karena Write.as mencitrakan diri sebagai platform blog, yang tentu format tampilan seperti itu umum digunakan oleh platform lainnya. Format tampilan lain yang hanya dapat diperoleh jika berlangganan paket Pro adalah format Novel (tidak ada tanggal terbit, artikel terlawas tampil pertama kali) dan format Notebook (ada tanggal terbit, artikel terbaru tampil pertama kali). "Tanggal terbit" yang dimaksud hanya keluar di laman utama blog (bukan artikel).

Versi gratis juga menyediakan fitur pengganti tema terang dan gelap.

Harga paket berbayar Write.as jauh lebih murah daripada platform blog lain, yakni paket Casual dengan harga 0,38 USD (harus dibayar pertahun) dan paket Pro dengan harga 4 USD/bulan atau 40 USD/tahun. Keuntungan paket berbayar bisa dilihat [di sini](https://write.as/pricing). Jika ingin menggunakan fitur statistik yang sangat detail, layanan penyimpan gambar snap.as (dibangun oleh pengembang Write.as), dan mengustom blog lebih dalam (mengubah CSS misalnya), disarankan untuk memilih paket Pro.

Hal menyebalkan ketika semua paket berlangganan tersebut harus dibayar dengan kartu kredit. Sungguh repot sekali jika tidak memiliki kartu ajaib tersebut. Pfffttt ...
