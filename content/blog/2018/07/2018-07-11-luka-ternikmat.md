---
title: "Luka Ternikmat"
date: "2018-07-11"
categories: 
  - "cerpen"
tags:
  - "cerpen"
coverImage: "luka-1-rmdzn.jpg"
aliases:
  - /post/2018-07-11-luka-ternikmat/
---

![luka](/blog/2018/07/images/luka-1-rmdzn.jpg)

Temanku sempat mengulur waktu ketika aku mengejar alasan mengapa dirinya terasa santai saat dilukai orang lain. Penafian, _luka_ yang dia rasakan bukanlah _luka romansa_. "Itu _cemen_," katanya dengan nada tegas saat ditanyai seputar kisah cinta pemuda-pemudi yang kerap dianggap sebagai penggalau hati. _Luka_\--setidaknya anggapanku—​yang ia dapat lebih pada pengkhianatan, kebohongan, peremehan, dan ketidakpedulian.

"Aku yakin, kau akan bercerita kepadaku meskipun terlihat seperti menghindar," tuturku menatap tajam temanku yang saat itu menunduk terlihat sibuk menyibakkan debu dari buku. "Bagaimana kau bisa menahan luka yang bagi banyak orang terasa mustahil untuk dilakukan?"

"Aku hidup secara normalnya manusia, tapi itu berubah saat aku melukai beberapa orang yang kukenal," sahut temanku masih sambil mengelap beberapa buku. "Aku tidak yakin apakah mereka memaafkanku atas tindakan salahku. Aku terlalu takut menghubungi mereka untuk meminta maaf. Kesalahan yang sangat besar!"

Aku menyimak perkataannya dengan saksama.

"Luka yang kau anggap menghampiriku saat dibohongi, dikhianati, diremehkan, dan diabaikan bukanlah luka. Rasa itu aku nikmati. Aku rasa itu adalah cara Tuhan menghukumku atas kesalahan yang kuperbuat sebelumnya. Eh …​ diam kamu."

Aku hampir berbicara tetapi kemudian menutup rapat mulutku.

"Walau begitu, aku tidak ingin dianggap murahan. Aku memilih siapa saja yang berhak melukaiku, dan aku nyaman dengannya. Dibandingkan harus beurusan erat dengan orang asing yang tak kuhendaki dan berpotensi melukaiku, aku memilih untuk menghindarinya. Sakitku spesial. Jangan paksa aku untuk mengubah perasaan ini. Aku tenang layaknya alunan lagu _Fourtwnty_," tegas temanku meski saat bercerita ia masih sibuk membersihkan buku-buku favoritnya.

"Layakkah aku dengan semua ini?" tandasnya. "Layak, titik."

Sebenarnya aku sudah sedikit membuka mulut untuk menyampaikan sesuatu. Namun, jari temanku yang mengacung dan mengisyaratkan diam membuatku kembali menutup mulut rapat-rapat.

Sore itu tidak ada rasa canggung. Kami mengobrolkan hal lain secara santai dan normal yang ditutup dengan perpisahan untuk melakukan aktivitas sama esok siang.

**Aku Tenang - Fourtwnty:**

\[soundcloud url="https://api.soundcloud.com/tracks/87810668" params="color=#ff5500&auto\_play=false&hide\_related=false&show\_comments=true&show\_user=true&show\_reposts=false&show\_teaser=true" width="100%" height="166" iframe="true" /\]
