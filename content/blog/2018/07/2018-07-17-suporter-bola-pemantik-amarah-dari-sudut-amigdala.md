---
title: "Suporter Bola, Pemantik Amarah dari Sudut Amigdala"
date: "2018-07-17"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "suporter-sepak-bola-rmdzn.jpg"
aliases:
  - /post/2018-07-17-suporter-bola-pemantik-amarah-dari-sudut-amigdala/
---

![suporter](/blog/2018/07/images/suporter-sepak-bola-rmdzn.jpg)

Setiap orang memiliki pemantik emosi. Rasa sedih, bahagia, dan marah yang meluap. Pemantik yang dimaksud merupakan sebuah kenangan di sudut amigdala yang jika diolah akan mengembalikan emosi seperti saat peristiwa pada kenangan terjadi.

Bertambahnya usia sekaligus "pengalaman", pemantik emosi bisa saja berubah. Perundungan yang saya alami semasa SD dan SMP adalah kenangan yang bisa saya pantik sebagai amarah. Namun, kini itu tidak lagi menjadi masalah besar karena sudah cukup lama saya tak melihat si perundung dan saya sudah menganggap mereka dewasa.

Pemantik amarah sudah luruh, tetapi sayang, peristiwa beberapa tahun silam kembali menjadi kenangan yang tersimpan di amigdala dan siap saya korek sewaktu-waktu. Gara-gara kenangan tersebut, saya mungkin bisa menjadi Hulk kapan saja saya mau.

Peristiwa itu terjadi pada tahun 2012 (sebetulnya saya sudah agak lupa, tapi …​ [_terima kasih mesin pencari!_](https://duckduckgo.com)). Dua klub sepak bola bertanding di Stadion Maguwoharjo; klub kandang dan klub tandang dari Jawa Tengah. Mereka rusuh. Kerusuhan tidak hanya terjadi di radius 0 sampai 1 km, tetapi juga melebar hingga jalan raya yang digunakan oleh suporter tandang saat perjalanan pulang.

Rumah saya dari jalan raya hanya berjarak sekitar 200-300 meter. Saat kerusuhan terjadi, beberapa warga berjaga di gapura kampung dan menutup tiga perempat jalan dengan bambu. Semua sadar bahwa suasana mencekam dan sadar jika para suporter rusuh ini diizinkan masuk kampung, mereka bisa merusak apapun di situ. Ini bukan isapan jempol belaka. Rumah tetangga, sekitar 200m utara rumah saya, tepat di samping Selokan Mataram bernasib buruk. Genteng mereka jebol akibat lemparan para perusuh. Sepanjang selokan ternyata cukup parah. Beberapa polisi berkejaran menghindari lemparan perusuh sambil mengusir mereka menjauh dari kampung. Bapak saya ada di sini, ikut mengusir suporter.

Karena sudah ditutup bambu, sepanjang jalan depan rumah relatif aman. Namun, tiba-tiba ada yang _nyelonong_ masuk menggunakan motor sambil memegang kayu. Seingat saya ia masuk ke perkampungan lewat belakang (memutar lewat jalan kampung). Yang pasti, dia adalah suporter klub bola tandang. Tepat di depan rumah saya, seberang jalan, dengan _pede_\-nya dia memukul-mukulkan kayu sambil berteriak,

"_Ndene, Su! Nek wani!_"

Dia berteriak kepada beberapa orang suporter tuan rumah yang dicegat oleh warga di gapura.

Saat itu amarah saya cukup tersulut, tetapi saya memilih untuk diam karena takut rumah _kenapa-kenapa_, apalagi ibu saya _panikan_ dan ketika itu memang saya sarankan masuk ke dalam rumah.

Jika saya mengingat memori itu lagi, saya menyesal tidak menjadi Hulk lalu menghantam orang itu sampai lahan kosong sebelah atau menjadi Thanos dan melenyapkan suporter perusuh dengan jentikan tangan.

Sejak dahulu saya tidak menyukai sepak bola dengan sikap netral, tetapi tidak dengan suporter. Saya tahu, mereka adalah oknum dan saya tahu juga bagaimana polah mereka saat bergerombol dan tersulut emosi hanya karena sebuah …​ olahraga sepak bola.

_Hei Amigdala! Redam reaksimu!_
