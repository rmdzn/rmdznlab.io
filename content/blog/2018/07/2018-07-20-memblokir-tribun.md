---
title: "Memblokir Tribun"
date: "2018-07-20"
categories: 
  - "tutorial"
tags:
  - "tutorial"
coverImage: "tanda-setop-pixabay-rmdzn.jpg"
aliases:
  - /post/2018-07-20-memblokir-tribun/
---

![stop](/blog/2018/07/images/tanda-setop-pixabay-rmdzn.jpg)

> Artikel ini hasil tulis ulang dari berkas README.adoc pada https://gitlab.com/rmdzn/blokirtribun dengan sedikit suntingan.

Blokirtribun adalah kumpulan domain laman utama Tribun dan saudara-saudaranya yang ditulis dalam format _hosts_ dan ditujukan untuk memblokir semua akses ke domain-domain tersebut melalui komputer kita. Berkas _hosts_ ini dapat diterapkan di sistem operasi GNU/Linux, Windows, maupun macOS (caranya bisa dilihat di bawah).

### Apa salah Tribun?

Media massa sudah seharusnya mencerdaskan. Menampilkan segala berita dan informasi sejernih-jernihnya kepada masyarakat dengan tetap menjunjung tinggi kode etik jurnalistik. Tribun cetak merupakan salah satu contoh baik mengenai hal tersebut, tetapi Tribun daring justru menjadi media yang berbeda 180 derajat hingga tak cukup layak disebut sebagai "produk jurnalisme".

Selama ini, Tribun daring hanya mengunggulkan konten fantastis nirfaedah dengan judul-judul berunsur umpan klik. Penerbitan konten daring yang sangat mudah dibandingkan konten cetak menjadi senjata pamungkas mereka untuk berprinsip _terbitkan dahulu, koreksi kemudian_, tak sesuai dengan praktik jurnalisme yang selama ini umum diketahui masyarakat bahwa media massa harus memverifikasi dan mengklarifikasi sumber berita dari manapun sebelum ia ditulis dan diterbitkan.

Beberapa contoh buruknya pemberitaan Tribun:

1. Kasus pemerkosaan remaja 16 tahun. Ref [@JennyJusuf](https://twitter.com/JennyJusuf/status/1017447519054204928)
2. Kasus bunuh diri Anthony Bourdain. Ref [@wisnu\_prasetya](https://twitter.com/wisnu_prasetya/status/1005317482633146368)
3. Asal menyebutkan sebuah akun Twitter pengulas film sebagai akun sutradara kenamaan. Ref [@jokoanwar](https://twitter.com/jokoanwar/status/1007896780334252032)
4. Asal berasumsi tentang pendapatan akun YouTube (ini tidak viral). Ref: [/r/indonesia](https://www.reddit.com/r/indonesia/comments/8x0cbq/media_kita_seringkali_berlebihan_dan_kurang_data/)
5. Menyebarkan hoaks terkait _mole people_. Ref: [Kaskus](https://www.kaskus.co.id/thread/5586265b529a4560178b4568/sekampret-inikah-media-kita). Mereka sudah menuliskan "berita klarifikasinya" [di sini](http://manado.tribunnews.com/2015/03/06/apa-sebenarnya-mole-people-benarkah-hanya-gelandangan-bawah-tanah), tetapi tak ada permintaan maaf.
6. Berita nirfaedah merusak akal sehat. Ref: [Tribunnews](http://www.tribunnews.com/seleb/2018/05/17/janeta-janet-introspeksi-diri-usai-ditabrak-jin-di-panggung)

Referensi lain dapat dibaca di artikel [_Bagaimana Tribunnews Membantu Terorisme?_](http://www.remotivi.or.id/pantau/466/Bagaimana-Tribunnews-Membantu-Terorisme?) oleh Muhamad Heychael dari Remotivi, artikel [_Soal Perdebatan antara Remotivi dengan Tribunnews_](https://kumparan.com/avicenna-raksa-santana/soal-perdebatan-antara-remotvi-dengan-tribunnews) oleh Avicenna Raksa Santana di Kumparan, dan artikel di Geotimes berjudul [_Puncak Kesadaran Digital Kompas-Gramedia Adalah Media Sekelas Tribunnews_](https://geotimes.co.id/kolom/tribunnews-kompas-gramedia/) oleh Iqbal Aji Daryono.

### Mengapa hanya fokus pada Tribun?

Diakui, hampir semua portal berita melakukan praktik umpan klik (lihat [@ClickUnbait](https://twitter.com/ClickUnbait)), tetapi Tribun ini luar biasa _nggilani_ (lihat daftar di atas). Tak hanya karena pemberitaannya, tanggapan [pemimpin redaksi Tribunnews](http://www.tribunnews.com/nasional/2018/05/23/analisa-yang-tendensius-kasar-dan-tidak-fair?page=all) atas artikel Remotivi penuh dengan saltik (sekarang sudah diperbaiki) dan terlalu membela diri, tanpa introspeksi.

### Mengapa menggunakan berkas 'hosts'?

Sebenarnya kita dapat memanfaatkan perkakas seperti _dnsmasq_ untuk langsung memblokir tribunnews.com beserta subdomain-nya. Sayangnya itu diperlukan teknis yang lebih detail, dengan berkas _hosts_ pengguna komputer kasual pun dapat langsung menerapkannya dengan sangat mudah.

## Cara menggunakan blokirtribun

Terlebih dahulu, unduh melalui [https://gitlab.com/rmdzn/blokirtribun/-/archive/master/blokirtribun-master.zip](https://gitlab.com/rmdzn/blokirtribun/-/archive/master/blokirtribun-master.zip). Lalu ekstrak.

### 1\. Windows

Klik kanan Notepad, pilih _Run as Administrator_. Salin (_copy_) isi _blokirtribun_ dan tempelkan (_paste_) ke berkas _C:\\Windows\\System32\\drivers\\etc\\hosts_.

### 2\. GNU/Linux

Salin tempelkan (_copas_) isi _blokirtribun_ ke _/etc/hosts_.

### 3\. macOS

Salin tempelkan (_copas_) isi _blokirtribun_ ke _/private/etc/hosts_.

Jika Tribun belum terblokir, Anda perlu memulai ulang koneksi internet (_disconnect_ lalu _connect_ lagi).

## Kontribusi

Ada domain Tribun yang belum saya masukkan? Berikan komentar di bawah ini atau kirim ke surel **ramdziana at protonmail dot com**.

## Demo ketika Tribun terblokir

[https://streamable.com/s/6aq8c/fertnc](https://streamable.com/s/6aq8c/fertnc)
