---
title: "Atas Pemberitaan Bandara NYIA, Media Lokal Memang Layak Disemprot"
date: "2018-07-25"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "koran-rmdzn.jpg"
aliases:
  - /post/2018-07-25-atas-pemberitaan-bandara-nyia-media-lokal-memang-layak-disemprot/
---

![koran](/blog/2018/07/images/koran-rmdzn.jpg)

Setiap kali melihat polah [Wahana Tri Tunggal](https://wahanatritunggal.wordpress.com/) di koran yang menolak pendirian Bandara NYIA (_New Yogyakarta International Airport_), saya merasa sebal. Beberapa kali membatin, "_kenapa_ __sih___, mereka tidak menerima pembangunan bandara? Ini kan demi kesejahteraan mereka. Dengan adanya bandara, Yogyakarta bakal semakin maju. Turis akan berdatangan lebih banyak. Industri pariwisata di Kulonprogo akan meningkat pesat, dsb._"

Koran tersebut sangat berhasil menancapkan argumen seperti itu di alam pikiran saya. Saya mungkin tidak sendiri, tiga jutaan penduduk DIY selain WTT pasti memiliki pikiran serupa.

Beberapa bulan ke belakang dan setelah WTT pecah, saya sadar. Ternyata saya keliru. Saya ternyata terlalu membabi buta mendukung proyek bandar udara baru tanpa mencoba bersimpati dengan korban penggusuran lahan warga Kulonprogo dan dampak lingkungan yang ditimbulkan.

Kamil Alfi Arifin, staf pengajar di Jurusan Ilmu Komunikasi UNRIYO yang juga aktif bergelut di pusham UII, hari Rabu (25/7) menerbitkan artikel menarik di Remotivi. Dia menyinggung pemberitaan berat sebelah yang selalu disampaikan oleh koran populer di DIY-Jateng, koran favorit yang selalu saya baca setiap hari. Setelah membaca artikel berjudul _[Bagaimanakah Media Lokal Memberitakan Pembangunan NYIA?](http://www.remotivi.or.id/pantau/477/Bagaimanakah-Media-Lokal-Memberitakan-Pembangunan-NYIA?)_ tersebut, saya paham ternyata selama ini koran itu memang selalu membahas sisi baik NYIA dan hanya sedikit (bahkan sangat sedikit) membahas dampak yang ditimbulkannya, yang tentu tidak kalah besar.

> Saya sangat sarankan Anda untuk membaca artikel [tersebut](http://www.remotivi.or.id/pantau/477/Bagaimanakah-Media-Lokal-Memberitakan-Pembangunan-NYIA?).

Semua berita hanya menyinggung kebenaran pemerintah, baik pemerintah pusat maupun pemerintah daerah, tanpa pernah menyampaikan dampak lingkungan yang ditimbulkan. Semua orang wajib taat tanpa perlu membantah. Bahkan untuk berkeluh kesah pun tak boleh karena pemegang kuasa berjanji akan memberikan ganti untung yang setimpal. _Yak_, semua alasan penolakan hanya dipersempit menjadi alasan ekonomi, tidak lebih, dan media memberitakan hal tersebut dengan gencar.

Bandara NYIA Kulonprogo sudah pasti dibangun. Lahan calon bandara sudah bebas dari para penolak yang selama ini mencoba bertahan hidup dari intimidasi ekskavator. Di masa mendatang, semoga media lokal dengan senang hati memberikan penjelasaan secara obyektif mengapa kuantitas air di sekitar Bandara NYIA terus menurun [seperti yang terjadi di Bali](https://vice.com/id_id/article/3k7j73/tak-banyak-pihak-sadar-bali-terancam-kehabisan-air-tanah-akibat-industri-pariwisata).

Membahas proyek NYIA, sambil mendengarkan lagu berjudul Apati dari Daramuda Project yang dinyanyikan oleh Rara Sekar.

https://www.youtube.com/watch?v=n1Zgy2mr6Kg
