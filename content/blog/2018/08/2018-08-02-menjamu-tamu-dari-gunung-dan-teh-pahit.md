---
title: "Menjamu Tamu dari 'Gunung' dan Teh Pahit"
date: "2018-08-02"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "teh-rmdzn.jpg"
aliases:
  - /post/2018-08-02-menjamu-tamu-dari-gunung-dan-teh-pahit/
---

![teh](/blog/2018/08/images/teh-rmdzn.jpg)

_Nyinom_ atau _laden_ atau kegiatan membawakan minuman dan makanan di acara pesta adalah sesuatu yang biasa dilakukan oleh pemuda/pemudi kampung. Di kampung yang saya tinggali (wilayah Sleman), "pekerjaan" yang memang sering dikaitkan dengan aktivitas pemuda/pemudi kampung ini biasa dilakukan hanya pada acara pesta pernikahan.

Berbeda dengan wilayah Gunungkidul, masyarakat di wilayah ini masih kental dengan tradisi pesta di samping pesta pernikahan (_walimatul 'ursy_). Salah satunya adalah pesta khitanan (_walimatul khitan_).

Tetangga saya, seorang pendatang berasal dari Gunungkidul yang sudah lama menetap di kampung tempat saya tinggal, hari ini (2/8) mengadakan pesta khitanan untuk anak bungsunya. Acara ini menerjunkan pemuda dari Gunungkidul dan berkolaborasi dengan pemuda lokal, salah satunya saya.

Pesta khitanan utama diadakan pada Kamis malam hari, setelah isya, yang diisi dengan pengajian dan mengundang warga sekampung. Siang hari sebelumnya, tetangga dari Gunungkidul datang secara rombongan. Empat bus. Pemuda lokal dan pemuda dari kabupaten sebelah beramai-ramai mempersiapkan hidangan untuk tamu yang baru datang.

Pemuda lokal sebenarnya diminta membantu pada pukul 10.00 WIB, setidaknya datang ke lokasi setengah jam sebelumnya. Namun, jauh sebelum pukul 09.30, pemuda dari Gunungkidul sudah mempersiapkan hidangan, lebih tepatnya cemilan, di atas tikar yang sudah digelar.

Nah, yang menarik di sini. Di kampung saya, pemilik hajatan hanya menyediakan teh manis dan air putih. Air putih adalah sajian opsional, untuk tamu yang meminta. Di pesta khitanan ini, pemilik hajatan menyediakan teh manis, teh pahit, dan air putih. Dua terakhir adalah opsional dan peminatnya memang cukup banyak. Penggemar teh pahit biasanya adalah tamu yang sudah sepuh.

Saya baru pertama kali menjamu tamu dari "gunung" apalagi di acara pesta khitanan yang baru pertama pula saya datang dan _nyinom_ di sini. "Teh pahit" adalah obyek yang saya anggap paling menarik. Selainnya adalah tradisi yang biasa dilakukan pada pesta pernikahan biasa di kampung saya.
