---
title: "Kemkominfo Sehatkan Internet Indonesia dengan Teknologi Kekinian, Selamat!"
date: "2018-08-10"
categories: 
  - "satir"
tags: 
  - "cuap"
coverImage: "sorry-closed-lnx.jpg"
aliases:
  - /post/2018-08-10-kemkominfo-sehatkan-internet-indonesia-dengan-teknologi-kekinian-selamat/
---

![tutup](/blog/2018/08/images/sorry-closed-lnx.jpg)

> SATIR!!!

Setiap hari, dengan modal ponsel pintar, komputer rumah, atau komputer jinjing, ditambah paket data yang bejibun bahkan tak terbatas masyarakat Indonesia selalu mengakses konten pornografi. Sejak pagi dini hari, siang, sore, hingga malam larut. Kalau disandingkan dengan aktivitas makan, mengakses pornografi layaknya minum. Harus. Karena akan membuat hidup gersang dan _seret_ jika tidak melakukannya.

Awal tahun 2016, Kementerian Komunikasi dan Informatika (Kemkominfo) [memblokir platform blog Tumblr](https://www.bbc.com/indonesia/berita_indonesia/2016/02/160217_indonesia_tumblr_diblokir). Berdasarkan riset, situs yang didirikan oleh David Karp tahun 2007 ini penuh dengan gambar maupun video pornografi. Hampir 89 konten Tumblr adalah pornografi, kata salah satu humas Kemkominfo. "Bahaya dong, ini sama saja Tumblr memiliki misi merusak generasi Indonesia," tegasnya.

Dua tahun sebelumnya, [Vimeo juga mulai diblokir](https://www.liputan6.com/tekno/read/2048474/situs-berbagi-video-vimeocom-diblokir). Kemkominfo yakin, Vimeo bukanlah situs untuk berbagi karya seperti yang didengungkan. Mereka menegaskan, Vimeo adalah situs untuk berbagi video tabu, termasuk pornografi. Memblokir Vimeo sama dengan menyelamatkan bangsa, ujar perwakilan Kemkominfo kepada awak media.

Selain keduanya, Reddit menjadi situs paling bersalah yang telah membagikan beragam konten pornografi. Meskipun banyak orang mengatakan bahwa Reddit merupakan situs sumber banyak ilmu, ia adalah situs porno. Titik.

Pemerintah sempat bingung, walaupun situs-situs di atas sudah diblokir, masyarakat tetap dapat menikmati konten pornografi. Mengapa? _Eng ing eng_ …​ ternyata mereka menyadari bahwa Google adalah sumber dari segala sumber pornografi. Sebagai mesin pencari unggulan, pemerintah tidak asal langsung memblokir Google. Dilansir dari kantor berita lokal ternama, "pemerintah masih menimbang patut tidaknya Google diblokir. Kami tidak ingin menghambat masyarakat untuk mendapatkan ilmu bermanfaat sebanyak satu persen dari Google. Google kan mesin pencari, bukan situs biasa. Iyaa kan yaa …​"

Oleh karena itu, guna membatasi akses pornografi di Google, Kemkominfo menggunakan teknologi terkini dari Google bernama SafeSearch. Teknologi SafeSearch merupakan teknologi yang baru saja dirilis sekitar tahun 2003. Selain itu, mereka juga menggunakan RestrictionMode pada YouTube untuk melakukan hal serupa. Teknologi yang sungguh sangat modern dan kekinian. Mulai beberapa hari lalu, secara otomatis, lingkungan internet di Indonesia sangat sehat. Setiap ISP yang beroperasi di Indonesia akan mengaktifkan SafeSearch di Google dan RestrictionMode di YouTube. Hampir tidak mungkin keduanya dinonaktifkan lewat aplikasi ponsel maupun web.

Pemerintah semakin serius menyehatkan internet demi menjamin masa depan generasi bangsa yang lebih baik dengan teknologi ciamik dan sangat kekinian.

Selamat sehat!
