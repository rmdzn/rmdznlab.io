---
title: "Ketika Deddy Corbuzier Lawan Dunia Gosip dan Isu Privasi yang Ada di Baliknya"
date: "2018-08-15"
categories: 
  - "privasi"
tags: 
  - "cuap"
  - "privasi"
coverImage: "media-sosial-lnx.jpg"
aliases:
  - /post/2018-08-15-ketika-deddy-corbuzier-lawan-dunia-gosip-dan-isu-privasi-yang-ada-di-baliknya/
---

![media sosial](/blog/2018/08/images/media-sosial-lnx.jpg)

_Semudah itukah nyomot foto buat bahan gosip layaknya nyolong sendal di masjid kampung?_

_Gaes_. ada peristiwa heboh. Suatu hari, Deddy Corbuzier berfoto dengan seorang wanita. Foto tersebut diunggah oleh si wanita di akun Instagram-nya yang saat itu dalam mode privat. Eh ... tiba-tiba, foto tersebut diketahui telah diunggah oleh akun _terselekedeng_ berinisial LT dan menjadi bahan gosip warganet budiman.

Konten akun dalam mode privat hanya dapat dilihat jika dan hanya jika diikuti terlebih dahulu. Dari fakta ini, Deddy dan timnya cukup menulusuri siapa saja pengikut akun si wanita yang berani-beraninya mencomot foto dan mengirimkannya ke admin LT. Dengan teknis _ciat-ciat_ ala kungfu (detailnya tidak tahu), tim Deddy berhasil mendapatkan akun tersebut bahkan hingga dapat membaca isi pesan pribadi si akun.

Tertangkaplah akun tersangka penyebar foto. Khas cara Deddy, pemilik akun tersebut diboyong ke kantor polisi tepatnya Polsek Medan Kota. Bukan untuk dikurung di dalam penjara, tetapi untuk "sekadar" mempertanggungjawabkan perbuatannya, terapi kejut, dan menjadi pelajaran untuk semuanya.

Dalam video yang diunggah oleh Deddy, mayoritas pengomentar mendukung aksi mantan mentalis ini. Namun, ada beberapa komentar menggelikan dan layak untuk disorot, yang menurut saya cukup membuat miris. Intinya adalah semua yang diunggah ke internet berhak dipakai oleh siapapun, untuk apapun.

Logika yang keliru.

Motor Joko diparkir di depan rumah. Semua orang bebas melihatnya, tapi apakah motor Joko bebas digunakan oleh orang lain? Tentu saja _tidak_ tanpa seizin pemiliknya. Meskipun nanti dikembalikan dengan keadaan sehat sentosa? Saya ulangi, _tidak boleh tanpa seizin pemiliknya (Joko)_. Siapapun Joko, figur publik atau bukan, pasti setuju akan hal itu.

Semua konten di internet seharusnya diperlakukan seperti itu, kecuali kalau konten tersebut memakai lisensi tertentu yang mengizinkan warganet menggunakannya sebebas-bebasnya tanpa izin.

Instagram adalah platform yang tak lepas dari analogi Joko dan motornya. Instagram adalah rumah Joko dan konten yang diunggah di sana secara publik adalah harta benda Joko yang diletakkan di depan atau luar rumah. Konten akun Instagram yang diatur dalam mode privat diumpamakan dengan perabot Joko yang dipajang di dalam rumah, sedangkan pengikut akun tersebut adalah tamu yang diperkenankan masuk ke dalam rumah.

Saya tak ingin mempersulit otak dengan membahas lisensi, pakai akal sehat saja lah, apakah foto yang ada di dalam ruang tamu Joko boleh kita ambil atau kita jepret tanpa izin lalu disebarkan begitu saja? Apalagi setelah foto diambil atau dijepret, kita sebarkan ke komunitas penggosip berdalih arisan di kampung?

_Eh, si Joko ternyata punya gebetan baru, nih bukti fotonya bla bla bla._

Tidak etis, tidak boleh, dan kampungan.

Sangat wajar ketika Deddy marah karena fotonya bersama si wanita yang diunggah di Instagram bermode privat lolos dan dipamerkan di akun gosip nan populer sejagat merah putih.

Dari sekian banyak komentar, tak sedikit pula yang menyalahkan si wanita karena telah mengunggah foto pribadinya ke akun Instagram. Coba kalau wanita itu tidak mengunggah fotonya, pasti fotonya tidak meluas tersebar, begitu inti komentarnya. Komentar tersebut mungkin ada benarnya. Namun, sayangnya, itu adalah _victim blaming_. Berpura-pura memberikan solusi dengan cara menyalahkan korban. Mirip ketika orang-orang menyalahkan korban perudapaksaan karena menggunakan baju yang katanya tidak pantas. Tak solutif.

Membahas kesalahan pengunggahan foto ke LT tak berarti membenarkan apa yang telah dilakukan Deddy Corbuzier bersama timnya. Pelanggaran privasi yang parah. Dalam video di YouTube, ayah Azkanio Corbuzier menyampaikan bahwa dirinya dapat membaca DM pengunggah foto. Melakukan apapun di akun pengunggah foto. Seram. Kalau saya diberi kesempatan duduk semeja dengan Deddy Corbuzier, saya akan sarankan kepadanya untuk sama sekali tidak menegaskan secara publik bahwa dia dan timnya berhasil membobol akun seseorang untuk menunjukkan orang tersebut salah. Jika itu benar, biarlah pengalaman tersebut disimpan secara pribadi, tidak untuk disebarkan. Mungkin niatnya untuk memberi peringatan kepada orang-orang yang mencoba berurusan dengan dia, tetapi perilaku tersebut seolah-olah mengafirmasi tindakan membobol akun seseorang adalah tindakan terpuji. Tidak sama sekali. Peretas populer, Jim Geovedi, pernah berseloroh dirinya sering diminta untuk meretas akun Facebook karena masalah konyol atau tidak jelas. Jangan biarkan manusia peminta-minta seperti itu menjamur. Lawan~~

Konsep privasi antara rumah Joko dan Instagram atau dunia internet pada umumnya memang berbeda. Fisik dan virtual. Jangan hanya karena Instagram adalah dunia virtual, etika dan akal sehat kita hilang. Bung, di balik kevirtualan akun seperti Instagram ada manusia asli yang memiliki hati. Camkan itu.

Wahai warganet budiman penggagas dan pemuja penyinyiran, diamlah sejenak. Nikmati hidupmu. Fokuslah pada golmu. Tak perlu meributkan hidup pribadi orang lain.

Video pernyataan Deddy Corbuzier: https://www.youtube.com/watch?v=bFg1Yu31Gu8
