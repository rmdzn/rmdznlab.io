---
title: "Si Pembakar Lahan yang Bodoh"
date: "2018-08-17"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "lahan-terbakar-rmdzn.jpg"
aliases:
  - /post/2018-08-17-si-pembakar-lahan-yang-bodoh/
---

Sepertinya orang-orang masih perlu diajar, membakar sampah di sekitar lahan kering adalah tindakan terbodoh di era ini dan mungkin sejak era munculnya ras manusia.

Sore di hari Kamis (16/8), seorang warga tergopoh-gopoh mendatangi rumah saya dan bermaksud mencari bapak saya yang saat itu sedang menikmati acara TV. Dia melaporkan bahwa lahan kosong di timur rumahnya terbakar.

Karena laporan itu, jalan menjadi ramai. Warga berbondong mencari tahu tentang apa yang terjadi. Bapak-bapak dan pemuda yang saat itu bergerombol di gardu setelah mempersiapkan peralatan lomba 17-an satu demi satu menuju lokasi kebakaran lahan.

Lihat: [Video kebakaran lahan (sumber: Fendi)](https://gfycat.com/CourteousThriftyAfricanfisheagle)

Lahan tepat di pojok perempatan jalan atau di tenggara RS Hermina Yogya itu kurang lebih seluas masjid kampus UGM. Bagian tengah kering sedangkan bagian pinggirnya semak masih cukup basah, hijau. Saat saya datang ke lokasi, api menjilat cukup tinggi dan luas. Beberapa warga sudah mulai mematikan api, menggebyur jilatan api dengan berember-ember air. Kebetulan, lahan itu dekat dengan sungai kecil atau _kalen_. Menariknya, pohon meranggas di utara lahan, yang sebenarnya dipisahkan oleh sawah, ikut terbakar. Merembet dari pucuk sampai bawah.

Berdasarkan keterangan pelapor, api bersumber dari seorang warga beda kampung yang membakar sampah di lahan bagian timur. Dia mengawasi kebakaran yang saat itu belum begitu melebar sekitar pukul 15.30 sampai 16.00 WIB sebelum kemudian datang ke rumah untuk menyampaikannya kepada warga lain. Narasumber lain mengungkapkan, bakar sampah dilakukan sejak siang hari.

Api dan titik-titik tanah yang masih berasap berhasil dimatikan sekitar waktu Magrib, kecuali pada titik api pertama (bakar sampah). _Bre_ …​ saya akui, benar-benar sulit memastikan tanah atau dahan pohon tidak berasap lagi. Saya memerlukan 15 sampai 25 ember hitam kecil (ember tukang) untuk mematikan kepulan asap dari batang pohon kering yang sudah menjadi arang.

Setelah salat Magrib, saya sempatkan memfoto titik api pertama dan pohon meranggas yang terbakar.

![Lahan terbakar](/blog/2018/08/images/lahan-terbakar-rmdzn.jpg) Titik api pertama (dok pribadi)

![Sisa pohon terbakar](/blog/2018/08/images/lahan-terbakar-pohon-rmdzn.jpg) Sisa pohon terbakar (dok pribadi)

\[**Moral cerita**\] Jangan bakar sampah sembarang, terutama di sekitar lahan kering yang berpotensi terembet api. Ini musim kemarau, Bung!
