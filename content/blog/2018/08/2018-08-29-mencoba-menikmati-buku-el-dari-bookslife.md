---
title: "Mencoba Menikmati Buku-el dari Bookslife"
date: "2018-08-29"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "aroma-karsa-rmdzn.jpg"
aliases:
  - /post/2018-08-29-mencoba-menikmati-buku-el-dari-bookslife/
---

Ibu Dewi Lestari alias Dee Lestari menelurkan karya lagi. Masih berhubungan dengan sang [_Aroma Karsa_](/blog/2018/06/2018-06-26-bersama-aroma-karsa-dari-dee-lestari/), buku anyar berjudul _Di Balik Tirai_ menyingkap cerita di balik layar penulisan buku yang saya sebut pertama.

_Di Balik Tirai_ mengikuti cara penerbitan novel _Aroma Karsa_ pada awalnya yakni secara digital melalui [Bookslife](https://www.bookslife.co/product-detail/Di-Balik-Tirai). Dari _sriwing-sriwing_ pernyataan bu Dewi, buku _Di Balik Tirai_ hanya akan tersedia secara digital, berbeda dengan _Aroma Karsa_ yang akhirnya dirilis secara fisik.

Saya terkagum-kagum dengan novel _Aroma Karsa_ ([lihat ulasan saya di sini](/blog/2018/06/2018-06-26-bersama-aroma-karsa-dari-dee-lestari/)) sehingga saya memutuskan untuk menjawab segala pertanyaan _wow, gimana riset bu Dee untuk menuliskan pengalaman Jati dan Suma?_ dengan membeli buku-el (buku elektronik) _Di Balik Tirai_ dari Bookslife.

!["Di Balik Tirai" per bagian di Bookslife](/blog/2018/08/images/b25e49f7-0d20-463d-af58-aebc71825414.jpg) "Di Balik Tirai" di Bookslife

Pendistribusian buku dari Bookslife memanfaatkan berkas PDC, berkas dokmen PDF yang dikunci dengan sistem untuk menghindari penyalinan ilegal. Tipe berkas PDC yang juga bisa disebut Lizard Safeguard Secure PDF ini dikembangkan oleh LockLizard. Tahu _dong_, kalau berkas PDF bisa disalin dan dibaca dengan bebas? Nah, si PDC berfungsi untuk menghindari "kebiasaan" tersebut. Berkas dengan format PDC hanya dapat dibuka oleh pengguna tertentu yang sudah terotorisasi, dalam kasus Bookslife berarti pengguna yang sudah membeli buku.

Berkas PDC hanya dapat dibuka di Windows, macOS, iOS, dan Android menggunakan PDCViewer. Di desktop, saya menggunakan GNU/Linux, platform yang tidak didukung LockLizard, sehingga mau tak mau saya menggunakan ponsel ber-Andraid untuk menikmati buku elektronik dari Bookslife.

[PDCViewer](https://play.google.com/store/apps/details?id=com.locklizard.pdcviewer) di Android nilainya jelek, hanya 2.3 dari 5.0. Namun saya masih merasa positif toh beberapa kali saya menjajal aplikasi bernilai rendah di Android masih aman, berjalan normal.

Mengunduh dan memverifikasi kunci otorisasi sukses saya lakukan dengan PDCViewer, tapi sialnya saat dipakai untuk membuka berkas PDC, aplikasi langsung macet (_crash_). Keluar tanpa sebab saat proses pemuatan aplikasi. Setelah bu Dewi menerbitkan bagian 1 dari _Di Balik Tirai_ dengan ukuran dokumen 2+ MB, PDCViewer akhirnya dapat membuka berkas PDC dengan sangat lancar. Saya berasumsi, kesalahan terjadi pada dokumen pertama, mungkin proses unduh yang menyebabkan ia rusak.

Untuk saat ini saya akan menikmati _Di Balik Tirai_, tunggu ulasan saya di artikel selanjutnya. _Bay bay!_
