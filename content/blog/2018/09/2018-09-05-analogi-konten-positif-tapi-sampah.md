---
title: "Analogi Konten Positif tapi Sampah"
date: "2018-09-05"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "sampah-rmdzn.jpg"
aliases:
  - /post/2018-09-05-analogi-konten-positif-tapi-sampah/
---

![sampah](/blog/2018/09/images/sampah-rmdzn.jpg)

Alkisah, di sebuah grup WhatsApp kampung yang sepi terdapat salah satu anggota yang rajin "meramaikannya" dengan konten indah, puitis, dan bermanfaat. Grup tersebut sebenarnya tak begitu sepi, ia terkadang ramai di kala ada acara tertentu. Namun, seorang anggota yang terlampau rajin itu menganggap grup selalu sepi. "Yuk, perlu diramaikan!," mungkin begitu katanya.

Dalam satu menit, seorang anggota grup yang akan saya sebut sebagai "Bapak X" membagikan beragam tautan, entah dari media daring terpercaya hingga blog abal-abal. Konten yang dibagikan pun beragam; jualan, berita, masalah politik, tips ini itu, lowongan pekerjaan, dan artikel lainnya yang dianggap positif. Oiya, bahkan, Bapak X sempat membagikan kabar lowongan CPNS yang disinyalir hoaks.

Tak sedikit anggota grup lain yang tidak menyukai cara Bapak X berbagi apapun di grup. Meskipun ada pula anggota grup lain yang membela si Bapak X, "Selama tulisan yang dibagikan positif, tidak apa-apa, kan?"

Terlepas dari kabar hoaks yang beliau bagikan, sebenarnya, apakah benar perilaku si Bapak X ini?

Begini bapak ibu sekalian. Anggap saja, grup WA tersebut adalah sebuah rumah dengan 10 anggota keluarga. Rumah tersebut sepi, tenang, dengan aggota keluarga lain yang sibuk dengan aktivitasnya masing-masing, kecuali Bapak X. Beliau tiba-tiba berteriak, "Halo! Ini kabar saham IHSG!". Beberapa menit kemudian, "Halo! Begini cara memelihara ponsel Android!". Tak selang beberapa lama, beliau mendobrak pintu sambil berteriak, "Hei! Ada lowongan pekerjaan nih dari perusahaan kecap!"

Isi teriakan Bapak X ini positif. Tentu positif. Namun, apakah kesembilan anggota keluarga membutuhkan semua info tersebut? Tidak! Bapak X justru sangat mengganggu. Kalau dalam istilah media sosial atau media daring, isi teriakan Bapak X ini SPAM alias SAMPAH.

Mungkin memang mudah menghapus sesuatu yang dibagikan oleh Bapak X dari WhatsApp. Tinggal pencet _chat_ yang dimaksud, lalu klik _hapus_. Gampang, kan?

Argumen "tinggal menghapus chat" itu bagaikan argumen "tinggal lupakan saja kan teriakan Bapak X di dalam rumah?". Gampang? Iya gampang, tapi itu SANGAT MENGGANGGU. Saya tak memerlukan satu atau bahkan semua info yang disampaikan oleh Bapak X, itu SPAM alias SAMPAH buat saya.

Kalau pakai akal sehat, Bapak X ini cocok bergabung di grup BERBAGI INFO, bukan grup YANG FOKUS PADA TEMA TERTENTU. Dalam kaitan grup WA kampung, tema grup tersebut adalah GRUP DISKUSI SUATU KAMPUNG; grup untuk membahas acara di kampung, menjenguk yang sakit di kampung, membahas rapat acara tertentu di kampung, dsb.

Kepada Bapak X terima kasih telah berbagi. Namun, alangkah baiknya istirahat untuk berbagi apapun di grup khusus ini. Grup ini bukan koran, bukan pula majalah dinding.

\[**Catatan**\] Analogi konten POSITIF tapi SAMPAH seperti di atas dapat diterapkan di grup daring manapun (tidak hanya WhatsApp).
