---
title: "Kebersihan Pasca \"Mancing Gerrsama\" di Selokan Mataram, Siapa yang Bertanggung Jawab?"
date: "2018-09-11"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "mancing-gerrsama-spanduk-rmdzn.jpeg"
aliases:
  - /post/2018-09-11-kebersihan-pasca-mancing-gerrsama-di-selokan-mataram-siapa-yang-bertanggung-jawab/
---

_Dua hari pasca Mancing Gerrsama, lingkungan selokan terlihat menjijikkan. Siapa penanggung jawabnya? Mana tahu?_

Bulan Agustus, tersebar rumor bahwa beberapa hari ke depan akan diadakan acara memancing bersama di sepanjang Selokan Mataram. Sekitar akhir bulan yang sama dan awal September terkonfirmasi, acara yang disebut sebagai _Mancing Gratis Gerrsama_ ini diadakan oleh relawan salah satu pasangan calon presiden dan calon wakil presiden periode 2019-2024 dan akan diadakan pada hari Minggu, 9 September untuk memperingati Amanat 5 September 1945 yang dikeluarkan oleh Sri Sultan Hamengkubuwono IX dan KGPAA Paku Alam VII. Acara diadakan di 14 titik dari Desa Banyurejo, Tempel sampai Maguwoharjo, Depok.

Sedikit kilas balik. Zaman dahulu, Yogyakarta tepatnya lahan luas di antara Sungai Progo dan Opak (Bantul, Sleman, dan Kota Yogyakarta) adalah wilayah yang gersang, kering, sebanding dengan hasil pertanian yang tak memuaskan. Untuk membasahi lahan sawah, masyarakat hanya mengandalkan air hujan karena sungai Code, sungai Winongo, dan sungai Gajah Wong yang mengalir di wilayah kerajaan ini tidak dapat diandalkan.

Pada tahun 1942, di masa pendudukan Jepang, Sri Sultan Hamengkubuwono IX berinisiatif membangun saluran air yang dapat dimanfaatkan masyarakat Yogyakarta untuk mengairi sawah. Selain untuk mengairi sawah, pembangunan Selokan Mataram ini juga untuk menyelamatkan masyarakat Yogyakarta dari "kewajiban" Romusha yang diterapkan sang penjajah. Dari pada mengikuti program Romusha yang berisiko mati sia-sia, lebih baik membuat Selokan Mataram yang di masa mendatang akan menguntungkan masyarakat Yogyakarta sendiri beserta turunannya.

Selokan Mataram dibangun dari Sungai Progo sampai Sungai Opak dengan jarak sekitar 30 km. Tidak tahu pasti bagaimana bentuk awal Selokan Mataram, yang pasti saluran ini memiliki [hulu yang terlihat _wah_ dan dijadikan tempat wisata](http://yogyakarta.panduanwisata.id/daerah-istimewa-yogyakarta/kulon-progo/ancol-bligo-menilik-hulu-selokan-mataram/) serta [hilir yang begitu sempit layaknya got pinggir jalan](https://www.menggapaiangkasa.com/2014/05/nama-selokan-mataramsepertinya-memang_15.html).

![Hulu Selokan Mataram di Ancol](/blog/2018/09/images/selokan-mataram-hulu-rmdzn.jpg) Hulu Selokan Mataram ([menggapaiangkasa.com](https://www.menggapaiangkasa.com/2014/05/nama-selokan-mataramsepertinya-memang_15.html))

![Hilir Selokan Mataram di Kulonprogo](/blog/2018/09/images/selokan-mataram-hilir-rmdzn.jpg) Hilir Selokan Mataram ([menggapaiangkasa.com](https://www.menggapaiangkasa.com/2014/05/nama-selokan-mataramsepertinya-memang_15.html))

Kata Rejomulia (Relawan Jokowi-Ma’ruf Amin untuk Kemuliaan Indonesia) selaku penyelenggara, acara ini diadakan untuk memperingati Amanat 5 September 1945, amanat yang disampaikan oleh Sultan dengan isi: Kasultanan Yogyakarta dan Paku Alaman menjadi bagian dari Republik Indonesia. Lalu mengapa memilih lokasi Selokan Mataram sebagai lokasi acaranya? Penyelenggara bermaksud [menghargai Selokan Mataram](https://kabarkota.com/mancing-geerrsama-di-selokan-mataram-seperti-ini-situasinya/) karena merupakan hasil karya Sri Sultan Hamengkubuwono IX yang menjadi lumbung pengairan bagi pertanian rakyat di Yogyakarta.

![Spanduk Mancing Gratis Gerrsama](/blog/2018/09/images/mancing-gerrsama-spanduk-rmdzn.jpeg)

Seperti yang saya sampaikan tadi. Lokasi pemancingan dilakukan di 14 titik. Saya tidak ikut memancing tetapi saya pergi ke titik terakhir di Maguwoharjo, barat Ringroad Timur, dua hari setelah itu. Tetangga yang ikut memancing bercerita, hari H selokan ternyata ditutup agar ikan tak berlarian ke timur Ringroad. Para pemancing sudah datang sejak pagi dan acara "resmi" ditutup sekitar pukul 17.00. Penutup selokan pun saat itu dibuka.

![Para pemancing di pinggir Selokan Mataram](/blog/2018/09/images/mancing-gerrsama-h-plus-2-rmdzn.jpg) Suasana H+2 Mancing Gerrsama di titik terakhir

Ternyata hari H sore hingga H+2 sebagian orang masih hobi memancing di sana, bahkan hingga malam hari! Bambu yang dibentuk sedemikian rupa sebagai penutup selokan ternyata dipasang lagi.

Saat ini Selokan Mataram memang sudah tak lagi menjadi pengair sawah dan justru lebih populer sebagai "tempat pembuangan sampah dan limbah", tetapi ternyata acara memancing bersama ini justru kontradiktif dari istilah "menghargai" dan justru menyuburkan status buruk terkini yang disandang oleh sang selokan. Sekitar pukul 10.00 hari Selasa saya bersepeda ke titik itu, sampah yang dibuang ke selokan menggumpal di situ. Menjijikkan. Sampah terdiri dari buangan sampah lawas yang mengalir dari barat dan mungkin sampah baru para pemancing.

![Sampah di acara Mancing Gerrsama](/blog/2018/09/images/mancing-gerrsama-selokan-1-lnx.jpg) Sampah pasca Mancing Gerrsama dan gambar penutup selokan dari bambu

![Sampah di Selokan Mataram](/blog/2018/09/images/mancing-gerrsama-selokan-2-lnx.jpg) Sampah pasca Mancing Gerrsama yang menggumpal

![Jumlah sampah saat Selokan Mataram ditutup sementara](/blog/2018/09/images/mancing-gerrsama-selokan-3-lnx.jpg) Sampah pasca Mancing Gerrsama yang menggumpal di Selokan Mataram

Penyelenggara seharusnya tidak lepas tanggung jawab. Mereka masih terikat dengan kebersihan lingkungan tempat acara diadakan. Ingat pak buk, orang-orang yang datang setelah Minggu, 9 September untuk memancing di situ yakin bahwa beberapa lele yang disebar masih berenang gembira di dasar selokan. Tentu karena lele yang menyebar itu adalah Anda, sang penyelenggara. Jangan cari gampangnya dengan cukup membuka penutup selokan dan membiarkan gumpalan sampah mengapung jauh. Kalau masih berpikir cara gampang seperti itu, memperingati Amanat 5 September 1945 lebih baik kalau diisi dengan acara bersih-bersih Selokan Mataram. Lebih berfaedah.
