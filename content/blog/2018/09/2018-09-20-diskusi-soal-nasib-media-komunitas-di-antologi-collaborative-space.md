---
title: "Diskusi Soal Nasib Media Komunitas di Antologi Collaborative Space"
date: "2018-09-20"
categories: 
  - "media"
tags:
  - "cuap"
coverImage: "20180919_144028-rmdzn.jpg"
aliases:
  - /post/2018-09-20-diskusi-soal-nasib-media-komunitas-di-antologi-collaborative-space/
---

_Sebelumnya, terima kasih kepada [Remotivi](http://remotivi.or.id/). Beberapa bulan terakhir, saya tertarik dengan dunia media, dan Remotivi adalah salah satu "tokoh" yang membuat kegemasan saya terhadap dunia media arus utama konsisten sampai sekarang._

Terima kasih juga kepada mereka atas twit yang mereka tulis pada 17 September bahwa Combine Resource Institution mengadakan acara diskusi publik dengan tema _Ketika Jurnalis Dipidana: Bagaimana Masa Depan Komunitas?_ di Antologi Space Collaborative. _Di Caturtunggal nih_, batin saya. Meskipun saya tertarik dengan media, bukan berarti saya adalah anak Pers yang selalu mengikuti akun-akun yang berhubungan dengan media dan jurnalisme. _Mumpung dekat rumah, dateng nih!_

![Poster acara diskusi publik](/blog/2018/09/images/poster-rmdzn.jpg)

Sebelum berangkat ke lokasi saya menghubungi sang narahubung untuk mendaftar hanya empat jam sebelum acara dimulai. Sebagai info, seperti yang tertera pada poster, diskusi publik direncanakan mulai pukul 14.00 dan selesai pukul 17.00 WIB.

_Capcuus …​_

Niatan awal saya mau bersepeda, tapi karena saya genjot _pedal_ dari rumah sudah sekitar pukul 13.30 dan yakin tidak akan sampai tepat pukul 14.00, sudah sekitar 400 meter jalan, saya balik ganti dengan motor.

Dengan motor, perjalanan dari rumah sekitar 15 sampai 20 menit. Lokasi _workspace_ Antologi Space Collaborative berada di sekitar utara Fakultas Kehutanan UGM dan Selokan Mataram. Masuk ke dalam kampung, cukup tersembunyi. Saya sampai di tempat pas pukul 14.00. Belum begitu banyak orang di tempat diskusi publik akan berlangsung, sepintas tak sampai 15 orang (sudah ditambah jumlah panitia dari Combine RI yang sibuk menyiapkan alat). Setelah menuliskan nama di formulir kehadiran, saya duduk dan sempat berkenalan dan _ngobrol_ super-pendek dengan Maheng, yang selanjutnya saya tahu bahwa dia adalah salah satu narasumber diskusi.

![Persiapan acara diskusi publik](/blog/2018/09/images/20180919_135411-rmdzn.jpg) Persiapan diskusi publik

Diskusi publik mengundang Roy Thaniago dari Remotivi, Aris Mulyawan dari AJI Semarang, Ferdhi F. Putra dari Combine RI, dan Rahmat Ali (Maheng) dari Perhimpunan Pers Mahasiswa Indonesia. Idha Saraswati dari Combine RI sebagai moderator.

> _Combine RI akan menulis dokumentasi tertulis mengenai acara di combine.or.id. Catatan yang saya tulis di sini adalah sisa-sisa ingatan yang mengintip dan yang dikeruk._
> 
> (23/9) Laporan Combine RI sudah terbit di [tautan ini](https://www.combine.or.id/article/Jurnalisme-sebagai-Hak-dan-Kewajiban-bagi-Warga__%23__238).

![Diskusi publik Combine RI](/blog/2018/09/images/20180919_144028-rmdzn.jpg) Dari kiri; Ferdhi F. Putra, Aris Mulyawan, Roy Thaniago, Rahmat Ali, dan Idha Saraswati

Tema _Ketika Jurnalis Dipidana: Bagaimana Masa Depan Komunitas?_ diambil karena kasus kriminalisasi yang dialami oleh Zakki Amali, jurnalis serat.id, oleh Universitas Negeri Semarang (Unnes). Serat.id sendiri merupakan media rintisan yang dibentuk oleh AJI (Aliansi Jurnalis Independen) Semarang.

Aris Mulyawan yang saat ini menjadi advokat sang jurnalis serat.id menjelaskan bahwa Zakki Amali membongkar dugaan plagiasi penelitian yang dilakukan oleh rektor Unnes. Dia menyebutkan langkah-langkah yang dilakukan oleh Zakki Amali saat mencari kebenaran dari dugaan tersebut, dari mencari obrolan yang menyinggung plagiasi melalui media sosial, mengonfirmasi ke pihak penulis yang diplagiasi dan Dikti, memastikan kelayakan berita, sampai mengklarifikasi langsung ke kampus Unnes (ditolak dan dilempar ke pihak humas kampus), sebelum tulisan diterbitkan. Hei! Langkah-langkahnya benar-benar jurnalis banget. Sayang beribu sayang, Zakki dilaporkan dengan UU karet atau UU ITE (UU no. 11 tahun 2008) oleh kampus, bukan dengan UU Pers. Bagi pihak pelapor, banyak alasan mengapa Zakki patut dipenjara karena tulisannya. Zakki Amali tidak memiliki sertifikat untuk disebut wartawan dan serat.id sebagai ruang tulisan bukanlah entitas berbadan hukum. Alasan kedua terbukti karena Tirto.id, media yang sudah berbadan hukum, yang memberitakan hal sama aman dari kriminalisasi.

Kasus Zakki Amali menambah panjang daftar kriminalisasi para jurnalis. Yang menarik, UU ITE umum dipakai untuk menjerat status di medsos, kasus jurnalis serat.id ini menjadi kasus pertama di Semarang yang muncul bukan karena status medsos tetapi tulisan berita.

Rahmat Ali menjadi pembicara berikutnya. Ia menceritakan bagaimana perjuangan berat yang dialami mahasiswa ketika mengorek informasi dan menerbitkannya sebagai berita. Ada saja pihak elit kampus yang menghalangi, membredel, mengintimidasi anggota Pers Mahasiswa, dan mengaburkan isu yang diangkat oleh mahasiswa sehingga warga kampus melupakan substansi liputan yang diterbitkan. Peremehan juga tak jarang dialami oleh mahasiswa, seperti saat mereka ingin meliput proyek di Kulonprogo dan meliput dana kampus yang digunakan untuk acara tertentu. Pers Mahasiswa dianggap tidak layak untuk meliput dan mencari informasi keuangan kampus. "Kalian bukan KPK," tiru Maheng.

Dari penelitian PPMI (Perhimpunan Pers Mahasiswa Indonesia) yang dirilis tahun 2017, 80% Pers Mahasiswa mengalami kekerasan (intimidasi, peremehan, dll).

Roy Thaniago yang saat ini menjabat Direktur Remotivi dan dosen luar biasa di UMN mengangkat ide tentang _journalism as citizenship_ menggantikan _journalism for citizenship_. Ide segar yang masih perlu didiskusikan tersebut menonjolkan jurnalisme sebagai hak dan kewajiban masyarakat sehingga tak perlu ada peraturan boleh tidaknya masyarakat meliput sesuatu sebagai produk jurnalisme. Hal itu diharapkan menjadi kekuatan dari media komunitas. Menguatkan pernyataan Roy, Ferdhi F. Putra menceritakan bagaimana media komunitas sangat berperan penting di tengah masyarakat. Media komunitas di Lombok, pasca gempa, berhasil mengumpulkan data tentang anak-anak korban gempa yang memerlukan bantuan penyembuhan trauma. Ferdhi yang sempat dikontak oleh media komunitas tersebut menyampaikan bahwa ia tidak memiliki kenalan orang-orang yang bergelut di dunia anak (penyembuhan trauma). Walau merasa kesulitan mendapatkan bantuan, mereka tak melulu mengharapkannya. Media komunitas itu kemudian bertindak sendiri menjadi penyembuh trauma anak dan didokumentasikan dalam bentuk video. Di waktu yang berbeda, mereka menyurvei kebutuhan korban gempa dan ditemukan bahwa rumah hunian sementara adalah bantuan paling kritis yang korban butuhkan. Setelah mengumpulkan donasi sekitar 500 ribuan, mereka bergerak membelikan _gedhek_ (dinding dari anyaman bambu) dan komponen lain untuk dijadikan hunian sementara untuk beberapa warga.

Media komunitas sangat penting meskipun mungkin isinya bagi kita secara umum tidak penting, jelas Ferdhi. "Misalnya berita tentang rapat Idul Adha, Maulid Nabi, …​"

Media komunitas yang merupakan implementasi dari jurnalisme warga bukanlah hal baru, Roy menyampaikan sebelumnya, zaman Orde Lama adalah hal umum jika seorang pedagang dan petani menuliskan berita untuk dikirim ke media. Orang-orang nonwartawan itu menulis untuk mengabarkan peristiwa yang ada dilingkungannya atau untuk menonjolkan dukungan kekirian maupun kekananan mereka. Zaman sekarang, jurnalisme warga yang dimonopoli oleh korporasi hanya merupakan keinginan untuk berpartisipasi itu sendiri. Maksudnya, pengirim warta tidak lagi bekerja untuk mengabarkan sesuatu ke khalayak, tetapi untuk berpartisipasi mengirim karya jurnalisme warga. Roy juga menyinggung praktik jurnalisme warga yang disediakan oleh saluran-saluran televisi.

Media komunitas sebenarnya sudah mendapatkan kepastian hukum dan jaminan dari segala tuntutan jika sudah menerapkan kode etik jurnalistik. Namun nyatanya selama ini mereka dibayang-bayangi oleh UU ITE yang memakan banyak korban itu. Catatan penting dari diskusi publik, masalah ini bukan hanya masalah media komunitas saja tetapi masalah kita semua dengan kebebasan ekspresi yang kita miliki.

Acara diakhiri setelah pembagian kenang-kenangan kepada narasumber dan penanya sekitar pukul 17.10. Saya pun langsung beranjak pulang diiringi mendung dan rintik serta sorotan lampu motor yang sibuk menyalip sesama.

**Pembaruan (31/10)**: Roy Thaniago menuliskan konsep detail _"journalism as citizenship"_  di [combine.or.id](https://www.combine.or.id/article/Melihat-Jurnalisme-Sebagai-Praktik-Kewargaan-Sebuah-Tawaran__%23__242).
