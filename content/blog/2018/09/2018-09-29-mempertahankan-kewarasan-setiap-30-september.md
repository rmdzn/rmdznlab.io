---
title: "Mempertahankan Kewarasan Setiap 30 September"
date: "2018-09-29"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "pengkhianatan-g30spki-f-rmdzn.jpg"
aliases:
  - /post/2018-09-29-mempertahankan-kewarasan-setiap-30-september/
---

> September ceria katanya, di awal mungkin, di akhir hampir mustahil, anak-anak merindu bapaknya telah keji dibunuh, anak-anak lain mengenang keluarganya mati dan hilang jadi korban pembalasan ~ **enam lima setiap tahun** oleh ramdziana

Bulan ini bulan September, sejarah kelam negeri ini tidak mungkin begitu saja dilupakan. Dalam satu malam, 30 September sampai 1 Oktober 1965, tujuh jenderal beserta beberapa orang lainnya dibunuh dengan cara "kejam". Mereka adalah Letjen Ahmad Yani, Mayjen Raden Suprapto, Mayjen Mas Tirtodarmo Haryono, Mayjen Siswondo Parman, Brigjen Donald Isaac Panjaitan, Brigjen Sutoyo Siswomiharjo, Bripka Karel Satsuit Tubun, Kolonel Katamso Darmokusumo, Letkol Sugiyono Mangunwiyoto, dan ajudan Jenderal Abdul Haris Nasution, Lettu Pierre Andreas Tendean. Jenazah mereka ditemukan di Lubang Buaya tanggal 3 Oktober.

Menurut sejarah populer yang digaungkan sejak Orde Baru itu, peristiwa didalangi oleh Partai Komunis Indonesia (PKI) yang ingin mengudeta pemerintah. Selanjutnya gerakan itu terkenal dengan sebutan G30S/PKI.

Sembilan belas tahun kemudian, PPFN (Pusat Produksi Film Negara) meluncurkan film berjudul _Pengkhianatan G30S/PKI_. Film ini disutradai oleh Arifin C. Noor, produser dan sutradara kondang kala itu. Film yang masuk dalam genre drama dokumenter ini menceritakan dengan dramatis peristiwa penculikan dan pembunuhan jenderal. Di tahun yang sama, 1984, film yang diadaptasi dari buku tersebut masuk tujuh nominasi dan menang di kategori _Best Screenplay_ di ajang Indonesian Film Festival. Tahun 1985, ia kembali menang di ajang yang sama di kategori _Best-Selling Film_.

![Poster Film 'Pengkhianatan G30S/PKI'](/blog/2018/09/images/pengkhianatan-g30spki-f-rmdzn.jpg) Poster Film 'Pengkhianatan G30S/PKI'

Sejak dirilis ke publik, _Pengkhianatan G30S/PKI_ menjadi film wajib dipertontonkan kepada khalayak setiap tanggal 30 September di TVRI dan di televisi swasta. Survei yang dilakukan oleh majalah Tempo pada tahun 2000 menemukan, 97 persen dari 1.101 pelajar telah menonton film itu, 87 persennya justru sudah menonton lebih dari satu kali. Tak kaget jika Sen dan Hill pada tahun 2006 menyebut _Pengkhianatan G30S/PKI_ sebagai film Indonesia terlaris sepanjang masa.

Film yang penuh kekejaman dan _gore_, terutama saat adegan penyiksaan para jenderal oleh PKI dan adegan pengangkatan jenazah dari Lubang Buaya. PKI di film itu digambarkan sebagai orang-orang yang buas begitu juga dengan organisasi Pemuda Rakyat, dan organisasi perempuannya, Gerwani, sebaliknya para jenderal digambarkan sebagai orang yang berwibawa, kalem, dan memiliki keluarga harmonis.

Empat bulan pasca runtuhnya rezim Suharto, September 1998, Menteri Penerangan Yunus Yosfiah menyatakan bahwa pemerintah tidak akan lagi menayangkan _Pengkhianatan G30S/PKI_. Film itu dituduh telah memanipulasi sejarah dan menciptakan kultus terhadap Suharto. Semua stasiun TV setuju untuk tidak lagi menayangkan film yang menelan biaya 800 juta rupiah tersebut hingga tahun 2017 kemarin, tokoh-tokoh dan beberapa stasiun televisi kembali rindu untuk menonton dan menayangkan kembali.

Stasiun TV yang mulai tahun kemarin menayangkan film itu di antaranya [TVOne](https://kumparan.com/@kumparannews/tvone-akan-tayangkan-film-g30s-pki-pada-29-september) dan [TVRI](https://twitter.com/TVRINasional/status/913713747025313793). Jelas, keduanya akan menayangkan kembali tahun ini. Instansi-instansi juga mulai rajin melakukan _nonton bareng_ termasuk TNI dan sekolah.

Seperti yang dilansir oleh [Vice](https://www.vice.com/id_id/article/j5gba7/review-pengkhianatan-g30spki-dari-orang-yang-belum-pernah-nonton-filmnya-seumur-hidup) dari Antara, Seno Gumira Ajidarma, rektor IKJ menyampaikan, "_Pengkhianatan G30S/PKI_ menarik untuk dipelajari sebagai kasus saja, bukan untuk dinikmati, apalagi untuk mencari fakta sejarah." Sejarahwan yakin, film itu jauh dari fakta sejarah dan hanya berniat melakukan propaganda saja. Referensi yang sangat layak untuk Anda baca: [_Membedah Film 'Pengkhianatan G30S/PKI'_ oleh Tirto](https://tirto.id/membedah-film-039pengkhianatan-g30s-pki039-cxxh), [_Menilik Ulang Propaganda dalam 'PENGKHIANATAN G30S/PKI'_ oleh Remotivi](http://www.remotivi.or.id/kupas/418/Menilik-Ulang-Propaganda-dalam-Pengkhianatan-G30S/PKI), dan [_Film Pengkhianatan G30S/PKI dan Fakta Sejarah_ oleh Historia](https://historia.id/film/articles/film-pengkhianatan-g30s-pki-dan-fakta-sejarah-P1BqW).

Sebagai anak milenial, film itu sudah tidak relevan. Kalaupun Anda sudah pernah melihat film itu, anggap saja ia hiburan semata, jangan anggap segala kejadian yang ada di film sebagai fakta sejarah. Orang tua saya yang sejak zaman Orde Baru selalu menontonnya, sampai hari ini masih mengganggap bahwa semua kejadian di situ benar adanya. Saya senang dengan pernyataan _Editor at large Tirto_ [beberapa waktu lalu](https://mobile.twitter.com/zenrs/status/1045553066009751552), "Problem pemutaran film G30SPKI di frekuensi pubik adl warga dijejali pengetahuan lawas berusia 50 tahun, padahal pengetahuan dan riset2 terbaru terus bermunculan — dan sayangnya yg mutakhir2 itu terus diblokir dan tak diberi kesempatan setara utk diuji secara terbuka."

Film _Jagal_ (_The Act of Killing_) dan _Senyap_ (_The Look of Silence_) adalah salah satu pengetahuan anyar yang membahas G30S/PKI, bukan membahas gerakan PKI-nya, tetapi membahas akibat dari tuduhan yang dilemparkan kepada PKI oleh orang atas. Ratusan ribu orang dituduh sebagai PKI dan dibantai tanpa pandang bulu.

![Poster Film 'Jagal' atau 'The Act of Killing'](/blog/2018/09/images/the-act-of-killing-jagal-poster-f-rmdzn.jpg) Poster Film 'Jagal' atau 'The Act of Killing'

_Jagal_ disutradarai oleh Joshua Oppenheimer dan dirilis pada tahun 2012. _Jagal_ menceritakan aktor-aktor pembantai "PKI" dan bagaimana mereka membunuh korbannya. _Senyap_ dirilis dua tahun kemudian. Ia menggambarkan sudut pandang dari penyintas keluarga korban pembantaian.

![Poster Film 'Senyap' atau 'The Look of Silence'](/blog/2018/09/images/the-look-of-silence-senyap-rmdzn.jpg) Poster Film 'Senyap' atau 'The Look of Silence'

Tidak kalah dari film propaganda yang saya sebut di awal artikel, film dokumenter _Jagal_ memperoleh penghargaan sebagai Film Dokumenter Terbaik di European Film Award dan Asia Pasific Screen Award pada tahun 2013 serta menjadi nominasi Academy Awards sebagai _Best Documentary Feature_. Di penghargaan BAFTA _Jagal_ menjadi film terbaik. Film _Senyap_ juga mendapatkan penghargaan di Busan International Film Festival, Starz Denver Film Festival, FIPRESCI, dll. Sayangnya, film yang menampilkan sudut pandang lain dari film yang diputar selama puluhan tahun ditolak oleh Indonesia (baca: pemerintah). Segala acara nonton bareng kedua film selalu dibubarkan, kalaupun ada yang berhasil, itu dapat dihitung dengan jari.

Meskipun ada [sisi etis-tidak-etis yang dikritik pada kedua film (khususnya _Senyap_)](https://cinemapoetica.com/bisakah-senyap-dipercaya/), _Jagal_ dan _Senyap_ dapat menjadi media untuk mempertahankan kewarasan kita dari cokokan puluhan tahun film _Pengkhianatan G30S/PKI_ yang kita lihat sebelumnya.

Sila cari _Jagal_ dan _Senyap_ di YouTube. Ia diunggah di kanal resmi _Jagal Senyap_.
