---
title: "Cara Baca Buku dari Bookslife dan Kesannya"
date: "2018-10-08"
categories: 
  - "tutorial"
tags:
  - "tutorial"
coverImage: "buku-dari-bookslife-rmdzn.jpg"
aliases:
  - /post/2018-10-08-cara-baca-buku-dari-bookslife-dan-kesannya/
---

![buku dari bookslife](/blog/2018/10/images/buku-dari-bookslife-rmdzn.jpg)

**_Update_** **(9/8/2020):**

_Untuk membaca buku dari Bookslife tidak lagi serumit cara pada tulisan ini. Kamu hanya perlu mengunduh aplikasi Bookslife di [Play Store (Android)](https://play.google.com/store/apps/details?id=com.bookslife.androidviewer). Buku yang sudah dibeli akan muncul di tab Rak Buku_.

Selesai sudah saya membaca buku digital legal nongratis pertama. Sebelum mengulas mengenai buku tersebut secara khusus, perkenankan saya menulis bagaimana cara mendapatkan dan membaca buku dari Bookslife—​platform penjualan buku digital—​serta kesan terhadapnya.

> Aplikasi Bookslife sudah diperbarui. Panduan di artikel ini mungkin sudah tidak berlaku. Kalau ada kesempatan pakai Bookslife lagi, panduan akan saya perbarui.

[Bookslife](https://Bookslife.co) merupakan "toko" tempat saya membeli buku digital _Di Balik Tirai Aroma Karsa_. Saya sempat menganggap Bookslife sebagai toko buku daring pada umumnya sebelum kemudian berganti dengan menganggapnya sebagai ruang penerbitan buku mandiri seperti NulisBuku. Bahkan saya belum juga _ngeh_ dengan _apa itu Bookslife?_ saat Aroma Karsa terbit sebagai cerita bersambung di platform itu (waktu itu saya memang belum berminat membaca buku _Aroma Karsa_). Seiring waktu, saat saya memiliki keinginan untuk membeli buku _Di Balik Tirai Aroma Karsa_, saya mulai menjamah lebih dalam tentang Bookslife.

Jadi ternyata begini, Bookslife itu platform penerbitan buku digital. Jika NulisBuku mengizinkan siapapun menulis dan menerbitkannya secara mandiri dalam bentuk fisik, Bookslife menerbitkannya dalam bentuk digital. Berbeda dari [NulisBuku](http://nulisbuku.com/faq) yang menyebut diri sebagai _selfpublishing_ sehingga sang penerbit adalah penulis itu sendiri, [Bookslife](https://www.bookslife.co/page/52621956-4865-4408-8555-c555e01a96a8/FAQ) bertindak sepenuhnya sebagai penerbit. Beberapa penulis populer sudah menerbitkan buku di Bookslife, termasuk Dewi Lestari, Asma Nadia, dll.

## 1\. Mendaftar di Bookslife

Tak perlu berlama-lama, untuk membeli buku di platform tersebut klik tombol _SIGN UP_ di pojok kanan atas. Akun Bookslife hanya memerlukan alamat surel dan kata sandi. Jika tidak ingin menggunakan surel, kita juga bisa membuat akun dengan akun Facebook atau Twitter.

![Kotak pendaftaran Bookslife](/blog/2018/10/images/fee1f72f-ef08-4813-9ffd-2d7d5666a6df.jpg) Kotak SIGN UP

Untuk mengaktivasi akun (terutama jika mendaftar menggunakan surel), periksa surel dengan subyek _Activation Account @Bookslife_ lalu klik _AKTIVASI_.

![Aktivasi Bookslife pada pesan surel](/blog/2018/10/images/aktivasi-link-rmdzn.jpg) Tautan aktivasi Bookslife

Ketika mendaftar menggunakan Facebook dan Twitter, Anda tak perlu direpotkan dengan urusan aktivasi.

Silakan klik _LOG IN_ di pojok kanan atas jika akun sudah teraktivasi.

![Kolom masuk akun Bookslife](/blog/2018/10/images/d8f9a573-c33b-48bf-b6e9-ef674eb2a149.jpg) Kolom SIGN IN

## 2\. Berseluncur mencari buku

Bookslife membagi 15 kategori buku; _Romance_, _Bookscamp_, Kumpulan Cerpen, _Contemporary Novel_, Puisi, _Cooking and Parenting_, Religi, _Comedy_, _Fantasy_, _Horror Thriller_, _Comic and Graphic Novel_, Buku Anak, _Comic Science_, _Science Fiction_, serta _Lifestyle_.

Di halaman muka [www.bookslife.co](https://www.bookslife.co), platform ini juga membagi kumpulan buku dengan Fiksi, Non Fiksi, Komik, _Editor’s Choice_, _Best Seller_, dan _Reader’s Choice_.

Saya tak begitu cocok pengategorian dengan bahasa campuran; Indonesia dan Inggris. Alangkah asyik jika Bookslife menampilkan kategori dalam bahasa Indonesia saja, dengan pilihan bahasa Inggris jika ingin mengganti semua kategori (dan navigasi) dalam bahasa tersebut.

## 3\. Top up wallet

Untuk membeli buku, saya kira dapat membayar langsung lewat transfer bank, ternyata tidak. Bookslife menerapkan _wallet_ untuk melakukan pembayaran dan ia dapat diisi (_top up_) melalui kartu kredit atau transfer bank (kartu debit). Untuk memeriksa isi _wallet_, klik nama akun pada pojok kanan atas lalu klik _My Wallet_.

![Menu akun Bookslife](/blog/2018/10/images/04d63986-7e44-435f-8d6d-b86767345d97.jpg) Menu akun Bookslife

![Halaman wallet Bookslife](/blog/2018/10/images/my-wallet-bookslife-rmdzn.jpg) Tampilan My Wallet

Di halaman _My Wallet_, klik _TOP UP_ untuk mengisi _wallet_ lalu isikan nominal _top up_. Minimal _top up_ adalah Rp10.000 dengan tambahan biaya administrasi yang berbeda baik ketika mengisi dengan transfer bank maupun kartu kredit.

## 4\. Membeli buku

Misal, saya ingin membeli buku [Simulakra Sepakbola oleh Zen RS](https://www.bookslife.co/product-detail/Simulakra-Sepakbola-(Zen-RS)). Kunjungi tautan [tersebut](https://www.bookslife.co/product-detail/Simulakra-Sepakbola-(Zen-RS)) kemudian gulirkan laman ke bawah, sorot _part_ buku yang ingin di beli (beberapa buku sudah ada yang meluncurkan versi lengkap—​bukan _part_\--dengan keterangan _Full Version_ seperti buku [_Di Balik Tirai Aroma Karsa_](https://www.bookslife.co/product-detail/Di-Balik-Tirai)), klik _ADD TO CART_ kemudian _PROCEED TO CHECKOUT_. Pilih _continue to shopping_ (tautan di bawah _PROCEED TO CHECKOUT_ jika masih ingin memilih buku lain untuk dibeli).

![Daftar buku yang disorot di Bookslife](/blog/2018/10/images/sorot-buku-bookslife-rmdzn.jpg) Menyorot buku di Bookslife

![Membayar buku di Bookslife](/blog/2018/10/images/32c8ec8c-e87f-4cb4-887c-561aced4e8db.jpg) Siap membayar buku di Bookslife

Untuk melihat keranjang belanja, cek [_My Cart_](https://www.bookslife.co/my-cart) lewat nama akun di pojok kanan atas.

**Buku sudah terbeli, kini saatnya kita membaca buku!**

Bookslife menerapkan teknologi dari [Locklizard](https://www.locklizard.com/) untuk melindungi hak cipta penulis. Buku dari Bookslife berformat PDC—​PDF yang dilindungi dengan enkripsi, kontrol DRM (_Digital Rights Management_), serta dukungan perlisensian untuk mencegah berkas dibuka, disalin, dibagikan, dimodifikasi, dicetak, dan ditangkap layar (_screenshot_) secara ilegal. Hanya orang-orang tertentu yang dapat membuka suatu berkas PDC, dalam kasus Bookslife berarti orang yang telah membeli buku.

Berkas PDC yang diunduh ke dalam perangkat ponsel atau komputer untuk dibaca tidak berbentuk fisik sehingga tidak mungkin ditelusuri secara manual melalui manajer berkas (_file manager_) serta dibuka dengan penampil dokumen PDF. Berkas itu hanya bisa diakses menggunakan aplikasi [PDCViewer](https://www.locklizard.com/download_pdf_viewers/) yang tersedia untuk Windows, macOS, iOS, maupun Android.

## Membaca buku dari Bookslife

### 1\. Instal PDCViewer di Android

PDCViewer dapat dicari melalu [_play store_](https://play.google.com/store/apps/details?id=com.locklizard.pdcviewer) dengan kata kunci "pdcviewer" atau "locklizard". Aplikasi bernama _Locklizard Safeguard Viewer_ itu terlihat meragukan karena hanya mendapatkan nilai dua bintang, tetapi percayalah, untuk membuka buku dari Bookslife, aplikasi ini berjalan mulus luar biasa.

![Aplikasi PDCViewer di Play Store](/blog/2018/10/images/screenshot_2018-10-08-13-24-31.png) PDCViewer di Play Store

### 2\. Aktifkan lisensi

Sebelum membaca buku, aktifkan terlebih dahulu lisensi yang Anda peroleh saat membeli buku dari Bookslife. Lisensi itu dapat diunduh melalui surel dengan subyek _Viewing Locklizard Protected PDF Files_. Cek keterangan semacam,

Your license

To view documents from BOOKSLIFE you must activate your license:

Ramdziana License

_Ramdziana License_ (sesuaikan dengan nama penguna Bookslife Anda) adalah tautan menuju berkas lisensi berformat LLV. Silakan unduh berkas tersebut melalui perangkat lain; ponsel lain, komputer lain, dsb, yang penting jangan mengunduhnya melalui ponsel yang akan dipakai untuk mengaktifkan lisensi dan membaca buku.

Sudah diunduh? Di ponsel Android, buka aplikasi PDCViewer lalu tekan tombol kanan atas bergambar sinyal WiFi dengan anak panah ke atas.

![Aplikasi PDCViewer di ponsel yang sudah terinstal](/post/2018/10/images/screenshot_2018-08-27-12-20-50.png) PDCViewer (tombol sinyal WiFi dengan anak panah ke atas di pojok kanan)

Setelah itu akan muncul dialog seperti tangkapan layar di bawah ini.

![Kotak dialog pengiriman dokumen PDC dan lisensinya ke Android](/blog/2018/10/images/screenshot_2018-08-27-12-08-01.png) Fitur PDCViewer untuk mengirim dokumen PDC dan lisensi

Lewat browser di ponsel atau komputer lain tempat mengunduh berkas LLV tadi, silakan ketikan alamat URL berbentuk IP seperti di atas: **192.168.1.9:8080**.

> Pastikan jaringan antara perangkat yang dipakai untuk membuka PDCViewer dan mengunduh berkas LLV sama (entah satu akses WiFi atau dengan _tethering_).

Unggah berkas LLV pada kolom _License_ kemudian klik _Submit_.

![Kotak unggahan berkas](/blog/2018/10/images/e7e66ef8-f2b9-4b22-af3d-0f718a9444af.jpg) Tampilan URL 192.168.1.x:8080 ketika dibuka

Lihat pada PDCViewer, bila berkas LLV sudah masuk, klik tombol _Finish Wi-Fi Upload_. Untuk mengaktivasi lisensi, cukup tap berkas LLV-nya. Mudah bukan?

### 3\. Mengunduh dan membaca buku

Masuk ke akun Bookslife, klik nama pengguna di pojok kanan atas, kemudian klik _My Library_. Pilih buku yang ingin diunduh kemudian klik _Download_. Oiya, mengunduh berkas buku harus lewat ponsel atau komputer lain.

Bila sudah terunduh, kirim berkas buku (PDC) mirip seperti cara nomor 2 di atas. Bedanya, alih-alih lewat kolom _License_ Anda harus mengirimkan berkas PDC melalui kolom _Document_. Jika sudah, klik _Submit_, lalu tutup dengan tombol _Finish Wi-Fi Upload_.

Silakan _tap_ berkas PDC jika tak sabar ingin membaca buku. Pertama kali membuka berkas tersebut, ponsel Anda harus dalam keadaan daring karena PDCViewer akan memverifikasi lisensi dengan dokumen yang dibuka. Setelah itu, Anda dapat membaca dalam keadaan luring, dimanapun, kapanpun.

![Buku dari Bookslife](/blog/2018/10/images/buku-dari-bookslife-rmdzn.jpg) Buku dari Bookslife (Di Balik Tirai Aroma Karsa)

## Kesan

Proses membaca buku Bookslife mungkin terlihat rumit, tetapi sebenarnya tidak bagi yang terbiasa membaca instruksi dan manual atau panduan. Repotnya, perlu dua ponsel untuk mengunduh PDC atau LLV dari Bookslife untuk kemudian dikirim ke aplikasi PDCViewer. Saya mencoba hanya dengan satu ponsel, setelah memencet tombol unggah (logo sinyal _WiFi_ dengan tombol panah ke atas pada PDCViewer) lalu saja jalankan ia di latar belakang untuk membuka peramban, fitur untuk mengirim berkas otomatis menutup.

_Hotline_/CS Bookslife cukup responsif terutama di Twitter. Saat menanyakan masalah pada _My Library_ lewat [@BookslifeCo](https://mobile.twitter.com/catatanrmdzn/status/1035361551522312192), masalah tersebut selesai 20 menit setelah respon terhadap twit saya dikirim oleh mereka.

Ingin membaca buku lagi dari Bookslife?

Bisa iya, bisa tidak. Tidak, karena saya bukan tipikal pembaca buku digital. Saya lebih senang menatap lama buku fisik daripada menatap layar ponsel, pun saya lebih senang mengeluarkan buku di pojok ruang pribadi atau publik dibanding mengeluarkan ponsel. Ya, karena apabila ada buku menarik yang memang sangat saya ingin baca dan itu tersedia di Bookslife.
