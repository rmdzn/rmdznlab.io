---
title: "Belajar Interaksi Beretika dengan Penyandang Disabilitas"
date: "2018-10-16"
categories: 
  - "cuap"
tags:
  - "cuap"
aliases:
  - /post/2018-10-16-belajar-interaksi-beretika-dengan-penyandang-disabilitas/
---

Selesai sudah gelaran olahraga Asian Para Games (APG) 2018 di Jakarta per tanggal 13 Oktober 2018 kemarin. Asian Para Games 2018 merupakan acara internasional se-Asia yang membuat kita sadar bahwa penyandang disabilitas bukanlah seseorang yang berpangku tangan dan tidak bisa berbuat apa-apa. Faktanya, mereka justru mampu berprestasi, menenggerkan Indonesia di peringkat ke lima menyalip Uzbekistan di detik-detik terakhir dengan perolehan medali 37 emas, 47 perak, dan 51 perunggu.

APG 2018 tidak hanya mewariskan prestasi, tetapi juga kesadaran luar biasa tentang keberadaan mereka. Ingat, mereka yang muncul di APG 2018 hanyalah sebagian kecil dari seluruh penyandang disabilitas di seluruh Indonesia yang [jumlahnya 11.580.117 orang menurut PUSDATIN Kementerian Sosial tahun 2010 (PDF)](https://www.ilo.org/wcmsp5/groups/public/---asia/---ro-bangkok/---ilo-jakarta/documents/publication/wcms_233426.pdf). Jadi, apabila kita berjalan-jalan ke ruang publik atau ke tempat manapun, ada potensi kita akan bertemu mereka atau bahkan berinteraksi dengan mereka.

Salah satu warisan epik apik dari APG 2018 adalah kumpulan video pada [kanal Asian Para Games 2018](https://www.youtube.com/channel/UCow7I3vE_vijzgtiAKwI61w), terutama [episode **INTERAKSI BERETIKA**](https://www.youtube.com/results?search_query=asian+para+games+interaksi+beretika). Beberapa jenis penyandang disabilitas harus mendapatkan perlakuan tertentu ketika kita ingin berinteraksi dengan mereka agar interaksi lancar dan tidak menyakiti. Nah, video ini dibuat untuk mengajarkan kita mengenai hal itu.

Serial video episode INTERAKSI BERETIKA dibuka dengan perkenalan:

https://www.youtube.com/watch?v=wOHnXeXPlTg

Berikutnya, sang tuan rumah mengajarkan interaksi beretika dengan penyandang disabilitas intelektual. Salah satu contohnya adalah berbicara langsung kepada mereka, tidak kepada pendampingnya.

https://www.youtube.com/watch?v=kNl6EjL180M

Sang tuan rumah mengajarkan interaksi beretika dengan penyandang disabilitas netra, seperti menepuk tangan mereka dengan punggung tangan kita untuk memperkenalkan diri dan mengobrol serta mempersilakan mereka untuk memegang tangan kita (menggandeng) alih-alih kita yang memaksa menggandeng mereka.

https://www.youtube.com/watch?v=n-gIJbYFvmQ

Video selanjutnya, sang tuan rumah mengajarkan interaksi beretika dengan penyandang disabilitas daksa, khususnya dengan pengguna kursi roda. Contoh interaksinya adalah meminta izin sebelum membantu, mencarikan tempat luas untuk tempat mereka duduk, dan membantu mereka menuruni tangga.

https://www.youtube.com/watch?v=j2XENJFke4Q

Pada episode interaksi beretika dengan penyandang disabilitas mental, tidak begitu banyak info yang disampaikan. Dalam video tersebut, narasumber menerangkan tak ada tindakan khusus ketika berinteraksi dengan mereka, cukup hormati.

https://www.youtube.com/watch?v=_pe29NDb8wA

Episode terakhir adalah penjelasan interaksi beretika dengan penyandang disabilitas rungu (tuli), seperti menepuk pundak jika memanggil dan mengedipkan lampu ketika menarik perhatian mereka di dalam ruangan.

https://www.youtube.com/watch?v=hdcFERE-TXE

Apakah video-video tersebut sudah mewakili semua jenis penyandang disabilitas? Saya tidak tahu, tetapi yang jelas video-video itu sangat membantu. Video sudah selayaknya diajarkan secara berkelompok di lingkungan internal maupun di lingkungan publik, "begini lho, cara kita berinteraksi dengan difabel atau penyandang disabilitas."

Saya jadi ingat, saya juga punya teman laki-laki disabilitas daksa semasa SMP. Tangan kirinya, seingat saya, hanya sampai di bawah siku. Interaksi seperti biasa layaknya ngobrol dengan teman-teman lain. Karena pada dasarnya orangnya _slengekan_ dan _easy going_, dia juga kerap di-_troll_, hasilnya ia dan teman-teman lain tertawa bareng.

Hehe …​
