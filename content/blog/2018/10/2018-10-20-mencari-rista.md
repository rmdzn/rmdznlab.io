---
title: "Mencari Rista"
date: "2018-10-20"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "binoculars-1209011_1280.jpg"
aliases:
  - /post/2018-10-20-mencari-rista/
---

![teropong](/blog/2018/10/images/binoculars-1209011_1280.jpg)

_Ini bukan kisah seperti Hera yang mencari Herman ~_

Di sebuah sore, hari Rabu 5 Oktober, kutengok layar ponsel. Bagian kiri atas terlihat ikon notifikasi pesan baru masuk di WhatsApp. Pinta, seorang sahabat SMA bertanya apakah aku mempunyai nomor HP kawan satu kelas.

Tidak punya, jawabku, kecuali nomor satu orang yang sepertinya masih aktif karena terlihat pada foto profil WA yang kerap diganti, tapi bukan ia yang dicari. Sahabatku mencari Rista. Terakhir berhubungan beberapa tahun lalu. Pinta bercerita, Rista pindah ke Sulawesi dan kuliah di Universitas Tadulako. Bencana gempa dan tsunami di Palu dan Donggala kemarin tentu menjadi kekhawatiran tersendiri mengenai nasib sang kawan. Aku terhenyak.

"Coba cari nomornya di buku tahunan SMA," aku menyarankan. "Tidak bisa, buku tahunan ada di tempat bapak," sahut sahabat yang saat ini tinggal bersama keluarga barunya, termasuk dua bayi lucunya itu.

Aku agak sangsi dengan nomor HP kawan-kawan di buku yang terbit sebagai kenang-kenangan masa SMA itu. Aku yakin hanya sedikit orang yang mempertahankan satu nomor HP selama bertahun-tahun.

Setelah mengambil buku itu dari lemari kecil di ruang tengah dan mencari halaman tepat di foto Rista berada, pas bersebelahan dengan fotoku, kujepret lalu kukirim ke WA Pinta. Sesuai dugaan, nomor Rista tidak didaftarkan di WA dan ketika kucoba kirim SMS, juga tidak terkirim. Kemudian kuberanikan diri untuk bertanya kepada [seorang kawan satu kelas lainnya](https://twitter.com/gitabmdw) lewat Twitter, selepas lulus kuliah ternyata dia juga tidak berkontak dengan Rista. Akun Facebook-nya sudah tidak ada.

Hampir dua minggu setelah itu, tepatnya Kamis 18 Oktober malam, Pinta mendapatkan titik terang. Ia mengirim tangkapan layar ponsel, profil Rista terpampang di platform les privat [banuamentor.com](http://banuamentor.com). Ia meminta nomor HP ke admin Banuamentor tapi tak diberi. Saat ditanya mengenai kabar Rista juga tidak direspon.

![Profil Rista di laman Banuamentor](/blog/2018/10/images/whatsapp-image-2018-10-18-at-20-40-56.jpeg?w=576) Profil Rista di Banuamentor

Sahabatku mengaku, teman-teman SMA lain yang ia tanyai tak memberi respon positif. Akhirnya, hanya dia yang berjuang mencari kabar sang kawan yang kini entah bagaimana kabarnya.

Tak menyerah begitu saja. Setelah memperhatikan _username_ Rista di Banuamentor, bisa-bisanya aku terlalu lugu dan bodo. Ia menggunakan _username_ dengan unsur "cherrizta". Bukan sekadar nama asal-asalan. Dia sepertinya menjadikan "cherrizta" sebagai jenama. _Username_ akun Facebook, Twitter, Wordpress, dan [Ask.fm](https://ask.fm/cherrizta) yang tidak aktif bahkan mati mengandung jenama "cherrizta". Aku kemudian menggunakan nama tersebut untuk mencari teman sekampus yang pernah berhubungan dengannya, setidaknya melalui status yang diunggah ke Facebook.

Sungguh beruntung. Di kampus, Rista kemungkinan besar adalah aktivis sehingga tak hanya satu dua akun Facebook yang menyebut nama "cherrizta" dalam statusnya. Di hari yang sama, aku mencoba menghubungi salah satu akun teman Rista mengenai keberadaan dan kontaknya. Jumat 19 Oktober siang, aku kembali menghubungi kawan Rista yang lain, dengan konteks yang sama seperti sebelumnya. Hingga ini ditulis, pesanku belum dibaca oleh keduanya.

Saya sebetulnya sudah agak menyerah. Tapi masih ada "jatah" satu teman kuliah Rista yang belum kuhubungi. Pagi hari, Sabtu 20 Oktober, kuputuskan untuk menjapri akun tersebut. Hanya beberapa menit setelah pesan dikirim, teman kuliah Rista menjawab, "_Alhamdulillah, Rista baik-baik saja, pasca gempa saya sempat (kirim) WA dia dan dia sekeluarga selamat_", sekaligus mengirim nomor WA kawan yang sedang dicari. Terima kasih [mbak Rhianhy](https://www.facebook.com/rhianhy.fekon).

Langsung dong saya kirim pesan membahagiakan itu ke inisiator proyek pencarian ini. "_Kabar baguuuuuuussssssssss ppiiiiiinnn, japri tiga temennya. Dua temen belum baca, satu temen ku tanya langsung jawab_," kataku sambil mengunggah tangkapan layar _chat_ Facebook teman kuliah Rista.

![Status WhatsApp Pinta setelah kontak Rista ketemu](/blog/2018/10/images/photo_2018-10-20_10-51-07.jpg) Status WA Pinta setelah kontak Rista ketemu

"Langsung (kirim) di WA saja, tadi saya sudah kasih tau dia kok …​ 🙂" Karena kata-kata itu, kuburu sahabatku untuk segera menghubungi Rista. Dia (Rista) pasti meluangkan waktu, ujarku meyakinkan. Aku tidak mengobrol banyak dengannya. Sulit mencari bahan basa-basi ketika aku sendiri tidak pernah berbicara via teks dengannya sejak SMA—​tujuh tahun lalu.

Rista masih di Palu, tetapi saat tulisan ini terbit, kemungkinan besar dia sudah di Australia. Dia kehilangan adik dan bapaknya tahun 2013 dan 2014, kita berharap semua baik-baik saja. Kami yang meluangkan waktu untuk berbincang dengannya semoga dapat menjadi tombo ati dan hiburan di kala dirinya menganggap hidupnya sepi.

> Hai Rista! Kalau kamu baca ini. Teman-temanmu masih di sini, mendukungmu dari sini, jangan ragu kalau ingin menghubungi kembali!
