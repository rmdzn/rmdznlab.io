---
title: "Sindrom Penikah Muda"
date: "2018-10-29"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "nikah-panggung-rmdzn.jpg"
aliases:
  - /post/2018-10-29-sindrom-penikah-muda/
---

![nikah](/blog/2018/10/images/nikah-panggung-rmdzn.jpg)

Menikah adalah sesuatu yang sakral. Bukan hanya urusan ranjang dan berkembang biak, menikah bertujuan membentuk kesalehan komunitas terkecil dalam masyarakat yang kemudian melahirkan generasi saleh untuk menjadi penyaleh komunitas berikutnya, dan seterusnya dan seterusnya hingga dunia makmur sentosa.

Beratnyaaa ...

Generasi kekinian sepertinya menyadari kalau menikah bukanlah hal remeh. Oleh sebab itu, ia menjadi sebuah gengsi yang memunculkan fenomena-fenomena berupa sindrom menggelikan.

Di media sosial, kita disuguhi dengan status menggalau ria para angkatan kerja yang masih menjomlo. Katanya, lebih baik menikah muda daripada pacaran. Namun, ketika "dipaksa" untuk menjomlo, perasaan baik-baik saja hanya menjadi cermin kepura-puraan.

Hari ini mengutip ayat Alquran tentang menjaga pandangan ditambah optimisme mendapatkan pasangan saleh/salehah, di hari berikutnya me-_repost_ status komunitas penolak pacaran terbesar di Indonesia dengan emoji _luv luv_ untuk menegaskan bahwa si pe-_repost_ adalah seorang jomlo berkualitas yang siap dipanen secara relijius.

Foto _prewed_ merajalela. Gaya _folk_\-nya yang indah menginspirasi para milenial untuk kebelet menikah. Kata si empu foto, pokoknya harus menikah, demi konten Instagram yang menarik. Mereka mungkin tak berniat pamer, tapi _naudzubillah_ tanggapan para netijen muda yang menjadikannya sebagai tujuan hidup.

Setelah menikah, para pelaku nikah muda mulai hobi memamerkan aktivitas barunya sebagai suami atau istri. Pergi jalan-jalan bareng pasangan, memasak bergantian dengan wajan penuh minyak goreng curah dan ikan goreng di atas spatula, dengan tulisan, "Beda banget rasanya dibanding saat sendiri, hehe." Foto pasangan yang dipotret dari belakang atau foto keduanya membelakangi terbenamnya matahari juga kadang menghiasi linimasa para pengikut.

Itu terjadi selama beberapa bulan atau malah hanya beberapa minggu. Setelah itu pasangan ini mulai sibuk dengan kehidupan duniawinya, pencitraan saat bekerja dan mengurus rumah tangga menjadi aktivitas yang jarang dilakukan meskipun sindrom akan kembali kumat ketika si ibu hamil dan _dedek_ bayi lahir--tentunya dengan obyek yang berbeda dari sebelumnya.

Sindrom tersebut bisa jadi memang masuk akal lho. Orang yang terbiasa membagi perkembangan hidupnya di media sosial akan selalu merasa senang dan puas setelah melakukannya. Senyawa dopamin terstimulasi oleh tindakan mengunggah konten atau ketika mereka berselancar di linimasa media sosial. Oksitosin yang biasanya muncul ketika seseorang berciuman atau berpelukan juga bertanggung jawab membuat seseorang betah di depan media sosial. Setiap berselancar selama 10 menit di media sosial, [level oksitosin akan meningkat 13,2%](https://www.fastcompany.com/1659062/social-networking-affects-brains-falling-love).

Intinya, keduanya sama-sama mengurangi stres dan depresi.

_Umm_ ... stres dan depresi? Padahal mereka mereka baru saja menikah dan lagi sayang-sayangnya? Stres timbul sebelum akad nikah dan _walimahan_, saat mempersiapkan penyewaan tenda, tata rias, _catering_, dll. Mengunggah semua hasil dari jerih payah mempersiapkan pernikahan adalah salah satu obat untuk mengurangi bahkan menghilangkan stres itu. "Akhirnya kita berhasil menikah, yay!"

Saya menyebutnya sebagai "sindrom", emosi dan tindakan yang biasanya membentuk pola yang dapat diidentifikasi. Meskipun fenomena itu secara harfiah tidak dapat disebut sebagai "sindrom", keberpolaan itu ada kalanya menggelikan dan uraian di atas adalah salah satunya.

Pola milenial itu tak selamanya seru.
