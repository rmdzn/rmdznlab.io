---
title: "Kisah Agni, Kultur Cabul, dan Kultur Menyalahkan-korban"
date: "2018-11-06"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "fear-1131143_1280.jpg"
aliases:
  - /post/2018-11-06-kisah-agni-kultur-cabul-dan-kultur-menyalahkan-korban/
---

![takut](/blog/2018/11/images/fear-1131143_1280.jpg)

Ini miris. Hari Senin (5/11) kemarin, [Balairung](https://id.wikipedia.org/wiki/Balairung_(majalah)) mengangkat laporan kasus pelecehan seksual dan pemerkosaan yang dialami oleh salah satu mahasiswa UGM. Cerita tersebut viral, hingga artikel berjudul [_Nalar Pincang UGM atas Kasus Perkosaan_](http://www.balairungpress.com/2018/11/nalar-pincang-ugm-atas-kasus-perkosaan/) tersebut sempat tidak dapat diakses karena bejubelnya pengunjung.

Pewarta Balairung menuliskan kisah Agni, nama samaran korban, yang saat itu menjalani KKN di Maluku. Berita yang sangat mengganggu nurani. Karena waktu yang tidak memungkinkan untuk pulang malam ke _camp_\-nya, Agni dengan terpaksa menginap di pondokan tempat tim KKN lelaki. Pelecehan seksual dan pemerkosaan itu terjadi saat ia sudah terlelap. Ia terbangun karena teman lelaki bejatnya yang berinisial HS melakukan hal tidak senonoh pada tubuhnya. Kultur menyalahkan-korban (_victim blaming_) membuat Agni enggan berteriak saat dilecehkan. Ia takut dituduh warga kalau tindakan itu disebabkan suka sama suka.

Di Kampus Biru, selepas KKN, Agni sibuk mengadvokasi dirinya sendiri karena pelaku bebas melenggang begitu saja. Namun, tanggapan dari kampus justru melukai kita semua. [Kutipan](https://www.reddit.com/r/indonesia/comments/9ufhwj/nalar_pincang_ugm_atas_kasus_perkosaan/e93tcd1/) menyakitkan dari artikel di Balairung di antaranya:

> Menurutnya kasus Agni lebih baik diselesaikan secara baik-baik dan kekeluargaan, sehingga tidak mengakibatkan keributan.

> Jangan menyebut dia (Agni) korban dulu. Ibarat kucing kalau diberi gereh (ikan asin dalam bahasa jawa) pasti kan setidak-tidaknya akan dicium-cium atau dimakan.

> Alasannya sanksi DO harus melalui prosedur pengajuan aduan ke komite etik UGM. Sementara kasus kekerasan seksual yang dialami Agni dianggap bukan termasuk pelanggaran berat sehingga tidak perlu penanganan yang serius.

> Mengingat HS berasal dari Fakultas Teknik dan Agni dari Fisipol, maka penyelesaian kasus tersebut seharusnya memang melibatkan tim investigasi, bukan perwakilan DPkM secara sepihak.

> Ia mengaku, sejak pertemuan di lokasi KKN kala itu, baik dirinya, Heru, maupun Djaka memutuskan untuk memberi sanksi kepada Agni dan HS. Mereka menilai bahwa baik Agni maupun HS sama-sama berkontribusi pada terjadinya peristiwa tersebut.

> "Kalau gitu, berarti Pak Adam tidak sepenuhnya bersalah. Seandainya kamu tidak menginap di sana kan tidak akan terjadi, tho?"

Tidak perlu menjadi feminis untuk merasakan pedih yang dialami Agni saat pihak kampus tidak berpihak kepadanya.

Peristiwa memilukan itu terjadi pada tahun 2017. Semua tindakan yang dilakukan oleh kampus UGM tidak memuaskan. [Respon yang kaku](https://twitter.com/UGMYogyakarta/status/1059743863781253120), templat rilis pers normatif yang membosankan. [Kata Dekan Fisipol UGM](https://tirto.id/tanggapan-ugm-soal-dugaan-kekerasan-seksual-mahasiswa-saat-kkn-c9mL), pada bulan Agustus 2018, tim investigasi berhasil mengumpulkan data dan fakta dari lapangan maupun dari pihak-pihak terkait. Itu akan dirumuskan dan sebagai rekomendasi kepada Rektor.

Tidak ada langkah konkrit. Warganet banyak yang kecewa. Pemerkosa justru melenggang dan siap diwisuda pada bulan November ini.

Pelecehan seksual di kampus ternyata sudah menjadi rahasia umum. Mahasiswi di Depok, Yogyakarta, dan Medan [sama-sama pernah mengalaminya](https://tirto.id/cerita-pelecehan-seksual-di-kampus-dosen-mesum-jadi-rahasia-umum-cNpg). Dosen genit yang melucah dan menggoda via verbal, hingga melecehkan secara fisik. Semua korban merasakan takut untuk melapor, lagi-lagi karena budaya menyalahkan-korban yang mengakar kuat. Jijiknya, salah satu dosen UGM berinisial EH yang sempat terekspos di The Jakarta Post dua tahun lalu karena telah melecehkan mahasiswinya justru mengajar teori-teori feminis. "EH tahu bahwa kebanyakan korban pelecehan seksual tidak akan melapor," [kata Margaretta, salah satu korban](https://tirto.id/yang-harus-kampus-lakukan-mengatasi-pelecehan-seksual-cNnF).

Ikhaputri Widiantini, pengajar di FIB Universitas Indonesia, yang fokus membahas feminisme melakukan [cara apik untuk memancing mahasiswanya bercerita](https://tirto.id/kampus-cenderung-menutupi-kasus-kasus-pelecehan-seksual-cNtK) tentang apapun kepada dirinya. Melalui _logbook_\--semacam buku harian—​mahasiswa menceritakan pelecehan seksual yang pernah mereka atau teman mereka alami. Upi, panggilan sang dosen, benar-benar menjaga privasi pencerita. Yang terpenting para korban tidak merasa sendiri dan bisa mendiskusikan traumanya, kata Upi kepada Tirto.

## Warganet yang "terlalu fokus"

Warganet sibuk men-_doxxing_ HS, sang pelaku. Mereka ingin menghukum pelaku secara sosial. Mereka berharap HS tidak mendapatkan kerja di manapun, karena siapa pula yang nyaman bekerja bareng dengan penjahat seksual?

HS memang kurang ajar. Saya juga berharap sesuatu yang buruk menimpanya. Namun, kita juga jangan mengalihkan fokus dari kultur menyalahkan-korban yang digaungkan kampus. Jangan lupa, HS berlindung di balik sistem kampus yang tidak berperikemanusiaan. Jangan lupa juga kejadian pelecehan juga tak hanya terjadi di kampus biru semata. Kampus-kampus ini perlu dituntut untuk berbenah menanggalkan kesombongan untuk hanya sekadar "menjaga nama baik". Moral harus tetap diutamakan, apapun alasannya.

Semua berawal dari kultur cabul (_rape culture_) yang dianggap menjadi hal remeh di masyarakat. Pelucahan (_catcalling_) misalnya, kultur cabul yang mungkin hampir mudah kita temukan di jalanan desa sampai kota. Harus ada edukasi lebih kepada masyarakat tentang kultur buruk ini. Ketika duduk-duduk bersama warga kampung setelah kerja bakti dan salah satu warganya melucah seorang perempuan yang sedang lewat kemudian disusul dengan tawa warga lain, saya merasa berdosa. Saya merasa tidak mampu memberi tahu kepada warga bahwa itu adalah hal yang sangat buruk. Itu terjadi belum lama ini.

Saat masih sekolah, kelas X waktu itu. Satu guru terkenal genit terhadap murid-murid yang terlihat cantik. Teman saya tak jarang digoda saat pelajaran berlangsung. Guru tersebut duduk di atas meja, posisi kaki menyilang, kaki kanan di atas kaki kiri dan tangan terpaut di atas lutut sambil mengajar serta sambil sedikit menggoda murid. Banyak teman yang tertawa, tapi saya _iiuuuh_ luar biasa. Jijik setengah mati. Kultur cabul yang dianggap biasa, hanya dianggap sebagai candaan.

Dengan kultur seperti itu, apakah kita masih bisa berharap edukasi dan hukum untuk melindungi wanita di Indonesia semakin paripurna dan berjalan maksimal? Atau, lagi-lagi kita hanya dapat mengandalkan segelintir orang untuk peduli dengan nasib kita? Tak perlu jauh-jauh, maukah kita memahami buruknya kultur cabul (_rape culture_) dan menyalahkan-korban (_victim blaming_)?
