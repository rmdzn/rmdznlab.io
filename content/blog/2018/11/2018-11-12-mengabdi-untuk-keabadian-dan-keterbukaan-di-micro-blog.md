---
title: "Mengabdi untuk Keabadian dan Keterbukaan di Micro.blog"
date: "2018-11-12"
categories: 
  - "blog"
tags: 
  - "blog"
aliases:
  - /post/2018-11-12-mengabdi-untuk-keabadian-dan-keterbukaan-di-micro-blog/
---

![Laman Micro.blog](/blog/2018/11/images/fbbb750f-f73b-4090-8826-17e4c8a2683d.png)

_"Mengabdi untuk keabadian" yang terinspirasi dari kutipan Pak Tur._

Hari ini (8/11) saya mencoba menulis di micro.blog, mumpung mendapatkan fasilitas _trial_ gratis selama 10 hari (ralat dari [pos aslinya](https://rmdzn.micro.blog/2018/11/08/mengabdi-pada-keabadian.html) yang menyebut dua minggu).

Jujur, saat menuliskan ini saya masih bingung bagaimana sebenarnya micro.blog bekerja mengingat platform ini sendiri disebut-sebut bukanlah jejaring sosial pada umumnya meskipun memiliki bentuk seperti media sosial besutan Jack Dorsey dkk—​Twitter. Kata Manton Reece, sang pendiri micro.blog, platform ini memang tidak ditujukan untuk menggantikan sang burung kenari.

Tulisan ini diunggah ke [rmdzn.micro.blog](https://rmdzn.micro.blog/2018/11/08/mengabdi-pada-keabadian.html), platform blog independen yang disediakan oleh layanan micro.blog. Ia mendukung _open web_ serta mempraktikan gagasan IndieWeb. Platform blog ini sebetulnya hanya dapat diperoleh secara premium dengan harga 5 USD perbulan. Namun, bagi para pendaftar, mereka mendapatkan _trial_ selama 10 hari untuk mencicipi micro.blog layaknya pengguna premium.

Setelah tulisan terbit, ia akan secara otomatis terunggah sebagai umpan (feed) ke akun micro.blog saya: [micro.blog/rmdzn](https://micro.blog/rmdzn). Akun ini gratis. Jadi, biarpun setelah 10 hari _trial_ habis, [micro.blog/rmdzn](https://micro.blog/rmdzn) akan tetap hidup. Yang pasti mati adalah halaman [rmdzn.micro.blog](https://rmdzn.micro.blog).

Platform blog yang berdiri sejak tahun 2017 ini mendukung Markdown sehingga sangat cocok dipakai oleh para penggemar format penulisan tersebut.

Saya akan terus mempelajari micro.blog dan melihat apa saja yang menarik di sini.

_Setelah mempelajari platform micro.blog selama beberapa hari …​_

Micro.blog adalah proyek hasil penggalangan dana via [Kickstarter](https://www.kickstarter.com/projects/manton/indie-microblogging-owning-your-short-form-writing) yang dibikin oleh [Manton Reece](https://manton.org). Penggalangan dana ini sangat sukses. Dari gol 10.000 USD, Micro.blog mampu meraup delapan kali lipatnya: 86.696 USD.

Manton sangat peduli dengan dunia keterbukaan web sehingga di samping membuat platform _microblog_, ia juga menulis buku bertema _Indie Microblogging_. Apa itu _Indie Microblogging_? Indie adalah kata ringkas dari "independen" yang berarti merdeka tidak terikat dengan apapun sedangkan _microblogging_ adalah aktivitas menulis blog kecil-kecilan. Bila digabungkan, _indie microblogging_ memiliki arti aktivitas menulis blog kecil-kecilan secara independen.

Di era meledaknya media sosial seperti sekarang ini, konten pengguna dikuasai oleh korporasi. Sebut saja Twitter, layanan _microblogging_ terpopuler. Konten yang dibagikan via Twitter hanya dapat diakses lewat Twitter dan hanya dimiliki oleh Twitter. Seember-embernya kita mentwitkan tulisan di Twitter, orang-orang bisa beramai-ramai melaporkan twit kita dan memberikan hak kepada Twitter untuk menghapus twit itu atau bahkan memblokir akun kita. Itu adalah contoh dari alasan kedua. Alasan lain, twit yang pernah kita tulis tidak dapat dipindahkan ke platform lain. [Diunduh bisa](https://help.twitter.com/en/managing-your-account/how-to-download-your-twitter-archive), tetapi diunggah kembali ke platform selain Twitter bahkan ke Twitter itu sendiri sangat tidak mungkin. Itu belum kasus-kasus yang berhubungan dengan privasi seperti kasus heboh Cambridge Analytica di Facebook kemarin dan potensi hilangnya konten kita ketika perusahaan itu bangkrut.

Era inilah yang merusak sisi keterbukaan web dan dunia media sosial khususnya. Manton [mengutip](http://manton.micro.blog/2016/11/18/todays-social-networks.html) beberapa pengguna Twitter dan Facebook yang kecewa dengan rusaknya dunia jejaring sosial (media sosial), entah karena _filter bubble_, merebaknya hoaks, dll. Bapak WWW, Tim Bernes Lee, saja [menyebut internet sekarang](https://bdtechtalks.com/2018/03/19/tim-berners-lee-broken-internet/) tidak lagi bersifat demokratis. Alih-alih dimiliki oleh pengguna, internet dimiliki dan dikuasai oleh korporasi.

## Bagaimana Micro.blog memperbaiki media sosial?

Micro.blog menerapkan [IndieWeb](https://indieweb.org)\--kumpulan utilitas yang mengizinkan seseorang untuk membangun web yang independen, tak terikat dengan layanan milik korporasi. Micro.blog dibangun berbasiskan [Jekyll](https://jekyllrb.com/) dengan dukungan microformats2, Webmention, dan Micropub. Ketiganya memperkenankan pengguna Micro.blog untuk menulis di platform tersebut atau di platform lain, mengobrol, saling menyebut (_mention_) pengguna lain, dan sebagainya.

> Saya tahu tentang IndieWeb beberapa bulan lalu, tetapi baru benar-benar paham kegunaannya setelah mencoba micro.blog.

[Microformats](https://indieweb.org/microformats) adalah ekstensi HTML (ya, bukan API) untuk, salah satunya, me-_markup_ tautan berupa _mention_ ('@') ke pengguna IndieWeb lain. Contohnya, di Twitter ketika saya menulis "@catatanrmdzn" maka secara otomatis ia akan membentuk tautan ke akun [twitter.com/catatanrmdzn](https://twitter.com/catatanrmdzn). Ekstensi ini juga dapat ditugasi untuk membentuk sebuah utas (_thread_) atau runtutan obrolan antar pengguna.

[Webmention](https://indieweb.org/Webmention) adalah sebuah standar untuk membuat respon, baik _mention_, komentar, maupun _like_. Blog di platform apapun yang mendukung Webmention, ketika URL-nya disebut, dibagikan, maupun dikomentari orang, pemilik blog akan mendapatkan notifikasi.

[Micropub](https://indieweb.org/Micropub) adalah API yang dapat dipakai oleh aplikasi pihak ketiga untuk membuat, memperbarui, dan menghapus konten. Aplikasi-aplikasi pihak ketiga yang dapat dipakai untuk mengelola akun micro.blog seperti [Quill](http://quill.p3k.io/) menggunakan API ini.

Ketiganya adalah standar W3C.

Saat saya membuat akun di Twitter, segala obrolan yang menyangkut tulisan yang saya bagikan hanya dapat disimak di Twitter. Dengan IndieWeb yang diterapkan oleh micro.blog, saya dapat menulis di blog premium \*.micro.blog, Wordpress, Tumblr, maupun platform lain dan dapat membicarakan tulisan tersebut di platform masing-masing yang notifikasinya akan muncul di akun micro.blog.

Micro.blog sebetulnya adalah kumpulan umpan (_feed_) dari masing-masing blog, sehingga semua konten yang kita tulis adalah milik kita, di server kita. Semua bebas berbicara di server masing-masing, tidak akan ada TOS yang melekat (kecuali jika menggunakan layanan semacam wordpress.com) yang akan mengganggu tulisan kita. Hanya saja, micro.blog tetap memiliki manajer komunitas yang akan mengawasi linimasa micro.blog. Mereka tidak akan mengizinkan konten-konten berbau kekerasan dan kebencian muncul pada linimasa mereka.

Platform besutan Manton itu, seperti yang saya sebut sebelumnya, menyediakan akun gratis dan akun premium. Jika kita ingin membuat blog sendiri (blog eksternal), kita cukup menggunakan akun gratis, dengan syarat memasang beberapa _plugin_ untuk mendukung fitur-fitur IndieWeb, seperti _plugin_ [Webmention](https://wordpress.org/plugins/webmention/) jika ingin menggunakan blog Wordpress. Sedangkan jika kita tidak ingin repot-repot mengatur sendiri blog Wordpress (atau blog eksternal lain), kita dapat membayar 5 USD perbulan dan mendapatkan domain _namauser.micro.blog_ (dapat dikustom dengan domain sendiri). Mereka menyebut layanan premium ini sebagai _micro.blog self-hosted_.

Salah satu contoh akun yang menggunakan _micro.blog self-hosted_ adalah Manton Reece. [Micro.blog/manton](https://Micro.blog/manton) adalah umpan dari [manton.micro.blog](https://manton.micro.blog). Contoh akun yang menggunakan blog sendiri (blog eksternal) adalah Noah Read. [Micro.blog/noah](https://Micro.blog/noah) merupakan umpan dari [noahread.net/snippets](https://noahread.net/snippets).

## Beda dengan Mastodon yang sedang _hype_ itu?

Mastodon dan Micro.blog bukanlah platform yang perlu dipertentangkan mengingat [micro.blog sendiri mendukung Mastodon](https://www.manton.org/2018/11/07/microblog-mastodon.html).

Dari perspektif saya, micro.blog memiliki banyak keunggulan dibandingkan Mastodon bila dilihat dari kepemilikan konten dan penghematan sumber daya server. Jika kita menyewa server untuk diinstal blog dan dijadikan pengisi konten micro.blog, sumber daya server (CPU, ruang penyimpanan, memori, dsb) hanya digunakan ketika kita mengunggah konten. Berbeda dengan Mastodon, platform media sosial itu juga akan mengunduh konten pengguna Mastodon dari _instance_ yang berbeda sebagai _cache_ saat kita mengaksesnya.

## Mengulas

Tampilan Micro.blog sangat sederhana, tidak ada sesuatu yang mengilap. Latar belakang berwarna putih dengan teks hitam dan tautan berwarna biru. Teks dan logo Micro.blog berwarna kuning menonjol di pojok kiri atas.

![Linimasa Micro.blog](/blog/2018/11/images/db1d0dad-df58-458b-a759-16c90e97ec41.jpg) Linimasa Micro.blog

Terdapat delapan menu dan satu tombol di bagian atas. _Timeline_ adalah linimasa. Kumpulan tulisan pengguna yang kita ikuti. _Mentions_ adalah tulisan pengguna lain yang menyebut nama pengguna kita (diawali karakter '@'). _Favorites_ adalah daftar tulisan yang kita favoritkan. _Discover_ adalah kumpulan tulisan yang diunggah pengguna lain. Cocok sebagai sarana untuk mencari pengguna Micro.blog lain. Di halaman ini, terdapat _dropdown_ untuk memilih tulisan atau akun yang fokus membahas topik tertentu; buku, sepak bola Amerika, musik, basket, _podcast_, foto, sepak bola, basebol, dan film. Topik disimbolkan dengan emoji.

Empat menu berikutnya adalah menu _Plans_ untuk memutakhirkan akun (5 USD/bulan) dan mengaktifkan fitur lintas-pos (_crosspost_) ke media sosial lain (2 USD/bulan), menu _Account_ untuk mengatur akun pengguna, menu _Help_ sebagai halaman bantuan, serta menu _Posts_ untuk melihat tulisan yang pernah kita unggah di _micro.blog self-hosted_.

Sederet dengan menu-menu tersebut, tombol _New Post_ digunakan untuk menulis pos ke _micro.blog self-hosted_. Saya belum tahu, apakah tombol ini akan hilang ketika 10 hari _trial_ saya habis. Kita lihat tanggal 19 November besok. **Update (19/11)** Setelah masa _trial_ habis, di atas linimasa akan muncul pesan _Your hosted microblog has expired. [**Please upgrade**](https://micro.blog/new/site) to continue posting to your site. Thanks for supporting Micro.blog!_. Tebak, laman [rmdzn.micro.blog](https://rmdzn.micro.blog) ternyata tidak mati. Kolom untuk menulis ke tempat tersebut pun masih ada, cuma tidak dapat mengunggah teks.

![Menulis di Micro.blog](/blog/2018/11/images/ce3d0109-4a4e-4517-be99-79b2ef1ce451.jpg) Pos Micro.blog

Meskipun mirip Twitter, tidak semerta-merta Micro.blog dapat disebut sebagai klonanya. Micro.blog tidak mendukung tagar. Micro.blog juga tidak menampilkan jumlah pengikut setiap pengguna. Ini menarik. Mereka hanya menampilkan jumlah akun yang diikuti karena tidak ingin menjebak pengguna dalam kultur banyak-banyakan pengikut dan _follback-follback-an_ akun.

Tidak hanya itu. Micro.blog juga tidak akan menampilkan notifikasi jika pengguna lain memfavoritkan status kita. Fitur _favorite_ adalah fitur untuk menyimpan pos favorit (_ya iyalah_), tidak lebih. Kalau kita menyukai tulisan pengguna, kita dapat me-_reply_ langsung ke pemilik tulisan. "Hai, saya menyukai tulisan kamu," begitu contohnya ;)

Tidak akan ada iklan di Micro.blog. Semua konten benar-benar murni ditulis oleh pengguna. Alih-alih lewat iklan, pembiayaan Micro.blog diperoleh dari akun premium.

Sayangnya, Micro.blog terlihat "eksklusif" karena hanya mendukung sistem operasi iOS dan macOS. Mereka hanya memiliki aplikasi resmi di sana. Pun tak sedikit aplikasi pihak ketiga yang juga hanya dibuat untuk sistem operasi tersebut. Dulu ada aplikasi MicroPost untuk Android, tetapi sekarang hilang. Ada aplikasi [Dialog](https://play.google.com/store/apps/details?id=com.dialogapp.dialog), tetapi masih dalam fase prarilis. Walau begitu, selama halaman web micro.blog dapat diakses dengan mudah, kita tidak perlu menghiraukan aplikasi-aplikasi "_native_" seperti itu.

Saya percaya, masa depan dunia jejaring sosial ada di tangan platform seperti micro.blog ini.

Saya memiliki akun micro.blog di [micro.blog/rmdzn](https://micro.blog/rmdzn) dan saat ini saya menggunakan versi gratisnya dengan konten umpan (_feed_) ramdziana.wordpress.com.
