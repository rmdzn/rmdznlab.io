---
title: "Dangdut VS 'Band'"
date: "2018-11-16"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "musisi-pegang-terompet-rmdzn.jpg"
aliases:
  - /post/2018-11-16-dangdut-vs-band/
---

![musisi](/blog/2018/11/images/musisi-pegang-terompet-rmdzn.jpg)

Malam 27 Oktober lalu, kampung sebelah mengadakan konser asyik-asyik untuk merayakan Hari Sumpah Pemuda. Karang Taruna kampung yang didapuk untuk mengurus acara itu mengundang organisasi pemuda tetangga, termasuk kami. Undangan disampaikan kepada ketua organisasi [yang belum lama terpilih itu](http://siarkampung.co.nf/2018/10/gondangan-selenggarakan-pemilihan-ketua-rw-dan-ketua-pemuda-baru/) dan dibagikan kembali lewat status WhatsApp-nya agar anggota-anggotanya tahu.

Ada yang seru. Bukan dangdutan atau koploan, Karang Taruna kampung sebelah justru mengundang grup musik pop. _Aah …​ itu biasa_, mungkin itu yang kamu pikirkan, tetapi grup musik yang diundang tak sekadar grup musik pop: grup musik instrumental yang mayoritas alat musiknya adalah terompet (atau mungkin klarinet). Nama grup musik itu masih terlihat asing bagi saya, [Swn Brass Band](https://instagram.com/swnbrassband).

Saya datang bersama dua pemuda lain yang salah satunya adalah ketua organisasi pemuda kampung. Berbeda dari keduanya yang hanya datang untuk memenuhi undangan, saya datang benar-benar bermaksud ingin melihat performa Swnbrassband secara langsung setelah sebelumnya menikmati permainan mereka lewat Instagram. Saya tidak melihat penampilan grup musik asal Sewon tersebut sampai acara selesai, tetapi saat melihat performa mereka pada sesi perkenalan, permainan mereka memang ciamik. Apresiasi tinggi!

Di balik konser itu, usut punya usut (aah …​ sebenarnya bukan karena usutan, tetapi curhatan dari narasumber kampung), menyelenggarakan konser berkultur pop di kampung butuh usaha yang berat.

Dangdut dan koplo adalah jenis musik yang biasa ditampilkan di perkampungan, setidaknya di padukuhan tempat saya tinggal. Campursari mungkin ada, tetapi sudah sangat jarang. Mengadakan "_band-band-an_" adalah usaha melabrak kebiasaan menikmati para biduan berlenggak-lenggok di atas panggung. Dari konflik antar warga yang disampaikan oleh sang narasumber A1, intinya tidak enak kalau yang tampil bukan dangdut atau koplo plus pakaian _sekseh_.

Suatu waktu, saat warga kampung mengundang biduan dangdut dan koplo bercelana panas, pemudi kampung _mutung_. Entah melakukan _walk out_ saat acara berlangsung atau tidak datang sama sekali sebelum acara berlangsung. Sebelum peringatan Hari Sumpah Pemuda tanggal 27 Oktober itu pun sesepuh tidak mendukung dan mencairkan dana sponsor karena tahu kalau hiburan utamanya adalah "_band_" (Pop, maksudnya) bukan dangdut dan koplo. _Aaah_ …​

Saya tak ingin menyampaikan lebih banyak konflik di kampung itu karena saya juga tidak ingin memuaskan para netijen yang haus keributan. Saya, sebagai warga tetangga, patut menyampaikan dukungan kepada Karang Taruna sebelah yang mampu dan berani melabrak kebiasaan meskipun tahu kalau itu berpotensi "rusuh" dengan warga lain. Karang Taruna sebelah mampu mempertahankan tekad untuk mengundang grup musik yang tidak umum, yang masuk selera mereka.

Dalam dunia bisnis dikenal dengan istilah pangsa pasar. Kalau diterapkan pada kasus Dangdut vs 'Band' (Pop), lingkungan kampung di padukuhan tempat saya tinggal kebanyakan bukanlah pangsa pasar hiburan berkultur pop. Memang, di acara Hari Sumpah Pemuda itu, tiga anak seusia 3 atau 4 SD ikut meramaikan dengan tari modern berlatar musik _Watch Me_\-nya Silento, tapi itu bukan hiburan utama. Di kampung saya juga pernah, anak seusia SD bermain drum mengiringi musik pop. Namun lagi-lagi itu juga bukan hiburan utama.

Kepada bapak-bapak terhormat, dangdut dan koplo bukanlah segalanya. Biar saja pemuda mengundang grup musik sesuai selera mereka. Tak perlu memaksa selera agar sesuai dengan _jenengan_ sekalian. Tertanda saya yang sok mewakili Karang Taruna sebelah~

Dengarkan saja salah satu musik yang dimainkan Swn Brass Band di bawah ini:

\[soundcloud url="https://api.soundcloud.com/tracks/530367999" params="color=#ff5500&auto\_play=false&hide\_related=false&show\_comments=true&show\_user=true&show\_reposts=false&show\_teaser=true" width="100%" height="166" iframe="true" /\]
