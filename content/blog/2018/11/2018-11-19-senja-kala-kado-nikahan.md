---
title: "Senja Kala Kado Nikahan"
date: "2018-11-19"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "gift-rmdzn.jpg"
aliases:
  - /post/2018-11-19-senja-kala-kado-nikahan/
---

![hadiah](/blog/2018/11/images/gift-rmdzn.jpg)

Satu sampai tiga tahun pasca lulus SMA, orang-orang mulai panik melihat kawan satu geng maupun satu kelasnya duduk di pelaminan. Melewati batas ketir-ketir kehidupan, satu geng merencanakan kado yang tak terlupakan untuk pasangan.

Satu geng mengadakan iuran. Beberapa orang membelikan barang. Perkakas dari Tupperware, barang pecah belah, dan barang-barang khas rumah tangga lain bisa jadi pilihan.

Polanya begini. Satu orang menyampaikan lewat grup daring kalau teman bernama A menikah.

"_Barakallah!_," ucap mereka serentak atau satu persatu selisih beberapa menit.

Teman yang terbiasa membelikan kado kemudian menawarkan opsi barang apa saja yang pantas dijadikan kado. Sistem demokrasi tetap dijunjung tinggi, mereka tetap menampung usul teman geng lain, siapa tahu ada usul menarik.

Setelah jenis kado dan jumlah iuran ditetapkan, teman yang biasa membelikan barang mulai bergerilya di sela-sela hari libur. Oiya, terkadang diskusi barang terjadi di grup secara langsung untuk menentukan merek barang mana yang harganya cocok (baca: murah, haha).

Barang terbeli. Kami yang di rumah merasa lega dan bahagia. Berterima kasih kepada teman yang telah rela meluangkan waktunya untuk jalan-jalan ke Progo alih-alih Hartono Mall. Semoga kalian segera memperoleh jodoh, mantap!

Barang yang sudah _dikrukup_ dengan kertas kado diantarkan ke meja among tamu di hari H pernikahan. Itupun jika beruntung ada teman yang memungkinkan datang, kalau tidak beruntung (biasanya karena hari pernikahan yang tidak diadakan di hari libur) kado akan terlunta-terlunta, teronggok di pojok kamar dengan lebah bersarang. Seperti nikahan teman saya bulan April lalu, kado baru diberikan awal bulan Juli. Aah, maafkan teman yang terlambat menyampaikan hadiah untukmu, Kawan.

Kami akhirnya menyadari, kami tak lagi dapat bergerumul memberi kado saat pernikahan teman satu geng. Semua sibuk. Kepada teman yang selama ini direpotkan dengan aktivitas mengumpulkan iuran dan membeli kado, saya sarankan untuk setop. Kurang lebih saya bilang ke mereka, "mungkin ini saatnya kita _enggak ngasih_ kado lagi," dengan latar belakang lagu Wiz Khalifa, See You Again.

Nikahan teman satu geng bulan Juli 2018 lalu menjadi bukti, kami tak lagi dapat menyumbang kado. Hanya uang dalam amplop berukir nama sang pemberi. Saya kok kadang jadi agak _mellow_ sendiri ketika membayangkan tak beruntungnya teman yang selama ini mencari kado nikahan justru tidak dapat giliran memperoleh kado.

Mas kandung saya bilang, dia dan teman segengnya yang berjumlah tiga orang itu masih berbagi kado nikahan. Selama ini memberi kado atau uang saja adalah tindakan fleksibel. Kalau ada teman lain yang mengajak memberi kado nikahan, dia mengiyakan, kalau tidak ada yang mengajak, ya udah _nyumbang_ uang saja.

Tidak ada waktu pasti kapan kita akan berhenti memberi kado nikahan. Semua tergantung dengan geng, waktu, situasi, dan kondisi. Semua akan nyumbang uang aja pada waktunya.
