---
title: "Remotivi Bicarakan Polarisasi Media dan Independensi di Digitalk #24, Fisipol UGM"
date: "2018-11-23"
categories: 
  - "cuap"
  - "media"
tags: 
  - "cuap"
coverImage: "20181122_151812.jpg"
aliases:
  - /post/2018-11-23-remotivi-bicarakan-polarisasi-media-dan-independensi-di-digitalk-24-fisipol-ugm/
---

Pusat penelitan di Fakultas Ilmu Sosial dan Ilmu Politik, UGM, Center for Digital Society (CfDS), setiap bulan mengadakan diskusi yang membahas beragam isu terkait tren, praktik, dan permasalahan digital di Indonesia dan dunia. Diskusi ini dijuluki dengan Digitalk dan selama ini mereka mengundang pembicara dari beragam latar belakang; kalangan pemerintah, akademisi, aktivis, sampai pegiat dunia digital.

Setelah menilik topik-topik yang pernah dibahas di Digitalk di laman [cfds.fisipol.ugm.ac.id](http://cfds.fisipol.ugm.ac.id/page/18/digitalk), ternyata seru-seru, Bre! Saya kok ya agak _nyesel_, baru tahu acara Digitalk beberapa hari lalu saat Remotivi mengiklankan Digitalk #24. Digitalk pernah mengundang Ndorokakung dan Agus Mulyadi, Kate Walton, Nidya Rahmanita (Konsultan PT Bitcoin Indonesia—​sekarang PT Indodax Nasional Indonesia), dll.

Di hari Kamis, 22 November, Digitalk #24 mengundang peneliti Remotivi, Muhamad Heychael yang beberapa waktu lalu namanya populer karena [amatan kritisnya terhadap Tribunnews](http://www.remotivi.or.id/pantau/466/Bagaimana-Tribunnews-Membantu-Terorisme?) untuk membahas tema _Well-informed Society: Social Media vs Television_. Tak perlu basa-basi, saya kemudian mendaftar. Sebelum itu saya mengontak sang narahubung untuk memastikan kalau orang umum bisa mengikuti acara ini, walau sayang tak terkirim via WA maupun SMS.

Berangkat dengan sepeda sekitar pukul 14.30 WIB. Perjalanan cukup tersendat di perempatan OB (Selokan Mataram), tetapi secara keseluruhan perjalanan _selo_. Melewati depan Fakultas Teknik UNY kemudian belok ke selatan, jalan yang biasa dipakai untuk _sunmor_. Digitalk #24 diadakan di Convention Hall lantai 4, Fisipol UGM. Kalau dari belokan jalan _sunmor_ tadi, ke selatan, kemudian belok kanan (barat) ke Jl Sosio Yustisia—​belokan tepat sebelum bunderan mini UGM. Convention Hall tepat di selatan jalan. Tempat parkir ada di besmen. Pas, saya menyenderkan standar sepeda pukul 15.00 WIB.

Untuk meyakinkan bahwa saya berada di gedung yang benar, saya bertanya kepada seseorang yang baru saja turun dari tangga bersama teman-temannya. Yep, ini adalah Convention Hall dan acara Digitalk diadakan di lantai 4, ruang paling pojok sana (utara), katanya sambil menunjuk. "Ada lift di situ kok, Mas," tambahnya. Saya tidak tahu tombol lift-nya di mana, ya udah, mending saya naik tangga saja. Cuma naik 4 lantai, tak berat. Haha.

Setelah menandatangani daftar kehadiran di samping nama saya dan menerima _voucher_ untuk mengambil makan siang, saya masuk ke ruangan. Belum banyak orang, tapi sudah lumayan lah. Mas Heychael masuk ruangan pukul 15.30 didampingi oleh orang-orang dari CfDS. Pasca basa-basi dari pewara, Project Officer menyambut dan memperkenalkan kerisauan yang memunculkan ide untuk membuka topik diskusi ini. Ia juga sempat menyinggung film yang disutradarai oleh Ucu Agustin, _Di Balik Frekuensi_.

![Ruangan acara Digitalk](/blog/2018/11/images/20181122_145820.jpg)

Muhamad Heychael memulai bahasan dengan menjawab "YouTube lebih dari TV?". Dilihat dari penelitian Nielsen tahun 2017, sebetulnya penetrasi TV masih unggul dari pada media lain. Internet berada di urutan 3. Bahkan, tak sedikit pengguna internet yang menonton TV lewat _streaming_. "YouTube lebih dari TV?", jawabannya "tidak juga".

Tidak banyak hal baru yang disampaikan oleh Heychael yang intinya adalah polarisasi media, polarisasi masyarakat. Dua video Remotivi sebagai salah satu referensi. Video berjudul [_Masih Percaya Media?_](https://www.youtube.com/watch?v=IM1Sx_VJWqY) dan [_Politik Tapi Entertainment_](https://www.youtube.com/watch?v=InY68ZieCeE).

https://www.youtube.com/watch?v=IM1Sx_VJWqY

Hubungan TV dan media sosial dapat dideskripsikan sebagai lingkaran setan. Pejabat A ngomong X di media sosial lalu dikutip oleh TV. Komentar pejabat yang disorot di TV dikomentari oleh pejabat lain. Mirip kasus telur dan ayam, bingung duluan yang mana.

https://www.youtube.com/watch?v=InY68ZieCeE

Media sosial bertebar hoaks dan ujaran kebencian yang membikin dunia barat mulai meminta tanggung jawab perusahaan media sosial tentang kerusakan yang mereka timbulkan. Kasus kemanusiaan di Myanmar adalah salah satu contohnya. Penggagas kekerasan di sana aktif di Facebook. Ini menjadi hal menarik jika pemerintah Myanmar mulai menerapkan sangsi terhadap media sosial besutan Mark Zuckerberg.

Heychael juga menyatakan dukungannya terhadap media komunitas karena media arus utama sudah tidak dipercayai masyarakat akibat polarisasi. Ia memiliki ide ekstrem agar pemerintah mulai menimbang ilmu jurnalistik untuk diperkenalkan sejak SD. Saya jadi ingat dengan ide dukungan terhadap media komunitas yang disampaikan [pendiri dan direktur Remotivi, Roy Thaniago](/blog/2018/09/2018-09-20-diskusi-soal-nasib-media-komunitas-di-antologi-collaborative-space/).

Tak lupa, ia juga membahas kultur jadul 5W1H dalam jurnalisme. Kalau cuma 5W1H, media komunitas atau orang-orang di media sosial saja bisa. Makanya, jurnalisme kiwari juga harus disisipkan dengan analisis dan tambahan konteks (seperti opini, tetapi tak memihak). Praktik metode itu sudah dipakai media luar seperti Washington Post.

Di sesi tanya jawab, saya sangat tertarik dengan satu pertanyaan dari peserta. _Remotivi kan baru saja mendapatkan investor, apakah ia akan tetap independen dan bisa dipercaya?_. Muhamad Heychael menampik, Remotivi mendapatkan kucuran dana (_funding_) bukan investasi. Pengucurnya adalah _Voice_, kalau saya tidak salah dengar. Mereka adalah organisasi nirlaba yang fokus pada urusan keadilan dan kemanusiaan. "Kalau saya bilang Remotivi independen, pasti jadi omong kosong. Jadi, yang menilai Remotivi tetap independen atau tidak, ya kalian. Silakan diriset," paparnya.

Waktu sudah menunjukkan hampir pukul 17.00. Acara ditutup dengan pembagian hadiah kepada penanya terbaik. Saya keluar, mengambil makan siang, dan langsung turun ke besmen, ambil sepeda dan _bablas_. _Getun_, tak berjabat tangan dengan Muhamad Heychael, setidaknya mengucapkan selamat karena Remotivi yang telah bertahan hidup dari ancaman kolaps beberapa waktu lalu.
