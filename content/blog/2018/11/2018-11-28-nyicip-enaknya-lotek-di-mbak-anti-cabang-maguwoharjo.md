---
title: "Nyicip Enaknya Lotek di \"Mbak Anti\" Cabang Maguwoharjo"
date: "2018-11-28"
categories: 
  - "kuliner"
  - "ulasan"
tags:
  - "kuliner"
coverImage: "photo_2018-11-25_12-17-20.jpg"
aliases:
  - /post/2018-11-28-nyicip-enaknya-lotek-di-mbak-anti-cabang-maguwoharjo/
---

Saya bukan tipikal orang yang suka jajan di luar (rumah). Apa-apa pasti makan di rumah, tetapi untuk lotek yang tidaklah umum menjadi makanan rumah, saya pasti makan di luar. Cocok, saat pulang dari urusan perkulakan barang, perut terasa lapar. Saya niatkan untuk mampir ke warung lotek.

Menggenjot sepeda dari arah barat melalui jalan Selokan Mataram, saya belok ke kiri (utara) dan berhenti di pojok perempatan. Ada Warung "Mbak Anti" di situ, khusus menyuguhkan masakan lotek, gado-gado, pecel, dan kupat tahu. Warung ini menghadap ke barat sekaligus ke utara. Pembeli yang datang dari arah timur diuntungkan, karena tinggal _mlipir_. Beda dengan pembeli yang datang dari arah lain, perlu memotong jalan atau setidaknya menyeberang jalan yang lumayan ramai. Jadi, kalau kamu mau ke situ hati-hati yaa.

Warung "Mbak Anti" yang ada di desa Maguwoharjo ini adalah cabang dari warung yang ada di Mirota Kampus UGM alias Terban.

Saya ke warung tersebut sekitar pukul 11.00 WIB, saat itu hanya dua orang pelanggan yang sedang menikmati hidangannya. Warung didominasi warna hijau dan berpenampilan bersih, bahkan kalau diperhatikan terkesan anyar. Dua gerobak; gerobak untuk meracik masakan dan gerobak jus berada tepat di samping jalan. Meja kayu berjejer sebagai tempat kasir dan pajangan cemilan yang dapat dibeli pelanggan. Di sisi lain, ada etalase kaca berisi air mineral botol, lemari pendingin, dan meja lain untuk meletakkan wadah krupuk.

![Meja kasir di warung makan "Mbak Anti"](/blog/2018/11/images/photo_2018-11-25_12-17-10.jpg) Meja kasir "Mbak Anti"

![Suasana di dalam warung makan "Mbak Anti"](/blog/2018/11/images/photo_2018-11-25_12-17-12.jpg) Suasana di dalam warung makan

Beruntung. Mungkin karena saat itu belum ramai pengunjung, pelayanannya sangat cepat. Tidak sampai 5 menit, pesanan lotek plus es teh diantarkan ke meja saya. Kalau dibandingkan dengan warung lotek mini di pinggir jalan, lotek Mbak Anti terlihat tak begitu banyak. Terlepas dari itu, ia tetap lengkap dengan lontong, krupuk, dan irisan bakwan (ote-ote). Pembeli dapat memilih lotek pedas atau biasa (tidak pedas). Saya memilih yang pedas sedang saja, sayangnya rasa lotek justru sama sekali tidak pedas. Lidah sudah terlalu kebal untuk mengecap rasa pedas sayu.

![Lotek dan es teh](/blog/2018/11/images/photo_2018-11-25_12-17-17.jpg) Lotek dan es teh yang telah tersaji

Sambel kacangnya terlalu lembut. Namun tenang, ia tak mengubah rasa, meskipun penggemar lotek yang menyukai tekstur _grenyel-grenyel_ pecahan kacang bisa jadi agak kecewa.

Di pojok meja disajikan tumpukan gorengan tempe, berukuran cukup besar dan lebih tebal dari kartu ATM. Masih anget, krenyes-krenyes, mantul.

![Lotek "Mbak Anti" di atas piring](/blog/2018/11/images/photo_2018-11-25_12-17-15.jpg) Lotek plus gorengan tempe

Total harga lotek, es teh, dan satu gorengan tempe yang saya nikmati adalah Rp14.000. Lumayan terjangkau lah harga segitu. Kalau dilihat di Google Maps, Warung "Mbak Anti" yang ada di Maguwoharjo ini buka setiap hari Senin sampai Sabtu, dari pukul 08.00 sampai 16.00.

Buat yang tertarik berkunjung ke Warung "Mbak Anti" datang saja ke alamat **Jl. Nangka III No.68, Sanggrahan, Maguwoharjo, Depok, Sleman, DI Yogyakarta** atau klik [**tautan lokasi Google Maps ini**](https://www.google.com/maps/place/Lotek+Dan+Gado+Gado+Mbak+Anti/@-7.7687793,110.4244446,15z/data=!4m8!1m2!2m1!1swarung+mbak+anti!3m4!1s0x0:0x24c9f7b1f3ea94e4!8m2!3d-7.7687796!4d110.424445?hl=en).
