---
title: "Menolak Katsueki-gata, Mengapa Kita Harus Donor Darah?"
date: "2018-12-12"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "photo_2018-12-11_23-37-44.jpg"
aliases:
  - /post/2018-12-12-menolak-katsueki-gata-mengapa-kita-harus-donor-darah/
---

![pmi](/blog/2018/12/images/photo_2018-12-11_23-37-44.jpg)

Satu teori yang cukup saya benci adalah teori kepribadian yang dikait-kaitkan dengan golongan darah. Nama bekennya, Katsueki-gata, sebuah sains semu yang populer di kalangan orang Jepang.

Katsueki-gata sebetulnya dimunculkan sebagai reaksi atas stereotipe berbasis etnis yang populer di Eropa. Teori ini disampaikan oleh Takeji Furukawa pada tahun 1927 dan didukung penuh oleh Masahiko Nomi lewat publikasinya yang lahir sekitar tahun 1970. Teori yang awalnya bersifat reaktif itu ternyata menjadi sumber "terpercaya" di Indonesia, seperti ramalan bintang. Di negeri ini, Katsueki-gata menjadi tren (mungkin) karena komik yang ditulis oleh Park Dong Sun yang dirilis secara digital (daring) dan fisik. Seingat saya, linimasa saya penuh dengan akun-akun pembagi kepribadian berbasis golongan darah sekitar tahun 2014 sampai 2016.

Orang-orang jadi hobi mencocok-cocokkan golongan darah dengan kepribadiannya. Banyak yang manggut-manggut. Banyak pula yang hanya menjadikannya sebagai hiburan semata. Padahal kalau _ngomongin_ golongan darah, bakal lebih asyik dan sehat jika menyangkutpautkannya dengan donor darah.

Saya bukan orang PMI. Saya hanyalah seseorang yang sudah tahu manfaat sebagai pendonor sukarela. Jadi, kalau saya bisa dan mau menjadi pendonor darah, mengapa kalian tidak?

Donor darah adalah salah satu cara kita untuk menjaga kesehatan. Ya, itu alasan klasik. Donor darah dapat menyehatkan jantung, menurunkan level zat besi, dan menurunkan risiko kanker. Itu hanya beberapa, manfaat-manfaat kasat mata lain sungguh banyak. Bagi saya pribadi, manfaat terfavorit adalah menjaga berat badan tubuh.

Setiap kali mendonorkan darah sebanyak 500ml, pendonor akan membakar 650 kalori. Berdasarkan pengalaman, berat badan saya selalu stabil, meskipun kadang makan mi instan malam-malam. Selain mendonorkan darah, mungkin juga karena saya menyisihkan minimal satu jam setiap pekan untuk bersepeda. Adanya peraturan "baru" yang mengizinkan pendonor untuk mendonorkan darahnya selama 60 hari sekali benar-benar sangat membantu. Percayalah! _Eit_, tetapi jangan sekali-kali menjadikan donor darah sebagai program diet lho ya.

Manfaat favorit lain adalah mencegah stroke. Salah satu penyebab stroke adalah darah tinggi. Dengan donor darah, saya menjadi lebih pintar mengolah emosi. Terutama bagi yang merasa kesulitan mencurahkan hati, aktivitas donor darah ini adalah cara sederhana yang hakiki. Apa hubungannya? Pikiran penat dan penuh amarah adalah salah satu faktor pemicu tekanan darah tinggi. Oleh karena itu, _mboh pye carane_, saya harus mempertahankan tekanan darah demi melakukan donor darah, entah dengan cara tidak begadang, lebih _nrimo_, lebih banyak menulis, membaca, dan sebagainya.

Manfaat eksternalnya, donor darah adalah cara termurah untuk membantu sesama manusia. Cuma modal badan sehat, darah, dan uang transpor, tidak perlu berkelahi seperti Batman. Uang transpor itupun relatif, bisa banyak, bisa sedikit. Kalau rumah kita jauh dari PMI, kita bisa menghemat uang transpor dengan cara mengikuti donor darah dalam sebuah acara atau di rumah sakit yang rutin menyelenggarakan acara donor darah, seperti JIH (Jogja International Hospital), yang lokasinya dekat rumah. Info seputar acara penyelenggaraan donor darah di luar PMI dapat dicari melalui laman [ayodonor.pmi.or.id](http://ayodonor.pmi.or.id/mobileunit.php). Selain itu, setelah melakukan donor darah kita juga akan mendapatkan cemilan. [Lumayan mengenyangkan …](/blog/2017/12/2017-12-02-makanan-favorit-dari-pmi-yogyakarta-oleh-oleh-donor-darah/)

### Indonesia masih kekurangan stok darah

Ya. Negeri berjuta-juta penduduk ini ternyata belum memenuhi jumlah minimal stok darah sesuai arahan WHO. Target WHO jumlah donor darah sukarela adalah 2 persen penduduk atau setara dengan 4-5 juta kantong darah pertahun bila dikonversi ke jumlah penduduk Indonesia. Berdasarkan laporan pada bulan [September 2018](https://beritagar.id/artikel/berita/indonesia-masih-kekurangan-stok-darah), kebutuhan darah nasional sebanyak 5.174.1000 tetapi UTD (Unit Transfusi Darah) baru dapat menghasilkan 4.201.578 kantong. Berarti masih perlu 972.522 kantong lagi, meskipun [kalau dirata-rata](https://tirto.id/defisit-stok-darah-yang-tak-kunjung-terpecahkan-bn3V) pertahunnya Indonesia masih kekurangan 1,3 juta kantong.

Jadi, mau nunggu apa lagi?

Dapet sehat? iya.

Dapet cemilan? iya.

Dapet cakep? bisa iya, bisa tidak.

Yok donor darah, singkirkan sains semu Katsueki-gata.

_Biar kayak promo-promo gitu, padahal mah juga enggak di-endorse PMI._
