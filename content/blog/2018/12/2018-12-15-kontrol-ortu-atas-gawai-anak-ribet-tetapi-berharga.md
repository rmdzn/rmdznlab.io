---
title: "Kontrol Ortu Atas Gawai Anak, Ribet tetapi Berharga"
date: "2018-12-15"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "anak-kecil-ponsel-rmdzn.jpg"
aliases:
  - /post/2018-12-15-kontrol-ortu-atas-gawai-anak-ribet-tetapi-berharga/
---

![anak kecil ponsel](/blog/2018/12/images/anak-kecil-ponsel-rmdzn.jpg)

Kerabat yang tinggal di sebelah rumah, khitan. Ia masih berusia 10 tahun. Sebagai "hadiah", orang tuanya membelikan ponsel Android (_yeah, it sucks!_). Aku diserahi ponsel itu sebentar untuk di-_setting_ sini situ, agar dapat dipakai untuk menonton (video) YouTube.

Ponsel tersebut memang belum membawa aplikasi YouTube. Ketika aku ingin memasangnya lewat Play Store, perangkat bermerek Advan itu ternyata juga belum didaftarkan ke akun Google. Oke kalau begitu, segeralah kubuatkan akun di ponsel. E .. tapi, aku baru tahu, ternyata sekarang kita dapat membuat akun sebagai diri sendiri atau sebagai anak. Fitur yang disebut sebagai Google Family Link ini dikenalkan [Maret 2017](https://blog.google/technology/families/introducing-family-link-app-helping-families-navigate-technology-together/) lalu. Kalau mendaftar sebagai diri sendiri, itu biasa seperti yang kita lakukan secara normal. Kalau mendaftar sebagai anak, kita berperan sebagai orang tua yang membuatkan akun sang anak.

Berapa batas usia untuk dibilang sebagai anak di situ? Pokoknya yang usianya di bawah 13 tahun.

Cukup tahu kan, kalau orang tua sekarang tidak mau ribet, apalagi mereka yang tinggal di daerah pedesaan atau setidaknya di pinggiran kota (belum masuk kota tetapi bukan desa) dan tak begitu paham teknologi. Karena pikiran itu, aku putuskan untuk menjadikan surelku sebagai akun orang tua sang anak. Lumayan, icip-icip kalau besok punyak anak beneran.

_Peringatan_: Ini bukan tulisan tutorial. Jadi, jangan harap akan banyak tangkapan layar (_screenshot_) runtut di sini.

Pendaftaran surel untuk anak luar biasa ribetnya. Intinya, buat identitas surel anak (seperti pendaftaran surel biasa) lalu masukkan surel orang tua dan konfirmasi dengan kata sandinya. Setelah itu, sebagai "orang tua" kita diberi pilihan untuk memasang aplikasi Google Family Link di ponsel keduanya ([orang tua](https://play.google.com/store/apps/details?id=com.google.android.apps.kids.familylink) dan [anak](https://play.google.com/store/apps/details?id=com.google.android.apps.kids.familylinkhelper)) atau di ponsel anak saja.

Setelah memasang Google Family Link (anak) di ponsel itu, kita sebagai orang tua masih perlu mengatur beragam opsi untuk mengoptimalkan fitur yang disediakan. Misalnya, fitur kontrol pada Google Play untuk membatasi instalasi aplikasi dewasa/umum (YouTube masuk kategori ini), memeriksa apa saja aplikasi yang dibuka sang anak, lama waktu membukanya, sekaligus membatasinya, mengatur secara remot kapan sang anak harus istirahat dari melihat layar, serta mengaktifkan filter anti situs dewasa pada peramban Chrome.

Aku belum mencoba fitur pengawasan pada ponsel untuk anak kecuali fitur kontrol pada Google Play. Ketika akan memasang YouTube, pemilik ponsel (sang anak) harus meminta izin kepada orang tua, baik melalui _tanya langsung_ maupun _tanya dulu_. Ketika memencet _tanya langsung_, kotak dialog untuk memasukkan kata sandi surel orang tua muncul untuk segera diinput, sedangkan saat memencet _tanya dulu_, ponsel orang tua akan memunculkan kotak dialog apakah sang anak diizinkan untuk memasang YouTube. Kotak dialog terakhir ini akan muncul tiba-tiba, entah saat orang tua sedang ber-_chatting_ ria atau (mungkin) bermain gim.

![Kotak dialog persetujuan untuk memasang YouTube](/blog/2018/12/images/photo_2018-12-15_19-51-38-fix.jpg) Kotak dialog persetujuan untuk memasang YouTube

![Setelan fitur Google Family Link](/blog/2018/12/images/photo_2018-12-15_19-52-08.jpg) Setelan Google Family Link

Lalu, apakah aku melanjutkan eksperimen itu? Tidak. Itu terlalu ribet. Aku tak dapat membayangkan ketika kerabatku yang masih kecil itu harus memasang aplikasi atas seizinku, bukan seizin bapaknya, atau ibunya. Apalagi setelah aku cek aplikasi yang terpasang dan beberapa riwayat aksesnya, ponsel itu juga dipakai oleh sang ibu. Riwayat pencarian _tas wanita_ di aplikasi Tokopedia, misalnya.

Walau begitu, aku memiliki testimoni sederhana:

**Ribet. Namun berharga.**

Ya, berharga.

Supaya eksplorasi anak atas teknologi tidak liar, orang tua perlu terjun langsung mengawasi ke manapun anaknya menyentuh layar gawai, terutama pada anak seusia 13 tahun ke bawah. Mereka adalah usia rentan. Usia 13 tahun ke bawah belum memiliki pijakan tepat untuk menentukan keputusan dalam dunia daring. Mereka mungkin jago mengutak-atik teknologi dibanding para tetua, tetapi struktur kognitif guna mengaktifkan pikiran kritis dan etis mereka sebetulnya belum terbentuk. Mereka berpotensi menjadi korban perundungan dan korban hal negatif lebih tinggi daripada usia-usia di atasnya.

Hampir semua media sosial melakukan pembatasan. Hanya anak berusia 13 tahun ke atas yang boleh membuka akun Facebook, Twitter, Tumblr, Pinterest, Snapchat, dll. Seperti dilansir [Huffington Post](https://www.huffingtonpost.com/diana-graber/3-reasons-why-social-media-age-restrictions-matter_b_5935924.html) tahun 2014 yang mengutip hasil studi knowthenet.org.uk, 59 persen anak sudah memiliki akun jejaring sosial (media sosial) saat berusia 10 tahun. Facebook tertinggi, sebanyak 52 persen anak berusia 8 sampai 16 tahun mengaku telah mengabaikan batasan umur—mendaftarkan diri dengan tahun kelahiran palsu.

Di Indonesia, angka itu bisa saja sama, bisa saja lebih tinggi. Seorang anak perempuan sekitar kelas 3 atau 4 SD memamerkan kalau dia punya akun Instagram, dan mengekspresikan wajah aneh saat kuceritakan bahwa aku tak memilikinya (Hei, [@sintaskata](https://www.instagram.com/sintaskata) itu ruang puitis !!!111!!1).

Aku bukan penggemar sebuah kultur yang menganggap anak-harus-diperkenalkan-dengan-gawai-sejak-dini-agar-tidak-gaptek tetapi aku tetap yakin, seyakin-yakinnya, Family Link adalah fitur paling layak disodori empat jempol. Google sadar, tidak sedikit anak kecil yang menjadikan ponsel Android sebagai media hiburannya oleh karena itu demi menjaga kewarasan sang buah hati dari pengaruh negatif, fitur _parental control_ adalah fitur kelas pertama yang mereka kenalkan ke khalayak.

Meskipun kalau bicara realita di lingkungan keluarga Indonesia, sisi positif yang besar itu ternyata dibarengi dengan sisi negatif yang juga tak kalah besar. Siapa _to_ yang mau dan sudah repot-repot menghidupkan fitur Family Link di ponsel anaknya? Kaum urban belum tentu peduli, apalagi kaum pinggiran. Yang penting anak diam. Yang penting anak senang.

_Haish, susah._
