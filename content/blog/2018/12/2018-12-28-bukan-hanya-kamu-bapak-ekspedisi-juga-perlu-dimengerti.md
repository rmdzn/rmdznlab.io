---
title: "Bukan Hanya Kamu, Bapak Ekspedisi Juga Perlu Dimengerti"
date: "2018-12-28"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "surat-rmdzn.jpg"
aliases:
  - /post/2018-12-28-bukan-hanya-kamu-bapak-ekspedisi-juga-perlu-dimengerti/
---

![surat](/blog/2018/12/images/surat-rmdzn.jpg)

Rumah saya tepat di tusuk sate yang kebetulan berbentuk warung kelontong. Jalan alternatif yang sekaligus jalan kabupaten depan rumah itu cukup ramai. Manusia-manusia bermotor dan bermobil ingin menuju ke Jl. Solo tanpa takut macet sambil mampir membeli permen, rokok, atau di kala lapar, mampir beli bakso dan mi ayam. Dua barang terakhir bukan saya yang jual, tetapi tetangga sebelah. Kampung tempat saya tinggal juga diramaikan anak indekos dan kontrakan. Putra dan putri. Dari mahasiswa sampai para pekerja.

Suatu hari, seorang Pak Pos mampir warung. Bukan mau beli teh botol atau permen, tetapi ingin menanyakan rumah si A untuk mengantarkan paket. "_Mas, niki leres kampung ABCD, RT XX, nggih? (Mas, ini benar kampung ABCD, RT XX, ya?)_," tanya beliau. "_Daleme bu A pundi nggih, Mas? (Rumahnya bu AA di mana ya, Mas?)_."

"_Leres pak, dalemipun mriku pak. (Benar pak, rumahnya di situ pak)_," jawab saya.

Bapak-bapak ekspedisi memang sering mampir rumah ketika mereka kebingungan dengan alamat yang dituju. Bapak Pos Indonesia, JNE, Tiki, J&T, dan Wahana pernah mampir semua. Kalau alamat yang ditanyakan jelas, tetapi nomor rumah dan nama orang tidak hafal, saya tinggal menanyakan ke tetangga penjual bakso. Beliau ini mantap, kalau soal tunjuk-menunjuk lokasi rumah tetangga, beliau hafal luar biasa.

Sebaliknya, tidak jarang bapak ekspedisi terlihat sebal (begitu juga saya) kalau nama dan alamat paket yang dituju tidak jelas. Kalau bapak penjual bakso tidak paham dengan alamat yang dimaksud, apalagi saya. Kami memilih pasrah dan menyampaikan kepada bapak ekspedisi kalau alamat tersebut memang tidak jelas.

Contoh dari "alamat yang tidak jelas"; nama kampung benar tetapi nomor RT salah. Nama kampung dan nomor rumah ada, tetapi nomor RT tidak ada. Nama jalan ada lengkap dengan nama desa, tetapi nama kampung atau nomor RW justru tidak ada, serta kombinasi ketiadaan lainnya.

Lebih jelasnya, seperti ini. Alamat lengkap _Jl. Garuda 36, Waringin RT 07/60, Desa Marwah, Kecamatan Kosidahan, Kabupaten Parapan, Provinsi Jawa Tengah_ atas nama _Royjudin_ (itu alamat ngawur, jangan dicari). Contoh dari kasus pertama adalah _Jl. Garuda 36, Waringin RT 09/60 dst_ sedangkan kasus pertama adalah _Jl. Garuda 36 RT 07, Desa Marwah, Kecamatan Kosidahan dst_. _Kan, nyebelin_.

Itu mending, _lah_. Ada yang pernah mencari alamat tetapi hanya menyebutkan nama jalannya saja. Nama kampung, RT/RW, nama desa, sama sekali tidak tahu. Kalau di kasus ini, penanyanya bukan bapak ekspedisi. Entah, mungkin hanya orang yang ingin bertamu. _Kan, nyebelin (2)_.

Sebetulnya, informasi berupa nama tujuan dan alamat tujuan merupakan informasi yang saling melengkapi. Namun unsur pertama tidak begitu penting dibandingkan dengan unsur kedua. Selama alamat ditulis lengkap, siapapun nama yang tertulis tidak berarti. Bisa saja, paket itu ditujukan memang bukan untuk Royjudin tetapi untuk ponakannya, Sari, kakaknya, Martono, atau anggota keluarga lainnya. Kalau di kampung saya, nama asing kerapnya merujuk nama penghuni kontrakan.

Zaman dahulu setelah Majapahit tumbang, saat surat dengan lontar kertas masih jadi idola, alamat tujuan surat yang tidak ketemu akan dikembalikan oleh pihak Pos Indonesia kepada pengirim. Zaman sekarang mungkin masih begitu, saya kurang tahu. Terlepas dari itu kok ya masih ada orang-orang yang menulis alamat dengan kacau. Padahal itu adalah aktivitas yang gampang.

Saya memiliki teman yang beberapa tahun lalu hidup di indekos. Siang hari, saat saya main ke tempatnya. Saya melihat tulisan runtut alamat indekos, lengkap dengan nama jalan, nama kampung, nomor RT dan RW, nama desa, kecamatan, nama kabupaten, hingga nama provinsi di kertas yang ditempel di dinding kamar. Saat saya tanya buat apa, dia menjawab, itu buat belanja daring. Saya akui, itu adalah sisi praktis yang tak banyak orang lakukan.

Saya bukan anak indekos, tetapi saya berusaha mengambil pelajaran berharga dari teman itu untuk saya bagikan. Kepada penghuni indekos dan kontrakan, usahakan mencatat alamat lengkap rumah yang ditinggali. Kalau masih malu bertanya kepada teman di kamar sebelah, tanyai saja bapak/ibu pemilik indekos atau kontrakan. Biarpun kamu merasa tidak akan belanja secara daring, itu akan sangat berharga ke depannya. Percayalah.

Kepada penghuni asli kampung, tidak salah kalian kamu menyimpan daftar nomor tetangga atau denah kampung. Siapa tahu rumah situ strategis, sering jadi rujukan bapak-bapak ekspedisi yang ingin bertanya alamat.

Yakinlah, dua hal sederhana itu akan sangat membantu bapak-bapak ekspedisi baik secara langsung maupun tidak langsung. _Emang kamu aja yang perlu diperhatiin? Bapak ekspedisi juga perlu lah_.
