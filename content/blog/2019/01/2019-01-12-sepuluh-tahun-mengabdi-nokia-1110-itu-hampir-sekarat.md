---
title: "Sepuluh Tahun Mengabdi, Nokia 1110 Itu Hampir Sekarat"
date: "2019-01-12"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "nokia-1110-rmdzn.jpg"
aliases:
  - /post/2019-01-12-sepuluh-tahun-mengabdi-nokia-1110-itu-hampir-sekarat/
---

Tahun 2008, ponsel pertama yang kupegang dengan sematan "ponsel milik sendiri" setelah satu tahun sebelumnya pegang ponsel ibu sebagai media untuk menghubungi bapak kalau aku sudah pulang sekolah, sekarat, atau boleh dibilang "hampir".

Ponsel itu ponsel Nokia 1110. Ponsel _second_ yang dibeli dari seorang saudara perempuan. Nokia 1110 lahir tahun 2005, paling tidak ponsel itu sudah dipakai selama tiga tahun sebelum saya gunakan sehari-hari. Aku "dihadiai" ponsel itu bukan karena kebelet ingin main _Snake Xenzia_ tetapi karena kebutuhan komunikasi dengan anggota keluarga di rumah--aku bersama kawan-kawan satu angkatan kelas VIII (kelas 2 SMP) _study tour_ ke Bandung.

Di masanya, ponsel Nokia 1110 tak begitu _wah_ karena tak sedikit ponsel sekelas Nokia N95 terlihat menyembul dari balik tangan teman di bangku/meja sebelah. Aku _gampangan_, asal bisa SMS dan telepon, ponsel itu sungguh paripurna. Tidak peduli dengan gim Asphalt maupun musik/video Avanged Sevenfold yang dimainkan di samping kanan dan kiri.

Sampai tahun 2015 akhir, aku masih setia menggunakan Nokia 1110. Satu dekade sejak kelahiran atau tujuh tahun sejak kupakai, baterai tidak nge-_drop_. Mungkin nge-_drop_, tetapi tidak terjun bebas. Pertama pakai, baterai mampu bertahan sekitar satu minggu sampai hampir dua minggu. Tahun  2015 pakai, baterai mampu bertahan selamai empat hari sampai seminggu. Lumayan, kan? Aku tak yakin, orang lain dapat memiliki ponsel seawet itu (mode _songong_).

Desember 2015, aku resmi menggunakan ponsel Android. Bukan berarti si Nokia 1110 pensiun, ia tetap kupakai untuk jualan pulsa. Setengah tahun terakhir, ponsel akhirnya menemui masalahnya. Setiap terbentur dengan bidang yang keras sedikit saja, ponsel akan mati. Baterai pun hanya bisa hidup selama maksimal dua hari. Hebatnya--hingga aku kagum sendiri--baterai yang masih ori itu tidak terlihat begitu menggelembung.

![Nokia 1110](/blog/2019/01/images/nokia-1110-rmdzn.jpg)

Tampilan ponsel sudah berubah dibanding saat baru dibeli. _Casing_ diganti dengan _casing_ murah. _Casing_ asli yang [berwarna biru tua](http://content-cms.mobile88.com/gen/photos/Nokia/1110/p1000550_b.jpg) masih tersimpan rapi di wadahnya. Dulu _eman-eman,_ katanya, kalau pakai _casing_ ori, mending diganti dengan _casing_ KW yang warnanya mudah luntur. Bagian pojok _casing_ sudah _cuil_ karena berulang kali terbentur lantai.

Kepada Nokia jadul, semoga mampu bertahan. Kamu membawa kenangan dari Bandung, kamu membawa kenangan dari tugas beruntun semasa SMP, kamu membawa kenangan masa-masa menjadi aktivis di SMA, kamu sangat berharga.
