---
title: "Kuharap Senyummu"
date: "2019-01-20"
categories: 
  - "cerpen"
tags:
  - "cerpen"
coverImage: "jalan-gang-rmdzn.jpg"
aliases:
  - /post/2019-01-20-kuharap-senyummu/
---

Aku bekerja sebagai _waitress_ di sebuah kafe di Yogyakarta. Menulis pesanan dan mengantarkannya kepada pelanggan adalah tugasku sehari-hari. Itu sudah kulakukan selama 2 bulan sejak kelulusan SMK beberapa bulan lalu.

Ke kafe, saat ingin hemat ongkos, aku bersepeda. Saat ingin menghemat waktu, aku memilih naik motor _grand_ tua warisan bapak. Tak beda jauh sebenarnya. Perjalanan dari rumah ke kafe dengan sepeda memakan waktu 10 menit. Pakai motor hanya sekitar tiga menit lebih cepat. Beda signifikannya hanya pada sumber bising yang kudengar.

Oiya, aku masih jomblo ... eh ... _single_. Menjadi _waitress_ di kafe membuatku memiliki kesempatan banyak untuk mencari target gebetan pelanggan yang wira-wiri dengan unyunya. Siapa tahu cocok. Terlihat remeh. Iya, sih. Maafkan. Itu hanya tujuan sekunder. Tujuan utamanya _mah_ tetap cari duit.

Perjalanan menuju kafe dari rumah sangat syahdu. Melewati sawah. Melewati angkringan yang saat malam terdengar riuh rendah penikmat jajanan. Melewati tukang cukur rambut yang kadang kala mengeluarkan bunyi benturan gunting. Pada akhirnya melewati jalan raya yang ramai, tepat di depan kafe. Jalan pulang tak jauh berbeda. Yang berbeda hanya saat melewati gang berkonblok di kampung, sekitar 100m dari rumah.

Seringnya, giliranku, pulang habis Magrib atau habis Isya. Kalau dapat giliran masuk agak siang, pulangku pukul 22.00 ke atas. Saat pulang habis Magrib, aku sering berpapasan dengan anak indekos perempuan, jalan kaki. Saat kutanya para pemuda desa di warung burjo, yang sering berpapasan denganku adalah mahasiswi. Sekarang lagi magang di sebuah hotel. Semampai. Mungkin, dia 5cm lebih tinggi dariku. Sebagai pemagang di hotel, bajunya elegan ala-ala wanita karier. _Make up_\-nya tidak tebal, manislah pokoknya.

Di kala pergi ke minimarket pagi hari, aku melihat dia belanja. Mukanya memang jutek, tetapi kalau senyum, manisnya minta ampun. Cara bicara dan gerak-geriknya juga elegan, bukan tipikal anak urakan. Mungkin gara-gara itu juga, perempuan itu menjadi idola pemuda dalam waktu sekejap saja ... termasuk aku. Namun, ada sisi minderan yang kurasakan saat bertemu dengannya. Sungkan mau menyapa. Takut dicuekin. Saat berpapasan di jalan pulang dari kafe pun, saya kebanyakan menunduk. Mau mendongak melempar senyum, malu.

Suatu hari, seingatku hari Rabu bulan Desember 2018, aku bersepeda pelan di gang. Sepuluh meter di depanku, perempuan itu jalan kaki mengenakan seragam hotel berwarna hitam. _Dag ... dig ... dug_. Ingin kusapa, tetapi kok yo malunya. _Hih_! Aku berusaha dengan bersusah payah, kupanggil "_mbak_" agak lirih. Tapi karena malu, aku tak menengok dan bablas belok ke rumah. Ya ampun! Batinku.

Di hari berikutnya, aku berpapasan lagi. Aku masih berpakaian sama begitu juga perempuan itu. Mungkin karena di hari sebelumnya aku menyapa dan langsung belok ke jalan, ia belum sempat mengenaliku (tidak mau merasa GR juga _sih_). Saat aku berusaha melihat wajahnya sepintas, wajahnya datar, seperti biasa. Ya udah deh, lebih baik aku tetap melanjutkan perjalanan ke rumah meskipun hati berontak dan mengutuk.

Tiga hari berikutnya aku tak berpapasan karena pulang hampir larut malam. Saat berpapasan lagi, sama saja. Tak ada hal yang berbeda. Tak ada keberanian dalam diriku.

Saat itu hari Senin di bulan yang sama. Aku pulang setelah azan Magrib. Berharap bertemu dengan perempuan itu lagi. Sayangnya tidak. Gang itu sepi. Tidak ada aktivitas sama sekali dari warga sekitar. Mungkin lagi libur, kataku kepada diri sendiri. Hari Jumat, 14 Desember, aku tidak berpapasan lagi begitu juga di hari Sabtu.

Sebagai informasi saja, gang itu sepi tetapi tak begitu gelap, masih ada dua sampai tiga lampu terpasang di pinggir jalan atau teras rumah.

![Jalan gang](/blog/2019/01/images/jalan-gang-rmdzn.jpg) Jalan gang yang kumaksud

Minggu, harinya anak-anak muda nongkrong di kafe, justru menjadi waktuku bekerja. Saat pulang aku tak mengharap bertemu dengan perempuan itu, karena kutahu, saat itu mungkin masih hari libur untuknya. Ternyata tidak. Dia berbelok dari tikungan menuju gang yang biasa kita lewati. Larinya agak cepat, sedikit lari kecil. Lagi keburu mungkin, batinku. Langkahnya melambat. Aku juga melambat, memberikan kesempatan hati dan mulutku untuk bersiap-siap. Ternyata langkahnya makin lambat, seolah-olah ia yang justru berusaha untuk menyapaku duluan. Beruntung!

Hatiku super _dag dig dug dong_. _Suer_. Ia melambat, melambat, melambat, menengok kepadaku. Aku cukup percaya diri ikut mengalihkan perhatian ke wajahnya. Ekspresiku langsung kaku. Orang itu wajahnya familiar tapi kedua matanya melotot hampir melingkar. Pupil matanya bundar di tengah. Perlahan senyum. Ada sobekan kecil pada sisi kanan dan kiri mulut. Senyumnya makin melebar aneh. Membuka sedikit-sedikit. Warna daging merah muncul di bagian samping saat ia tersenyum membuka mulut. Mata yang tadi melotot, makin menyipit. Bagian dahi dan pipi ada bercak merah. Aku tidak tahu itu apa. Aku langsung melengos, mempercepat pedal sepeda, terbirit. Aku tak mau menengok ke belakang lagi. Copot sepatu, ganti baju, dan menutup diri di kamar.

Paginya, kata ibu indekos perempuan itu, selama seminggu terakhir, ia mudik ke rumahnya di daerah Jawa Timur. Aku merinding. Aku tidak ingin lewat jalan itu lagi dan melihat perempuan itu lagi. Titik.

> Terinspirasi dari teman. Lokasi dan waktu berdasarkan suasana nyata.
