---
title: "Mlipir Jalan Samping (Calon) Bandara NYIA"
date: "2019-01-26"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "screenshot_2019-01-26-ground-breaking-nyia.jpg"
aliases:
  - /post/2019-01-26-mlipir-jalan-samping-calon-bandara-nyia/
---

![ground breaking nyia](/blog/2019/01/images/screenshot_2019-01-26-ground-breaking-nyia.jpg)

Bandar udara NYIA (_New Yogyakarta International Airport_) adalah proyek raksasa yang diinisiasi dengan pembebasan lahan pada tahun 2016 di Temon, Kulonprogo. Pembebasan lahan rampung pada pertengahan 2018 dilanjutkan dengan dimulainya konstruksi beberapa pekan kemudian.

Bulan Juli 2018 lalu, keluarga Noerita jalan-jalan ke pantai Glagah. Niat awalnya sebenarnya ke hutan bakau di sebelah pantai Congot tetapi karena tidak ingin _rempong-rempong_ naik perahu, akhirnya kami pergi saja ke pantai Glagah. Kami hanya mampir di rumah makan, menikmati masakan laut sembari menghirup udara di samping laguna dan menonton wisatawan wara-wiri menaiki perahu dengan anak kecil duduk di pucuk depan bak Monkey D. Luffy.

Calon bandara NYIA akan berdiri tepat di samping jalan sepanjang pantai Glagah sampai Congot. Dari utara hingga belok ke timur, menyisir Glagah, NYIA benar-benar mepet jalan. Sebelum pantai Congot, jalan membelok ke selatan sehingga bandara tak lagi mepet karena dibatasi dengan tambak.

Saya tertarik merekam lahan NYIA dari balik kaca penumpang yang terbuka. Merogoh tas, menghidupkan Samsung WB35F, dan memencet tombol rekam. Sambil sesekali _ngobrol_ dengan anggota keluarga lainnya.

https://www.youtube.com/watch?v=vp5Ugeipal8

Lahan itu dibatasi pagar. Beberapa titik dipasang spanduk dukungan pendirian NYIA. Di beberapa lokasi, masih ada bangunan di balik pagar yang belum dirubuhkan, termasuk rumah berwarna kuning itu yang saya ingat adalah kantor urusan desa setempat.

Di selatan jalan, di balik kamera, masih cukup banyak warung-warung makanan berdiri. Begitu juga lahan bertanam buah-buahan yang sudah kering kerontang. Saya yakin, pinggir jalan nanti akan ditanami banyak pohon cemara sebagai area penghalau tsunami. Di koran Kedaulatan Rakyat edisi Jumat, 25 Januari lalu, Bupati Kulonprogo, Hasto Wardoyo menegaskan, selatan bandara tidak boleh dibuat hunian apapun, termasuk warung. Sepanjang area itu akan ditanami pohon Cemara.
