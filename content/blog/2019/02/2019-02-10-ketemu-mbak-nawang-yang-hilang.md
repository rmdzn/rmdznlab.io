---
title: "Ketemu Mbak Nawang yang Hilang"
date: "2019-02-10"
categories: 
  - "cuap"
tags: 
  - "cuap"
draft: true
coverImage: "photo_2019-02-10_15-16-32.jpg"
aliases:
  - /post/2019-02-10-ketemu-mbak-nawang-yang-hilang/
---

_Ruang yang dingin di sisi,_ _riuh manusia menghangatkan permadani,_ _dua manusia berbagi hati,_ _sisa yang sepadan ikut bersaksi~_

_FYI_, itu bukan penggalan puisi yang ditulis oleh mbak itu.

Pada hari Sabtu, 9 Februari 2019, [Mediakita](https://mediakita.com/) dan Gramedia menyelenggarakan _Meet and Greet_ bersama Nawang Nidlo Titisari (Nawang) dan Brian Khrisna (Br) di Gramedia Sudirman. Ini adalah acara _Meet and Greet_ ke sekian setelah sebelumnya dilakukan di kota-kota lain bersama penulis Mediakita yang lain pula.

_Meet and Greet_ ini merupakan acara pertama yang saya kunjungi. Bukan karena tak minat mengikuti acara serupa sebelumnya, tetapi karena saya sering ketinggalan informasi atau karena tidak terpilih menjadi peserta acara. Misalnya, penerbit [Aroma Karsa](/blog/2018/06/2018-06-26-bersama-aroma-karsa-dari-dee-lestari/) pernah mengadakan _gathering_ di Yogyakarta. Pesertanya terpilih karena telah menjadi pembaca digital padahal saya baru beli bukunya beberapa bulan setelah itu.

Menurut jadwal, acara _Meet and Greet_ di Gramedia Sudirman diadakan pukul 14.00 sampai 16.00 WIB. Saya berangkat ke perempatan Kotabaru itu sekitar pukul 13.30 dari rumah. Karena pas ramai-ramainya, motor harus diparkir di depan Dunkin' Donuts. Timur gedung, sebelah tempat keluar kendaraan.

<blockquote class="twitter-tweet"><p dir="ltr" lang="in">Jangan lupa, besok Jogja (Gramedia Sudirman) akan kedatangan <a href="https://twitter.com/briankhrisna?ref_src=twsrc%5Etfw">@briankhrisna</a> sama <a href="https://twitter.com/nanidlot?ref_src=twsrc%5Etfw">@nanidlot</a> <a href="https://t.co/XaPKcnQ8xa">pic.twitter.com/XaPKcnQ8xa</a></p>— #DongengYangTakUtuh (@mediakita) <a href="https://twitter.com/mediakita/status/1093783365906354176?ref_src=twsrc%5Etfw">8 Februari 2019</a></blockquote>

_Meet and Greet_ diadakan di lantai tiga, tempat ribuan buku terpampang di rak maupun papan. Panggung sederhana menghadap ke timur. Logo Mediakita menghiasi latar belakang, satu kursi pendek sedikit menyerong dan satu kursi panjang (cukup untuk dua orang) terpampang di depannya. Meja dengan tumpukan buku _Hilang_ dan _This is Why I Need You_ berada di antara kursi itu dan karpet merah yang saat itu dijejali para perempuan. Ya, saya baru sadar. Mayoritas penggemar kedua buku dan penulisnya adalah perempuan. Sampai acara rampung memang terlihat laki-laki (_btw, ini termasuk saya, wekeke~_), tetapi mungkin tidak sampai sepertiganya penonton perempuan.

![Keramaian di depan panggung](/blog/2019/02/images/photo_2019-02-10_14-54-16.jpg) Penonton/pengunjung Meet and Greet

![Penonton duduk di atas karpet depan panggung](/blog/2019/02/images/photo_2019-02-10_14-54-08.jpg) Penonton/pengunjung Meet and Greet

Nawang adalah penulis _unyu_ buku _Hilang_. Buku berisi sajak-sajak tentang perpisahan dan mencintai sendirian ([lihat ulasan bukunya di blog ini](/blog/2019/02/2019-02-14-menikmati-kehilangan-dengan-hilang/)). Saat ditanyakan apakah tulisan-tulisan di buku itu pengalaman pribadi, _tidak,_ katanya. Itu tulisan berdasarkan kisah teman yang curhat kepadanya. Ironi memang. Mbak satu ini justru merasakan kehilangan bukan saat menulis buku _Hilang_, tetapi saat buku itu sudah terbit.

Nawang menulis karyanya pertama kali lewat blog dan akun twitter. Buku menjadi pelabuhan berikutnya untuk berkarya secara lebih besar. Saat memutuskan untuk menulis buku, ia sama sekali tidak terbayang judul yang cocok. Ketika ditanyai oleh pihak Mediakita, ia menjelaskan bahwa bukunya tentang kehilangan.

![Nawang, Brian, dan moderator duduk di atas kursi](/blog/2019/02/images/photo_2019-02-10_14-54-00.jpg) Nawang dan Br di panggung acara

"Hilang" kemudian menjadi judul pilihan. Namun masih ditandai sebagai "tentatif"--masih bisa diganti. Meskipun akhirnya Nawang tak menyangka kalau _Hilang_ dipertahankan hingga terbit.

Ia, sebagai perempuan, lebih banyak malu untuk terang-terangan mengutarakan perasaannya dan menulis adalah solusi, aktivitasnya untuk bebas bercerita kepada orang-orang. Kalau dipertengahan tulisan buntu (mengalami _writer's block_), _istirahat dulu_, kata perempuan berkaos _apa liat-liat?_ itu.

Apakah ia ingin menulis buku Hilang-hilang yang lain? _Siapa sih mbak, yang terus-terusan pengin kehilangan?_ Kata Nawang, menjawab pertanyaan si pewara (_Karung, mana karung?_).

Jawaban lebih serius, Nawang ingin menulis lagi dengan konteks yang lebih kompleks. Bukan seperti ini (_Hilang_)_._ Misal ada tokohnya gitu. Mungkin seperti novel pada umumnya? Mungkin.

Kalau dalam sebuah penokohan film, mbak Nawang ini jadi tokoh favorit. Dia adalah penulis buku _best_ _seller_ yang masih saja grogi ketika ngomong di depan orang ramai. Saya tetap salut, dong! Kegrogiannya malah jadi keimutan sendiri. Beberapa kali saya tersenyum dan terkekeh melihat responnya terhadap pertanyaan yang diajukan pewara, penonton, maupun rekannya, Br. _Lovely bat_ ...

Brian Khrisna (Br) beda lagi. Ia adalah penulis buku _This is Why I Need_ _You_, buku ketiga yang ia tulis dan menjadi _best seller_ bersama buku-buku terbitan Mediakita lainnya.

Br bercerita soal bukunya yang awalnya ditulis di tisu sembari menunggu temannya itu. Ada banyak pelajaran yang ia sisipkan. Orang-orang tidak boleh asal menghakimi orang lain. Banyak sekali kejadian yang membentuk sifat/sikap seseorang, kita tidak boleh asal melihatnya dari situasi terakhir. Siapa tahu mabuknya seseorang sekarang menyelamatkannya dari bunuh diri tadi? Tuturnya.

Acara kali ini tidak hanya menjadi ajang promo buku tetapi juga menjadi ajang berbagi tips menulis.

Tetaplah menulis--jika kita senang melakukannya--jangan pedulikan penilaian orang lain. Kalau sudah mulai menulis, manfaatkan _mood_ itu sebaik-baiknya. Tulislah sebanyak-banyaknya. Masalah penulisan dan ejaan yang salah, itu urusan nanti. Karena, kalau sudah berhenti, rasa malas itu akan terus menggerogoti tubuh hingga nanti-nanti. Lalu si tulisan, kapan jadi?

Br dan Nawang setuju, menulislah apapun itu. Kedua orang itu pernah diremehkan karena tulisan yang kebanyakan orang dianggap _menye-menye_ dan galauan, entah oleh lingkungan atau teman-teman. Mereka hanya perlu membuktikan karyanya benar-benar sukses untuk membungkam mereka semua. HAHAHAHAHAHA ~~

Banyak poin menarik yang penting dicatat di acara kemarin. Selain tips di atas, Br bilang persistensi adalah kunci. Persisten adalah tindakan konsisten tetapi selalu berkembang. Sikap inilah yang ia pegang selama ini.

Mengapa pengunjung harus membaca buku mereka? Br menyampaikan, tulisannya penuh dengan pelajaran hidup. Sedangkan Nawang bilang, tidak baca juga tidak apa-apa. _Wkwk_~

Mulai sekitar pukul 15.30, acara memasuki sesi gim (_games_). Dua orang maju membawakan puisi, satu orang bernyanyi, dua pasangan saling menggombali. Br dengan kekocakannya menimpali. Nawang memandang peserta dan rekannya, berdiam diri, _kyut_, di kursi.

![Sesi pembacaan puisi](/blog/2019/02/images/photo_2019-02-10_14-53-56.jpg) Sesi baca puisi

![Sesi saling menggombali antar penonton](/blog/2019/02/images/photo_2019-02-10_14-53-51.jpg) Sesi menggombali

![Sesi menyanyi](/blog/2019/02/images/photo_2019-02-10_14-53-48.jpg) Sesi galau menyanyi

Hampir pukul 16.00, para pengunjung diizinkan antre untuk meminta tanda tangan ataupun meminta foto.

![Antre tanda tangan Nawang dan Brian](/blog/2019/02/images/photo_2019-02-10_14-53-44.jpg) Antrean

Di tas, _Hilang_, sudah saya siapkan. Saya mulai mengantre di barisan Nawang saat menyisakan kurang dari 10 orang. Saya menyodorkan ponsel ke panitia untuk titip fotoin dan menyodorkan buku kepada Nawang. Entah, awalnya ia ingin menulis apa, yang tergores di buku malah huruf "d".

![Nawang sedang menandatangani buku](/blog/2019/02/images/photo_2019-02-10_14-53-32.jpg) Minta tanda tangan

_Eh, kok "d" sih?_

"_d__ear_" menjadi pilihan tulisan di atas tulisan _Hilang_ itu.

(Bodonya, buku _Hilang_ kurang saya angkat dikit ke atas. _Hadeeh_~)

![Berfoto dengan Nawang](/blog/2019/02/images/photo_2019-02-10_14-53-30.jpg) apa liat-liat?
