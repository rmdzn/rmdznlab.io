---
title: "Menikmati Kehilangan dengan 'Hilang' (Awas Spoiler!)"
date: "2019-02-14"
categories: 
  - "buku"
tags: 
  - "buku"
coverImage: "hilang-buku-1-rmdzn.jpg"
aliases:
  - /post/2019-02-14-menikmati-kehilangan-dengan-hilang/
---

> Peringatan, ulasan ini mungkin mengandung bocoran (spoiler).

Saya lagi senang berpuisi. Baru-baru ini _sih_. Sampai-sampai saya membuat proyek dengan nama Sintaskata sebagai jenama ruang pamer puisi (di IG, Sintaskata fokus pada puisi, sedangkan di Twitter, ia juga bisa berkonten acak) selain lewat status WhatsApp.

Kebetulan, saat berkeliaran di media sosial Twitter, saya dihadapkan dengan foto buku bersampul hitam dengan judul _Hilang_ berwarna kuning emas. Itu kesaksian kesekian kali setelah sempat melihat gambar buku yang sama, sebelum saya _ngeh_ dan memutuskan untuk membelinya di Gramedia. Saya beli dua, yang satu saya baca sendiri, satunya lagi saya hadiahkan kepada seorang kawan yang baru saja pulang ke kampungnya.

Buku yang sudah dicetak dua kali sejak akhir 2018 oleh Mediakita itu adalah kumpulan sajak karya [Nawang Nidlo Titisari (alias @nanidlot)](https://www.nanidlot.com). Sajak cantik yang mengizinkan saya untuk membayangkan peristiwa saat penulis menggubahnya. Di pojok kamar berlampu kuning (remang-remang) dan _mellow_. Mata menetes _eluh_ tetapi saat kawan mengetuk pintu, wajahnya langsung mengeras. Sok kuat.

Oke, itu memang berlebihan.

_Hilang_ adalah sajak tentang perpisahan dan mencintai sendirian. Sudut pandang yang diambil dari seorang perempuan dengan kecintaan yang luar biasa kepada seorang laki-laki. Sayangnya, bukan akhir cerita indah yang didapat perempuan itu, tetapi justru kisah sedih, ia ditinggal dan dilupakan. Ia harus berjuang menahan rasa sakit hati sambil sebisa mungkin mempertahankan hubungan yang terancam kandas, meskipun Tuhan berkehendak lain.

Dilihat dari sisi umum, mungkin memang berakhir sedih, tetapi kalau dilihat dari sisi spesial, ia berakhir membahagiakan. Sang perempuan mampu mengikhlaskan dan merelakan.

Bagi pembaca yang pernah merasakan sebuah hubungan spesial dan berakhir kandas, _Hilang_ akan mengobrak-abrik perasaan mereka. Mengingatkan mereka akan rasa sakit dan berusaha melupakan. Bikin _baper_ asli. Sebaliknya, bagi pembaca yang belum pernah merasakan hubungan spesial, buku _Hilang_ hanyalah buku biasa--dari segi perasaan. Jangan khawatir, jika pembaca adalah jenis orang kedua, mereka tetap bisa menikmatinya. Bahasanya renyah. Penggemar sajak dapat menikmati "nada gemas" pada setiap akhir baitnya.

Mengulang pernyataan yang disampaikan oleh Nawang di [_Meet and Greet_, Gramedia Sudirman kemarin](/blog/2019/02/2019-02-10-ketemu-mbak-nawang-yang-hilang/), tulisan-tulisan di dalam _Hilang_ adalah kumpulan penggambaran penulis terhadap curahan hati kawan-kawannya. Bukan curahan hatinya sendiri, meski setelah buku terbit, ia kemudian justru merasakan kehilangannya sendiri.

![Lembar pertama buku "Hilang"](/blog/2019/02/images/hilang-buku-2-rmdzn.jpg) Buku 'Hilang' dengan tanda tangan penulis

Buku _Hilang_ berisi 159 halaman, ditambah 8 halaman awal (termasuk kata pengantar, halaman judul, dll). _Hilang_ bukan tipikal buku dengan teks banyak berdempet pada setiap halamannya. Selain berformat klasik--judul dilanjut isi--_Hilang_ juga melengkapi diri dengan format teks ala-ala kutipan (_quote_).

Ada beberapa isi yang saya simpan dan jadikan kutipan favorit:

> Aku mencintaimu sebanyak hujan, kau mencintaiku sesingkat senja. Seperti hujan aku jatuh cinta berkali-kali. Seperti senja, kau jatuh cinta kemudian pergi. (Hal. 94)

> Boleh jadi menerima untuk ditinggalkan adalah bentuk lain dari mencintai, atau cara berbeda untuk menghargai diri sendiri. Dari kehilangan ini aku belajar bahwa cinta tidak berhak mengekang. Beberapa cinta harus bertugas melepaskan. (Hal. 116)

> Tanpa kusadari, kehilanganmu telah mendewasakanku. Dari yang hanya ingin mencintai dan dicintai kamu, menjadi ingin berjalan sendiri. Menyusuri perjalanan tanpa harus ada ambisi dan patah hati. Karena dunia terlalu luas untuk kunikmati. (Hal. 118-119)

> Pada akhirnya kita hanya manusia yang butuh mengerti. Bahwa semua yang datang akan selalu pergi. Bahwa semua yang menyala akan selalu mati. Bahwa semua yang cinta, kadang harus berakhir sendiri. Tidak lagi mencintai dan dicintai. Tidak lagi melukai dan menangisi. Pada akhirnya .... Kita akan bahagia dengan jalan kita sendiri. (Hal. 135)

> Tenang saja. Kalau langit sudah setuju, kita pasti akan bertemu. Dalam waktu yang pas dan tidak keliru. (Hal. 147)

Dan kutipan pecah pada halaman terakhir (ya, itu silakan lihat sendiri :p).
