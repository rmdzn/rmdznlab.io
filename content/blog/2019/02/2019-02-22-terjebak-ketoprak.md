---
title: "Terjebak Ketoprak"
date: "2019-02-22"
categories: 
  - "kuliner"
tags: 
  - "kuliner"
coverImage: "photo_2019-02-22_16-11-43.jpg"
aliases:
  - /post/2019-02-22-terjebak-ketoprak/
---

Saya sudah lama penasaran dengan rasa dan bentuk ketoprak. Bukan ketoprak seni budaya lho ya, tetapi makanan ketoprak yang populer di wilayah ibu kota Indonesia itu.

Ketoprak sendiri bagi banyak orang dianggap sebagai makanan khas Betawi, meskipun ini belum pasti karena ada juga yang menganggapnya berasal dari Cirebon bahkan Jawa Tengah. Asal muasal nama "ketoprak" juga belum bisa dipastikan. Apakah namanya muncul karena peracik mengolah sambil nonton kethoprak (seni budaya)? Ternyata tidak. Ada yang bilang nama "ketoprak" berasal dari singkatan _ketupat, toge (taoge), dan digeprak_. Ada juga yang bilang sang peracik terinspirasi suara piring yang berbenturan, _ketuprak_. Bodo amat lah dari sejarahnya, yang penting makan~

Kebetulan. Sebetulnya bukan kebetulan, Tuhanlah yang mempertemukan aku dan kamu ketoprak. Di sepanjang jalan Selokan Mataram, jalan yang biasa saya lalui terdapat satu penjual ketoprak bernama _Ketoprak Khas Jakarta_, tepatnya di belokan antara jalan selokan dan Jln. Affandi (Gejayan). Mampirlah saya ke situ, memesan satu porsi ketoprak pedas (tanpa telur), siang hari sembari menikmati pemandangan lingkungan yang tak begitu indah.

Selama ini saya tidak mencari informasi detail seputar ketoprak di internet (mungkin pernah lihat gambarnya, tapi tak begitu memperhatikan), sehingga mengalir saja, mengharap kejutan setelah makanan disajikan. Benar saja, bayangan yang terlintas di benak dengan yang ada dikenyataan berbeda. Saya selama ini menganggap ketoprak mirip seperti lotek atau gado-gado--ada banyak sayuran. Eh, nyatanya tidak. Ketoprak yang tersaji di atas piring terdiri dari irisan ketupat, mie putih, taoge, dan tahu yang disiram saus kacang hingga _bleber-bleber_ serta cucuran kecap. Beberapa keping kerupuk udang tersebar di atasnya.

![Ketoprak](/blog/2019/02/images/photo_2019-02-22_16-11-43.jpg) Ketoprak sudah tersaji

Meski memesan pedas, rasa ketoprak saat itu tidak pedas, paling cuma bikin panas di sela-sela bibir. Dari segi rasa yaa tidak asing, lah. Mungkin yang agak asing teksturnya, _nyemek_ karena penuhnya saus kacang.

Harganya murah. Dengan porsi sebanyak itu saya hanya perlu merogoh dompet Rp8.000, kalau tambah telur tinggal tambah Rp3.000.

Untuk saat ini kalau saya ditanya _apakah ingin beli ketoprak lagi?_, saya akan bilang _tidak dulu_. Lidah dan mulut masih belum cocok, hehee~
