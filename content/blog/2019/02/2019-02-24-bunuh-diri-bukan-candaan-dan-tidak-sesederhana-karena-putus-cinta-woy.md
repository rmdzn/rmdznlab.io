---
title: "Bunuh Diri Bukan Candaan dan Tidak Sesederhana karena 'Putus Cinta', Woy!"
date: "2019-02-24"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "rosenzweig-3997249_1920.jpg"
aliases:
  - /post/2019-02-24-bunuh-diri-bukan-candaan-dan-tidak-sesederhana-karena-putus-cinta-woy/
---

![duri](/blog/2019/02/images/rosenzweig-3997249_1920.jpg)

Hari Jumat, 22 Februari 2018, sekitar pukul 16.00 seorang mahasiswa berdiri di atas gedung Transmart, Lampung, dengan terlebih dahulu duduk _ongkang-ongkang--_duduk dengan posisi kaki menggantung, menempel dinding. Ia berniat bunuh diri. Bukan "tarikan" untuk menjauh dari tepi atap gedung, ia malah mendapatkan "dorongan" untuk benar-benar melakukannya. Beberapa menit kemudian, ia lompat [dengan menghadap ke belakang](https://kumparan.com/lampunggeh/breaking-news-seorang-pria-bunuh-diri-di-transmart-lampung-1550831366668196050).

Parahnya di video yang tersebar secara viral, si perekam justru dengan entengnya bilang _loncat loncat_. Fakta lain yang bikin jengkel, salah satu saksi mata melihat ada orang berpakaian hitam, dia pikir orang ini sedang bernegosiasi, [ternyata juga cuma numpang mengambil foto](https://regional.kompas.com/read/2019/02/23/15072161/banyak-yang-merekam-tetapi-tak-menolong-ini-fakta-kasus-mahasiswa-bunuh-diri). Ego di atas segalanya. Ego mengalahkan empati.

Seperti jatuh tertimpa tangga. Korban tak banyak mendapatkan simpati, ia justru mendapat olokan dan candaan. Di akun Instagram korban yang sudah hilang dari peredaran, manusia-manusia abad 20 berkomentar seenak udel mereka. [Jijik banget](https://chirpstory.com/li/420335) ([jaga-jaga kalau tautan itu mati](http://archive.is/0p8qK)).

Orang-orang menganggap bunuh diri adalah tindakan lemah, seperti saat Deddy Corbuzier menyampaikan hal yang sama dalam videonya. _Alah, cuma masalah kayak gitu, bunuh diri_, kata orang-orang berjempol neraka di sana. Tidak sesederhana itu, Ferguso. [Bunuh diri bukanlah kasus yang terjadi karena faktor tunggal](https://tirto.id/bagaimana-seharusnya-media-memberitakan-bunuh-diri-mahasiswa-deW3) seperti yang selalu digemborkan media; karena putus cinta lah, karena sakit ini itu lah, karena xyz lah. Hanya korban dan Tuhanlah yang benar-benar tahu mengapa bunuh diri dilakukan. Kita tak berhak menghakimi segampang itu dan mengklaim diri sebagai manusia tangguh.

Seseorang berpikiran untuk bunuh diri bisa disebabkan oleh kesehatan mental yang terganggu. Mereka sebetulnya bisa melakukan pengobatan, tetapi apa yang masyarakat katakan? Mereka adalah _orang gila, salatnya belum bener, kurang dekat dengan Tuhan, blablabla_. Itu belum karena faktor ekonomi yang menyebabkan tidak terjangkaunya pengobatan. Mereka ingin menyampaikan masalah-masalah itu kepada orang terdekat, tetapi apa yang mereka dapatkan? Penghakiman dan ketidakpedulian. Lagi-lagi, saya tekankan, banyak faktor yang menyebabkan seseorang bunuh diri, dan itu kompleks. Ingat! Mungkin salah satu faktornya adalah _tindakan dan cocot_\-mu.

Orang-orang perlu berempati. Itu saja. Cukup.

Bagaimana caranya? Mudah. Semudah situ lahir menjadi manusia, bukan kambing.

Setiap melihat nasib atau masalah orang lain. Tutup mulut serapat-rapatnya. Jangan cerewet. Jangan nyinyir. Jangan hakimi. Selama ini kita meremehkan nasib orang lain karena kita mencoba menempatkan masalah mereka dalam kehidupan kita dengan latar belakang hidup yang kita miliki. Padahal, pemilik masalah memiliki latar belakang yang berbeda dengan kita. Berempati adalah ketika mencoba menempatkan masalah orang lain ke kehidupan kita dengan latar belakang hidup yang kita tidak tahu pasti dan berusaha menduplikasi perasaan mereka.

Masih merasa kesulitan untuk berempati? Lahirlah kembali sebagai manusia dengan segala nuraninya. Jika selama ini belum pernah merasakan empati, ini saatnya. Mulailah secara pelan-pelan dan konsisten dengan sikap itu. Hati yang tak terbiasa berempati itu seperti batu. Perlu ditetesi air berkali-kali untuk melubanginya bahkan menghancurkannya.

**_Hotline_ Bunuh Diri**

Kemenkes [memiliki](https://www.cnnindonesia.com/nasional/20170725120000-20-230158/kemenkes-buka-nomor-darurat-untuk-konseling-cegah-bunuh-diri) _hotline_ **119** untuk konseling pencegahan bunuh diri dan kesehatan mental. Saya yakin nomor ini masih aktif sampai sekarang apalagi setelah ditegaskan juga oleh [dr. Jiemi Ardian](https://twitter.com/jiemiardian/status/1097508144920506368).

Ada juga konseling komunitas kesehatan mental [Save Yourselves](https://www.instagram.com/saveyourselves.id/https://www.instagram.com/saveyourselves.id/) (daerah Jakarta). Kontak bisa dilihat di bawah ([sumber](https://twitter.com/novikamil/status/1097545240330756096/photo/1)).

\[caption id="attachment\_605" align="aligncenter" width="720"\]!["Cerita" hotline bunuh diri Save Yourselves](images/hotline-bunuh-diri-save-yourselves-id-rmdzn.jpg) Hotline bunuh diri dari Save Yourselves\[/caption\]

Di [/r/indonesia](https://www.reddit.com/r/indonesia/comments/7wybjk/suicide_hotline_asap/) saya mendapatkan _hotline_ lainnya:

![Daftar hotline bunuh diri dari Reddit Indonesia](/blog/2019/02/images/hotline-bunuh-diri-dari-reddit-rmdzn.png) Hotline bunuh diri (/r/indonesia)

![Daftar hotline bunuh diri dari Reddit Indonesia](/blog/2019/02/images/hotline-bunuh-diri-dari-reddit-1-rmdzn.png) Hotline bunuh diri (/r/indonesia)

Ada yang punya _hotline_ lain? Boleh dong tempel komentar di bawah.
