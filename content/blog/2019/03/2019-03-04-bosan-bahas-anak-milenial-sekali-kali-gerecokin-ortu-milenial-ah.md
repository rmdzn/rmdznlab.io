---
title: "Bosan Bahas Anak Milenial, Sekali-kali 'Gerecokin' Ortu Milenial, Ah!"
date: "2019-03-04"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "parenting-rmdzn.jpg"
aliases:
  - /post/2019-03-04-bosan-bahas-anak-milenial-sekali-kali-gerecokin-ortu-milenial-ah/
---

![parenting](/blog/2019/03/images/parenting-rmdzn.jpg)

Dunia itu adil, orang jahat kena karma, orang baik dapat pahala. Ah, tidak juga. Untuk beberapa hal, dunia mungkin bisa dibilang tidak begitu adil.

Bagaimana bisa adil? Zaman sekarang, anak-anak milenial dirundung habis-habisan. Mereka dianggap tidak jelas dan tidak bekerja seekstra keras para pendahulunya. Stereotipe hadir karena munculnya teknologi yang memudahkan aktivitas mereka berikut kerandoman yang mereka pamerkan di internet.

Sebagai info, "milenial" adalah sebutan para tetua kepada manusia yang lahir di era teknologi atau yang semasa balignya sudah mengerti beragam teknologi. Pokoknya, anak-anak yang tidak merasakan repotnya cari referensi tugas lewat buku buluk di perpustakaan, anak-anak yang mainannya TikTok, anak-anak yang diskusi tugas lewat _video call_, dan lain-lain. Kalau boleh jelasin dikit sih, sebenarnya mereka [generasi Z](https://www.careerplanner.com/Career-Articles/Generations.cfm) bukan milenial. Hihi ...

Para tetua memaksa anak-anak milenial untuk bertingkah normal dan merasakan kenikmatan bergelesotan bermain di tanah seperti mereka dahulu. Yaah, itukan pikiran mereka.

Kalau mau bahas milenial secara adil, seharusnya orang-orang juga membahas generasi milenial yang sudah menjadi orang tua; menikah dan memiliki anak. Bagaimana orang-orang tua milenial ini harus bertingkah demi melahirkan generasi yang hebat hingga menjadikan bumi ini aman, tenteram, maju, elok dan edi, bersih dan berbudaya, seperti yang telah para tetua ini lakukan.

Saya telah mengumpulkan beberapa catatan penting untuk saya bagikan kepada rekan-rekan calon mempelai serta para muda-mudi yang sudah terikat tali pernikahan. Oiya, tulisan ini juga mungkin cocok untuk saya--entah kapan. Hehe ...

Kepada orang tua milenial, tolong bebaskan anak untuk melakukan apapun, asal positif. Kalau sedari kecil dikekang, anak akan cenderung memberontak atau cupu di hari-hari berikutnya. Pegang itu, tidak boleh, nyobain ini, tidak boleh, lalu mereka harus bagaimana? [Mainan HP](/blog/2018/12/2018-12-15-kontrol-ortu-atas-gawai-anak-ribet-tetapi-berharga/)? Mending kalau mereka menonton saluran National Geographic, lha ... kalau nonton lagu _Bad_\-nya Awkarin sampai melotot bahkan tidak tidur?

Kalaupun ingin melarang dengan mengatakan "tidak boleh" dan "jangan", berikan alasan yang logis. Bukan karena mentang-mentang sedang malas mengajari anak mencuci tangan setelah memegang lumpur, orang tua bisa melarang mereka seenaknya. Padahal, kata iklan deterjen, berani kotor itu baik. Ya, kan?

Ini bukan berarti, orang tua asal membiarkan anaknya wira-wiri tanpa pegangan. Gabut dong, orang tuanya. Orang tua tetap berhak menghentikan atau tetap membiarkan anaknya melakukan sesuatu, dengan syarat ada komunikasi sebelumnya (ini masuk pada bahasan ngobrol bareng di bawah).

Kepada orang tua milenial, tolong ajarkan anak-anak membaca buku sejak dini. Anak kecil lebih mudah meniru perilaku ketimbang mendengarkan ceramah. Perlihatkan kepada mereka bahwa bapak ibunya suka menenteng buku atau koran dan membacanya di sela-sela waktu. Tunda dulu membaca buku digital kalau anak-anak sedang berada di sekeliling orang tua, kecuali mereka dapat menjelaskan kepada anak-anak bahwa orang tuanya memang sedang membaca buku, bukan sekadar bermain HP.

Jangan merasa kaku lho ya. Kita jangan menyuruh mereka untuk baca buku Das Kapital. Banyak kok buku bacaan yang memang ditujukan untuk anak--penuh dengan gambar, teks sedikit. Oiya, terpenting, jangan paksa mereka harus lancar membaca buku sehingga tak perlu minder kalau dalam usia tertentu anak belum bisa lancar mengeja seperti teman sebayanya. Konon, anak yang telat lancar membaca cenderung akan lebih hobi membaca buku daripada anak yang lebih cepat melakukannya.

Kepada orang tua milenial, jangan hakimi anak-anak dengan kata kasar dan membandingkannya dengan orang lain. Seperti saat kita ditinggal mantan, anak-anak hatinya sangat rentan. Sekali dibentak, mereka akan hobi menyerang dengan kasar. Saat dibanding-bandingkan, mereka akan merasa rendah diri, tak lagi percaya diri.

Kepada orang tua milenial, tolong budayakan untuk berbagi pendapat dan menghormatinya jika berbeda di lingkungan keluarga. Orang tua arogan adalah ia yang selalu menganggap dirinya selalu benar. _Perhantu dengan pendapat anak yang masih ingusan_. Orang tua milenial seharusnya lebih terbuka dengan opini-opini yang disampaikan orang lain--dalam kasus ini anggota keluarga. Soal apakah opini anggota keluarga lain dirasa belum matang, itu bisa didiskusikan lagi.

Poin ini juga menitikberatkan pada budaya ngobrol bareng, serius dan tak serius. Orang-orang tua tidak jarang memaksa anaknya yang pendiam untuk bercerita kepada mereka. Saat si anak memilih untuk diam saja, orang tua akan bergerak cepat menghakimi. _Biarin saja bisu kalau gak mau cerita_, kurang lebih seperti itu. Padahal olokan itu tak bakal meluluhkan si anak untuk mau bercerita, ia malah makin mengintimidasi mereka.

Jika ingin mendengarkan cerita dari anak, orang tua seharusnya membangun kultur bercerita di keluarga sejak mereka masih kecil. Cerita tentang hal remeh sekalipun. Percayalah, ini juga akan meningkatkan rasa percaya antar anggota keluarga, terutama antara anak dan orang tua.

Kepada orang tua milenial, tolong ajarkan masalah seks sejak dini. Ini bukan berarti orang tua tiba-tiba membuka topik tentang hal tersebut. Edukasi seks dapat dimulai jika pertanyaan anak mulai menjurus pada tema itu, jawab secara jujur. Ini mungkin berat. Namun itu sungguh bermakna. Jangan sampai hingga remaja mereka malah lebih memilih untuk mencari edukasi seks lewat video porno dan pergaulan bebas. Edukasi seks dari orang tua juga dapat meningkatkan kepedulian terhadap pelecehan maupun kekerasan seksual.

Salah satu contoh konkritnya, orang tua perlu mengajarkan bagian-bagian tubuh mana yang tidak boleh disentuh orang lain, serta bagian tubuh orang lain yang tidak boleh mereka sentuh tanpa persetujuan. Di era kekinian, topik seks jangan lagi menjadi hal tabu. Orang tua milenial bertanggung jawab soal itu. Masa iya setelah 19 tahun era milenium, topik reproduksi pada mata pelajaran biologi di sekolah masih mengundang sorak sorai norak yang tak perlu.

Dari bahasan ini, bukan cuma anak milenial yang dituntut "sesempurna" generasi sebelumnya, orang tua milenial juga dituntut melakukan hal serupa. _Gimana, gimana? Mau menikah atau enggak?_

\[Gambar oleh **skalekar1992** di **Pixabay**\]
