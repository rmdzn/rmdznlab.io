---
title: "Mengenal Masalah Media Sosial dan Sedikit Tentang Dunia Fediverse"
date: "2019-03-11"
categories: 
  - "teknologi"
tags:
  - "medsos"
coverImage: "media-sosial-lnx.jpg"
aliases:
  - /post/2019-03-11-mengenal-masalah-media-sosial-dan-sedikit-tentang-dunia-fediverse/
---

_Media sosial sungguh sangat keren, tetapi ada masalah di situ, Kawan~_

Di saat Wiro Sableng masih hidup, komunikasi antar manusia masih super konvensional. Menggunakan daun lontar yang diterbangkan dengan burung merpati atau diantarkan oleh kurir terpercaya. Mendapatkan berita di zaman itu juga masih sebatas dari mulut ke mulut. Pastinya, semua menggambarkan satu "masalah" yang sama: waktu yang lama, yaah ... meskipun "masalah" itu relatif. Kalau saya terbang ke masa itu dan menanyakan "masalah" tersebut kepada bapak/ibu yang tengah _ngemil_ kacang di gubuk, mereka akan menyatakan komunikasi dan pengiriman pesan yang lama bukanlah sebuah masalah, teknologi komunikasi saat itu memang sangat terbatas.

Berbeda dengan era ini, aplikasi perpesanan (_messaging_) dan media sosial menjadi senjata untuk mengabarkan tentang apapun kepada orang lain dengan sangat cepat. Media sosial (medsos) menjadi andalan banyak orang untuk memperoleh berita tanpa perlu menunggu terbitnya koran cetak setiap pagi. Era media sosial dan kepopulerannya ini ditandai dengan munculnya platform pada satu dasawarsa terakhir seperti Facebook, Twitter, Instagram, Path, Pinterest, dsb.

## Masalah media sosial kekinian

Di balik kemewahan dengan segala fitur yang serba gratis itu, media sosial populer memiliki sisi gelap yang membuat penggunanya secara sadar maupun tidak sadar dikelabui, dikekang, dan dilabrak haknya oleh entitas-entitas tertentu. Simak deh bawah ini.

- Semua konten yang dibagikan di medsos populer bersifat tertutup

Apa maksudnya? Coba kita membaca tulisan seseorang di Facebook **tanpa login terlebih dahulu**. Mampukah? Nyamankah? Atau kaget karena tiba-tiba muncul formulir login beserta gambar profil sebuah akun? Oke, mungkin kita dapat menolak login dengan mengklik _Lain Kali_, tetapi ia akan muncul lagi ketika kita akan melakukan hal yang sama. Selain itu, ada _bar_ login/daftar akun yang juga muncul di bagian bawah.

Kasus "pemaksaan login" seperti itu muncul sejak kehadiran medsos. Sebelum era meledaknya medsos (zaman Friendster tidak dihitung), berbagi informasi dilakukan menggunakan blog. Interaksi dan diskusi terjadi di blog tersebut atau di situs agregasi seperti LintasBerita (lokal) dan Technorati (luar negeri). Orang-orang kekinian lebih suka langsung berbagi tulisan lewat media sosial alih-alih menuliskannya dalam blog untuk kemudian dibagikan lewat tautan.

- Kontenmu bukanlah kontenmu

Ya, konten yang diunggah di media sosial secara teknis adalah milik perusahaan pengembang media sosial. Ia tak akan terbebas dari ancaman penghapusan bahkan pemblokiran. Sebuah konten kontroversial yang dilaporkan oleh massa akan berpotensi besar dihapus, begitu juga konten yang dilaporkan oleh pemerintah sebuah negara.

- Pengguna adalah produk yang layak dijual

Ada kutipan yang sangat terkenal, bunyinya, "_jika kamu tidak membayar sebuah produk, kamulah produknya._"

Kutipan yang hidup sejak tahun [1970-an](https://www.quora.com/Who-originally-suggested-that-if-youre-not-paying-for-the-product-you-are-the-product) ini seperti meramalkan dunia media sosial kiwari. Penguasa medsos adalah korporasi--entitas pencari keuntungan. Karena medsos yang mereka buat dapat diakses secara gratis oleh siapapun, untuk mencari keuntungan, mereka menjual data pengguna kepada para pengiklan. Data pengguna yang dimaksud termasuk usia, jenis kelamin, riwayat saat berselancar lewat peramban, dan data serupa lainnya yang secara tak langsung menggambarkan personal pengguna.

Korporasi dengan produknya telah melanggar batas privasi yang dimiliki oleh pengguna.

- Medsos mengatur apa yang harus dilihat pengguna

Apa yang diharapkan pengguna ketika membuka linimasa Facebook atau Twitter? Status yang muncul sesuai kronologi. Siapapun yang baru saja menuliskan sesuatu di sana, seharusnya muncul di linimasa paling atas. _No .. no_, tak segampang itu, Sergio. Setelah korporasi di balik medsos sukses mengumpulkan data pengguna, mereka mengatur konten apa saja yang harus ditampilkan kepada pengguna berdasarkan data yang mereka kumpulkan. Dalih mereka _sih_, pengguna adalah konsumen yang wajib dipuaskan dengan konten-konten relevan.

Coba lihat linimasa Facebook kamu, lalu simak iklan _fan page_ yang muncul, pasti sesuai dengan kebiasaan berselancarmu. Facebook akan menjejali pengguna iklan _fan page_ meme jika penggunanya hobi mengakses konten-konten meme di dalam maupun di luar Facebook. Tak hanya itu, iklan-iklan produk yang muncul juga pasti berhubungan dengannya.

Kemudian silakan periksa linimasa Twitter. Pengguna akan disuguhi tulisan seseorang yang selama ini tidak mereka ikuti, karena tulisan itu viral atau disukai oleh akun yang mereka ikuti (_following_). Di samping itu, mereka memang membuat opsi untuk melihat twit sesuai kronologi melalui tombol _See latest Tweets instead_, tetapi mereka seringkali [mematikan opsi itu tanpa sepengetahuan pengguna](https://a.wholelottanothing.org/2019/03/06/optimizing-for-outrage/) sehingga twit yang akan muncul di linimasa adalah twit-twit populer terlebih dahulu--seperti biasanya.

![Twit akun Admin Usam](/blog/2019/03/images/screenshot_2019-03-10-home-twitter-contoh-twit-likes-rmdzn.png) Twit yang muncul di linimasa saya. FYI, saya tak pernah mengikuti @AdminUsam

- Tersentralisasi

Medsos dimiliki oleh entitas tunggal. Apa artinya? Jika entitas (baca: korporasi) itu kolaps dan kalau tidak ada entitas lain yang mengakuisisinya, secara otomatis medsos yang disediakan ikut mati. Banyak kok contoh medsos yang kolaps, di antaranya Yahoo! Koprol, Path, dan Friendster.

## Solusi

Komunitas yang selama ini peduli dengan negatifnya ekosistem medsos telah cukup lama menerapkan protokol terbuka agar pengguna satu medsos dengan medsos lain dapat saling berkomunikasi.

Maaf, kalau sebutan "protokol" terlalu teknis bin jelimet. Anggap saja "protokol" itu "bahasa". Apakah pengguna Twitter dapat langsung mengobrol dengan pengguna Facebook, tanpa harus mendaftarkan diri dulu di medsos yang disebut terakhir? Tidak bisa, karena selama ini keduanya berbicara dengan "bahasa" yang berbeda. Naah ... "protokol terbuka" yang dimaksud di atas mengizinkan sebuah proyek medsos untuk berbicara dengan "bahasa" yang sama meskipun tampilan dan bentuk medsosnya berbeda.

Implementasi "protokol terbuka" mengizinkan medsos mengudara tanpa tergantung pada satu server terpusat. Seperti yang saya bilang tadi, kelemahan dari medsos populer adalah hidup tersentralisasi. Yang berarti, mereka dimiliki oleh satu entitas. Tulisan galau, tulisan motivasi, gambar-gambar lucuk, yang kita unggah di sana dikelola oleh satu dan hanya satu entitas. Dengan "protokol terbuka", medsos-medsos dapat dikelola di server yang terdesentralisasi dan terdistribusi (terfederasi) oleh banyak entitas.

Lihat ilustrasi di bawah ini. Ini adalah [ilustrasi dari teori seorang](https://networkcultures.org/unlikeus/resources/articles/what-is-a-federated-network/) _[engineer](https://networkcultures.org/unlikeus/resources/articles/what-is-a-federated-network/)_ [kelahiran Polandia-Amerika, Paul Baran](https://networkcultures.org/unlikeus/resources/articles/what-is-a-federated-network/):

![Tiga jaringan; tersentralisasi, terdesentralisasi, dan terdistribusi](/blog/2019/03/images/tipe-jaringan-rmdzn.png)

Konsep sentralisasi terlihat pada gambar (a) sedangkan konsep desentralisasi dan distribusi tergambarkan pada poin (b) dan (c). Titik-titik di situ menggambarkan server berisi medsos dan garis menggambarkan jaringan atau koneksi. Untuk selanjutnya saya akan menyebut poin (b) dan (c) sebagai _federasi_.

Sentralisasi adalah bagaimana Facebook, Twitter, Instagram, dll bekerja. Kalau federasi? Federasi adalah bagaimana surel (_email_) bekerja. Pengguna GMail dapat mengirim pesan kepada pengguna Outlook. Keduanya juga sama-sama dapat mengirim pesan kepada Protonmail, Tutanota, Yahoo Mail, FastMail, dan StartMail, begitu juga sebaliknya. Surel merupakan salah satu contoh penerapan "protokol terbuka" yang telah kita nikmati hingga kini.

Protokol terbuka untuk medsos itu ada banyak. Meskipun banyak, dasarnya sama sehingga mereka tetap bisa berkomunikasi satu sama lain.

Khusus untuk medsos, dunia federasinya punya sebutan sendiri, yakni _federation_ (protokol Diaspora dan OStatus) dan _fediverse_ \[gabungan dari kata _federation_ dan _universe_ (protokol OStatus dan ActivityPub)\]--walaupun untuk saat ini dunia federasi medsos didominasi oleh _fediverse_. Tunggu dulu, kita tidak akan menuju penjelasan detail mengenai hal itu.

### Keuntungan medsos terfederasi

- Ada banyak pilihan medsos, baik yang berbentuk seperti Twitter, Facebook, Instagram, bahkan YouTube sehingga dapat disesuaikan dengan keinginan dan tujuan calon pengguna.
- Calon pengguna dapat bergabung dengan server yang sudah ada atau membuat server sendiri.
- Banyak pilihan server umum maupun yang fokus pada topik-topik tertentu. Server untuk para desainer, ilustrator, penulis, penggemar olahraga, dll.
- Dunia medsos akan tetap hidup meskipun satu server mati. Matinya server hanya mempengaruhi pengguna yang mendaftar di server tersebut.
- Konten yang diunggah muncul sesuai kronologi.
- Tidak ada iklan dan tidak ada pelacak. Pengguna bukanlah produk.

### Kekurangan medsos terfederasi

- Belum banyak pengguna.

## Macam-macam medsos terfederasi

Sebetulnya terdapat laman khusus yang memamerkan medsos apa saja yang ada di dunia _fediverse_. Bisa dong kamu cek di https://fediverse.party.

### 1\. Mirip Twitter

**\*** **[Mastodon](https://joinmastodon.org/)**

![Akun Mastodon](/blog/2019/03/images/mastodon-ss-rmdzn.png) Halaman profil Mastodon

Mastodon menjadi medsos di _fediverse_ yang paling populer.

Server terpopuler: [mastodon.social](https://mastodon.social).

- **[GNU Social](https://gnu.io/social/)**

![Akun GNU Social](/blog/2019/03/images/gnu-social-ss-rmdzn.png) Halaman profil GNU Social

- **[Pleroma](https://pleroma.social)**

![Akun Pleroma](/blog/2019/03/images/pleroma-ss-rmdzn.png) Halam profil di Pleroma

Server terpopuler: [pleroma.site](https://pleroma.site).

- **[MissKey](https://joinmisskey.github.io/en/)**

![Akun MissKey](/blog/2019/03/images/misskey-ss-rmdzn.png) Halaman profil di MissKey

MissKey lebih terkenal di Jepang. Ia berbentuk _microblogging_ dan memiliki tampilan yang mudah dikustomisasi seperti Friendster.

### 2\. Mirip Facebook

- **[Diaspora](https://diasporafoundation.org/)**

![Akun Diaspora](/blog/2019/03/images/diaspora-ss-rmdzn.png) Halaman profil di Diaspora

Server terpopuler: [joindiaspora.com](https://joindiaspora.com).

- **[Hubzilla](https://zotlabs.org/page/hubzilla/hubzilla-project)**

![Akun Hubzilla](/blog/2019/03/images/hubzilla-ss-rmdzn.png) Halaman profil di Hubzilla

- **[Friendica](https://friendi.ca/)**

![Akun Friendica](/blog/2019/03/images/friendica-ss-rmdzn.png) Halaman profil di Friendica

### 3\. Mirip Instagram

- **[Pixelfed](https://pixelfed.org/)**

![Akun Pixelfed](/blog/2019/03/images/pixelfed-ss-rmdzn.png) Halaman profil di Pixelfed

Server terpopuler: [pixelfed.social](https://pixelfed.social).

### 4\. Mirip YouTube

- **[PeerTube](https://joinpeertube.org/en/)**

![Akun Peertube](/blog/2019/03/images/peertube-ss-rmdzn.png) Halaman profil di Peertube

### 5\. Unik

- **[Socialhome](https://socialhome.network/)**

![Akun Socialhome](/blog/2019/03/images/socialhome-ss-rmdzn.png) Halaman profil di Socialhome

### 6\. Mirip Reddit

- **[Prismo](https://gitlab.com/mbajur/prismo)**

## Akun saya di _Fediverse_

Saya membuat beberapa akun medsos di fediverse. Sila sampaikan _hai_ ke [@ramdziana@mastodon.sacial](https://mastodon.social/@ramdziana) (Mastodon), [@rmdzn.libranet.de](https://libranet.de/~rmdzn) (Friendica), [ramdziana@joindiaspora.com](https://joindiaspora.com/u/ramdziana) (Diaspora), atau [@ana@pleroma.site](https://pleroma.site/ana) (Pleroma). Akun Pleroma adalah akun yang kini sedang aktif saya pegang.
