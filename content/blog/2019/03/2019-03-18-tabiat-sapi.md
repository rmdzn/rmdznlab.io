---
title: "Tabiat Sapi"
date: "2019-03-18"
categories: 
  - "cerpen"
tags:
  - "cerpen"
coverImage: "sa-sa-pi-pi-sapi-rmdzn.jpg"
aliases:
  - /post/2019-03-18-tabiat-sapi/
---

![sapi](/blog/2019/03/images/sa-sa-pi-pi-sapi-rmdzn.jpg)

Lapis legit tergigit separuh. Hamparan kamboja, gelombang cinta, dan puring menghiasi pandangan di senja mendung. Pohon pepaya tegak lalu melambai ke depan, seraya menantangku untuk mengubah nasib seburuk apapun.

Dunia ini normal. Sapi-sapi berlalu lalang, memamah rumput hijau. Kadang juga rumput busuk berulat, kalau sapi tidak mendapatkan upah setimpal saat menjadi sopir bos monyet.

Setiap tiga tahun sekali, para sapi sangat bergembira. Wajar. Ada pesta. Ada hajatan pemilihan anggota wakil suku yang rugi kalau dilewatkan. Sapi-sapi akan disuguhi rumput dari padang sabana jika mereka mau memilih kandidat wakil suku yang foto-fotonya tertempel rapi di setiap kandang kumuh sekalipun.

Di samping kandang bercorak bunga melati, sapi hitam berbadan ramping duduk santai. Ia membayangkan dunia ideal. Rumput hijau didapatkan dengan pantas, estetik, dan bermoral. Tahun ini, bayangan itu terlihat menjanjikan untuk direalisasikan. _Sungguh menarik dunia ini_, katanya sambil mengusap ingus yang sedari tadi menetes.

Sapi hitam mendapatkan undangan di malam Kamis. Sesuai tradisi tiga tahunan, acara itu sungguh normal. Pengundang alias kandidat wakil sapi menyampaikan alasan undangannya. Ia percaya diri, keinginannya untuk mewujudkan suku yang makmur, bermartabat. Sang kandidat lalu menyerahkan 200,5 kg rumput untuk dibagikan kepada sapi lainnya sekaligus mengamankan cap tapak kaki setiap sapi tetangga.

Sebanyak 200,5 kg rumput tak dimakan sendiri. Itu tabu. Oleh sebab itu, rumput disumbangkan sebagai harta kolektif suku sapi. Boleh dipakai untuk menjamu tamu yang sangat penting atau dikeringkan sebagai tempat rebahan.

Rumput yang diserahkan sendiri oleh calon wakil suku itu dimaksudkan sebagai pengikat kehendak. Sayangnya, saat sang kandidat kembali mengundang sapi hitam dan rekan-rekannya sebagai bukti kesetiaan, mereka tidak datang. Sang kandidat berujar, _brengsek benar mereka, sudah kuberi rumput, eh malah pergi_.

Sejak saat itu, sang kandidat tidak lagi menghubungi sapi hitam dkk. Desas-desusnya ia _ngambek_.

Di kampung yang sapi hitam tinggali, terdapat bangunan suci untuk menghormati leluhur. Berdoa, berceramah membagikan wejangan, dan mendengarkannya adalah aktivitas rutin yang dilakukan di situ. Suatu waktu, bangunan suci akan dirombak. Diperindah dengan rumbai-rumbai dan diganti dengan balok berbahan besi pada bagian atap.

Kandidat calon wakil suku lain yang terbiasa membagikan wejangan di situ terpelatuk. Di bangunan suci, ia menyebar puluhan gambar diri dan berbaris-baris tulisan mengapa para sapi harus melakukan cap kaki untuk dirinya.

Tak lama, sang kandidat yang terobsesi dengan kesucian itu mengundang sapi hitam dan rekan sekampung untuk makan bersama di sebuah kandang unik di lereng bukit. Ia mengutarakan, tak berminat memberi sebagor rumput saat itu juga tetapi berjanji akan membantu perombakan bangunan suci dan memberi hadiah kepada sapi-sapi yang datang malam itu kalau berhasil terpilih sebagai wakil suku.

Hanya dalam waktu dua malam, dunia menjadi gelap gulita, menggelegar. Dunia terbalik, mengutuk, dan mengubah semua semua sapi di situ menjadi satu-satunya makhluk yang mereka benci: manusia.

Seorang perempuan berkacamata memegang pundakku dan menggoyangkannya lalu terbirit pergi. Aku tergagap. _WOE, MIMPI APAAN INI!_
