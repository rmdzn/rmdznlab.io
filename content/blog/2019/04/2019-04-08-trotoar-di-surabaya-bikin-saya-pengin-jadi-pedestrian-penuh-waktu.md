---
title: "Trotoar di Surabaya Bikin Saya Pengin Jadi Pedestrian Penuh Waktu"
date: "2019-04-08"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "jl-stasiun-gubeng-rmdzn.jpg"
aliases:
  - /post/2019-04-08-trotoar-di-surabaya-bikin-saya-pengin-jadi-pedestrian-penuh-waktu/
---

_Di Surabaya, berprofesi sebagai pedestrian adalah hal menyenangkan._

Sebagai anak Jogja tulen yang jarang kemana-mana, berencana pergi menjauh dari "bibir" gunung Merapi adalah aktivitas menakutkan. Maret tahun lalu contohnya. Saya pergi ke Surabaya. Kalau tidak karena tugas menjadi admin media sosial sebuah konferensi akbar, saya mungkin lebih memilih di rumah. Berdiam diri. Mengetik sembari berimajinasi.

Sebentar saya ingat-ingat dulu, kapan waktu tepatnya saya ke sana.

...

Saya berangkat hari Jumat, 23 Maret 2018 pagi, bersama kakak yang ngakunya juga ingin menjenguk bayi temannya yang sedang mondok di Rumah Sakit Siloam, Surabaya. Melenggang selama sekitar 7 jam dari Stasiun Lempuyangan sampai Stasiun Gubeng. Seharusnya sih, menurut jadwal dari tiket yang saya beli, perjalanan hanya memakan waktu 6 jam. Namun karena itu kelas ekonomi, kereta yang saya tumpangi kerap berhenti, memprioritaskan kelas di atasnya lewat duluan. Harap Maklum.

![Ruang tunggu Stasiun Lempuyangan](/blog/2019/04/images/lempuyangan-rmdzn.jpg) Menunggu giliran terbang di Stasiun Lempuyangan

Kota Surabaya yang ramai. Begitu pandangan saya saat menginjak kaki tangga stasiun dan memandang jalanan di depannya.

Hari itu sudah sore, tetapi kami tak segera _njujug_ hotel yang sudah dipesan lewat salah satu aplikasi populer, kami mampir ke Alfamart lebih dahulu untuk beli kopi. _Eemm_ .... sebetulnya hanya kakak saya yang beli kopi, saya hanya duduk di kursi samping pintu dan kembali memandang kesibukan warga Surabaya pulang dari kantornya.

![Jl. Stasiun Gubeng](/blog/2019/04/images/jl-stasiun-gubeng-rmdzn.jpg) Jl. Stasiun Gubeng

Warung makan pecel di sebelah musala menjadi tempat istirahat berikutnya. Kami membeli dua bungkus untuk dibawa ke hotel sebagai makan malam.

Kami menginap di J22 Jawa Hotel & Residence, tepat di depan Hotel NEO Gubeng. Hanya perlu jalan kaki sekitar 10 menit dari stasiun. Karena ingin segera merebahkan tubuh, apalagi waktu Magrib juga hampir tiba, kami bergegas menyusuri trotoar di sepanjang jalan Stasiun Gubeng. Ke jl. Sumatera dan jl. Raya Gubeng. Di persimpangan, kami belok kiri menuju jl. Blinton lalu belok kanan, berhenti di jl. Jawa tempat hotel yang dituju berada.

Kata orang-orang di teve, tata kota Surabaya luar biasa rapi dan sedap dipandang mata saat dipimpin oleh Ibu Tri Rismaharini. Kakak saya yang juga sebelumnya pernah ke sana mengakui hal itu. Di setiap sudut ada taman, penuh bunga. Saya bayangkan, bunga-bunga berwarna merah muda atau ungu berkumpul di perempatan jalan raya. Di blok berbentuk segitiga. Setiap berhenti di lampu merah, pengendara mencium wanginya, meski samar karena menguap lebih cepat akibat panasnya cuaca.

Selama tiga hari dua malam di sana, saya hanya bolak-balik antara kampus PENS (Politeknik Elektronika Negeri Surabaya) dan hotel menggunakan Gojek. Sayangnya selama itu juga saya tidak sempat menikmati suasana kanan-kiri jalan, kecuali saat melewati bunderan sebelah kampus ITS yang waw. Sebelumnya, saat jalan kaki, perjalanan turun dari stasiun sampai ke hotel aslinya juga menyenangkan, tetapi karena keburu, saya tidak menikmati lingkungan sekitar. Nah ... cerita bakal beda _dong_ ketika acara yang membawa saya ke sana akhirnya berakhir.

Saya _check out_ dari hotel hari Senin, 26 Maret, pagi bersama salah satu teman dari Jogja. Omong-omong, kakak saya sudah pulang duluan hari Minggu, siang. Kalau tak salah ingat saat itu pukul 06.30. Udara masih sangat segar meskipun mentari telah memamerkan terik yang hangat. Suasana yang cocok untuk berjalan pagi menuju stasiun.

Kalau mengikuti jalur berangkat, saya seharusnya dari hotel belok ke kanan lalu menuju jl. Blinton, tetapi saya benar-benar lupa. Saya justru mengajak teman saya untuk belok ke kiri menuju jl. Raya Gubeng. Sungguh kesalahan yang penuh berkah, walau lebih jauh beberapa menit ke stasiun, trotoar di sepanjang jalan itu keren bukan kepalang.

Jl. Jawa depan hotel itu hampir setiap hari ramai. Bukan ramai karena motor mobil berlalu lalang, tetapi karena mobil yang terparkir di kanan kiri jalan. Pemandangan para paruh baya berbincang sambil menyeruput kopi di warkop berbentuk mobil adalah hal biasa di jalan ini.

Di perempatan jalan, kami belok kanan. Jalur pedestrian sangat lebar, sekitar dua sampai tiga meter. Kursi bersekat menghiasi pojok jalan. Bagian tengah terlihat lantai penunjuk jalan atau _guiding block_ untuk penyandang tuna netra dengan lima tiang pendek untuk mencegah masuknya orang-orang yang tidak berhak berjalan di trotoar, seperti pengendara motor, di atasnya.

Trotoar pagi itu sungguh syahdu. Apalagi di bagian kiri ditanami pohon. Udara jadi terasa lebih segar.

Kami terus berjalan. Melewati gedung oranye Nur Pacific yang menjulang. Lanskap jadi sedikit menarik karena hijaunya pohon, oranyenya gedung serta tiang-tiang, dan pola kotak-kotak kehitaman pada lantai yang dipakai trotoar.

Seorang bapak pasukan kuning berleha di atas kursi, melengkapi pandangan kami akan aktivitas pagi Surabaya. Di depan, jalur pedestrian melewati sebuah taman yang sungguh hijau. Di jalur ini pula, pohon-pohon tak lagi berada di samping, tetapi di tengah dengan ukuran yang agak lebih besar.

Taman dilengkapi dengan tempat duduk serta alat bermain sekaligus dapat digunakan untuk olahraga. Beberapa lelaki berolahraga ala SKJ yang ditunggui oleh anak atau cucu kecilnya di atas kursi. Di sisi lain, seorang ibu berpakaian senam manunggui anaknya yang sedang bermain bandulan. Usut punya usut, taman yang kami lewati saat itu adalah Taman Blinton.

Kami dibantu dua lelaki berseragam keamanan untuk menyebrang ke trotoar di depan karena ramainya jalanan saat itu. Banyaknya pengemudi ojek daring di depan dan suara klakson kereta api menandakan kami sudah dekat stasiun. Perasaan antara sedih dan senang berkecamuk.

Jalur pedestrian di Surabaya sangat nyaman dirasa. Meskipun tidak semua. Trotoar di sebagian jalan Blinton misalnya, rusak. Tutup got yang miring hingga warna jalan yang kelam.

Secara keseluruhan, trotoar di Surabaya bikin ketagihan. Rasanya saya ingin berhenti menjadi pengendara motor dan menjadi pejalan kaki penuh waktu.

Pas pulang sampai Jogja, saya maunya begitu. Namun ternyata aksi untuk menjadi pesepeda lebih saya sukai daripada menjadi pejalan kaki. Hehe~

Omong-omong terima kasih buat kota indahnya, _Rek_!
