---
title: "Manusia Cinta dengan Kebrutalan dan Kekejaman, Bukan Kamu"
date: "2019-04-12"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "darah-ceprot-rmdzn.png"
aliases:
  - /post/2019-04-12-manusia-cinta-dengan-kebrutalan-dan-kekejaman-bukan-kamu/
---

![darah ceprot](/blog/2019/04/images/darah-ceprot-rmdzn.png)

_Pada dasarnya, manusia menyukai kebrutalan dan kekejaman._

Saya tidak ingin mudah menyalahkan Qabil atau Cain yang telah membunuh saudaranya sendiri. Namun sejak dulu, sepertinya manusia hobi banget dengan sesuatu yang penuh dengan kekerasan dan kekejaman.

Zaman jahiliyah, bapak ibu dengan tega langsung membunuh bayi perempuannya. Zaman keemasan tanah Jawa, Majapahit menaklukkan hampir semua wilayahnya dengan peperangan. Di seluruh dunia, tiga agama samawi juga tak luput telah melakukan hal sama. Contoh lainnya adanya Perang Dunia pertama dan kedua yang terjadi karena terpantik dari aksi kekerasan pula.

Tak perlu kabar peperangan dan diskriminasi terhadap komunitas tertentu untuk membuktikan kekejaman manusia abad ini. Lihat saja, setiap ada kecelakaan, beberapa manusia yang terekam sebagai kawan dekat atau sekadar kawan media sosial berlomba-lomba mengunggah foto korban. Otak dan usus _nyeprot_ sana sini.

Kejadian lain, pernah ada tetangga saya di WhatsApp mengunggah video di statusnya. Perempuan berbaju putih telah meninggal karena menggantung diri. _Shit_ ... saya menontonnya sepintas dari ponsel kakak yang saat itu sedang melihat status tersebut.

Esai ini muncul karena twit akun [@densa\_foxtrot](https://twitter.com/densha_foxtrot/status/1115955867008942082) yang sukses mengerjai netijen budiman dengan gambar truk berhenti, ceceran merah di samping bawah. Pada pusat ceceran, pengunggah menyensor dengan emoji besar-besar. Saya kurang tahu, siapa yang pertama memopulerkan foto jebakan tersebut.

![Twit akun](/blog/2019/04/images/truk-darah-troll-rmdzn.png) Twit @densha\_foxtrot

Bagi netijen berpikiran liar. Gambar itu pasti gambar mayat manusia terlindas truk. Ceceran merah yang menggambarkan darah. Namun ternyata bukan. Pusat gambar yang disensor adalah sirup cair botol yang jatuh pecah bersama kardusnya. Bikin sebel, kan?

Para netijen yang terjebak telanjur mengirimkan gambar itu ke teman-temannya lewat WA atau bahkan mengunggahnya sebagai status. Ada yang sengaja untuk ikut _nge-troll_ temannya, ada juga yang menganggapnya foto serius sebelum kemudian menyadari kalau ia telah tertipu. _Sialan_, umpatnya.

Orang-orang yang ikut berbagi gambar kejam itu niatnya apa sih? Ingin terlihat tahu segalanya? Ingin terlihat manusia _up to date_ sepanjang masa? Oiya, orang yang pertama kali mendokumentasikan kekejaman atau kematian itu juga niatnya apa sih? Pengin kayak om Dhandy Laksono, tetapi bagian dokumenter kematian? Atau ingin pamer kalau dia aparat berwenang yang memiliki privilese untuk jepret sana jepret situ di tempat kejadian?

Konyolnya nih. Dahulu, waktu SMA, saya adalah penonton subforum _Disturbing Picture_ (DP) di KasKus. Bahkan saya meracuni kawan sebangku untuk menonton hal yang sama. Bedanya, gambar-gambar yang kami tonton saat itu hanya menjadi obrolan internal, tidak diumbar lewat Facebook. Padahal saat itu adalah zaman awal meledaknya Facebook di Indonesia. Kami paham dong, mana candaan dan obrolan internal, mana yang perlu diumbar (mode songong).

Saya hanya menonton beberapa kali, beda dengan kawan saya yang sedikit kecanduan (maaf, Pak!). _Ada yang salah, nih!_. Ketika menonton gambar-gambar mengganggu, selain mual, batin juga gelisah. Rasa simpati berkurang. Setiap mendengar berita kecelakaan, kami justru melempar candaan gelap (_dark jokes_). _Pokoknya udah enggak beres_. Alhasil, saya menjauh dari subforum itu.

Kalau dipikir-pikir, entah mengapa saya sempat tertarik dengan DP padahal sebelumnya saya tak pernah punya niat menonton video ataupun gambar seram sejenisnya, seperti keributan Sampit dan Madura yang viral awal milenium lalu.

Lalu, benarkah manusia suka dengan kekejaman bukan kamu?

Mungkin. Toh film-film perang dan kekerasan luar biasa laku. Begitu juga dengan gim-gim serupa (GTA salah satunya).

Tidak hanya kekejaman kepada sesama manusia, orang-orang sepertinya juga menikmati kekejaman terhadap makhluk astral. Di zamannya, tayangan Pemburu Hantu sangat laku. Coba kalau hantu-hantu itu berwujud fisik, mungkin setelah dihantam oleh para kyai, jenggot G_deruw_ yang copot dengan mata bonyok akan difoto dan dibagikan lewat media sosial. S_nd_l B_long yang tambah bolong mungkin akan direkam pula. K_nti yang sedang terbang, tiba-tiba terjatuh ditarik pak Kyai, disorot lalu diedit dengan tulisan kredit _Directed by Robert B. Weide_.

Psychoooooo ...

\[Gambar oleh Clker-Free-Vector-Images di Pixabay\]
