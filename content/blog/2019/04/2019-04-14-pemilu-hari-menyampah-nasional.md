---
title: "Pemilu, Hari Menyampah Nasional"
date: "2019-04-14"
categories: 
  - "pemilu"
tags:
  - "cuap"
coverImage: "sampah-apk-pemilu-rmdzn.jpg"
aliases:
  - /post/2019-04-14-pemilu-hari-menyampah-nasional/
---

![sampah pemilu](/blog/2019/04/images/sampah-apk-pemilu-rmdzn.jpg)

_Nyampah di Pemilu, budaya menjijikan yang tak segera terhapuskan dari muka bumi ini._

Masyarakat Indonesia akan menghadapi pesta demokrasi lima tahunan besok Rabu, 17 April. Sebelum Minggu, 14 April--hari tenang Pemilu--semua media memamerkan hingar bingar kampanye. Foto calon legislatif DPR RI, DPRD Provinsi, dan DPRD Kabupaten/Kota berjejer "menghiasi" pojok lampu merah, gang mini dekat sawah, jembatan kumuh dan mewah, hingga perempatan kampung sebelah. Foto calon senator juga tak kalah menghiasi hampir semua sudut kota dan desa.

Ada satu hal yang luput diperhatikan oleh masyarakat dari hingar bingar ini. Sampah. Ya, sampah. Bukan sampah visual kasatmata itu yang saya maksud, tetapi sampah dari bahan spanduk yang dipaku di bambu atau dikawat di tiang listrik. Mayoritas bahan yang dipakai sebagai spanduk berbahan anti air itu sungguh membahayakan lingkungan.

[Kata Leonard Simanjuntak, kepala Greanpeace Indonesia,](https://tirto.id/mari-bikin-caleg-jera-agar-tak-pasang-atribut-kampanye-seenaknya-dhya) bahan APK (Alat Peraga Kampanye) tergolong kategori plastik berbahan kimia. Ia seperti bahan-bahan plastik lainnya, terdaur ulang secara alami 400 sampai 1.000 tahun mendatang. Ini fakta menyebalkan pertama.

Fakta menyebalkan kedua. Seharusnya pemasanglah yang mencopotnya. Siapa yang melakukan, dialah yang bertanggung jawab. Dalam kasus APK, tim sukses caleg dan Presiden/Wapres bersangkutanlah yang wajib melepasnya sendiri. Namun apa kabar? Seperti yang terdahulu, Bawaslu dan Satpol PP-lah yang akhirnya menurunkan APK-APK tersebut.

Padahal, [Tirto.id](https://tirto.id/mari-bikin-caleg-jera-agar-tak-pasang-atribut-kampanye-seenaknya-dhya) melansir, alur pencopotan APK yang melanggar aturan ini cukup panjang. Intinya begini. Setiap ada dugaan pelanggaran, pihak Bawaslu memverifikasi tempat dan bentuk pelanggaran. Setelah itu mereka memberikan rekomendasi kepada Bawaslu tingkat kalurahan dan kecamatan untuk turun langsung mencopotnya. Sebelum menurunkan APK, Bawaslu lebih dulu menghubungi tim pelanggar; caleg maupun calon Presiden/Wapres. Sayangnya, segudang alasan muncul dari tim pelanggar untuk tidak langsung menerima telepon dari Bawaslu.

Matilah kita.

Bawaslu menurunkan ratusan APK yang seharusnya sudah dilepas oleh tim sukses caleg atau Presiden/Wapres masing-masing. Tim sukses terkesan _njagakke_ (menggantungkan) Bawaslu yang akan mencopotkan APK milik mereka. Seperti kasus ayam dan telur. Tidak jelas mana yang mulai duluan.

Saya sudah membuktikan bagaimana cueknya tim sukses caleg dan Presiden/Wapres atas APK yang mereka pasang. Ini sudah masa tenang. Semua APK harus dicopot. Namun saya cukup bergerak tak sampai radius 1Km dari rumah, APK berukuran mini hingga sedang masih terpasang "rapi"--tak tersentuh.

Sebagai anggota Pengawas TPS, saya diberi hak untuk ikut membantu Panitia Pengawas Pemilu Panwaslu) Kecamatan mencopoti APK-APK di lingkungan TPS dan sekitarnya. Setelah sampah APK terkumpul, tidak ada mekanisme khusus ke mana sampah itu harus dibuang. Pencopot diberi hak untuk memanfaatkan kembali APK yang sudah tidak terpakai, kalau sudah tidak dimanfaatkan, yaaa, buang.

Saya bahagia ketika mendapati kabar adanya perusahaan yang memiliki proyek pembuatan tas menggunakan spanduk tak terpakai. [Parongpong](https://www.instagram.com/parong.pong/) namanya. Perusahaan yang bergerak di bidang nol sampah (_zero waste_) ini berpusat di Bandung tapi beberapa waktu terakhir mereka menyediakan _drop point_ di Jakarta dan, tentu saja, Yogyakarta!

Ini jadi angin segar. Kalau spanduk caleg dan Presiden/Wapres masih sehat (tidak sobek) dan tidak ada orang lain yang ingin memanfaatkan, _drop point_ Parongpong di Yogyakarta dapat menjadi rujukan. Selain lewat surel, saya akan kembali mengatakan di sini. **Terima kasih, Parongpong!**

_Drop point_ Parongpong di Yogyakarta dapat ditemukan di **Jl. Bener TR II/IV No. 39 RT 10 RW 03, Tegalrejo, D.I Yogyakarta**.

!["Cerita" di akun Instagram Parongpong](/blog/2019/04/images/parongpong-jogja-rmdzn.png) Parongpong di Yogyakarta

Semoga model konvensional kampanye dapat segera terhapuskan, digantikan dengan media digital luring maupun daring. Kalaupun tidak bisa, semoga para pemasang APK punya kesadaran tinggi untuk mencopotnya sendiri dan bertanggung jawab dengan sampahnya agar tak membahayakan lingkungan.

Perlu ditegaskan di sini: Pemilu jangan lagi menjadi ajang berlomba memproduksi sampah.

**\[Tambahan\]**

Sedikit ada kekeliruan pada penulisan alamat. _Drop point_ Parongpong di Yogyakarta dapat ditemukan di **Jl. Bener TR IV No. 39 RT 10 RW 03, Tegalrejo, D.I Yogyakarta** (lihat pada bagian **TR**). Rumah baru, sehingga warga sekitar belum tentu tahu kalau kita bertanya kepada mereka. Saya sudah mengalami itu.

Tanggal 16 April, saya pergi ke Tegalrejo, bahkan sudah sempat bertanya kepada tujuh orang warga dari RT sebelah dan dua orang warga yang tinggal di rumah no. 37, RT 10 RW 03. Mereka tak tahu. Tanggal 19 April, saya kembali lagi ke sana dengan petunjuk yang sudah disampaikan oleh admin Gifood dan Sedekah Baju Yogyakarta selaku rekanan Parongpong lewat surel maupun lewat kontak WhatsApp.

![Markas Sedekah Baju Yogyakarta](/blog/2019/04/images/spanduk-sedekah-baju-yogya-resized-rmdzn.jpg) Markas Sedekah Baju Yogyakarta. Rekanan Parongpong, tempat drop point spanduk bekas untuk diolah kembali menjadi barang bermanfaat

![Spanduk bekas kampanye yang telah diikat](/blog/2019/04/images/spanduk-untuk-diolah-parongpong-resized-rmdzn.jpg) Spanduk bekas kampanye

Terima kasih, mas Fathin Naufal dan mbak Azka!
