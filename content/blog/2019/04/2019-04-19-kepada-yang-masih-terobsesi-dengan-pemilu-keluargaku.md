---
title: "Kepada yang Masih Terobsesi dengan Pemilu, Keluargaku"
date: "2019-04-19"
categories: 
  - "cerpen"
tags: 
  - "cerpen"
coverImage: "catur-rmdzn.jpg"
aliases:
  - /post/2019-04-19-kepada-yang-masih-terobsesi-dengan-pemilu-keluargaku/
---

![catur](/blog/2019/04/images/catur-rmdzn.jpg)

Aku pulang dari sebuah petualangan pendek. Ingin bersender di lincak yang biasa kujadikan sebagai alat bersantai. Aku termenung. Gelas di bawahku masih saja kosong. Kopi susu yang tersaji di dapur terasa menjadi sebuah harta berharga untuk segera dikonsumsi.

Sayang sekali. Aku merasa berat melangkah saat itu. Ruang keluarga yang terjangkau oksigen terasa sumpek. Ibu menyetel video YouTube keras-keras dari ponsel Android. Sebuah suara lelaki sibuk meneriakkan, "_Kawal pemilu!!!_". Beberapa menit kemudian, lelaki lain menyebut sebuah kata berulang kali dengan cara berteriak.

Aku mual. Memilih untuk kabur ke depan rumah. Namun, bapak yang sedari tadi memegang ponsel itu juga menyetel video keras-keras. Seperti video yang dipotong-potong dari berita kemudian disatukan menjadi sebuah video saling tuduh. Enek banget.

Sudah lama aku tidak mengintip grup WhatsApp keluarga, bukan karena tak peduli, tetapi lebih untuk menjaga kesehatan mentalku sendiri. Saat era politik begini. Hampir setiap hari, grup WhatsApp keluarga dipenuhi dengan unggahan seputar pemilihan pemimpin negeri. Lebih umumnya tentang politik. Bahkan kebisingan politik ini masih terjadi hingga pasca Pemilihan Umum bulan April ini.

Bukankah Pemilu sudah berlalu? Seharusnya kehidupan kembali seperti dahulu. Dahuluuu ... sejauh kehidupan lima tahun lalu.

Aku bukan seorang perangkul keluarga. Meski begitu, aku merasakan, dunia perpolitikan menjadikanku semakin tak mampu masuk erat ke dalam lingkungan tempatku lahir.

Mengapa bapak begitu terobsesi dengan pemilihan presiden? Padahal selama ini dunia politik tak begitu menarik bagimu. Apa mungkin Pemilu kali ini berhasil memuaskan egomu? Ego untuk membuktikan bahwa dunia akan kiamat jika satu kandidat mengalahkan kandidat lainnya. Aku ingat, dalam sebuah perjalanan menuju suatu tempat, bapak membahas dua calon presiden dengan berapi-api, sedangkan aku lebih pilih menunduk, membaca buku yang kusukai.

Ibu juga begitu. Setiap hari menyetel video tentang keributan Pemilu. Padahal lima tahun lalu tak seperti itu. Apa perlu, aku menyalahkan ponsel modern yang kamu miliki itu? Aku merasa sedih, ponsel yang kini dapat dijadikan sebagai media untuk bereuni dengan teman lama ternyata malah melupakanmu dari kehidupan nyata. Ayo, Bu, lupakan saja itu. Kalau terus-terusan menikmati segala kericuhan dan kontroversi di dunia politik, kutakuti darahmu yang sudah tinggi akan semakin tinggi.

Bolehkah kusampaikan kepada anggota keluarga lainnya?

Pesta demokrasi memang terasa seperti pasar malam, satu gerombolan orang ingin bergerak ke sudut tertentu, satu gerombolan lain pergi ke sudut yang lain. Seharusnya, jika hasilnya tak sesuai yang kauharapkan, jangan menuduh yang bukan-bukan. Sebaliknya, jika hasilnya sesuai yang kauinginkan, jangan mencela orang-orang yang tak sepaham.

Tahu tidak saat kalian memancing obrolan politik denganku? Aku diam, bukan? Aku juga mungkin terlihat seperti masa bodoh dan tak peduli? Benar sekali. Aku memang menganggap dunia politik tak secuprit itu, tapi juga tak menganggap dunia politik harus dibahas terlalu serius hingga mengorbankan kesehatan mental dan hubungan di dunia nyata. Denganmu, saat ini aku anggap dunia politik sama sekali tidak penting.

Maafkan anggota keluargamu ini yang tak pandai berbicara.

Oleh anak dan ponakanmu yang mungkin besok sudah jadi batu.
