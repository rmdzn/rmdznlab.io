---
title: "Info yang Tak Lengkap Itu Menjebak Calon Pemilih"
date: "2019-04-23"
categories: 
  - "pemilu"
tags:
  - "cuap"
coverImage: "photo_2019-04-23_16-29-16.jpg"
aliases:
  - /post/2019-04-23-info-yang-tak-lengkap-itu-menjebak-calon-pemilih/
---

![panitia pemilu](/blog/2019/04/images/photo_2019-04-23_16-29-16.jpg)

_Banyak berita menyesatkan, menjebak calon pemilih dari luar kota._

Pagi menuju siang. Saya mengintip penunjuk waktu di ponsel, saat itu angka berada di sekitaran pukul 10.00 WIB. Ponsel lagi-lagi berbunyi _cengklang-cengklung_, belasan bahkan puluhan notifikasi WhatsApp muncul dari grup Pengawas TPS (PTPS).

Beberapa rekan seperjuangan tengah curhat, TPS yang ia awasi diserbu oleh mahasiswa berdokumen A5 maupun yang tidak, serta penduduk dari kota lain yang juga berdokumen A5 maupun yang tidak.

Dalam batin saya berceletuk, _ah ... masa sih_.

Selisih sekitar satu jam setelah itu. Kejadian yang sama di TPS lain terjadi di TPS yang saya awasi. Seorang bapak pendatang yang tinggal di kampung itu bertanya kepada saya jikalau dirinya dapat ikut mencoblos hanya dengan menunjukkan KTP-el (KTP elektronik), setelah sebelumnya ia mengaku tidak memiliki dokumen A5. Dengan sangat bersemangat, bapak itu menunjukkan tangkapan layar status Facebook dan sebuah berita di Kumparan bahwa kita dapat memberikan suara hanya dengan menunjukkan KTP-el.

_This is the real job!_

Ya, selain mengawasi TPS dan kinerja KPPS, saya juga harus memberikan pengertian kepada warga yang kebingungan apakah mereka dapat mencoblos atau tidak.

Pendeknya, saya memberi tahu bapak itu seperti ini.

Di Pemilu 2019, terdapat tiga jenis pemilih; DPT (Daftar Pemilih Tetap) adalah pemilih yang sudah tercatat sejak awal, DPTb (Daftar Pemilih Tambahan) adalah pemilih yang pindah TPS menggunakan A5, dan DPK (Daftar Pemilih Khusus) adalah pemilih yang dapat memilih hanya menggunakan KTP-el dengan syarat lokasi TPS masih sealamat dengan KTP-el calon pemilih. DPK pun hanya boleh mencoblos mulai jam 12.00.

Kami sempat _ngotot-ngototan_, walaupun akhirnya si bapak mau mengerti. Terima kasih, Bapak, saya hanya menjalankan tugas.

Di seberang pagar, dua polisi yang sedang dikerubungi oleh mahasiswa-mahasiswa perempuan mengayunkan tangannya ke arah saya. _Mas ini dari Bawaslu, coba tanya ke dia, mbak_, kata pak polisi kepada mahasiswa itu.

Mereka, para mahasiswa yang ternyata adalah tetangga kontrakan sebelah menanyakan hal yang sama. Apakah mereka dapat ikut mencoblos siang itu. _Kalau mbak-mbak tidak memiliki A5, tidak bisa mbak_, kata saya menerangkan.

Kepada mereka, saya sampaikan minta maaf saat itu juga. Sedari tadi banyak orang yang semangat menyumbangkan suaranya, sayangnya terkendala dengan dokumen yang tidak lengkap. A5, maksudnya. Saya juga menyampaikan, berita tentang bolehnya memilih dengan KTP-el itu juga tidak lengkap, menjebak.

Cukup banyak warga dan mahasiswa non-DPT yang datang setelah itu. Secara umum, suasana tetap terkendali. Saya sangat puas dengan pelayanan pemilih di TPS yang saya awasi. Satu kelompok mahasiswa berdokumen A5, sukses mencoblos pada detik-detik terakhir TPS ditutup, begitu juga seorang bapak dari timur dan juga sepasang suami istri. TPS kami tidak kehabisan surat suara.

Di TPS lain, para pemegang A5 mayoritas kehabisan surat suara, padahal mereka sudah tercatat sebagai DPTb di dokumen C7. Saya senang, setelah mereka menuntut haknya ke KPU, KPU mengizinkan para pemegang A5 untuk mencoblos di lain waktu. Kabarnya, beberapa TPS di desa akan mengadakan PSL (Pemungutan Suara Lanjutan) untuk para pemegang A5 yang sudah tercatat di C7.

Bagi KPPS (Kelompok Penyelenggara Pemungutan Suara) dan PTPS setempat, semangat _nggih_, Pak/Bu!

\[Gambar utama: pengambilan sumpah KPPS\]
