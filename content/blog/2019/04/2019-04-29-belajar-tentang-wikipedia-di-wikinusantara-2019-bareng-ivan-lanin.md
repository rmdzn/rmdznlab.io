---
title: "Belajar Tentang Wikipedia di WikiNusantara 2019, Bareng Ivan Lanin!"
date: "2019-04-29"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "photo_2019-04-29_16-28-53.jpg"
aliases:
  - /post/2019-04-29-belajar-tentang-wikipedia-di-wikinusantara-2019-bareng-ivan-lanin/
---

_Belajar tentang Wikipedia bareng Ivaaaannnn Laaannnniinnn~~_

Selama dua hari, tanggal 27 sampai 28 April 2019, Wikimedia Indonesia menyelenggarakan [WikiNusantara 2019](https://meta.wikimedia.org/wiki/WikiNusantara_2019), sebuah konferensi nasional Wikimedia pertama di Indonesia. Tepatnya diadakan di [Hotel Innside by Melia](https://meta.wikimedia.org/wiki/WikiNusantara_2019/Tempat), Yogyakarta.

Konferensi ini ditujukan untuk para sukarelawan (tentu saya tidak termasuk di dalamnya). Saya hanya ingin bercerita, di sela-sela konferensi, Wikimedia mengadakan acara untuk umum yang mengundang Ivan Lanin sebagai narasumbernya. Sesuai judul, _Urun Daya Pengetahuan dalam Wikipedia_, acara tersebut membahas laman ensiklopedia bebas terbesar di dunia--Wikipedia. Pokoknya menarik.

Siang itu cukup panas. Di ponsel, prediksi suhu cuaca menunjukkan angka 31 derajat Celcius. Tak masalah. Cukup _gowes_ sebentar, saya sudah sampai di tempat parkir hotel sebelum acara dimulai pukul 13.30 WIB.

Lokasi acara di Ruang Yogyakarta atau dari tempat parkir silakan jalan ke utara. Di situ ada lubang pintu dengan tulisan "ke lobi" pada bagian dinding. Setelah masuk lobi jalan ke kanan (timur), lalu belok ke kiri. Ruang Yogyakarta ada di pojok situ. Tim Wikimedia Indonesia sudah siaga di balik meja, beberapa lembar kertas bertuliskan para peserta menumpuk di atasnya. Di sisi lain meja, terdapat stiker Wikipedia Jawa dan Wikimedia Indonesia untuk bebas diambil oleh peserta.

Ruang terasa dingin. Di belakang terdapat meja panjang, beberapa laptop terpajang di atas--mungkin alat kerja panitia penyelenggara konferensi. Tiga spanduk kecil berdiri di kiri ruang. Spanduk yang mengajarkan lisensi CC, macam-macam proyek Wikimedia, dan penjelasan tentang Wikipedia.

Saya langsung menjepret beberapa sisi, termasuk panggung melalui kursi peserta, lanjut duduk anteng menunggu acara dimulai.

![Tiga spanduk kecil di ruangan samping](/blog/2019/04/images/photo_2019-04-29_16-29-25.jpg) Spanduk CC, Wikipedia, dan Wikimedia

"Urun daya", dua kata yang tertera dalam judul acara adalah padanan dari _crowdsourcing_. Padanan tersebut berasal dari usulan seorang pegiat internet, [Donny B U](https://mobile.twitter.com/donnybu/), dari Internet Sehat.

Uda Ivan Lanin, begitu panggilan akrabnya, kala itu adalah seorang pemrogram komputer. Pada suatu waktu, ia justru menghabiskan harinya untuk menulis dan menyunting artikel di Wikipedia alih-alih menulis kode nan jlimet. Bayangkan, dari pukul 06.00 sampai 12.00 (atau 24.00?). Ada keinginan sederhana yang ditanamkan oleh Uda, ia ingin ketika anaknya sudah SMP, semua tugasnya dapat dicari lewat Wikipedia dan itu terwujud!

Saking rajinnya menulis di sana, ada manfaat yang ia peroleh secara cuma-cuma. Uda Ivan semakin mahir bahasa Indonesia hingga menjadi pencinta sejatinya. Wikipedia benar-benar mengubah hidup pemilik akun Twitter @IvanLanin ini. Hingga suatu hari, ia menyebut diri sebagai _Wikipediawan pencinta bahasa Indonesia_. "_Itu adalah identitas_," ungkapnya.

![Panggung WikiNusantara 2019](/blog/2019/04/images/photo_2019-04-29_16-29-14.jpg) Panggung WikiNusantara 2019 dari arah barat laut

Wikipedia Indonesia tumbuh cukup pesat, dari yang hanya 18 ribuan tulisan pada tahun 2006, sekarang sudah sampai 400 ribu lebih. Wikipedia dengan bahasa daerah atau bahasa yang dipertuturkan di Indonesia juga ikut tumbuh, meskipun ada yang cepat ada yang lambat. Ada Wikipedia Melayu, Gorontalo, Aceh, Tetun, Jawa, Sunda, Minangkabau, Bugis, Banyumasan, dan Banjar. Pertumbuhan artikel paling cepat ada pada Wikipedia Jawa, Sunda, dan Minangkabau.

![Daftar Wikipedia dengan bahasa-bahasa yang ada di Indonesia](/blog/2019/04/images/bahasa-di-wikipedia-rmdzn.jpg) Daftar bahasa Wikipedia di Indonesia

Dahulu, uda Ivan memiliki ambisi besar untuk menjadikan Wikipedia sebagai rujukan utama. Namun ternyata itu tidak mungkin, katanya siang itu. Kini ambisi berubah, ia bersama Wikipediawan lain ingin menjadikan laman ensiklopedia daring (_online_) ini sebagai rujukan awal utama. Maksudnya, ketika menuju sebuah artikel di Wikipedia, pengguna dapat mengakses catatan-catatan kaki yang tertera di bawahnya.

Tak lupa, selain memperkenalkan Wikipedia melalui salindia (_slide_), ia juga membahas tesis yang pernah digarap berjudul [_Strategi Peningkatan Kualitas Ilmiah Wikipedia Bahasa Indonesia_ \[PDF\]](https://drive.google.com/file/d/0B2RJ4tG4sxtvTVdfU3M1WWRyNHc/view). Uda Ivan memang memiliki keinginan untuk meningkatkan kualitas ilmiah konten Wikipedia dengan cara bekerja sama dengan ahli-ahli di luar sana. Namun itu belum dapat terlaksana.

Di samping membahas Wikipedia, Uda Ivan juga sempat membahas istilah bahasa Indonesia. Seperti penggunaan _pranala_ yang berarti sungai kecil sebagai padanan _link_ pada Wikipedia dan sebuah proposal kata _lingko_ sebagai padanan dari _commons_. [_Lingko_](https://kbbi.kemdikbud.go.id/entri/lingko) memiliki arti tanah adat yang digunakan secara bersama-sama, sepaham dengan kata _[commons](https://www.merriam-webster.com/dictionary/commons)_ yang menggambarkan hal sama.

![Moderator dan Ivan Lanin di atas panggung WikiNusantara 2019](/blog/2019/04/images/photo_2019-04-29_16-28-48.jpg) Ivan Lanin di panggung WikiNusantara 2019

Hampir mendekati sesi akhir, panitia memperkenankan tiga penanya, oh maaf ... penanggap maksud saya (mengikuti kata uda Ivan kemarin). Penanggap pertama menyampaikan, alih-alih meminta bantuan langsung kepada ahli, bagaimana jika Wikimedia Indonesia mengumpulkan Wikipediawan yang selama ini bergerak di bidang tertentu dalam kelompok yang sesuai bidang tersebut. Sulit, kata uda Ivan, apalagi jika mengingat beratnya Badan Bahasa mengumpulkan para ahli untuk dijadikan rujukan. "_Itu \[yang mengumpulkan\] Badan Bahasa, lo ya. Dari negara langsung. Apalagi kalau sukarela seperti kita, pasti \[mengumpulkannya\] lebih sulit_," begitu kurang lebih tanggapan Uda.

Penanggap kedua, seorang penulis lepas di bidang gaya hidup, meminta saran kepada Uda Ivan tentang cara memopulerkan istilah bahasa Indonesia menggantikan istilah bahasa Inggris yang telanjur terkenal di kalangan muda. Kata Uda, ada beberapa tips, penulis dapat menuliskan kata bahasa inggris yang telanjur populer diikuti dengan padanan bahasa Indonesianya. Contoh, "email atau surel". Bisa juga memakai tanda kurung, "email (surel)". Pembaca akan mendapatkan dua info sekaligus; info tentang isi artikel itu sendiri dan info tentang padanan kata.

Sayangnya saya lupa apa yang dibicarakan oleh penanggap ketiga. Jadi saya lewati dulu ... =)

Wikipedia menjadi salah satu bukti besar penerapan sistem urun daya, setelah Jeff Howe memopulerkan istilah ini (_crowdsourcing_) pada tahun 2006. Tepuk tangan untuk itu.

Pada acara yang hanya berlangsung sekitar satu jam itu, peserta yang sudah berkontribusi di Wikipedia diharapkan dapat meningkatkannya, sedangkan peserta yang sama sekali belum pernah berkontribusi di sana, diharap segera mencobanya.

Pukul 14.30 lebih sedikit, acara rampung. Peserta konferensi melanjutkan agenda-agendanya, peserta yang datang dari luar kembali ke kesibukan lainnya.
