---
title: "Bagaimana Kalau Buka Puasa dengan Kurma dan Air Putih Saja?"
date: "2019-05-09"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "nasi-vinay-gupta-372394-unsplash-rmdzn.jpg"
aliases:
  - /post/2019-05-09-bagaimana-kalau-buka-puasa-dengan-kurma-dan-air-putih-saja/
---

![nasi](/blog/2019/05/images/nasi-vinay-gupta-372394-unsplash-rmdzn.jpg)

_Ramadan tiba, ramadan tiba, tiba-tiba ramadan, tiba-tiba ramadan~~_

Bulan Juni 2019 ini, kita memasuki bulan Ramadan. Beragam aktivitas yang dilakukan satu tahun lalu kembali dilakukan tahun ini. Dini hari sahur. Pagi hingga sore puasa sambil beraktivitas normal. Menjelang magrib berbuka puasa, entah sendiri atau bareng rombongan. Malamnya tarawih dilanjut tadarus Alquran. _Masyaallah ya akhii wa ukhtii ..._

Khusus saat berbuka puasa, orang-orang memiliki cara yang berbeda-beda saat menikmatinya. Ada yang bersantap di rumah bersama keluarga. Ada yang beramai-ramai di angkringan atau restoran bersama teman SMA ... kalau tidak wacana. Ada yang di masjid bertugas menyiapkan persiapan buka bersama selagi menyandang status sebagai pemuda.

Dari sekian alternatif, hampir setiap tahun saya melakukan cara terakhir: menyiapkan makanan dan minuman dengan pemuda lain untuk berbuka puasa bersama para tetua dan anak-anak kecil di masjid.

Ya, menyenangkan, meskipun lelah.

Masih untung kalau lelah itu terbayarkan dengan sajian makanan yang terlahap habis, tetapi kalau banyak sekali makanan yang terbuang. Sungguh itu menjadi pemantik emosi tersendiri.

Sebelum ke bahasan itu, saya ingin menjelaskan sedikit. Hanya ada dua jenis masjid di NKRI (Negara Kesatuan Republik Indonesia) pada bulan Ramadan. Masjid yang menyediakan menu nasi kotak berikut cemilan dibungkus plastik dan masjid yang menyediakan menu makanan menggunakan piring atau mangkok.

Di Yogyakarta, jenis pertama tergambar pada masjid-masjid besar, seperti Masjid Gedhe Kauman dan Masjid Jogokariyan. Biasanya, warga sekitar masjid memang berniat ingin beramal melalui aktivitas mempersiapkan buka bersama dan membersihkan alat-alat makan minum. Kompak. Bagi lingkungan, itu juga menguntungkan. Tak perlu ada sampah plastik dan kertas menumpuk seperti yang terjadi pada masjid jenis kedua.

Masjid jenis kedua umumnya tergambar pada masjid-masjid kecil seperti masjid yang ada di kampung saya. Menu-menu berbuka tersaji dalam bungkus plastik atau kotak kertas. Selain karena kebiasaan, bentuk pengemasan menu seperti itu sama dengan efisiensi tenaga; tenaga donatur (penyedia menu berbuka) dan tenaga penyaji menu ke depan jemaah (pemuda). Efisiensi tenaga juga terlihat setelah buka bersama rampung, hanya perlu beberapa orang saja untuk mengumpulkan sampah, mencuci gelas, baki, dan mengepel lantai yang terguyur tumpahan kuah.

Payahnya, masjid jenis kedua sangat tak ramah lingkungan. Sampah plastik dan kertas menumpuk. Berdasarkan kotak makanan bekas yang kami kumpulkan kemarin, dalam sehari dapat terkumpul satu sampai tiga kilogram sampah. Karena kami hanya mengumpulkan sampah yang berpotensi didaur ulang, secara otomatis sampah-sampah plastik yang tak bernilai ekonomis dibuang. Menumpuk di tong sampah dan "mengalir" mencemari lingkungan.

Isu sampah yang dihasilkan oleh masjid kedua tak kalah pelik jika dibandingkan dengan isu limbah makanan (_food waste_) yang dihasilkan kedua jenis masjid. Ya, kedua jenis masjid pasti memiliki isu yang sama pada ranah ini. Orang-orang di Indonesia masih terbiasa membuang-buang makanan meskipun mereka diberikan izin untuk mengambilnya sesuai porsinya sendiri (baca: prasmanan di acara pesta pernikahan). Berdasarkan sampah yang kami kumpulkan pasca buka bersama di masjid hari Rabu, 9 Mei 2019, limbah makanan terkumpul sekitar 1-2kg.

Menurut [laporan yang diterbitkan tahun 2016](http://foodsustainability.eiu.com/whitepaper/), Indonesia menjadi produsen limbah makanan terbanyak ke dua di dunia. Arab Saudi berada di peringkat pertama. Sangat tidak adil kalau membayangkan [13 juta ton](https://www.hipwee.com/feature/miris-indonesia-itu-penyampah-makanan-terbanyak-no-2-di-dunia-padahal-masih-banyak-yang-kelaparan/) makanan terbuang percuma, padahal ia dapat dipakai untuk memberi makan 11 persen populasi negeri ini.

Limbah makanan juga mengancam lingkungan. Mencemari udara dengan bau busuknya dan mencemari tanah serta air dengan zat yang mereka keluarkan. Selain berdampak langsung terhadap lingkungan, berapa besar sumber daya yang terbuang percuma untuk mengolah makanan yang akhirnya terbuang itu? Banyak!

Apa iya pengelolaan menu buka bersama masjid begini terus? Memproduksi sampah dan/atau limbah dalam sekali waktu. Isu seperti ini perlu didengungkan hingga menciptakan solusi terbaik. Usul saya, menu berbuka puasa di masjid lebih baik menggunakan piring. Dengan syarat, tenaga penyaji dan tenaga cuci-cuci alat makan juga cukup. Menu yang disajikan pun sedikit saja agar produksi limbah makanan mendekati nol.

Solusi tercepat, bagaimana kalau berbuka puasa dengan kurma dan air putih saja? Tidak perlu makan besar. Cukup bermodalkan gelas dan alas tak sekali pakai berukuran setatakan kopi. Bukankah puasa itu ibadah sedangkan mencemari lingkungan dan berperut kenyang itu tidak?

_\[Foto utama oleh Vinay Gupta di Unsplash\]_
