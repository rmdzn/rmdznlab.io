---
title: "Pada Dasarnya Semua Orang Adalah Pencerita"
date: "2019-05-25"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "book-2929646_1920-rmdzn.jpg"
aliases:
  - /post/2019-05-25-pada-dasarnya-semua-orang-adalah-pencerita/
---

![buku](/blog/2019/05/images/book-2929646_1920-rmdzn.jpg)

_Menggosip, mengumbar rasa, mendongeng, men~~_

Saya melirik monitor di meja sebelah. Rona Mentari, pendongeng muda lihai dari Yogyakarta, muncul di layar. Ingatan saya langsung melompat pada acara Pildacil (Pemilihan Dai Cilik) beberapa tahun lalu saat Rona menjadi peserta bersama boneka kesayangannya--Trimbil.

Di sebuah acara teve, ia mempraktikkan kemampuannya mendongeng di depan penonton sedangkan dua pembawa acara, Vincent dan Desta, memeragakan cerita di panggung terpisah. Pembawaan Rona di depan kamera membuat saya kagum. Gayanya bercerita membuat saya jatuh hati ikut menikmati dongeng yang disampaikan. Kata Rona di acara yang sama, dongeng memang tidak hanya dapat dinikmati anak-anak, tetapi juga orang dewasa, bahkan eyang-eyang di panti jompo yang pernah ia sambangi.

Saya jadi teringat beberapa tahun lalu ketika mengisi acara untuk anak-anak sebelum berbuka puasa bersama di masjid. Saya menceritakan kisah salah satu nabi, kalau tak salah ingat. Namun anak-anak tetap ramai, ribut sendiri. Hanya secuil saja yang mendengarkan.

Dasar saya yang memang tak pandai membawakan cerita. Datar dan hampir tanpa intonasi.

Sulit.

Tetangga yang hingga hari ini menjadi pencerita sebelum buka puasa sepertinya juga kesulitan menarik perhatian anak-anak. Yaa, ada _sih_ yang memperhatikan pencerita dan tentu jumlahnya lebih banyak dari pada saat saya bercerita.

Bercerita atau _storytelling_ memang tidak mudah. Perlu terus dilatih dan diasah hingga menjadi pendongeng profesional seperti Rona. Meski begitu, pendiri Rumah Dongeng Mentari di Yogyakarta ini meyakinkan khalayak bahwa semua orang adalah pencerita.

Seorang bapak pulang dari kantor sembari mengeluh karena kertas kerja menumpuk tak karuan di meja. Seorang adik kecil pulang ke rumah mengabarkan sang ibu bahwa nilai ujiannya sempurna. Rombongan ibu-ibu bergosip di balik gerobak sayur adalah bukti bahwa orang-orang pada dasarnya adalah pencerita. Tentang mengapa cerita begitu menarik bagi lawan bicara adalah topik tersendiri yang kadang dapat diduga--gayamu datar dan tak istimewa.

\[Gambar utama oleh Jonny Lindner di Pixabay\]
