---
title: "Terguncang 27 Mei"
date: "2019-05-28"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "gempa-rmdzn.jpg"
aliases:
  - /post/2019-05-28-terguncang-27-mei/
---

![gempa](/blog/2019/05/images/gempa-rmdzn.jpg)

_Pagi yang syahdu, sebelum itu terjadi._

Hari Sabtu pagi, suasana begitu biasa. Kokokan ayam menyambut udara nirpolutan.

Kaki yang turun dari kasur menjadi awal langkahku menuju kamar mandi. Mengambil air wudu, salat, lalu kembali ke kamar mandi untuk melakukan ritual pagi.

Aku masih SMP. Hari Sabtu berarti hari untuk memakai seragam identitas sekolah--warna hijau. Setelah mengeringkan tubuh, aku bergegas mengenakan baju seragam dengan lilitan handuk sebagai pengganti celana.

Aku masih sibuk mencari celana. Ketika melihat jam dinding yang jarumnya menunjuk sekitar angka enam. Atap dan dinding berkeretak. Lantai yang kupijak goyang semakin keras.

Gempa!

Anggota keluarga, selain ibu, yang masih _gegoleran_ di kasur, bangun. Kami bertiga langsung lari ke belakang, lewat dapur dengan lantai masih bergoyang ke kiri dan kanan. Itu adalah satu-satunya catatan waktu terburuk kami untuk membuka gerendel kunci.

Tetangga sudah berkerumun di luar rumah. Sibuk menebak dan menduga apa yang sedang terjadi.

Aku masih memakai handuk. Ingin sekali segera menggantinya dengan celana. Beberapa kali bimbang mau ambil celana karena takut masuk rumah. Namun aku merasa harus melakukannya.

Isi rumah berantakan. Lemari ambruk. Beberapa botol sirup cair jatuh pecah. Aku bergegas memakai celana dengan posisi tubuh siap lari jika terjadi gempa susulan lagi.

Semua orang bertebaran di jalan kampung. Awalnya sih suasana tidak tegang meskipun gempa susulan beberapa kali terjadi, tetapi suasana berubah ketika jalan lingkar (RingRoad) utara yang terdiri dari empat jalur (dua jalur lambat ke utara dan ke selatan serta dua jalur cepat ke utara dan ke selatan) menjadi satu jalur saja. Semua pengendara motor dan mobi ke arah utara. Hoaks menyebar, katanya air dari laut selatan naik. Berbanding terbalik dengan jalan di wilayah utara, mereka malah ke selatan. Kabarnya, gunung Merapi meletus.

Kasusnya mirip seperti di daerah Minggir, hampir perbatasan dengan Kulonprogo. Ibu saya sedang berada di sana--rumah simbah--ketika gempa berlangsung. Kata ibu, di sana juga muncul isu tsunami hingga warga berbondong-bondong pergi ke utara.

Melihat jalan lingkar utara yang begitu kacau, kami di kampung juga ikut panik. Beberapa orang mempertimbangkan untuk ikut lari. Namun bapak saya berhasil meredamkan suasana. Pokoknya jangan percaya dahulu dengan berita seperti itu. Alhasil warga kampung tetap bertahan. Beberapa hari setelah itu, tersiar berita, begitu banyak rumah dijarah saat isu tsunami tersiar di Bantul.

![Lokasi episentrum gempa Yogyakarta](/blog/2019/05/images/episentrum-gempa-jogja-rmdzn.gif) Episentrum gempa Yogyakarta oleh USAID

Pagi hari itu, aku masih berminat masuk sekolah meskipun agak ragu. Keluarga meyakinkanku bahwa sekolah pasti diliburkan. Ternyata benar. Seorang kawan bercerita, saat gempa, ia sudah berada di sekolah. Ia menjadi saksi bagaimana kerusakan sekolah terjadi saat gempa mengguncang di depan mata. Hari itu memang tak ada kegiatan belajar mengajar.

## Tidur di luar rumah

Kami tidur di gardu ronda dengan tikar dipasang miring sebagai dinding untuk menahan hawa dingin. Beberapa warga lain ada yang tidur di teras rumah dan mobil pribadi. Rasanya, trauma untuk masuk rumah itu nyata.

Ketika aku pertama kali masuk ruang keluarga pasca gempa, kepala terasa pusing. Tempat seperti bergerak. Badan ikut limbung. Benar-benar perasaan yang sulit dikendalikan saat itu.

Bapak mengeluarkan tv jadul ke teras rumah. Kami dan beberapa tetangga menyimak berita seputar gempa 27 Mei lewat banyak saluran. Ya, gempa Yogyakarta itu ternyata menjadi berita nasional dan hampir disiarkan di semua kanal. Yang tak terlupakan adalah lagu _Berita Kepada Kawan_ oleh Ebiet G. Ade yang selalu disiarkan setiap kali pembahasan seputar korban gempa. Bagiku pribadi, lagu itu dapan memantik ingatan dan emosi seputar gempa Yogyakarta hingga saat ini.

## Ujian di tenda sekolah

Berminggu-minggu murid belajar di luar kelas. Sekolah mendirikan tenda berwarna biru di lapangan voli. Kalau siang, hawanya panas luar biasa. Kami sempat menikmati kegiatan belajar mengajar dan ujian di tenda. Omong-omong saya benar-benar lupa separah apa kerusakan yang dialami oleh gedung sekolah.

Gempa Yogyakarta sudah berlalu 13 tahun lalu. Yang pasti, ia mengingatkan kita bahwa manusia adalah makhluk penuh emosi. Penuh lelah. Penuh prasangka dan lemah.

\[Foto utama hanya ilustrasi oleh Angelo Giordano di Pixabay\]
