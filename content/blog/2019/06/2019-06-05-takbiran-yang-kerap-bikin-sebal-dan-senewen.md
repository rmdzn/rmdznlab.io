---
title: "Takbiran yang Kerap Bikin Sebal dan Senewen"
date: "2019-06-05"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "takbiran-rmdzn.jpg"
aliases:
  - /post/2019-06-05-takbiran-yang-kerap-bikin-sebal-dan-senewen/
---

![takbiran](/blog/2019/06/images/takbiran-rmdzn.jpg)

_Takbiran seringkali tak begitu menyenangkan~_

Keramaian kerap membuat saya senewen. "Keramaian" secara harfiah, yang timbul karena banyaknya orang dan suara bising. Seperti hari Selasa malam, takbiran yang mengakhiri bulan Ramadan sekaligus menyambut Idulfitri 1440H.

Entah mengapa, sejak dahulu budaya takbiran tak selalu cocok dengan diri saya. Takbiran yang isinya _bleyer-bleyer_ dan klakson motor serta sulutan mercon atau kembang api. Saya lebih menyukai takbiran klasik, jalan kaki sambil bawa obor atau lampion, atau mungkin tambah pukulan genderang ala drumben.

Begini, bukan apa-apa. Selain sisi subyektif bahwa tak suka keramaian, ada banyak hal yang boleh dikata obyektif, tak masuk akal setiap kali takbiran konvoi (istilah ini saya pakai untuk menyebut takbiran dengan konvoi kendaraan--mobil atau motor) diadakan.

Alasan **pertama**. Keamanan yang tidak aman. Dari sekian seksi panitia, seksi keamanan adalah seksi terpenting pada takbiran konvoi. Mereka bertugas mengamankan jalan dari potensi gangguan yang akan dialami oleh takbiran konvoi. Biasanya, tugas mereka menonjol pada saat berada di persimpangan jalan. Pertigaan atau perempatan.

Ironinya, mereka tak dilengkapi dengan alat keamanan berkendara motor. Maksud saya helm. Klasik. Mayoritas pengendara motor pada setiap konvoi takbiran, termasuk para seksi keamanan, tak memakai helm. Di samping hanya melindungi kepala dengan rambut, mereka kadang menggunakan peci dan kafiyeh (laki-laki) serta kerudung (perempuan) saja.

Alasan **kedua**. Bikin jalan macet. _Yes_, takbiran klasik dengan jalan kaki juga terkadang bikin jalan macet. Namun paling tidak, takbiran klasik hanya melalui jalanan kampung, kalaupun melalui jalan besar pasti tak jauh-jauh amat (_capai, Bos!_). Beda dengan takbiran konvoi, ia pasti melalui jalan besar dan itu pasti cukup jauh. Saya mencoba menempatkan diri sebagai warga biasa yang mungkin terganggu oleh konvoi apapun bentuknya.

Alasan **ketiga**. Bisingnya minta ampun. Berbeda dari ketukan genderang atau drumben, bisingnya takbiran konvoi justru dipenuhi suara kendaraan itu sendiri ditambah suara klakson. Penginnya _sih_, mungkin mereka ingin meramaikan suasana, seperti saat kampanye partai, tapi itu norak sekali, _Bung_!

Kemarin malam, saat takbiran konvoi berlangsung, pengendara motor dari kampung sebelah memainkan klakson motornya secara bersahut-sahutan. Tak berapa lama, kami berpapasan dengan takbiran konvoi kampung lain. Rombongan depan menuntun motor-motornya kemudian memainkan gas seolah-olah memamerkan kekompakan mereka di depan rombongan kami. Batin saya langsung berteriak, _NDESO!_.

_Mas, naik motor, santai, tenang, tanpa memainkan klakson dan gas motor apakah hal terkutuk?_

Alasan **keempat**. Petasan dan kembang api di mana-mana. Ini mungkin bukan alasan khusus pada takbiran konvoi, lebih pada Idulfitri pada umumnya. Kecuali kembang api dengan tangkai seperti permen dan akan memancar seperti kembang saat disulut, petasan dan kembang api hanya menghasilkan polusi suara dan lingkungan.

Bising, iya. Mengotori lingkungan, iya. Tradisi di kampung sebelah adalah menyulut petasan atau kembang api setiap perayaan Idulfitri dan tahun baru. Bahkan ketika ada rumah sakit yang berdiri, warga kampung tetap melestarikan tradisi yang dimaksud. Saya tak pernah setuju dengan keberadaan petasan dan kembang api, dahulu dan sekarang.

Saya bahkan pernah bertanya kepada kenalan yang bekerja di rumah sakit soal pendapatnya tentang petasan dan kembang api di lingkungan tempat kerjanya. _Tidak setuju_, katanya. Ia merasa kasihan kepada pasien-pasien yang tak jarang disuguhi suara ledakan padahal satu-satunya yang mereka butuhkan adalah ketenangan.

Selain tentang polusi suara, siapa penyulut petasan yang mau membersihkan tanah dari serakan kertas hasil pecahannya? Tidak ada. Pada bulan Ramadan dan takbiran, sudut-sudut jalan dipenuhi dengan ceceran kertas petasan. Sampah ini hanya akan terbuang oleh hujan atau angin.

_Huh_, tapi mau bagaimana lagi? Itu sudah menjadi tradisi. Bagi saya, tradisi takbiran di sini hanya menjadi ajang anggota _sambat-sambat club_ mencurahkan hatinya dalam diam. _Eh_ ... tidak tahu, _ding_, apakah hanya saya yang mempunyai keluhan di atas atau ada juga orang lain merasakan hal serupa.

Sebelum pergi. Tidak lupa saya ucapkan _taqabalallahu minna waminkum_. Selamat lebaran. Selamat merayakan Idulfitri 1440. Mohon maaf lahir dan batin.
