---
title: "Yang Dirindu dari Lebaran Adalah Jalanannya!"
date: "2019-06-11"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "img20190606100849.jpg"
aliases:
  - /post/2019-06-11-yang-dirindu-dari-lebaran-adalah-jalanannya/
---

_Ada kenikmatan duniawi pada setiap centi jalan mulus maupun nggronjal-nggronjal di hari Lebaran._

Lebaran memang penuh kerinduan. Siapa yang tidak setuju? Pasti yang bilang _tidak_, bercanda _doang_. Bagi perantau, Lebaran menjadi ajang untuk berkumpul bersama keluarga setelah berminggu-minggu atau bahkan setahun tidak pulang. Bagi nonperantau, Lebaran menjadi hari libur untuk berkeliling ke rumah saudara maupun tetangga berdalih silaturahmi demi beragam cemilan dan makanan gratis.

_Hayo, ngakuu ... enggak ngaku, enggak papa deh, saya enggak maksa. Kalaupun beneran enggak, ya maap :((_

Kalau ditanya, saya akan menjawab seperti kalimat pertama tulisan ini, _kok_. Menikmati jalan kampung bersama rombongan untuk menuju ke satu rumah dan rumah yang lain--termasuk suasana hangat pagi setelah salat Ied dan panasnya siang saat hampir pulang. Guyon-guyon yang dilempar dalam rombongan juga khas. Kisah THR yang tak lagi mereka terima seperti saat mereka masih kecil, salah satunya.

Soal cemilan, ada salah satu rumah yang setiap Lebaran pasti menyajikan tahu bakso dan sate kolang-kaling dingin. Ditambah suasana khas ala Lebaran, cemilan tersebut sungguh nikmat tiada tara.

Oiya, kalau boleh cerita, rombongan yang saya maksud adalah rombongan pemuda (Karang Taruna), bukan keluarga alias rombongan bapak ibu yang sambil sesekali menggendong anak dan menjadikannya alasan untuk pulang duluan karena rewel.

_Eit_, meskipun saya bilang silaturahmi ke rumah tetangga dan menikmati cemilannya adalah hal yang dirindukan dari Lebaran, ada suasana lain yang pantas banget menjadi objek rindu seperti kamu.

Ada obsesi keterlaluan setiap Lebaran hadir. Obsesi terhadap jalanan sepi dan ruko-ruko yang tertutup pintu _rolling_. Tak jarang, di jalanan yang sepi itu, orang-orang terlihat berlalu-lalang menggunakan motor. Rapi, klimis, berpakaian batik, tanpa memakai helm.

Karena sudah bertahun-tahun tidak mudik, termasuk Lebaran tahun 1440H/2019 ini, pada hari kedua Lebaran (H+1), saya sempatkan diri untuk bersepeda sejak pagi demi memuaskan hasrat akan obsesi tadi. Saya berangkat antara pukul 08.00 dan 09.00, waktu yang saya rasa tepat untuk berkeliling membuktikan bahwa jam kerja juga bisa sesepi hatimu.

Setelah manset, masker _buff_, dan topi terpasang sesuai lokasi, begitu juga tas tergantung di bahu, saya langsung meluncur ke jalanan. Potret yang pertama saya ambil adalah jalan di sekitar SMPN 3 Depok. Pada hari biasa, sebetulnya lokasi ini tidak terlalu ramai, tetapi seliweran anak sekolah dan mahasiswa INSTIPER yang digantikan oleh bapak/ibu bersama anak-anaknya berpakaian rapi menjadi hiburan tersendiri.

![Berhenti di sekitar SMPN 3 Depok](/blog/2019/06/images/img20190606091526.jpg) Di sekitar SMPN 3 Depok. Kampus INSTIPER

Saya kembali memacu sepeda. Pergi ke arah barat melewati Grha INSTIPER. Di perempatan Jl. Tasura (Paingan), saya belok ke kanan. Pada hari biasa. Jalur ini relatif ramai, maklum, di sini warga dapat menemukan layanan fotokopi yang buka hingga malam dan buka pada tanggal merah, penjual gorengan, minimarket, warung kelontong, penjual pulsa, penjual air minum isi ulang, warung makan, istana kepresidenan, gedung MPR/DPR, Monumen Nasional ... _eh ... maaf_, tiga yang terakhir hanya bualan.

Karena suasana yang syahdu, saya putuskan mandek di sini dan menjadikannya sebagai tempat pemotretan kedua.

![Berhenti di Paingan](/blog/2019/06/images/img20190606092317.jpg) Di Paingan Raya

Suasana sepi minta ampun. Paling hanya satu dua kali mobil lewat disusul dua motor yang pengendaranya berpakaian rapi ala _njagong manten_. Semua lokasi bisnis tertutup pintu _rolling_.

Bergerak sedikit ke utara, saya berhenti tepat di pertigaan yang kerap dilewati oleh mahasiswa Universitas Sanata Dharma. Dapat diduga, lokasi ini juga sepi, hanya ada satu penjual pulsa yang melayani pembeli. Pada salah satu sudut terdapat mobil yang saya duga adalah tamu yang sedang bersilaturahmi.

![Berhenti di pertigaan Paingan](/blog/2019/06/images/img20190606092536.jpg) Di pertigaan Jl. Tasura (Paingan)

Pada perjalanan berikutnya, di rentetan jalan yang sama, tidak ada sesuatu yang spesial bahkan saat di depan kampus Sanata Dharma. Karena saya tak begitu _ngeh_ dengan keramaian jalan ini, saya memutuskan untuk tidak mengabadikannya dalam kamera hape.

Berbeda ketika melewati pertigaan Stadion Maguwoharjo. Persimpangan yang cenderung ramai setiap harinya ini relatif sepi. Beberapa penjual di kanan jalan tetap membuka jajanannya, cocok sebagai tempat pemberhentian sementara para pelancong untuk mengisi perut. Tak ada pertandingan sepak bola, tak ada konser, tak ada latihan motor di halaman stadion, tak ada pasar. Sunyi.

![Berhenti di pertigaan Stadion Maguwoharjo](/blog/2019/06/images/img20190606093243.jpg) Di pertigaan Stadion Maguwoharjo

_Gowes_ berlanjut melewati Jogja Bay lalu belok ke kiri menuju Jl. Candi Gebang, _bablas_. Pentokan perempatan pada Jl. Nusa Indah ini sangat epik. Dari yang biasanya ramai luar biasa, sering macet, sekarang _krik-krik_. Hanya satu dua motor yang lewat, yang satu hanya pakai kaos dan celana pendek ala _akamsi_ (anak kampung sini), satunya lagi pengendara berdandan klimis dengan ibu berkerudung kembang membonceng di belakang. Ooh ... Lebaran.

![Berhenti di pertigaan antara jalan Candi Gebang dan jalan Nusa Indah](/blog/2019/06/images/img20190606094007.jpg) Simpang Jl. Candi Gebang-Jl. Nusa Indah

Saya kemudian belok ke kiri (selatan). Di sepanjang Jl. Nusa Indah ini lumayan sepi, tapi entah mengapa saya tak tertarik untuk mengabadikan jalan sebelum kali/jembatan. Toko-toko di kanan kiri jalan tutup, kecuali Alfamart. Percayalah, di sepanjang jalan saya hanya menemui para pelaku silaturahmi berwira-wiri.

Saya hanya mengambil foto setelah melewati jembatan.

![Berhenti di jalan Nusa Indah, selatan jembatan](/blog/2019/06/images/img20190606094754.jpg) Di Jl. Nusa Indah, tak jauh dari jembatan

Tidak lupa, masih di jalan yang sama, di pertigaan jalan yang setiap harinya selalu ramai karena dipenuhi oleh mahasiswa Kampus Ungu, saya potret _dong_. Pertigaan ini benar-benar sunyi. _Suer_! Warung Gado-gado, penatu, bengkel motor, bahkan toko celana dalam ikut tutup.

Saya mendapati pengendara berbaju koko, berpeci, ibu-ibu berkerudung, anak muda necis muncul dari arah belakang saya dan belokan di kanan jalan. Ingin ucapkan salam kepada mereka, tetapi saya takut mereka kaget, tiba-tiba _nggobek_ dan kehilangan kontrol motor, malah repot. Akhirnya saya urungkan, kata seseorang yang pernah jatuh di dua titik jalan ini.

![Berhenti di utara pertigaan jalan Nusa Indah](/blog/2019/06/images/img20190606095135.jpg) Masih di Jl. Nusa Indah. Pertigaan yang kalau tak Lebaran ramai ada di ujung, di bawah pohon kanan

Di pertigaan yang selalu ramai itu, saya belok ke timur atau ke kiri. Ruas jalan yang kerap menyulitkan dua mobil saling berpapasan tetapi sering padat itu sedang longgar. Kalau bisa bicara, jalanan itu pasti bersyukur dapat bernapas lebih lega meskipun hanya beberapa hari dalam setahun.

Sangat dimaklumi mengapa jalan ini ramai, karena ia merupakan penyambung dua kampus berbeda; UII dan Amikom Yogyakarta. Begitu hidupnya bisnis di sini, "kematiannya" yang sementara menjadikan jalan begitu syahdu. Tidak percaya? Cobalah tengok jalan ini kembali saat Lebaran Idulfitri tahun depan. Haha!

![Berhenti di jalan antara kampus UII dan Amikom Yogyakarta](/blog/2019/06/images/img20190606095320.jpg) Bukan iklan. Ini di jalan antara UII dan Amikom.

Sepinya jalan membuat saya dapat memacu sepeda dengan santai dan mulus. Kalau saat ramai, pesepeda seperti saya harus mengalah dari mobil atau motor dengan cara _mlipir_ ke kiri--tempat alas _cor-coran_ tak rapi alias _nggronjal_. Saat sepi, aspal yang mulus tentu terlihat _semlehoy_, pesepeda dapat _menggowes_ agak ke tengah jalan dan menyanyikan lagu Queen, _Bicycle Race_, sambil goyang.

Perempatan selanjutnya (perempatan Mancasan) terasa menenangkan hati. Keruwetan yang timbul gara-gara mobil dan motor saling berebut dulu-duluan dari empat penjuru setiap harinya membuat persimpangan itu menjadi primadona saat sepi. Kayaknya, saya bisa turun dan _gelesotan_ sebentar di tengah jalan. Sesuatu yang tak mungkin dapat dilakukan di luar hari Lebaran.

![Berhenti di pertigaan Mancasan](/blog/2019/06/images/img20190606095818.jpg) Perempatan ruwet yang kini lega

Sekitar depan UII juga begitu, terlihat lengang walaupun di selatannya perempatan UPN yang sangat ramai.

![Berhenti di jalan depan kampus UII](/blog/2019/06/images/img20190606095650.jpg) Jalan di depan UII

Melajulah saya ke depan Hartono Mall. Titik yang sering saya hindari saat tidak bersepeda itu sepi. Selama beberapa menit di sana tak terlihat motor dan mobil berbelok ke dalam mal. Ruas jalan yang selalu padat di 100m depan saya begitu nyaman dipandang dan dilewati.

Mungkin ... mungkin lo yaa, Hartono Mall saat itu mirip seperti lapangan bola. Terasa lapang. Tapi jangan sekali-kali menendang bola di sana ya, takut kena tiang atau kena _embak-embak_ berkaca mata yang mungkin juga sedang jalan-jalan sendirian. Ini bukan FTV soalnya.

![Berhenti di depan Hartono Mall yang lengang](/blog/2019/06/images/img20190606100849.jpg) Depan Hartono Mall

Di sepanjang jalan Ring Road juga relatif sepi, khususnya dari arah timur ke barat. Kalau kamu punya nazar untuk lompat-lompat di tengah Ring Road, Lebaran adalah kesempatan. Kalau nazarmu belum kesampaian, coba saja tahun depan. Kalau perlu, sambil teriak-teriak dan bergerak seperti menembak dengan alunan _Let's kill this love ... tetetetet ... ayayayaya!_.

Tak perlu banyak potret di sini. Saya hanya menjepret Ring Road di depan Asrama Haji dan jalan layang Jombor. Tidak begitu asyik.

![Berhenti di depan Asrama Haji di RingRoad Utara](/blog/2019/06/images/img20190606102544.jpg) Depan Asrama Haji

![Berhenti di timur jalan layang Jombor](/blog/2019/06/images/img20190606103017.jpg) Di jalan layang Jombor

Selanjutnya saya tak mendokumentasikan apa-apa sepanjang jalan Ring Road Barat hingga jl. Wates. Saya langsung belok ke kiri (jl. Wates) menuju Kota Yogyakarta. Di Kota, libur Lebaran seperti tidak ada perubahan. Tetap ramai. Apalagi di Titik Nol, sama saja.

Potret terakhir yang saya ambil adalah jl. Babarsari. Bisa dibayangkan kota Las Vegas ala Sleman ini setiap harinya. Ramai laksana TOSERBA. Beda saat Lebaran. Hari libur benar-benar mempengaruhi ruas jalan yang tak kenal lelah memamerkan hingar bingarnya ini.

Saya sebenarnya ingin mengambil foto tepat di depan Sahid J-Walk dan Universitas Atma Jaya, karena panas (saat itu pukul 12.00 lebih) saya memilih titik di depan Badan Tenaga Nuklir Nasional (BATAN) yang relatif rindang.

![Berhenti di depan BATAN, Babarsari](/blog/2019/06/images/img20190606124554.jpg) Di jl. Babarsarai, depan BATAN

Sekalian pamer. Di perjalanan panjang ini, saya mampir sebentar ke markas Avengers. Bertemu dengan Thor, Hulk, Wanda alias Scarlet Witch, dll dalam wujud manusiawinya; menikmati nastar dan opor, menghidangkan sirup Marjan, berkelakar soal THR yang sudah habis karena belanja koko dan gamis, dsb.

![Berhenti di depan markas Avengers](/blog/2019/06/images/img20190606110659.jpg) Avengers assemble!

Maaf. Saya ingin bercerita lebih jauh, tetapi mereka menolaknya. Saya pun dilarang untuk menyampaikan lokasi markas yang kini ruang tamu dan kamar mandinya sedang direnovasi besar-besaran.

_Aahh ..._
