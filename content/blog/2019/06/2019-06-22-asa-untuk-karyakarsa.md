---
title: "Asa untuk KaryaKarsa"
date: "2019-06-22"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "karyakarsa-site-rmdzn.png"
aliases:
  - /post/2019-06-22-asa-untuk-karyakarsa/
---

_KaryaKarsa? Itu lo, yang sedang booming ..._

Saya sempat iri dengan orang luar negeri--maksudnya orang Amerika dan Eropa--yang kelihatannya sebegitu gampangnya cari duit. Platform-platform digital muncul, memudahkan orang-orang di sana untuk menumpuk duit dengan "hanya" mengerjakan pekerjaan yang mereka sukai.

Salah satu platform yang saya maksud adalah Patreon. Di Patreon, para kreator dipersilakan untuk membuat profil untuk memamerkan karya-karyanya. Para penggemar kreator dapat mendukung sang kreator secara langsung dengan cara menjadi _patron_. Membayar sedikitnya 1 USD/bulan (tergantung dengan _tier_ yang disediakan kreator). Sebagai timbal balik, biasanya sang kreator menyediakan karya-karya eksklusif yang hanya dapat dinikmati oleh para patron. Solusi menang-menang!

Namun, karena Patreon didirikan dan bermarkas di Amerika Serikat, secara otomatis kerja Patreon mengikuti sistem yang ada di sana. Mata uang pakai dollar Amerika. Pembayaran memakai kartu kredit atau PayPal. Penetrasi kartu kredit yang kecil di Indonesia membuat tak semua orang dapat menjangkau layanan Patreon.

Sepintas, saya sempat berpikir. Apakah bakal ada layanan serupa Patreon di Indonesia? Lumayankan, kita bisa mendapatkan rupiah dengan "hanya" memamerkan karya dan sekaligus dapat mengirim rupiah yang sama kepada orang lain.

_Eng ing eng_ ... berbulan-bulan setelah pikiran itu terlintas, seorang konsultan sekaligus penulis yang saya ikuti di Twitter menyebut suatu nama yang langsung menarik perhatian--KaryaKarsa.

> Nah…..kalau di US ada Patreon yang diprakarsai oleh @jackconte di Indonesia ada KaryaKarsa yang diprakarsai oleh @barijoe dan @rajasa ya? Coba deh cek2 linimasa mereka atau colek2 lah (mereka seneng dicolek kok) untuk tau lebih jauh….:-D
> 
> [~@bangwinissimo](https://twitter.com/bangwinissimo/status/1137920948789628929)

Ada kata kunci yang menarik di situ. Patreon, Indonesia, @rajasa, dan KaryaKarsa itu sendiri. Omong-omong, [@rajasa](https://twitter.com/rajasa) adalah nama pendiri Tees Indonesia, Aria Rajasa Masna. Setelah mengulik sebentar dari [lanjutan twit Bangwin](https://twitter.com/bangwinissimo/status/1137923379900559360), ternyata KaryaKarsa adalah usaha rintisan yang bekerja mirip seperti Patreon. "_Berikan tip kepada kreator favorit \[kamu\] setiap dia \[kreator\] mengunggah karya. Bisa juga langganan bulanan untuk konten eksklusif_," kata @rajasa.

Agar lebih jelas, saya langsung mengulik informasi lebih dalam ke akun Twitter Ario Tamat alias [@barijoe](https://twitter.com/barijoe), sang pendiri KaryaKarsa, dan ke situsweb resminya.

![Halaman situsweb KaryaKarsa.com](/blog/2019/06/images/karyakarsa-site-rmdzn.png) Laman KaryaKarsa.com

[KaryaKarsa](https://karyakarsa.com) adalah Patreon ala Indonesia yang tecermin dari slogannya: Platform Apresiasi Kreator. Situsweb ini belum berdiri sepenuhnya. Ia masih mengumpulkan dukungan dalam bentuk donasi. Keren _sih_ ini. Proses pengumpulan donasi seperti menggambarkan sekilas sistem yang akan dibangun mereka nanti.

Hanya berselang beberapa hari setelah melakukan riset kecil-kecilan terhadap KaryaKarsa, saya akhirnya memutuskan untuk langsung mendukung situsweb ini melalui donasi.

![bukti donasi](/blog/2019/06/images/bukti-donasi-karyakarsa-rmdzn.png) Bukti pembayaran dari KaryaKarsa

# Mengapa mendukung KaryaKarsa?

Memikirkan KaryaKarsa membuat saya teringat dengan proyek Wujudkan.com. Proyek tersebut berdiri [pada tahun 2012](https://edukasi.kompas.com/read/2012/03/29/19462079/wujudkan.com.ayo.bantu.industri.kreatif.indonesia). Perbedaannya, alih-alih untuk mendanai kreator individu, Wujudkan.com digunakan untuk mendanai proyek, mirip seperti Indiegogo dan Kickstarter. Sayangnya, mereka hanya bertahan kurang lebih selama lima tahun. Pada tahun 2017 [mereka resmi berhenti beroperasi](https://id.techinasia.com/situs-crowdfunding-wujudkan-tutup-layanan-pada-31-maret-2017).

Pada sisi teknis, ada hal yang menarik untuk diperhatikan. Untuk berdonasi ke sebuah proyek, Wujudkan.com memanfaatkan kartu XL (potongan pulsa?), ATM (kartu debit), dan kartu kredit. Ketiganya tak begitu praktis, yang terakhir justru sangat merepotkan. Sedangkan KaryaKarsa menggunakan GoPay--yang saat ini sudah dipraktikkan untuk mengumpulkan dukungan proyek mereka--yang lebih praktis; tak perlu memakai kartu seluler tertentu, tak perlu membuat kartu debit maupun kartu kredit, mungkin hanya perlu direpotkan dengan memasang aplikasi ojol berukuran beberapa puluh megabita. Sekarang, mereka sedang dapat momennya. Momen _booming_\-nya uang digital.

Salah satu pendiri Wujudkan.com, Mandy, pernah [berkomentar](https://id.techinasia.com/pelajaran-dari-tutupnya-situs-crowdfunding-wujudkan) soal salah satu "alasan" mengapa proyeknya berhenti. Konsep urun dana (_crowdfunding_) untuk industri kreatif belum bisa diterima secara luas di tanah air. Masyarakat Indonesia telah sangat mengerti tentang konsep penggalangan dana di bidang sosial, namun tidak di bidang lain. Sistem urunan untuk menjenguk orang sakit adalah contoh konvensionalnya sedangkan KitaBisa.com yang begitu sukses dapat menjadi contoh modernnya.

KaryaKarsa diharapkan mampu mengedukasi masyarakat tentang urun dana/donasi di ranah industri kreatif sembari menghapus stigma yang kadang ada benarnya bahwa masyarakat Indonesia bermental gratisan.

Mendukung KaryaKarsa bukanlah sebuah investasi untuk mendapatkan keuntungan dalam jangka pendek. Ada asa pada setiap dukungan untuk memberikan ruang karya kepada kreator sekaligus memberikan ketenangan finansial kepada mereka. Secara lebih luas, KaryaKarsa dapat menjadi alat untuk memperkenalkan manusia-manusia pencipta karya dari pelosok kampung hingga perkotaan dan menyambungkannya dengan penikmat karya sejatinya.

Kalau kamu mau dukung KaryaKarsa, langsung saja pergi ke situsweb [KaryaKarsa.com](https://karyakarsa.com). Minimal seribu, maksimal sebanyak-banyaknya.

KaryaKarsa sekaligus menjadi ruang saya untuk menampilkan karya dan mendapatkan dukungan. Kamu dapat mendukung saya lewat KaryaKarsa dengan mengunjungi halaman [karyakarsa.com/rmdzn](https://karyakarsa.com/rmdzn/), ada cerpen ekslusif saya di sana! Terima kasih!
