---
title: "Menggugat Tutur Duka"
date: "2019-06-29"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "mourn-moritz-schumacher-mytkwuqncj0-unsplash-resized-rmdzn.jpg"
aliases:
  - /post/2019-06-29-mengugat-tutur-duka/
---

![mourn](/blog/2019/06/images/mourn-moritz-schumacher-mytkwuqncj0-unsplash-resized-rmdzn.jpg)

_Peduli, atau pedulii~_

Setiap ada kenalan, teman misalnya, yang sedang sakit, minimal orang yang mengetahui akan mengucapkan harapan lekas sembuh melalui jalur pribadi (japri). Itu juga terjadi ketika ada berita lelayu, orang-orang "membombardir" ponsel keluarga yang ditinggalkan dengan ucapan belasungkawa. Kalau perlu mereka langsung menyambangi orang/keluarga yang sedang kesusahan.

Ada rasa autentik pada setiap simpati yang dihaturkan. Karena memang butuh modal untuk melakukannya. Ketika ingin mengucap duka cita, perlu menyisihkan beberapa rupiah pulsa. Ketika ingin langsung menyambangi ke rumah, perlu melangkah yang kadang bikin ogah akibat hawa panas dan hawa dingin menyapa.

Melompat belasan tahun lalu. Seorang teman SD sakit, tak masuk sekolah. Tidak perlu saling berkabar lewat ponsel (_wong_ ketemu di kelas), mereka langsung berembuk, membesuk teman setelah pulang sekolah. Ada yang mengeluh? Pasti ada. Malas menjenguk. Namun karena rasa tidak enak jika tidak ikut menjenguk, akhirnya mereka ikut, hitung-hitung nanti sekalian mampir main ke tempat teman.

Setelah makin dewasa dan ruang antar teman makin menjauh. Satunya di Yogya, satunya di Jakarta, sedangkan yang lain menyebar sampai Bandung hingga Lampung. Jika bukan karena teman serekat lem super, kehilangan memicu rasa hampa yang sangat. Efeknya satu yang sakit atau kehilangan, yang lain benar-benar sama merasakan. Simpati disampaikan dengan lugas tak basa-basi.

Di sudut lain. Ketika sang perekat hanya berupa sebutir nasi, ucapan simpati seperti tidak berarti. Ucapan "saya peduli" hanya selayak penggugur kewajiban.

Seorang anggota grup obrolan yang berisi orang yang pernah dekat--lama tak bersua--pernah kehilangan. Teman-temannya yang berjumlah lima sampai sepuluh orang cuma bilang, "turut berduka cita ya". Templat. Tidak ada ucapan tambahan. Setelah ucapan tersampaikan, hati terasa lega.

Lalu apa?

Ya, udah.

Haturan hanya bukti keeksistensian, bahwa saya ada, kami ada. Apalagi apa yang diketik kini bermodalkan kurang dari sepuluh persen biaya yang dikeluarkan zaman kebesaran Nokia. Keadaan lanjutan setelah pengucapan bukan lagi sebuah urusan.

Saya rindu. Zaman dahulu yang sedang lugu-lugu itu. Namun apa daya. Gesitnya perubahan dunia membuat kita berbeda.

Hak saya untuk menggugat. Bukan hak saya untuk mengerdilkan ucapan. Semua punya waktunya. Semua punya masanya. Tapi, tak berarti semua punya rasa yang sama.

\[Foto oleh [Moritz Schumacher](https://unsplash.com/@locustxswarm?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) di [Unsplash\]](https://unsplash.com/search/photos/mourn?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
