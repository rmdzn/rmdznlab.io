---
title: "Selamat Tinggal Counter-Strike Indonesia, Semoga Gak Bikin Kangen"
date: "2019-07-04"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "ak47-rmdzn.png"
aliases:
  - /post/2019-07-04-selamat-tinggal-counter-strike-indonesia-semoga-gak-bikin-kangen/
---

![ak47](/blog/2019/07/images/ak47-rmdzn.png)

_Waktunya perpisahaan._

Duduk di kursi, di depan monitor tabung alias CRT yang diletakkan di atas meja berlapis kaca. Saya berselancar ria. Entah dirasuki siapa, saya iseng mengetikkan kata kunci semacam "bermain counter strike online indonesia". Itu adalah usaha lanjutan setelah saya sempat bermain dengan _Counter-Strike: Condition Zero_ secara daring (_online_).

Sudah sejak sekitar tahun 2003-2004 saya menyukai game tembak-tembakan. Kala itu _Counter-Strike_ sedang _booming-booming_\-nya dimainkan anak muda. Bahkan kakak saya pernah bilang, "sekarang zamannya CS (_Counter-Strike_) bukan lagi PS (PlayStation)." Meski setelah besar saya akui informasi itu cacat, PS masih berjaya dan masih pantas disebut pemegang pangsa pasar game konsol yang besar.

Saya pernah kecanduan game _First-person Shooter_ (FPS) tersebut, sampai tiga tahun lah, seusia SD akhir hingga pertengahan SMP. Candu yang tidak memabukkan. Main di komputer rumah yang seringnya hanya dipakai untuk mendengarkan musik lewat _Winamp_. Pada tahun 2011 keinginan untuk bermain CS kumat lagi dan meletakkan saya pada posisi seperti paragraf pertama esai ini. Beruntunglah saya, tepat saat sedang berselancar di mesin pencari, saya menemukan laman Megaxus yang memamerkan kehadiran _Counter-Strike Online_ (CSO) di Indonesia.

Megaxus Infotech adalah penerbit (_publisher_) game daring di Indonesia. Ia memiliki hak untuk menyediakan game beserta layanan-layanan di dalamnya kepada kita yang suka bermain-main hati. CSO yang dibikin oleh Valve Software dan Nexon adalah salah satu game yang mereka pegang, selain _AyoDance_ dan _Closers Online_.

Menemukan CSO merupakan kenikmatan duniawi, mengingat saat itu saya merasakan repotnya bermain daring lewat CS dari _1.6_ dan _Condition Zero_. Repot soal cara _setting-seting_\-nya dan menemukan server yang tepat. Benar saja, setelah diunduh dan diinstal, selain mudahnya memulai permainan daring dan memilih server, CSO memberikan gameplay yang berbeda dari biasanya. Ada _Gun Death Match_ dan _Team Death Match_. Yang satu, antar karakter saling tembak sampai mati, satunya lagi antar tim (_terrorist_ dan _counter-terrorist_) saling tembak sampai mati pula. Tak hanya itu, di sana juga ada konsep klan. Beberapa anggota dapat bergabung dengan sebuah klan dan bertarung dengan klan lain (_clan wars_).

Hari itu sangat spesial. Saya ternyata termasuk pengguna CSO Indonesia generasi pertama yang mendaftarkan diri ketika sesi _Open Beta_. Setelah bermain beberapa bulan dan CSO Indonesia dirilis secara resmi, karakter saya langsung mendapatkan gelar "Veteran". Gelar yang bikin sombong. Saya merasa, saya memang layak menyandang gelar tersebut mengingat saya datang dari _CS: Condition Zero_ meski cara mainnya tidak begitu jago. Ketika main pertama kali, kawan satu tim dan tim musuh banyak yang juga menyandang gelar sama. Artinya? Mereka mendaftarkan diri saat _Open Beta_ juga. Tua-tua~

Saking seringnya bermain game daring ternyata tak baik bagi mental, sering bikin uring-uringan. Game daring tak bisa di-_pause_, sungguh bencana bagi saya yang bermain sembari menjaga warung di rumah. Saya memutuskan pensiun. Kakak saya yang juga ikut membuat akun di Megaxus untuk bermain CSO saya persilakan untuk menggunakan akun saya--tentu untuk menikmati beragam bonusan senjata yang sayang jika dilewatkan.

Walaupun lama tak bermain CSO, pengumuman dari Megaxus yang [disampaikan](http://cso.megaxus.com/news/penutupan-layanan-counter-strike-online) Rabu, 3 Juli, bagai samberan petir. Bikin kaget. CSO akan dihentikan. Bulan Juli 2019, segala transaksi dari Mi-Cash (mata uang game yang disediakan Megaxus) ke CSO tidak lagi dapat dipakai. Mulai 1 Agustus 2019, Megaxus akan secara resmi menutup server game. Sebagai kompensasi, para pemain CSO akan memperoleh bonus untuk game _AyoDance_ dan _Fatal Raid_ dengan syarat dan ketentuan berlaku.

Tidak tahu pasti alasan penghentian CSO Indonesia. Mungkin saja ia sudah kalah populer dibanding pemain-pemain baru, seperti Apex, Free Fire, dan PUBG, sehingga tak lagi menghasilkan keuntungan. Anak kecil yang kerap berlalu-lalang di depan rumah sangat familiar dengan bagaimana game _Free Fire_ dan _PUBG_ bekerja. Bahkan suatu saat, ketika saya sedang membaca artikel lewat ponsel, saya dipanggil oleh salah satu dari mereka, "_Mas, baru main Free Fire, Mas?_". Di lain waktu, kepada salah seorang anak kecil saya tanyakan tentang game _Counter-Strike_. "_Tahu_," jawabnya, tapi tak pernah memainkan. Sekadar tahu, tapi tidak diperhatikan. Seperti dia yang tak pernah memperhatikanmu.

Berakhirnya CSO bukanlah kiamat. Penggemar CS mungkin bisa geser ke _Counter-Strike: Global Offensive_ (CS:GO) yang belum lama ini digratiskan oleh pihak Valve. Toh bagi penikmat senjata berwarna-warni nan alay bisa juga mengunduh mod-mod senjata yang tersebar di luar sana. Kalau ingin lebih jadul, mungkin bisa pindah ke _Counter-Strike: Source_. Kalau ingin lebih jadul lagi, bisa beralih ke _Counter-Strike: Condition Zero_. Saya yakin, masih banyak loyalis CS jadul yang tersebar di dunia dan khususnya di Indonesia.

![Poster perpisahan Counter-Strike Online Indonesia](/blog/2019/07/images/cso-tutup-2.png) Perpisahan ...

Sampai saat ini, CSO Indonesia adalah game daring pertama dan terakhir yang saya mainkan. Terdapat emosi yang terkuras pada setiap ketukan _keyboard_ dan pencetan _mouse_ saat game dijalankan. Sedih, senang, marah semua melebur bersama capeknya mata dan tubuh karena posisi yang salah. Bagi pemain yang tiap harinya di _gamenet_, bacotan tetangga dan benturan _mouse_ dengan meja bisa jadi pemantik memori yang cukup berharga.

Saya tak pernah ingin berharap apa-apa, kecuali semoga tak merindu permainan yang sampai saat ini layak untuk digemari. Hehe~

Sudah, ah, terima kasih Megaxus ... selamat tinggal Counter-Strike Online Indonesia!

\[Gambar utama oleh Clker-Free-Vector-Images-3736 dari Pixabay\]
