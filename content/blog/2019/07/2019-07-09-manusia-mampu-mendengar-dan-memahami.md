---
title: "Manusia Mampu Mendengar dan Memahami (?)"
date: "2019-07-09"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "orang-bicara-harli-marten-m9jrkdxoqou-unsplash-fix-rmdzn.jpg"
aliases:
  - /post/2019-07-09-manusia-mampu-mendengar-dan-memahami/
---

![orang bicara](/blog/2019/07/images/orang-bicara-harli-marten-m9jrkdxoqou-unsplash-fix-rmdzn.jpg)

_Katanya, manusia mampu mendengar dan memahami?_

Bergerak, menyampaikan sesuatu kepada orang lain kerap kali membuat perut bergejolak hebat. Ragu. Maju atau mundur. Padahal kata yang akan disampaikan jelas-jelas sudah diolah sedemikan rupa, sematang-matangnya.

Dasar manusia yang hampir setiap hari _ngendon_ di kamar. Yang tak asyik diajak pesta tapi menggebu-gebu ketika diajak berdiskusi tentang buku setebal gitar akustik yang baru saja dibaca. Ia kadang iri ketika melihat manusia lain dapat mengucap ceplos-ceplosan. Seperti menunjukkan kalau mereka berani, apa adanya. Sementara ia tidak.

Di jalanan yang sepi, terbayang ular derik melata di depan. Ular tersebut diam saat sedang beraktivitas normal. Diam tenang. Namun akan berisik saat merasa terancam. Sebaliknya, ular piton cenderung diam, sepanjang waktu, tak menampakkan suara apapun (kecuali desisan normal) meskipun ada pengganggu di sekitarnya.

Kemampuan dasar manusia adalah berbicara. Mengutarakan pendapat yang sering berkecamuk dalam pikiran. Pada sisi lain, manusia berusaha membela diri jika manusia lainnya menyerang pendapat yang diutarakan. Menyerang pendapat yang merupakan perwujudan dari camukan pikiran.

Sesederhana itu.

Bukan berarti manusia dapat disamakan dengan ular yang secara insting hanya berpikir untuk hidup. Manusia lebih tinggi dari itu. Kata-kata yang meluncur dari mulut adalah kebanggaan personal. Cerminan kemampuan. Tentu membantahnya merupakan usaha untuk menjatuhkan rasa tersebut.

Padahal bantahan tak selalu berwujud serangan. Inilah pembeda antara manusia dan ular. Susunan ribuan kata versus desisan. Manusia memiliki logika yang akan berputar untuk menentukan suatu ujaran, apakah membangun atau menghancurkan. Lalu mengambil manfaat darinya, sebesar serpihan pun.

Di bawah pohon mangga, arus pikiran memadat menjadi kesimpulan. Apa benar manusia berevolusi menjadi ular? Mendesis tak karuan? Beringas kepada yang dianggap oposan?

Manusia mendengar. Memahami perkataan kawan dan lawan. Sehingga tuduhan tak perlu keluar. Bukankah seharusnya seperti itu?

Dalam hembusan angin semilir ia tersenyum. Keraguan untuk menyampaikan sesuatu kepada orang lain ternyata dipicu oleh keraguan lainnya--semua manusia tak selalu mau dan mampu mendengar dan memahami. Kelemahan yang ia rasakan justru merupakan kekuatan. Tahu kapan bicara, tahu kapan menanggapi, dan tahu kapan diam.

Apakah manusia tidak ingin saling mendengar, memahami, dan mengerti?

\[Foto oleh [Harli Marten](https://unsplash.com/@harlimarten?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) di [Unsplash](https://unsplash.com/search/photos/people-talk?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)\]
