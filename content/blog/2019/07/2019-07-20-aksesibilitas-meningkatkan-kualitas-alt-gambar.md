---
title: "Aksesibilitas: Meningkatkan Kualitas Alt Gambar"
date: "2019-07-20"
categories: 
  - "cuap"
tags: 
  - "blog"
coverImage: "kursi-roda-wheelchair-3088991_1280-rmdzn.jpg"
aliases:
  - /post/2019-07-20-aksesibilitas-meningkatkan-kualitas-alt-gambar/
---

![kursi](/blog/2019/07/images/kursi-roda-wheelchair-3088991_1280-rmdzn.jpg)

_Blas_ ... memikirkan kehadiran para difabel atau penyandang disabilitas di sekitar kita sungguh sulit. Terutama bagi orang yang terbiasa hidup di lingkungan orang-orang "normal". Seperti saya.

Ketika mencoba peduli, ada saja kehendak yang bergerak melawan rasa itu. Saya pernah mengalaminya sebelum Pemilu. Saya Pengawas TPS (PTPS). Secara struktural, saya berada di bawah Bawaslu. Berhak memberi saran, mengoreksi, dan melaporkan potensi kesalahan yang terjadi semasa pra hingga saat coblosan berlangsung.

Ada satu poin penilaian yang tak banyak dipedulikan oleh Kelompok Penyelenggara Pemungutan Suara (KPPS), yakni ramah tidaknya TPS terhadap para penyandang disabilitas. TPS tidak boleh berundak, atau lebih umumnya TPS harus aksesibel. Orang sehat, orang sakit, orang difabel berhak keluar masuk TPS dengan rasa nyaman dan mudah.

Misi saya memastikan syarat itu terpenuhi. Sayangnya, anggota KPPS yang saya beri saran untuk memasang papan pada undakan terbukti cuek. Padahal ini demi membantu beberapa tetangga yang selama ini perlu tongkat untuk berjalan. Perasaan saya? Tentu saja kecewa.

Berbulan-bulan setelah itu, Remotivi mengunggah video yang membuat saya kembali sadar bahwa tak hanya satu cara untuk membantu penyandang disabilitas. Video yang dimaksud berjudul [4 Prinsip Aksesibilitas Website](https://www.youtube.com/watch?v=U3mgxQweDMc), membahas penerapan aksesibilitas pada situsweb.

https://www.youtube.com/watch?v=U3mgxQweDMc

Menurut [Wikipedia](https://id.wikipedia.org/wiki/Aksesibilitas), aksesibilitas (_accessibility_) atau bisa juga disebut ketercapaian adalah derajat kemudahan yang dicapai oleh orang, terhadap suatu objek, pelayanan, atau lingkungan. Dalam banyak kasus, aksesibilitas disebut dalam konteks semudah apa suatu objek dapat dijangkau oleh penyandang disabilitas.

Seperti dalam judul, Remotivi fokus tentang aksesibilitas pada situsweb untuk para penyandang tuna netra dan penderita gangguan penglihatan lainnya. Secara ringkas, poin penting yang dibahas adalah pelanggaran yang dilakukan oleh situsweb-situsweb di Indonesia sehingga aksesibilitasnya rendah--penyandang disabilitas dipersulit ketika ingin membaca berita/artikel di sana menggunakan pembaca layar (_screen reader_).

Pelanggarannya:

1. Tidak ada fitur "_skip to content_" sehingga penyandang disabilitas harus berepot-repot mendengarkan penjelasan tak penting sebelum masuk ke konten utama.
2. Iklan yang berada di tengah tulisan.
3. Tidak ada keterangan teks pada gambar.
4. Berita yang dipotong menjadi beberapa halaman.

Remotivi juga menyinggung 4 prinsip untuk membangun layanan digital yang mudah diakses (aksesibel).

1. _Perceivable_. Informasi di dalam web harus dapat ditampilkan dengan cara yang dapat mereka rasakan.
2. _Operable_. _User interface_ dan navigasi pada web harus dapat dioperasikan oleh mereka.
3. _Understandable_. Informasi dan pengoperasian antarmuka mudah dimengerti bagi mereka.
4. _Robust_. Konten dapat diinterpretasikan dengan berbagai perangkat lunak termasuk teknologi bantuan khusus untuk disabilitas.

Dengan bahasan tersebut, saya jadi berpikir bagaimana _sih_ terapan aksesibilitas pada blog tercinta ini? Saya belum ingin repot-repot mempelajari detail panduan dari W3C alias [Web Content Accessibility Guidelines (WCAG)](https://www.w3.org/TR/WCAG21/). Sederhana dulu. Ada satu kesalahan yang kerap saya lakukan pada blog ini sehingga mengurangi aksesibilitasnya.

Merujuk pada poin pelanggaran ke tiga--_Tidak ada keterangan pada gambar_, blog saya sebetulnya sudah cukup baik. Saya sudah memastikan semua gambar dalam setiap postingan memiliki keterangan. Hanya saja, keterangan tersebut tidak berguna. "Keterangan" dapat berupa deskripsi (_caption_) atau _alt_ yang akan selalu dibaca oleh aplikasi pembaca layar (_screen reader_).

Saya belajar dari situsweb yang khusus menjelaskan detail soal aksesibilitas web: _web accessibility in mind_ (WebAIM), terutama pada topik [Alternative Text (Alt)](https://webaim.org/techniques/alttext/).

# Tentang Alt

_Alt_ merupakan salah satu atribut pada kode HTML untuk menjelaskan gambar yang akan muncul ketika tidak termuat atau untuk menjelaskan gambar kepada penyandang disabilitas melalui teknologi pembaca layar (_screen reader_).

Kode di bawah adalah contoh dari pemanggilan gambar gajah jantan menggunakan atribut _alt_.

```
<img href="gajah.jpg" title="gajah jantan" alt="kehidupan gajah" />
```

_Alt_ adalah atribut yang sangat penting. Di samping sebagai komponen aksesibilitas, ia dapat meningkatkan keterlihatan gambar di mesin pencari. Oleh sebab itu, menuliskan atribut _alt_ pada tag _img_ sangat diwajibkan.

# Perbaiki atribut alt

Kalau kata [WebAIM](https://webaim.org/techniques/alttext/), mengisi atribut _alt_ tidak boleh sembarangan. _Alt_ harus ditulis sesuai konteks dan sebisa mungkin tidak mubazir. Jika di sekeliling gambar sudah mendeskripsikan gambar tersebut, _alt_ bisa dikosongkan. "Dikosongkan", bukan "dihapuskan" sehingga kode seperti ... alt="" ... adalah hal normal. Inilah yang dimaksud dengan menghindari mubazir.

Saya menuliskan semua judul tulisan di blog ini ke dalam [daftar TODO](https://gist.githubusercontent.com/rafeyu/93fcfe74ad0e5878c410056730631e2f/raw/37540f009d7395373b4f3ebf2dc40ed9e703c0a6/tasks.org), kemudian memeriksanya satu persatu, apakah _alt_ dalam gambar yang saya posting sesuai konteks tulisan atau tidak.

Sampai saat ini terdapat 70 tulisan lebih yang telah saya unggah. Supaya tidak terlalu banyak, saya sampaikan saja satu artikel yang memiliki banyak gambar--_[Yang Dirindu dari Lebaran Adalah Jalanannya!](https://ramdziana.wordpress.com/2019/06/11/yang-dirindu-dari-lebaran-adalah-jalanannya/)_\--dan apa yang saya lakukan padanya.

Beberapa tahun lalu, saya menjadi penulis konten blog kawan kakak saya. Salah satu ilmu yang saya serap saat itu adalah cara mengisi _alt_ pada gambar. Pokoknya, _alt_ harus berisi kata kunci sesuai artikel. Cara gampangnya tempel saja judul artikel ke dalam _alt_. Gara-gara ilmu itu, lima belas gambar pada artikel tersebut semuanya saya beri "Lebaran di jalan" pada _alt_\-nya.

Cara tersebut bukanlah ide bagus apabila didasarkan pada penerapan aksesibilitas, oleh karena itu _alt_ pada setiap gambar saya ganti sesuai konteks artikel/tulisan. Misalnya, "berhenti di sekitar SMPN 3 Depok", "berhenti di Paingan", "berhenti di jalan Nusa Indah, selatan jembatan", dan "berhenti di pertigaan Mancasan".

![Contoh isi alt gambar](/blog/2019/07/images/edit-alt-gambar-rmdzn.png) Salah satu contoh pengeditan alt gambar

Ini bukanlah praktik yang mudah. Mengisi _alt_ sesuai konteks sembari mempertimbangkan apakah ia mubazir atau tidak adalah hal sulit. Saya masih ragu, _alt_ yang saya tulis sebetulnya mubazir, karena sudah adanya keterangan (_caption_) pada setiap gambar.

Kesulitan yang saya rasakan juga terjadi saat mengisi gambar utama (_featured image_)--gambar paling atas, biasanya menjadi latar belakang judul artikel. Pada WordPress, platform blog yang saya pakai, gambar utama menggambarkan satu gambar. Dalam artian, satu gambar hanya dapat diwakili satu _alt_ saja. Padahal beberapa artikel menggunakan gambar yang sama sebagai gambar utamanya (_featured image_). Ini merupakan masalah tersendiri karena satu gambar yang hanya dapat memiliki satu _alt_ itu kemungkinan besar dipakai oleh banyak artikel yang memiliki konteks-konteks berbeda. Alhasil, isi _alt_ bisa-bisa tidak _nyambung_ dengan konteks artikel.

Terapan aksesibilitas tak akan lulus di sini.

Saya tak tahu apakah saya dapat menerapkan aksesibilitas sepenuhnya pada blog ini. Namun perjalanan untuk ke sana merupakan hal yang sangat menarik untuk dicoba dan dituliskan.

\[Gambar oleh [Mabel Amber, still incognito...](https://pixabay.com/id/users/MabelAmber-1377835/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3088991) dari [Pixabay](https://pixabay.com/id/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=3088991)\]
