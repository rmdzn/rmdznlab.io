---
title: "Kamar Nomor Lima"
date: "2019-08-01"
categories: 
  - "cerpen"
tags: 
  - "cerpen"
coverImage: "pintu-dima-pechurin-jubjyfvcv00-unsplash-resized-rmdzn.jpg"
aliases:
  - /post/2019-08-01-kamar-nomor-lima/
---

![pintu](/blog/2019/08/images/pintu-dima-pechurin-jubjyfvcv00-unsplash-resized-rmdzn.jpg)

Yogyakarta. Kota keduaku setelah Jakarta yang super sibuk itu. Kota yang membuatku merasa nyaman beraktivitas pada siang maupun malam hari. Maklum, aku adalah mahasiswi yang kerap pulang malam setelah rampung mengerjakan segala tumpukan tugas dari dosen.

Aku masih di sini, baru lulus kuliah. Mendaftar lowongan pekerjaan di sebuah rumah sakit yang membuka cabangnya di Yogyakarta belum lama ini sebagai Pusat Layanan Panggilan. Untuk menghemat tenaga dan biaya transportasi, aku memilih pindah kos dari daerah Babarsari ke sekitar rumah sakit yang terletak di jalan Lingkar Utara.

Setelah satu pekan pelatihan di rumah sakit pusat di Jakarta, aku kembali ke Yogyakarta. Naik bus pada Sabtu sore. Menurut perkiraan aku akan sampai Kota Pelajar pada Minggu, Subuh. Namun tak menyangka, aku sudah sampai Minggu dini hari.

Kalau aku sampai tujuan lebih cepat, seharusnya dianggap beruntung, kan, ya?

Bagiku sial.

Aku turun dari bus lebih jauh dari tujuan awalku. Aku seharusnya sudah turun di jalan Yogya-Solo tapi akibat ketiduran, aku turun di jalan Sudirman. Itu lo, dekat Gramedia Kotabaru. Langsung saja kurambah ponsel di tas untuk memesan ojek online. Kos yang sudah kubayar di muka adalah tujuanku. Di situ ada adikku, Agis, yang sudah menunggu.

Kosku tak jauh dari jalan Lingkar Utara dan berada satu rumah dengan pemiliknya. Saat aku memesan satu kamar untuk kutinggali bersama adikku, aku sudah diberitahu bahwa penghuni kos tidak akan mendapatkan duplikat kunci. Kalau ingin pulang malam, kami harus janjian dengan Bu Mini dahulu, seorang Asisten Rumah Tangga (ART), yang selama ini dipasrahi mengurus kos.

Payah. Benar kata Bu Mini. Karena aku sampai di depan pintu gerbang pukul 02.30 dini hari, aku tak bisa masuk kamar hanya untuk segera membaringkan punggung yang sudah lelah ini. Gerbang dikunci rapat. Adikku tak diberi kunci untuk membuka sendiri pintu besi di depanku. Beruntung, bapak pengemudi ojek menemaniku sebelum dua orang lelaki yang tinggal di seberang kos, yang mengaku baru saja pulang dari ronda, menemaniku saat pengemudi ojek pergi.

Subuh. Bu Mini membuka pintu gerbang. Aku bergegas masuk ke sana.

"Pagi, Bu!"

"Pagi, Mbak! Sudah sampai dari tadi, ya? Silakan kalau mau segera tidur di kamar. Kemarin sudah saya pel," kata Bu Mini.

"Siap, Bu! Toh, adik saya juga sudah di sini sejak kemarin," timpalku. "Mbak di kamar lima sudah datang, Bu?"

"Nggak. Dia ke sini setiap hari Selasa sama Rabu."

Ketika aku memesan kamar, aku diberitahu oleh Bu Mini lewat WA kamar 5 sudah ada penghuninya dan selalu pulang ke kos ini setiap hari Sabtu dan Minggu. Lah, kok ini malah katanya datang setiap Selasa dan Rabu. _Nggak tahu, ah! Mending tidur!_

Tak ada perbedaan jauh di kos ini sejak aku menapakkan kaki dua minggu lalu. Pintu masuk ke kos adalah garasi mobil/motor yang tertutup pintu _rolling_. Berhadapan dengan tangga untuk naik ke kos ada sebuah pintu menuju ruang keluarga pemilik kos.

Kos berbentuk "L" dengan total tujuh kamar ditambah satu kamar yang ditujukan sebagai ruang tamu atau ruang bersama. Sampai ke atas, tiga kamar berada di kanan tangga. Ruang bersama ada di pojok utara. Mudah terlihat dari jalan yang terbentang di depan. Sebelahnya adalah Kamar 1 dan Kamar 2. Di kiri tangga terdapat Kamar 3 dan Kamar 4. Sebelum belok ke kanan, ke kamar lainnya, terdapat tempat jemuran dan kamar mandi dengan tangga ke bawah yang menghubungkan tempat itu dengan dapur pemilik kos.

Aku dan adikku memilih Kamar 7. Kamar paling pojok selatan.

Demi badan segar dan tidur pulas, aku langsung mandi. Sembari menunggu hari pertama kerja pada Senin esok. Segalanya menyenangkan, paling tidak hingga sore hari. Adikku yang sudah mencicipi tidur di kamar ini selama satu malam bercerita kepadaku.

"Kak, tadi malam aku dengar ada langkah kaki di depan kamar sebelah," kata Agis tiba-tiba.

"Palingan Pak Kos," gerutuku, masih dalam posisi gegoleran.

"Yakin deh, Kak. Aku sama sekali nggak ngerasa ada orang di luar. Nggak ada juga langkah kaki naik turun tangga."

Tepat sebelum aku membuka mulut, adikku _nyerocos_. "Dan nggak mungkin itu penghuni Kamar 5. Kata Bu Mini, dia dateng hari Selasa sama Rabu, kan? Bukan Sabtu sama Minggu kayak yang dibilangin lewat WA? Nggak ada suara buka tutup pintu juga."

Aku mengangguk.

Benar. Hanya aku, Agis, dan seseorang di Kamar 5 yang menyewa kamar kos ini. Aku cukup yakin Agis hanya takut tak beralasan dan terus meyakinkan dia bahwa Pak Kos lah yang tadi malam wira-wiri di depan kamar.

Hari sudah malam. Kantuk menerjang. Aku dan Agis memilih untuk mematikan televisi dan mencoba langsung terlelap. Hampir pukul 22.00. Sebuah suara membuatku membuka mata. Adikku yang tidur di sebelahku langsung menatapku, khawatir. Pelan-pelan kami duduk, mendengar dengan saksama langkah kaki pelan yang terdengar mendekat.

Merinding.

Tak perlu mengucap kata, tatapan kami saling setuju bahwa diam adalah jalan terbaik untuk saat ini. Perlahan aku menarik tubuhku ke jendela dekat pintu. Menyilakkan sedikit pojok gorden. Wajah adikku makin terlihat cemas saat aku memandangnya sambil menggelengkan kepala.

Ketika aku menjauhi jendela, langkah kaki kembali terdengar. Sekarang cukup keras. Langkah tak hanya terdengar di sepanjang koridor kos tetapi juga di tangga. NAIK TURUN!

Kami tidak tahan! Kompak kami menutup mata dan telinga dengan tangan.

"Udah?" adikku berbisik.

Aku membuka sedikit mata dan kupingku. Adikku mengikuti. Tak ada suara lagi. Sunyi. Lebih dari satu jam kami terjaga. Suasana tegang masih menyelimuti.

Agis akhirnya tidur. Aku tak ingat pukul berapa mataku benar-benar mengantuk. Akhirnya kurebahkan badan di kasur tipis yang kubawa dari kos sebelumnya.

_Srreeek ... sreekk_

Jangan bilang ini suara aneh lagi.

_Gruukk ... grukkk ... gruuuk_

Bukan dari depan kamar, bukan dari genteng. Kali ini suara muncul dari kamar sebelah. Suara seperti seseorang menggaruk dinding. Bukan satu dua kali, tetapi berkali-kali. _Ini nggak mungkin orang, nggak mungkin juga kucing_.

\*\*

Pagi harinya, kuceritakan apa yang terjadi semalam saat adikku tidur. Dengan wajah pucat, ia langsung merogoh ponsel dan menghubungi kawan akrabnya untuk menemaninya di kos saat aku pergi.

Hari pertama tak begitu melelahkan. Aku pulang ke kos hampir pukul 16.00. Kawan adikku masih di situ. Seorang laki-laki berkaca mata bundar yang cukup familiar. Ya ya ya. Seperti biasa, senyumnya selalu sukses bikin aku senewen atau malah kadang benci.

"Jangan bilang ada apa-apa." kataku kepada Anton.

Meskipun mengaku bukan anak indigo, Anton selalu berhasil menakut-nakuti aku dan Agis tentang apa yang ia lihat dan rasakan di suatu tempat. Aku mengumpat. Adikku punya banyak teman tapi kok ya harus dia yang diajak untuk menemani hari ini.

"Kak, kayaknya kita pindah aja, deh," Agis menyahut. "Kata Anton saat ke sini tadi, ada yang diem duduk di tangga."

_Kentut! Berkali-kali pindah kos, baru kali ini kami mengalami kejadian seperti ini._

"Mungkin, Kak," timpal Anton. "Bukan hanya rumah makan yang memiliki penglaris, kos-kosan--NGGAK NIAT NAKUT-NAKUTIN--dimungkinkan punya juga. Kos temanku dulu sangat ramai. Tapi, dari sepuluhan kamar, hanya satu kamar yang tak boleh disewa. Alasannya selalu sama, kamar itu sudah ada yang menyewa."

"Berarti kos-kosan itu nggak mungkin sepi?" tanyaku.

"Oo ... mungkin banget. Penglaris akan berjalan ketika ada tumbal. Kalau tumbal nggak dipenuhi, penyewa kamar akan diusir. Diusir dengan cara diganggu, entah ditakuti dengan suara, diperlihatkan dengan sesuatu, dan semacamnya. Kabar terakhir yang kutahu, kos itu sekarang sepi. Setiap ada penyewa baru, paling cuma betah beberapa hari, rekor terakhir hanya betah sebulan."

Tiba-tiba aku melihat ke belakang, adikku juga.

"Di kamar yang nggak boleh disewa itu isinya ada apa? Tumbalnya bentuk apa?" Agis kembali menghadap Anton.

"Aku kurang tahu. Temanku diusir oleh pemilik kos ketika ketahuan mengintip lewat kaca nako. Temanku lainnya berhasil mengendap dan melihat seperti ada cahaya lilin di atas meja di dalam kamar. Seperti ada taburan sesuatu di sebelahnya. Kadang-kadang semerbak bau amis."

Pada suatu waktu, kami bertiga mencoba melewati Kamar 5. Gorden tertutup rapat. Ventilasi udara di atas jendela juga ditutup koran.

Aku dan Agis memutuskan untuk berkonsultasi dengan tetangga yang sempat menemaniku kemarin. Mencari kos baru yang tak jauh dari tempat kerjaku dan tempat kuliah Agis. Selama itu pula, kami menginap di tempat kawan, di kos lama.

Hari Kamis. Aku resmi mendapatkan kos baru di sekitar Kampus UPN dan ingin segera minggat dari tempat terkutuk ini.

"Permisi," kataku menyapa Bu Mini yang sedang menyapu di dekat garasi. "Sebelumnya saya minta maaf, Bu. Saya mau pindah, tinggal ke tempat saudara. Saudara saya tidak ada teman soalnya."

"Oya, tidak apa-apa, Mbak. Silakan. Sudah pesan jasa angkut?"

"Sudah, Bu. Tinggal angkut. Itu," aku menunjuk mobil bak terbuka di balik pagar. "Biar cepat, takut tidak keburu soalnya. Hehe."

\*\*

Suatu hari, aku pulang dengan muka mendidih. Hingga tak sengaja membanting tasku ke depan Agis yang melongo.

"Apaan, sih, Kak?" tanya Agis.

"Tadi mampir ke rumah mas-mas yang nolongin kita cari kos ini, bawain kue. Ada yang aneh. Ibu-ibu berkumpul dan memandang kami dengan muka curiga."

"Ternyata, Bu Mini dan pemilik kos menyebarkan rumor kalau kita diusir dari kos karena kumpul kebo. TAIK!"

\[Foto oleh [Dima Pechurin](https://unsplash.com/@pechka?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) di [Unsplash](https://unsplash.com/search/photos/door?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)\]
