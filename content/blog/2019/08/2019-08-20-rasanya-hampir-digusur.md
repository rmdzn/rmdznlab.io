---
title: "Rasanya Hampir Digusur"
date: "2019-08-20"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "jalan-fog-1819147_1920.jpg"
aliases:
  - /post/2019-08-20-rasanya-hampir-digusur/
---

![jalan](/blog/2019/08/images/jalan-fog-1819147_1920.jpg)

Pagi itu, kami--ibu, bapak, bulik, dan saya sendiri--berada di atas mobil rental, melaju ke Bojonegoro. Beberapa kilometer setelah gerbang masuk tol di Ngawi sang pengemudi berceletuk, "nah enak kan, Buk? Kita jadi punya pilihan \[jalan\]. Kalau mau cepat dan ngerogoh kocek lebih, ya lewat tol. Orang Indonesia seharusnya gini, kudu mau berkorban tempat untuk dijadikan tol, memudahkan orang lain. Toh pemerintah punya peraturan jika ada yang menghalangi pembuatan infrastruktur kayak gini."

_COCOTE!! LAMBENE!!_

Ya.

Saya berteriak.

Dalam hati.

Saya hanya bersimpati kepada warga-warga tergusur atas nama pembangunan infrastruktur. Mereka mengorbankan rasa nyaman. Membangun rasa cemas akan masa depan.

Saya tak yakin, sang pengemudi yang asal _jeplak_ itu pernah merasakan bagaimana rasanya tersingkir dari rumah yang telah dihuni berpuluh-puluh tahun. Padahal, itu tak semudah menjual barang pribadi di Shopee atau melepas bibit ikan di kali. Sulitnya luar biasa.

Saat itu saya tak jauh beda dari sang pengemudi. Berpendapat hanya dengan bibir--dan sedikit-banyak perasaan. Kalau sekarang, beda lagi. Gara-gara rumah hampir tergusur, saya makin mengerti.

Eh ... bentar, bentar.

Hampir digusur?

_Yes_.

Cerita dimulai dari [Sri Sultan yang akhirnya setuju pembangunan tol di Yogyakarta](https://www.goodnewsfromindonesia.id/2018/06/27/dan-sri-sultan-pun-setuju-tol-solo-jogja-semarang-ini-rutenya) yang menghubungkan Semarang dengan daerah istimewa setelah sekian lama menyatakan [_tidak_](https://www.suara.com/bisnis/2019/06/13/150302/takut-rakyatnya-jadi-susah-sultan-yogyakarta-belum-setuju-pembangunan-tol)\--jalur Yogya-Solo dan Bawen-Yogya.

Kabar awal masih belum jelas, daerah mana yang akan dibangun tol. Lalu munculah _sriwing-sriwing_ di masyarakat, tol akan berada di Sleman utara. Daerah sana sempat heboh. Bahkan di [Selomartani](https://www.openstreetmap.org/relation/5615716), sempat terlihat orang-orang yang katanya dari Dinas Pekerjaan Umum (DPU) meneliti/mengukur tempat/jalan untuk pembangunan tol.

Kami yang ada di Maguwoharjo masih adem ayem dan itu berubah saat kabar tol di atas Selokan Mataram [mengemuka](https://finance.detik.com/infrastruktur/d-4084388/tol-yogya-bawen-bakal-melintas-di-atas-selokan-mataram). Sri Sultan ingin pembangunan tol melayang di atas selokan agar tak perlu banyak membebaskan lahan. Jelas. Kampung tempat saya tinggali yang mepet Selokan Mataram cukup geger. Tapi belum geger-geger amat ya.

Ada tiga opsi trase jalur tol di Yogyakarta. Atas pertimbangan potensi adanya lebih banyak cagar budaya di dua opsi trase, maka opsi mengerucut pada satu titik, wilayah desa Maguwoharjo. [Kata Badan Pelestarian Cagar Budaya (BPCB)](https://radarjogja.jawapos.com/2019/06/19/bpcb-sarankan-lewat-manisrenggo-lotte-mart/), titik di wilayah ini adalah titik paling memungkinkan. Tepatnya melewati Manisrenggo kemudian menuju Lotte Mart.

Rumah kami bersebelahan dengan Selokan Mataram dan dekat dengan Lotte Mart. _Waduh, Mak!_

Hari-hari menjadi sedikit suram. Perasaan tiba-tiba tak nyaman ketika membayangkan tol yang akan melewati jalan belakang rumah. Rumah saya memang tak terlalu mepet selokan, tetapi jika dihitung-hitung rerata lebar jalan tol, rumah saya kena. Tidak mungkin, dong, tidak ada batas aman lokasi pembangunan. Masa iya, di balik tembok dapur tempat saya sedang makan malam, bapak-bapak berhelm proyek berseliweran dan alat berat berdentuman.

Rumor berubah. Sempat melegakan. Katanya, tol akan dibangun di utara Lotte Mart. Ini bukan sembarang rumor. Seorang tetangga memiliki teman yang bekerja di DPU. Hampir setiap hari rapat, menentukan trase tol yang akan melewati Yogyakarta. Hasil rapat kerap dikabarkan kepada temannya yang juga tetangga saya.

Spekulan opini angkat bicara. Bisa saja tol lewat sini (selatan Lotte Mart), ujarnya. Jarak antara Selokan Mataram dengan Lotte Mart cukup lebar. Cocok jadi area untuk membuat jalan putar tol. Mirip jalan layang Semanggi di Jakarta. Opini ini didukung oleh surat kabar Harian Jogja yang kala itu menerbitkan berita soal tol dengan salah satu narasumbernya tetangga sebelah. _AAaah, akhirnya kami merasakan apa yang dirasakan oleh warga Kulonprogo. Bedol desa!!_

Saya bahkan sempat keliling cari koran tersebut, tak tahunya semua ludes. Malamnya saya baru ingat, tadi lupa mampir ke penjual koran depan PENI. Sial.

Seperti yang diduga. Keresahan menggila. Dari pagi sampai sore, bahasan tol tak kunjung selesai. Di pojok gardu, di burjo, di warung bakso, di angkringan, dan di warung kelontong. Saat itu, kami hanya menyiapkan mental sembari menghibur diri. Semoga semua baik-baik saja. Seolah-olah, kalau ada yang bilang, "_kan, dapat ganti untung!_" mending geser dulu, lah.

Hari yang dinantikan tiba. Kepastian. OOo ya, Ooo, tidak!

![Potret berita pada koran Kedaulatan Rakyat](/blog/2019/08/images/photo_2019-08-21_00-16-20.jpg)

Koran Kedaulatan Rakyat (KR) tertanggal 13 Agustus 2019 terbit. Kepala berita tertulis _Tol Yogya-Solo dan Bawen-Yogya Disepakati_. Apa yang menarik? Setelah membaca sedikit ringkasannya, tol tidak jadi lewat kampung sini. Dari Manisrenggo, tol akan muncul lewat Tajem. Sampai Lotte Mart belok ke barat, tol melayang di atas jalan Ring Road Utara. Lega, dong!

Lega.

Meskipun sudah ada kesepakatan soal trase tol. Rasa ragu masih mengundang. Kami masih merasa perlu antisipasi. Semoga memang tidak sampai lewat sini. Semoga ...

\[Gambar utama [Markus Spiske](https://pixabay.com/id/users/markusspiske-670330/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1819147) dari [Pixabay](https://pixabay.com/id/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1819147)\]
