---
title: "Bincang 'Ndlogok', Peluncuran Buku di Warung Mojok"
date: "2019-09-01"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "img20190828192934.jpg"
aliases:
  - /post/2019-09-01-bincang-ndlogok-peluncuran-buku-di-warung-mojok/
---

Selamat ulang tahun, [Mojok](https://mojok.co)! Selamat buat buku yang baru terbit [_Mojok: Tentang Bagaimana Media Kecil Lahir, Tumbuh, dan Mencoba Bertahan_](https://bukumojok.com/product/mojok-tentang-bagaimana-media-kecil-lahir-tumbuh-dan-mencoba-bertahan/).

Rabu lalu, 28 Agustus 2019, Mojok bersama [Buku Mojok](https://bukumojok.com/) mengadakan peluncuran buku berjudul 10 kata itu di [Warung Mojok](https://twitter.com/warmojogja). Di Jalan Kapten Haryadi, Jalan Kaliurang. Dari perempatan Kentungan yang sedang macet-macetnya, pergilah ke utara. Melewati Superindo, Pasar Kolombo, Gereja Katolik Keluarga Kudus, dan gardu induk PLN. Gampangnya, lampu bangjo kedua, belok ke kiri. Lokasi tidak jauh dari bangjo situ.

Acara dimulai jam 19.00--menurut poster. Faktanya, ia sedikit molor. Keuntungan buat saya yang sebelumnya was-was ketinggalan beberapa menit karena terlambat. Panggung terpampang di depan, di bawah kanopi lebar dengan spanduk kuning semirip poster yang [disebarkan](https://twitter.com/mojokdotco/status/1165583163105644544/photo/1). Dua orang duduk di kursi penonton. Di ruang utama warung--bangunan di sisi selatan, kru Mojok berkumpul. Bercerita dan tertawa. Setidaknya itu tebakan saya.

![Twit pengumuman dari Mojok](/blog/2019/09/images/twit-mojok-peluncuran-buku-crop-rmdzn.jpg) Twit pengumuman dari Mojok

Tak berapa lama setelah saya duduk, dua pewara (MC) berbasa-basi di depan panggung, sambil menunggu penonton berdatangan kemudian.

Saya baru pertama kali ke Warung Mojok. Saya juga baru pertama kali mendatangi acara peluncuran buku. Meski begitu, saya tak berharap mendapatkan banyak info dan hal baru. Biar jadi kejutan gitu. Mengalir.

Benar saja. Acara kali ini gila. Bayangkan acara pentas seni di kampung saat tirakatan. Nah, sang pewara sesantai itu. Bahkan lebih santai daripada acara tirakatan itu sendiri. Pakai kaos. Merokok pula. Haha.

![Dua pewara (MC) di depan panggung](/blog/2019/09/images/duo-pewara-mc-rmdzn.jpg?w=768) Duo pewara

Dari sini, saya bisa membayangkan bagaimana kelanjutan acara.

Prima Sulistya duduk di depan pewara. Bersiap menjadi moderator. Mbak Prima ini adalah pemimpin redaksi (Pemred) Mojok. Pengganti Agus Mulyadi alias Gus Mul, pria asal Magelang yang baru saja mendapatkan tulang rusuknya yang hilang beberapa waktu lalu.

Saya tuliskan _ndlogok_. Ya, memang _ndlogok_. Mbak Prima menyambut penonton sembari mengapit sulutan rokok.

Suasana agak tenang sebelum kemudian tumpeng tersaji di depan untuk dipotong oleh sang Pemred lama (Gus Mul) dan diserahkan kepada Pemred baru (Mbak Prima) untuk diserahkan lagi ke Aditya Rizki, sang _co-founder_ Mojok.

![Prima Sulistya dan Agus Mulyadi di depan nasi Tumpeng](/blog/2019/09/images/siap-potong-tumpeng-rmdzn.jpg) Siap memotong nasi tumpeng

"Ini dalam rangka apa to, potong tumpeng? Sebetulnya, hari ini bukan merayakan ulang tahun Mojok. Hanya peluncuran buku," kira-kira begitu katanya. "Akan ada acara perayaan ulang tahun sendiri, bulan September."

Buku _Mojok: Tentang Bagaimana Media Kecil Lahir, Tumbuh, dan Mencoba Bertahan_ bercerita tentang ... ya sesuai judulnya. Bagaimana Mojok (Mojok.co) lahir dan tumbuh. Kemudian bertahan hingga berusia lima tahun. Peluncuran buku malam ini mengundang lima dari banyak penulis buku tersebut; Agus Mulyadi, Ega Fansuri, Aditya Rizki, Aprilia Kumala, dan Rean Aqila.

Agus Mulyadi kini redaktur Mojok. Dirinya bercerita panjang lebar sambil membanyol. Bagi saya: _guyonane ndeso, merga kui aku nyambung_ (Banyolannya rasa desa, justru itu saya nyambung). HAHAHAHA. Dia bercerita, sebelum masuk Mojok, dia adalah tukang parkir di Mall Artos.

Lah, saya dulu sempat nonton [_Rurouni Kenshin: Kyoto taika-hen_](https://www.imdb.com/title/tt3029558/) di Mall Artos Magelang. Jangan-jangan saat saya keluar parkiran, Gus Mul ada di situ. Hehe~

Ega Fansuri adalah ilustrator Mojok bareng Rean Aqila. Masih ingat seorang laki-laki melihat dengan muka ketus sambil bilang ["Banu baj\*\*\*\*n"](https://www.youtube.com/watch?v=XmxMifAC5Go)? Nah, dialah Ega Fansuri. Model video yang kerap muncul di kanal YouTube [mojokdotco](https://www.youtube.com/user/mojokdotco). Berperawakan gempal. Tidak lagi gondrong. Irit bicara. Ia yang juga adalah teman Gus Mul sejak SMA hanya bercerita secara pendek bagaimana ia masuk ke Mojok.

https://www.youtube.com/watch?v=XmxMifAC5Go

Rean Aqila 'Lala' bergabung dengan Mojok saat pulang kampung ke Yogya dari perantauannya di Bandung. Mojok, media yang besar itu, ternyata markasnya di sini, dekat rumah. Perempuan berkacamata yang saat itu diteriaki _INTROVERT!_ oleh kawannya saat di panggung mendaftarkan diri sebagai ilustrator hingga diterima, beradaptasi, dan bertahan dengan lingkungan kerja.

Seperti Lala, Aprilia Kumala 'Lia' bergabung ke Mojok saat media ini membuka rekruitmen. Lia kini adalah redaktur. Satu dari lima redaktur. Lia bilang begini, _saya gak mau papa buka \[situsweb\] Mojok. Masa papa udah bayarin kuliah dll, eh anaknya cuma nulis soal logo Gudang Garam_. Haha. Mbak Novia Kolopaking~

Aditya Rizki dan Prima Sulistya berbagi, bagaimana Mojok pra dan pasca tutup selama dua bulan. Kata Prima yang sempat meninggalkan Mojok selama satu tahun, Mojok banyak berubah. Gus Mul mengiyakan. Mojok tak lagi produk hobi dan suka-suka seperti sebelum memutuskan tutup. Ia menjadi proyek serius. Mengubah kultur komunitas yang manasuka menjadi kultur industri yang "seragam" dan penuh tuntutan.

![Para pembicara di atas panggung](/blog/2019/09/images/narasumber-di-atas-panggung-rmdzn.jpg) Dari kiri ke kanan: Agus Mulyadi, Ega Fansuri, Aprilia Kumala, Rean Aqila, Aditya Rizki. Prima Sulistya berdiri.

Datang ke acara ini membuat saya teringat perkenalan saya dengan situsweb Mojok. Beberapa bulan sebelum ditutup, saya adalah pembacanya meski tak sesering ini. Karena belum menjadi penggemar berat, kabar ditutupnya Mojok tak membuat saya susah hati, walau sempat membuat saya merasa _mak deg_. Bahkan saya juga sempat baca tulisan pak Puthut EA yang katanya kantor Mojok pernah dikirimi kepala kambing (ternyata ada arsipnya di akun [Tumblr Mojok](https://mojokco.tumblr.com/post/159026723045/rahasia-kenapa-mojok-harus-tutup))--entah fiktif atau tidak.

Waktu berlalu. Sekali lagi, saya ucapkan selamat ulang tahun ke-5 untuk Mojok. Selamat menerbitkan buku baru! Salam buat Mbak Lala~

_FYI, ndlogok adalah gendeng, gila, dsb_.
