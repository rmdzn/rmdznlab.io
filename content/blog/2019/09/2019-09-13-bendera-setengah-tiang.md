---
title: "Bendera Setengah Tiang"
date: "2019-09-13"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "photo_2019-09-13_13-47-52.jpg"
aliases:
  - /post/2019-09-13-bendera-setengah-tiang/
---

![bendera setengah tiang](/blog/2019/09/images/photo_2019-09-13_13-47-52.jpg)

Malam hari, pasca pemberitaan meninggalnya B.J. Habibie, terbit unggahan dalam status WA kawan soal himbauan pemerintah untuk mengibarkan bendera setengah tiang. Tanda kehilangan. Tanda duka. Di seluruh Indonesia.

Merebaknya hoaks, termasuk meninggalnya eyang sehari sebelumnya, berefek pada kabar himbauan itu. Hingga ada yang berseloroh, _alah ... hoaks itu, bu Ani Yudhoyono aja kemarin gak disuruh (pengibaran bendera setengah tiang)_.

_Omong-omong_, itu tuduhan tingkat kampung, bukan tingkat nasional hingga internasional. Tinggal _kepret_ pakai _postingan_ berita resmi, si penuduh langsung bilang, _ooh ... ternyata benar, bukan hoaks_. Presiden lho ini yang meninggal.

Oke, itu masalah lain.

Saya tidak ingin meremehkan himbauan pemerintah soal pengibaran bendera setengah tiang mulai tanggal 12 sampai 14 September. Apalagi ini untuk eyang B.J. Habibie. Kok rasanya sayang sekali, kalau kita tidak mengikuti himbauan ini. Saking sayangnya, mungkin saya akan pasang bendera setengah tiang sendiri, tanpa himbauan sekali pun. Oiya, ini bukan berarti saya meremehkan tokoh bangsa lainnya yang meninggal lho ya.

Kiprah Eyang Habibie sungguh tinggi makna. Kalau tak ada eyang, mungkin saja tak akan ada pers sekritis Tempo, Tirto, dsb. Kalau tak ada eyang, mungkin saja kita tak pernah bermimpi punya pesawat sendiri dulu dan kini. Kalau tak ada eyang, mungkin saja rumusan IPTEK dan IMTAQ itu tak pernah kita mengerti.

Eyang Habibie mengawali pijakan negeri sebagai negara demokratis dan manusiawi. Pers yang bebas, entitas penyelenggara Pemilu yang independen, dunia partai politik yang tak didominasi itu-itu saja, penghormatan terhadap hak asasi manusia, dan sebagainya.

Saya ingat, sejak dahulu sampai sekarang, tak sedikit orang yang bilang keputusan terbodoh eyang adalah melepaskan Timor Timur (sekarang Timor Leste). Bos, masalah Timor Timur begitu kompleks. Tak sekadar NKRI harga mati yang kerap didengungkan akhir-akhir ini. Ada nurani di situ. Kemanusiaan yang dijunjung tinggi. Dengar dari [Tirto](https://tirto.id/warisan-ham-habibie-timor-timur-merdeka-hingga-bebaskan-tapol-orba-ehVt), referendum yang diadakan di Timor Timur juga adalah upaya untuk tidak merepoti presiden-presiden berikutnya. Biar mereka fokus pada masalah politik dan ekonomi nasional. Dua poin, terpuruk 32 tahun lamanya.

Soal Papua pun, eyang lebih memilih penyelesaian lewat diplomasi--tak perlu kekerasan--yang kemudian dilanjutkan oleh presiden Abdurrahman Wahid alias Gus Dur.

Bila boleh mengumpamakan, Eyang Habibie ini lebih demokrat daripada Partai Demokrat itu sendiri dan lebih pekarya daripada Golongan Karya itu sendiri.

Benar-benar sayang banget kalau kita tak ikut mengibarkan bendera setengah tiang. Cuma tiga hari. Saya tak ingin menafikan ungkapan "yang penting kita doakan, bukan benderanya". _Yes_, itu juga tidak salah. Saya ingin mengajak, yuk bersama tinggikan simbol bahwa kita kehilangan. Kita mencintai. Kita menyayangi Bacharuddin Jusuf Habibie.
