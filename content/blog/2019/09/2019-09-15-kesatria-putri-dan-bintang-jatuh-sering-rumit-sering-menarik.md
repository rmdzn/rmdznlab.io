---
title: "Kesatria, Putri, dan Bintang Jatuh: Sering Rumit, Sering Menarik"
date: "2019-09-15"
categories: 
  - "buku"
  - "ulasan"
tags:
  - "buku"
aliases:
  - /post/2019-09-15-kesatria-putri-dan-bintang-jatuh-sering-rumit-sering-menarik/
---

Saya mulai mengikuti serial Supernova. Baru saja. Kemarin sore kalau tak salah ingat. Bukan kemarin ini, "kemarin" beberapa hari yang lalu.

Saya belum beli bukunya. Padahal beberapa hari lalu mampir Toga Mas Affandi yang sedang jor-joran memberi diskon 30% sebagai perayaan ulang tahunnya yang ke-20. Bukan apa-apa. Cuma mau hemat dahulu (baca: beli buku yang benar-benar diinginkan). Untuk mengawali seri Supernova ini, saya membacanya lewat [iPusnas](https://play.google.com/store/apps/details?id=mam.reader.ipusnas) (saya sama sekali belum menonton [filmnya](https://id.wikipedia.org/wiki/Supernova_(film))).

_Kesatria, Putri, dan Bintang Jatuh_ (KPBJ). judul pertama serial novel Supernova. Ditulis oleh Dewi Lestari alias Dee Lestari dan terbit pertama kali pada bulan Maret 2012.

* * *

![Buku Supernova: Kesatria, Putri, dan Bintang Jatuh di iPusnas](/blog/2019/09/images/photo_2019-09-15_15-20-50.jpg) Buku Supernova: Kesatria, Putri, dan Bintang Jatuh di iPusnas

Reuben-Dimas dan Rana-Ferre-Diva. Tokoh-tokoh yang menggambarkan dua cerita dengan plot berbeda. Cerita dasarnya sih standar. Yang bikin unik adalah hubungan cerita mereka.

Cerita yang dihubungkan dengan cara menarik dan mengejutkan. Kecuali kalau kita membaca sinopsis (tulisan di belakang buku) KPBJ. Gara-gara sinopsis KPBJ, saya jadi mulai berhati-hati membaca sinopsis-sinopsis buku. Mending mampir GoodReads.

Sinopsis buku terkadang membocorkan (_spoil_) nilai sebuah buku. Nilai mengapa konten buku terasa unik dan berharga. Sebetulnya KPBJ menjebak pembacanya untuk berasumsi bahwa cerita Reuben-Dimas dan Rana-Ferre-Diva berada di dimensi yang berbeda. Maksudnya, saya kira cerita yang ditulis oleh Reuben-Dimas adalah cerita Rana-Ferre-Diva itu sendiri. Ternyata tidak--mereka berada dalam satu dimensi.

Jika tak membaca sinopsis, kejutan hubungan antara dua plot cerita baru akan terjawab (lebih tepatnya tertebak, meski masih terasa meragukan) di halaman 187, saat Dimas menyebut nama Diva sebagai gadis sampul di salah satu majalah miliknya. Saya yakin, jika tak membaca sinopsis, kejutan akan benar-benar menjadi kejutan.

Plot cerita yang meloncat, segar, membantu saya menikmati buku ini dari penjelasan-penjelasan ilmiah yang tak saya mengerti. Lebih tepatnya plot sinkronisasi diskusi Reuben-Dimas dan cerita Rana-Ferre-Diva.

Untuk memahami diskusi ilmiah Reuben-Dimas yang lebih baik, sepertinya kita perlu menyanding KBBI, buku fisika tentang teori tertentu, buku psikologi, dan mungkin filsafat. Rumit. Seringkali membosankan. Apalagi pada keping-keping (bab-bab) terakhir saat Reuben dan Supernova lebih banyak "bicara".

Plis, jangan beri bisikan. Saya berekspektasi bahasan ilmiah yang merumitkan masih akan ada di judul-judul berikutnya.

Habis ini _Akar_.

Besok baca lagi.

Besok cerita lagi.

* * *

**Judul**: Kesatria, Putri, dan Bintang Jatuh

**Penulis**: Dee Lestari

**Penerbit**: Bentang

**Tebal**: xii+332
