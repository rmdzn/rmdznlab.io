---
title: "#GejayanMemanggil Ubah Pandangan Saya Soal Demonstrasi"
date: "2019-09-30"
categories: 
  - "cuap"
tags:
  - "cuap"
coverImage: "gejayan-memanggil-salah-satu-pesan-rmdzn.jpg"
aliases:
  - /post/2019-09-30-gejayanmemanggil-ubah-pandangan-saya-soal-demonstrasi/
---

_Aksi beberapa pekan terakhir, mengubah pandangan saya terhadap demonstrasi._

Rohani Islam (Rohis). Organisasi pertama yang mengubah kepribadian dan pandangan saya terhadap sesuatu. Pelajar dengan karakter Islam yang kuat.

Saat itu saya menganggap organisasi kerohanian Islam sebagai _aku banget_. Perkawanan rekat saya dengan seorang penganut Islam taat semasa SMP menjadi pendorongnya. Saya cocok.

Organisasi Rohis di SMA menyalurkan rasa kecocokan yang saya nikmati. Organisasi tersebut (setidaknya Rohis di SMA yang saya ikuti) tak seperti yang kita bayangkan. Ia tak dipengaruhi oleh pemikiran organisasi tunggal yang sudah besar. Katakanlah Ikhwanul Muslimin (IM) yang lahir di Mesir itu, meski mentor-mentornya kebanyakan adalah Aktivis Dakwah Kampus (ADK) yang notabene bergerak ala IM.

Bukti kami tak terpengaruh dengan gerakan itu dilihat dari pandangan soal demonstrasi. Kami anti-demonstrasi. Kiblat yang kami pegang adalah para salaf. Demonstrasi menganggu ruang umum. Merusak fasilitas.

Saya percaya, pemerintah harus dipatuhi. Pemerintah harus diingatkan lewat belakang. Jaga kehormatannya. Pandangan ini masih terus saya amini. Buat apa demonstrasi? Menganggu. Lebih baik utarakan pendapat/tuntutan lewat tempat terhormat. Lewat belakang bilik, bukan depan panggung. Teriak-teriak sambil mengangkat tangan itu kuno.

Hampir setiap ada demonstrasi, sekecil apa pun itu, pikiran saya langsung berkata: tidak berguna.

Itu sebelum gerakan #GejayanMemanggil dilaksanakan.

Beragam revisi UU dan Rancangan Undang-undang (RUU) yang mengusik memicu terjadinya demonstrasi di beragam tempat. Dari depan DPR RI di Jakarta sampai daerah lain, Sumatera. Kalimantan sampai Sulawesi. #GejayanMemanggil adalah aksi demonstrasi yang diselenggarakan di Yogyakarta sebagai bagian dari gerakan #ReformasiDikorupsi. Sambil mengenang lokasi heroik tempat mahasiswa bernama Moses Gatutkaca meninggal pada aksi tuntutan turunnya Presiden Suharto dari jabatannya.

Saya tak ikut terjun ke lokasi. Namun melihat bagaimana demonstran beraksi di media sosial, membuat saya berpikir, demonstrasi tak selalu bikin emosi.

Jauh sebelum itu, Indonesia sebetulnya memiliki banyak kisah demonstrasi damai. Rutin malah. Aksi Kamisan yang selalu diselenggarakan tiap hari Kamis di depan istana kepresidenan, misalnya. Sayang, demonstrasi untuk menuntut penyelesaian pelanggaran HAM ini tak banyak yang paham.

Efeknya, peserta tak begitu besar. Liputan media konvensional maupun media sosial pun seadanya. Padahal kalau jadi rujukan demonstrasi damai, saya yakin semua mengiyakan. Hasilnya bagaimana? Sejak dahulu percuma. Tak ada gubrisan dari atasan hingga Aksi Kamisan masih berlangsung sampai episode 600-an.

Beda dengan #GejayanMemanggil. Isu RUU ngawur memaksa banyak rakyat bangun. Saya yang tak begitu paham maksud demonstrasi, menjadi lebih tahu apa gunanya. Orang-orang apolitis berusaha mengerti. Mempelajari isu politik pemantik gerakan #GejayanMemanggil dan memasukkannya ke dalam pikiran rasional.

Dilihat dari konteks kepentingan umum, demonstrasi mungkin saja menganggu. Saya setuju. Tapi tidak dengan #GejayanMemanggil. Ada kepentingan umum yang jauh lebih besar untuk dipedulikan daripada potensi kepentingan umum yang terganggu akibat demonstrasi.

Toh kalau disebut "terganggu", saya bakal bilang _ah ... tidak juga_. #GejayanMemanggil mampu membangkitkan dukungan yang luar biasa dari penduduk sekitar. Bagi-bagi buah, bagi-bagi minuman, dan dukungan dalam bentuk lain.

Saya jadi ingat dialog viral. Seorang yang apatis dengan aksi demonstrasi terpahamkan setelah seorang penjual makanan bilang (kurang lebih), "_ya, hanya mereka \[mahasiswa\] menyuarakan hak-hak kita, Mas._" Penjual itu tak membicarakan #GejayanMemanggil, tetapi demonstrasi mahasiswa yang lebih luas.

Demonstrasi terikat konteks. Nyambung atau tidak. Sesuai kebutuhan publik atau tidak. Sesuai kebutuhan kelompok minoritas, marginal, rentan atau tidak. Kalau tidak, saya mungkin kembali apatis dengan aksi demonstrasi. Kalau seperti #GejayanMemanggil? Jangan tanya lagi, saya siap dukung seribu permil.

Kalau bisa damai aja.

Kalau tidak?

Ya mau bagaimana lagi. Aksi kadang perlu mendisrupsi agar didengar. Aksi Kamisan, seanteng-antengnya demonstrasi itu saja tidak dilirik dan didengar. Bukan berarti saya mendukung kerusuhan lo, ya.

Esai ini selesai saya tulis hari Senin, 30 September 2019. Tepat saat #GejayanMemanggil2 diadakan di tempat yang sama. Damai. Aman. Meski makin banyak elemen masyarakat yang ikut kumpul, termasuk pelajar itu, kekhawatiran akan adanya provokasi sama sekali tak terjadi.

Mari doakan yang terbaik untuk aksi demonstrasi di luar #GejayanMemanggil. Jika pun kondisi rusuh, sampaikan, semoga tak lagi ada yang meninggal. Tak lagi ada aksi represif (sayangnya ini masih terjadi :( ). Tak lagi ada sakit untuk negeri ini.

\[Gambar utama: Twitter/@BukuMojok\]
