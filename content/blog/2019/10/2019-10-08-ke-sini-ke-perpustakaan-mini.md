---
title: "Ke Sini, ke Perpustakaan Mini"
date: "2019-10-08"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "photo_2019-10-08_11-35-16.jpg"
aliases:
  - /post/2019-10-08-ke-sini-ke-perpustakaan-mini/
---

_Ini bukan ajakan, karena esok, perpustakaannya belum tentu ada~_

Teras rumah semirip lorong itu ramai. Riuh rendah teriakan bocah-bocah kecil menghiasi suasana panas yang cukup terik. Bukan arisan, bukan pula lomba 17-an. Itu adalah perpustakaan baru yang dibentuk untuk meningkatkan budaya baca anak-anak di kampung.

Perpustakaan ini diinisiasi oleh mahasiswa Fakultas Ekonomi Universitas Islam Indonesia (FE UII) sebagai bagian Program Kreativitas Mahasiswa (PKM). Saya sering membaca karya-karya PKM lewat koran. Biasanya program diadakan oleh mahasiswa tingkat dua. PKM FE UII ini justru sudah dilakukan oleh mahasiswa tingkat pertama, semester satu pula. Para mahasiswa baru yang belum sampai satu bulan kuliah, sudah mendapatkan jatah PKM.

"_Iya, Mas,_" kata salah satu mahasiswa saat saya tanyakan topik ini. "_Padahal baru masuk 3 minggu_."

Perpustakaan digelar di teras Bapak RW. Sudah dua kali diadakan, yang pertama Minggu, 29 September 2019, yang kedua 6 Oktober 2019. Cuma dibuka dua kali. Awal-awal pertemuan dulu, ketika mereka menyosialisasikan program, teman-teman mahasiswa (saya panggil "teman", biar kelihatan akrab~) berharap perpustakaan tetap berjalan meskipun mereka sudah tidak menjadi pengurusnya.

Tanggal 29 September saya tidak sempat ke perpustakaan karena ada program kampung lain yang dilakukan berbarengan. Saya sempatkan 6 Oktober siang untuk _cus_ ke sana.

Anak-anak sangat ramai. Dari RT tempat saya tinggal maupun RT sebelah--masih di kampung yang sama, semua berkumpul. Tiga mahasiswa yang saya lihat sebelumnya ikut menyosialisasikan program adalah penanggung jawab perpustakaan ini.

"_Yang bukan anak kecil boleh ke sini?~_"

"_Boleh, Mas._"

![Sandal-sandal yang ditaruh sebelum menginjak perpustakaan](/blog/2019/10/images/photo_2019-10-08_11-35-21.jpg) Jejak tamu perpustakaan

Bukan hanya membaca buku, kegiatan di perpustakaan kala saya ke sana adalah mengerjakan kuis matematika. Satu orang mahasiswa menuliskan soal pada papan putih, anak-anak maju ke depan menjawab soal secara bergantian. Suasana menyenangkan. Hampir semua anak tertarik dan heboh. Sayangnya saya tidak mendokumentasikan peristiwa ini.

![Anak kecil menulis di papan tulis](/blog/2019/10/images/photo_2019-10-08_11-34-56.jpg) Satu anak sibuk menulis soal dan menjawabnya sendiri

Panasnya minta ampun~

Lokasi perpustakaan didesain sesantai mungkin. Beberapa botol minuman di sisi tembok, piring dengan segala suguhannya dan beberapa cemilan ditaruh di atas meja. Beberapa buku terhampar di tikar. Rak dengan isian buku yang tak begitu banyak bersender di samping papan putih.

Saya _njujug_ rak buku. Komik _Doraemon_, _Kobochan_, _Paman Gober_, _Miiko!_. Ada juga novel yang judulnya masih asing di telinga saya. Di rak sebelahnya, buku-buku pelajaran untuk anak SD dipajang. Saya membaca _Doraemon_ sambil kadang berbincang dengan beberapa anak kecil dan salah satu mahasiswa.

![Rak buku](/blog/2019/10/images/photo_2019-10-08_11-35-10.jpg) Rak buku perpustakaan

![Buku-buku di rak](/blog/2019/10/images/photo_2019-10-08_11-35-16.jpg) Buku-buku di rak

Ada yang senang, ada juga yang _bete_. Sambil mengerucutkan bibirnya, Putri, kelas 3 SD bilang, "_uuu ... aku kan belum \[diajari di sekolah\] akar-akar gitu. Jadinya males._"

Bahkan ada juga yang rusuh. Satu anak laki-laki memukul kepala belakang satu anak perempuan sambil mengintimidasi. Ia mendorong-dorong agar anak perempuan itu tidak mengambil buku di rak. Setelah pergumulan demi kondusifitas ruang, si perundung dipanggil ibunya. Ruang jadi tenang, saya yang sedari tadi capai berkompromi dengan anak itu akhirnya bisa membaca dengan nyaman.

Perpustakaan mini ini menjadi debut saya membaca, meski sebentar, komik _Smurf_, _Dorami_, dan _Cubitus_.

![Buku berserakan di tikar](/blog/2019/10/images/photo_2019-10-08_11-36-06.jpg) Serakan buku

PKM diadakan selama 3 minggu. Beragam program/kelompok disebar di kampung. Dua yang saya tahu adalah program bank sampah dan perpustakaan. Saya kira perpustakaan akan dibuka kembali 13 Oktober 2019, ternyata tidak. Hari ini (6/10) adalah hari terakhir. "_Hari ini kami tinggal mengumpulkan testimoni,_" kata mahasiswa yang saya ajak ngobrol.

Sekitar satu jam sebelum perpustakaan ditutup pukul 12.00, mahasiswa mengajak anak-anak untuk menonton video bersama.

"_Nonton apa nih?_" tanyanya.

"_Mawang aja, Mbak!_" satu anak menunjuk tangan.

Tak direstui. Tapi saya agak lupa akhirnya mereka nonton apa. Kalau tak salah Larva.

![Anak-anak duduk di depan laptop, menonton video](/blog/2019/10/images/photo_2019-10-08_11-37-03.jpg) Menonton video bersama

![Anak-anak duduk berkumpul di tikar](/blog/2019/10/images/photo_2019-10-08_11-35-07.jpg) Duduk di tikar

Belum sampai pukul 12.00, saya pamit pulang.

Saya tidak begitu tahu bagaimana PKM bekerja. Dari perbincangan saya dengan seorang kawan yang tinggal di kampung yang sama, PKM "sosial" akan kalah populer dengan PKM "yang memproduksi sesuatu". Perpustakaan adalah program sosial. Nasibnya bakal tak sehebat PKM program lain. Namun saya rasa, program perpustakaan sangat sarat makna. Mempraktikan apa yang populer sebagai "buku adalah jendela dunia".

Buku tak selalu murah. Perpustakaan umum juga tak selalu dekat. Kehadiran ruang pojok baca di perkampungan tentu adalah anugerah.

Mengutip perkataan Catlin Moran dalam _The Library Book_ yang disampaikan oleh seorang pustakawan, M Aan Mansyur dalam esai _[Membaca Bukanlah Gaya Hidup yang Baik](https://www.qureta.com/post/membaca-bukanlah-gaya-hidup-yang-baik)_:

_Perpustakaan di tengah-tengah komunitas adalah persilangan antara pintu darurat, rakit, dan festival. Mereka adalah katedral pikiran; rumah sakit jiwa; taman hiburan imajinasi._

Pesan untuk teman-teman mahasiswa FE UII yang menyempatkan membuka perpustakaan mini di sini dan bercengkerama dengan anak-anak: terima kasih, _ges_! Kalian sudah mengingatkan anak-anak dan orang tuanya bahwa masih ada buku bacaan yang siap disantap, tak kalah mantap dengan ponsel dan segudang gim di dalamnya.
