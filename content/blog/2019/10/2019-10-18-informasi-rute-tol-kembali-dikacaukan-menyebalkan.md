---
title: "Informasi Rute Tol Kembali Dikacaukan, Menyebalkan!"
date: "2019-10-18"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "jalan-fog-1819147_1920.jpg"
aliases:
  - /post/2019-10-18-informasi-rute-tol-kembali-dikacaukan-menyebalkan/
---

Beberapa pekan lalu saya bercerita tentang kemungkinan [rumah saya digusur](/blog/2019/08/2019-08-20-rasanya-hampir-digusur/). Demi meningkatkan infrastruktur negeri, katanya. Saya dan warga sekitar yang merasa "hampir tergusur" ternyata belum juga diperbolehkan leha-leha karena kabar pembangunan tol itu tak lekas jelas.

Sekitar bulan Oktober 2019 awal, tersiar rumor bahwa tol Yogya-Solo jadi melewati kampung saya. Kalau dibayangkan (menurut rumor tersebut), beberapa hektar tanah--tempat saya mengetik blog ini dan sekitarnya--akan dijadikan sebagai jalan putar tol mirip seperti jalan layang Semanggi di Jakarta itu.

"Tapi itu masih isu," kata tetangga yang saban hari mendapatkan kabar mengenai tol dari temannya yang bekerja di Dinas Pekerjaan Umum (DPU).

Koran Kedaulatan Rakyat edisi Jumat, 18 Oktober 2019 menampilkan berita di halaman depan berjudul _Tol Yogya-Solo Lewati 14 Desa, Yogya-Bawen 8 Desa_. Desa saya adalah salah satu dari 14 desa yang terdampak tol Yogya-Solo.

Apa yang saya harapkan dari berita ini adalah kepastian. Apakah dampaknya memang sampai kampung yang saya tinggali 20 tahun lebih ini? Atau, dampaknya masih sama seperti [kabar yang disampaikan 13 Agustus lalu](/blog/2019/08/2019-08-20-rasanya-hampir-digusur/)?

Duduk di ruang depan. Saya mulai membaca pelan-pelan. _Boo_! Ternyata tak ada kabar yang saya inginkan. Alih-alih hanya kabar bahwa ujung tol Yogya-Solo memang berada di jalan Ring Road Utara. Tidak ada kabar detail lainnya.

Kepala Dinas Pertanahan dan Tata Ruang DIY, Krido Suprayitno, justru berkata, "Saya belum bisa menyampaikan rute tol untuk mengantisipasi kenaikan harga tanah karena spekulan."

Jawaban yang sama juga disampaikan oleh Krido ketika ditanyakan tentang kawasan segitiga emas, kawasan yang diharapkan pertumbuhan ekonominya terdorong karena pembangunan tol di Yogyakarta.

Sepertinya ini adalah strategi pemerintah untuk kembali mengacaukan informasi soal rute tol. Padahal kalau diingat, info rute tol yang disampaikan sebelumnya justru sudah detail. Info yang sulit disanggah karena sudah tersebar di media cetak berkredibilitas tinggi di Yogya.

Ia adalah strategi pemerintah untuk mengantisipasi spekulan tanah. Saat pemerintah akan membangun infrastruktur, mereka pasti akan membebaskan tanah. Mereka memiliki patokan harga untuk tanah yang akan mereka beli. Pemilik tanah bisa jadi meningkatkan harga tanahnya berkali-kali lipat lebih banyak jika mereka yakin bahwa tanah mereka akan dilewati infrastruktur. Menaikkan harga setinggi-tingginya ini bisa jadi juga akan dilakukan para makelar tanah. Pemerintah tidak mau itu terjadi.

Strategi yang masuk akal tapi kok ya bikin emosi. Bikin _ketar-ketir_.

Iya, mereka berhasil menghapus spekulan tanah ... sekaligus juga melahirkan lebih banyak spekulan opini. Spekulan ke mana tol akan mengarah. Apakah ia akan menabrak Rumah Sakit? Kalau iya, menganggu, dong. Apakah Rumah Sakit akan setuju? Ganti ruginya berarti banyak, dong!

Seorang bapak berjalan ke arah saya menyampaikan pesan dengan pelan, "kalau tol jadi lewat sini, semua kena! Bapak X, Ibu Y, di dua rumah di sana mungkin besok stres, lo. Gara-gara tidak punya tetangga."

Oalah, saya kira apa pak!

Ada juga yang mulai mewacanakan pindahan, meskipun sebatas canda. Pindah ke mana ya besok? Ke Berbah? Ke Wonosari? Sepertinya pindah ke Pulau Nusakambangan juga enak. Bersama para napi, kita berseri.

Mau pindah ke Kulonprogo, dekat Bandara Internasional Yogyakarta (BIY) sepertinya asyik. Biar kalau mau ke bandara tidak kejauhan. Sayangnya tanah di sana pasti mahal. Di sana juga mau dibangun tol Yogya-Cilacap. Tergusur lagi, dong!

Saya malah lebih senang dengan para spekulan opini daripada spekulan tanah yang katanya menakutkan itu. Kami sudah _deg-degan_ dari dulu, berharap tidak terdampak pembangunan tol. Lebih baik kami menghibur diri dengan celotehan spekulan opini sembari menunggu kepastian.

Kepastian yang dikacaukan memang menyebalkan.
