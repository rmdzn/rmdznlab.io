---
title: "Catatan Setahun Bersepeda"
date: "2019-11-18"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "img20190606092317.jpg"
aliases:
  - /post/2019-11-18-catatan-setahun-bersepeda/
---

Pernahkah kita memiliki keinginan untuk melakukan sesuatu secara rutin, tetapi tak sempat-sempat? Semua orang sepertinya pernah. Bentuknya saja yang beda-beda. Termasuk saya ini.

Sebelum bapak membeli sepeda baru, sepeda yang sering ia pakai adalah sepeda yang juga saya pakai sejak sekolah SD sampai awal SMA. Sepeda tua Federal. Bertahun-tahun pensiun dari persepedaan, sebetulnya saya ingin bersepeda lagi tetapi karena sepeda itu masih sering dipakai bapak, keinginan saya tunda dahulu.

Baru sekitar dua sampai tiga tahun terakhir, bapak membeli sepeda baru. Akhirnya, sepeda Federal "dipensiunkan". Bapak lebih sering pakai sepeda anyar. Si Federal hanya sekali-kali dipakai untuk pergi ke masjid. Ia kerap menganggur di teras rumah. Menyender di tembok sebelah galon Aqua kosong.

Apa yang saya bilang tadi? Saya pengin bersepeda lagi. Nyatanya itu tidak mudah. Meski sudah ada sepeda yang dapat dipakai kapan saja. Apalagi sudah terpoles indah. Mirip niat untuk diet. Kapan? Besok. Kapan? Besok. Terus berulang. Tidak kelar-kelar.

Ketidaksegeraan untuk mengeksekusi tindakan yang bermanfaat bagi tubuh sendiri maupun lingkungan--begitulah saya menyebut aktivitas bersepeda--membuat saya "dijegal" oleh peristiwa.

Tanpa babibu per 1 September 2018 lalu. Saya mengendarai motor, lewat jalan Padjajaran alias Ring Road Utara. Dari arah timur ke barat. Di sekitar depan Hotel Lafayette, saya terjatuh. Meluncur secara miring di aspal sejauh kurang lebih 1 meter.

Kaget. Saya mengerem mendadak karena motor depan saya yang juga melakukan hal serupa untuk menghindari tabrakan dengan motor di depannya. Kecepatan cuma 40-50km/jam. Namun karena jalanan antara basah karena hujan dan sedikit kering, aspal menjadi licin. Motor terjatuh ke kanan. Kaca spion pecah. Kulit telapak tangan kanan robek. Lutut dan siku lecet. Jaket bagian pundak sobek. Beruntung sekali, pria bermotor yang berada di depan saya tadi berhenti. Ikut meminggirkan motor dan membelikan revanol, obat merah, dan perban.

Wangsit langsung turun. Di titik ini, saya menggumam, "_saya memang harus mulai bersepeda_."

## Mulai bersepeda

Setelah satu sampai dua pekan pemulihan, saya mulai menggowes. Ini tidak terlalu sulit, karena saya memang jarang bepergian. Kalau pun pergi, pasti juga tidak begitu jauh dari sarang.

Awalnya saya terlalu idealis. Berniat pakai sepeda ke mana pun saya pergi. Titik. Nyatanya itu sangat konyol. Di mana-mana, niat yang tak terlaksana adalah karena gol yang terlalu tinggi. Terlalu sulit. Tidak masuk akal. Saya menyadari ini. Alhasil saya berpikir pragmatis. Pergi ke manapun naik sepeda kecuali jarak yang jauh, saat membawa banyak barang, atau saat ingin pergi secara mendadak ke tempat yang cukup jauh.

Sambil jalan. Saya berencana membeli perlengkapan untuk meningkatkan keamanan dan kenyamanan saat bersepeda. Satu yang sudah direalisasikan adalah membeli manset atau deker. Melindungi kulit lengan dari serangan panas siang dan dingin malam. Berikutnya adalah membeli _buff_ sebagai masker. Serius banget ya saya? _Ember_.

![Sepeda berhenti di sebelah tugu BKKBN](/blog/2019/11/images/photo_2019-11-18_15-16-23.jpg?w=768) Berhenti di tugu BKKBN

## Pengalaman Bersepeda

Adalah menyenangkan mampir ke tempat yang selama ini belum pernah saya sambangi dengan sepeda. Ada rasa penasaran pada setiap genjotan pedal. _Nanti parkir di mana ya?_. Atau, _nanti bayar parkir tidak ya?_, yang dilanjutkan dengan rasa tidak nyaman ketika melihat tukang parkir yang sedang sibuk menarik uang parkir dari pemotor sementara saya bingung akan melakukan apa. Antara memanggil tukang parkir atau _bablas_ saja.

Baru kemarin Oktober saya pergi ke Samsat Pembantu (barat eks dugeman Hugos). Memarkir sepeda di antara belasan motor. "Berapa \[biaya parkirnya\] pak?", kata saya saat urusan sudah selesai. Si bapak agak bingung, "Seribu, Mas".

_Yah, bayar_. Padahal niat saya cuma basa-basi. Haha~ Tapi lumayan lah, diskon 50% dari harga normal.

Pesepeda memiliki privilese untuk _ndusel-ndusel_ fleksibel. Saat pergi ke kantor kecamatan Depok, di bawah balai sedang ada acara. Ibu-ibu tumpah ruah sampai menutup jalan. Saya langsung _ndusel_ saja sambil tetap bilang permisi. Kelakuan yang tidak mungkin saya lakukan ketika naik motor. Di tempat lain, saya juga pernah melawan arus jalan tanpa takut ditilang (_eit_, tapi hati-hati).

Baru kali ini juga saya masuk ke wilayah kampus menggunakan sepeda, seperti yang sudah saya tulis di [artikel saat saya ke UGM](/blog/2018/11/2018-11-23-remotivi-bicarakan-polarisasi-media-dan-independensi-di-digitalk-24-fisipol-ugm/). Saat itu saya menghadiri acara diskusi yang diadakan oleh pusat penelitian FISIPOL UGM, Center for Digital Society, di Convention Hall. Saya cukup yakin ada parkir khusus untuk sepeda mengingat UGM sendiri mempunyai program sepeda kampus. Ternyata tidak (atau karena saya bukanlah mahasiswa sana sehingga tak ada yang mengarahkan ke parkiran sepeda). Saya parkir di basemen. Samping pintu keluar. Sendiri. Tak ada sepeda lain yang menemani.

Saya malah jadi tahu, ternyata jalan depan Conventian Hall itu satu arah. Untuk keluar dari komplek FISIPOL UGM saya harus memutar ke arah utara, timur, lalu ke selatan. Balik ke jalan utama. Jalan putar tersebut benar-benar menyenangkan untuk dilewati dengan sepeda. Bukan hanya karena aspalnya yang mulus, tetapi juga karena melihat pohon-pohon di samping jalan dan melihat orang-orang berdiskusi di bawahnya.

![Sepeda berhenti di tugu Batu](/blog/2019/11/images/photo_2019-11-18_15-15-57.jpg) Berhenti di tugu Batu

## Keuntungan bersepeda

Tidak hanya karena jatuh _ndlosor_, motivasi terbesar saya adalah, pertama, menghemat ongkos bahan bakar bensin. Kedua, berjuang ramah terhadap lingkungan. Tak perlu mengeluarkan emisi buang. Paling cuma kentut doang.

Ketiga, bersepeda adalah aktivitas menyehatkan untuk para penikmat diam. Coba bandingkan dengan jalan kaki. Kita akan merasakan sesuatu yang tidak nyaman saat melewati orang lain di perkampungan tanpa sapa. Sebaliknya, kita bisa sedikit lebih cuek saat melewati orang lain dengan sepeda. Paling tidak, cukup modal senyuman jika ingin sapa.

Ini dipengaruhi oleh kecepatan gerak. Sesuatu yang bergerak lebih lambat memiliki tanggung jawab moral lebih besar terhadap sesuatu yang ada di sekelilingnya.

Bukan karena yang bergerak lebih cepat lepas dari tanggung jawab, tetapi karena mereka melewatkan banyak detail. Seorang kakek merokok Samsu di pinggir sawah dengan bergairah, ibu tua berkerut lesu menitih sepeda dengan krombong opak, anak kecil bersinglet berlarian dengan permen Hot Hot Pop di mulutnya berteriak ceria, dsb. Pemotor tidak akan memperhatikan objek sedetail itu dibanding pesepeda. Pesepeda juga tidak akan memperhatikan objek sedetail itu dibanding pejalan kaki.

Keempat, bersepeda mengizinkan saya untuk lebih leluasa memikirkan ide apa pun tanpa takut terdistraksi oleh sapaan dan kebisingan maupun kecepatan motor sendiri.

Di luar itu, dengan bersepeda pula, saya menikmati [situasi jalanan pasca hari besar](/blog/2019/06/2019-06-11-yang-dirindu-dari-lebaran-adalah-jalanannya/). Hasil mendambakan lingkungan sunyi tanpa banyak kesibukan. Menggali makna rehat dari camukan obrolan omong kosong dan akivitas nirdaya.

Panjang usia para pesepeda!
