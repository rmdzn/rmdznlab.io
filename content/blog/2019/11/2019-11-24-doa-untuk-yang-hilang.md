---
title: "Doa untuk yang Hilang"
date: "2019-11-24"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "crypt-229934_1280-grave-rmdzn.jpg"
aliases:
  - /post/2019-11-24-doa-untuk-yang-hilang/
---

Dua kejadian timpang di hari Minggu. Satu kejadian berpeluk bahagia dan tawa. Kejadian lainnya bersembab pilu dan sedu.

Satu kawan se-SMA pergi lagi. Ria panggilannya. Bukan kawan akrab karena tidak pernah satu kelas dan satu organisasi. Dulu kenal Ria karena satu kegiatan, proyek syuting [film](https://www.youtube.com/watch?v=23oePMaE5RU) untuk dilombakan, dan itu pun tidak begitu lama.

Saya tahu kabar terakhir dari obrolan kawan lainnya. Bertahun-tahun lalu, katanya ia terkena penyakit yang saya baru tahu akhir-akhir ini: lupus. Beberapa unggahan kawan Facebook saat itu berseliweran di linimasa, membuktikan Ria memang sedang sakit dan rawat inap.

Saya tak tahu tepatnya. Beberapa bulan kemudian kabar berganti menjadi kabar membahagiakan. Ria bertemu tambatan hatinya. Kalau saya tak salah ingat, pernikahan dilangsungkan di rumah. Tempat syuting satu tim dahulu.

...

_Ting!_ Satu kawan mengirim kabar. Sore tadi. Ria menghembuskan nafas terakhirnya. _Inalillahi_. Ingatan saya langsung melompat. Tepercik untuk menyampaikan rasa.

Ya, menghilang dan kehilangan adalah kepastian yang menyakitkan untuk kita semua.

Tanggal 14 Oktober di akun Instagram. Ria bercerita ia masuk rumah sakit per 7 Oktober setelah sebelumnya bepergian sendiri naik motor ke Solo. Saking capai badan, ia pulang menggunakan mobil. Hingga hari foto darah dalam kantong diunggah, ia merahasiakan rawat inapnya dari banyak orang. Harus tirah baring. Istirahat total dari aktivitas.

Ria butuh banyak doa. Ria butuh optimistis. Berkali-kali transfusi. Opname beberapa hari.

Namun, manusia tak berkehendak sebebas penciptanya. Yang Kuasa lebih berhak atas ciptaannya. Konon, Dia memilih untuk memeluk beberapa manusia lebih cepat dari dugaan. Dan saya percaya itu.

![Upacara di tenda, sebelum jenazah dimakamkan](/blog/2019/11/images/photo_2019-11-25_11-19-15.jpg) Sedu di bawah tenda

Selamat jalan Novi, Faizal, Previ, Ria, dan kawan lain yang sudah mendahului. Doa untuk kalian. Semoga keluarga tetap dikuatkan.

Semua dikuatkan dalam kehilangan.

_\[Foto utama oleh_ _[Philipp Ruch](https://pixabay.com/users/phio-100908/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=229934)_ _dari_ _[Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=229934)__\]_
