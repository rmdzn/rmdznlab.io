---
title: "Haru Biru Perjuangan Timnas Voli Putri di Sea Games 2019"
date: "2019-12-11"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "voli-putri-sea-games-2019-team-waifu2x-rmdzn.png"
aliases:
  - /post/2019-12-11-haru-biru-perjuangan-timnas-voli-putri-di-sea-games-2019/
---

Banyak orang terkesima dengan perjuangan Osvaldo Hay dkk di ajang Sea Games ke-30 2019, tetapi saya justru terpesona dengan Wilda Nurfadhilah dkk di ajang yang sama.

Wilda adalah kapten tim Voli Putri yang [terdiri dari](https://rs.2019seagames.com/RS2019/Athlete?cid=3&sid=28&gender=F) dirinya, Novia Andriyanti, Dita Aziizah (libero), Hany Budhiarti, Yulis Indahyani (libero), Tri Retno Mutiara, Amalia, Shella Bernadetha, Megawati Pertiwi, Arsela Purnama, Tisya Putri, Wintang Sakti, Ratri Wulandari, dan Agustin.

Voli dipertandingkan beberapa hari setelah pembukaan Sea Games. Sebagai penonton mereka saat berjuang di Asian Games 2018, tentu kali ini saya juga mengikuti permainan mereka di ajang olahraga terbesar di Asia Tenggara ini. Sayangnya saya tidak sempat menonton saat bertanding dengan Thailand, tapi mulai menonton saat melawan Vietnam dan Filipina. Kalah saat melawan dua yang pertama. Menang saat melawan negara tuan rumah.

Tim yang diterjunkan di Sea Games 2019 sedikit berbeda dengan yang diturunkan di Asian Games 2018. Di sini tidak ada Manganang bersaudara, tidak ada Asih, Yolana, Nandita Ayu, dan Berlian yang saat itu menjadi komentator di MNC TV.

![Pelukan kemenangan di lapangan Sea Games 2019](/blog/2019/12/images/mpv-shot0003.jpg) Pelukan kemenangan

Tim Voli Putri Indonesia gugur lebih cepat di Asian Games 2018. Mereka tak mendapatkan medali apa-apa kala itu. Namun tidak di Sea Games, mereka memperebutkan medali perunggu dengan kembali melawan Filipina.

Menyaksikan mereka selayak menonton perjuangan seseorang merebut hati. Kelahi dengan orang lain dan diri sendiri. Jatuh berkali-kali lalu berakhir manis. Tim Voli Putri seperti seseorang yang tidak berpenampilan tampan atau cantik menurut standar kebanyakan. Ia perlu melakukan sesuatu yang lebih untuk mendapatkan perhatian gebetan.

Berbeda dengan tim Voli Putra yang sudah memiliki privilise ketampanan atau kecantikan lebih. Bahasa kasarnya, cukup berbincang sebentar, tersenyum, hingga dirinya pun mudah mengalihkan pandangkan kepada dirimu.

Sampai saat ini, ketika saya melihat video ringkasan pertandingan Indonesia versus Filipina pada babak final, tenggorokan masih saja tercekat. Haru.

Agustin yang servis dan _spike_\-nya mengagumkan tapi sempat menumpu dengan keliru hingga kaki kirinya cedera, mengerang kesakitan. Megawati yang men-_spike_ bola dengan keras ke arah lawan lalu dengan senyum melegakan menyemangati kawan-kawannya. Wilda, sang kapten, dengan muka tenang merangkul teman-temannya setelah memblokir bola lawan. Indah dan Dita yang berulang kali mempertahankan bidang dari _spike_ lawan dengan sempurna.

Arsela yang melakukan pukulan _dummy_ dengan tenang dan sukses menipu lawan. Ratri yang melakukan _spike_ keras lalu berteriak menyemangati rekan-rekannya. Mutiara yang sukses mengumpan bola untuk dipukul keras/pelan oleh kawan ke bidang lawan. Shella, penyervis terakhir, bermain tenang menggantikan Agustin dan berkali-kali sukses memblokir serangan.

<blockquote class="twitter-tweet"><p dir="ltr" lang="in">Highlight set terakhir <a href="https://t.co/sVwmk1YqkR">pic.twitter.com/sVwmk1YqkR</a></p>— SON! (@rissonn) <a href="https://twitter.com/rissonn/status/1204017522011299842?ref_src=twsrc%5Etfw">9 Desember 2019</a></blockquote>

 

Tak mungkin menyebutkan satu persatu peran pemain. Masing-masing punya cara epik demi mengejar ketertinggalan 4 poin (8-12) hingga memperoleh 4 poin berturut-turut (dari 12-14 sampai 16-14) dan mengantarkan kemenangan.

Hormat buat semua. Kalian luar biasa!

Merinding, euy!
