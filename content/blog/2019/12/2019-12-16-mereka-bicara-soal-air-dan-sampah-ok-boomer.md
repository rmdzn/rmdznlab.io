---
title: "Mereka Bicara Soal Air dan Sampah, Ok Boomer!"
date: "2019-12-16"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "photo_2019-12-16_22-23-34.jpg"
aliases:
  - /post/2019-12-16-mereka-bicara-soal-air-dan-sampah-ok-boomer/
---

Dunia kelam. Hutan di Kalimantan, Sumatera, Amazon terbakar. Pohon-pohon ditebang, tercerabut. Es di kutub dan gletser di Puncak Jaya mencair lebih cepat. Musim tak lagi konsisten. Panas berbulan-bulan, hujan sekejap. Angin ribut. Longsor dan banjir menyertai. Sampah plastik bertumpuk di lapangan, kali, dan lautan.

Benar kata "pepatah". Makin banyak tahu, makin susah. Yang dilihat setiap hari hanyalah keredupan.

Bapak (mantan) kepala dukuh di kampung pernah membawa kabar baik. Optimisme. Pada sebuah kegiatan, beliau menyosialisasikan penanganan sampah kepada warganya.

_Apresiasi tinggi, Pak Dukuh!_

Kata beliau, pisahkan sampah kering dan sampah basah. Oke, bagus. Tolong lanjutkan.

Sampah basah atau sampah dapur untuk diberikan kepada tukang angkut sampah. Sampah kering (kardus, botol plastik, wadah cemilan, dll) silakan dibakar. Tunggu ... tunggu. Saya kira sosialisasi akan membahas pengelolaan sampah basah menjadi kompos dan sampah kering menjadi produk daur ulang. Ternyata solusi permasalahan berhenti pada buang-membuang dan bakar-membakar.

Bikin frustrasi? Iya. Sebagai seseorang yang ikut mengelola warung kelontong orang tua, saya bisa membayangkan berapa banyak sampah yang diproduksi dalam sehari. Mungkin juga salah satu sampah dari kami sudah mengalir hingga lautan sana. _Hai, kura-kura, lumba-lumba, dan lainnya maafkan makhluk egois ini. Kalian marah boleh kok :(_.

Asal membuang sampah basah dan membakar sampah kering sangat tidak membantu. Kolot. Yang ada, udara semakin tercemar gas metana, CO2, dan dioxin.

Akhir tahun ini, hampir semua sumur tetangga kering, sebagian besar bahkan mulai beralih ke sumur suntik. Kami beruntung memakai air PDAM. Sebuah privilese untuk menghindari kasus kekeringan yang sewaktu-waktu mengancam. Privilese ini pula yang memanjakan orang yang menikmatinya hingga terbiasa membuang-buang air.

Hanya karena punya air banyak, kita jadi lebih sering membuangnya karena alasan luber, lupa mematikan keran. [Masih ingat iklan di TVRI ini?](https://www.youtube.com/watch?v=t1W6TFVcpnE) Begitulah gambaran sederhana dari "membuang air".

https://www.youtube.com/watch?v=t1W6TFVcpnE

Orang tua saya masih sering melakukan ini di rumah. Menghidupkan keran PDAM, lalu ditinggal begitu saja. Kata beliau lebih baik (diisi) penuh sampai luber tidak apa-apa, biar tidak sering-sering menghidupkan keran. Sekarang, saya masih berusaha keras untuk memberitahu bahwa itu salah, meski selalu mentah.

Kita sudah lama terjebak dengan klaim: air adalah sumber daya yang dapat diperbarui. Alhasil manusia sejak zaman baheula akan berpikir: bebas pakai air, toh dia akan muncul lagi. Padahal kalau krisis iklim semakin buruk, bukan tidak mungkin air baku akan mengering. Faktor manusia juga dapat diperhitungkan. Lihat saja sungai Bengawan Solo sebagai air baku PDAM. Sekali tercemar, ia akan mempengaruhi stok air pelanggannya.

Menghemat air adalah kunci.

Boomer memperparah situasi. Dengan jadulnya pemikiran mereka, kita bisa saja tidak akan kemana-mana. Berputar-putar terus di satu titik. Perlu dicatat, 'Boomer' adalah simbol pemikiran. Ia dapat diganti dengan individu yang menyebarkan ide pragmatis tanpa acuh dengan kelestarian lingkungan. Bisa saja ia adalah kepala keluarga, saudara, tokoh kampung, bahkan tokoh negara.

Kita perlu terus belajar, menyegarkan pikiran. Apa akibat dari tindakan kita terhadap lingkungan. Terus amati kabar. Terus amati perkembangan. Tapi usahakan mungkin jangan jadi beban. Nikmati. Bertindaklah sekecil mungkin.

_Ok, boomer_.
