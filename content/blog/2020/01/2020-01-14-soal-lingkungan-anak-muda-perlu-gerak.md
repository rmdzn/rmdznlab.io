---
title: "Soal Lingkungan, Anak Muda Perlu Gerak"
date: "2020-01-14"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "photo_2020-01-14_14-34-47.jpg"
aliases:
  - /post/2020-01-14-soal-lingkungan-anak-muda-perlu-gerak/
---

Baru kali ini, ketua Karang Taruna di kampung sebagian besar kebijakannya soal lingkungan, tidak melulu sosial. Setidaknya di rezim ketika saya mulai sangat aktif di organisasi kampung setelah rezim Karang Taruna sebelumnya vakum sangat lama.

Saya boleh bangga dan songong. Menyarankan organisasi-organisasi kampung untuk mulai melakukan hal yang sama. Terutama yang di kota atau semi kota. Peka terhadap lingkungan. Sudah seberapa gersang kampung yang ditinggali. Sudah berapa kali sumur kering akibat kemarau.

Ini bukan tentang desa-desa yang tiap kali kemarau, daerah menjadi kering parah. Seperti Bantul dan Gunungkidul. Itu masalah yang butuh penanganan kompleks; dari Pemda sampai ke bawahnya, sedangkan topik yang saya singgung adalah gerakan di bawah. Anak-anak muda yang peduli dengan bumi yang menua.

Penghijauan adalah salah satunya. Di beberapa sisi jalan, kami mulai menanam pohon Ketapang, hasil pembibitan sendiri yang dilakukan oleh sang ketua. _Puja kerang ajaib_. Bersamaan dengan itu, pembibitan pohon lain juga masih berlangsung. Minggu ini rencana kami pindahkan mereka ke media tanam yang lebih besar untuk kemudian ditanam di sebelah pohon sebelumnya dan di sudut jalan lain.

Pohonnya Tabebuya. Mirip di jalanan Surabaya itu yang indah saat mekar-mekarnya. Semoga esok di sini juga begitu.

- ![Proses penanaman pohon](/blog/2020/01/images/photo_2020-01-14_14-34-25.jpg)
    
- ![Ketapang yang siap ditanam](/blog/2020/01/images/photo_2020-01-14_14-34-47.jpg?w=1024)
    

Ada alasan mengapa kami butuh lebih banyak hijau-hijau. Pertama, mempertimbangkan kebun dengan pohon-pohon tinggi di kampung sudah terjual ke perusahaan. Kabarnya, kebun tersebut akan menjadi gudang (_warehouse_). Entah kapan. Secara otomatis, lahan serapan air berpotensi berkurang yang merujuk pada alasan kedua; menjaga air tanah agar sumur tak mudah kering.

Baru tahun 2019 kemarin, sumur-sumur tetangga yang selalu bertahan hidup saat kemarau ikut kering. Setidaknya ada 5 sumur. Hingga ada yang beli air bersih bertandon-tandon, ada juga yang mulai menggunakan sumur suntik. Memang ada yang pakai air PDAM, tapi hanya sebagian kecil.

![Orang sedang mencampur tanah dan pupuk untuk media tanam](/blog/2020/01/images/photo_2020-01-14_14-35-10.jpg)

Proses pencampuran tanah dan pupuk

Program lain yang tak kalah seru, sehat untuk lingkungan adalah program bank sampah--angkut rosok barang bekas layak jual kemudian dijual ke pengepul. Sebetulnya tujuan utama adalah untuk menambah kas, tetapi secara tidak langsung anak-anak muda di sini ikut membantu mengurangi jumlah sampah yang dibuang ke Tempat Pembuangan Akhir. Memastikan yang dibuang adalah residu.

![Gotong royong mengumpulkan rosok](/blog/2020/01/images/photo_2020-01-14_14-39-22.jpg)

Gotong royong mengumpulkan rosok. Kami tidak mengeksploitasi dua anak kecil, mereka ingin ikut sendiri :)

Program lain yang sedang direbus adalah pemasangan spanduk peringatan untuk tidak membuang sampah sembarangan. Dan, _yaa_, kampung sudah beberapa kali memasang spanduk anti buang sampah sembarangan di beberapa ruas jalan. Pinggir selokan ada tiga dan pinggir jalan batas kampung ada satu. Yang masih tersisa adalah yang terakhir disebut.

Soal efektivitas spanduk masih dipertanyakan. Selama ini "hanya" satu dua gumul sampah dalam plastik yang dibuang di sekitaran spanduk (bukan karena ada spanduk lalu orang membuang sampah di situ, tapi karena lokasi itu memang titik orang membuang sampah sembarangan). Si pelaku tidak pernah terpergok. Sedangkan sampah sembarangan di Selokan Mataram tidak terbendung. Kalau saat tidak mengalir, sampah terlihat meski tak sebanyak selokan bagian timur. Sampah dalam gumul plastik dan sampah satuan (botol plastik, bungkus Ciki, bungkus Indomie goreng, dll).

Padahal kalau diturut, pelaku adalah warga lokal dan warga luar. Dari diskusi program spanduk anti sampah sembarangan terlihat orang-orang ingin sibuk memergoki dan memersekusi orang lain. Edukasi diri sangat penting. Apakah kita masih melakukan hal yang sama?

Saya tidak bermaksud menolak program spanduk toh kita sama-sama bisa belajar sambil jalan. Apalagi masih muda. Peka terhadap lingkungan, sikap sendiri lalu terhadap sikap orang lain. Jangan hanya menjadi moralis penghukum kelakuan orang lain.

Tulisan ini adalah dukungan saya kepada sang ketua dengan program-programnya. _Mumpung kita masih muda_, tuturnya dalam bahasa Jawa, _kalau sudah tua mana kepikiran begini_. Kehadiran anak muda pada isu seperti ini juga diharapkan mendorong orang-orang tua untuk melakukan hal sama. Tidak apatis dengan lingkungan mereka.

Anak muda mari peka. Sehat terus untuk kalian semua. Biar bisa belajar bersama. Berbuat baik bersama. Bermanfaat bersama.

_Lalu, kapan kita makan ayam bakar dan kemangi lagi? Kemarin mencret, tidak sempat ikut._

Punya akun Telegram? Sila mampir ke _channel_ blog ini di [@sintaskata](https://t.me/sintaskata). Terima kasih~
