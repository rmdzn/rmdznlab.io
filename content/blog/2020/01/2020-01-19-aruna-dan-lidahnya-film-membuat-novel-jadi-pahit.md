---
title: "'Aruna dan Lidahnya', Film yang Membuat Novel Jadi Pahit"
date: "2020-01-19"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "aruna-dan-lidahnya-kover-rmdzn.jpg"
aliases:
  - /post/2020-01-19-aruna-dan-lidahnya-film-membuat-novel-jadi-pahit/
---

Kalau saya ingin melihat film Indonesia yang bukan laga atau drama berat, saya akan menonton _Aruna dan Lidahnya_. Namun karena saya tidak hobi ke bioskop dan tidak _ngebet_ nonton film baru, saya "tunggu" saja kehadirannya di teve.

Film yang dibintangi Dian Sastro, Nicholas Saputra, Oka Antara, dan Hannah Al Rashid ini akhirnya tayang di teve saat musim liburan tahun baru 2020. Saya nontonnya ketinggalan, mungkin sudah sekitar separuh perjalanan, gara-gara keasyikan nonton film komedi nirfaedah di kanal sebelah. _Telek_.

Karena kesibukan pula, saya tetap tidak dapat menonton film secara runtut. Pasti kepotong. Jalan ke depan melayani pembeli. Berulang kali. Oiya, jangan dikira saya mengetik ini fokus lama menatap layar. Saya sering berdiri, melayani pembeli, lalu duduk lagi.

![Kover buku Aruna dan Lidahnya, dilihat dari aplikasi iPusnas](/blog/2020/01/images/photo_2020-01-19_11-24-31.jpg)

Untuk mengobati rasa kecewa, saya putuskan membaca bukunya saja. Tidak niat membeli karena sudah tersedia di perpustakaan digital iPusnas. _Aruna dan Lidahnya_ ditulis oleh Laksmi Pamuntjak dan terbit pertama kali pada November 2014. Omong-omong, baru kali ini saya membaca karya Laksmi Pamuntjak jadi tidak dapat membandingkan tulisan di novel ini dengan novel Amba misalnya.

Sebagai orang yang menonton filmnya terlebih dahulu dibanding baca novelnya, saya patut berkata sial. Saya malah sibuk menebak dan membayangkan mana Aruna, Bono, Nadezhda, dan Farish. Mana yang Dian Sastro, Nicholas Saputra, mana yang Hannah Al Rashid, Oka Antara. Untuk mengimajinasikan muka dan tindak tanduk semuanya saja saya sudah tidak mampu.

Aruna seorang ahli wabah yang diberi tugas untuk memastikan kasus flu burung di beberapa provinsi. Sembari bertugas, ia menikmati beragam kuliner bersama dua teman dekatnya; Bono dan Nadhezda, dan satu teman sekantornya, Farish.

Di novel ini Laksmi lebih memilih "flu unggas" (bukan "flu burung" yang lebih sering kita dengar) dan menggunakan Kementerian Mabura (Kemakmuran dan Kebugaran Rakyat) sebagai pengganti Kementerian Kesehatan. Nama kementerian yang aneh, tapi tak apa, namanya juga novel fiksi. Laksmi juga sudah menyebutkan di halaman sebelum bab Racau bahwa beberapa organisasi/lembaga memang ia fiksikan.

Penyebutan "NGO" yang tidak konsisten, bikin risih. Seringnya _en-ji-o_, penyebutan yang menyenangkan, unik. Tapi pada satu atau dua bab, penulis "kepleset" dengan penyebutan resmi yang membosankan: NGO. Di dunia Aruna saya juga dibingungkan dengan eranya, kadang pakai SMS, kadang pakai WhatsApp. Mungkin tahun 2014 harga paket data masih cukup mahal dan tidak semua memakai ponsel pintar. Mungkin lo yaa …

Laksmi bercerita dengan sangat menyenangkan di _Aruna dan Lidahnya_. Dari diksi yang dipakai dan alur. Bagaimana ia menyisipkan mimpi-mimpi pada awal bab setiap Aruna tertidur meski berulang kali saya kebingungan dengan maksud mimpi-mimpi aneh itu.

Saya tadi sudah bilang, film _Aruna dan Lidahnya_ berhasil merusak pandangan saya tentang karakter bahkan cerita pada novelnya. Konflik dan cerita pada film lebih menarik dari pada novel. Siapa saya menilai cerita film padahal saya tidak menontonnya secara penuh?

Begini. Film _Aruna dan Lidahnya_ memiliki konflik dan terselesaikan dengan baik. Saya puas. Farish yang punya hubungan gelap dengan mbak Priya (Ayu Azhari). Aruna yakin kehadiran Farish di sana, di dunia One World (lembaga tempat Aruna bernaung) yang korup, adalah demi mbak Priya. Ternyata tidak, Farish di sana karena Aruna. Ya, Aruna. Begitu juga saat Bono menyukai Nadezhda dan ternyata diterima.

Sedangkan di novel, akhir cerita terasa aneh … ya aneh saja gitu. Nilai "aneh" ini mungkin saja karena bias saya yang lebih dahulu menonton akhir versi film. Aruna yang berduka atas kematian Leon (rekan yang sempat disukai dan dibenci) sembari tetap menghormati dan kagum dengan sikap Farish karena membiarkannya begitu. Bono ditolak dan berakhir dengan dirinya dan Nadezhda yang berkawan lagi secara normal.

Novelnya juga ternyata lebih dewasa, lebih eksplisit. Sialnya. Saya jadi membayangkan Dian Sastro dan Oka Antara melakukan hal yang tidak-tidak. _Gaf!_

Jangan tanya soal penggambaran kuliner pada novel ini. Laksmi membawakannya dengan luar biasa, bikin lapar, apalagi ketika pembacanya iseng mengintip tampilan makanan yang disebutkan di novel lewat mesin pencari. Detail penyebutan makanan, tekstur, dan rasa. Kenyang dan enek yang tergambarkan dengan sempurna. Iya, saat Bono dan Farish makan lagi di restoran di seberang tempat makan sebelumnya tanpa memberitahu teman-teman lainnya. Kalau tak salah di Pontianak, _eh_ atau di mana lupa. Aplikasi iPusnas tak mengizinkan saya untuk mencari sebagian kalimat di buku.

![Kover Aruna dan Lidahnya edisi 2018](/blog/2020/01/images/kover-aruna-dan-lidahnya-2018-rmdzn.jpg)

Kover Aruna dan Lidahnya edisi 2018 ([laksmipamuntjak.com](http://laksmipamuntjak.com/books/aruna-dan-lidahnya-2))

Kover buku terbitan awal bergambar mangkuk berayam jago yang legendaris dengan huruf 'A' merah besar di dalamnya, berlatar belakang warna cyan terang. Kover buku terbitan Agustus 2018 bergambar empat pemeran versi film dengan latar belakang warna kuning. Beruntung sekali bagi yang memiliki versi awalnya.

* * *

**Judul**: Aruna dan Lidahnya  
**Penulis**: Laksmi Pamuntjak  
**Penerbit**: Gramedia Pustaka Utama  
**Tebal**: 432
