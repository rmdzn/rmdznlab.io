---
title: "Catch Me Up! dan 5.45 Merilis Lebih Banyak Endorfin dari Tubuh Saya"
date: "2020-01-24"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "koran-nawala-newsletter-rmdzn.jpg"
aliases:
  - /post/2020-01-24-catch-me-up-dan-5-45-merilis-lebih-banyak-endorfin-dari-tubuh-saya/
---

Esok hari, sila duduk di teras, atau di samping jendela kamar, atau di tempat yang pokoknya bikin nyaman … termasuk WC. Lalu buka aplikasi surel alias _email_. Lihat isinya. Berapa banyak yang menyenangkan? Berapa banyak yang isinya promosi saja? Kalau pun ada yang menyenangkan, pesan akan diakhiri dengan _blablabla diskon 30% jika membeli lewat link ini_. Yang lain? Mungkin tentang pekerjaan yang dalam titik tertentu membosankan.

Definisikan "menyenangkan", dong! Simpel. Apa yang bikin kalian senang, selain kiriman surel tagihan yang cair lo yaa. Pesan surel yang berisi konten asli, tidak tercemar dengan laporan pekerjaan, tidak didominasi promosi, bukan notifikasi komentar YouTube, media sosial lain, dan bukan pengingat belajar harian dari Duolingo mau pun yang semacamnya.

Bagi saya, pesan surel yang menyenangkan datang dari kiriman Wordpress.com, tulisan terbaru dari narablog-narablog yang saya ikuti. Selain itu kiriman berbahasa Inggris seperti intisari mingguan (_weekly digest_) dari DEV.TO. Tidak banyak, _btw_.

Mulai awal Desember 2019, isi surel saya tak lagi menjemukan. Dalam satu twitnya, bang Iman Sjafei ([@imanlagi](https://twitter.com/imanlagi))--_co-founder_ Asumsi yang kini tak lagi di Asumsi--merekomendasikan layanan [Catch Me Up!](https://catchmeup.id/). Ia adalah layanan nawala (_newsletter_) harian yang mengirimkan ringkasan berita terkini. Tidak mendalam tapi seru.

Target pasar Catch Me Up! (CMU) adalah pekerja muda yang sebegitu sibuknya. Yang tak sempat baca koran atau mengikuti kanal-kanal berita daring (_online_) yang merilis berita sangat cepat. Terlihat _asyique_, bukan?

CMU benar-benar menyegarkan. Saya tak _melebaykan_ diri. Memang begitu kenyataannya. Nawala (_newsletter_) disampaikan dengan bahasa yang mudah dimengerti meski campur aduk dengan bahasa Inggris. Ini plus minus _sih_. Bagi yang tak suka dengan campur aduk bahasa seperti itu, mungkin saja tidak merasa nyaman.

Mereka meringkas berita dengan pertanyaan yang dilanjut dengan jawaban lalu membagi konteks sesuai poin (atau sebaliknya, poin lalu pertanyaan). Misalnya, kiriman CMU tanggal 9 Januari 2020 membahas konflik Iran dan Amerika Serikat yang dibuka dengan poin _**Balas Dendam**_ (Iran menembakkan rudal ke pangkalan AS di Irak) lalu dilanjutkan dengan pertanyaan _**Ada hubungannya sama jenderal yang baru meninggal kemarin?**_ (Ya, ada, _blablabla_). CMU berusaha sebaik mungkin mengingatkan memori pembacanya dalam poin berikutnya, _**I haven’t been following the case…**_ (penjelasan mengenai awal mulai konflik secara ringkas).

https://twitter.com/sintaskata/status/1203993392834695170

Dalam satu nawala (_newsletter_), kadang CMU menuliskan dua sampai tiga (bahkan empat berita). Berita terakhir ditutup dengan cuplikan pernyataan (_quote_) yang disusul dengan tempat kemunculan peryataan tersebut.

CMU terbit dari hari Senin sampai Jumat. Hari Jumat ditutup dengan bonus konten Dataviz alias Data Visualization. Info dalam bentuk gambar yang cukup memberikan info penting mengenai berita yang disampaikan. Infografik, istilah umumnya. Akhir-akhir ini mereka juga membuka "rubrik" tanya jawab dengan sang penulis yang jawabannya akan dipaparkan pada akhir tulisan.

Sang pendiri sekaligus penulis adalah [Haifa dan Amir](https://catchmeup.id/tentang-kami/). Menjura! Hormat untuk mereka yang setiap hari menulis dan menerbitkan pukul 06.00 pagi. Kalau mau mengintip arsip nawala (_newsletter_), silakan kunjungi [catchmeup.id/daily-catch-up/](https://catchmeup.id/daily-catch-up/).

Itu permulaan.

Beberapa pekan setelah saya menerima nawala (_newsletter_) pertama dari CMU, satu media baru favorit dengan logonya yang berwarna merah jambu, [Asumsi](https://asumsi.co), membuka program baru bernama 5.45. Nawala (_newsletter_) yang dikirim setiap pukul 05.45, katanya. Tidak setepat itu, kadang lebih cepat, kadang lebih lambat. Tidak masalah toh yang penting kontennya. Saya membaca surel pasti lebih dari pukul 08.00 WIB juga.

Di sini, saya menikmati tulisan Bang Pange (Pangeran Siahaan), _founder_ Asumsi, bersama Dea Anugrah sebagai nawala (_newsletter_) pembuka. Bukan tulisan enteng, tapi juga bukan tulisan berat. Kehadirannya pun bukan sebagai kompetitor CMU, karena memang topiknya lebih dalam, berbentuk esai lumayan panjang.

Saya suka dengan penjelasan ala Pangeran dan Lisa Siregar, umpatan karena ketidakpuasan dan sindiran ala Dea Anugrah. Saya juga senang dengan tulisan kritis dan menantang ala Raka Ibrahim. Cerita dari Yudistira Dilianzia. Kabar dari Permata Adinda dan Ramadan Yahya.

![Tangkapan layar nawala (newsletter) Asumsi 5.45](/blog/2020/01/images/photo_2020-01-24_15-57-02.jpg)

Nawala (newsletter) 5.45

Isi nawala (_newsletter_) 5.45 lebih cair. Kadang berupa esai panjang, berita pendek, dan konten tanya jawab tentang isu tertentu. Satu yang favorit tentang topik yang terakhir saya sebut adalah obrolan soal Royal House Inggris yang disampaikan oleh Ilyas Husein dan Jonny Castrol (edisi 15-01-2020). Favorit lain adalah edisi hari ini (24-01-2020) hanya satu tulisan, tapi bikin bungah sekaligus emosi dan lelah. Raka Ibrahim dan Dea Anugrah menggugat narasi tak jelas tentang "milenial".

Sama seperti CMU, 5.45 hanya terbit dari hari Senin sampai Jumat.

CMU dan 5.45 sangat menyenangkan untuk dinikmati. Mereka mampu memantik kemunculan lebih banyak hormon endorfin dari tubuh. Cocok untuk yang hobi membaca, tanpa mau ribet menggotong koran tebal dan membaca _epaper_ panjang, secara objektif (membaca karena topik-topiknya) maupun secara subjektif (membaca karena penulisnya). Mereka cocok dibaca di halte bus Trans, di tengah perjalanan kereta, di samping etalase jualan, di belakang sembari berbuang, di kantin sambil menyeruput teh hangat, dan di titik-titik lain yang dianggap ramah manusia dan diri sendiri.

Tapi ada satu kekurangan yang mereka miliki, tentu saja, mereka lebih fokus pada isu umum, atau kalau boleh dibilang isu nasional, dan internasional. Kalau pun ada isu regional yang dibahas, lebih karena isu yang viral. _Halo, Keraton Agung Sejagat!_

Sebagai kawula Yogya yang ingin menyimak berita di sini atau di sekitarnya, Kedaulatan Rakyat, Harian Jogja, Tribun Jogja, dsb jelas masih tetap jadi andalan. _Hati-hati buat peternak kambing dan sapi di Gunungkidul, Lur!_

Untuk berlangganan,

- Catch Me Up! di [https://catchmeup.id](https://catchmeup.id)
- Asumsi 5.45 di [http://bit.ly/545Asumsi](http://bit.ly/545Asumsi)

Sedangkan untuk berlangganan blog ini silakan lirik kolom berlangganan atau untuk yang memiliki akun Wordpress.com bisa langsung ikuti lewat web atau aplikasi. Pembaca juga dapat mengikuti blog ini lewat akun Twitter [@sintaskata](https://twitter.com/sintaskata) dan akun Telegram [@sintaskata](https://t.me/sintaskata).

\[Gambar unggulan oleh [meineresterampe](https://pixabay.com/users/meineresterampe-26089/) dari [Pixabay](https://pixabay.com/)\]
