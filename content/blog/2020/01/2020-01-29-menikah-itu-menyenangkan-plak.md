---
title: "Menikah itu Menyenang ... PLAK!"
date: "2020-01-29"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "nikah-breakup-908714-pixabay-rmdzn.jpg"
aliases:
  - /post/2020-01-29-menikah-itu-menyenangkan-plak/
---

Pernikahan selalu terlihat menyenangkan. Foto _prewed_ di depan pancuran. Satu pasangan memegang payung dengan grojogan air di atas layaknya Niagara. Foto di pantai, di gunung, di sabana, di gumuk pasir. Pernikahan seperti sebuah tujuan hidup. Biar hidup tak kesepian. Berdua bersama selamaanyaaa … [Segala "sindrom" yang dapat mudah dibaca](/post/2018-10-29-sindrom-penikah-muda/), berpola, dan sering terasa membosankan bagi yang melihat.

Namanya juga pernikahan. Apa pun yang diperlihatkan pasti yang menyegarkan dan menyenangkan. Hal yang gelap dan suram tak banyak disampaikan. Dipendam di balik fosil daun lontar zaman Sriwijaya.

Ketika saya mendapati banyak cerita tentang kegagalan pernikahan, iya banyak, bbaaaanyaaaakkk, saya berpikir, _oooh, ternyata nikah memang tak sesimpel itu_. Pantaslah kuliah pranikah diperlukan. Selain kemampuan mengelola ekonomi, mengelola emosi, dan konflik rumah tangga harus dimiliki oleh pasangan yang berniat membangun mahligai rumah tangga.

![Meme Batman nyuruh namaste](/blog/2020/01/images/batman-namaste-rmdzn.jpg)

Satu atau dua bulan kemarin, saya mendapati twit adik angkatan yang pernah berbincang dengan seorang psikolog. Katanya, kalau masih jomlo, jangan kebanyakan mencari berita soal kegagalan pernikahan. Ia mengiyakan. Saya bermuka masam. Jujur, ini memantik saya.

Gila apa. Saya tidak mencari kabar tentang itu, justru si kabar datang sendiri. Dari orang dekat pula. Kalau pun jauh, yaa tidak jauh-jauh _amat_.

Seorang wanita dewasa menikah. Beberapa bulan akur, tenang, aman bersama suaminya. Namun pada suatu waktu, konflik terjadi. Si pria langsung mencari tempat lain untuk singgah. Si wanita yang merasa salah, langsung menuju indekos yang ditinggali suaminya. Meminta maaf. Sayangnya, tak ada respon. Didiamkan saja. Itu terjadi terus-terusan, berbulan bulan.

Akhirnya si wanita meminta cerai dan menuntut hak nafkah harta yang selama berbulan-bulan tidak diberikan. Si pria menyetujui. Tapi minta dicicil dan sayangnya tidak pernah cair hingga sekarang.

Seorang wanita lain menikah cukup lama dan sudah memiliki dua putri berusia kurang lebih 10 tahun. Baru-baru ini ketahuan, si suami ternyata kerap melakukan kekerasan. Dada si wanita pernah ditendang. Kepalanya pernah dipegang oleh suami dan dibenturkan ke dinding. Ucapan kasar selalu menyertai. Dua anaknya selalu menjadi saksi.

Saat dikonfrontasi ke rumah bersama keluarga besar. Si pria justru menantang, "laporkan saja ke polisi, saya berani!" Kurang lebih seminggu kemudian, keluarga wanita benar-benar datang bersama polisi. Si pria pucat. Saya kurang mengikuti apa yang terjadi setelah itu. Namun, si wanita bersama dua anaknya telah aman di rumah orang tuanya. Mereka resmi bercerai.

Cerita datang silih berganti. Ada cerita lain saat si pria posesif mati-matian, hingga melarang istrinya berkomunikasi dengan orang lain. Ada cerita lain lagi saat suami dan istri pertama ribut dengan istri kedua hingga membuat satu keluarga kerap berpindah-pindah kontrakan, dua anaknya putus sekolah.

Ini sangat mengkhawatirkan. Apalagi masa kini pernikahan menjadi sebuah gol, perlombaan, _cepet-cepetan_. Siapa sangka perempuan atau lelaki yang sempat dipacari adalah manusia yang tak mau mendengarkan dan suka main tangan. Egois. Pasangan yang selalu ingin mengubah sikap orang lain tanpa memperhatikan sikapnya sendiri yang begitu buruk.

Ada pesan yang tak pernah saya lupakan. Sedapat mungkin, ubah kebiasaan buruk sedini mungkin--sebelum menikah. Karena banyak sekali orang yang selalu beralasan untuk berubah setelah menikah. Padahal siapa yang mau mengubah kebiasaan saat ia berada di zona nyaman (sudah menikah)?

Konkretnya. Lelaki yang tak membiasakan ikut berkecimpung di urusan dapur dipastikan tidak akan melakukannya meski ia menikah. Mencuci baju, mencuci piring, menyapu, mengepel lantai, dan merapikan baju di lemari. _Mending pasrahkan saja ke istri?_ Iya, _to_?

Padahal kebiasaan ini vital. Berbagi tugas. Jangan beralasan dengan "_capek_ pulang kerja", jangan beralasan dengan "gengsi sebagai laki". Itu ego gender. Tidak bermutu. Biasakan juga berbincang bareng, mendalam, terbuka dengan pasangan. Jangan cuma dipendam. Nanti meledak, marah-marah melulu setelah menikah.

Sedikit banyak, cerita-cerita suram mempengaruhi pandangan saya soal pernikahan. Mungkin benar kata adik angkatan, jangan kebanyakan melihat cerita masalah pernikahan kalau masih jomlo. Bikin tidak yakin. Tapi mau bagaimana lagi jika cerita datang tanpa permisi? Cerita yang datang dari orang dekat, bukan gosip selebriti.

Saya kadang berpikir, apakah kekhawatiran ini berlebihan? Entahlah. Yang pasti, pandangan saya lebih melebar, tak melulu mendekam pada satu sisi yang selalu saya iyakan sejak lama. Tulisan ini sekaligus menjadi penanda tentang pandangan saya soal pernikahan.

Daripada sibuk sendiri mencari pasangan dan berusaha mengejar teman seangkatan yang tak lagi bujang, lebih baik sibuk mengenal dan menata diri sendiri terlebih dahulu, lalu belajar menuangkan emosi--bahagia, sedih, kecewa--secara tepat. Dan jadi diri yang paling baik dengan kebiasaan-kebiasaan yang baik pula.

Klasik.

_Ember_.

\[Gambar unggulan oleh [Tumisu](https://pixabay.com/users/Tumisu-148124/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=908714) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=908714)\]
