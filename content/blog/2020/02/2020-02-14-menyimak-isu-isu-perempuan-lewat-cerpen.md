---
title: "Menyimak Isu-isu Perempuan Lewat Cerpen; Haru dan Kesal"
date: "2020-02-14"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "photo_2020-02-11_13-33-21.jpg"
aliases:
  - /post/2020-02-14-menyimak-isu-isu-perempuan-lewat-cerpen/
---

Perempuan bukan hanya sebuah cerita dan tulisan. Menghormatinya bukan hanya dengan cara membaca karyanya. Kata Dea Anugrah, _perempuan bukan sekadar konsep. Setiap orang adalah pribadi, bukan setumpuk kopi yang identik dari selembar kertas_.

Menghormati perempuan adalah menyadari keeksistensiannya dengan segala hal yang melekat padanya tanpa objektifikasi. Setiap perempuan mampu berdikari, berpikir, dan menentukan yang terbaik bagi mereka sendiri tanpa perlu dituntut oleh lingkungannya.

Buku _Waktunya untuk Tidak Menikah_ (WUTM) menjadi sebuah paket komplit untuk menjembatani narasi tersebut. Ia ditulis oleh seorang perempuan kelahiran Malang, Amanatia Junda, yang secara menarik membahas isu-isu perempuan dalam bentuk cerita pendek (cerpen).

![Buku Waktu untuk Tidak Menikah bersender di dinding](/blog/2020/02/images/photo_2020-02-11_13-33-21.jpg)

Buku besutan Amanatia Junda sudah saya target cukup lama, yaa belum lama banget. Pokoknya sejak ia bersampul baru; gambar perempuan dengan kuncir kuda dan latar belakang hijau toska, menggantikan sampul lama dengan gambar jendela berlatar belakang coklat (terbit Oktober 2018). Secara sepintas, WUTM dapat menjadi salah satu rujukan bagi manusia-manusia yang ingin mengembangkan pengetahuan gender--terutama perempuan--dengan gaya tak kaku.

Beberapa cerita dalam WUTM berdasarkan waktu nyata (_true event_). Ada yang pasca kebakaran hutan besar tahun 2015 (_Denyut Merah, Kuning, Kelabu_), ada yang pasca pemilu mungkin pada tahun yang sama (kata kunci: kalimat "_mengolok presiden gagal legawa_" pada _Prelude_).

Asumsi awal saat saya membaca beragam penilaian atau cuplikan WUTM adalah cerpen-cerpen di dalamnya akan selalu menceritakan anak muda atau paling tidak kisah seseorang usia kepala tiga. Ternyata tidak begitu, dalam cerita Abha, sang tokoh dan kawannya justru manusia paruh baya.

Dari representasi tokoh itu, terlihat WUTM menampilkan isu-isu begitu beragam, tak sekadar perempuan yang kehilangan cinta atau memilih dua cinta yang berbeda. Ia menggambarkan seorang buruh yang memilih untuk menikah karena tekanan atau mengasuh anak angkatnya, buruh yang diperkosa dan yang tak mendapat gaji semestinya, hidup perempuan dengan _premenstrual syndrome_, pasutri yang bangkrut dan bagaimana sang istri menyadari emosinya, kehidupan ibu baru dengan masalahnya, dan sebagainya.

Keberagaman ini membuat saya bermuka masam, haru, bingung, kesal. _What the ...!_ Kadang kala ia juga memancing sedikit senyum di sudut bibir.

Imajinasi penulis pun "liar". Bagaimana bisa Amanatia membikin dunia sedemikian aneh tetapi tetap membawa masalah sakit hati dengan begitu menarik? Ada amputasi luka batin. Ada pula lumut luka yang dapat kembali ditelan untuk memantik emosi lama. Keabsurdan dunia ini tersaji dalam judul _Planet Tanpa Gravitasi_.

Bagi saya, alur cerita paling menarik ada di _Prelude_. Ia mengingatkan saya pada film _Vintage Point_. Kalau ditanya tentang cerita, saya akan menunjuk _Waktu untuk Tidak Menikah_ (ya, benar, judul buku ini diambil dari salah satu judul cerpennya), _Abha_, dan _Pisah Ranjang_ sebagai yang paling favorit.

Sedangkan untuk pernyataan favorit muncul pada cerpen _Pisah Ranjang_\--saat si istri merapikan tempat tidur suaminya setelah lama tak melakukannya.

> Bukan untuk rutinitas, bukan untuk ritme, melainkan untuk dirinya sendiri. Untuk kenyamanan berdua.

_Pamer dulu ..._

![Tanda tangan Amanatia Junda di buku "Waktu untuk Tidak Menikah"](/blog/2020/02/images/photo_2020-06-22_16-01-45.jpg)

* * *

**Judul**: Waktu untuk Tidak Menikah  
**Penulis**: Amanatia Junda  
**Penerbit**: Buku Mojok  
**Tebal**: viii + 178
