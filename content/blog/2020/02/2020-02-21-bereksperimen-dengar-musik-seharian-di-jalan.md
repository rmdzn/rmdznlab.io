---
title: "Bereksperimen Dengar Musik Seharian di Jalan"
date: "2020-02-21"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "headphone-malte-wingen-pdx_a_82obo-1000px-unsplash-rmdzn.jpg"
aliases:
  - /post/2020-02-21-bereksperimen-dengar-musik-seharian-di-jalan/
---

Katanya, musik mempengaruhi seseorang. Gabungan dari not-not yang membentuk nada itu dapat menjadi penyerap dan penyalur emosi dan sering menjadi sarana mengingat maupun melupakan.

Pada satu titik, ia bahkan diharamkan oleh sebagian mazhab pada agama yang saya anut. Takutnya, musik melalaikan pendengar dari mengingat tuhannya. Karena saya bukan manusia religius, saya tak akan membahas ini lebih jauh. _Skip_.

Saya sempat penasaran, bagaimana orang-orang dapat tergila-gila dengan musik. Teman sekolah saya, beberapa orang lewat, dan kakak saya wira-wiri dengan _earphone_ di kuping. Saya tidak dapat memastikan apakah dua yang pertama hanya ingin menghindari basa-basi orang lain, tapi untuk yang ketiga, musik sepertinya memang menjadi fitur kehidupan yang dimanfaatkan dengan sebaik-baiknya.

Di rumah dengar musik, di motor sambil berkendara dengar musik.

Kalau ada yang bertanya kenapa saya penasaran, maklumi saja ya. Saya memang tak terbiasa mendengar musik berjam-jam dan di mana saja. Mendengar musik seringnya di rumah. Kadang satu dua lagu yang diulang-ulang. Kadang satu album atau daftar lagu acak. Itu pun tidak setiap hari.

Untuk membuktikan rasa penasaran, saya mencoba sendiri pengalaman mendengarkan musik sesering mungkin. Bukan di rumah, tetapi di luar. Bukan naik motor, tetapi naik [sepeda](/blog/2019/11/2019-11-18-catatan-setahun-bersepeda/). Sambil berkeliling sejauh yang saya bisa.

Bagaimana rasanya?

## Siap-siap

Senjata saya pada eksperimen kali ini--selain ponsel dan _earphone_\--adalah tiga album dari penyanyi Norwegia, Aurora Aksnes, biasa disebut sebagai AURORA (semua kapital) di platform-platform musik. Mengapa dia? Yaa, karena saya memang suka.

![AURORA di Spotify](/blog/2020/02/images/photo_2020-02-20_09-56-44.jpg)

![](/blog/2020/02/images/photo_2020-02-20_09-56-47-1.jpg)

Sedangkan jalan yang kerap saya lalui dengan sepeda adalah jalan Padjajaran sampai Monjali lalu memutar ke selokan Mataram. Kadang lewat Paingan dan Tasura, pernah juga mengelilingi Ring Road.

## Berangkat

Hari pertama, saya mendengarkan album pertama _All My Demons Greeting Me as a Friend_. Hari kedua, album _Infections of A Different Kind (Step 1)_. Hari ketiga, mendengar album _A Different Kind of Human (Step 2)_. "Hari" yang saya maksud adalah simbol dari kumpulan hari. Jadi bukan secara harfiah satu hari.

Saat saya sampai di jalan depan Direktorat Jenderal Pajak, lagu _Conqueror_, _Warrior_, dan _Running with the Wolves_ menghentak dan secara tidak langsung genjotan sepeda mengikuti irama. Sepanjang jalan Padjajaran (Ring Road Utara) yang ramai benar-benar dapat menjadi ajang untuk manggut-manggut, menggantikan rasa kesal pada suara knalpot _blombongan_ atau pekikan klakson.

https://www.youtube.com/watch?v=dmVAmlpbnD4

Conqueror dari AURORA

Dari Hartono Mall sampai Hotel Lafayette, lagu AURORA berjudul _It Happened Quiet_ dan _Mothership_ tidak cocok diperdengarkan. Alunannya yang misteri dan pelan tak begitu menggugah selera. Namun, saat saya sudah sampai perempatan Kentungan dan belok ke arah selokan Mataram, alunan irama _Daydreamer_, _Animal_, dan _Queendom_ berhasil menghiasi suasana pagi menuju siang yang terik.

![Bersepeda di UGM](/blog/2020/02/images/photo_2020-02-20_09-57-02.jpg)

_Beat-beat_ yang cepat membuat saya mudah menggelengkan dan menganggukkan kepala. Bahkan saya percaya diri melakukannya saat berhenti di _bangjo_ (_traffic light_). Saya seperti tokoh di film laga yang sedang mengejar para penjahat atau tokoh di film drama yang sedang menikmati rutinitasnya paginya.

Sering juga saya merasa seperti Peter Quill dalam _Guardians of The Galaxy_. Berjoget dengan _headphone_ di kepala dan menunjuk-nunjuk makhluk di sekitarnya. Dengan muka sok asik.

Musik-musik berirama tegas masih nyaman didengarkan sampai selokan, timur Gejayan. Lagu AURORA yang pelan diawal dan menghentak di belakang atau yang tak begitu menghentak sejak awal cocok disetel di sini. Ada _I Went Too Far_, _Soft Universe_, _In Bottles_, dan _The River_. Ooh, saya juga sempat memainkan _The Seed_ di jalur ini sambil melirik selokan yang kumuh.

Saat perjalanan semakin ke timur. Sejak belakang SMP 4 Depok Babarsari, alunan lagu AURORA yang pelan lebih masuk ke suasana daripada yang lain. Jalanan itu memang lengang, tidak seramai selokan bagian barat. Lagu yang cocok disetel termasuk _Through the Eyes of a Child_, _Wisdom Cries_, _Half the World Away_ (koveran lagu Oasis), dan _Under Stars_.

![Bersepeda di jalan selokan Mataram](/blog/2020/02/images/photo_2020-02-20_09-57-07.jpg)

Judul-judul itu mengiringi genjotan sepeda yang pelan, angin sepoi, dan suara sepeda lain yang menyalip atau suara motor sesekali. Perasaan melankolis muncul. Pilu. Kadang juga memunculkan rasa gembira yang aneh.

Saat itu, saya merasa menjadi tokoh film yang sedang menyisir jalan karena hatinya terlukai. Terkadang merasa menjadi tokoh lain yang sedang ingin sendiri, tanpa perlu ocehan orang lain yang berisik.

Musik memang ajaib. Setiap jengkal nada yang didengar akan mempengaruhi emosi. Apakah ia menjadi candu? Mungkin.

Sekarang saya memilih berhenti. Bisa jadi bosan. Yang pasti saya lebih memilih untuk tidak menutup kuping ketika bepergian. Menikmati dahulu suara kebisingan lalu lintas. Kalau sedang ingin, saya akan melakukannya lagi.

\[Gambar unggulan oleh [Malte Wingen](https://unsplash.com/@maltewingen) di [Unsplash](https://unsplash.com/s/photos/music)\]
