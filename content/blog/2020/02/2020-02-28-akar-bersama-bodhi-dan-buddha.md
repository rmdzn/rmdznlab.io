---
title: "'Akar' Bersama Bodhi dan Buddha"
date: "2020-02-28"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "akar-sampul-kover.jpg"
aliases:
  - /post/2020-02-28-akar-bersama-bodhi-dan-buddha/
---

Halo hola!

Setelah menyelingi diri dengan bacaan lain, saya kembali membaca serial Supernova. _[Kesatria, Putri, dan Bintang Jatuh (KPBJ)](/blog/2019/09/2019-09-15-kesatria-putri-dan-bintang-jatuh-sering-rumit-sering-menarik/)_ telah lunas, sekarang giliran _Akar_. Ada harapan besar terhadap buku ini setelah yang lalu berhasil membuat saya makin menyukai cerita yang ditulis oleh Dewi Lestari (Dee Lestari).

_Akar_ mengingatkan saya pada memori belasan tahun lalu. Kakak saya meminjam novel tersebut dari perpustakaan sekolah atas rekomendasi guru Bahasa Indonesia. Kata sang guru, "bagus!".

"Apakah memang bagus?" tanya saya beberapa waktu terakhir. "_Emang_ paham?"

"Mungkin. Tapi yaa, _enggak_ paham," timpal kakak saya.

Setuju. Saya yang masa itu ikut membacanya juga tidak mengerti. Dunia yang diperlihatkan sejak _KPBJ_ dan menyambung ke _Akar_ sungguh rumit, apalagi saya masih SD. Kami juga tidak pernah membaca _KPBJ_. Mana paham, lah.

Membaca kembali _Akar_ adalah sebuah keberuntungan. Menguak kembali memori lawas yang mayoritas terlupakan bukan lagi tujuan utama. Saya hanya ingin mengerti dan menikmati.

_Akar_ dibuka dengan Keping 34 (Bab) yang menjadi bukti bahwa buku ini lanjutan _KPBJ_. _Akar_ juga dibuka dengan cerita Gio (kekasih Diva di _KPBJ_) yang sedang bergelut dengan kegundahan hati. Sekian baris cerita, saya akhirnya menemukan nama "Diva" di sana yang diceritakan hilang dan membikin Gio panik.

![Sampul buku 'Akar' di iPusnas](/blog/2020/02/images/photo_2020-02-26_21-51-26.jpg)

Sampul buku baru. Kalau sampul buku lama bergambar naga yang memakan ekornya sendiri hingga membentuk cincin.

Fokus cerita _Akar_ ternyata bukan pada Gio, tetapi Bodhi. Seorang beragama Buddha yang dilahirkan secara misterius. Ia tidak tahu siapa bapak ibunya. Yang ia tahu, Bodhi dirawat oleh seorang biksu di sebuah kuil. Kehadirannya pun misterius, seakan-akan sang Buddha lah yang menakdirkan Bodhi hadir di bumi dengan cara ditemukan di bawah pohon dan oleh guru Liong.

Dee memperkenalkan banyak hal di sini. Di samping Buddhisme dan istilah-istilah yang ada di dalamnya, ia juga memperkenalkan anarkisme dan punk--lewat cerita sekilas Bodhi.

Membaca _Akar_ pada tahun 2020 bagi saya sangat menakjubkan. Saya tidak ingin berlebihan _lho_. Anarko/anarki yang muncul sebagai sebuah gerakan beberapa tahun terakhir dan kerap disebut oleh pemerintah sebagai biang kerusuhan membuat kita mengerenyitkan dahi. Delapan belas tahun lalu, Dee menuliskan paham gerakan ini sepintas di _Akar_. Novel yang tak pernah tua!

Selain itu, Dee juga memperkenalkan proses pembuatan tato--saat Bodhi belajar menato dari Kell sejak menyiapkan jarum dan meniti kulit konsumen dengan hati-hati.

Pada 100 halaman pertama (dan seterusnya), saya menikmati alur yang dituliskan. Yakin tak membosankan. Alur saat bolak-balik (meski tak sesering itu) antara isi cerita Bodhi dan saat dia menceritakannya kepada teman-teman punk. Bahasa juga lebih mudah dimengerti dibanding _KPBJ_ yang penuh dengan sains jlimet.

Saya beruntung dapat memahami _Akar_ pada usia sekarang karena ternyata novel tersebut sesensual itu. Adegan pra hingga pasca menggambar tato di tubuh Star--tokoh perempuan seksi yang hanya muncul beberapa subbab tapi muncul di otak Bodhi hingga akhir--jelas tak mungkin saya pahami masa SD dahulu. Hahaha.

_Akar_ membawa banyak pelajaran bagus. Termasuk saat membandingkan begitu tekun dan bersemangatnya pemeluk agama baru (Tristan) dibandingkan pemeluk agama sejak lahir (Bodhi). _Hei, kita tersindir!_ _Akar_ juga membawa nilai filosofis yang berfaedah. Bagaimana manusia lebih baik fokus pada nilai dibandingkan apa yang terlihat.

> Lebih baik tanya mengapa menciptakan negara daripada menghafal nama presiden dan tahun berapa ia dilantik. Lebih baik tanya mengapa ada agama daripada tahun berapa ia mulai disebarkan.
> 
> Bodhi, hal. 151

Pembaca seperti belajar tentang Buddhisme. Bagi saya yang tidak pernah mengenal agama Buddha, saya jadi menebak-nebak segala fenomena yang dialami oleh Bodhi. Apakah fenomena tersebut merupakan bagian dari Buddhisme atau itu hanya berlaku di dunia Supernova?

_Sungkem_ kepada ibu Dee. Ia menyisipkan beberapa bahasa asing. Ada Thailand, Khmer (Kamboja), dan Prancis. Konten jadi makin meriah. Sayangnya, sebagian obrolan dalam bahasa tersebut tidak diterjemahkan. Contohnya saat Bodhi bertanya kepada petugas kereta api dan berkata, "_Chaa rawn?_". AAaarrggghh.

_Akar_ yang menyenangkan. Cerita ditutup dengan kisah menggantung. Siapa Petir?

Yap. Habis ini adalah _Petir_.

Besok baca lagi :)

Omong-omong, saya ingin berterima kasih kepada Perpustakaan Nasional atas dibuatnya aplikasi iPusnas. Menjuuurrraaaaa.

* * *

**Judul**: Akar  
**Penulis**: Dee Lestari  
**Penerbit**: Bentang  
**Tebal**: 256
