---
title: "Surat Adikku"
date: "2020-03-06"
categories: 
  - "cerpen"
tags: 
  - "cerpen"
coverImage: "letters-1724704_1920-amplop-surat-pixabay-rmdzn.jpg"
aliases:
  - /post/2020-03-06-surat-adikku/
---

Aku masih belum percaya, tidak lama lagi kesenanganku mungkin terenggut oleh tanggung jawab lebih untuk mencinta. Namun itu masalah gampang, aku masih tetap dapat melakukannya meski kadang sepintas bayangan keraguan membuatku mual.

Aku terus mencoba mengusir pikiran buruk bahwa barang yang ada di depanku adalah satu-satunya yang kurindukan saat masa lajangku berakhir. Payah memang. Aku yang seharusnya memikirkan strategi untuk mendapatkan uang lebih banyak justru malah disibukkan dengan, yang kuakui, tak berfaedah.

Kuhentikan dulu lamunanku. Tanganku sedang siap mengetik kata di peramban saat ponselku berbunyi. Hmm … WhatsApp dari adikku.

"Aku meninggalkan surat di bawah papan tikmu."

He?

Amplop putih dengan lem yang masih utuh. Di dalamnya selembar kertas A4 ditekuk sekali.

_Kepada masku,_

_Selamat, Mas! Akhirnya kamu bertemu seorang perempuan yang kaudambakan. Yang bahkan dengan cara tak terduga. Perempuan pekerja keras. Perempuan yang dari segi fisik memang seperti tipemu kaubilang._

_Izinkan aku titip pesan untukmu. Boleh, ya?_

_Oke, bismillah. Mulailah belajar merelakan. Meninggalkan aktivitas yang sangat kausukai. Ada dua maksud dalam pesanku. Pertama, meninggalkan aktivitas secara total. Aktivitas yang mungkin membuat pasanganmu tak nyaman dan marah. Kalau kamu masih dapat bertanggung jawab dengan hobimu dan mampu berkompromi dengan pasanganmu, pergilah ke maksudku yang kedua yakni meninggalkan aktivitas sementara demi pasangan. Meninggalkan saat pasanganmu membutuhkan bantuan mengurus rumah tangga, misalnya._

_Biasakanlah dirimu dengan urusan domestik. Urusan rumah tangga. Merapikan kamar tidur, menyapu dan mengepel lantai, mencuci pakaian, mencuci piring dan gelas kotor. Aktivitas yang bakal kauanggap berat kalau tak kaumulai lakukan sekarang. Pasanganmu adalah rekanmu. Bukan pembantumu._

_Lakukanlah komunikasi. Jangan seperti bapak ibu yang sering ribut tak pernah memahami. Sekali komunikasi, jangan biarkan egomu menguasai. Pasanganmu adalah pribadi yang berdikari. Mohon jangan paksakan kehendakmu kepadanya. Pertimbangkan pemikirannya. Saling bicara lalu temukan jalan tengah yang mendamaikan bagi kalian._

_Aku kagum dengan pasangan yang berprinsip setiap masalah harus selesai hari itu juga, sebelum tidur. Semoga kalian begitu. Pokoknya jangan mempersulit masalah sekaligus jangan juga menyepelekannya._

_Terakhir dariku. Setelah menikah nanti, semoga hidup tentram, penuh cinta dan kasih sayang sampai akhir hayat._

_Pagi ini syahdu,_

_Adikmu_

Pelan, kuletakkan surat itu di atas papan tik. Punggung kutaruh di senderan sofa. Hari ini aku tak ingin melakukan apa-apa.

(sorot teks di bawah ini untuk melihat akhir alternatif):

Mukaku mungkin merah. Langsung saja kertas kuremas. Sumpah, adikku sok tahu.

\[Gambar oleh [Catkin](https://pixabay.com/users/Catkin-127770/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1724704) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1724704)\]
