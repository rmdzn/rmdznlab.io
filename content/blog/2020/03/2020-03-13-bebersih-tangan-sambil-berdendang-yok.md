---
title: "Bebersih Tangan Sambil Berdendang, Yok!"
date: "2020-03-13"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "wash-hands-4906750_1280-cucitangan-rmdzn.jpg"
aliases:
  - /post/2020-03-13-bebersih-tangan-sambil-berdendang-yok/
---

Awal tahun yang sibuk dan menegangkan. Virus corona dari Wuhan atau yang mulai disebut dengan nama resmi COVID-19 menghantui seluruh dunia. Bikin tegang. Waspada. Was-was.

Imunitas warga Indonesia yang sempat diglorifikasi berminggu-minggu akhirnya runtuh. Tiga puluh empat warga positif COVID-19 dan mungkin bertambah setelah tulisan ini terbit.

Tentu ini menjadi kabar buruk. Rasa senang karena rasa aman tiba-tiba tergusur dengan rasa tidak nyaman yang terus menggedor-gedor dengan keras. Manusia boleh memilih, ingin larut dengan perasaan takutnya atau mengambil sisi positif lalu bertindak.

Bertindak adalah hal yang paling tepat. Mulai dari diri sendiri lalu mengajak orang-orang untuk memaksa diri peduli dengan kesehatan. Dari yang semula leha-leha menjadi lebih tegas. Menjaga kehigienisan tangan, makan makanan sehat, tidur cukup, berolahraga, dan sebagainya.

Satu yang paling sederhana tapi mungkin sulit dilakukan adalah menjaga kehigienisan tangan. Ada ribuan bahkan jutaan kuman dan virus, yang mungkin menempel di tangan. Setiap harinya. Semuanya terkirim ke tangan karena jabat tangan, memegang gagang pitu, tombol lift, stang motor, kemudi mobil, uang, dan lain-lain.

Karena itu, cuci tanganlah secara rutin. CUCI TANGAN. Iya, CUCI TANGAN. Pakai air (mengalir) dan sabun lebih baik, tapi bagi yang memiliki mobilitas tinggi atau sedang di situasi yang tidak mungkin mencari air dan sabun, _hand sanitazer_ adalah kunci. _Hand sanitizer_ yang memiliki kandungan alkohol 60-70%, kalau di bawah itu namanya bukan _hand sanitazer_, tapi Vodka.

Cuci tangan bukan sembarang membasuh tangan. Sejak dahulu WHO menyebarkan panduan bagaimana mencuci tangan dengan benar. Kertas panduan yang biasa menempel di dinding ruang rumah sakit itu, lho. Memberantas kuman hingga ke sela jari dan pucuk kuku. Walaupun pada gambar di bawah tertera 40-60 detik, kita dapat melakukannya minimal 20 detik saja. Cepat, _kok_. Tidak selama antre di ATM BRI. Kecuali kalian habis bermain oli, pegang eek sapi, dan masuk ke ruang isolasi pasien COVID-19 sambil tepuk-tepuk kasurnya tanpa pengaman (ini tak mungkin _sih_).

![Panduan cuci tangan dari WHO](/blog/2020/03/images/cuci-tangan-who-1.png)

Panduan cuci tangan dari WHO (catatan: handrub adalah cuci tangan dengan hand sanitazer)

Kalau di video seperti ini.

https://twitter.com/sintaskata/status/1227512244734312448

Minimal 20 detik lho ya. Agar selubung pelindung virus yang menempel di tangan ditarik oleh sabun dan luruh mengalir bersama air. Biar penghitungan waktu cuci tangan tepat, dokter-dokter di negeri sono menyarankan kita untuk bernyanyi lagu "selamat ulang tahun" dua kali. Yang nadanya seperti video di bawah:

https://www.youtube.com/watch?v=9nvmj0HhSYg

Lagu "Selamat ulang tahun" sangat membosankan, apalagi diulang-ulang, makanya Asumsi--lewat [nawala 5.45](https://ramdziana.wordpress.com/2020/01/24/catch-me-up-dan-5-45-merilis-lebih-banyak-endorfin-dari-tubuh-saya/) yang terbit 7 Maret lalu--menyarankan cuplikan-cuplikan lagu lain berdurasi minimal 20 detik. Termasuk lagu pembuka Crayon Shin-chan, Disco Lazy Time (Nidji), Sewindu (Tulus), Welcome to the Black Parade (My Chemical Romance), Dan (Sheila on 7), Cinta ini Membunuhku (D'Masiv), Surti Tejo (Jamrud), dan Sampai Jadi Debu (Banda Neira).

Mungkin saja lho, lagu-lagu tersebut kurang _relate_ dengan kehidupan kalian. Terasa kurang sip saat dinyanyikan. Sebagai alternatif dan agar tak bosan pula, saya juga mengumpulkan cuplikan lagu-lagu berbahasa Jawa, berdurasi minimal 20 detik.

1. **[Kartonyono Medot Janji (Denny Caknan)](https://www.youtube.com/watch?v=WlmWXoP0C0s)**

_Mbiyen aku jik betah_

_Suwe-suwe wegah_

_Nuruti kekarepanmu sansoyo bubrah_

_Mbiyen wis tak wanti-wanti_

_Ojo ngasi lali_

_Tapi kenyataannya pergi_

Durasi lirik ini sekitar 23 detik, kecuali kalau berhenti pada penggalan _Ojo ngasi lali_, tepat 20 detik sekian.

Buat yang sedang kangen Ngawi dan Tugu Gading Kartonyono lagu ini pas didendangkan saat cuci tangan. Kalau saat itu juga kalian teringat mantan, resapi saja. Meski menyebalkan, doakan yang terbaik buat mereka supaya tidak terkena corona.

2. **[Pamer Bojo (Didi Kempot)](https://www.youtube.com/watch?v=Xo9egupmgoE)**

Terserah mau pakai cendol dawet atau tidak. Toh lirik yang saya ambil sedikit.

_Koyo ngene rasane wong nandang kangen_

_Rino wengi atiku rasane peteng_

_Tansah kelingan kepengin nyawang_

_Sedelo wae uwis emoh tenan_

Durasi 20 detik sudah terpenuhi pada baris ketiga. Kalau mau lebih silakan.

Menikmati patah hati karena ditinggal kekasih dan mengantisipasi emosi yang datang tiba-tiba adalah cara lain untuk "menikmati" wabah corona yang semoga kita tidak kena.

3. **[Sugeng Dalu (Denny Caknan)](https://www.youtube.com/watch?v=W1e1ZZidrAg)**

_Udan tangise ati_

_Saiki wis rodo terang_

_Masio isih kadang kelingan_

_Kowe sing tak sayang-sayang_

Empat baris lirik _Sugeng Dalu_ memiliki durasi 25 detik. Kalau mau 20 detik harus potong di tengah jalan, tanggung. Sudah nikmati saja penggalan lirik dan cuci tanganmu.

4. **[Dalan Liyane (Hendra Kumbara)](https://www.youtube.com/watch?v=Msj8WvqsHYY)**

_Yowis ben tak lakoni nganti sak kuat-kuate ati_

_Pesenku mung siji, sing ngati-ati_

_Isoku mung mendem esuk tekan sonten_

_Mergo sadar diri kulo dudu sinten sinten_

Durasi lirik sampai baris ketiga sudah cukup 20 detik. Tapi kalau kalian ingin meyakinkan diri dengan kebersihan tangan, selesaikan saja sampai baris terakhir. Tiga puluh detik, lumayan.

5. **[Korban Janji (GuyonWaton)](https://www.youtube.com/watch?v=S0kaqFBoGqI)**

_Abot tak trimo nganti ikhlas legowo_

_Sing tak karep kowe ra disiyo-siyo_

_Ben cukup mung aku korban janji manismu_

Mencuci tangan sambil memasrahkan diri karena cinta adalah anugerah. Kalian sudah berkorban hati jangan lagi berkorban kesehatan karena tangan yang sewaktu-waktu kotor terkena segala macam kuman.

6. **[Kangen Neng Nickerie (Didi Kempot Ft. Dory)](https://www.youtube.com/watch?v=SMaLKBGR7Fk)**

_Rembulan sing ngilo ono segoro_

_Padhangono ati kulo_

_Pujaan hatiku ra teko-teko_

_Neng nickerie ndang muliho_

Totalnya 24 detik. Kalau mau cuma 20 detik berhenti saja di baris ketiga. "Nickerie" juga boleh diganti dengan nama kekasihmu yang lama pergi. Suprapti, misalnya.

Aduh itu dulu.

Sekali lagi cuci tangan. Hindari juga memegang muka. Garuk-garuk, mengusap atau mengucek mata, mengupil, membersihkan coklat dari bibir, menghilangkan daun sawi yang menyempil di gigi, dsb sebelum mencuci tangan. Perhatikan juga untuk selalu bersin atau batuk di siku tangan dalam dan menjauh dari orang yang sedang sakit sekitar [4 meter](https://twitter.com/redditindonesia/status/1237877210578432001). Jangan lupa, jaga kesehatan tubuh.

Semoga sama-sama dikuatkan. Wabah COVID-19 semoga segera berhenti. Yok bisa, Yok!

\[Gambar unggulan oleh [JKerner](https://pixabay.com/users/JKerner-15491704/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4906750) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4906750)\]
