---
title: "Meratakan Kurva"
date: "2020-03-20"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "coronavirus-4937226_1920-isolasi-sosial-distance-rmdzn.jpg"
aliases:
  - /post/2020-03-20-meratakan-kurva/
---

Jumat, 20 Maret 2020 pukul 09.56, pasien COVID-19 Indonesia "bertahan" di angka 308 orang dengan total sembuh 15 orang dan total pasien meninggal 25 orang. Ini mungkin masih akan meningkat sore nanti atau Sabtu besok.

Kebijakan … bukan, lebih tepatnya imbauan untuk tetap bertahan di rumah tidak diindahkan. Masih banyak, buuaaaanyaaaak, manusia Indonesia nonpelayan publik dan nonpekerja informal, yang lebih memilih liburan. Bermacet-macet ria di jalan. Berdesak-desakan di pantai. Mendengungkan "liburan corona". Padahal, potensi penyebaran Sars-CoV2 (virus penyebab COVID-19) makin cepat.

Saya bahkan merinding masih melihat satu dua acara _walimatul 'ursy_ alias resepsi nikahan. Keluarga, saudara sepupu, hari Sabtu 21 Maret akan mengadakan akad nikah dan resepsi di Purbalingga. Orang tua saya berangkat Jumat siang ini sedangkan saya dan kakak saya menyusul Sabtu kemudian bersama satu rombongan bus.

Teman saya juga mengadakan akad dan resepsi nikahan, Sabtu 21 Maret. Di Depok dan Ngemplak. Tamu-tamunya pun tentu saja juga teman-teman saya. Keduanya sama. Saling berkerumun. Bersalam-salaman. Mungkin bercipika-cipiki. Lalu prasmanan makan bersama tamu-tamu lain.

Saya khawatir. Seminggu terakhir, saya sulit fokus. Semoga keadaan baik-baik saja. Semua lancar. Pergi sehat. Pulang sehat. Toh saya tak dapat menyalahkan sepenuhnya, mengingat banyak harta dan waktu yang dikorbankan. Sewa tenda. Sewa MUA (_Make Up Artist_). Sewa _sound system_. Dan lain sebagainya. Tapi, tentu, kalau tiba-tiba ada yang menunda resepsi nikahan terlebih dahulu, saya dan orang-orang lain yang khawatir akan mengirim hormat dari hati.

Beda dengan salat Jumat. Di Jogja, masih banyak yang meributkan apakah masjid di kampung mereka lebih baik mengadakan salat Jumat atau tidak. Masjid Kampus (Maskam) UGM yang _magrong-magrong_ itu hari ini resmi meniadakan salat Jumat. Ini sangat wajar, status UGM ditingkatkan ke 'awas' karena satu profesor di sana telah positif COVID-19.

Di luar UGM, sepertinya tak ada masjid yang berinisiatif untuk melakukan hal serupa. Takmir Maskam malah justru menyarankan jemaah untuk salat Jumat di masjid lain. Sama saja, _dong_. Padahal letak UGM di Kecamatan Depok, salah satu kecamatan padat di Sleman. Saat ini ada 9 Orang Dalam Pengawasan (ODP) di kecamatan ini--yang sayangnya kita tidak pernah tahu riwayat perjalanan mereka.

Kecuali, takmir-takmir masjid berani menggebrak seperti [Masjid Jogokariyan](https://news.detik.com/berita-jawa-tengah/d-4944877/takmir-masjid-jogokariyan-bikin-terobosan-demi-jemaah-tetap-datang). Rutin mendesinfeksi masjid. Rutin mencuci sarung dan mukena. Menyediakan sabun dan _hand sanitazer_ di masjid maupun menyediakan untuk jemaahnya di rumah. Meskipun ini tetap riskan.

Seorang ustaz, dijadwalkan sebagai khotib salat Jumat di kampung saya, kemarin malam meminta izin untuk tidak menjadi khotib dan memilih untuk tidak melaksanakan salat Jumat terlebih dahulu. Beliau mengirim dalil panjang yang intinya melarang salat berjamaah di masjid saat musim wabah/pagebluk.

Semalam mungkin beliau bingung. Tidak bisa tidur. Merenung. _Glimpang-glimpung_ di atas kasur.

Pagi ini, beliau mengirim pesan susulan. Akan tetap mengikuti salat Jumat di masjid lain--bukan di masjid kampung saya. Menyertakan dalil yang panjang pula, yang intinya justru di masa wabah/pagebluk harus salat Jumat, meminta ampunan dan pertolongan kepada Allah.

Begitu bimbang, bapak ini.

Padahal. Tak perlu menunggu lokasinya berstatus Kejadian Luar Biasa (KLB) atau zona merah untuk tidak mengadakan salat Jumat. Justru, meniadakan dahulu salat Jumat adalah usaha bersama untuk terhindar dari status tersebut.

Ikhtiar dan tawakal adalah satu kesatuan. Tidak dapat dipisahkan. Kanjeng Nabi saja meminta orang untuk mengikat untanya sebelum memasrahkan kepada Allah. Alih-alih hanya berdoa dan memasrahkan untanya kepada Allah tanpa usaha.

Pada banyak artikel berbahasa Inggris, sering terdengar istilah _flattening the curve_ (meratakan kurva). Semakin tinggi kurva, semakin tinggi pula pasien yang tak terawat di RS dan menularkan ke banyak orang. Bahkan meninggal. Memendekkan kurva lalu meratakannya adalah usaha untuk menjaga kestabilan. Menjaga jumlah pasien agar tak melebihi jumlah ruang yang tersedia di rumah sakit.

Salah satu cara meratakan kurva adalah menjauh dari kerumunan, termasuk salat berjamaah. Kerumunan lain yang tidak perlu. Yang tidak keburu.

Ilustrasi meratakan kurva dapat dilihat di twit ini.

https://twitter.com/SiouxsieW/status/1237275231783284736

Meratakan kurva juga berarti menjaga agar tenaga-tenaga kesehatan tidak terlalu lelah. Mereka garda depan. Jika mereka lelah, tertular, lalu sakit. Siapa yang mampu mengurus pasien? Kita yang ongkang-ongkang, yang kerap cuek dengan kesehatan sendiri? Kita yang egois, yang memakai masker tetapi saat batuk dilepas begitu saja? Kita yang tak peduli, yang menggaungkan doa bersama di masa krisis, tanpa mau usaha fisik sekalipun?

Hormat untuk tenaga kesehatan di luar sana. Menjura. Menjura. Menjura. Berkali-kali.

\[Gambar unggulan oleh [Selling of my photos with StockAgencies is not permitted](https://pixabay.com/users/congerdesign-509903/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4937226) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4937226)\]
