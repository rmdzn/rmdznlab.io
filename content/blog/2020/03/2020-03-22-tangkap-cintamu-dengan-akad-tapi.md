---
title: "Tangkap Cintamu dengan Akad, Tapi ..."
date: "2020-03-22"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "people-2595862_1280-nikahan-gandengan-rmdzn.jpg"
aliases:
  - /post/2020-03-22-tangkap-cintamu-dengan-akad-tapi/
---

> Saya beri cerita lagi. Masih seputar korona. Semoga tidak bosan, ya!

Kalau dapat mencintai seseorang, sekarang. Akan saya lakukan segera. Cinta adalah energi positif. Mampu mengeluarkan endorfin. Menyenangkan otak. Menurunkan stres. Meningkatkan imun. Situasi yang bagus di tengah wabah COVID-19 yang menyebar tanpa henti.

Maksudnya, bukan demi meningkatkan imun di tengah wabah, kalian mencintai seseorang. Begini. Saya menyukai seseorang, sebelum wabah ini ada. Dan saya tetap akan melakukannya. Soal apakah orang yang saya sukai tahu atau tidak, itu persoalan nanti. Maaf, curhat.

Cinta yang terpupuk di kalangan muda-mudi mendorong mereka untuk segera mengucapkan sumpah janji; antar pasangan maupun antar seorang laki dengan bapak sang perempuan.

Tak ada yang salah. Yang salah adalah berkumpul dan merayakan pesta setelah melakukan sumpah janji (baca: akad/ijab qabul) di tengah situasi abnormal seperti ini.

Saya gugup sekali saat dua acara resepsi pernikahan diadakan kemarin, Sabtu 21 Maret. Yang satu masih saudara sepupu, yang satu kawan dekat saya. Namun di balik kegugupan, saya memiliki cerita menarik yang dapat saya beri. Cerita dari resepsi nikahan saudara sepupu.

Saya dan kakak saya berangkat dari rumah pukul 02.30 pagi. Dunia masih senyap-senyapnya. Saya harus mandi, berbenah, lalu menenteng tas berisi baju batik, masker kain, penutup kepala (kupluk), alas salat, dan _hand sanitazer_ mungil. Salah satu kru bus, seorang bapak, berkaos tangan putih memegang botol dengan tutup _pump_. Kami dan lainnya harus membersihkan tangan dengan _hand sanitazer_ tersebut sebelum masuk bus.

Ini pertanda baik. Prosedur Operasional Standar (POS) yang memuaskan dari kru bus di tengah pandemi seperti ini. Tapi sayang, saat bus berhenti dan membiarkan penumpangnya untuk salat Subuh di Kebumen, POS yang menyenangkan itu tidak dilakukan lagi--bahkan sampai kami kembali ke Jogja.

Jogja sampai Purbalingga sekitar 4 jam. Kami tiba sekitar pukul 07.00 lebih. Padahal acara resepsi yang ingin kami datangi baru mulai pukul 10.00. Kami harus pintar-pintar memanfaatkan waktu sekitar 3 jam yang tersisa; buang hajat, makan cemilan, sarapan nasi kotak, berkeliling ruangan resepsi, berbincang kesana kemari bersama penumpang lain.

Saat kakak saya memeriksa tenda, pintu masuk sebelum ke gedung utama, ada pesan penting menempel di dua tiangnya. Pengelola pernikahan akan menyemprotkan HOCI (Hypochlorous Acid Water) kepada setiap tamu undangan dan menyediakan _hand sanitazer_ untuk dipakai oleh para tamu.

![Kertas pengumuman di pintu masuk gedung pernikahan](/blog/2020/03/images/photo_2020-03-22_00-00-43.jpg)

Kertas pengumuman di pintu masuk

HOCI dan _hand sanitazer_ dipegang oleh dua perempuan di sebelah kiri dan sebelah kanan. Posisi yang biasanya dipakai oleh penjaga buku tamu. Saya ingat-ingat, sepertinya memang tidak ada buku tamu di sini, atau memang ada tapi dalam bentuk digital (ada laptop di samping kanan di video bawah).

Yang disayangkan, HOCI sebagai desinfektan hanya disemprot sekenanya. Saya malah hanya disemprot tubuh bagian kiri. Tamu lain mungkin hanya tubuh bagian kanan. Selain karena tidak mungkin menyemprot tubuh tamu sepenuhnya yang sangat banyak, botol penyemprot juga kecil. Tidak segampang itu menjangkau sebidang besar tubuh. Dalam kasus ini, bilik desinfektan lebih efektif.

https://youtu.be/c2oVhWRHpoM

Saat saya pertama kali masuk gedung utama. Dua perempuan memakai masker bedah, berusia tiga sampai kepala empat, datang. Mengeluarkan sebotol cairan lalu disemprotkan ke tangan mereka masing-masing.

Ternyata beliau berdua dari Dinas Kesehatan. Terlihat dari kartu nama yang dikalungkan. Dua ibu ini bergegas memeriksa setiap tamu yang masuk menggunakan pistol pengecek suhu tubuh. Kecil, berwarna putih. Di sebelahnya, berjejer dengan jarak sekitar 1,5 meter, para among tamu menelangkupkan tangannya, menyambut tamu-tamu yang lewat di sepanjang karpet.

![Proses pengecekan suhu tubuh](/blog/2020/03/images/photo_2020-03-22_00-08-25.jpg)

Proses pengecekan suhu tubuh

Saya mencoba melewati beberapa pasangan among tamu. Gestur tangan saya ala salam namaste/wilujeng, salam rohis, apapun itu. Sebagian among tamu sedikit gugup lalu memegang tangan saya layaknya berjabat secara normal. Kebiasaan yang mungkin sulit diubah. Maklum sekali.

![Among tamu menggunakan salam namaste/wilujeng](/blog/2020/03/images/photo_2020-03-22_00-09-41.jpg)

Among tamu menggunakan salam namaste/wilujeng

Di dalam gedung pernikahan, tamu saling berdesakan. Duduk maupun berdiri. Yang mengambil segelas dawet lalu duduk menyantapnya. Yang mengambil sepiring siomay dan nasi lalu memakannya. Di sana saya sama sekali tidak melihat skuad pemegang _hand sanitazer_ berseliweran atau botol serupa yang ditempel di dinding atau ditaruh di meja.

Para tamu melakukan kegiatan seperti acara nikahan pada umumnya yang, jujur, sungguh riskan.

Pengelola acara pernikahan boleh saja menambahkan fitur-fitur untuk menjaga kesterilan tamu di depan pintu. Tapi budaya berdesak-desakan tamu resepsi pernikahan tak mudah dielakkan.

Mencintai pasanganmu boleh. Menyayanginya setulus hati, apalagi. Tapi membahayakan orang-orang bukanlah hal baik. Akan banyak lansia rentan yang datang dan pergi. Dan mungkin saja akan ada anak muda berimun tinggi yang membawa virus tanpa disadari.

Usul saya, berulang-ulang. Tangkap cintamu dengan akad nikah (ijab qabul). Kasihi dia sesayang-sayangnya. Tapi tahan dulu untuk mengundang banyak orang ke KUA. Tunda dulu resepsi nikahan dan _ngundhuh mantu_. Tidak lucu kalau--amit-amit--pestamu menjadi klaster baru.

Samawa buat pasangan. Semoga wabah segera berhenti.

\[Gambar unggulan oleh [StockSnap](https://pixabay.com/users/StockSnap-894430/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2595862) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2595862)\]
