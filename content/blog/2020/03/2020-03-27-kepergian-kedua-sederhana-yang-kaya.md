---
title: "Kepergian Kedua, Sederhana yang Kaya"
date: "2020-03-27"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "photo_2020-03-27_09-55-37.jpg"
aliases:
  - /post/2020-03-27-kepergian-kedua-sederhana-yang-kaya/
---

> Awas, ada bocoran (spoiler).

Beberapa pekan lalu, saya beruntung banget, pas sedang pengin-penginnya baca buku karya Amanatia Junda, toko buku daring yang ada di Yogyakarta menawarkan paket buku Amanatia Junda. Dapat diskon lagi. Kalau mau intip akun IG-nya ini: [@paperplanebookstore](https://www.instagram.com/paperplanebookstore/).

Sampai saat tulisan ini terbit, Amanatia telah menelurkan dua buku. Terbitan Buku Mojok. Yang pertama [_Waktu untuk Tidak Menikah_](/blog/2020/02/2020-02-14-menyimak-isu-isu-perempuan-lewat-cerpen/) dan yang kedua _Kepergian Kedua_. _Waktu untuk Tidak Menikah (WUTM)_ sudah habis terbaca dan sudah saya ulas di artikel [_Menyimak Isu-isu Perempuan Lewat Cerpen; Haru dan Kesal_](/blog/2020/02/2020-02-14-menyimak-isu-isu-perempuan-lewat-cerpen/), kini giliran _Kepergian Kedua_ yang perlu dibabat habis.

Buku _Kepergian Kedua (KK)_ terbit Januari 2020. Iya, masih gres. Hasil karya Amanatia sejak Agustus 2016 (naskah awal) lalu masuk penyuntingan di penerbit dua bulan. Impresi awal saat memegang buku ini adalah "_keren ni buku_". Kovernya tebal, semacam berbahan karton dengan tekstur kasar (kalau di-_zoom_ terlihat _prentul-prentul_). Ada pembatas buku yang menyajikan bahan yang sama.

Kalau di _Waktu untuk Tidak Menikah_ saya mendapatkan bonus kartu pos, di KK saya memperoleh surat terima kasih berwarna coklat dari sang penulis. Amanatia yang mampir di warung pecel lele dan menganalogikannya dengan buku KK.

![Surat dari Amanatia Junda](/blog/2020/03/images/photo_2020-03-27_09-55-37.jpg)

Daftar isinya lucu, karena setiap bab hanya berjudul pendek; Satu, Dua, Tiga, sampai Sembilan, penulisan daftar isi yang lurus tengah menjadikannya seperti garis lurus ke bawah.

KK adalah novela. Cerita yang lebih panjang dari cerpen tetapi lebih pendek dari novel. Membaca beberapa paragraf awal langsung membuat saya berpikir, "_eh, ini Amanatia banget_". Khas.

KK menceritakan seorang Birrul Walidain, cucu laki-laki satu-satunya pasangan mendiang Yu Jariyah dan Wak Jauhari. Sebagai manusia yang disemati nama yang berarti "berbakti kepada orang tua", Irul--nama panggilannya--menjadi pemikir yang diandalkan oleh ibu, pakde, maupun paklik untuk segala masalah yang terjadi di keluarga besar. Terpaksa maupun sukarela. Sebetulnya bukan karena namanya, tapi karena gender Irul dan pengalamannya melanglang sampai Jerman yang membuat keluarga besar percaya padanya.

![Kepergian Kedua](/blog/2020/03/images/photo_2020-03-27_10-38-31.jpg)

Cerita membawa isu-isu terkini yang menarik. Termasuk dijualnya buku-buku bajakan di Gayut, kampung halaman Irul, karena buku-buku asli yang tak mungkin laku dijual di sana. Kisah ini mengingatkan saya pada polemik buku bajakan yang menjamur di Jogja. Sebagai antitesis dari argumen soal pelanggaran Hak Cipta, ada argumen lain yang lebih pro dengan pembajakan jika itu untuk membuka cakrawala orang-orang pinggiran. Buku itu mahal, tuturnya. Di Gayut mungkin juga terjadi seperti itu.

Isu lain yang tak asing adalah sentimen terhadap imigran di Eropa. Khususnya di Jerman, negara yang ditinggali Irul. Hingga membuat kekhawatiran lain dengan kehadiran _lonewolf_ yang menyerang secara acak. Di luar itu--tapi masih di kultur negara yang sama--cerita pengasuhan anak (_parenting_) yang sangat menarik dituliskan di sini. _Uwu_.

Cerita mengalir. Sederhana. Tak berat. Menyenangkan. Apalagi dengan adanya Irul yang menggambarkan silsilah keluarga demi memudahkan pembaca membolak-balik halaman kala lupa siapa anak Pakdhe Kar atau Paklik Dar--daripada membaca penjelasan deskriptif yang membosankan.

Gambar silsilah keluarga ini saya yakin adalah tulisan tangan (hal. 27), dalam arti sebenarnya. Tulisan tangan yang didigitalisasi. Jadi bukan tulisan hasil ketikan. Antar huruf 'a' yang berbeda, begitu juga dengan huruf 'y' dan lainnya. Gambar silsilah benar-benar membantu, karena alur cerita juga kerap menyinggung siapa anak siapa, pakdhe mana yang pertama, siapa adiknya, dsb.

Konflik atau masalah di Kepergian Kedua sangat membumi dan manusiawi. Keributan keluarga dan pengiringnya. Irul yang menjadi penengah masalah menjadi pemicu stres saya. Berengsek betul. Membaca novela ini sambil berada di masalah keluarga yang itu-itu saja adalah ujian mental.

Saya penasaran dengan perasaan Amanatia saat menulis ini. Di acara yang diselenggarakan oleh Togamas Kotabaru, 5 Maret 2020 lalu, saya bertanya langsung kepada sang penulis. Apakah ia terpantik dengan cerita yang ditulis sendiri? Hal-hal yang terolah di kepala, tuturnya, jika itu sastrawi akan langsung ditulis. Kalau tulisan tersebut _relate_ dengan pembacanya yaa bagus. Tapi, jadikan ia sumber tawa. _Yaa, mari ketawa bareng!_

![Foto bareng Amanatia Junda di Togamas Kotabaru](/blog/2020/03/images/photo_2020-03-27_10-23-01.jpg)

Foto bareng Amanatia Junda di Togamas Kotabaru

Banyak sekali pesan dalam buku KK. Tak jarang, saya tersenyum sendiri. Salah satunya saat Irul mampu membantah saat ibunya berusaha menyetir kehidupannya. _Anaknya sudah besar, sudah dapat menentukan jalan hidupnya sendiri_.

Irul yang tangguh. Kehidupannya yang lebih "mengutamakan" orang lain mampu membuat hati terenyuh. Terutama saat ibunya mengutarakan "_Kamu sudah pernah mengasuh bayi bule, nyebokin orang tua bule, dan mau kerja keras untuk anak yang sebenarnya bukan tanggung jawabmu. Ibuk doakan panjang rezeki dan umur ya, Rul_."

Ada lagi. Irul yang memperlakukan sang adik sepupu sebagai manusia. Bukan hanya perempuan kecil yang disetir oleh keluarga besarnya.

_Aaah, melted …_

Novela yang sederhana tetapi memiliki kisah yang padat. Sebagai penggemar cerita pendek dan cerita panjang (novel), KK begitu kaya. Alur yang tidak datar dan bagi saya tidak mudah ditebak. Cerita yang manusiawi. Tak khayali.

Namun, ada satu bagian kisah yang tidak begitu saya sukai: saat Paklik Dar menghilang.

_Udah gitu …_ _Minggir, saya mau pamer dulu._

![Buku Kepergian Kedua](/blog/2020/03/images/photo_2020-03-27_10-55-07.jpg)

* * *

**Judul**: Kepergian Kedua  
**Penulis**: Amanatia Junda  
**Penerbit**: Buku Mojok  
**Tebal**: vi + 108
