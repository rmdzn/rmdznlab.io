---
title: "Korona dan Kewajaran"
date: "2020-04-03"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "balloon-381334_1280-chat-balon-rmdzn.png"
aliases:
  - /post/2020-04-03-korona-dan-kewajaran/
---

Mulai satu bulan terakhir, grup WhatsApp (GWA) Karang Taruna di kampung saya sibuk membahas perkembangan korona. Jumlah Orang Dalam Pemantauan (ODP) dan pasien yang terus bertambah khususnya di daerah Jogja serta bagaimana pencegahannya, sekecil apapun itu.

Bahasan terus-terusan muncul hingga membuat satu atau mungkin dua tiga orang yang risih. Ingin damai. Ingin keluar dari topik-topik obrolan yang membosankan. Bikin enek dan bikin stres.

Di GWA bapak-bapak ibu-ibu terjadi hal yang sama meski tak terus-terusan dan tak terkurasi sebaik grup Karang Taruna. Sebagian menolak informasi yang terunggah dengan alasan yang mirip; takut jiwanya terpapar korona. Takut kepikiran. Takut begini dan begitu.

Tidak masalah kalau ketidaknyamanan disimpan sendiri, tapi kalau disampaikan di forum terbuka, yaa, keliru.

"_Jangan bahas korona terus, dong!_" Begitu contoh sederhananya.

Kondisi kali ini adalah kondisi luar biasa. Wabah yang mengintai dari pagi sampai malam. Dari tempat-tempat publik, kota sampai desa.

Saling berbagi info dan kabar adalah keniscayaan. Di musim wabah, berbagi apapun lewat komunitas merupakan wujud kewaspadaan. _Ini itu. Hati-hati, yaa._ Tindakan yang sangat wajar.

Justru saat ada yang enek dengan kabar korona dan menyampaikannya ke komunitas, malah bahaya. Di tengah sebagian masyarakat yang masih abai--anak-anak muda yang masih nongkrong di kafe/burjo, wira-wiri berkendara di jalan tanpa mengenakan masker, bapak-bapak yang kongkow di depan tenda _lockdown_ kampung, ibu-ibu yang belanja sambil berkerumun--makin bahaya.

Berdiskusi soal wabah korona sebentar maupun sesering mungkin adalah salah satu wujud untuk bertahan hidup. Ikhtiar itu sendiri. Insting manusia bergerak di situ. Jangan dipatahkan. Ini bukan tentang topik-topik politis atau konspirasi, lho. Itu beda.

Menyampaikan topik penting merupakan cara untuk melindungi komunitas yang lebih besar. Keluarga sendiri yang sudah peduli isu sebegitu rupa akan terancam jika komunitas di atasnya abai. Sebaliknya, jika komunitas yang lebih besar peduli dengan isu, keluarga sendiri akan lebih aman, terkendali.

Kalau kita sering terpapar dengan info-info soal korona hingga pada titik menganggu kesehatan mental seperti cemas berlebihan, berhentilah. Taruh ponselmu. Lakukan kegiatan luring yang bermanfaat untuk dirimu sendiri. Membaca buku. Berolahraga di rumah. Mencuci piring. Merapikan tempat tidur dan rak.

Kalau masih ingin memegang ponsel. Baca atau tonton saja konten-konten nonkorona. Baca buku di iPusnas atau menonton video koplo, _kek_. Koveran lagu Nadia & Yoseph, _kek_. Video sepak bola atau badminton, _kek_, atau lainnya.

\[Gambar unggulan oleh [Thanks for your Like • donations welcome](https://pixabay.com/users/stux-12364/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=381334) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=381334)\]
