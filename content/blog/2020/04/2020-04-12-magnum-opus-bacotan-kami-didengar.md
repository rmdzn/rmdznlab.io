---
title: "Magnum Opus, Bacotan Kami Didengar!"
date: "2020-04-12"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "mailbox-2607174_1920-kotak-surat-rmdzn.jpg"
aliases:
  - /post/2020-04-12-magnum-opus-bacotan-kami-didengar/
---

_Fiuh_, satu minggu lebih, mental terkuras. Lelah, bener. Yang secara tidak langsung mempengaruhi badan pegel. Ini bukan psikosomatik, tapi memang pegel gara-gara mental terkeruk berulang-ulang.

Selama satu bulan, Karang Taruna kampung selalu mendiskusikan pencegahan Covid-19 di kampung yang lamban. Ada yang menyuarakan langsung lewat grup WhatsApp RT/RW tapi selalu saja disepelekan. Sebagian bapak-bapak memotong diskusi dengan topik lain yang tak fundamental. Bakar-bakar daging. Cari belalang. Di semua RT, pasti ada bapak-bapak seperti itu. Padahal ada satu rumah sakit rujukan yang sudah merawat beberapa PDP di kampung ini. Bagaimana tidak pusing kepala manusia kalau umatnya seperti ini?

Puncaknya, Selasa pagi tanggal 7 April 2020. Saya meminta izin ketua Karang Taruna untuk menyusun surat terbuka. "Iya, tidak apa-apa," katanya dalam bahasa Jawa. Berdasarkan diskusi berulang kali lewat grup Karang Taruna dan diskusi secara personal dengan sang ketua, saya langsung menyusun kata demi kata. Kalimat demi kalimat.

Di fase ini, jari saya gatal semua. Sempat terngiang untuk melabrak dengan bahasa tegas. Berisi sarkasme dengan kata "vulgar". Sebelum mengeksekusi, untungnya otak saya langsung menyuruh jari untuk menahan gelisahnya. Saya hanya meninggalkan sedikit saja sindiran dilanjutkan rekomendasi dan ajakan secara normal. Lega rasanya.

Naskah awal jadi menjelang sore hari, pukul 14.41. Langsung saja saya kirimkan kepada ketua. "Bagian itu diubah," katanya. "Itu terlalu membatasi." Revisi ini merujuk pada pernyataan yang ditakutkan terlalu otoriter dan mengungkung warga.

Baik, sikat langsung.

Hanya itu. Naskah diterima dan dicetak. Seperti rencana awal saya yang diterima oleh ketua, surat akan dibubuhi tanda tangan anggota Karang Taruna sebanyak-banyaknya pada lembar kertas kosong di belakang dengan cap organisasi. Seperti petisi.

Sehabis Magrib, surat tercetak sempurna. Saya mulai berkeliling ke rumah anggota Karang Taruna. Mengabarinya lewat aplikasi perpesanan lebih dahulu, lalu memintanya untuk membaca surat dan memutuskan untuk ikut tanda tangan atau tidak.

"Apa sih, Mas?" Tanya salah satu anggota Karang taruna.

"Itu, lho, seperti di grup (WhatsApp). Keresahan yang selama ini kita pendam, kita tuang di sini."

Malam itu saya mendapatkan 8 sampai 10 tanda tangan. Saya mengirim pesan WA. Dan menyuruh mereka membaca di tempat lalu tanda tangan. Polanya seperti itu. Berbeda dengan saat saya berkeliling pada hari Rabu, 8 April, siang. Anggota Karang Taruna akan saya kirimkan surat digital. Jika mereka berminat, saya akan mendatangi rumahnya dan menyodorkan selembar kertas untuk tanda tangan.

Surat ini kami rahasiakan rapat-rapat. Setiap anggota Karang Taruna selalu saya beri pesan untuk memendam informasi seputar surat, sekecil apapun itu. Pokoknya jangan sampai orang lain selain anggota Karang Taruna tahu, termasuk orang tuanya sendiri.

Mengapa harus rahasia? Kami ingin memberikan kejutan. Supaya secara tiba-tiba ada surat menyebar dengan label "SURAT TERBUKA". Cara ini memaksa saya memberanikan diri untuk tidak menawarkan tanda tangan dukungan kepada anggota Karang Taruna yang juga anak ketua RT dan ketua RW. Bahkan, saya menyiapkan mental jika sewaktu-waktu muncul drama karena keputusan tak tertulis ini. Dan itu terjadi. Meskipun tak seseram yang dibayangkan.

Dua hari ini sepertinya cukup. Rabu, malam hari. Saya mendapatkan tanda tangan terakhir. Tanda tangan kedua puluh. Surat langsung saya fotokopi secukupnya. Target pembagian adalah untuk pengurus RT/RW, tokoh masyarakat, dan beberapa warga. Totalnya 24.

Malam itu ujian mental datang lagi. Capek setengah mati. Apalagi karena satu orang anggota Karang Taruna senior mengirim pesan untuk saya.

_Sudah konsultasi dengan pembina Karang Taruna belum? Wajib, Bro. Nanti kita dikira tidak menghargai mereka._

Sial. Masih saja menuntut urusan birokrasi. Keresahan kok dikonsultasikan. Kalau dirunut kembali, akan ada bias jika surat ini saya sampaikan dahulu kepada dua pembina Karang Taruna yang notabene adalah pengurus RT/RW--entitas yang ingin kami labrak saat ini.

_Maaf, Mas. Biarkan ini menjadi suara alami Karang Taruna_, jawab saya.

Malam itu benar-benar melelahkan. Isi hati seperti _roller coaster_. Kadang berpikir, apakah surat ini penting? Apakah ada yang memperhatikan? Kadang tersemangati, kadang _ngedrop_ lagi, terus-terusan. Saya sampaikan ke grup Karang Taruna bahwa saya memang secapek itu. Ini adalah sisa-sisa tenaga dan harapan saya. Semoga bapak-bapak mendengar. Sekaligus menawarkan kepada anggota Karang Taruna yang waktunya luang untuk saya ajak menyebarkan surat esok pagi.

Kamis, 9 April, sekitar pukul 09.20. Saya dan satu orang teman Karang Taruna mulai berkeliling menyebarkan surat. Yang pertama mengunjungi kediaman ketua RW, menyerahkan surat asli (bukan salinan). Selanjutnya kami terus mengunjungi rumah per rumah sesuai catatan hingga selesai sebelum pukul 11.00.

Banyak sekali hal yang kami simpan di memori; utamanya warga yang terkesan tidak menyeriusi surat. Setelah menyambangi seorang pengurus RT sekaligus anggota Karang Taruna, ia adalah pengurus yang sama-sama merasakan keresahan dan menjadi penandatangan malam sebelumnya, mengusulkan untuk memperbanyak surat dan membagikannya kepada semua warga; penetap dan warga kos/kontrakan/penginapan. Kita perlu menegaskan keresahan kepada mereka dan menekan pengurus RT/RW secara tidak langsung.

YOS!!

Kami telah menghitung berapa lagi kertas yang harus kami fotokopi. Hampir di angka 80. Oke, aman. Saya yang menyalin kemudian membaginya dua untuk disampaikan ke warga masing-masing RT. Kami hampir berangkat saat notifikasi muncul di ponsel saya.

Undangan rapat darurat dari Ibu RW. Mengundang pejabat penting RW dan tiga perwakilan Karang Taruna, termasuk saya. Malam itu pukul 19.30.

_Okey, this is intense._

Kami berhasil.

Sore itu kami tidak jadi menyebarkan lebih banyak surat ke warga. Kami, dari Karang Taruna, lebih banyak berdiskusi. Ada 5 orang di gardu yang berkumpul, santai, membahas apa kiranya poin-poin penting penjelas surat kami yang akan kami sampaikan nanti. Bahkan kami menyiapkan bahan jika dicecar.

Ternyata, malam itu, rapat darurat tak semengkhawatirkan pikiran kami. Kami berkumpul menyetujui pokok-pokok aturan baku pencegahan Covid-19 di tengah warga, lebih tegas mendata tamu/pendatang yang keluar masuk kampung, serta mengusulkan bantuan ekonomi kepada yang membutuhkan jika situasi ini masih akan terjadi.

Ini adalah _magnum opus_ angkatan Karang Taruna ini dalam sudut pandang "bersuara". Menyusun surat terbuka untuk mendesak petinggi kampung agar gegas bertindak. Doakan semoga sikap ini konsisten, ya!

Berikut surat terbuka (versi digital yang belum ditandatangani) yang kami kirimkan kepada warga.

[![Surat terbuka versi digital](/blog/2020/04/images/surat-terbuka-kompag-judul-rmdzn.png)](https://my.pcloud.com/publink/show?code=XZz3HPkZ9ndJSyKpYSkIFQE3QSsXAmtv7FBy)

Cuplikan surat terbuka (klik untuk membacanya)

Berikut aturan baku kampung (kami terinspirasi dari kampung Kauman yang telah melakukannya lebih dahulu).

![Surat edaran pencegahan Covid-19](https://ramdziana.files.wordpress.com/2020/04/surat-edaran-gondangan-disensor-rmdzn.jpeg?w=720)

Aturan baku hasil rapat darurat diwujudkan dalam surat edaran

\[Gambar unggulan oleh [CrafCraf](https://pixabay.com/users/CrafCraf-3969510/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2607174) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2607174)\]
