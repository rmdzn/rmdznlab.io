---
title: "Setop Debat, Masjid Harus Fokus Bantu Warga di Sekitarnya"
date: "2020-04-18"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "photo_2020-04-18_17-11-52.jpg"
aliases:
  - /post/2020-04-18-setop-debat-masjid-harus-fokus-bantu-warga-di-sekitarnya/
---

Setiap pekan. Setiap hampir Jumat. Selalu saja topik seputar salat Jumat dilempar ke publik, minimal di tingkat kampung. Komunitas terpecah antara yang setuju untuk tetap mengadakan salat Jumat, lainnya tidak setuju karena itu sama dengan membahayakan umat.

Sepertinya poin tersebut dapat menjadi pendukung pernyataan yang sedang sering didengungkan orang bahwa sifat asli manusia akan terlihat saat pandemi. Apakah bersikap peduli dengan sesama. Apakah sebaliknya, tidak peduli dengan situasi, yang ia pedulikan adalah ego diri. Atau justru sampai pada tingkat ekofasis--memaknai manusia sebagai virus itu sendiri dan Covid-19 adalah imun bumi untuk menyembuhkan diri--sehingga tidak peka dengan ratusan orang yang sedang sekarat bahkan mangkat di luar sana.

Begini maksud saya.

Orang-orang terlalu sibuk membahas kapan salat Jumat akan dibuka. Ada yang ingin Jumatan segera diselenggarakan. Hampir selalu dengan argumen, "pasar buka, toko buka, masa masjid tidak boleh buka?" Ada yang ingin saat wabah ini surut dan hilang, pokokknya saat sudah aman.

Mirisnya, keributan ini bukan disampaikan oleh warga biasa, tapi justru oleh takmir masjid itu sendiri.

Pertanyaan memaksa untuk membuka masjid di kala wabah mudah ditangkal dengan logika sederhana. Pasar dan toko adalah ruang untuk bertahan hidup. Untuk membeli bahan makanan dan pernak-perniknya. Kalau memang bisa beli Indomie di rumah kalian sendiri, yaa silakan. Tapi, kan, tidak bisa.

Kalau pasar dan toko langgananmu menyediakan layanan antar, itu adalah privilisemu. Silakan manfaatkan. Tapi ingat, tidak semua orang punya nasib yang sama. Punya ponsel canggih untuk pesan ini itu saja belum tentu.

Sebaliknya. Agama saja mengizinkan ibadah di rumah, termasuk meniadakan salat Jumat dan menggantinya dengan salat Zuhur jika terjadi hal menggentingkan, salah satunya wabah. Organisasi Islam di negeri ini setuju akan hal itu; MUI, Muhammadiyah, dan NU. Ustaz-ustaz kondang juga mengiyakan.

Mau apa lagi?

Takut dunia kiamat karena tidak ada yang salat di masjid?

Wabah berpotensi menjangkiti jemaah di masjid. Lalu jemaah itu akan pulang ke rumahnya masing-masing kemudian menulari keluarganya; kakek, nenek, suami, istri, anak-anak, dan anak tetangga yang sedang lucu-lucunya. Rumah sakit kolaps. Semua wafat. Lalu, siapa yang akan beribadah di masjid?

Menunda kerumunan di manapun, termasuk di masjid, adalah cara untuk mempercepat penanganan wabah. Untuk kemaslahatan jangka panjang. Agar wabah segera tertangani dan mempercepat hadirnya kehidupan yang normal seperti sediakala. Tolong, berpikir rasional lebih dahulu.

Kalau masih ingin mengadakan salat Jumat, pastikan para takmir bekerja seperti [takmir Masjid Jogokaryan](https://news.detik.com/berita-jawa-tengah/d-4944877/takmir-masjid-jogokariyan-bikin-terobosan-demi-jemaah-tetap-datang). Meskipun potensi penyebaran virus masih ada, ikhtiar masjid tersebut sudah sangat luar biasa.

Namun, pasti sulit, kan? Masjid Jogokaryan adalah panutan prima bagi masjid manapun. Jika masih belum dapat mengikuti gerak Masjid Jogokaryan dan daripada masih bingung ingin mengadakan salat Jumat atau tidak serta malah masih memaksa di situasi yang berbahaya, takmir-takmir masjid sudah seharusnya mulai beralih fokus: meringankan beban hidup masyarakat yang kesulitan di lingkungannya.

Seperti di masjid [di Jetisharjo Cokrodiningratan, Yogyakarta](https://jogja.suara.com/read/2020/04/01/184314/krisis-corona-komunitas-masjid-di-sleman-bagi-bagi-sembako-gratis). Masjid membagikan sembako kepada warga di tiga RW. Ada juga [Masjid Jami Banjarmasin](https://www.sonora.id/read/422106835/dapur-umum-masjid-jami-jadi-sarana-berbagi-di-tengah-wabah-virus-corona?page=all) yang membangun dapur umum bekerja sama dengan TNI dan Polri. [Masjid Salman ITB juga](https://www.liputan6.com/lifestyle/read/4229769/masa-pandemi-kantin-masjid-salman-itb-disulap-jadi-lumbung-sembako-bagi-keluarga-rentan).

Tunda dahulu kalau ternyata takmir masjid sedang ingin merenovasi masjid. Alihkan dana untuk bergotong royong bersama warga membantu warga lain yang kurang mampu, baik dalam bentuk dapur umum atau lumbung pangan (berbagi sembako).

Saya jadi teringat dengan perkataan mbak Kalis Mardiasih, ia miris melihat masih banyaknya spanduk atau baliho kekurangan dana renovasi sebesar sekian ratus juta. Di waktu yang sama, nasib warga sekitar tidak ada yang mempedulikan. Masjid hanya fokus pada uang yang terkumpul untuk renovasi alih-alih masalah nyata di depan mata.

Padahal kalau masjid mau turun langsung membantu warga lingkungannya, jemaah maupun yang bukan, akan lebih banyak orang yang ingin menyumbang uang renovasi. Di mata masyarakat, masjid jadi lebih dipercaya, karena uang infak tak hanya bercokol di rekening tetapi juga tersalurkan dengan cara benar. Warga senang, takmir masjid senang.

Sudahi debat tentang ibadah ritual. Kini masjid harus mulai turun langsung. Jangan lupa, membantu sesama manusia itu juga ibadah, lho. _Mbok kira …_
