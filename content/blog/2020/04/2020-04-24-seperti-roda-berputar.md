---
title: "Seperti Roda Berputar"
date: "2020-04-24"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "photo_2020-04-23_16-03-49.jpg"
aliases:
  - /post/2020-04-24-seperti-roda-berputar/
---

Bulan lalu saat saya akan pergi ke luar kota. Dengan bus. Bersama rombongan tetangga. Saya berniat membawa buku bacaan untuk menemani perjalanan empat sampai lima jam ke depan.

Buku kecil bersampul warna muda yang redup. Judulnya _Seperti Roda Berputar_, karya Alm. Rusdi Mathari, seorang wartawan senior yang telah melanglang di dunia media. _Seperti Roda Berputar_ yang selanjutnya akan saya sebut sebagai SRB adalah kumpulan catatan Cak Rusdi, begitu panggilan akrabnya, saat dirawat di rumah sakit.

Buku ini diterbitkan oleh Buku Mojok dan dirilis pada tahun yang sama saat Cak Rusdi meninggal. Tahun 2018. Kala itu, saya tak begitu tahu siapa itu Rusdi Mathari. Saya hanya tahu sepintas, sekadar wartawan. Namun saat membaca beberapa artikel mengenang meninggalnya sang wartawan, saya baru tahu, ia adalah wartawan dan pimpinan media yang mantap suraya.

![Buku Seperti Roda Berputar](/blog/2020/04/images/photo_2020-04-23_16-09-27.jpg)

Sampul buku Seperti Roda Berputar

Membaca pengantar buku ini bikin saya terenyuh. Terlihat pada ucapan terima kasih Voja Alfatih--anak tunggal Cak Rusdi--kepada rekan bapaknya dan tulisan dari penerbit yang menceritakan mengapa SRB ini muncul. Selama terbaring di kasur rumah sakit, Cak Rusdi ternyata masih rutin menulis. Bukan dengan buku atau laptop sambil duduk tegak, tetapi punggung bersender dengan ponsel di tangan kiri dan jempol kanan mengetik. Menulis novel dan menulis untuk Mojok.

Novelnya tak pernah selesai. Sebagai ganti, catatan-catatan Cak Rusdi diterbitkan dalam buku ini.

Satu yang menarik di _Seperti Roda Berputar_ adalah sisipan percakapan WhatsApp antara Cak Rusdi, Nody, dan Prima Sulistya (pimpinan redaksi Mojok). Tapi yang lebih sering dengan yang ketiga. Obrolan tentang tagih menagih dan revisi tulisan untuk kemudian diterbitkan ke laman Mojok. Ada sensasi kagum sekaligus khawatir, Cak Rusdi yang sesakit itu saja masih produktif menulis.

> Saya harus mengetik,  
> harus bertahan dan hidup.  
> Menulis adalah pekerjaan saya.  
> Karena itu saya mencoba  
> menulis menggunakan gajet,  
> meskipun hanya dengan satu jari.  
> Tangan kiri memegang gajet,  
> jempol tangan kanan mengetik.
> 
> Rusdi Mathari

Setelah membaca beberapa halaman, saya merasa cukup senang karena telah memutuskan tak jadi membawanya ke dalam bus. Di situasi _mbleber_\-nya kasus korona, perginya kami serombongan adalah sebuah ketegangan. Buku SRB yang banyak menggambarkan isi rumah sakit saya pikir bakal menjadikan hati ini tidak karuan. Terima kasih saya ucapkan kepada intuisi tersayang.

Catatan menyayat sekaligus bikin haru, saat Cak Rusdi bercerita soal keluarga pasien sebelah yang ikut membangunkan teman-temannya ketika ia butuh pertolongan ke kamar mandi, dini hari. Cak Rusdi memang sepopuler itu. Ketika keluarganya tak dapat menunggu, teman-temannya dengan setia mau diajak bergiliran menunggu sekaligus membantu menyelesaikan keperluan Cak Rusdi.

Buku ini juga mengajarkan saya privilise ternyata tidak selalu buruk. Kesempatan Cak Rusdi untuk mendapatkan pelayanan maksimal dengan BPJS adalah hal menyenangkan yang saya baca hari ini. Tapi sebaliknya, ketika dokter onkologi selalu menjawab pertanyaan Cak Rusdi dengan "_elu pengin mati?_", rasa kesal saya membuncah.

Lalu, secepat petir menyambar, hati saya berubah damai, tepat ketika Cak Rusdi mengatakan tumor itu adalah bagian dari tubuhnya dan harus diterima. Di mana-mana, penerimaan terhadap suatu hal yang terjadi pada diri adalah sulit. "Damai" yang saya rasakan bukan bermaksud mensyukuri kesulitan Cak Rusdi, lho ya, tapi pada sikap sang wartawan yang menerima. Benar benar menerima.

Cerita berlanjut ke ruang ICU, tempat perawatan Cak Rusdi setelah operasi selesai. Ia menyaksikan pasien yang datang pergi. Pergi karena sembuh atau pergi karena meninggal. Tangis yang pecah dari keluarga. Rintihan menyayat pada dini hari. Saya, antara bisa atau tidak bisa membayangkan kesenduan ruangan itu.

Saya jadi ingat, tahun 2015 lalu budhe sepupu saya sakit, kritis. Ia dirawat di ruang ICU RS Sardjito. Beruntung, saya bersama Karang Taruna sempat menjenguk ke rumah sakit. Kami masuk dua orang dua orang ke ruang tersebut. Budhe terbaring di kasur. Saya tidak yakin, apakah ia mendengar kami atau tidak. Pundaknya naik turun, perutnya kembang kempis, matanya tertutup rapat.

Saat diumumkan sehari kemudian bahwa budhe meninggal, saya baru sadar, perut yang kembang kempis dan pundak yang naik turun saat itu ada dan hanya ada karena bantuan alat pernapasan. Bagi saya, ruang ICU itu telah menjadi pemantik yang menyakitkan.

Lewat buku ini, Rusdi Mathari mengajarkan kita bahwa hidup dapat berubah sedrastis itu. Ia dapat berputar secara cepat, dari atas ke bawah, dari berdaya sampai lumpuh tak bisa apa-apa. Meski begitu, tetap ada nilai-nilai yang diperjuangkan. Kata Cak Rusdi, "\[Aku\] harus bertahan dan hidup." Lalu terus berusaha, sekuat-kuatnya, sebaik-baiknya. Bahkan sampai maut menjemput.

Jumat, 2 Maret 2018. Perawat dari Kediri, Devi, menyaksikan sendiri proses meninggal Cak Rusdi di sebuah rumah kecil di tepi Setu Babakan. Cak Rusdi seperti ingin mengucapkan salam kepada Voja sebelum pergi. Lewat tatapan dan lewat ganggaman tangan.

_Alfatihah_ buat Rusdi Mathari ...

* * *

**Judul**: Seperti Roda Berputar  
**Penulis**: Rusdi Mathari  
**Penerbit**: Buku Mojok  
**Tebal**: viii + 78
