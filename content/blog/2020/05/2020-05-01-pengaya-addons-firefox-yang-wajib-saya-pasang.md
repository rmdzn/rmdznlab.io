---
title: "Pengaya (Addons) Firefox yang Wajib Saya Pasang"
date: "2020-05-01"
categories: 
  - "tekno"
tags: 
  - "software"
coverImage: "panda-merah-rmdzn.jpg"
aliases:
  - /post/2020-05-01-pengaya-addons-firefox-yang-wajib-saya-pasang/
---

Sudah cukup lama saya kembali ke Firefox, peramban besutan Mozilla yang populer dengan logo panda merah melingkar, setelah sempat [berpindah-pindah](https://ramdziana.wordpress.com/2017/03/26/perjalanan-peramban-favorit-dari-opera-sampai-pale-moon/) ke peramban lain. Selalu ada alasan untuk balik ke sini. Tapi bukan tentang itu saya menuliskannya. Saya ingin menyampaikan pengaya/ekstensi (_addons_) apa saja yang saya pakai, entah untuk tujuan fungsional atau perlindungan privasi.

**Daftar isi**:

1. [uBlock Origin](#ublockorigin)
2. [Bitwarden](#bitwarden)
3. [Firefox Multi-Account Containers](#firefox-container)
4. [ClearURLs](#clearurls)
5. [Redirect AMP to HTML (amp2html)](#amp2html)
6. [Auto Tab Discard](#autotabdiscard)
7. [Universal Bypass](#universalbypass)
8. [Old Reddit Redirect](#oldreddit)

Dari sejak beralih lagi ke Firefox, pengaya tumbuh. Berkembang. Dari satu sampai selebihnya. Saya termasuk pengguna yang mengikuti keributan karena peralihan pengaya berbasis XUL (XML User Interface Language) ke WebExtensions. Gara-gara peralihan ini, pengaya-pengaya favorit tersingkirkan, termasuk [DownThemAll!](https://addons.mozilla.org/en-US/firefox/addon/downthemall/) yang belum lama ini merilis versi berbasis WebExtensions-nya dan [Pentadactyl](https://github.com/5digits/dactyl) yang mati sejak 2017.

Karena saya tak berminat menyinkronkan pengaya saya ke peranti lain, saya putuskan untuk merekamnya dalam tulisan dan memperbaruinya bila perlu. Sewaktu-waktu jika saya memasang Firefox di tempat lain, saya tahu harus melakukan apa.

1. **uBlock Origin**

uBlock Origin adalah pemblokir iklan (_adblock_) yang mendapuk diri lebih efisien, gesit, dan ramping. Saya belum pernah melakukan _benchmark_ sendiri, membandingkan dengan pemblokir iklan lain seperti Adblock Plus salah satunya. Tapi saya yakin, seyakin-yakinnya, pemblokir iklan ini menjadi pengaya yang akan selalu saya pasang pertama kali saat memasang ulang peramban.

![uBlock Origin](/blog/2020/05/images/photo_2020-04-30_23-29-08.jpg)

Menjalankan peramban tanpa uBlock Origin adalah seperti saya yang menelanjangi diri sendiri. Telanjang bulat sambil berlarian di lapangan dan jalanan tentu bikin saya rentan oleh serangan; dari depan, dari belakang, dari samping, dari bawah, maupun dari atas, dibandingkan dengan pakai pakaian sekaligus armor tebal mengelilingi badan.

Tautan: [https://addons.mozilla.org/id/firefox/addon/ublock-origin/](https://addons.mozilla.org/id/firefox/addon/ublock-origin/)

2. **Bitwarden**

Bertahun-tahun, saya menggunakan pola konvensional untuk membuat sandi (_password_) di banyak layanan situsweb. Dengan menerapkan rumus sandi utama (_master password_) lalu dipadukan dengan karakter khusus situsweb tempat saya mendaftar akun. Tips menyenangkan agar saya tetap ingat dengan sandi yang saya buat sendiri.

![Bitwarden](/blog/2020/05/images/photo_2020-04-30_23-32-50.jpg)

Namun lama kelamaan pola ini menyulitkan. Terutama ketika ada beberapa situsweb yang membatasi karakter tertentu dipakai sebagai sandi (misalnya tidak boleh menggunakan tanda seru, tanda tanya, atau spasi) dan yang membatasi jumlah karakter yang dipakai sandi (misalnya sandi minimal 8 karakter, maksimal 12 karakter). Apalagi ketika saya sering _login_ akun lewat laptop dan ponsel, cara konvensional semakin menyulitkan karena pada titik ini saya hanya mengandalkan memori otak. Ingin saya catat pada berkas _txt_ atau layanan penyimpanan teks awan, tapi itu riskan.

Sebagai solusi, saya mendaftarkan diri ke Bitwarden dan memasang pengayanya di Firefox. Dengan Bitwarden, saya dapat menyimpan sandi akun yang sudah pernah saya buat dan menciptakan sandi yang kuat untuk akun layanan situsweb baru.

Tautan: [https://addons.mozilla.org/id/firefox/addon/bitwarden-password-manager/](https://addons.mozilla.org/id/firefox/addon/bitwarden-password-manager/)

3. **Firefox Multi-Account Containers**

Karjo tinggal bersama tiga temannya di kontrakan. Satu ruangan besar. Sekali satu penghuni kontrakan masuk rumah, ia dapat melihat barang milik Karjo dan dua temannya. Termasuk tempat tidur, buku harian, celana dalam, dan barang pribadi lain. Pemilik kontrakan melihatnya sebagai sebuah kesalahan. Oleh sebab itu, sang pemilik membuat sekat-sekat terpisah sehingga Karjo dan tiga penghuni lain dapat tidur dan menyimpan barang-barang pribadinya di ruang tersekat yang disediakan.

Saya lah pemilik kontrakan itu dan sekat tersebut adalah pengaya yang saya bahas pada poin ini.

![Firefox Multi-Account Containers](/blog/2020/05/images/photo_2020-04-30_23-33-53.jpg)

Firefox telah memiliki "fitur bawaan" bernama Facebook Container yang berfungsi mengisolasi situsweb yang berhubungan dengan Facebook ke dalam sekatnya sendiri. Termasuk Facebook dan Instagram. Firefox Multi-Account Containers bekerja seperti itu. Bedanya saya dapat memasukkan situsweb lain ke dalam sekat-sekat tersebut. Seperti situsweb Google, GMail, dan YouTube.

Tujuannya tidak lain tidak bukan untuk mencegah situsweb yang ada di dalam sekat melihat dan memanfaatkan barang-barang pribadi (terutama data _cookie_) situsweb lain.

Tautan: [https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)

4. **ClearURLs**

Pendek kata, pengaya ini dipakai untuk membersihkan URL dari pelacak atau kode lain yang mengikuti.

![ClearURLs](/blog/2020/05/images/photo_2020-04-30_23-35-50.jpg)

Contohnya, saya mendapatkan URL jualan helm sepeda di Tokopedia seperti ini:

https://www.tokopedia.com/adam-whyudee/helm-sepeda-batok-murah-hitam-doff-hitam?trkid=f%3DCa1404L000P0W0S0Sh%2CCo0Po0Fr0Cb0\_src%3Dsearch\_page%3D1\_ob%3D203\_q%3Dhelm+sepeda\_bmexp%3D24\_po%3D1\_catid%3D1510\_bmexp%3D24&whid=0

Pengaya ini akan mendeteksi kode _?blabla=blabla_ di belakang URL utama lalu menghapusnya. Sehingga saya akan mengunjungi URL seperti ini:

https://www.tokopedia.com/adam-whyudee/helm-sepeda-batok-murah-hitam-doff-hitam

Bersih tanpa embel-embel.

Tautan: [https://addons.mozilla.org/id/firefox/addon/clearurls/](https://addons.mozilla.org/id/firefox/addon/clearurls/)

5. **Redirect AMP to HTML (amp2html)**

Saya bukanlah penggemar AMP (Accelerated Mobile Pages). Karena itu, setiap membuka artikel blog/situsweb, sebisa mungkin saya akan membukanya dengan tampilan asli. Kalau kebetulan saya membuka halaman AMP maka saya akan memeriksa URL artikel dan menghapus unsur yang merujuk pada "amp". Itu sedikit _tricky_.

_Amp2html_ mempermudah proses ini. Pengaya akan mendeteksi apakah URL yang akan saya kunjungi adalah URL AMP. Seketika itu, _amp2html_ akan melempar artikel ke halaman asli.

Contoh kasus dapat dilihat di blog ini. Secara _default_, platform wordpress.com yang saya pakai mendukung AMP. Misalnya artikel di bawah ini:

https://ramdziana.wordpress.com/2018/12/15/kontrol-ortu-atas-gawai-anak-ribet-tetapi-berharga/amp/

Ketika pengaya _amp2html_ aktif, ia akan melempar URL ke versi non-AMP (halaman asli).

https://ramdziana.wordpress.com/2018/12/15/kontrol-ortu-atas-gawai-anak-ribet-tetapi-berharga/

Tautan: [https://addons.mozilla.org/id/firefox/addon/amp2html/](https://addons.mozilla.org/id/firefox/addon/amp2html/)

6. **Auto Tab Discard**

Laptop saya cuma memiliki memori 2GB. Kalau saya berselancar di situsweb berat seperti Google Maps dengan belasan tab dan kelupaan menutup tab yang sudah-sudah, mesin laptop bakal ngos-ngosan. Selanjutnya kursor akan berat untuk digerakkan menuju tanda 'x' pada jendela aplikasi.

![Auto Tab Discard](/blog/2020/05/images/photo_2020-04-30_23-37-04.jpg)

Untuk mengatasi hal tersebut, saya menggunakan pengaya Auto Tab Discard. Pengaya ini dapat menonaktifkan tab yang tidak diakses selama beberapa waktu (secara bawaan, 600 detik). Jadi, tab itu masih ada, tidak ditutup, tapi tidak aktif. Saya dapat mengaktifkan tab tersebut dengan cara membukanya.

Dengan pengaya ini, memori yang tak terpakai dapat berkurang secara otomatis. Potensi laptop ngos-ngosan akibat kelupaan menutup tab sama dengan nol.

Tautan: [https://addons.mozilla.org/id/firefox/addon/auto-tab-discard/](https://addons.mozilla.org/id/firefox/addon/auto-tab-discard/)

7. **Universal Bypass**

Kalau ini, pengaya yang berhubungan dengan tautan pendek (_shorten link_). Ia berfungsi menghapus pelacak pada setiap tautan pendek yang dikunjungi. Misalnya, _nih_, setiap kali kita mengklik tautan bit.ly dan t.co (tautan pendek dari Twitter), kita sebetulnya dilacak. Dari mana klik berasal, salah satunya. Universal Bypass menghapus pelacak yang akan mendeteksi pengekliknya.

![](/blog/2020/05/images/photo_2020-04-30_23-37-55.jpg)

Fungsi lain. Seperti namanya, Universal Bypass memangkas proses akses dari tautan pendek ke tautan aslinya. Maksudnya, saya tidak perlu mengklik tombol lagi jika ingin mengunjungi halaman yang dipendekkan dengan adf.ly dan sejenisnya. Cocok buat kegiatan undah-unduh di tempat macam-macam.

Sayangnya, kini Mozilla memblokir pengaya ini dari AMO (addons.mozilla.org) karena ia meng-_inject_ javascript ke halaman web dan itu dianggap melanggar kebijakan pengaya Mozilla. Jadi, kalau ingin memasang Universal Bypass harus mengunduh berkas .xpi-nya dari halaman resmi [universal-bypass.org](https://universal-bypass.org).

_Update_ (3-8-2020):

Universal Bypass sudah muncul lagi di AMO.

Tautan: [https://addons.mozilla.org/id/firefox/addon/universal-bypass/](https://addons.mozilla.org/id/firefox/addon/universal-bypass/)

8. **Old Reddit Redirect**

Saya pengguna Reddit tapi saya belum menyukai tampilan Reddit terbaru. Pengaya ini akan melempar secara otomatis halaman reddit.com (yang memiliki tampilan baru) ke halaman old.reddit.com (yang memiliki tampilan lawas).

Tautan: [https://addons.mozilla.org/id/firefox/addon/old-reddit-redirect/](https://addons.mozilla.org/id/firefox/addon/old-reddit-redirect/)

* * *

Di luar pengaya di atas masih ada pengaya lain yang saya pakai. Namun belum saya jadikan pengaya yang wajib saya pasang. Kalau pengaya tersebut cocok secara fungsional maupun dalam melindungi privasi, saya akan memperbarui artikel ini. Tunggu, ya!

\[Gambar oleh [strichpunkt](https://pixabay.com/id/users/strichpunkt-2136421/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1982445) dari [Pixabay](https://pixabay.com/id/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1982445)\]
