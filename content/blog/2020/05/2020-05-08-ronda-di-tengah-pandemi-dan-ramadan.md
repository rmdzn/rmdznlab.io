---
title: "Ronda di Tengah Pandemi dan Ramadan"
date: "2020-05-08"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "photo_2020-05-07_20-52-55.jpg"
aliases:
  - /post/2020-05-08-ronda-di-tengah-pandemi-dan-ramadan/
---

Semenjak adanya korona, ramai-ramai perusahaan mem-PHK karyawannya atau merumahkan hingga waktu yang tak pasti. Karena masalah ekonomi yang tak jelas, apalagi disusul dengan kebijakan pembebasan napi, kasus kemalingan atau kasus pengejaran maling makin marak di daerah saya.

Untuk menjaga kampung dari hal yang tidak-tidak pada masa pandemi, tentu saja ronda harus tetap jalan. Namun Ramadan kali ini beda. Khusus jadwal ronda Karang Taruna, termasuk saya (iya, saya masih muda, belum bapak-bapak), sebelum ronda kudu menyemprot kampung dengan disinfektan.

Minimal menyemprot gagang pintu, pagar, dan dinding atau tiang listrik yang biasa dipakai buat pegangan atau senderan. Ini adalah jadwal penyemprotan rutin yang digeser dari Minggu pagi. Normal lah, kami takut lemas, haus dan lapar berjamaah jika penyemprotan masih dilakukan pagi, pas puasa pula. _Heemmm …_

Ada dua jenis alat semprot yang dipakai; yang dipompa dan yang pakai baterai. Kalau versi pompa, penyemprot akan pegal sampai ke bisep dan trisepnya. Sering mengokang seperti saat menembak dengan _shotgun_. Kalau versi baterai, tinggal _charge_ dua sampai tiga jam, hidupkan saklar, lalu pencet semprotan. Tidak perlu memompa.

Setiap penyemprot akan ditemani minimal satu orang. Sebagai teman ngobrol, sebagai pengingat kalau-kalau ada sisi tempat yang lupa dijamah, dan sebagai teman gantian kalau punggung sudah terasa pegal. Saya selalu menjadi yang menemani. Kawan saya badannya kekar, tidak mau gantian. Hebat memang. Sehabis rampung, makan gorengan dan minum teh yang disediakan di gardu oleh ibu RT dan ibu RW tersayang.

Pekerjaan yang sederhana sekali.

Aktivitas berikutnya ialah ronda. Siaga, sembari menonton film di teve. Ada yang bermain karambol dan poker. Tenang, tidak ada judi di situ. Yang kalah hanya diteriaki dan ditertawai.

Ronda yang berbeda. Pada hari sebelum pandemi, Karang Taruna akan bergerombol di satu titik gardu. Gardu utara, kami menyebutnya begitu, pada Sabtu pertama. Gardu selatan pada Sabtu berikutnya. Gantian. Dua gardu ini berada di RT yang berbeda. Namun saat ini, untuk menghindari kerumunan, kami memutuskan untuk membelah diri. Siapa yang mau di gardu utara, silakan. Mau di gardu selatan, silakan.

Kalau sudah waktunya berkeliling kampung; mengambil jimpitan dan melaporkan jika ada hal-hal mencurigakan di sekitar rumah-rumah warga, kami siaga di gardu. Entah untuk melanjutkan hiburan sebelumnya atau ngobrol-ngobrol seperti biasa.

Ronda di tengah pandemi sekaligus Ramadan ini kadang melelahkan, kadang juga berasa normal. Rasa yang campur-campur. Apalagi kalau sampai rumah langsung kepikiran untuk sahur, soalnya tanggung kalau mau tidur. Mending santap besar saja sekalian.

Saya tahu, situasi ini tidak akan pulih secepat biji nasi jatuh ke lantai. Kita hanya perlu beradaptasi. Terbiasa dengan suasana dan situasi baru. Kenormalan yang baru.
