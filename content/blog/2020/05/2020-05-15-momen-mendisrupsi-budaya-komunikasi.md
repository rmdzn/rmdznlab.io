---
title: "Momen Mendisrupsi Budaya Komunikasi"
date: "2020-05-15"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "maria-krisanova-oxvxfgowewm-unsplash-solasi-rmdzn.jpg"
aliases:
  - /post/2020-05-15-momen-mendisrupsi-budaya-komunikasi/
---

_Aaah._

Mungkin saya tidak akan bosan mengatakan pandemi secara langsung maupun tidak langsung memperlihatkan sifat dan sikap manusia sebenarnya. Beda-beda. Apatis cuek bebek, peduli, konspiratif, ekofasis, argumentatif, rasional, _manutan_, atau campuran dari mereka.

Pelabelan seperti itu bukanlah hobi saya. Ia seakan menjebak saya masuk ke dalam label tertentu, membenarkan diri tanpa menerima liyan, dan bersikap bias.

Meski begitu saya menjadikan "label" sebagai pelajaran. Sebagai upaya agar saya sibuk mengenal diri sendiri dan tidak melabelinya dengan satu dua sikap tunggal saja. Supaya saya terlalu sibuk dengan diri sendiri hingga lupa menilai, melabeli, bahkan menghakimi orang lain. Kalau pun telanjur melabeli orang lain, saya anggap sebagai bahan untuk lebih "mengenal" pemilik label dan menentukan sikap saya terhadapnya.

Semasa pandemi, kemampuan "pelabelan"--saya lebih suka menyebutnya dengan "mengenal sikap"--mau tidak mau tumbuh lebih baik. Kemampuan mengerti tentang diri sendiri maupun tentang orang lain.

Tidak hanya sebatas pada personal tapi juga pada cara orang-orang memperlakukan lingkungannya. Individu terhadap individu lain atau lebih luas, kelompok terhadap kelompok lain. Atau, keduanya terhadap situasi.

Saya sebal sekali ketika pandemi Covid-19 mulai menjangkiti provinsi tempat saya tinggal tetapi tidak ada respon tanggap dari lingkungan. Minimal respon jika situasi semakin tidak baik. Respon terhadap situasi terburuk yang mungkin terjadi adalah cara kita mempersiapkan mental, agar tidak syok, kaget, dan kacau. Ini juga cara kita untuk mempersiapkan tenaga jika ada apa-apa.

Pengusiran tenaga kesehatan dari indekosnya, penolakan jenazah pasien Covid-19, dan pengucilan ODP/PDP/pasien positif adalah dampak dari ketidaksigapan lingkungan. Komunikasi antar warga tidak berjalan di situ. Biasanya karena mereka memelihara _status quo_. Banyak warga memaksa pikirannya untuk menerima bahwa situasi di lingkungan mereka akan selalu baik-baik saja.

Setiap bahasan yang mengarah pada respon kritis terhadap situasi yang berpotensi terjadi di masa mendatang akan diredam dengan bahasan lain yang tidak penting (cenderung berbentuk candaan) atau olokan/sindiran dan pernyataan pasif agresif berbasis agama.

Sebagian warga peduli dengan situasi. Namun karena mereka tak memiliki daya untuk bersuara, akhirnya memilih diam. Daripada dipersekusi atau didiamkan oleh tetangganya. Yang ada mereka hanya berdiskusi lewat _chat_ pribadi, alias menggerutu di belakang.

Di lain sisi, keengganan warga yang mau berpikir rasional untuk mengungkapkan pikirannya secara terang-terangan justru menjadi hal yang tidak baik. Opini tandingan kalah siar dengan opini seberang.

_Huuft_, letih …

Mengapa budaya menggerutu di belakang ini masih saja diaplikasikan, _sih_? Apa karena budaya Jawa itu sendiri? Terutama budaya Jogja yang _pekewuh_, tidak enakan. Tidak mau "konfrontasi" dan inginnya mengalah? (Saya sendiri orang Jogja dan sangat merasakan fenomena budayawi ini).

Di masa pandemi, saya berpihak pada rasionalitas. Budaya _ewuh pekewuh_ ini justru membuyarkan harapan. Tidak adanya opini lain yang menandingi opini ketidakpedulian, meremehkan, dan kolot di sekitar masyarakat membuat keragaman pikiran di masyarakat menjadi terhambat. Membahayakan masyarakat itu sendiri.

Situasi masyarakat yang membudaya hingga hari ini adalah polarisasi. Melihat suatu masalah hanya dengan hitam dan putih. Siapa yang berbeda pendapat, itulah yang dianggap musuh. Perbedaan pendapat menjadi hal asing. Padahal jika opini berseberangan saling disuarakan dengan bijak, seharusnya jalan tengah lebih nyaman didapat.

Bukankah "musyawarah mencapai mufakat" adalah prinsip masyarakat Indonesia untuk memutuskan sesuatu? Kalau isinya hanya gerutuan di belakang, mana bisa musyawarah? Pihak A mengungkapkan pendapatnya lalu bertindak. Pihak B tidak setuju tetapi hanya _ngedumel_. Tidak bertindak, menyampaikan langsung sesuai nilai yang mereka pegang.

Saya tahu, cukup banyak konsekuensi yang akan terjadi jika masing-masing pihak mengutarakan pikirannya. Namun justru itulah konsekuensi yang perlu dihadapi bersama. Ini tindakan kolektif, _kok_, bukan tindakan individu.

Melihat kultur yang sedemikian rumitnya hanya untuk ceplas ceplos (tentu sesuai koridornya), bagaimana kalau kita mulai mengubahnya? Sekarang. Ini adalah momen bagus untuk mulai menebarkan tradisi terus terang. Jangan ada gerutuan lagi, jangan ada gosip-gosip lagi di belakang. Agar semua terbuka. Menolkan potensi kesalahpahaman.

Percayalah, pandemi ini dapat menjadi ajang bagus untuk melakukan perubahan. Demi kebaikan, kenyamanan, dan keamanan bersama.

Contoh konkritnya sudah saya utarakan di tulisan _[Magnum Opus, Bacotan Kami Didengar!](/blog/2020/04/2020-04-12-magnum-opus-bacotan-kami-didengar/)_. Kami mungkin sedang beruntung saat itu. Tapi perlu dicatat, mengutarakan pendapat dengan cara yang berbeda dari kebiasaan sekitar patut dipertimbangkan, dalam taraf tertentu **harus dilakukan**. Jika pendapat kita berdasar dan rasional, pasti akan ada orang lain yang mendukungnya. Sedikit atau banyak.

Konsekuensi? Mungkin ada, tapi untungnya sampai sekarang belum terdengar. Konsekuensi paling ringan mungkin adalah olokan bernapas ageisme. _Kalian masih bocah, tau apa?_ Semacam itulah.

Namun, pada akhirnya, konsekuensi bukanlah apa-apa kalau dihadapi bersama. Ingin mulai mendisrupsi budaya komunikasi di sekitarmu? Yuk, lah!

\[Foto oleh [Maria Krisanova](https://unsplash.com/@mkrisanova?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) di [Unsplash](https://unsplash.com/s/photos/speak?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)\]
