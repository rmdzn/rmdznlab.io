---
title: "Kumpulan Cerpen, Mata Penuh Darah"
date: "2020-05-22"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "photo_2020-05-21_10-59-27.jpg"
aliases:
  - /post/2020-05-22-kumpulan-cerpen-mata-penuh-darah/
---

Baca buku lagi, _Sodara_.

Seminggu ini saya membaca _Mata Penuh Darah: Dua Peristiwa, 1966-2033_ (yang akan saya sebut sebagai _Mata Penuh Darah_ saja), kumpulan cerita pendek (cerpen) yang ditulis oleh 18 penulis. Bagi saya yang menggemari cerpen, buku seperti ini adalah pom bensin di tengah perjalanan puluhan kilometer. Tempat rehat setelah perjalanan panjang melelahkan tangan, kaki, dan fokus pikiran.

_Mata Penuh Darah_ dibuka oleh Agus Noor, sebagai Prolog. Ia membahas cerita-cerita soal masa depan dan fantasi yang akan selalu ada di dunia. Dulu maupun sekarang. Cerita tentang robot atau kalau dalam agama, cerita tentang juru selamat.

Agus Noor menyebutkan beberapa judul cerpen dalam buku ini sebagai contoh topik yang sedang dibahas. Misalnya, ia menyebut judul _Secangkir Kopi Ethanolic_ adalah upaya meyakinkan mengenai latar masa depan sebagai penerapan "formulasi teknologis".

Bagi saya, prolog ini sedikit ruwet. Namun saya yakin seiring saya membaca cerita-cerita di dalamnya, pasti akan paham sendiri. Tentu, ini merupakan asumsi …

Pernyataan terfavorit dari seorang Agus Noor melalui Prolog-nya adalah sindiran kepada umat manusia melalui perumpamaan robot yang hobi membaca karena takut terpapar risiko seperti manusia--terancam punah karena lupa membaca. Sindiran yang sangat relevan.

Lihat saja berapa kali kasus razia buku di negeri ini terjadi? Literasi di Indonesia masih terhitung rendah. Masih saja buku dirazia. Maunya apa? Memberangus pikiran kritis lalu menyuruh pemilik pikiran untuk manut-manut saja lalu ditembak di tempat?

![Mata Penuh Darah](/blog/2020/05/images/photo_2020-05-21_10-59-27.jpg)

Kembali ke topik.

_Mata Penuh Darah_ merupakan kumpulan cerpen untuk memperingati satu dekade Shira Media (sang penerbit buku). Judul tersebut juga merupakan salah satu judul cerpen yang ditulis Faisal Oddang. Cerita yang lumayan gelap. Saya jadi teringat G30S/PKI dengan narasi pemerintah yang begitu kelam. Film yang kerap [disetel](/blog/2018/09/2018-09-29-mempertahankan-kewarasan-setiap-30-september/) oleh stasiun televisi itu, _lho_.

Cerpen _Siapa yang Membawa Lesatan Ingatan ini Bermuara?_ oleh Teguh Dewangga menampakkan fenomena sosial tentang mudahnya sekelompok orang menuduh sekelompok lain sebagai biang kesesatan. Bahkan hingga mengorbankan nyawa. Lagi-lagi, cerita yang relevan dengan masa kini.

_Vanesia_, karya Pringadi Abdi Surya, membahas isu banjir Jakarta yang tidak pernah surut, yang diperparah oleh proyek reklamasi. Tidak dibahas secara langsung, tapi melalui cerita Jakarta yang kini serupa Vanesia, kota di Italia. Bagus di awal, tapi diakhir justru membahas generasi vetsin. Macam mana ini.

Pada _Perempuan dari Jalan Kuno Lingkar_, Pilo Poly bercerita tentang pemberontakan. Coba bayangkan film _Hunger Games_ dan _Star Wars_, _nah_ kurang lebih seperti itu.

Sebagian cerpen memang membahas isu-isu sosial dan lingkungan tetapi ada juga yang membahas masalah personal. _Mengenang Olea_ oleh Wi Noya menceritakan kisah manusia yang "menghidupkan" kembali orang mati melalui teknologi. Namun meskipun "menghidupkan" kembali orang mati adalah hal mengharukan, sebagian orang malah merasa tidak nyaman. Setiap manusia memang memiliki cara sendiri untuk mengenang sang kasih. Walau dengan sekadar ingatan.

Ada juga yang bercerita tentang keramahan dan kepedulian. Bukan pada manusia, tetapi pada robot. Hingga sang empu mencintainya. Cerita ini dapat ditemukan di _Secangkir Kopi Ethanolic_ oleh Ahmad Ijazi H. Cerita yang pendek dan menarik hati.

Cerita personal juga tersaji pada _Sembilan yang Kedelapan_ oleh Asmi. Kisah satu ini sangat manusiawi, tentang dua pasangan suami istri yang akhirnya berpisah tetapi berjanji untuk tetap mengadakan perayaan hari jadi. Dijamin akan memantik perasaan korban perselingkuhan atau pelaku perceraian.

Cerpen yang bernapas sama, tapi lebih liar, dituliskan oleh Bernard Batubara. Berjudul _Percintaan Terakhir M_. Pokoknya, _soo wiiild_ … Sedangkan untuk mewakili cerita personal relijius, ada _Katakombe Santa Fallecia_ oleh Agus Noor. Cerita ini salah satu favorit saya, _sih_.

Kalau perpaduan antara teknologi dan budaya Jawa, ada _Program Pembaca Nasib_ oleh Muhamad Aan. Keren, asli. Cerita ini dapat menjadi perkenalan awal dengan ilmu weton. Yaa, meskipun tetap sedikit samar.

Buku kumpulan cerpen _Mata Penuh Darah_ membawa begitu banyak pesan dan rasa. Bahkan membingungkan. Bagi saya, saya masih belum mengerti _Ule Sondok So-Len_ oleh Ai El Afif. Bahkan saya juga belum bisa menikmati _Budayut_\-nya Toni Lesmana.

Saat terus membaca, saya menemukan keunikan tersendiri pada sebagian cerpen, termasuk cerpen _Adam-Hawa, Iblis, dan Eksperimen Ali Mugeni_ oleh Ken Anggara, cerpen yang hanya memiliki satu kalimat saja.

Tidak banyak cerita pada buku ini yang dapat saya nikmati. Namun, secara umum, buku _Mata Penuh Darah_ dapat menjadi hiburan dan referensi bagi para penikmat cerita-cerita pendek.

* * *

**Judul**: Mata Penuh Darah: Dua Peristiwa, 1966-2033  
**Penulis**: Faisal Oddang dkk  
**Penerbit**: Shira Media  
**Tebal**: 206
