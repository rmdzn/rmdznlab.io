---
title: "Membangun Tembok"
date: "2020-05-29"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "backdrop-21534_1280-tembok-rmdzn.jpg"
aliases:
  - /post/2020-05-29-membangun-tembok/
---

Ada satu idiom berbahasa inggris yang sempat bikin saya bingung--dan baru benar-benar saya pahami kemarin lebaran.

_Setidakjelasnya sikap, hari lebaran malah membaca soal idiom. Ckck._

_Take for granted_. Idiom yang menyebutkan soal mewajarkan atau kurang menghargai. Contoh kasusnya, seorang suami yang menganggap bahwa pekerjaan rumah tangga sudah semestinya dilakukan oleh istri. Hingga menyebabkan sang suami kurang menghargai (_take for granted_) pekerjaan yang dilakukan sang istri.

> Baca artikel di [Sederet](https://www.sederet.com/tutorial/ungkapan-take-for-granted-arti-dan-penggunaannya/) kalau pengin lihat penjelasan panjangnya.

Saya sudah terbiasa tidak kemana-mana saat Idulfitri menjelang, kecuali saat hari H. Berkeliling ke rumah sesepuh bersama pemuda-pemudi kampung. Menyantap cemilan bersama. Berbasa-basi bersama.

Hari setelah itu, kehidupan datar-datar saja. Iya _sih_, ada keluarga di rumah. Tapi yaa relasinya biasa saja gitu. Tidak ada beda dengan hari biasa. Saya lebih sibuk dengan pikiran sendiri. Kesana kemari. Sambil melayani pembeli hingga leher belakang dan betis kencang. Sehari-hari, saya memang berjaga di warung kelontong kalau kalian belum tahu.

Mungkin, saya sudah masuk fase menganggap situasi seperti ini _take for granted_. Untuk masalah pada lingkungan keluarga, jujur, saya belum dapat menanganinya. Tapi, untuk _take for granted_ pada situasi sendiri, bolehlah saya bercerita.

"Sendiri" sudah mendarah daging. Sudah terlalu biasa. Tapi tahun lalu saya merayakannya dengan [bersepeda "keliling" Jogja](/blog/2019/06/images/2019-06-11-yang-dirindu-dari-lebaran-adalah-jalanannya/). Menikmati suasana kota yang sepi kala lebaran. Saya bahagia melakukannya. Tahun ini, saya memilih untuk berkontemplasi. Sebut saja begitu.

Dari sekian topik yang berlomba mencari perhatian korteks serebral, ada satu yang menarik, yang bikin saya bilang, "_ooh … gini ya_".

Setiap orang memiliki sekat atau tembok yang membatasi pergaulan mereka dengan orang lain. Beda-beda bentuk dan ukuran, tergantung dengan kepribadian orang tersebut.

(Sebagai catatan, "saya" di bawah belum tentu saya. Ini hanya penyederhanaan subjek)

1. Tembok tinggi berbahan batu bata

Ada yang membangun tembok, setinggi lebih dari manusia dewasa. Tembok padat berbahan batu bata. Tembok ini saya bangun untuk membatasi interaksi dengan orang lain, terutama dengan orang yang baru bertemu.

Saya tidak mengizinkan orang lain untuk masuk lebih dalam ke kehidupan pribadi saya. Orang lain masih tetap bisa berbicara dengan saya, meski mungkin sambil teriak karena tak terdengar.

2. Tembok tinggi berbahan plastik transparan

Tembok plastik berbahan transparan mengizinkan orang lain untuk melihat saya sepenuhnya. Mereka dapat menyapa, berkomunikasi dengan bahasa tubuh. Namun mereka tak saya izinkan untuk mendekat lebih jauh.

Kita kenal. Kita saling sapa. Tapi, setop, sampai situ saja.

3. Tembok tinggi berbahan karet transparan

Tembok ini setinggi tembok-tembok sebelumnya, tapi bedanya dia pakai bahan karet. Kita bisa _ngobrol_, saling menyapa dengan kata atau bahasa tubuh. Orang lain juga dapat menyentuh tubuh saya; sekadar colek-colek atau menepuk untuk memanggil.

Namun ingat, jari tetap terhalang oleh karet. Jadi, cukup. Berhenti di situ dulu, ya.

Tembok yang dibangun tidak selalu permanen. Pemilik tembok dapat memotong atau menggantinya dengan bahan lain semau mereka. Jika awalnya tembok berbahan batu bata lalu jarak antara saya dan orang lain semakin dekat, tembok akan saya ganti dengan plastik transparan, kemudian karet.

Bila memungkinkan, tembok tinggi dengan batu bata dapat dipreteli langsung, sedikit demi sedikit. Tanpa perlu mengubahnya ke bahan plastik atau karet transparan. Bisa juga sebaliknya, tembok yang sudah hampir terkikis habis, kembali dibangun rapat setinggi mungkin.

Itu semua hanya perumpamaan, _lho_. Tidak ada tembok fisik yang benar-benar saya dirikan.

Setiap manusia memiliki hak untuk membangun temboknya sendiri. Tanpa penghakiman. Namun mereka juga harus paham, tembok yang dipasang jangan sampai berdiri di samping pelantang suara. Pelantang yang kerap bikin berisik tetangga dan suka mengajak orang lain untuk membangun tembok serupa di hadapan orang yang sama.

_Sebentar kontemplasi saya hampir rampung ..._

\[Gambar unggulan oleh [PublicDomainPictures](https://pixabay.com/users/PublicDomainPictures-14/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=21534) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=21534)\]
