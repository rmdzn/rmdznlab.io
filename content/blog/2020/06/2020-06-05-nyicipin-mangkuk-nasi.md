---
title: "Nyicipin Mangkuk Nasi"
date: "2020-06-05"
categories: 
  - "kuliner"
tags: 
  - "kuliner"
coverImage: "photo_2020-06-02_11-58-01.jpg"
aliases:
  - /post/2020-06-05-nyicipin-mangkuk-nasi/
---

Masa pandemi begini saya jarang kemana-mana tapi justru saya jadi ingin menambah tulisan soal kuliner. Curang memang. Sebelum pandemi, waktu yang dapat saya pakai untuk minimal ke satu dua tempat untuk mencicipi makanan tidak saya manfaatkan. Haha.

Banyak orang mulai mengalihkan mata pencahariannya ke bisnis kuliner. Teman dan tetangga saya ada yang membuat kue ala lebaran; kuki, putri salju, nastar, kastangel, dll. Ada juga yang membuat pempek, lotek, masakan warteg, dan masakan unik lain yang perantara jual belinya secara daring (_online_).

Yang menarik _nih_, tetangga saya baru saja membuka bisnis kulinernya dengan nama: Rice Bowl 29. Saya memanfaatkan kesempatan ini untuk mengulasnya secara pendek, mumpung dekat.

Seperti namanya, Rice Bowl adalah nasi dengan segala rupa-rupa yang disajikan di dalam mangkuk. Mangkuk kertas dengan tutup plastik. Fakta seru, "29" dalam nama "Rice Bowl 29" adalah nomor rumah sang penjual.

Banyak sekali si _toping_. Seperti kalau roti tawar ada selai dan mesesnya, di Rice Bowl 29 ada daun selada, irisan tomat, kemangi, dan timun. Irisannya juga tidak nanggung. Besar lho itu, bukan seiris kecil-kecil dan tipis.

![Rice Bowl 29](/blog/2020/06/images/photo_2020-06-02_11-58-05.jpg)

Rice Bowl 29 juga dilengkapi dengan oseng ikan teri, mie goreng (mienya sekecil mie Atom Bulan), sayur tomat.

Kemarin ibu saya pesan varian ayam geprek. Ayamnya cukup besar. Saya kurang merhatiin, ayam bagian mana yang saya makan malam itu. Sambalnya cukup pedas, bikin saya lumayan kepedesan. Lumayanlah, bagi saya yang tidak begitu hal-hal berbau sambal.

Hari berikutnya, kami pesan varian telur. Ternyata sambal tidak hanya ditujukan untuk varian ayam geprek, di varian telur juga ada. Bedanya sambalnya lebih pedas. Sepertinya ini karena pengolahan sambal yang belum konsisten. Saya harap _sih_ besok bisa lebih konsisten dan menyediakan level kepedasan. Macam menu masakan pedas kekinian.

Nasinya terlihat sedikit, tapi sebetulnya padat. Semangkuk itu dijamin bikin perut kenyang. Kalau memang kurang, bisa kok beli dua porsi sekalian. Omong-omong, saya kurang pandai menilai rasa. Namun dengan sekelebat kecapan, rasanya jos. Yakin.

Kata penjualnya, kemarin masih harga promo. Ibu saya beli dihitung 13.000 per porsi. Itu yang berlauk ayam geprek. Kalau lauk telur ceplok tentu lebih murah lagi. Mungkin kalau kamu beli sekarang, harga dihitung normal. Tidak ada promo lagi.

Kalau kamu penasaran dan pengin pesan, lihat info di bawah ini. Sebagai catatan, pemesanan masih diantar sendiri oleh penjual, jadi masih terbatas wilayah Yogyakarta.

![Rice Bowl 29](/blog/2020/06/images/photo_2020-06-03_23-51-17.jpg)

Gambar ini saya ambil dari status WhatsApp penjual :D

Ini kalau mau klik lokasinya: [https://maps.app.goo.gl/y4NYnQRscvmxk2SR6](https://maps.app.goo.gl/y4NYnQRscvmxk2SR6).
