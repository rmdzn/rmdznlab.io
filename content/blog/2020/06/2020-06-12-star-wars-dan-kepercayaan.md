---
title: "Star Wars dan Kepercayaan"
date: "2020-06-12"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "school-colorful-figures-toys-2953771-star-wars-rmdzn.jpg"
aliases:
  - /post/2020-06-12-star-wars-dan-kepercayaan/
---

Selama tidak kemana-mana, bosan _sih_, meski kebiasaan masa pandemi tak jauh berbeda dari hari-hari sebelumnya. Namun, sejak itu banyak nilai kehidupan yang saya tangkap. Yang baru ada. Yang lama ada, dengan penguatan.

Salah satunya, jangan mudah percaya orang lain, termasuk keluargamu sendiri.

Sejak masih bocah, saya selalu dicekoki pendapat pribadi kakak saya bahwa film Star Wars tidak menyenangkan. Dia mendapuk diri tak suka dengan perang-perang ala luar angkasa. Saya angguk-angguk saja.

Masa pandemi, pikiran kritis saya bangkit. _Emang_ iya, Star Wars tidak semenyenangkan itu?

Satu-satunya cara adalah menontonnya. Membuktikan premis. Bermodalkan artikel Star Wars di [Wikipedia](https://en.wikipedia.org/wiki/Star_wars), saya mulai mengikuti film perang luar angkasa ini. Awalnya bingung. Apakah saya lebih baik menonton berdasarkan garis waktu cerita atau waktu peluncuran film.

Kalau berdasarkan garis waktu cerita, urutannya prekuel (Episode I-III) lalu _original_ (Episode IV-VI), dilanjut sekuel (Episode VII-IX). Saya terpikir untuk menonton dengan cara ini karena teringat _postingan_ orang-orang beberapa bulan lalu. Tentang cara menonton berdasar garis waktu; Star Wars maupun film-film Marvel Cinematic Universe (MCU). Niatnya biar tidak bingung gitu.

Namun, atas rekomendasi kakak saya (iya, saya bilang dia kalau saya ingin mengikuti film Star Wars), lebih baik saya menonton berdasarkan waktu rilis film. Urutannya _Original_ kemudian Prekuel lalu Sekuel.

Mulai pekan awal April saya bertualang menikmati film produksi Lucasfilm. Dimulai dari _Episode IV: A New Hope_. Princess Leia muncul pertama kali. Mark Hamill, sang pemeran Luke Skywalker, masih muda banget.

Blas, tidak ada bayangan cara tertawa Joker di film itu. Maklum, selama ini saya selalu menyangkutpautkan Mark Hamill dengan karakter Joker pada film kartun DC. Dia memang pengisi suara Joker sejak tahun 1993.

Selanjutnya adalah _Episode V: The Empire Strikes Back_ dan _Episode VI: Return of the Jedi_. Mereka bagian dari serial _original_ Star Wars. Semua karakter telah ditunjukkan di sini; Luke Skywalker, Leia, Han Solo, Chewbacca, Yoda, dan para antagonis seperti Darth Vader, Boba Fett, dll. Nama "Boba Fett" belum disebutkan tapi karakter fisiknya cukup berperan. Bahkan dia punya [basis penggemar sendiri](https://www.bobafettfanclub.com/). Saya kaget bukan main.

Selesai melihat satu seri, saya menyimpulkan Star Wars bakal menjadi cerita kompleks. Darth Vader yang ternyata bapaknya Luke adalah fakta pendukungnya. Di samping itu perlu ada kejelasan, siapa itu Obi-Wan Kenobi dan mengapa ia menjadi guru dari seorang Vader yang digadang-gadang menjadi Jedi penyeimbang Force tapi malah berbelok menjadi karakter jahat (yang kemudian saya ketahui dengan sebutan "Sith").

Demi menikmati tontonan itu sendiri, saya tak menonton secara maraton. Ada jeda. Sekitar satu pekan sekali lah. Satu atau dua hari setelah menerbitkan tulisan baru di blog ini.

Hari berikutnya, saya mulai menonton prekuel. _Episode I: The Panthom Menace_, _Episode II: Attack of The Clones_, dan _Episode III: Revenge of The Sith_. Segala kompleksitas yang saya bayangkan terjawab pada seri ini. Gila memang George Lucas.

Dalam prekuel, ia menceritakan masa kecil Darth Vader, apa itu Jedi dan Sith, alasan mengapa Jedi diburu, dan bagaimana Palpatine menjadi kaisar. Bengis pula. Pada titik ini saya akui, Star Wars memang pantas memiliki basis penggemar yang sangat besar.

Omong-omong, kalau saya menonton Star Wars sejak lama, Natalie Portman akan menjadi _crush_ saya, menggeser Emma Watson.

Sejak prekuel berakhir, George Lucas berhenti menulis naskah Star Wars. Sekuel Star Wars dikerjakan oleh J.J Abrams dkk di bawah naungan Lucasfilm yang sudah dibeli oleh Disney pada tahun 2012. Saya menghabiskan sekuel tersebut dua bulan terakhir. _Episode VII: The Force Awakens_, _Episode VIII: The Last Jedi_, dan _Episode IX: The Rise of Skywalker_.

Sekuel didominasi oleh pemain-pemain baru, termasuk Daisy Ridley (Rey), John Boyega (Finn), Adam Driver (Kylo Ren), dan Oscar Isaac (Poe). Namun bukan berarti pemain _original_ tidak terlibat, di serial ini juga ada kok Mark Hamill (Luke Skywalker), Carrie Fisher (Leia), dan Harrison Ford (Han Solo). Droid lawas juga muncul. R2 dan C-3PO.

Di sela-sela itu, ada film antologi atau _spin off_; _Rogue One: A Star Wars Story_ yang menceritakan kisah sebelum Dark Star meledak dan _Solo: A Star Wars Story_ yang, tentu saja, menceritakan perjalanan Han Solo sewaktu muda.

Aaa, ternyata Star Wars begitu menyenangkan untuk dinikmati. Ceritanya yang lumayan padat dan berisi. Saya langsung menancapkan Chewbacca (Chewie) sebagai karakter favorit saya. Makhluk berbulu dari bangsa Wookie yang kadang marah-marah sendiri. Berkarakter setia yang tak perlu diragukan lagi. Isu kematian Chewbacca (Chewie) bikin saya emosional dibandingkan kematian asli Han Solo dan Princess Leia.

Chewie karakter lama yang bertahan hingga akhir. Bersama Lando (diperankan oleh Billy Dee Williams) dan dua droid yang saya sebut sebelumnya. Makhluk ini memiliki tingkat kepercayaan yang tinggi. Dimulai dari pertemuannya dengan Han Solo di bawah tanah lalu menjalankan misi-misi bersama sang pemuda acak. Rasa percayanya juga berhasil mengantarkan Rey ke sebuah planet yang dihuni Luke tua. Chewie yang tangguh meski kehilangan sahabat lekatnya.

![Chewbacca di Star Wars](/blog/2020/06/images/chewbacca-rmdzn.jpg)

Kepercayaan yang tinggi tak hanya dapat diambil dari kisah Chewie. Luke tua yang akhirnya percaya kepada Rey dan Rey yang percaya pada nilai-nilai Jedi adalah contoh lainnya.

Huh, tak seperti saya yang tidak percaya bahwa Star Wars sangat layak diikuti dan digemari--hanya karena perkataan seorang dewasa.

_Semoga Force bersamamu ..._

\[Gambar unggulan oleh **[Markus Spiske](https://www.pexels.com/@markusspiske?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/photo/school-colorful-figures-toys-2953771/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
