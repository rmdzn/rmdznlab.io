---
title: "Juni yang Tak Tertebak"
date: "2020-06-19"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "calendar-2428560_1920-juni-rmdzn.jpg"
aliases:
  - /post/2020-06-19-juni-yang-tak-tertebak/
---

Kalau diingat-ingat, satu sampai dua bulan lalu, sebagian masyarakat terbagi menjadi dua kutub. Yang berpikir kritis serta menjalankan saran dari dokter dan ahli serta yang masa bodoh dan tak berminat mengikuti saran dokter dan ahli. Saran yang dimaksud adalah protokol kesehatan; pakai masker, cuci tangan secara rutin dengan sabun dan air mengalir atau _hand sanitazer_, serta jaga jarak fisik.

Kutub pertama cukup solid. Dalam segi sikap maupun tindakan. Kadang mereka membuat orang-orang rasional lainnya lebih tahan banting. Mereka yakin banyak orang yang sepaham sehingga mental lebih mantap.

Kala itu rasa takut yang menyebar membantu orang lain untuk sama-sama bersikap waspada. Aliansi rasionalitas terus berkembang, menguatkan. Saya melihat masa itu adalah tempat kita untuk "hanya" disibukkan dengan uraian pengelolaan rasa takut. Agar rasa takut tetap wajar, tidak berlebihan sehingga merusak imun dan kesehatan.

Jika digambarkan dengan grafik, **angka penyakit (Covid-19) rendah, angka rasa takut tinggi.**

Pada waktu yang sama, di sebuah kampung bernama Balada (huruf 'a' pada 'lada' dibaca seperti 'apa' dalam bahasa Jawa), sebagian besar warganya mau disiplin dan waspada. Setidaknya dalam penggunaan masker. Tahu apa yang harus dilakukan saat itu.

Hari ini, bulan Juni 2020. Situasi tak mudah ditebak.

Sekomplek yang sebelumnya mampu berpikir rasional memilih untuk membelot. Padahal, ada lebih dari satu orang anggota komplek yang bekerja di rumah sakit rujukan penyakit Covid-19. Mereka adalah saksi adanya pasien positif keluar masuk, tenaga kesehatan berhazmat keluar masuk, dan segala protokol kesehatan yang bejibun.

Sekarang? Mereka berada dalam mode cuek. Tetangga akrab yang rumahnya di sebelah komplek mengatakan tetangga mereka sudah jarang memakai masker. Bahkan satu dua orang komplek mulai mempropagandakan bahwa keadaan akan selalu aman, baik-baik saja. "_Gilo, buktine ra ana apa-apa_," kata mereka. (Artinya: ini lho, buktinya tidak ada apa-apa).

Semakin waktu berlalu ternyata sikap ketidakpedulian menjadi separah itu. Orang-orang yang diharapkan makin sadar dan waspada karena grafik terus meningkat justru melakukan sebaliknya. Masa bodoh.

Hal ini berhubungan dengan rasa bosan yang dialami selama dua bulan lebih. Apalagi disambut dengan ketidakpastian dari pemilik kuasa di negeri ini. Yang bikin sebal, masalah diperparah dengan banyaknya teori konspirasi. Semakin banyak orang yang percaya bahwa Covid-19 adalah rekayasa. Covid-19 adalah bisnis semata. Covid-19 adalah cara rumah sakit dan tenaga kesehatan untuk mencari keuntungan lebih dalam bentuk bantuan dari pemerintah dan jualan organ. Tidak ada pasien Covid-19 yang meninggal. Adanya mereka meninggal karena penyakit lain. _Enggak nyangka orang-orang begini amat …._

Jika digambarkan dengan grafik, saat ini **angka penyakit (Covid-19) tinggi, angka rasa takut rendah**.

Ada rasa _capek_ membahas topik soal Covid-19 yang itu-itu saja--masyarakat yang abai. Tapi, lewat sini saya ingin mengajak semua pembaca untuk terus mengedukasi diri. Bersama di barisan rasional lalu sampaikan apa yang sebenarnya terjadi, minimal di lingkaran terkecil (keluarga). Semampu mungkin tangkal hoaks, praduga tak jelas, dan cerita yang mengada-ada.

Kata dr. Reisa, "\[…\] saling mengingatkan kembali, dan kalau ada yang masih belum patuh dan disiplin, saling menegur dan mengingatkan yaa."

https://twitter.com/Reisa\_BA/status/1272114761673531396/

Padahal tidak semudah itu, Ibu. Selama yang ditegur telinganya mampet, yaa kita tidak akan gerak kemana-mana. Justru kita malah stres sendiri.

Saat ini yang kita perlukan adalah kesadaran bahwa situasi ini mungkin masih bertahan lama. Tidak apa-apa jika kita merasa takut dan cemas. Biarkan ia mengalir. Pertahankan perasaan tersebut dalam batas normal, tidak berlebihan. Jika perasaan mulai menganggu aktivitas, tidak apa-apa kalau ingin konsultasi dengan psikiater atau psikolog. Banyak sekali informasi seputar ini di media sosial.

Yang dapat kita sama-sama lakukan adalah saling menguatkan. Di tengah masyarakat yang abai untuk menerapkan protokol kesehatan, kita dapat saling berteriak lantang, "Yok bisa yok!"

Terus patuhi protokol kesehatan. Pakai masker kemana-mana, cuci tangan rutin dengan air mengalir dan sabun, jangan memegang muka jika belum cuci tangan, dan jaga jarak fisik. Jika tak bisa melakukan jaga jarak fisik akibat sistem di lingkungan yang masih lemah, kuatkan pada protokol kesehatan lainnya. Kita terapkan apa kata Descartes: _Cogito ergo sum_, aku berpikir maka aku ada. Berpikir kritis sambil melihat realita. Bukan malah percaya diri dengan kata-kata: _Saya dan keluarga saya tidak kena, maka penyakit ini tidak ada._

\[Gambar unggulan oleh [MaeM](https://pixabay.com/users/MaeM-5698852/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2428560) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2428560)\]
