---
title: "Kala Saya Bingung Ingin Menulis Apa"
date: "2020-06-26"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "cats-1852980_1920-kucing-ngambek-cranky-rmdzn.jpg"
aliases:
  - /post/2020-06-26-kala-saya-bingung-ingin-menulis-apa/
---

Ada satu waktu atau paling tidak selama seminggu, saya tidak memiliki ide apa-apa untuk ditulis. Padahal ada banyak coretan di buku saya yang mungkin saja bisa dijabarkan lebih. Namun mau bagaimana lagi kalau otak sedang mandek.

Saya hanya ingat tiga penulis yang memiliki solusi untuk hal serupa. Nadhifa Allya Tsana alias Rintik Sedu, penulis _Kata_ dan _Geez & Ann_, mengatasi masalah tersebut dengan tidur. [Nawang Nidlo Titisari](/blog/2019-02-10-ketemu-mbak-nawang-yang-hilang/), penulis [Hilang](/blog/2019/02/2019-02-14-menikmati-kehilangan-dengan-hilang/), juga tidur. Amanatia Junda, penulis _[Waktunya untuk Tidak Menikah](/blog/2020-02-14-menyimak-isu-isu-perempuan-lewat-cerpen/)_ dan _[Kepergian Kedua](/blog/2020/03/2020-03-27-kepergian-kedua-sederhana-yang-kaya/)_, melakukan hal serupa tapi ditambah dengan melupakannya (haha). Maaf kalau ingatan saya remuk.

Meskipun saya tidak sepopuler mereka, saya juga mau cerita. Saya juga akan tidur--kalau ada waktu dan kalau situasi memungkinan. Ruang tempat saya mengetik ini sewaktu-waktu ramai, sewaktu-waktu sepi. Sewaktu-waktu bisa fokus dan duduk lama, sewaktu-waktu bisa duduk-berdiri-duduk-berdiri berulang kali. Jadi, tidur belum tindakan tepat.

## Main _Half-Life_

Kalau sudah begitu, pantat saya akan bergeser ke samping. Menghadap monitor layar panjang dengan cangkang komputer pribadi berlampu kerlap-kerlip pada bagian samping. Singgah sebentar di komputer kakak saya yang sedang ditinggal pergi.

Saya mengoleksi serial _Half-Life_ di akun Steam. Sudah beli sepaket beberapa tahun lalu, tapi sejak saat itu saya belum pernah memainkannya sekalipun, kecuali _Half-Life 2_ yang sebelumnya saya beli secara satuan.

_Half-Life_ adalah _game_ lawas dengan grafik yang masih kotak-kotak. Unduhannya tak sampai berukuran satu keping CD (<700MB). Jadi, wus wus wus.

![Akun steam](/blog/2020/06/images/steam-akun-rmdzn.jpg?w=720)

Akun steam saya (ID-nya rahasia)

Sekitar 18 jam saya menamatkan _Half-Life_ 1. Bertualang dengan karakter Gordon Freeman lalu melawan alien-alien dan militer Amerika. Ini tidak saya mainkan sekali jalan _lho_ ya, tapi sekitar satu pekan lebih. Saya bangga, saya dapat menamatkan seri pertama _Half-Life_ ini tanpa _cheat_, seperti mengecilkan nilai _sv\_gravity_ agar bisa terbang-terbang. Sebelumnya saya pernah main _Half-Life_ sebentar lalu pakai kode itu untuk melawan bos terakhir soalnya. Haha!

Setelah _Half-Life_, saya mengunduh _Half-Life: Blue Shift_\--_spinoff_ _game_ utama. Di sini saya memainkan karakter Barney Calhoun, seorang penjaga keamanan di Black Mesa. Karakter sama yang juga dimunculkan di _Half-Life 2_, bertemu dengan Gordon. Total main sampai tamat sekitar 4 jam saja.

Berikutnya, saya mengunduh _Half-Life: Opposing Force_. Ini adalah _spinoff_ lain dengan pemeran utama Adrian Shephard, kopral marinir Amerika yang misi utamanya sebetulnya adalah membunuh alien dan membungkam saksi insiden di Black Mesa tapi malah sibuk bertahan hidup. Di sini saya menghabiskan waktu total hampir 15 jam.

Yang menarik, sebagai _game spinoff_, Barney Calhoun dan Adrian Shephard juga dapat melihat Gordon Freeman secara langsung. Barney melihat Gordon lewat portal yang bergerak acak. Gordon saat itu tengah diseret oleh pasukan militer Amerika. Sedangkan Adrian melihat Gordon saat melompat ke portal Xen--skena sebelum Gordon masuk ke dunia alien.

Sekarang saya sedang mengulang bermain _Half-Life 2_. Belum tamat. Lalu lanjut _Half-Life 2: Lost Coast_, _Half-Life 2: Episode One_, dan _Half-Life 2: Episode Two_.

## Eksplorasi Musik

Saya masih di usia senang mengeksplorasi musik baru. Maksudnya, musik yang belum pernah saya dengar, bukan musik gres yang baru dirilis. Beda dengan angkatan bapak saya yang selera musiknya sudah berhenti di Koes Plus, Broery Marantika, Ebiet G. Ade, dll.

Saya tidak membatasi dengan genre tentu, yang penting _mah_ _nyantol_ di kuping. Awal-awal pandemi di Indonesia, saya mendengarkan lagu-lagu Reality Club. Itu _lho_, grup musik rock indie yang salah satu personilnya Fathia Izzati. Asyik, _btw_. Ketukannya menyenangkan. Hampir semua judul menjadi favorit saya, terutama _Hesitation_, _2112_, dan _Telenovia_.

Berikutnya adalah lagu K-Pop. Ini bukan pertama kali saya mendengarkan lagu K-Pop dengan gencar, toh sebelumnya saya hobi mendengarkan lagu BlackPink. Gara-gara lagu _Eight_\-nya IU bersama Suga, saya mulai mendengarkan lagu IU lainnya. _Blueming_ mantap juga. Kalau yang _mellow_, _Through The Night_ enak di telinga. Saya belum merambah lebih jauh, jadi yaa belum bisa menyebutkan lagu perempuan cantip ini banyak-banyak.

Tambah lagi. Secret Number baru saja debut. Menelurkan lagu pertamanya berjudul _Who Dis?_. Kemungkinan besar saya akan mengikuti grup ini. Haha.

Kalau sedang random-randomnya, saya memilih untuk mencari daftar lagu di YouTube. Kemarin saya mendapatkan daftar lagu di bawah. Enak. Coba saja.

https://www.youtube.com/watch?v=2chfsFTNEXw&t=4360s

## Menonton Drama Korea (Drakor)

Seumur hidup saya baru menonton dua judul drakor. Yang pertama, Bel Ami. Dua tokoh utamanya diperankan oleh Jang Geun-Suk sebagai Dokgo Ma Te dan IU sebagai Kim Bo Tong. Iya, saya baru ngeh bahwa Kim Bo Tong adalah IU gara-gara saya mulai mendengarkan lagu-lagu miliknya.

Beberapa tahun lalu Bel Ami tayang di GlobalTV. Karena sudah mengikuti sejak episode awal, tanggung, akhirnya menonton sampai selesai.

![Poster 'Reply 1988'](/blog/2020/06/images/poster-reply-1988-tvn-wiki-rmdzn.jpg)

Poster 'Reply 1988'

Pada masa pandemi ini, saya menonton drakor lagi. Kali ini drakor "direkomendasikan" oleh diskusi yang kerap berseliweran di Twitter yaitu _Reply 1988_. Sampai tulisan ini terbit, saya belum menyelesaikan film ini.

**Update**: Saya sudah menyelesaikan drakor ini. Cek tulisan saya di [_Kita Masih Bertahan di Ssangmun-dong Setelah Semua Keluarga Pergi_](/blog/2020-07-10-kita-masih-bertahan-di-ssangmun-dong-setelah-semua-keluarga-pergi/).

## Membaca Buku

Sebenarnya membaca buku adalah salah satu cara untuk mencari ide lain untuk ditulis. Namun, kalau saya sedang mau santai saya akan pergi ke iPusnas dan melihat katalog buku yang tidak akan saya ulas di blog ini, entah memang "tidak" sampai seterusnya atau "tidak" untuk sementara.

Saya sekarang sedang mengikuti petualangan Hercule Poirot. Sebagai penyeimbang agar sikap dan metode pemecahan masalah ala detektif tidak hanya merujuk pada Sherlock Holmes. Ada 43 buku karya Agatha Christie yang menceritakan perjalanan _Monsieur_ Poirot bersama Arthur Hastings. Semoga ada di iPusnas semua. _Amin paling serius … seluruh duniaa_~.

Sayang _nih_, saya sedang membaca _The Mysterious Affair at Styles_ di iPusnas, tapi ada masalah pada satu halaman. Ia menjorok ke kiri. Jadi halaman terpotong separuh lebih. Sekarang masalah sedang diperbaiki. Admin Perpusnas dan iPusnas gercep!

Terima kasih telah mengunjungi blog ini. Kalian bisa _lho_ mendukung saya untuk terus menulis di sini dan di platform lain. Kunjungi akun saya di [karyakarsa.com/rmdzn](https://karyakarsa.com/rmdzn) (ada karya pertama saya) atau [trakteer.id/rmdzn](https://trakteer.id/rmdzn). Terima kasih lagii!

\[Gambar unggulan oleh [Aline Dassel](https://pixabay.com/users/dassel-989431/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1852980) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1852980)\]
