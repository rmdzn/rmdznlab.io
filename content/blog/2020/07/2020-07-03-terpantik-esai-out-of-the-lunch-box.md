---
title: "Terpantik Esai-Esai 'Out of the Lunch Box'"
date: "2020-07-03"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "photo_2020-07-02_14-46-32.jpg"
aliases:
  - /post/2020-07-03-terpantik-esai-out-of-the-lunch-box/
---

Demi mengayakan mata dan pikiran, kali ini saya membaca buku _Out of The Lunch Box_, kumpulan esai Iqbal Aji Daryono. Esai-esai yang dituliskan di sini bukanlah esai yang murni ditulis untuk buku tetapi esai yang sebelumnya telah terbit di media daring (_online_), termasuk Detik, Mojok, Tirto, Kumparan, dan platform penerbitan daring Medium.

"Mengayakan" yang saya maksud adalah memperbanyak genre buku yang dibaca. Biar saya tidak cuma _muter-muter_ doang pada satu topik dan pikiran saya makin ramai dengan referensi-referensi.

"_Out of the Lunch Box_" sengaja dimiripkan dengan buku Iqbal Aji Daryono lain yang berjudul "_Out of The Truck Box_". Supaya orang atau teman lebih mudah mengenali bahwa itu bukunya, begitu katanya pada bagian pengantar.

Respon saya saat membaca pengantar lanjutan adalah "_iya, benar juga ya_". Bertahun-tahun lalu, jika orang-orang ingin mengungkapkan pikiran di media massa, mereka harus repot-repot menyusun kata sesuai bahasa Indonesia yang disempurnakan (EYD) dan menyiapkan data pendukung topik yang disampaikan sedetail-detailnya. Lalu melewati penyaringan oleh editor yang ketat. Kalau naskah tidak diterima, harap maklum, perlu ada pembenahan lagi. Kalau diterima, itu luar biasa hebatnya.

Dunia daring mendisrupsi semua, setiap orang berhak dan dengan mudahnya memiliki alat penerbit pikirannya sendiri. Dalam bentuk blog atau akun media sosial seperti Facebook dan Twitter. Orang lain juga dapat menantang atau menyanggah tulisan dengan tulisan lainnya. Soal apakah tulisan penuh [cacat logika](/blog/2016/10/2016-10-11-macam-macam-kesalahan-logis-logical-fallacy/) atau lepas substansi itu cerita lain.

Sesuai dengan subjudulnya, _Out of the Lunch Box_ berisi esai tentang sosial, politik, dan agama--tiga topik yang bagi mas Iqbal memang jagonya (saya memang belum mengikuti media sosial penulis buku tetapi saya sempat membaca beberapa esainya jauh sebelum saya membaca buku ini).

Ada beberapa yang cukup berhubungan dengan kehidupan saya. Termasuk _Ustadz Yusuf Mansur, Betulkah Kita Sedang Membela Islam?_ yang mengingatkan saya pada kejadian beberapa tahun lalu. Saya masih SMA, anak rohis (Rohani Islam) aktif. Pada malam hari, tetangga saya (kristen atau katolik saya tak tahu pasti) menjalankan ibadah rutin (semacam kebaktian sebulan sekali), mengundang rekan-rekannya. Alhasil, rumah tersebut menjadi sangat ramai. Ibadah pun terdengar keras.

Saya merespon dengan cepat. Di ruang depan, saya menyetel _murottal_ cukup keras. Seburuk-buruk tindakan yang dilakukan. Goblok. Kalau saya mengingat situasi kala itu, rasa menyesal langsung timbul.

Saya lebih senang dengan Bab 2. Bab Capcay Kuah. Bahasan tentang agama alih-alih tentang politik pada Bab 1 (yang lebih berhubungan dengan pemilu dan partai-partai). Misalnya _nih_, bahasan soal apakah ada Muhammadiyah garis lucu. Sebagai sesama manusia yang lahir di keluarga Muhammadiyah, saya kok seratus persen mengiyakan. Haha …

Selintingan tentang agama yang dipadukan dengan sosial memang tidak ada matinya. Misalnya isu kristenisasi dan islamisasi serta tentang fungsi doa yang membabi buta (tanpa ikhtiar dengan semestinya) pada esai _Undangan "Doa Besama 270 Rakyat Indonesia"_. Sangat _relate_ dengan situasi akhir-akhir ini. Orang-orang beramai-ramai melanggar protokol kesehatan dengan alasan bahwa sakit dan mati ada di tangan gusti Allah. Padahal protokol kesehatan adalah ikhtiar untuk mencapai takdir yang lebih baik.

Cerita tentang agama warisan menohok saya. Bikin saya mengakui bahwa, "_saya menganut agama warisan_". Saya yakin tidak sedikit orang yang mengakui, tapi ternyata banyak juga yang bersikap menyangkal. Marah-marah sendiri. Aah, esai yang penuh arti.

![](/blog/2020/07/images/photo_2020-07-02_20-46-20.jpg)

Beberapa esai menjadi favorit saya, termasuk _Tarawih di Gereja Bersama Maulana_. Iqbal masih tinggal di Perth, Australia. Pada bulan Ramadan, ia ikut buka bersama dan salat tarawih di gereja. Sang imam adalah seorang mualaf. Seorang ortodoks yang terbuka dengan dialog.

Ternyata, di gereja tersebut sudah diselenggarakan Jumatan rutin, bahkan sudah menginjak tahun keenamnya pada saat esai tersebut ditulis. Ini adalah hasil kerjasama dengan sang pastur. Maulana, nama sang imam, memang memiliki misi untuk mengenalkan Islam yang kerap disalahpahami di Australia.

Ketika Iqbal di sana, kegiatan buka bersama dan salat tarawih itu adalah acara Ramadan pertama yang diadakan sang imam di gereja tersebut. Sebelum bikin dahi berkerenyit, ibadah umat Islam dilakukan di sebuah aula yang terpisah dari gereja utama. Dan ini adalah hasil kompromi dari kedua pimpinan agama agar etika tetap terjaga.

Cerita tak selalu lempeng. Penulis membahas topik soal Dimas Kanjeng dan Karomah dengan cara lebih menarik lagi. Ia menarasikan sebuah cerita, pengikut Dimas Kanjeng tengah berbincang dengan syeikh, guru penulis. Intinya, karomah bisa terjadi tapi hanya pada lingkup personal dan tidak akan terjadi pada skala masif. Contoh konkritnya, setelah sedekah langsung mendapatkan ganti berkali-kali lipat.

Esai bagus lainnya juga dapat ditemukan pada pembahasan maksud "liberal" yang mudah dituduhkan kepada diri penulis dan orang lain. Pak Quraish Shihab misalnya. Ini dapat ditemukan pada judul _Oh, Betapa Indahnya Jadi Muslim Liberal_.

Di buku ini, Iqbal Aji Daryono juga menyinggung orang-orang seprofesinya. Terutama penulis songong yang mudah meremehkan kemampuan orang lain. Di samping itu, saya jadi tahu kisaran persentase keuntungan yang didapatkan penulis dan penerbit dari buku yang terjual. Cek saja di _Halah, Cuma Penulis Aja Kok Nggaya_.

Dalam esainya, Iqbal juga menyentil status Raja Mataram. Menarik ini. Logis pula, haha.

Ada juga pengetahuan mengenai peran sopir (_driver_) di Australia dan bagaimana konstruksi sosial mampu membedakan strata sosial mereka dengan para sopir di Indonesia. Bahasan isu bahkan terbang sampai "pertengkaran" antara pendukung dan penolak rokok serta soal boikot-boikotan.

Penyampaian ala esai yang ringan dapat menjadi pintu untuk mengenal banyak isu. Kemudian mengajak pembacanya untuk berpikir serta memancing untuk setuju atau menolak. Memancing pertukaran ide. Begitulah saya melihat tulisan Iqbal Aji Daryono di buku ini. Bagi saya, tulisan ini juga dapat menjadi referensi untuk menulis esai dengan gaya yang menarik!

* * *

**Judul**: Out of the Lunch Box  
**Penulis**: Iqbal Aji Daryono  
**Penerbit**: Shira Media  
**Tebal**: xvi + 244 halaman
