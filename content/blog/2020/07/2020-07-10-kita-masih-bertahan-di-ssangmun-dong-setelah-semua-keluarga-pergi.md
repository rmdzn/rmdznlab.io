---
title: "Kita Masih Bertahan di Ssangmun-dong Setelah Semua Keluarga Pergi"
date: "2020-07-10"
categories: 
  - "cuap"
tags: 
  - "cuap"
  - "drakor"
coverImage: "reply-1988-rmdzn.png"
aliases:
  - /post/2020-07-10-kita-masih-bertahan-di-ssangmun-dong-setelah-semua-keluarga-pergi/
---

Sampai pertengahan tahun kembar pertama era milenia, lumayan banyak hal yang saya perkenalkan kepada diri sendiri. Yang bikin pikiran kencang sampai yang bikin kendor. Cerocosan saya pada hal-hal serius, sebagian sudah saya utarakan sebelumnya. Untuk saat ini saya ingin menulis yang ringan-ringan lagi.

Kalian masih ingat peran orang dewasa di bumi? Yak, menipu orang yang tak tahu. Bulan lalu saya curhat habis-habisan tentang info yang saya konsumsi sampai alam bawah sadar bahwa [Star Wars](/blog/2020/06/2020-06-12-star-wars-dan-kepercayaan/) tidak menyenangkan. Kemudian saya buktikan hal sebaliknya.

Sejak dulu sampai beberapa waktu lalu, saya kerap disodori argumen serupa yang menyasar drama korea (drakor). Tapi yang bilang orang lain, bukan kakak saya. Katanya, film Korea yang bagus hanyalah film laga atau yang sejenisnya. Ia mencontohkan _Train to Busan_, _Ajeossi (The Man from Nowhere)_, dan _Gamgi (Flu)_. Drakor tidak asyik, hanya menye-menye.

Bolehlah kalau dibilang menye-menye meskipun itu relatif, tapi kalau dibilang tidak asyik, belum tentu, _Palguso_. Drakor juga seperti film dan serial TV luar negeri lainnya, tentu ada yang tidak bagus dan ada yang bagus. Bedanya kalau genre drama, saya yakin Korea Selatan adalah salah satu juaranya.

Awal Juni kemarin saya mulai menonton serial TV drakor yang sedang sering dibahas di jagat Twitter, walau sebetulnya drakor tersebut sudah ada sejak tahun 2015. Ia adalah _Reply 1988_. Tak sedikit yang memberikan testimoni bahwa drakor ini bagus. Layak buat ditonton.

Memangnya seberapa layak dan bagus?

Saya tak ingin membahas detail film ini toh sudah banyak orang yang mengulasnya di luar sana.

_Reply 1988_ bukanlah cerita yang menitikberatkan pada kisah percintaan dan mungkin perselingkuhan seperti drama baru _The World of the Married_. _Reply 1988_ merupakan drama yang menayangkan kilas balik tahun 1988 di Ssangmun-dong--wilayah setingkat RW di Kota Seoul--dan bercerita tentang kehidupan persahabatan dan keluarga. Anak sekolah, mahasiswa, dan orang tua dengan beragam masalahnya.

Persahabatan dengan beragam sifat. Cuek, peduli, _ndlogok_ (ini kalau bahasa Indonesia apa ya? Gila? Gokil?), ngeyel, diam menghanyutkan, dsb. Semua tertuang pada karakter masing-masing. Terbentuk sejak kanak-kanak. Lekat. Jangan heran kalau penonton merasa baper dengan persahabatan yang diperlihatkan di drama ini. Apalagi dibumbui kisah cinta segitiga yang menyebalkan.

Para orang tua di sana tak ubahnya orang tua pada umumnya. Rindu dengan orang tuanya sendiri. Merasa cemas jika anaknya kenapa-kenapa. Merasa marah jika anaknya membangkang. Merasa kecewa kalau anaknya gagal--digambarkan dengan lapang dada yang begitu epik. Dan bagaimana rasa galau orang tua karena anaknya sudah beranjak dewasa sehingga tidak lagi bergantung dengan mereka dan tak akan sering bertemu mereka di rumah.

Hubungan antar anggota keluarga di sini diceritakan dengan realistis. Orang tua yang tidak terlalu dekat dengan anaknya iri dengan orang tua lain yang mengalami hal sebaliknya. Tentu saja di sisi lain, nasib satu keluarga dengan keluarga lain jomplang.

https://www.instagram.com/p/BAY7-BUO2DM/

Sekitar tahun 1988 situasi negara dan politik secara langsung dan tidak langsung mempengaruhi penghuni Ssangmun-dong. Penyelenggaraan olimpiade adalah salah satunya. Peristiwa lain, demonstrasi mahasiswa besar-besaran di Korea untuk menuntun penegakan HAM dan antikorupsi begitu juga meledaknya kelahiran bayi sejak tahun 1950 yang memberi efek terduga; perebutan kursi di universitas dan pekerjaan.

_Reply 1988_ membuat saya ingin hidup di sana dan menghadapi problematika sama-sama. _Huh_, tipikal pernyataan "rumput tetangga lebih hijau". Tapi biarkan saya mengungkapkan rasa terdalam dengan segala kejujurannya.

Ada 11 musik film orisinal di sini. Dua terfavorit adalah _Don't Worry, Dear_ yang dinyanyikan oleh Lee Juck dan, tentu saja, _Hyehwa-dong_ yang dinyanyikan oleh Park Boram. Dengarkan saja lalu resapi patah hatinya sekali lagi.

Bila ada yang bertanya: siapa yang bertahan di Ssangmun-dong setelah lima keluarga itu pergi?

Tentu saja, kita.
