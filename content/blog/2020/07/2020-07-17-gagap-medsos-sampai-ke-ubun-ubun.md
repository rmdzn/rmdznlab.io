---
title: "Gagap Medsos Sampai ke Ubun-Ubun"
date: "2020-07-17"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "portrait-of-woman-making-a-face-3958818-bingung-edit-rmdzn.jpg"
aliases:
  - /post/2020-07-17-gagap-medsos-sampai-ke-ubun-ubun/
---

"_Lur, teman perempuanku banyak. Ada yang janda, ada juga yang masih gadis._"

"_Aku kenal artis-artis cantik. Banyak. Ada si anu, anu, dan anu._"

"_Aku juga biasa menghentak Ahok dan biasa diejek dengan panggilan 'kadal gurun'._"

Malam itu, seorang bapak menggebu-gebu bercerita soal pencapaiannya kepada seorang pemuda yang ia anggap cakap.

"_Aku bahkan diajak bertemu dengan seorang gadis Rusia. Aku dikira orang kaya, ya?_" lanjutnya dengan wajah serius.

"_Komentarku juga diberi gini_," kata si bapak melempar senyum dan mengangkat jempolnya.

Pemuda itu mengira si bapak sedang bercanda. Tidak. Bapak itu mengaku baru sekitar dua bulan memegang ponsel pintar. Dibelikan anaknya untuk mengisi waktu luang semasa pensiun. Kalau dipikir-pikir pencapaian tersebut adalah kemajuan pesat!

Selam--tunggu.

Sepertinya ada yang salah. Si pemuda menelisik lebih jauh. Bapak itu sebetulnya tidak paham dengan apa yang ia katakan sendiri. Segala yang ia kemukakan sebagai "teman perempuanku banyak" adalah grup Facebook yang berisi gadis-gadis dan janda, "kenal artis-artis cantik" adalah akun-akun Instagram para artis yang ia ikuti, dan "menghentak Ahok" adalah upayanya ikut berdebat di grup Facebook tentang politik. Dia bahkan merasa tinggi ketika komentarnya diberi jempol oleh akun lain (entah dalam tombol suka atau emoji jempol ke atas).

Bagaimana dengan "diajak bertemu seorang gadis Rusia"? Tidak tahu. Besar kemungkinan, ia dikirimi pesan pribadi oleh sebuah akun dengan foto berwajah bule dan mengaku orang Rusia lalu menyampaikannya dengan bahasa Indonesia bukan bahasa Slavia--aksara _cyrillic_.

Sejak lama saya dibayang-bayangi oleh ujaran: banyak orang tua gagap teknologi. Yang saya pahami adalah orang tua yang tidak paham teknologi--secara teknis. Dan perlu dipahamkan pelan-pelan agar mereka dapat menggunakannya dengan benar.

Gara-gara cerita di atas, saya semakin paham bahwa "gagap" tak berhenti pada penguasaan teknologi secara teknis. Namun juga pada pemahaman konsep dan penggunaannya dengan baik-benar.

Dunia sudah terbagi dua. Dunia nyata dan dunia maya. Bagi yang sudah paham akan dengan sangat mudah membedakan keduanya. Peran satu orang di dunia pertama belum tentu sama dengan dunia kedua. Namun karena dunia kedua tak punya batas dan tak memerlukan tatap muka, ia lebih liar. Penuh kejujuran tapi juga tak sedikit kebohongan. Penuh pujian tapi juga tak sedikit cacian. Penuh keaslian tapi juga tak sedikit tipuan. Penuh kesantunan tapi juga banyak kebrengsekan.

Kegagapan teknologi yang dialami seorang bapak di atas ada di titik ini. Menganggap apa yang dilakukan sampai saat ini lewat ponsel pintarnya adalah pencapaian. Dirinya menganggap bahwa apa yang dilakukannya sangat penting yang secara otomatis menjadikannya orang yang penting.

Oke. Oke. Kemampuan untuk bereksplorasi dengan ponsel pintar dan media sosial adalah pencapaian. Tapi bukan itu yang saya maksud.

Si bapak sebenarnya hanya melakukan hal biasa. Berkomentar. Mengikuti akun orang. Debat tanpa juntrungnya. Sayangnya menganggap diri sebagai hal yang penting ini yang bahaya. Berapa kali kalian melihat orang-orang mencaci di Facebook, Twitter, dan Instagram, terutama pada topik politik? Memaksakan kehendaknya kepada orang lain? Pasti berulangkali. Bahkan sampai bikin bosan.

Saya kadang berpikir orang-orang itu kok ya kurang kerjaan banget. Ribut di sana, tubir di sini. Tapi setelah saya mendengar cerita si bapak, ternyata memang asumsi saya terbukti, meskipun di luar sana ada juga yang menjadi tukang ribut profesional.

Media sosial adalah racun dan orang-orang di dunia nyata adalah sengatnya. Si bapak tidak salah 100%. Orang-orang di sekitarnya, terutama keluarga, bertanggung jawab memberikan pengertian. Hati-hati di media sosial. Begitu. Jangan mudah percaya. Jangan juga reaktif terhadap apapun. Belajar bijak. Minimal berikan pengertian bahwa semua orang bisa melakukan apapun di media sosial. Orang penting dan rakyat jelata, orang berpengetahuan dan orang asal _jeplak_ sama-sama memiliki ruang yang dapat diakses cukup dengan modal paket data.

Memang perlu proses, mengingat subjek yang diajari adalah orang tua … yaa meskipun usia tidak menjamin. Anak-anak muda bebal juga banyak.

Dalam kasus si bapak, yang salah anaknya. Ia bercerita, anaknya melarang membuka kembali usaha bengkel motor. Sebagai kompensasi, anaknya membelikan ponsel pintar dan diajari hal-hal teknis penggunaan. Si anak melepaskan orang tuanya tanpa pengertian apapun soal media sosial. Kurang ajar betul.

Omong-omong, ini adalah solusi jangka pendek saya: menguarkan umpatan kepada anaknya lewat tulisan. Solusi jangka panjang: berikan pemahaman kepada orang tua. Ajari mereka untuk bersikap skeptis terhadap kabar apapun yang didapat. Ini sulit tapi saya yakin bermakna. Saya saja belum tentu bisa.

\[Gambar unggulan oleh **[Polina Zimmerman](https://www.pexels.com/@polina-zimmerman?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/photo/portrait-of-woman-making-a-face-3958818/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
