---
title: "Selamat Ulang Tahun, Jurnal Tersayang!"
date: "2020-07-24"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "blank-composition-desk-journal-606539-jurnal-rmdzn.jpg"
aliases:
  - /post/2020-07-24-selamat-ulang-tahun-jurnal-tersayang/
---

Hari itu hari Kamis. Pagi menuju siang, 25 Juli 2019. Suasana hati sangat kusut. Pikiran terburai walau sedang dikelilingi oleh kubah beton dengan pintu kayu kuat. Di dalam sana, pikiran-pikiran saling berebut mendapatkan perhatian pemiliknya. Bahkan sampai bertarung berdarah-darah.

Kebalikan dari bangsa Viking yang menggedor-gedor benteng dari luar, sebatalyon pikiran mencoba mendobrak pintu kayu dari dalam, memaksa pemiliknya melakukan tindakan bahaya. Pahlawan saya saat itu duduk di pojokan. Merenung bersama yang lainnya. Dia berjalan ke tengah bersama beberapa kawan sambil meyakinkan komplotan barbar, "kami dulu ya yang keluar."

Mereka sangat bersahabat. "Hidup memang tidak selalu baik-baik saja. Saat ini contohnya," kata mereka kepada pemiliknya, serentak meskipun sebagian ada yang berwajah cemberut, sedih, kesal, marah, _bete_, dll.

Ada satu yang mengenakan kaos bertulis **Kenanganmu Dulu 20xx** dengan goresan tahun yang tak jelas. Siangnya, makhluk berkaos itu menjadi inspirasi kuat sekaligus melengkapi pernyataan dari salah satu yang mendaku sebagai pimpinan.

"_Lakukan saja seperti dulu tapi harus dengan kertas dan goresan tanganmu. Sekalian melatih bentuk tulisan barumu, bukan?_"

Klik! Saya menyambar sebuah buku kecil. Hadiah dari sales _marketplace_ daring lokal populer tempat saya pernah menjadi agen. Buku kecil berspiral.

_JOURNAL_.

Begitulah pembukanya.

Sejak saat itu, saya menulis jurnal pribadi. Bukan buku harian dengan "_Dear, diary_"-nya loh, bukan pula buku agenda tempat saya menulis kegiatan yang akan saya lakukan. Jurnal yang menggambarkan perasaan saya terhadap situasi. Terhadap apapun. Saya bahagia, saya marah, saya kesal. Saya juga menuangkan ide-ide yang muncul sekelebat di sana. Tak perlu khawatir dengan penghakiman, peremehan, atau komentar-komentar tak perlu dari orang lain.

Ia juga menjadi ajang untuk mengenal diri sendiri dengan lebih baik. Apa yang perlu saya lakukan jika begini, jika begitu? Mengapa emosi tersebut timbul? Cara terbaik apa yang dapat saya terapkan jika emosi yang sama kembali muncul suatu waktu?

![](/blog/2020/07/images/jurnal-saya-rmdzn.jpg)

Potret jurnal saya

Tentu, dengan konsistensi yang tak kaku. Ada kalanya saya bisa terus-terusan menulis jurnal dalam seminggu. Ada kalanya tidak. Bahkan pernah hanya menulisnya beberapa kali dalam sebulan.

Saya bahagia. Sangat bahagia. Berhasil menemukan wahana terbaik untuk menyalurkan isi pikiran; logika dan emosi secara bersama-sama. Menemukan teman rekat yang tidak ditemukan di mana-mana selain dari diri sendiri. Bahkan mungkin saya bisa semirip Amanatia Junda yang berseloroh merasa menikahi diri sendiri saat berhasil menerbitkan [bukunya](/blog/2020/02/2020-02-14-menyimak-isu-isu-perempuan-lewat-cerpen/). Haha~

Rasa yang bukan pertama kali saya nikmati. Beberapa tahun sebelum kisah itu terjadi, saya melakukan hal serupa. Pikiran saya saat itu kacau berat. Perasaan yang ditimbun sampai hampir meledak. Spontan saja, saya menuliskannya di laptop, dalam bentuk berkas yang saya unggah ke awan. Rasa yang timbul--secara pelan tidak instan--berikutnya adalah lega, tenang, damai, dan nyaman.

Dibandingkan dengan pengalaman pertama, saya lebih memilih menulis jurnal secara manual. Menggores dengan tangan di atas buku. Ada sensasi lebih personal di situ.

Beberapa pekan terakhir, saya kembali menemukan saluran YouTube favorit. [Lana Blakely](https://www.youtube.com/c/LanaBlakely/). Seorang perempuan dari Stockholm, Swedia. Konten utamanya adalah pengenalan dan pengembangan diri. Berdasarkan pengalaman yang ia alami sendiri.

Penyampaiannya yang tenang dan lembut adalah salah satu nilainya. Sedangkan caranya bercerita yang tak menggurui dan tak membumbui dengan kata-kata motivasi bombastis dibarengi dengan pengambilan gambar dan pengeditan video yang oke minta ampun adalah nilai lainnya.

Lana adalah penjurnal sejak pertama kali mampu menulis--usia 8 tahun. Ia masih melakukannya sampai saat ini, 17 tahun dan terus berlangsung. Suasana hati, suasana sekitar, dan peta pikiran (_mindmap_) merupakan sebagian hal yang ia tulis.

Kata Lana, sewaktu-waktu ia dapat membaca jalan pikirannya sewaktu usia 8 tahun, 14 tahun, 20 tahun, dst. Kemudian membayangkan apakah ia memang memikirkan hal seperti yang dituliskan. Mengapa ia berpikir seperti itu? Ada kalanya, tulisan acak miliknya kebetulan tidak ditulis dengan format yang benar sehingga tak terbayang jelas apa maksudnya. "Format" ini adalah tanggal dan jam.

Video-video yang ia unggah selama ini adalah hasil dari pengalihan pikiran dari media tulisan menjadi gambar bergerak. Jadi secara teknis, video-video yang ia buat juga merupakan jurnal itu sendiri.

https://www.youtube.com/watch?v=8gbf4STEEBI

Menulis jurnal adalah keputusan terbaik yang pernah saya lakukan dalam hidup. Ia "memaksa" saya menumpahkan pikiran dengan jujur. Tulisan yang akan memantik suasana saat saya membacanya lagi entah kapan sekaligus melatih saya untuk memilah ide-ide yang sering keluar sembarangan.

Selamat ulang tahun, jurnal!

Seperti Lana Blakely, saya akan terus mengisi dan menyayangimu seumur hidup. Selagi bisa. Sampai mati.

\[Gambar unggulan oleh **[Jessica Lewis](https://www.pexels.com/@thepaintedsquare?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/photo/blank-composition-desk-journal-606539/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
