---
title: "'Petir' yang Bijak-Bijak Komedi"
date: "2020-07-31"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "petir-kover-fix-rmdzn.jpg"
aliases:
  - /post/2020-07-31-petir-yang-bijak-bijak-komedi/
---

Seacak-acaknya suasana, membaca buku adalah salah satu cara untuk melampiaskan fokus pada hal lainnya. Kini giliran judul ketiga dari Supernova yang mulai saya santap beberapa waktu lalu--Petir.

Beda dengan dua judul sebelumnya, [KPBJ](/blog/2019/09/2019-09-15-kesatria-putri-dan-bintang-jatuh-sering-rumit-sering-menarik/) dan [Akar](/blog/2020/02/2020-02-28-akar-bersama-bodhi-dan-buddha/), _Petir_ hanya memiliki 3 Bab/Keping aja. Keping 37 (Kado Hari Jadi), Keping 38 (Petir), dan Keping 39 (Dua Siluet yang Berangkulan). Melihat daftar isinya saja bikin capai. Bayangan saya, _Petir_ tak mengizinkan saya untuk bernafas, terutama pada Keping 38 dan 39 yang berjarak 252 halaman.

Ternyata tidak. _Petir_ tetap mempunyai pembatas berupa poin-poin angka di kanan atas teks. _KPBJ_ dan _Akar_ sebetulnya juga punya, tetapi tidak sebanyak _Petir_.

Novel ini dimulai dengan kemunculan Dimas dan Ruben. Kemudian Gio dan Diva. Kehadiran mereka tak selalu disebut dengan nama, tapi dalam tindakan. Lewat surel yang memberitahu Dimas bahwa Diva menghilang yang kini sedang dicari oleh tim SAR, serta antara Ruben dan Dimas yang saling berkomunikasi.

Cerita utamanya setelah itu. Elektra, perempuan yang tinggal bersama satu kakak perempuan, yang hidupnya tidak jauh-jauh dengan listrik. Dulu bapaknya memang tukang listrik dan menjadikan rumahnya seonggok gudang barang elektronik raksasa; masih rusak maupun yang sudah diperbaiki.

Fenomena-fenomena yang mempertemukan antara hal-hal ghaib dan ilmu pengetahuan menjadikannya selalu menarik. Contoh kasusnya ketika Elektra dianggap kesurupan dan coba disembuhkan lewat gereja yang berakhir gagal. Situasi ini menggambarkan bagaimana pemahaman kolot selalu menganggap situasi abnormal disebabkan oleh hal di luar nalar.

Beruntunglah Elektra, sang ayah alias Wijaya alias Dedi (_daddy_) memarahi Watti (sang kakak) karena membawa adiknya ke gereja. Dedi kekeuh bahwa Elektra terserang ayan/epilepsi bukan karena kuasa gelap atau roh iblis. Sungguh fenomena yang familier. Ceritanya begitu kekal, mampu menceritakan hal serupa sampai sekarang.

![Menikmati serial Supernova lagi. Kali ini 'Petir' yang lebih komedi daripada sebelumnya.](images/photo_2020-07-30_19-19-04.jpg)

Ada kisah sentimen terhadap orang _chinese_ yang tersampaikan lewat pengalaman Elektra sendiri. Sejak SD, ia selalu diolok oleh temannya dengan narasi yang tidak jauh dari "Cina" dan "sipit" secara langsung maupun tidak langsung. Elektra pun memilih diam dan lebih sibuk memikirkan apakah dirinya anak alien karena relasinya dengan listrik dan petir yang tidak umum.

Perempuan berjuluk "Etra" itu sangat berhubungan dengan duniamu yang biasa saja. Lihat, apa yang disampaikan Etra di halaman 33.

"_Bagi Elektra, dunia senantiasa tempat yang aman serta full hiburan. Selalu ada tingkah orang yang bisa kutertawakan dalam hati. Selalu ada yang bisa kukomentari. Ayahku yang jarang ngomong dan Watti yang mulutnya tak bersumpal telah membentukku menjadi seorang penonton bioskop. Cukup menonton. Dan, betapa aku nyaman di kursi gelapku._"

Bapaknya sendiri yang mengajarkan hal itu. Etra menikmati hidup seperti menonton bioskop. Tidak terlalu banyak berperan dan penghindar konflik.

Dalam kehidupan nyata, semua orang pada mulanya hanya penonton. Lalu perlahan-lahan menjadi pemainnya.

Etra mengiyakan. "_Akan tetapi, kursi itu berguncang hebat pada akhirnya. Ternyata hidup tidak membiarkan satu orang pun lolos untuk cuma jadi penonton. Semua harus mencicipi ombak._"

Tokoh utama cerita ini terkesan manusia individualis. Namun entah mengapa dia terlihat _ngangenin_ dan bikin jatuh hati.

Di lain waktu, Etra diperlihatkan sebagai manusia yang tak doyan basa-basi. Ini terlihat saat Bu Sati bertanya kenapa Elektra ingin belajar meditasi.

"_Bagian paling menyebalkan. Kenapa orang-orang harus selalu penasaran dengan kenapa'? Kenapa tidak langsung to-the-point, hajar bleh, dar-der-dor!_" Padahal Elektra sendiri juga bertanya "kenapa" kepada kita semua. Haha.

_Petir_ sungguh komedi. Sudut pandang sebagai Etra yang konyol, termasuk selorohan "_empat lembar foto diri terbaik sepanjang hayat dikandung badan_". Ia bercerita sambil memelesetkannya sebagai lagu. Ada satu lagi, perkataan Etra yang mengejek Watti dan memelesetkannya dengan lirik lagu _Naik Delman_.

Dari sekian karakter yang diperkenalkan, Bu Sati adalah unggulan. Kebijakannya banyak tertuang di sana. Ibu ini memperlakukan tubuh dan rumahnya dengan baik, termasuk bebersih. Ia juga memanjakan diri dengan aroma wangi, lilin temaram, dan wangi alam. Bu Sati mengajarkan kita untuk sibuk menjaga diri dan lingkungan. Sehingga hidup akan selalu terasa damai dan tak membosankan.

Penjelasannya yang tinggi. Kalau di bumi ditarik semua ke level atom, manusia dan makhluk-makhluk di sekitarnya itu sama. Manusia dengan hewan. Manusia dengan pohon-pohon. Manusia dengan batu-batu. Di dunia, manusia tidak memiliki apa-apa. Manusia sebetulnya hanya meminjam tetapi merasa memiliki.

Tentu saja. Akhir cerita Petir sangat menggantung. Perlu ada penjelasan lain mengenai karakter misterius seorang Toni alias Mpret, sang _alpha male_ sekaligus rekan kerja Etra saat membuka dunia bisnis Elektra Pop.

Saya mungkin setuju bahwa Petir adalah seri terlucu di Supernova. Tapi itu perlu saya buktikan dulu dengan membaca lanjutannya; _Partikel_ dan _Gelombang_.

Cao, besok ketemu lagi!

* * *

**Judul**: Petir  
**Penulis**: Dee Lestari  
**Penerbit**: Bentang  
**Tebal**: 280 halaman
