---
title: "Jadi Beda dan Itu Tidak Apa-Apa"
date: "2020-08-07"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "ball-407081_1920-bola-sendiri-rmdzn.jpg"
aliases:
  - /post/2020-08-07-jadi-beda-dan-itu-tidak-apa-apa/
---

Saya memiliki teman sekaligus sepupu perempuan. Panggil saja V. Bertetangga, teman main sejak jadi bocah kecil (bocil), teman TK, dan teman serta kompetitor juara kelas 2 & 3 masa SD.

Zaman bocil, saya kerap ke rumahnya begitu pun sebaliknya. Sepulang sekolah, ibunda V pergi sementara kakak-kakaknya masih sekolah. Saya ikut ke rumahnya tapi lupa apakah memang berniat main ke sana atau mampir sebentar lalu main bareng di tempat lain.

V masuk ke dapur, saya mengikuti dari belakang. Dia mengambil secarik kertas di atas meja makan. Berisi pesan dari sang ibu. Katanya, ibu sedang pergi dan makanan di atas meja harus dimakan untuk makan siang.

Saya iri hampir setengah mati. Sesampainya di rumah, saya bilang ke ibu. _Mbok ibu juga begitu, kalau aku pulang tapi ibu pergi, ibu nulis surat seperti ibunya V_. Ibu saya mengiyakan. Namun, tak sampai 5 kali, ibu saya berhenti. Saya juga tidak protes.

Enam tahun berlalu. Pada masa SMP, saya ingin mempunyai banyak kenalan. Perempuan iya. Laki-laki iya. Tidak harus teman. Pokoknya yang penting kenal. Obsesi yang tak begitu kuat itu saya ambil dari sifat kakak saya yang mudah bergaul. Saya ingin sekali mirip dia. Mudah berkenalan dan bercengkerama.

Saya juga ingin seperti dia ketika dapat membalas ledekan orang lain dengan kata-kata. Saat sesekali saya bercerita kepadanya tentang perundungan yang saya alami, dia mendorong saya untuk membalas dengan ejekan serupa. Membalik dan menghantam dengan untaian kalimat.

Namun nyatanya itu tidak dapat saya lakukan. Saya justru menyiksa diri dengan memaksa mulut untuk menyapa orang asing. Saya juga seperti mendera diri saat memikirkan kata yang akan saya keluarkan sebagai balasan rundungan yang datang. Saya lebih banyak diam. Kalaupun mengejek balik, ejekannya tak menohok. _Cringe_ pula.

Temanku ada yang aneh, kata kakak saya bertahun-tahun kemudian. Saat kami, aku dan beberapa teman, berkumpul di sebuah tempat tongkrongan langganan, semua ramai berbincang ria. Kecuali satu. Ia yang terkenal sebagai anak kurang pergaulan (kuper) diam. Mata dan kepalanya terkantuk-kantuk.

Setelah acara bubar. Aku dan yang lain mampir sebentar ke indekos si kuper. Tebak apa yang dia lakukan? Jarinya bergerak gesit di atas papan tik komputer. Matanya membesar segar. Ia sedang menulis kode pemrograman. Memang gila. Saat nongkrong malah mengantuk, saat di indekos mengerjakan hal yang membosankan malah melek.

Tepat setelah mendengarnya, saya tertawa. Mengolok. Saya masih melakukan hal yang sama saat mendengarnya kembali, tapi tidak untuk sekarang. Kini saya memahami keadaan "si kuper". Bukan, bukan. Bukan dengan cara berbincang langsung dengan orangnya. Saya saja tidak mengenalnya sampai sekarang. Tapi saya berusaha menempatkan diri sebagai orang itu. Di dalam relung hati terdalam, ternyata saya juga akan melakukan hal yang sama. Tentu dengan aktivitas berbeda. Menulis kode pemrograman diganti dengan membaca buku atau bergabung dengan iluminati misalnya.

Itu adalah beberapa dari banyak kisah tentang berusaha mengikuti standar menyenangkan dari sebuah kehangatan dan keuwuan serta standar dunia terekstrovert. Yang ternyata tidak selalu nyaman, tidak cocok dengan keadaan dan kebiasaan. Toh kalau tak melakukan seperti itu juga situasi tetaplah aman dan menggembirakan.

Rasa bahagia memang tak seharusnya berpatokan pada orang lain. Tidak apa-apa kalau caramu berkomunikasi dengan keluarga memang berbeda. Tidak apa-apa kalau kamu memilih diam alih-alih menyerang. Tidak apa-apa kalau kamu mendekam di kamar. Asal menimbulkan rasa bahagia. Kalau rasa bahagia tidak juga muncul, mungkin perlu ada yang perlu diperbaiki. Tapi pelan-pelan dulu saja ya. Jangan terburu-buru.

Saya mungkin jadi gila jika tak merasakan sendiri teori ini. Tapi percayalah, berlatih untuk menerima diri sendiri dan tahu apa yang dibutuhkan oleh psikis adalah anugerah di tengah norma sosial yang terlalu umum dan itu-itu saja.

Sehat-sehatlah lalu yakinkan diri bahwa tidak apa-apa menolak membantu teman kalau situasi diri sedang tidak baik-baik saja, tidak apa-apa menolak telepon video (_vidcall_) jika suasana hati sedang tidak nyaman, tidak apa-apa mendekam seharian di kamar, tidak apa-apa kalau tidak memiliki minat untuk berbincang di warung kopi atau Warmindo, tidak apa-apa menolak ajakan untuk melakukan sesuatu--apapun itu.

Tidak apa-apa kalau hubunganmu dengan keluarga berbeda dengan hubungan kawan-kawanmu dengan keluarga mereka, tidak apa-apa kalau tidak mau _ngobrol_ seharian, tidak apa-apa menangis--tumpahkan saja. Tidak apa-apa. Tidak apa-apa.

Tidak apa-apa.

\[Gambar oleh [SplitShire](https://pixabay.com/users/SplitShire-364019/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=407081) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=407081)\]
