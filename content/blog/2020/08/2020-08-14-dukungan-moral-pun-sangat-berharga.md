---
title: "Dukungan Moral pun Sangat Berharga"
date: "2020-08-14"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "face-mask-5128855_1920-masker-rmdzn.jpg"
aliases:
  - /post/2020-08-14-dukungan-moral-pun-sangat-berharga/
---

Ini sudah Agustus, ya. Sungguh, tahun 2020 berlalu sangat cepat.

Bulan ini seharusnya kita sedang menikmati keramaian; anak-anak kecil berlarian dengan sendok dan kelereng di mulut. Berlarian sambil mengurung sebagian tubuh dengan karung goni. Serta berdiri tegak di bawah kerupuk yang digantung dengan tali.

Di lapangan, seharusnya kita juga dapat menonton pemuda memanjat tiang dengan belepotan oli dan lumpur, para bapak yang bertanding sepak bola sambil mengenakan torong bensin di kepala, dan para ibu yang bermain bola voli atau gobak sodor.

Saya rindu dengan suasana itu. Rindu dengan pena dan kertas di tangan. Lalu mencatat siapa saja yang menang untuk kemudian diberi hadiah pada malam 17-an.

Sebagai pengagum salah satu filosofi yang digaungkan oleh Zeno of Citium, situasi ini sebetulnya sudah saya antisipasi jauh-jauh hari sehingga tak begitu mengagetkan dan mengecewakan. Tapi tentu saja setelah melihat fenomena/kejadian yang terus muncul, meme di bawah benar-benar tak terelakkan.

![](/blog/2020/08/images/holyfck-meme-dicrop-rmdzn.jpg)

Saya tahu ini membosankan, jadi saya mohon maaf. Saya ingin membahas seputaran korona alias Covid-19 lagi setelah jeda sejak [Juni lalu](/blog/2020/06/2020-06-19-juni-yang-tak-tertebak/).

Di media sosial terutama Twitter, bahaya Covid-19 sangat terasa. Saya bisa "bertemu" dokter [Dirga Rambe](https://twitter.com/dirgarambe), [Adinda Falla](https://twitter.com/falla_adinda), [Amelia Martira](https://twitter.com/irasjafii), [Jaka Pradipta](https://twitter.com/jcowacko/), dan lainnya yang menjelaskan tentang tidak remehnya penyakit baru yang sedang melanda dunia. Begitu juga dokter Jiemi Ardian yang pernah menyoal kesehatan mental masa pandemi korona.

Di media sosial, saya juga dapat melihat solidnya rasa untuk saling membantu. [Mas Banu](https://twitter.com/banumelody) yang pernah mencetak poster-poster waspada korona dari Kemenkes untuk dibagikan gratis kepada yang berminat. Maret lalu, saya langsung menghubungi beliau untuk meminta poster dan saya tempel di sudut-sudut kampung. Ada juga [Pak Lantip](https://twitter.com/lantip) dan [Pak Budhi](https://twitter.com/budhihermanto) yang menggagas [Mamajahit](https://mamajahit.id/)\--proyek para penjahit menyediakan Alat Perlindungan Diri (APD) untuk para tenaga kesehatan berbasis komunitas dengan pendanaan melalui donasi sukarela.

Di media sosial, saya menemukan [Ghina Ghaliya](https://twitter.com/ghinaghaliya/status/1285517175772418050) dkk yang mengumpulkan ponsel-ponsel bekas dari warganet untuk dibagikan kepada keluarga yang tak mampu menyediakan ponsel untuk anaknya melakukan Pembelajaran Jarak Jauh (PJJ).

https://twitter.com/ghinaghaliya/status/1285517175772418050

Kita dibikin selalu sadar bahwa hari ini sedang tidak baik-baik saja. Kita dibuat waspada. Kita diajak selalu patuh dengan protokol kesehatan dan peka terhadap beberapa masalah yang muncul semasa wabah. Secara langsung maupun tidak langsung sama-sama menguatkan.

Sejak Covid-19 tiba di Indonesia, kampung tempat saya tinggal mengadakan penyemprotan disinfektan satu minggu sekali. Menyemprot rumah warga, terutama bagian depan; pagar, gerbang, dan pintu serta lokasi umum yang berpotensi sering dipegang; tempat duduk, tiang listrik, gardu Siskamling, dsb. Sekitar dua kali penyemprotan dipegang oleh bapak-bapak, Karang Taruna mengambil alih kemudian.

Ternyata, dunia nyata tak senyaman dunia maya. Tiga bulan pertama, warga sangat mendukung program penyemprotan. Setidaknya mendukung dengan cara diam. Namun akhir-akhir ini satu atau dua warga yang mulai meyakini bahwa Covid-19 adalah konspirasi, yang berkata virusnya tidak ada, mulai berkoar-koar dan menyepelekan program ini. Hal ini juga didukung oleh fakta bahwa dalam satu padukuhan yang masih rutin menyemprot hanya kampung kami. Kami pun sempat goyah.

Akhirnya yang terjadi hanyalah diskusi internal. _Apakah kita masih akan melanjutkan program ini?_ Kata sang ketua. Beberapa anggota Karang Taruna mengusulkan untuk tetap jalan. Beruntung sekali, program penyemprotan akhirnya tetap jalan.

Boleh dikata ini mengkhawatirkan. Di dunia daring, kita bisa mendapatkan semangat serupa dengan mudah untuk sama-sama menyelamatkan diri dan lingkungan. Tapi tidak dengan dunia luring. Ingin tetap bertindak secara sederhana pun perlu berdarah-darah lebih dahulu.

Kita sudah tak bisa lagi meremehkan dukungan meski hanya sekecap bibir. Khususnya dari warga.

Kepada pembaca tulisan ini, jika ada aktivitas individu atau kelompok di kampung tempatmu tinggal yang bertujuan menanggulangi penyebaran Covid-19, mohon dekati dia/mereka. Jangan diam. Tolong sampaikan dukungan. Lewat WhatsApp, media _chatting_ lain, atau temui secara langsung. Minimal dukungan moral. Dukunganmu berjuta-juta lipat sangat berharga.

Jangan sampai keabaian menjebak seluruh kampung. Setidaknya, aktivitas-aktivitas seperti penyemprotan rutin yang cukup berdampak dan dapat menjadi penanda bahwa lingkungan masih belum baik-baik saja. Tanda bahwa kita sama-sama harus waspada.

\[Gambar unggulan oleh [pasja1000](https://pixabay.com/users/pasja1000-6355831/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=5128855) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=5128855)\]
