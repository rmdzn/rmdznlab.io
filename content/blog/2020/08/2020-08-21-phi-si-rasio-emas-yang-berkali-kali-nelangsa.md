---
title: "PHI, Si Rasio Emas yang Berkali-kali Nelangsa"
date: "2020-08-21"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "photo_2020-08-20_22-04-35.jpg"
aliases:
  - /post/2020-08-21-phi-si-rasio-emas-yang-berkali-kali-nelangsa/
---

> Hati-hati dengan ulasan ini. Jangan meneruskan membaca jika kamu ingin sekali membaca buku PHI.

Sebentar. Dari kamu, saya alihkan tatapan ke buku dahulu.

PHI.

Buku yang selesai saya baca beberapa hari lalu. Sebuah novel karya Pringadi Abdi Surya. Nama yang bikin saya tidak menyangka, penulis yang esai-esainya beberapa kali saya baca ternyata menulis novel. Malahan sejak tahun 2018. Kuper betul saya.

Tak ada bayangan apapun tentang _PHI_. Saya tak berniat menduga-duga sekaligus mengantisipasi perasaan apa yang timbul saat membaca buku ini. Saya ingin ia menjadi kejutan sepenuhnya. Dengan prinsip itu, saya tak membaca sinopsisnya sama sekali.

Impresi awal saat membaca beberapa halaman Akson (kata yang dipakai penulis untuk menyebut Bab) dan Hipotesis adalah _PHI_ memiliki cerita yang ringan dan mampu dinikmati dengan mudah. Pun sepertinya, akan banyak teori-teori ilmu pengetahuan yang dipampangkan, yang membuat kata "dinikmati" ini menjadi semakin relatif. Dalam kasus tertentu membosankan.

Pembahasan ilmu pengetahuan dimulai dari teori mendel yang disampaikan di Akson 2. Ilmu dalam fisika, matematika, bahkan hingga filsafat--Descartes, Sartre--dapat ditemukan di akson-akson berikutnya. Penjelasan tentang "phi" dan apa yang membedakannya dengan "pi" ada. Bahkan metafisika semacam _lucid dream_ juga ada.

Beberapa pengetahuan sungguh berfaedah, memancing saya untuk kembali memasukkan info baru ke dalam otak, termasuk soal jus dan susu yang hanya akan menyisakan lemak. Jadi, kalau mau sehat, cukup makan buahnya. Jangan campur susu. Selain nutrisi buah yang tak akan mudah terserap oleh tubuh, susu yang dicampur di dalamnya juga hanya akan meningkatkan lemak tubuh. Padahal sup buah itu seger tahu. Ada juga pengetahuan umum yang tersaji dengan baik; tentang Liga Indonesia, klub bola Sriwijaya FC khususnya.

Penulis menyisipkan cerita anime, drama Korea, dan lainnya, sebagai pembanding kisah yang dialami oleh tokoh Phi. Referensi judul yang begitu banyak membuat saya membayangkan tontonan penulis yang memang bejibun. Secara tidak langsung, _PHI_ juga dapat menjadi sumber referensi bagi kamu yang sedang mencari judul-judul lawas drama Korea.

_PHI_ menyindir ragam fenomena di negeri ini, termasuk fungsi Bimbingan Konseling (BK) di sekolah yang tidak berjalan semestinya. BK yang seharusnya menjadi fasilitas siswa untuk mengembangkan kemampuannya justru hanya berhenti menjadi sarana pelampiasan ketidakpuasan guru terhadap murid-murid yang dianggap ngeyel dan bandel.

Selain itu juga ketidaknormalan sistem pendidikan yang hanya menitikberatkan pada nilai akhir. Hingga membuat pihak sekolah melakukan hal-hal licik saat masa Ujian Nasional (UN) berlangsung. Saya jadi ingat. Pada suatu malam Minggu, tetangga saya membahas bagaimana pergerakan sekolah mereka masa UN. Salah satunya mengaku menjadi saksi bahkan pelaku kecurangan itu sendiri--jual beli kunci, membantu mengerjakan soal, dsb. Parah-parah. Lagi-lagi saya membuktikan bahwa diri ini memang kuper luar biasa.

Dari sudut pandang yang lebih luas, Phi menceritakan hubungan buku dan hujan. Hanya di Indonesia, orang melindungi kepala dari hujan dengan buku. Di negeri lain, orang-orang justru melindungi buku.

"_Buku-buku jauh lebih berharga dari kehidupan mereka sendiri. Buku-buku dapat diwariskan. Manusia, belum tentu. Kenangan terbatas hanya milik beberapa orang atau bahkan hanya milik kita sendiri._ _Karena itu, suatu hari aku akan menulis buku, sebagai upaya menjaga kenangan itu dan mewariskannya kepada lebih banyak orang._"

Tidak hanya itu, sindiran atau kritikan juga dialamatkan kepada petinggi negeri. Dalam bab khusus yang disebutkan sebagai cerpen karya Phi untuk mengikuti lomba, seseorang sedang mencari uang dari Tuhan di sebuah instansi negeri. Padahal negara bukanlah Tuhan yang dapat memberikan uang kepada pihak yang benar-benar membutuhkan. Ini jadi sindiran tervaforit dalam buku ini.

Saya menunggu kejutan. Sebenarnya tentang apa sih _PHI_? Mengapa ceritanya juga begitu lambat? Pada Akson 4 dan 5, pola cerita lumayan terjawab. Phi sedang menceritakan pengalamannya saat ini. Kemudian mengumpamakan/mengingat situasi lampau yang mirip dengannya. Sehingga terjadilah kilas balik berulang kali. Bahkan ada kilas balik di dalam kilas balik itu sendiri. Hal ini ternyata digambarkan dalam subjudul buku: _Hidup adalah perkara mengatasi kenangan demi kenangan_.

Apakah saya sedemikian benar?

![Buku PHI](/blog/2020/08/images/photo_2020-08-20_22-04-35.jpg)

PHI yang saya ajak jalan-jalan pagi-pagi

Saya cukup mantap dengan asumsi saya, sebelum akhirnya muncul karakter Sakum dan kenangan-kenangan yang muncul berikutnya. _PHI_ ini tentang apa? Apakah kisah cinta belaka? Kehidupan manusia normal? Atau manusia spesial dengan kekuatan khusus? Pertanyaan terakhir tergambar pada kecerdasan Phi dan kemampuannya menemukan perempuan di dunia nyata yang sama seperti yang ada di mimpinya.

Di balik kecerdasannya, pemikiran misoginis Phi cukup mengganggu. Ia tidak mau kalah dari perempuan. Dia juga tidak setuju dengan presiden perempuan. Bahkan ia menyebut keangkuhannya untuk tidak menyatakan perasaannya terlebih dahulu ke perempuan adalah sebuah kebanggaan (_pride_). Kebanggaan yang sama yang menyebabkan iblis masuk neraka dan Qabil membunuh Habil.

Banyak hal cantik dan menarik yang disampaikan di sini, saat penulis menggambarkan hujan pada awal Akson 4 dan saat Phi menganggap kamar mandi sebagai ruang kediktatoran. Untuk yang kedua, Phi adalah pemimpinnya sedangkan sabun, pasta gigi, sikat gigi, dan alat-alat lain di ruang tersebut adalah rakyatnya yang tak boleh membantah.

Kebetulan, detik saat saya membaca satu halaman, perasaan saya sedang kacau. Sungguh berhubungan dengan situasi. "_Pada saat kita merasa begitu sepi, di saat itu pula sebenarnya kita memahami cinta._" Bukan cinta yang begitu lho ya, tetapi cinta untuk saling memahami. Terserah dalam bentuk relasi apa. Secara kebetulan pula, saat itu saya juga sedang senang mendengarkan lagu terbaru Lee Hi berjudul _HOLO_. Yang memiliki topik sama. Momennya pas banget.

Perkataan Sakum kerap membuat saya berpikir. Ia sangat cerdas membahasakan rasa suka dan rasa cinta. Membiarkan Phi semakin galau dengan perasaanya--tentu saja terhadap Zane. Saya kira, Phi adalah calon ilmuwan yang mumpuni, ternyata ia hanyalah seorang bucin yang _creepy_.

Semakin akhir. Semakin "menegangkan" dan semakin jernih. Pada cerita awal, waktu sekarang (_present day_) adalah ketika Phi berangkat ke Sumbawa; bersiap ke bandara, di atas kapal terbang, lalu menjejak tanah dan bekerja di kantor. Ternyata aslinya tak sesederhana itu. PHI berhubungan dengan masa lalu, masa sekarang, dan masa depan. Serta masa kini tak selalu masa kini.

Bagi yang tak terbiasa menikmati cerita yang berhubungan dengan waktu, _PHI_ mungkin akan membingungkan. Tapi berbeda dengan Interstellar yang melibatkan sains dengan rumitnya penjelasan, Phi yang mampu melompat-lompat ke masa lalu adalah Phi dalam bentuk pikiran/kesadaran, bukan fisik. Saya kagum dengan imajinasi Pringadi soal ini.

Phi mampu pergi ke masa lalu kemudian mengubah masa depan. Itu bayangan saya. Tapi ternyata tidak. Meskipun cara kembali Phi ke masa lalu dengan cara unik, dunia Phi ternyata mengenal dunia paralel. Saat Phi memiliki nasib buruk di dunia X, ia melompat ke masa lalu untuk memperbaiki nasib buruk di sana, padahal Phi hanya akan merealisasikan realitas baru dengan membentuk dunia Y. Dunia X masih ada dengan Phi yang masih nelangsa.

Sakum memang kurang ajar. Ia mempermainkan Phi yang sedang "menikmati" hidup. Dunia Phi sungguh remuk. Dia sudah merasakan sebagian dunia paralel dan terus mengingat kenangan yang ada di masing-masing dunia. Kenangan yang penuh kehilangan dan luka.

Atau jangan-jangan Phi tidak kemana-mana dan dia sebenarnya gila?

_PHI_ melahirkan cara segar dalam memandang cerita. Meskipun bertema umum; soal cinta, petualangan seorang bucin yang bikin _creepy_, _cringe_, dan teori-teori pengetahuan yang membosankan, _PHI_ memberikan celah menarik untuk dinikmati dan membuat pembacanya berpikir, "ini maksudnya apa?".

* * *

**Judul**: PHI  
**Penulis**: Pringadi Abdi Surya  
**Penerbit**: Shira Media  
**Tebal**: 368
