---
title: "Selamat Datang, Mbak!"
date: "2020-08-28"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "panggung-akad-rmdzn.jpg"
aliases:
  - /post/2020-08-28-selamat-datang-mbak/
---

Dua puluh tahun lebih, saya hidup dengan satu saudara laki-laki. Sehingga lebih sering mengucap _Mas_ dibanding panggilan lain. Alternatifnya adalah _Dab_ yang dalam bahasa _walikan_ juga memiliki arti yang sama. Bercanda dengan canda internal ala-ala dan berkelahi dengan cara maskulin.

"Maskulin" adalah sebutan untuk tindakan melempar koin 100 rupiah ke kepala kakak saya belasan tahun lalu dan lengan atas saya yang lebam karena dipukulnya. Bukan dipukul dengan kemarahan, tapi dipukul dengan niat iseng. Seperti saat tubuh saya dijungkir kemudian gigi depan mementok ubin lantai.

Bertahun-tahun kemudian, tiga adik ibu saya (_bulik/paklik_) menikah. Lima laki-laki _mbrojol_ dari liang rahim mereka sendiri atau istrinya. Tapi ada satu perempuan yang mengintip. Lahir di tengah kumpulan putra yang gersang. Ikut menyemarakkan keluarga besar dari pihak ibu.

Anak lucu berambut bob lurus itu menjadi adik sepupu perempuan dengan garis keluarga terdekat pertama saya. Selamat datang. Selamat datang.

Saudara dari garis keluarga kakek dari bapak saya tak sebanyak saudara dari garis keluarga kakek dari ibu. Hanya satu yang masih sehat sampai sekarang. Pakde alias kakak bapak saya yang hanya memiliki satu anak. Laki-laki (beliau meninggal beberapa bulan lalu). Jika ditarik garis generasi ketiga dari kakek nenek saya, istri dari anak pakde saya adalah kakak perempuan pertama saya.

Namun, karena pakde saya tinggal di Ibu Kota dan hanya mampir sesekali ke rumah kami, relasi saya dan kakak saya tidak begitu dekat dengan mereka. Alhasil ikatan antara saya dengan anak pakde; kakak laki-laki dan istrinya (kakak perempuan pertama saya) juga sama-sama tidak dekat.

Jadi, seperempat abad lebih sedikit hidup di dunia ini, saya sama sekali tidak tahu bagaimana rasanya memiliki kakak perempuan.

Tahun kembar abad ke-20 dimulai. Kakak saya yang lama dikejar dengan pertanyaan _kapan nikah?_ akhirnya melabuhkan hatinya kepada seseorang. Bulan Agustus ini dia melangsungkan pernikahan di sebuah lokasi yang cukup terbuka dengan tamu-tamu yang jumlahnya terbatas. Tahu kan, pandemi ini benar-benar merepotkan kita semua.

Perempuan bertudung dari tengah Pulau Jawa itu telah resmi menjadi kakak saya. Kakak perempuan pertama dengan garis keturunan terdekat sekaligus menjadi kakak perempuan tertua kedua di generasi ketiga keluarga. Selamat datang. Selamat datang!

Selamat datang di keluarga yang normal-tapi-gimana. Akan banyak adik sepupu baru yang diperkenalkan kepadamu. Ditambah _prunan_ yang berjalur agak jauh tapi rumahnya dempet di belakang yang ada bengkelnya itu. Saya pun seperti itu. Saya juga akan mendapatkan kakak dan adik sepupu baru. Plus ponakan-ponakan lucu.

Titip kakak saya yang kadang menyebalkan minta ampun. Yang juga bisa diajak gila-gilaan bareng sekaligus bisa diajak diskusi serius. Kalau kalian sempat, buat saja lampu sokle yang besar dengan gambar hati melingkari siluet dua kepiting kembar. Sorotkan ke atas jika mbak perlu bantuan. Saya yakin, kakak saya yang berkumis itu bisa diandalkan. Pagi, siang, maupun malam.

Jangan bingung dengan urusan panggilan. Saya akan tetap memanggilmu dengan _mbak_, bukan dengan _yu_, _ukhti_, _madame_, maupun _senora_. Saya juga tidak keberatan kalau dipanggil balik dengan _monsieur_ atau _senor_.

Berhubung daripada _nganggur_ tak karuan kala menjaga mas kawin di lahan parkir sekaligus tempat akad pagi itu, saya menulis puisi ini untuk kalian.

![](/blog/2020/08/images/cut2.png)

Sebelum beberapa jam kemudian foto ini tercipta dengan sengaja.

![Foto setelah akad nikah](/blog/2020/08/images/photo_2020-08-27_14-04-54.jpg)

Setelah akad nikah

Tongkat estafet akan terus bergilir. Kini saya yang akan lebih intens mendapatkan pertanyaan membosankan dari para pemuja perkembangbiakan.

_Kapan nikah?_
