---
title: "Merengek Sampai Bahagia"
date: "2020-09-04"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "smiley-2979107_1920-emot-senyum-marah-rmdzn.jpg"
aliases:
  - /post/2020-09-04-merengek-sampai-bahagia/
---

Seorang anak kecil ingin membeli es tungtung yang lewat depan rumah dengan gerobak birunya. _Jangan, Dik, nanti pilek_, kata si ibu yang sedari tadi digondeli si kecil sambil menunjuk-nunjuk penjual es sejak dari kejauhan.

Keinginan yang tak disegerakan membuat si kecil mutung, hilang kendali. Ia menggelesot di lantai. Dan menangis sejadi-jadinya. Orang-orang bilang … tantrum.

Mengingat rasa bahagia saat masih menjadi anak kecil adalah hal yang sulit. Apalagi mengingat pengorbanan apa yang dilakukan untuk mendapatkannya. Saya pun tidak dapat membayangkan, apa yang saya pikirkan jika saya menjadi anak itu. Apa yang saya rasakan ketika es tungtung berhasil saya genggam dan jilati sepuasnya.

Namun, saat beranjak dewasa saya melihat bagaimana polah mereka mendefinisikan kebahagiaan. Minimal dapat tertawa lepas atau kalau sedang kelebihan tenaga, berlarian dan berteriak. Sedangkan pengorbanan yang mereka lakukan adalah suara yang serak, sesenggukan, dan rengekan yang melelahkan.

Di luar konteks baik buruknya pengasuhan anak saat mereka tantrum, kebahagiaan anak kecil saat mendapatkan es tungtungnya adalah kebahagiaan mutlak. Ia benar-benar terjadi. Yaa, meskipun beberapa jam setelah itu mereka melupakannya. Berikutnya sibuk dengan keinginan-keinginan lain.

Lalu, bagaimana orang dewasa berbahagia? Saat seabrek tanggung jawab menggelayuti tubuh dan pikiran; utang belum lunas, karier yang mandek, tenggat waktu tugas yang selalu mengejar, urusan rumah tangga yang tidak kelar-kelar, sambil badannya ditarik-tarik anak kecil yg menginginkan es tungtung.

Berat-beraaat~

Sebetulnya bahagia tak perlu direncanakan. Bahagia adalah _saat_ dan _pasca_. Suatu hal yang terjadi saat ini atau ingatan masa lalu yang dipantik kemudian. Kalau kita merencanakan kebahagiaan, berarti sekarang tidak bahagia, dong. Saya bahagia kalau besok punya motor sendiri, saya bahagia kalau besok punya rumah sendiri, saya bahagia kalau besok diterima lewat SBMPTN, terus saja begitu sampai bangsa Asgard datang ke sini.

Iya, iya … bahagia memang segalanya!

Tapi, apakah kita sebagai manusia seutuhnya juga tidak boleh bersedih? Yaa boleh dong. Bukankah bahagia ada karena kesedihan itu sendiri. Selayak keberadaan bagus karena adanya buruk, panjang karena adanya pendek, dan bersih karena adanya kotor.

Menyadari rasa sedih adalah proses untuk bahagia. Sedih karena masih berada di zona nyaman padahal teman-teman di luar sana berpindah-pindah tempat kerja, sedih karena diri sendiri belum memiliki pasangan, sedih karena es tungtung yang tengah dijilat jatuh ke lantai--err, lupakan.

Setelah menyadari rasa sedih, waktunya memancing dengan logika dan rasa. Bukankah merasa cukup itu menenangkan, membahagiakan? Lalu mengapa saya harus repot-repot keluar dari zona nyaman untuk mencari situasi yang katanya menantang? Padahal penuh ketidakpastian. Bukankah mencintai diri sendiri adalah bahagia itu sendiri? Mengapa repot-repot mengejar pencapaian teman-teman untuk duduk di pelaminan? Kalau kuping panas dan bosan karena tetangga mengejar dengan pertanyaan _kapan kawin_, bacakan saja Kamus Inggris Indonesia karya John M. Echols dan Hassan Shadily. Biarkaaan mereka ikut bosaan, lalu kita tinggalkan.

Selain menyadari rasa sedih dan menerima keadaan diri sendiri, kini giliran meredakan ekspektasi. Harapan yang terlalu tinggi adalah bibir jurang kekecewaan yang sangat dalam. Meredakan ekspektasi sama dengan mengurangi potensi kekecewaan sekaligus meningkatkan potensi kehadiran kebahagiaan.

_Iih_, ribet banget ya? Saya tidak mau terjebak mendefinisikan rasa bahagia agar tak menyesatkan saya pada kesibukan berpikir dan lupa untuk merasakannya.

Saya bukan Merry Riana yang motivatif bombastis. Saya juga bukan Adjie Santosoputro yang dapat memberikan penjelasan rinci seputar sehat mental. Saya hanya mau berbagi apa yang saya rasakan dan pikirkan.

Yang sederhana saja. Fokus pada aktivitas yang sedang dikerjakan. Bahagia itu bisa sekarang. Tak perlu menunggu nanti. Kalau saya _nih_, bahagia ketika dapat menikmati mie instan setelah beberapa hari sebelumnya menghabiskan _jangan nget-ngetan_ (sayur yang dihangatkan berkali-kali). Bahagia ketika membaca buku sambil menyeruput kopi susu. Dan bahagia ketika menumpahkan pikiran pada lembar kertas putih atau blog ini.

Sampai sekarang saya juga masih memiliki rasa iri, tidak aman, minder, yang masih akan menghambat rasa bahagia. Perasaan tersebut saya "nikmati" dulu. Lalu _move on_. Meskipun di waktu mendatang kumat lagi. Tidak apa-apa sih, namanya juga manusia. Yang penting jangan bikin hidup redup seterusnya.

Tidaklah rugi menjadi pembelajar seumur hidup. Kita rasakan bareng. Kita nikmati bareng. Jangan menolak rasa yang datang, sekaligus peka terhadap rasa lain yang mengintip dari dalam. Yang dapat dikeruk dan dikeluarkan pelan-pelan, sedikit, atau sekaligus.

Hari ini, jangan lupa bahagia!

\[Gambar oleh [Gino Crescoli](https://pixabay.com/users/AbsolutVision-6158753/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2979107) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2979107)\]
