---
title: "Cerita yang Tak Pernah Tuntas"
date: "2020-09-11"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-suzy-hazelwood-1995842-cerita-rmdzn.jpg"
aliases:
  - /post/2020-09-11-cerita-yang-tak-pernah-tuntas/
---

Malam hari, melewati Lotte Grosir (dulu Lotte Mart). Saya dibonceng oleh seorang laki-laki yang jika garis keturunan miliknya ditarik, ia masih merupakan saudara supupu saya yang jauh.

"Jaga warung kelontong capai, ya?" Tanyanya.

Mata saya agak berbinar. Saya tidak pernah membayangkan pertanyaan itu meluncur dari mulutnya dan saya juga tidak menyangka bahwa saya mendapatkannya.

Banyak hal yang ingin saya ceritakan kepada orang-orang. Orang-orang penting dan terkasih. Dan jawaban dari pertanyaan itulah salah satunya.

Saat saya membuka jawaban dan masih berada di paragraf pertama, obrolan berbelok tak karuan. Ia justru kembali menuju cerita yang subjeknya adalah dirinya sendiri. Panjang kali lebar.

Lagi-lagi, kandidat orang penting dan terkasih itu tak jadi bertambah. Kembali semula.

Masalah curhat yang tak tersampaikan ini lama-lama kok jadi fenomena umum. Saat mau membuka mulut ada saja halangannya. Entah buat orang yang memang hobi cerita atau orang yang sangat pilah-pilah kepada siapa dia bercerita. Yang kedua, sekali klik dengan orang yang sepertinya tepat untuk dibagi cerita, eh ternyata juga tidak berguna.

Ketika cerita tersampaikan, eh diakhiri kompetisi cerita dari orang lain. Lebih menarik mana. Lebih seru mana. Lebih menyenangkan mana. Lebih nelangsa mana.

Saat mengingat masa lalu, saya tertegun. Bisa-bisanya saya dahulu bergabung dengan organisasi yang menganjurkan keterbukaan. Organisasi yang mengglorifikasi rasa kepemilikan dan kekeluargaan.

Jangan salah sangka. Saya tidak pernah menyesal telah bergabung di organisasi tersebut. Apalagi ia merupakan salah satu penyumbang pencapaian hidup saya sampai saat ini.

Di sebuah acara pertemuan rutin. Duduk bersila melingkar di atas rumput. Saya mengacungkan tangan saat sesi tanya jawab. Saya curhatkan satu masalah yang berhubungan dengan keluarga saya. Sedikit unek-unek. Siapa tahu ada yang merespon.

Apakah ada yang meresponnya?

Tidak ada. HAHAHA~

Entah, apakah karena penyampaian saya yang tidak asyik bahkan _cringe_ atau bagaimana. Saya tidak tahu. Toh kalau saya ingat, pertanyaan saya normal-normal saja. Sejak itu saya kembali ke posisi awal, tak ingin menceritakan apa pun lagi seputar kehidupan pribadi atau sekitarnya. Walau tetap merasa kagum ketika melihat anggota lain yang percaya diri menyampaikan cerita di forum terbuka yang sama.

Dari sini saya membuktikan, keluarga asli saja belum tentu mendengarkan apalagi entitas yang berpura-pura menjadi keluarga. Saya tak mengeneralisasi. Ada juga kok keluarga dan organisasi yang "sempurna". Rangkul dan jangan anggap itu hal biasa, ya!

Di lain hari. "Zi, tolong ajarin ini itu dong," panggil seorang teman lewat WhatsApp beberapa tahun lalu. Dia menganggap saya jago, padahal dialah yang lulus sekolah duluan. Saya sedang _ngos-ngosan_ mengejar ia dan teman-temannya.

"Saya sedang fokus mengerjakan hal lain. Maaf, kamu bisa cari temen lain, ya. Atau lebih baik ikut kursus." Maksud saya, kursus tentang pemrograman yang sedang ia bingungkan.

"Tidak bisa, Zi."

Sore hari berikutnya, dia datang dan mengharapkan ilmu bejibun dari saya. Sambil beberapa kali menertawai karena pekerjaan saya yang tidak kunjung usai.

Pada awalnya saya merasa positif dan gembira. Saya ternyata dianggap pintar dan dapat dipercaya. Tapi kalau dipikir saat _overthinking_, ternyata penolakan saya saat itu tidak didengarkan dan dimengerti. Sekarang, saya lebih baik dianggap pelit ilmu jika terjadi hal serupa di kemudian hari. Saya percaya batasan sikap dapat menstabilkan kesehatan mental saya.

Tenang. Kalau batasan itu cukup dan saya memang berminat membantu teman. Saya akan bantu sebisanya. Kalau sempat, saya akan panggil bala jin Bandawasa yang mungkin kini masih malu karena sempat tertipu kokokan ayam.

Untuk saat ini, memang lebih baik monolog dan bercerita kepadamu saja. Bercerita kepada orang lain belum tentu memuaskan. Tentang apakah kamu mengerti cerita saya atau tidak, itu persoalan lain. Tanyakan saja lewat kolom komentar di bawah. Saya akan segera membalas, tentu dengan pertimbangan kesulitan pertanyaannya.

Kepada manusia-manusia yang tak mengerti indahnya diceritakan dan didengarkan, sekarang mulailah untuk menyicil. Kalau biasanya hanya ingin bercerita, mulai saja untuk belajar mendengarkan. Diam. Lalu berikan respon saat pencerita meminta. Pelan-pelan dulu. Suatu saat nanti ia akan menjadi hal biasa. Dan keahlian untuk diam dan berbicara akan meningkat!

Jadi, "jaga warung kelontong capai, ya?"

\[Gambar unggulan oleh **[Suzy Hazelwood](https://www.pexels.com/@suzyhazelwood?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/photo/black-and-red-typewriter-1995842/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
