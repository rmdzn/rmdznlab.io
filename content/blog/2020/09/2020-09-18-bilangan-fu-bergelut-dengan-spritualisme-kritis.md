---
title: "Bilangan Fu, Bergelut dengan Spritualisme Kritis"
date: "2020-09-18"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "bilangan-fu-ijogja-featured-rmdzn.jpg"
aliases:
  - /post/2020-09-18-bilangan-fu-bergelut-dengan-spritualisme-kritis/
---

Saya lupa. Sejak kapan saya memasukkan buku ini ke dalam kantong keinginan. Yang saya ingat adalah sejak saya belum mengenal aplikasi yang dibikin Perpustakaan Nasional keren itu.

_Bilangan Fu_. Novel karya Ayu Utami yang terbit lebih dari sedekade lalu. Saat membacanya, minimal saat sampai bab-bab awal, saya cukup paham dengan kisah yang disampaikan. Setidaknya tentang seorang pemanjat tebing.

Dasar manusia, karena penilaian pendek itu, saya langsung menghakimi bahwa novel ini akan saya nikmati dengan nyaman dan mudah. Asumsi-asumsi tak perlu yang konyol kadang menyenangkan. Yang kemudian membuat saya berpikir, _apa hubungannya dengan 'spiritualisme-spiritualisme' yang tertulis di sinopsisnya?_

![Buku digital Bilangan Fu](/blog/2020/09/images/bilangan-fu-ijogja-rmdzn.jpg)

Ini buku digital Bilangan Fu dari aplikasi iJogja. Salinan buku ini di iPusnas selalu habis.

Oke. Parang Jati, tokoh penting dalam _Bilangan Fu_, menceritakan dongeng filosofis seputar Watugunung yang disusul kisah tentang penanggalan pada kalender Jawa; pasaran, jumlah hari. Serta yang berkaitan dengan penamaan hari dalam Bahasa Inggris.

Lalu, cerita berikutnya membuat saya menyangka sekaligus tidak dengan kehadiran penjelasan seputar Sewugunung/Watugunung. Bukit tersebut kerap dianggap sebagai simbol kegagahan jika orang-orang mampu memanjat di sana, padahal sang bukit aslinya malah menggambarkan tentang feminitas.

Ia adalah garba raksasa. Pemanjatan di sana merupakan simbol lelaki yang ingin "kembali" ke liang rahim ibunya yang kisahnya tercermin dalam legenda Sangkuriang dan Prabu Watugunung.

Tidak berhenti di situ. Jangan meremehkan segala mitos atau legenda di sekitar kita. Begitu pesan implisit Parang Jati. Cerita-cerita tersebut sebenarnya dapat menggambarkan kejadian lampau. Misalnya di Bandung 160 ribu sampai 16 ribu tahun lalu, kemungkinan besar memang terdapat danau besar yang dalam legenda adalah hasil bendungan sungai oleh Sangkuriang.

Mirip cerita Bandawasa yang diberi syarat untuk membangun seribu candi oleh Rara Jonggrang tetapi kemudian ditipu oleh Rara dengan kokokan ayam palsu, Sangkuriang diberi syarat oleh Dayang Sumbi untuk membendung sungai dan menjadikannya danau dalam semalam serta membuat bahtera di atasnya untuk keduanya bercinta. Namun, Sangkuriang ditipu. Ia marah lalu menendang bahtera dan menjadikan objek tersebut sebagai gunung.

Cerita Prabu Watugunung merupakan derivasi dari dua kisah di atas. Bedanya, sang istri bukanlah ibunya. Ia seenaknya menguji kesetiaan istrinya. Lalu, ketika dirinya mencoba lolos dari ujian yang tak pernah diketahuinya, Prabu Watugunung malah menolak. Sehingga sang istri dikutuk menjadi raksasa menyeramkan.

Intinya mirip. Gaya basi misoginis. Mengapa sang perempuan tidak jujur saja kalau mereka adalah ibunya, daripada melakukan sesuatu yang mencederai dan merusak kehidupan manusia? Mengapa sang perempuan seenaknya "diuji", tapi ketika dia menciptakan solusinya sendiri malah dihakimi?

_Bilangan Fu_ membawa isu keperempuan dengan cara _wah_. Bisa-bisanya diselipkan di sebuah isu sederhana sekaligus komplek. "Perempuan memiliki banyak pesan di belakang layar, tapi mereka tak mendapat tempat di panggung upacara. Ini tetap dunia beradat lelaki, sekalipun orang menghormati Nyai Ratu Kidul." (hal. 141)

Konteks yang memicu pernyataan tersebut adalah Nyi Manyar yang dianggap cakap spiritual. Yang juga mengurus hujan. Namun, tak ditampakkan saat acara seremonial atau upacara tertentu. Seperti Ratu Kidul yang dianggap besar tapi tidak mencerminkan tingkah laku orang-orang dalam memperlakukan perempuan.

Di sini, manusia disindir habis-habisan. Isu keperempuan hanyalah salah satunya. Yang lain adalah soal lingkungan.

"Yaitu, bahwa manusia begitu tamak. Dan bagian dari kerasukan lelaki adalah ingin menaklukan alam, dengan cara memperkosanya. Persis seperti tindakan mereka terhadap perempuan. Mereka memaku, mengebor, memasang segala jerat demi bisa melampaui tebing. \[…\] Dan jika mereka mencapai puncak itu dengan segala kerusakan yang dibuat, betapa dungunya, mereka kira mereka telah berjaya." (hal. 81)

Perkataan tersebut dalam konteks pemanjatan tebing kotor yang kerap dilakukan oleh orang-orang. Cara pemanjatan yang mengebor banyak titik sebagai pengaman. Berbeda dengan kampanye yang didengungkan oleh Parang Jati tentang pemanjatan bersih--seminim mungkin merusak tebing.

Parang Jati sangat menghargai lingkungan. Sedikit demi sedikit, ia mengajak warga desa untuk menolak segala kegiatan penambangan kapur di Watugunung. Penambangan yang akan menyebabkan menyusutnya dan keruhnya suplai air warga.

Perlawannya dari kubu seberang tidak main-main. Penyuapan warga dalam bentuk sponsor pengadaan ritual persembahan sesaji sampai militerisme yang dibenci oleh Jati. Dua metode yang kemungkinan besar masih dilakukan sampai sekarang.

Untuk militerisme, Parang Jati mencontohkan kejadian bertahun-tahun lalu. Isu ninja yang membantai para tertuduh dukun. Tidak tahu siapa yang memulai. Tidak tahu mengapa itu terjadi. Tapi ia berhenti begitu saja, tak ada aba-aba.

Militerisme digaungkan untuk memuluskan jalan. Entah dilakukan oleh militer itu sendiri atau rakyat sipil. Tokoh lain bernama Kupukupu yang kemudian berganti nama menjadi Farisi, justru berseberangan dengan kakaknya, Parang Jati. Farisi dan gerombolannya menjadi simbol militerisme berbaju agamis yang memandang kebaikan secara biner. Benar dan salah. Dan dimanfaatkan oleh perusahaan untuk melawan para penentang perusahaan. Satu orang bersahaja yang wafat diduga sebagai korban dari aksi militerisme ini.

Lagi-lagi. _Bilangan Fu_ menguarkan kritikan tajam kepada yang lainnya. Kali ini kepada penganut monotheisme, atau dalam pernyataan lain penganut agama turunan Semit atau agama langit yang memandang kebenaran pasti.

Pandangan kebenaran pasti tak mengizinkan pandangan lain masuk ke dunia ini. Pandangan lain harus menyingkir. Sikapnya terlihat pada pandangan monotheis terhadap Tuhan yang satu. Padahal "1" sendiri menyimbolkan keterbatasan. Justru bilangan "0" (nol)-lah yang menyampaikan ketidakterbatasan. Bilangan Fu menyimbolkan hal yang seharusnya. Tuhan adalah satu sekaligus nol. Dengan pandangan yang saklek, ia tidak peka terhadap lingkungannya sehingga asal gerak (yang penting kebenaran yang digaungkan tercapai, salah satunya lewat kekuasaan).

Parang Jati sangat mendukung agama bumi. Karena ia lah alam-alam dapat terjaga dan terus lestari. Tapi bukan berarti Jati tak menawarkan solusi berupa pandangan kepada penganut agama langit. Dirinya memperkenalkan istilah _laku kritik_. Spiritualisme kritis.

"Kebenaran biarlah berada di langit. Kelak kita akan mengetahuinya, mesteri itu, ketika waktu kita telah tiba. Tapi hari ini bumi membutuhkan kebaikan kita. Maka marilah kita berbuat baik kepada bumi. Sebab yang di langit itu tidak membutuhkan belas kasih kita." (hal. 454).

Dalam bahasa Jawa, nilai itu tercermin pada kata _eling lan waspada_. Tidak menerima mentah-mentah dan tidak menolak mentah-mentah. Berani hidup dengan menunda kebenaran. Hidup dengan kebenaran tertunda.

Akhir cerita _Bilangan Fu_ terasa sangat personal. Penceritaan memori yang deskriptif membuat semua emosi benar-benar terasa. Mangkel, sedih, sekaligus haru dan bangga.

Novel ini adalah cerita yang disampaikan oleh Yuda. Satu dari trio sekawan sekaligus pasangan; Parang Jati, Yuda, dan Marja. Di beberapa bab, saat membahas topik tertentu, secara unik cerita Yuda melampirkan kliping berita. Sebagai referensi kabar-kabar terdahulu, terutama kabar tentang konflik antara Polri-TNI pasca _lengser keprabon_ (turunnya Presiden Soeharto dari tahtanya).

Meski memusingkan dan melelahkan (saya menghabiskan 11 lembar kertas untuk mencatat segala hal penting dan pendapat saya tentang novel ini), berlembar-lembar halaman _Bilangan Fu_ yang tersantap sangat sepadan dengan apa yang saya dapatkan dan rasakan. Dan ternyata, _Bilangan Fu_ adalah trilogi. Judul kedua: _Manjali dan Cakrabirawa_. Judul ketiga: _Lalita dan Maya_.

Besok lagi. Baca lagi.

* * *

**Judul**: Bilangan Fu  
**Penulis**: Ayu Utami  
**Penerbit**: Kepustakaan Populer Gramedia (KPG)  
**Tebal**: x + 537
