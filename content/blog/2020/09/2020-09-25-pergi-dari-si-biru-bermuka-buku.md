---
title: "Pergi dari Si Biru Bermuka Buku"
date: "2020-09-25"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "penghapus-rmdzn.jpg"
aliases:
  - /post/2020-09-25-pergi-dari-si-biru-bermuka-buku/
---

Tidak mudah pergi dari rumah yang sudah ditinggali bertahun-tahun. Tidak mudah melambaikan tangan kepada orang-orang yang berdiri di bawah satu atap lalu berkata _sampai jumpa_ atau lebih _mellow_ lagi, _selamat tinggal_.

Saya begitu. Tapi bukan rumah fisik dengan penghuni yang dapat dicubit seketika itu juga. Rumah yang saya tinggalkan adalah rumah virtual yang saya diami sejak tahun 2009.

Tahun segitu memang sedang senang-senangnya bikin akun di rumah virtual bernama media sosial. Maklum, inisiatif beberapa pemuda (termasuk kakak saya) untuk patungan langganan Speedy dengan koneksi dibagi-bagi. Dengan kabel LAN yang dijulurkan lebih rendah dari kabel listrik dan telepon, saya jadi bisa ikut merasakan berselancar internet di rumah, tak perlu pergi ke warung internet langganan di kawasan Babarsari.

Forum Kaskus (saat itu masih Kaskus.us) adalah salah satu "media sosial" yang saya kunjungi dan daftarkan diri. Saya lebih sering _nongkrong_ di subforum _Computer Stuff_ dan _The Lounge_. Kadang juga lari ke _Disturbing Picture_ bersama teman sebangku SMA yang sama-sama mendaftarkan akun di sana. Hanya beberapa kali menyambangi satu dua _thread_ langsung tobat menjadi psikopat. Berbeda dengan teman saya yang saat itu masih hobi menonton gambar-gambar kucuran sirup Marjan dan seonggok kue coklat berjari dengan selai stroberi dan _marshmello_ yang menghambur.

Sedikit memutakhirkan kemampuan diri, saya ikut berdiskusi di subforum _Linux dan OS Selain Microsoft & Mac_ dan _Programmer Forum_. Dan dari sekian komentar yang pernah ditulis di Kaskus, paling banyak ada di subforum Linux itu. Saya berhenti aktif pada tahun 2015 dengan "jabatan" _Kaskus Addict_, tapi ID saya masih aman di sana.

Pada tahun yang sama, saya juga bergabung di media sosial yang populer dengan dominasi warna biru dan logo "F"-nya. Bermain _game_ _Rock Legend_ dan setahun kemudian _nimbrung_ ke beberapa grup dengan seketertarikan.

Dinding penuh dengan obrolan. Sungguh _cringe_ kalau saya ingat. Album foto juga penuh dengan penanda muka. Canda-canda berseliweran. Ada yang bertulis normal, ada juga yang bertulis dengan campuran 4n6k4-an9k4.

Saya juga membuat halaman _fanpage_ untuk berbagi pengetahuan--terinspirasi dari _fanpage_ serupa yang sudah ada. Kebetulan, saat itu saya berlangganan banyak situs pengetahuan dan gatal sekali untuk menceritakan ulang dengan ringkas dalam Bahasa Indonesia.

Saya juga membuat halaman _fanpage_ lain. _Fanpage_ organisasi Rohis SMA dan _fanpage_ untuk jualan jam tangan yang bikin pusing saat dibaca. _Fanpage_ pertama sangat laku karena adik kelas yang ditunjuk menjadi admin sungguh paripurna kerennya sedangkan _fanpage_ kedua gagal karena saya tidak paham bagaimana cara memasarkan produk yang saya jual. Yang lain, ada juga _fanpage_ tentang situsweb berita yang saya kelola.

Semua itu masa lalu. Baru kali itu saya seniat-niatnya ingin pergi dari sana. Tak sekadar pergi, tapi menghilangkan jejak sebersih mungkin.

Ada sebuah perasaan tak lagi memiliki pada ruang yang pertama diharapkan sebagai tempat teduh. Harapan yang pertama dan seterusnya. Intensitas interaksi yang tak bernilai pada ruang itu berikut perasaan saya juga yang sedang tercampur aduk, lelah, tak stabil, memantik saya untuk minggat.

Tindakan itu terjadi. Memang bisa menghapus akun Facebook? Bukan cuma deaktivasi lho, yaa.

Setelah berkeliling mencari, saya akhirnya menemukan caranya. Untuk menghapus akun harus dimulai dengan menghapus semua tulisan-tulisan dan gambar yang saya unggah. Entah yang diunggah di dinding sendiri, di dinding orang lain, di _fanpage_, maupun di grup. Saya tidak tahu apakah sekarang masih harus seperti itu atau cukup menekan tombol "Hapus akun" lalu selesai.

Tidak dengan cara manual dong ya. Tapi dengan [ekstensi peramban](/blog/2020/05/2020-05-01-pengaya-addons-firefox-yang-wajib-saya-pasang/). Saya lupa nama ekstensinya. Ia bekerja bagai robot, membuka lembar Aktivitas di akun Facebook lalu memencet tombol hapus secara otomatis. Hanya aktivitas berupa _tag-tag_ foto yang tidak dapat dihapus--hanya bisa disembunyikan (_hide_).

![Pesan hapus akun dari Facebook](/blog/2020/09/images/facebook-pesan-hapus-rmdzn.png)

Pesan hapus akun dari Facebook

Saya baru sadar, ternyata sudah setahun lebih akun Facebook saya tuntas menghilang. Tak ada yang dirindukan kecuali grup panitia penerimaan mahasiswa baru dan foto bertanda muka saya dari kawan-kawan organisasi masa SMA.

![Kepala akun Facebook saya](/blog/2020/09/images/facebook-ramdziana-rmdzn.jpg)

Hanya ini tangkapan layar akun yang pernah saya ambil

Kini saya lebih fokus mengurus media sosial Twitter dan [Mastodon](/blog/2019/03/2019-03-11-mengenal-masalah-media-sosial-dan-sedikit-tentang-dunia-fediverse/). Konsep mikroblog pada media sosial dengan batasan sekian karakter ternyata lebih saya nikmati. Tapi, tentu saja platform tempat saya dapat menulis dengan panjang adalah ruang yang paling saya cintai. Di sini. Ya, di sini.
