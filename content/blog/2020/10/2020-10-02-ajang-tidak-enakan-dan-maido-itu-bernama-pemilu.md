---
title: "Ajang Tidak Enakan dan 'Maido' itu Bernama Pemilu"
date: "2020-10-02"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-element-digital-1550337-votes-rmdzn.jpg"
aliases:
  - /post/2020-10-02-ajang-tidak-enakan-dan-maido-itu-bernama-pemilu/
---

April 2019 lalu, saya menjadi Pengawas Tempat Pemungutan Suara (PTPS). Bergerak di bawah Panitia Pengawas Pemilu (Panwaslu) Kecamatan yang secara struktural di bawah Badan Pengawas Pemilu (Bawaslu). Tugasnya adalah mengawasi TPS dan Kelompok Penyelenggara Pemungutan Suara (KPPS) bekerja. Pra dan saat Pemilu diselenggarakan.

Kalau tidak salah, baru kali itu PTPS dibentuk. Pada Pemilu sebelumnya, pengawasan dilakukan langsung oleh Panwaslu. Itu pun setiap pengawas mengover beberapa TPS. Mereka biasanya berkeliling bersama polisi yang juga ditugaskan untuk menjaga kondusivitas TPS.

PTPS yang dibentuk pada tahun 2019 hanya fokus bekerja di satu TPS. Tak perlu berkeliling ke TPS lain. Sehingga kami diberi kesempatan untuk memperkenalkan diri kepada KPPS setempat secara personal dan bekerja sama sebaik-baiknya dengan mereka.

Saya termasuk beruntung. Saya ditugaskan di TPS di kampung saya sendiri sehingga tak perlu repot-repot berkenalan dengan anggota KPPS, _wong_ sudah kenal dari dulu.

![Kaos PTPS berkerah](/blog/2020/10/images/kaos-kerah-ptsps-bawaslu-rmdzn.jpg)

Kaos PTPS Pemilu 2019

Tugas saat pra acara adalah memeriksa kesiapan TPS berikut memastikan lokasi yang digunakan benar-benar inklusif. Ramah buat semua. Salah satu indikator ramah adalah TPS yang tidak berundak. Ini tercantum di buku panduan milik KPPS maupun PTPS.

Selama ini, lokasi langganan TPS yang dipakai di kampung saya cacat. Ia berundak. Tidak tinggi, hanya sekitar 3 anak tangga, tapi bagi orang-orang sepuh dan difabel belum tentu mudah diakses. Sebelumnya, saya konfirmasi terlebih dahulu kepada salah satu warga sepuh di sana. Bagaimana tanggapan soal TPS berundak seperti itu. Bertanya bukan sebagai warga biasa dan tetangga, tetapi sebagai PTPS yang lengkap dengan _cocard_ di dada.

_Ya, tidak nyaman. Tapi gak apa-apa, saya bisa \[masuk ke TPS\] kok, mas. Saya nanti diantar ke sana sama Pak XXXX._

Begitulah jawaban si ibu yang saya temui sore itu. Khas. Rumah langganan TPS itu selalu digunakan bertahun-tahun lamanya; termasuk TPS Pemilu nasional maupun daerah, sehingga jawaban aman lebih nyaman, menormalisasi tindakan tak nyaman lebih dipilih dibandingkan melontarkan gugatan atas menahunnya kesalahan.

Perlu dicatat dalam hati dan pikiran, ketiadaan orang sepuh dan penyandang disabilitas di kampung bukan berarti meniadakan usaha untuk menjadikan lokasi seinklusif mungkin.

![Syarat TPS inklusif](/blog/2020/10/images/tps-inklusif-rmdzn.jpg)

TPS harus terjamah dengan mudah. Potret panduan buku dari Bawaslu untuk PTPS.

Hanya selang sehari, saya langsung menemui anggota KPPS yang juga tetangga saya.

_"Mas, saya usul, mbok itu tangganya diberi papan, biar warga kayak ibu yyyy naiknya enggak repot."_

_"Alah, enggak usah. Nanti beliau kita bantu \[masuk ke TPS\]. Pengawas kok ngurus begituan?"_

Ternyata idealisme berakhir sebatas jargon, peraturan hanya sebatas teks di kertas. Saya _mingkem_. Diremehkan usulnya. Di-_paido_ tugasnya. Dibandingkan para KPPS yang berusia kepala tiga sampai paruh baya, saya benar-benar paling junior. Dan itu sungguh menyiksa.

Apakah KPPS memang membantu ibu itu masuk ke TPS? Tidak juga. Saat masuk, si ibu naik sendiri berpegang dinding. Sedangkan saat turun ia dibantu oleh bagian keamanan.

Pada siang hari saat pencoblosan berlangsung. Grup WhatsApp PTPS dipenuhi keluhan atas kurangnya surat suara. Tapi PTPS lain melaporkan bahwa di tempatnya justru kelebihan surat suara. Untuk mengatasi hal tersebut, KPPS yang kekurangan surat suara dapat mengunjungi TPS yang kelebihan dan meminta surat dengan tetap mengindahkan prosedur. Yakni, kelebihan dan kekurangan surat dicatat dengan rinci serta proses kunjungan harus ditemani masing-masing PTPS dan beberapa saksi (terserah, dari partai atau dari calon DPD).

TPS di kampung saya kelebihan surat suara, sedangkan TPS kampung sebelah justru kekurangan. Ketika itu, KPPS dari TPS sebelah mengunjungi TPS saya untuk meminta surat suara. Tapi sayang, PTPS dari TPS sebelah tidak ikut mengawasi langsung KPPS yang seharusnya ia awasi.

_"PTPS-nya enggak nemenin, Pak? Harusnya nemenin."_

"_Enggak, mas. Situ saja yang melihat prosesnya, sudah cukup,_" kata ketua KPPS kepada saya.

Padahal di waktu yang sama, saksi partai dari TPS sebelah ikut protes terhadap ketiadaan PTPS. Bahkan marah sampai urat kepalanya terlihat jelas. Saya yang akhirnya mau tidak mau menenangkan beliau sekaligus menyarankan untuk melakukan gugatan prosedural.

Iya saya kenal dengan anggota KPPS TPS sebelah. Saya juga cukup optimis mereka tidak melakukan kecurangan. Namun, mendiskreditkan prosedur seperti itu juga menjadi hal cela. Tidak elok. Suka sebal dengan hal-hal seperti ini. _Huh_.

Pemilihan Kepala Daerah (Pilkada) akhir tahun nanti mungkin tak akan serumit Pemilu serentak kemarin. Mungkin juga tidak ada PTPS, hanya pengawas dari Panwaslu yang mengover beberapa TPS. Saya belum tahu pasti.

Yang pasti, kebiasaan _maido_ (meragukan, menyanggah, dan _ngeyel_) dapat muncul saat hari pencoblosan nanti. Apalagi dengan peraturan keharusan menerapkan protokol kesehatan seketat mungkin. Sikap _maido_ bakal datang dari KPPS-nya sendiri atau pengunjung TPS yang akan menggunakan hak suaranya.

_Maido_ yang datang dalam bentuk tindakan tak menggunakan masker atau menggunakannya tapi dipelorotkan ke dagu atau dipelorotkan sedikit dan memperlihatkan hidung. Sejenisnya. Semacamnya. Diiringi pula dengan sikap tidak enakan dari orang lain untuk menegur personil dan pengunjung yang tak tertib protokol.

Pilkada Desember 2020 mungkin akan menjadi salah satu Pemilu merepotkan sepanjang sejarah. Karena korona, sikap tidak enakan, dan sikap _maido_ banyak-banyak.

\[Gambar unggulan oleh **[Element5 Digital](https://www.pexels.com/@element5?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/photo/person-dropping-paper-on-box-1550337/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
