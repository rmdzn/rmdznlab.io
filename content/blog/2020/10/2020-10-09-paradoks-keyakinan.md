---
title: "Paradoks Keyakinan"
date: "2020-10-09"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "tunnel-4496526_1920-terowongan-rmdzn.jpg"
aliases:
  - /post/2020-10-09-paradoks-keyakinan/
---

Saya sangat percaya bahwa bumi itu datar sebentuk _azimuthal equidistant_. Seperti gambar di logo Persatuan Bangsa-Bangsa (PBB). Teori-teori bumi berbentuk datar sudah dibuktikan oleh banyak orang.

Orang-orang harus percaya, bumi bulat adalah pengetahuan palsu untuk membodohi publik. Orang yang percaya dengan tipuan itu sangat mudah diperdaya dan akan menjadi babu elit penguasa global seumur hidupnya.

…

Saya makin emosi. Bisa-bisanya orang mudah percaya dengan penyakit rekaan bernama Cvd (sori, saya lebih seneng nulis begitu). Penyakit yang dibuat dan dilebih-lebihkan oleh Organisasi Kesehatan Dunia (WHO) dan Bill Gates. Disebarkan melalui sinyal teknologi terbaru dan dipakai sebagai alasan untuk menanamkan vaksin yang dapat dilacak oleh teknologi tertentu. Kemudian akan berakhir menjadi alat untuk mengatur-atur orang-orang di dalam kepenguasaan tunggal bernama _Éclairage_.

…

Omnibus Law baru saja disahkan. Tapi kenapa orang-orang menolak? _Blangsak_! Padahal, dengan Omnibus Law orang-orang jadi lebih mudah mendapatkan pekerjaan karena investor-investor akan memilih menginvestasikan sumber dayanya ke sini. Bisa-bisanya mereka ditipu oleh orang-orang yang sok tahu. Baca draf RUU-nya dulu _tong_ sebelum protes, biar tidak mudah ditipu.

…

Semesta ini luas dan paralel. Kebetulan, saya diberi kesempatan untuk berbagi pikiran dengan "saya" dari belahan semesta lain. Anehnya begini. Tingkat paralel masing-masing semesta berbeda. Kebetulan lagi, "saya" tinggal di bumi dengan masalah yang sama, yang membedakan saya dengan dirinya adalah bagaimana pikiran bekerja.

Empat paragraf di atas adalah surat dari "saya". Dikirim tiga kali dengan alat yang ia sebut sebagai Kapsul Semesta.

Belum lama ini saya menemukan unggahan pengguna Twitter (sudah dihapus tak sempat saya skrinsut), dia bilang yang intinya, "_siapa saja yang mengolok-olok pemercaya konspirasi, pasti orang sama yang selalu mengiyakan apa kata pemerentah._"

Tuduhan yang "menakjubkan". Pola sama yang menyiratkan paradoks keyakinan.

Tuan-puan, setiap manusia selalu menerapkan kebenaran sesuai dengan keyakinannya sendiri. Dia bisa memilih untuk percaya dengan sesuatu karena satu hal. Sedangkan dia juga bisa memilih untuk percaya dengan satu hal karena hal lain pula.

Keyakinan dapat dipengaruhi oleh segala bias. Manusialah yang seharusnya mereduksi si bias. Sekecil-kecilnya. Agar yang masuk ke dalam pikiran menjadi lebih bening. Meski tak sampai sebening air mineral dari olahan air Klaten. Salah satu yang "sederhana" adalah mendengar orang-orang yang memiliki ilmu.

Orang berilmu yang dibuktikan dengan gelar pendidikan ditambah karir sebagai peneliti dan segudang buku atau jurnal ilmiah. Tidak cukup itu, ada adab yang ditunjukan dengan rekam jejak terapan ilmunya, apakah seturut etika atau melanggarnya.

Jejak si pemilik ilmu dapat diulik sendiri secara dalam atau dicari sepintas dari narasumber-narasumber bermutu. Ya _kalik_ kita mencari rekam jejak orang-orang tertentu, sepanjang hari, sepanjang minggu. Kalau ada waktu sih tidak apa-apa, justru bagus sekali. Kalau tak ada waktu, mencari penilaian seorang pemilik ilmu dapat dilakukan dengan membaca media, apapun yang kredibel.

Saat membahas bumi bulat, manusia perlu merujuk bukti-bukti ilmiah yang telah dipaparkan para ilmuwan tadi. Bukan dari bukti-bukti sains semu yang disampaikan berdasar asas hanya-dugaan. Yang kemudian mengglorifikasi bahwa percaya bumi bulat adalah simbol dari ketidakpercayaan pada sistem penguasaan dunia melalui elit global dan sebangsanya.

Teori konspirasi elit global ini menarik, tapi juga tidak sampai mengampanyekan bumi datar kali. Bagaimana kalau ternyata bumi berbentuk lengkungan orang kayang?

Percaya kepada pemilik ilmu juga dapat membangun rasa yakin terhadap keberadaan penyakit Covid-19. Penyakit ini memang ada dan bahaya.

Karena penyakit ini penyakit baru, pengetahuan akan terus diperbarui berdasarkan perkembangan penelitian yang dirilis oleh masing-masing ilmuwan. Bukan berdasarkan asumsi-asumsi, berdasar katanya-katanya alias kabar miring yang tak jelas muasalnya. Kalau pun ada penanganan penyakit Covid-19 yang berlebihan, itu adalah antisipasi pada kemungkinan terburuk (contohnya pemulasaraan jenazah pasien positif maupun suspek meninggal menggunakan plastik). Prinsip kehati-hatian.

Dengan mencari kabar-kabar kredibel berdasarkan pengetahuan ilmiah, kita bisa tahu, apa manfaat masker. Apa manfaat jaga jarak. Dan kenapa prinsip Ventilasi, Durasi, dan Jaga Jarak (VDJ) itu sangat penting diterapkan. Begitu juga dengan manfaat pelacakan, pengetesan, dan isolasi secara masif.

Ini bukan soal penguasaan terhadap umat manusia agar mereka tak lagi dapat bersosialisasi sekaligus dapat dilacak dengan mudah dengan cip bernama vaksin, tetapi bagaimana pandemi segera berakhir dan sebisa mungkin menekan jumlah penularan dan pasien meninggal. Ingat, yang meninggal ini manusia, bukan sandal jepit _Swallow_ hijau tua dengan kotoran hitam di sisinya.

Kami tak percaya konspirasi dan justru tak percaya pemerintah karena penanganannya yang tak mumpuni, kami percaya pada ilmuwan, epidemiolog, dan tenaga kesehatan. Begitu, bund.

Saya jadi ingat twit Pak Neil deGrasse Tyson kemarin, hehe.

https://twitter.com/neiltyson/status/1312371036655099907

Sedang heboh Omnibus Law alias RUU yang sekarang sudah jadi UU Cipta Kerja. Di luar konteks bahwa prosedur penyusunan UU ini kacau, termasuk naskah terunggah yang ternyata belum final, banyak orang yang mengolok orang lain karena sibuk protes padahal tidak membaca drafnya sendiri.

Kalau punya waktu untuk membaca drafnya, silakan, itu bagus pakai banget. Tapi sudah banyak akademisi atau kalau boleh dikata ahli _menjembrengkan_ kecacatan UU ini. Per klaster. Ada akademisi yang membahas klaster pendidikan. Ada yang membahas klaster ketenagakerjaan dan ada _environmentalist_ yang mengulas klaster lingkungan. Tinggal pilih mana yang ingin dipelajari.

Jadi, sah-sah saja untuk menyatakan protes. Baik protes karena alasan penyusunan UU yang cacat atau karena kontennya yang membahayakan.

Sensitivitas keyakinan akan berperan di sini. Apakah keyakinan Anda menciptakan keabaian dan membahayakan manusia maupun lingkungan atau tidak? Kurang-kurangilah keyakinan teori-teori tak jelas seperti itu. Nikmati saja layaknya film. Sebagai "hiburan". Jangan jadi pijakan prinsip dan pijakan hidup.

Saya akan mengirimkan tulisan kepada "saya" sebagai jawaban. Kapan lagi punya teman pena dari semesta lain. Diri sendiri lagi.

_Bhay~_

\[Gambar unggulan oleh [Xpics](https://pixabay.com/users/xpics-8861433/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4496526) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=4496526)\]
