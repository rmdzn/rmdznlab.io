---
title: "Berubah Drastis"
date: "2020-10-16"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "caterpillar-1209834_1280-ulat-rmdzn.jpg"
aliases:
  - /post/2020-10-16-berubah-drastis/
---

Baru kemarin saya menonton film Thailand keluaran GDH: _Homestay_. Sekali lihat genrenya--_thriller_ dan drama--di [TrueID](https://trueid.id), saya kira film tersebut menyeramkan, ternyata … memang menyeramkan.

Bukan dalam arti bunuh-bunuhan, terorisme, kerusuhan, dan kekerasan lainnya, tapi menyeramkan dalam konteks pesan yang disampaikan sepanjang cerita.

Satu ruh diberi hadiah oleh penciptanya dengan cara ditiupkan kembali ke tubuh seorang yang telah meninggal. Min, nama jenazah tersebut. Sang ruh ditugaskan selama 100 hari untuk menyelidiki musabab alasan tubuh yang ia masuki memilih bunuh diri.

https://www.youtube.com/watch?v=1b7jupQbi7w

Proses penelusuran Min sangat depresif. Sungguh depresif. Dari yang mulanya bahagia dapat menikmati kehidupan lagi, kemudian terbawa arus dengan masalah yang selama ini dihadapi tubuh Min.

Seratus hari adalah waktu yang tak panjang. Apalagi untuk seorang yang baru "terlahir kembali"; menemukan hidup tanpa rasa tak nyaman. Rasa yang mendominasi adalah rasa mencintai, disusul rasa sebenci-bencinya benci. Dari yang selalu tersenyum lebar sampai murung setiap hari. Meski ini fiksi, _Homestay_ memberi izin penontonnya untuk ikut larut ke dalam kisah yang membombardir pikiran dan hati.

Ada nilai yang terkandung di film ini. Kesempatan berubah. Ke arah yang lebih baik. Memperbaiki hidup yang telanjur remuk. Di sana, akhirnya Min bisa berubah. Dalam waktu kurang dari 100 hari saja. Dengan cara _nrimo kahanan_ … menerima kenyataan.

Cerita tentang perubahan diri mengingatkan saya pada seorang teman. Personil grup musik sekaligus _playboy_ yang sekarang menjadi seorang alim. Begitu juga dengan seorang teman yang "kerjaannya" mesum lalu sekarang "bergelar" _ikhwan_. Keduanya saling memanggil dengan panggilan _akh_ dan sesama istri mereka saling memanggil dengan _ukh_.

Beda banget dengan saya yang kayaknya belum akan cocok berlaku seperti itu. Haha~

Lainnya, ada juga teman perempuan yang ternyata dulu pernah pacaran dengan teman SD yang sering merundung saya. Sekarang dirinya berubah salihah juga bercadar rapat.

Proses perubahan mereka lebih dari waktu yang diberikan kepada Min--berbulan-bulan bahkan bertahun-tahun. Mungkin gara-gara Min menerapkan SKS (Sistem Kebut Semalam) atau _the power of kepepet_ dibanding yang lain. Maklum, ketika ruh yang memasuki Min tidak dapat menjalankan misinya dalam 100 hari, ia akan dimatikan lagi. Bergegas adalah satu-satunya cara agar dia berhasil.

Belum ada _sih_ kisah perubahan yang greget dibandingkan dengan karakter dalam film besutan GDH itu. Tapi begini, poin menarik dari perubahan Min adalah kemampuannya untuk menerima keadaan.

Klasik. Namun, nyatanya memang begitu.

Dari yang semula pikirannya kacau karena keadaan keluarganya yang tak membaik, begitu juga dengan teman sekolahnya yang bertindak buruk, lalu menyalahkan keadaan, Min berusaha menerima apa yang terjadi padanya dan lingkungannya.

Kompleksitas masalah dapat dilihat di film _Homestay_. Ia mengobrak-abrik asumsi yang menyatakan depresi dan bunuh diri hanya disebabkan oleh satu hal. Silakan cari di jagat media sosial, berapa banyak media daring yang menyebutkan tindakan bunuh diri karena diputus cintanya, karena tugas akhir tak rampung-rampung, karena blablabla? Padahal, tidak seremeh dan sesederhana itu, _Palguso_. Banyak dokter yang menolak segala pemberitaan bunuh diri seperti itu.

Saya pernah bertindak sebodoh itu--menganggap hanya satu dua masalah lah yang memantik rasa depresi hingga keinginan bunuh diri. Seorang kenalan depresi berat sampai harus ke psikiater dan minum obat secara rutin. Dari cerita yang ia sampaikan, keadaannya saat ini disebabkan oleh urusan percintaan (salah satunya _ghosting_). Saya mengangguk, mengiyakan bahwa situasi yang ia alami hanya dan hanya karena urusan percintaan.

Ternyata tidak. Setelah saya mengetahui keadaan keluarganya, saya jadi tahu, keadaan kenalan saya ini luar biasa kompleksnya. Lingkungan keluarganya sangat menekan. Dia termasuk anggota keluarga yang suaranya jarang atau hampir tidak pernah didengar. Ditambah dengan masalah-masalah lainnya. Kelar sudah. Mental meledak dan perlu penanganan profesional.

Kini apa kabarnya? Sehat. Dokter sudah memperbolehkan dirinya untuk berhenti meminum obat. Dia kini berubah menjadi seseorang yang lebih kuat. Menerima masa lalu, menatap ke depan. Doa terbaik buat dirinya.

Semoga yang sedang berproses menuju kebaikan segera dimudahkan. Semoga yang sedang tidak baik-baik saja segera dibaikkan. Tentu, jangan fokus pada perubahan orang lain. Sadari fakta diri sendiri hari ini dan berterima kasihlah dengan menjaga kesehatan mental dan tubuhmu.

_Cao lah~_

\[Gambar unggulan oleh [Free-Photos](https://pixabay.com/photos/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1209834) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1209834)\]
