---
title: "Belajar Hidup dari Emper Rumah; Filosofi Teras"
date: "2020-10-23"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "photo_2020-10-22_10-57-46.jpg"
aliases:
  - /post/2020-10-23-belajar-hidup-dari-emper-rumah-filosofi-teras/
---

_Cetar!!!_

Konfeti meledak tepat di depan saya. Saya yang dengan sumringah melihatnya, tersenyum lebar.

Mungkin itulah situasi yang menggambarkan saya saat berhasil membeli buku ini. Hasil dari mempertimbangkan beberapa kali (baca: _overthinking_).

Bagi saya. Cuplikan ulasan, resensi, atau apapun yang berbalut "Filosofi Teras" menjadikannya sangat personal.

Omong-omong, baru kali ini saya punya "ikatan" lebih pada satu judul buku bahkan sebelum saya membacanya. Sampai-sampai ada pikiran sepintas bahwa buku ini akan membabat pikiran yang sudah saya tanamkan sebelumnya. Tapi, nanti sajalah ceritanya, saya mau mengulas buku dahulu.

_Filosofi Teras_ dibuka oleh pengantar dari dosen Sekolah Tinggi Filsafat Driyarkara, Dr. A. Setyo Wibowo. Dari sana saya mulai tahu, ternyata Marcus Aurelius penganut stoisisme. Seorang kaisar Romawi yang muncul dalam film Gladiator dengan brewok putih.

"Teras" merupakan terjemahan harfiah dari "_stoic_" yang membentuk kata "_stoicsm_/stoisisme". Karena saat itu penggagas filosofi teras, Zeno, membuka kelas filsafatnya di emperan teras. Di samping tiang-tiang penyangga khas yang kerap digambarkan dalam bangunan-bangunan Yunani atau Romawi itu.

Stoisisme hadir sebagai filsafat praktis. Bukan filsafat mengawang yang perlu pemikiran berat. Ia dapat dipikirkan sekaligus dapat dipraktikkan. Dan filsafat ini dapat menjadi alternatif … oo tidak … bahkan menjadi gaya hidup utama untuk manusia.

Pola pikir stoisisme sebenarnya dan seharusnya dapat diterapkan di agama apapun oleh siapa saja, bahkan oleh orang yang tak beragama. Stoisisme mengajarkan kita untuk fokus pada pikiran sendiri dan berlandaskan pada nalar. Mengajak untuk kembali ke alam. Alam yang berarti luas, tak sekadar ekosistem lingkungan.

Untuk mengaplikasikannya, kita perlu melihat cakupan luas dari suatu kejadian. Mana fakta dan mana persepsi. Filosofi teras bergerak di ranah ini. Mengatur persepsi.

Contohnya, kita melihat ada orang bermotor menyalip kita dengan cara _ngebut_ (ini fakta). Cara kita mempersepsikannya akan mempengaruhi emosi kita. Saya bisa marah-marah dan meneriaki orang itu dengan "GILA" keras-keras. Tapi saya juga bisa tetap tenang dengan menganggap bahwa mungkin saja orang yang menyalip kita sedang terburu-buru karena istrinya akan melahirkan (ini persepsi). Nah, kita tinggal menentukan persepsi mana yang ingin ditangkap.

Filosofi ini juga bukan berarti tentang kepasrahan. Buktinya, James Stockdale tetap dapat mengendalikan diri meski terus disiksa sebagai tawanan perang dan diisolasi selama 4 tahun. Begitu juga dengan Viktor Frankl yang tetap waras meskipun keluarganya dibantai di kamp konsentrasi Nazi. Ya, keduanya adalah penerap stoisisme.

Persepsi adalah segalanya. Apa yang membuat kita stres atau frustrasi bukanlah peristiwa itu sendiri tetapi pandangan atau persepsi kita terhadapnya. Kita memiliki pilihan untuk menjadi sekoci yang terombang-ambing oleh ombak atau memasang dayung dan layar--dengan persepsi.

Penulis memperkenalkan STAR. _Stop-Think & Assess-Respond_. Saat melihat atau mengalami suatu kejadian. Sadari saat emosi akan terumbar. Berhenti lalu berpikir. Kemudian nilailah situasi, mana yang fakta dan mana yang persepsi. Fase terakhir adalah merespon kejadian itu dengan bijak.

Saya kaget, stoisisme ternyata benar-benar mengakui bahwa manusia merupakan makhluk sosial. Filsafat ini justru menganjurkan manusia untuk berbaur atau bersosial. Dengan tetap bersandar pada nalar dan rasional. Misalnya, agar kita dapat menangani ketika kita bertemu atau berteman dengan orang-orang menyebalkan. Tapi dalam titik tertentu, kita juga diberi hak untuk meninggalkan orang yang perilakunya benar-benar buruk dan suka menyakiti. Dengan catatan, ini adalah opsi terakhir.

Saya jadi ingat obrolan orang-orang di suatu media sosial. Mereka sama-sama membuat candaan bahwa karakter _santuy_ adalah stoisisme itu sendiri. Memikirkan diri sendiri itu lebih baik daripada repot-repot memikirkan hal lain di luar jangkauan, katanya. Ternyata tidak begitu. Stoisisme tak selalu berhenti pada diri sendiri. Banyaknya praktisi filosofi yang peduli pada krisis lingkungan adalah salah satunya.

Logikanya sederhana. Dalam stoisisme, kita dituntut untuk selaras dengan alam. Apabila alam rusak, berarti situasi tersebut tidak selaras dengan alam, dong. Dan juga, perubahan iklim akan merugikan umat manusia, padahal filosofi ini mengajarkan kita untuk mencintai seluruh manusia.

> "Epictitus berkata, 'Orang bijak tidak akan berpartisipasi dalam urusan publik, kecuali harus.' Zeno berkata, 'Orang bijak akan terus berpartisipasi dalam urusan publik, sampai ia tidak mampu lagi'".
> 
> Seneca di buku On Leisure

Buku _Filosofi Teras_ merupakan pemikiran dan gaya hidup Henry Manampiring kini.

Awalnya, ia terdiagnosis depresi lalu mendapatkan "obat" berupa buku besutan Massimo Pigliucci, _How to Be a Stoic_. Pengalaman penulis untuk mengatasi emosi yang sering tidak stabil menjadi penyumbang besar pada tulisan-tulisan di _Filosofi Teras_. Kata om Piring (begitulah panggilan akrab beliau), stoisisme adalah filsafat yang aplikatif, tak hanya abstrak. Beberapa tokoh pun adalah penganut stoisisme, termasuk Bill Clinton dan J.K. Rowling.

Kerennya begini. Penulis juga memaparkan tentang teori terapan stoisisme untuk orang tua maupun untuk anak yang diasuhnya. Belum ada filosof yang khusus membahas hal ini sehingga mau tak mau Henry Manampiring mengulasnya dengan tetap rendah hati bertanya pada para praktisi pengasuhan anak (_parenting_).

Buku ini sungguh layak dibaca oleh siapapun. Apalagi untuk orang-orang yang ingin memiliki pengendalian diri yang lebih baik. _Enggak-enggak_, saya bukan cuma basa-basi. Ini adalah ungkapan saya sejujurnya.

* * *

**Judul**: Filosofi Teras  
**Penulis**: Henry Manampiring  
**Penerbit**: Penerbit Buku Kompas  
**Tebal**: xxiv + 320 halaman
