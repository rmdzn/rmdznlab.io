---
title: "Berani Telanjang Itu Baik"
date: "2020-10-30"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "koala-2010177_1280-koala-rmdzn.jpg"
aliases:
  - /post/2020-10-30-berani-telanjang-itu-baik/
---

Beruntung sekali jadi umat manusia. Diciptakan menjadi pemimpin di bumi dan sejak awal terbentuk sebagai makhluk berbibit unggul.

Saya tidak mau muluk-muluk dahulu. Dari sekian kebiasaan manusia, satu yang menjadi favorit adalah tidak telanjang. Sikap ini telah menjadi norma tersendiri, kecuali bagi kaum nudis yang lebih nyaman beraktivitas sambil memperlihatkan kulit, bentuk, dan bulu-bulu tubuhnya.

Penting nih. Ihwal "tidak telanjang" tidak selalu merujuk pada telanjang bulat. Bisa saja manusia berpakaian meskipun dengan model terbuka; bercelana saja, berbaju saja, atau gabungan keduanya dengan desain baju jadul maupun kekinian tertentu.

Namun, bayangkan kalau manusia tidak diberi norma bawaan untuk tidak telanjang. Spektrum warna manusia jadi terbatas. Hanya ada hitam, coklat, kuning, putih, dan merah di antrean perpanjangan STNK di Samsat. Itupun tergantung genetika. Kecuali antara hitam dan putih, warna-warna lainnya tidak mencolok. Bandingkan dengan baju-baju yang diproduksi zaman ini, ada yang hijau, ungu, oranye, biru, dll. Saya jadi bisa menunjuk orang dengan, "_itu lho mas-mas baju hijau yang ditampol ibu-ibu di pasar karena nyerempet gerobak jualan gorengan._"

Bayangkan kalau manusia tidak diberi norma bawaan untuk tidak telanjang. Saya tidak dapat memamerkan karakter lucu Tony Tony Chopper, Upin Ipin, atau yang gagah seperti Superman dan Batman yang biasa ada di baju orang-orang. Saya juga tidak dapat memamerkan logo-logo sponsor pekerjaan atau logo konferensi fafifu yang pernah saya kunjungi. Tato mungkin akan menjadi budaya normal menggantikan gambar-gambar seperti itu. Dan rasa ketika disulut-sulut jarum akan menjadi rasa geli seperti digigit semut pudak.

Saya membayangkan kalau manusia tidak memiliki norma bawaan untuk tidak telanjang, saya tidak akan malu ketika syuting film pendek dulu masa SMA. Dalam sebuah adegan, saya diharuskan semi telanjang--hanya memakai kaos singlet dan kepala saya dicelupkan ke dalam ember berisi air. Rencananya muka saya juga harus diolesi saos sambil dibalut pembalut kering dan masuk ke kelas yang saat itu berisi kawan-kawan perempuan. Kampretnya, sudah repot-repot syuting, film itu tidak diselesaikan karena satu dan lain hal.

Jika definisi telanjang diperlebar sebagai telanjang fisik dan mental, maka situasi saat itu adalah telanjang kedua-duanya. Malunya dobel-dobel.

Berbeda kalau definisi telanjang lebih dipersempit pada telanjang mental. Maka kisah ketika hidung saya pernah terkena bola plastik sampai _bonjrot_ berdarah-darah termasuk dalam definisi ini.

Saya dan dua sampai tiga teman duduk di bangku bawah pohon. Di depan kami, kakak kelas bergaya gali berniat melakukan _russian roulette_. Menendang bola plastik secara acak ke arah kami. Yang terkena bola dialah yang beruntung.

Dewi Fortuna terbang bersama bola.

_Peng!!_

Wajah terasa panas. Marjan langsung keluar deras dari balik hidung. Saya berlari ke kamar mandi disusul penendang yang bentuk galinya langsung hilang. Berlagak melas dan minta maaf.

Sebetulnya saya tak perlu malu, tapi rasa tersebut spontan muncul saat teman-teman mulai membahas bahwa saya, seorang bantet dan gendut, kena tendangan bola dari si A sampai mimisan. Dan beritanya terus _membeleber_ sampai jam sekolah usai.

Pada usia yang sama, saya kembali mengalami tragedi memalukan. Zaman SD adalah zaman seburuk-buruknya stigma terhadap orang yang pergi ke WC. Setiap ada siswa yang izin ke WC saat jam pelajaran di kelas, kawan-kawannya akan meneriaki _NAMA NGISINGG!!_.

Daripada diteriaki seperti itu, saya yang kebelet sedemikian rupa karena perut sedang tidak nyaman akhirnya memilih menahannya sebisa mungkin. Tapi, semesta tak mendukung. Apa yang saya tahan, merojol. Satu kelas kena bau. Saya mau tak mau pergi ke WC dan diizinkan pulang lebih cepat. Senang? Tidak juga. Malunya itu lhooo …

Menceritakan bahwa saya pernah _eek_ di celana adalah contoh _realtime_ dari telanjang mental. Bikin deg-degan. Bagaimana kalau tiba-tiba setelah ini saya diteriaki _TUKANG PUP DI CELANA_? Saya saja perlu memikirkan beberapa kali menulis tentangnya di sini.

Menulis adalah cara merentankan diri paling mencemaskan sekaligus paling nyaman. Saya dapat menelanjangi diri dan harus siap dengan tembakan berupa ucapan atau pandangan mengiyakan atau menolak.

Saya menghormati prinsip penganut [nudisme](https://www.bbc.com/news/world-asia-41396130). Namun, kalau ditanya apakah saya tertarik menelanjangi badan sendiri di luar aktivitas mandi, lebih baik saya jadi koala atau harimau sumatra. Telanjang tidak apa-apa. Toh, mereka hidup sesuai peruntukannya--mematuhi piramida makanan. Tanpa perlu merusak tempat tinggalnya. Kecuali ada semesta lain tempat para hewan maupun tumbuhan bekerja dan hidup hanya untuk menyiksa manusia, maka sepertinya saya memang lebih baik jadi manusia.

\[Gambar unggulan oleh [Stevie Lee](https://pixabay.com/users/lonewolf6738-1763802/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2010177) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2010177)\]
