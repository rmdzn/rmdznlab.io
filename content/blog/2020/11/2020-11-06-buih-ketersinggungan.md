---
title: "Buih Ketersinggungan"
date: "2020-11-06"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "incredible-hulk-1710710_1920-marah-rmdzn.jpg"
aliases:
  - /post/2020-11-06-buih-ketersinggungan/
---

Seorang guru di negara Prancis menampilkan gambar karikatur baginda Rasul Muhammad Saw. karya majalah satire Charlie Hebdo untuk mengajarkan makna kebebasan berbicara kepada murid-muridnya. Beberapa waktu kemudian, ia mati dipenggal oleh seseorang yang kini juga sudah mati ditembak polisi.

Pernyataan Presiden Prancis, Emmanuel Macron, setelah kejadian itu dianggap tidak membantu. Macron justru disebut-sebut membudayakan penghinaan nabi di Kota Mode dan sekitarnya. Walau kemudian Macron membantah bahwa dia hanya berusaha menghormati sekaligus mempertahankan hak kebebasan berbicara di Prancis.

Kadung, negara-negara Arab bergerak memboikot produk-produk Prancis. Di Asia Tenggara, Indonesia menjadi salah satu "wakil". Pemerintah mengecam. Masy--lebih tepatnya, sebagian masyarakatnya ikut memboikot produk-produk dari Prancis.

Saat esai ini ditulis, di Indonesia sedang viral-viralnya video tumpukan air mineral Aqua yang dibuka dari kardus dan disobek-sobek oleh orang-orang tua di sekelilingnya. Bermaksud memboikot, tapi hasilnya malah menghambur-hamburkan sumber daya air dan mungkin saja menghambur-hamburkan duit yang dikeluarkan untuk membeli mereka.

Tersinggung boleh kok. Dia adalah kebebasan berekspresi itu sendiri. Tersinggung adalah salah satu spektrum emosi. Namanya juga manusia. Yang terpenting adalah bagaimana samsak ketersinggungan dibentuk. Apakah dengan teriak-teriak tak karuan? Membeli produk perusahaan terboikot lalu merusaknya bersama-sama? Yang justru malah memperkaya pemilik produk dan meletakkan para pekerja lokalnya pada ancaman PHK. Atau … memukul penyinggung dengan pentungan gardu ronda?

Silakan lakukan hal tersebut, tapi sebelum itu lihat dulu prioritas kasus. Rehatkan dulu pikiran lalu perhatikan baik-baik. Apakah cara melampiaskan ketersinggungan itu berfaedah? Atau hanya tunduk pada ego dengan buncahan marah-marah?

Iya, saya tahu. Pertanyaan saya bisa dijawab dengan argumen pembuktian iman. Marah demi membela adalah simbol sayang umat kepada Baginda Rasul dan demi mendapatkan rasa sayang dari beliau di samping lewat selawat.

Tapi, jangan-jangan selama ini lampiasan marah hanya terjebak pada identitas belaka. Apa yang terlihat lalu perlu validasi. "_Ini lho, bukti kami mencintai beliau!_" Makin epik lagi bukti cinta disampaikan sambil membeli lalu menimbun produk Aqua dan membuangnya di pekarangan atau tengah jalan, di saat daerah lain ada yang kekurangan air bersih. Antitesis paripurna dari hadis "sebaik-baik manusia adalah yang paling bermanfaat bagi manusia".

Saat melihat situasi, sekali-kali "lepas" dulu lah identitas yang berpotensi mengotak-ngotakkan. Fokus dulu dengan manusianya, bukan fokus terhadap identitas yang mereka miliki. Komik strip [Aji Prasetyo](https://www.facebook.com/story.php?story_fbid=10221910713469194&id=1599769256) ini benar-benar _mengeplak_ siapa saja yang hanya terlalu fokus pada identitas saat menghadapi isu tertentu.

![Komik Aji Prasetya](/blog/2020/11/images/2020-11-05-16.32.58-www.facebook.com-8c9ef032bf69.jpg)

Karya Aji Prasetya

Selain memperhatikan manfaat peluapan rasa tersinggung, perlu juga _nih_ mulai meletakkan rasa tersebut pada tempatnya.

Seorang takmir masjid di sebuah kampung Gadungan pernah diberi saran penting dari dua warga dalam kurun waktu berbeda. Mereka usul agar masjid menyisihkan sebagian uang infaknya untuk meringankan beban biaya sekolah anak-anak di kampung, dalam bentuk beasiswa murid berprestasi atau bantuan untuk keluarga kurang mampu.

Masih kepada orang yang sama. Pada awal-awal pandemi menerjang Indonesia, sebagian warga juga mengusulkan agar masjid menggelontorkan dana bantuan untuk warga kampung terdampak. Entah dalam bentuk uang tunai atau sembako.

Apa tanggapan Yang Mulia Takmir? Menolak tanpa eksepsi, dalam ucapan maupun dalam bentuk diam. Dirinya tetap mempertahankan uang umat untuk merenovasi masjid, walaupun sampai saat ini status tanah masjid tidak jelas, gerendel loker masjid banyak yang hilang, tembok gudang melesak ke dalam, dan eternit jebol yang selalu terocoh kala hujan.

Di lain waktu, saya menjadi saksi ketika calon presiden junjungan takmir tersebut disindir oleh tetangga, ia tersinggung kemudian sigap melawan balik sindiran sambil tangan bersedekap di dada.

Hmm … prioritas ketersinggungan yang sungguh tak sip. Bisa-bisanya, ia tersinggung karena tokoh favorit politiknya disindir-sindir tetapi tak tersinggung saat masih banyak warga tak mampu di sekeliling, padahal sang tuan memegang bergepok-gepok dana panas yang sumbernya juga dari warga sekitar.

Tersinggunglah di tempat yang tepat. Kalau sudah tersinggung, lampiaskan dengan penuh manfaat. Buktikan kalau kamu adalah umat yang elegan.

Kalau tersinggung dan marah hanya disebabkan oleh masalah identitas, buat apa? Ego pengagungan diri. Sungguh tidak ada manfaat sama sekali kecuali manfaat untuk diri sendiri (itupun jika Gusti Allah memang menganggapnya sebagai manfaat).

Tambahan: jika saya ditanya, "lalu bentuk marah apa yang kamu rekomendasikan?"

Demo sejatinya cukup kok. Tapi jangan marah saat obyek demo tidak menanggapi. Demo berminggu-minggu di Indonesia yang ditujukan untuk orang Indonesia saja tidak ditanggapi, apalagi demo sekejap yang ditujukan untuk orang luar negeri.

_Bay~_

\[Gambar oleh [ErikaWittlieb](https://pixabay.com/users/erikawittlieb-427626/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1710710) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=1710710)\]
