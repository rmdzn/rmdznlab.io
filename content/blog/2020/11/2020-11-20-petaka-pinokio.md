---
title: "Petaka Pinokio"
date: "2020-11-20"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pinocchio-3046731_1920-pinokio-rmdzn.jpg"
aliases:
  - /post/2020-11-20-petaka-pinokio/
---

Ingat tidak, Pinokio tidak memiliki hati? Bahkan juga jantung. Tapi atas keajaiban, dia dapat hidup seperti manusia.

Ia dapat merasakan senang, sedih, sayang, marah, sekaligus keinginan kuat untuk menjadi manusia seperti pada umumnya. Padahal dia tidak tahu, dibuatnya sebagai boneka kayu justru anugerah, dan apa yang ia pikir sebagai keajaiban adalah petaka.

Jika dia hanya boneka, dia akan terpajang di sebuah ruang. Antara ruang pengap atau ruang yang penuh kehangatan. Terpayung sinar matahari dari lubang ventilasi dan atap transparan sebagian. Tentu, karena Geppetto sayang sekali dengannya, Pinokio akan ditempatkan di area terbaik.

Pinokio mungkin hanya akan dijadikan sebuah mainan rumah atau mainan jalanan atau hiasan ruang sekaligus peneman hidup. Geppetto yang tidak memiliki keluarga akan lebih memilih yang ketiga meski tak salah juga menjadikan Pinokio yang pertama.

Boneka kayu dengan pakaian ala bocah kecil akan berdebu dan bersarang laba-laba pada bagian ketek dan lekuk tubuh lainnya. Itupun kalau Geppetto menelantarkannya di loteng. Namun tidak mungkin. Geppetto menyayangi karyanya lebih dari apapun, ia tak akan membiarkan Pinokio seperti barangmu yang kamu taruh di pojok belakang lemari lalu dilupakan begitu saja.

Pinokio mungkin juga dapat terciprat es teh yang tak sengaja terinjak dirinya sendiri saat dikendalikan Geppetto dengan batang kayu bertali tipis di atasnya, atau justru terkena noda wedang boba yang muncrat karena Geppetto iseng menginjak wadah bekas di samping tempat sampah.

Lagi-lagi, itu hampir tidak mungkin. Karena rasa sayang yang menggebu-gebu, Geppetto takkan membiarkan itu terjadi. Ia akan menggandeng atau menggendong Pinokio dengan sangat hati-hati. Kalau bisa, saat pulang pun Pinokio dilap dengan serbet mini halus yang biasa dibeli dari optik--si tukang kacamata.

Semua itu proses dengan tujuan yang tidak dapat ditebak. Bisa jadi, Geppetto akan merasa bosan dan meninggalkan Pinokio sendirian. Skenario pertama terjadilah. Pinokio membusuk di pojokan. Sifatnya yang tak sekadar barang mati mungkin akan memberi kesan lebih tapi tetap akan berakhir menjadi kata, "_ya sudah, mau bagaimana lagi_".

Ia tetap akan menjadi barang mati seperti mulanya.

Tak dinyana, skenario kedua lebih dipilih dunia. Pinokio hidup seperti manusia dengan segala pelik perasaan, situasi, dan kompleksitas kehidupan.

Masih untung lahir dari bayi, segala polah akan lebih dimaklumi, toh mereka tidak bisa berbuat banyak kecuali menangis dan merangkak. Pinokio? Sekali lahir langsung bertubuh seusia anak SD. Segala tindak tanduk otomatis termonitor ketat, meski tak seketat pemuda balig masa SMA.

Semua orang, termasuk Geppetto lupa, Pinokio seharusnya masih bayi yang harus digendong, di-_pukpuk_, dan diajari dari nol. Dia tidak perlu didikte untuk melakukan ini itu lebih dahulu. Dia cukup berekspresi sesuai kemampuannya dengan tetap diberi tahap kemana badan dan pikirannya pergi, sambil tetap diawasi. Sekenanya.

Selanjutnya, Pinokio perlu diberi teori-teori kesosialan. Persiapkan mentalnya saat sewaktu-waktu dipanggil kawan-kawan dengan nama bapaknya. "_Pet, mau kemana?_" misalnya.

Pinokio juga butuh diberi pengertian tentang jangan mudahnya percaya kepada orang lain. Jangan terjebak pada muka manis teman sebaya yang ingin meminjam mainan atau bukunya, karena bisa saja mereka tak kembali atau jika kembali, rupa si barang sudah tak karuan. Patah atau mungkin tertimpa air hujan atau air liur pasca dijadikan bantal tidur.

Berikan pengertian pula bahwa ia bukan manusia. Di budaya yang tak memahami dan menerima perbedaan, dia punya potensi dikucilkan. Pinokio bisa jadi dianggap sebatang kayu bergerak yang tidak diberi hak untuk mengungkapkan sepuluh (dan kelipatan) emosi miliknya. Kuatkan mental Pinokio jika sewaktu-waktu dia dituduh _baper_ karena satu dua kata obrolan yang bikin dia tidak nyaman.

Oiya, tambahkan pula pesan kepadanya untuk jangan mengungkapkan keyakinannya secara sembrono, kecuali jika ingin diganyang rombongan manusia barbar dari seluruh negara.

Ia juga perlu dijaga mentalnya. Jangan kaget kalau sewaktu-waktu hidupnya penuh keramaian, bersenang-senang, lalu terpuruk. Atau jangan kaget kalau ia sejak lahir tidak begitu menyukai rumbai-rumbai manusia di sekelilingnya. Atau justru sebaliknya, jangan besar kepala jika saat lahir merasa kesepian kemudian berhasil mendapatkan rasa senang dan kemakmuran yang didamba-dambakan.

Iya, saya lebih banyak menebar kekhawatiran dan ketakutan buat masa kecil dan masa depan Pinokio, toh tidak lama setelah lahir, ia telanjur dihukum berhidung panjang saat bicara tak benar.

\[Gambar oleh [joe137](https://pixabay.com/users/joe137-7028793/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3046731) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3046731)\]
