---
title: "Kecil Sebesar Partikel"
date: "2020-11-27"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "partikel-buku-rmdzn.jpg"
aliases:
  - /post/2020-11-27-kecil-sebesar-partikel/
---

Kembali menikmati Supernova yang fenomenal itu. Sekarang adalah _Partikel_, judul keempat dari dunia unik yang saling tertaut, dengan karakter beragam dan nilai-nilai tersirat yang, setidaknya sampai judul ini, bikin manggut-manggut.

Tokoh utama novel berseri ini selalu unik. Pada _Partikel_, hiduplah perempuan bernama Zarah yang menjadi bagian dari keluarga besar--tokoh masyarakat yang religius . Kecuali bapaknya yang berpikir dan bersikap tak wajar--tentu dengan asumsi "wajar" menurut masyarakat mayor.

Bapaknya yang nyeleneh, yang kekeh mengajar Zarah secara langsung alih-alih menempatkannya di sekolah formal, membentuk pola pikir tokoh dengan cara logis dan unik. Kritik-kritik tersirat darinya muncul secara alami.

Saat akhirnya dia mau tak mau bersekolah formal dan mengerjakan bejubel soal, Zarah berpikir, _"untuk inikah anak-anak itu disekap berjam-jam di kelas? Lebih baik mereka semua ikut Ayah ke Kebun Raya dan mendengarkan cerita-ceritanya tentang alam semesta."_ (hal. 95).

Cara belajar Zarah dengan sang Ayah memang tak terpaku pada kurikulum. Berkisah tentang alam dan beraksi menjaganya adalah salah satu yang mereka lakukan. Kalau di era kekinian, konsep belajar Zarah ini mungkin bisa disebut _[unschooling](https://en.wikipedia.org/wiki/Unschooling)_.

_Partikel_ mengingatkan saya pada [_Bilangan Fu_](/blog/2020/09/2020-09-18-bilangan-fu-bergelut-dengan-spritualisme-kritis/). Mereka sama-sama setuju bahwa kepercayaan terhadap keangkeran suatu tempat menjadi hal baik jika ia secara langsung maupun tidak langsung dapat melindungi tempat dari jamahan manusia bengal.

Keduanya juga memperjelas sisi mistis dalam setiap peristiwa ilmiah. Bedanya, sisi mistis di sini lebih ditujukan pada kehidupan ekstraterestrial.

![Buku Partikel](/blog/2020/11/images/photo_2020-11-26_21-18-58.jpg)

Buku 'Partikel' di iPusnas

Segala pengetahuan yang diserap Zarah selama diajar ayah maupun saat belajar sendiri adalah hal tak membosankan yang dapat saya nikmati di _Partikel_. Termasuk pengetahuan soal DNA simpanse dan manusia yang hanya berbeda 0,4%. Kemudian disandingkan dengan prinsip-prinsip soal hubungan horizontal antara manusia dan lingkungannya.

"Jangan pisahkan dirimu dari binatang. Kamu lebih dekat dengan mereka daripada yang kamu bayangkan," kata Firas, ayah Zarah. "Biar kamu tidak sombong jadi manusia."

Hasil belajar Zarah yang lain adalah pengetahuan tentang _missing link_. Kita adalah campuran makhluk bumi dan ekstraterestrial. Sedangkan "buah kuldi" adalah simbol dari pengetahuan yang tak sengaja didapatkan oleh Adam-Hawa berupa rahasia tentang reproduksi. Karena "buah kuldi" itu, manusia dapat berkembang biak sendiri tanpa diatur oleh "alien".

Tentang lainnya. Bagi Firas, manusia modern adalah manusia yang semakin dekat dengan binatang dan alam. Sikap yang berbanding terbalik dari orang umum yang menganggap kemodernan manusia terlihat dari perlakuan mereka terhadap pengembangan dan pembangunan tanpa menghiraukan alam.

Pun kalau dilihat dari sisi sosial, alam lebih paham manusia dibanding manusia dengan manusia lainnya. Seperti kata Zarah yang selama ini merasa terasing dari komunitas karena pandangannya yang nyeleneh. Tak ada yang mau mendengar pemikiran dirinya dan Firas, kecuali langsung menghakimi dengan beragam cemoohan. "_… yang jelas makhluk-makhluk ini \[fungi\] adalah pendengar yang luar biasa, yang tidak akan memotong kita bicara atau memberikan solusi tak perlu._" (hal. 81).

_Partikel_ memang lebih banyak bercerita tentang lingkungan. Sisi kelam. Kritikan yang bikin pening tentang reboisasi yang hanyalah simbol belaka. Tidak ada yang dapat mengganti hutan beragam pohon berusia ribuan bahkan jutaan tahun. Begitu juga kasus pencemaran air di Sekonyer, Kalimantan, yang membuat Zarah dkk cemas sebelum sampai hotel dan merebahkan badan.

Ada juga cerita tentang sampah raksasa _Great Pacific Garbage Patch_. "_Dalam hati, kami sadar itu masalah serius. Menuju kehancuran, manusia modern bahu-membahu. Menghabiskan dana dan tenaga untuk jutaan hal tidak penting dan mengesampingkan urusan hidup dan mati bumi ini sembari berteriak tak cukup dana._" (hal. 376)

Kemampuan Zarah untuk menyatu secara alami dengan alam akan kembali menenangkan kalian semua, Kawan. Lewat ilustrasi pelukan bayi orangutan dan hubungan mereka dengan manusia di _camp_ bikin terenyuh hati. Yakin.

_Partikel_ lebih banyak "dipecah" dalam cerita-cerita kecil dibandingkan seri Supernova pendahulunya. Lebih banyak titik pemotong sebagai batas antar skena. Bukan berarti cerita menjadi tak runtut. Ia tetap runtut tapi justru lebih bisa saya nikmati karena lebih mudah dipotong untuk ditinggal sebentar ke dapur atau kamar mandi.

Saya akan setuju jika penggemar Supernova beramai-ramai turun ke jalan pada tahun 2012. Menggelar orasi dukungan dan ucapan terima kasih kepada Dee Lestari karena lanjutan novel favorit mereka terwujud setelah 8 tahun lamanya. Sepadan dan menyenangkan.

Besok lanjut _Gelombang_!

* * *

**Judul**: Partikel  
**Penulis**: Dee Lestari  
**Penerbit**: Bentang  
**Tebal**: viii + 500 halaman
