---
title: "Membual Serentak"
date: "2020-12-04"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "speak-238488_1920-bicara-rmdzn.jpg"
aliases:
  - /post/2020-12-04-membual-serentak/
---

Ayo pesta bualan!

Dibandingkan pesta lendir dan pesta narkoba yang haramnya kelihatan, pesta bualan ini tidak jelas--abu-abu. Diadakan beberapa tahun sekali dan dipecah dalam beberapa bagian. Sekali waktu, diadakan besar-besaran.

Dahulu sih, saya pernah tertarik dengan pesta ini. Karena memang tidak perlu mengendap-ngendap dan izin jika ingin bergabung. Saya malah didorong-dorong untuk ikut. Dari sirkel terkecil sampai senegara.

"Ayo! Masuk _venue_! Keluarkan congormu!" Kata teks yang ditempelkan di mana-mana. Spanduk dan baliho, halaman depan dan tengah koran, radio, serta televisi.

Teks-teks itu berhasil menjebak saya. Emm, saya akan merasa curang kalau menganggapnya jebakan, karena saat itu saya memang tertarik. Bukan tertarik seperti saat anak kecil melihat gulali di pasar malam, tapi lebih pada saat saya melihat isi lemari pendingin minimarket.

Kapan lagi ikut berpesta pora secara bebas? Saya bisa memilih botol minum yang saya anggap paling enak atau yang menawarkan nutrisi semu yang baik untuk tubuh. Sambil tetap sadar, kalau kebanyakan minum serupa akan kelebihan glukosa. Penyakit gula.

Sekian pesta yang saya ikuti, satu dua bualan terlampau samar. Satu dua lainnya terlihat mencolok.

Saya ingat, suatu hari, seorang pembual yang berpuluh-puluh tahun duduk nyaman di tahtanya kembali mengikuti pesta ini.

"Tidak mungkin ada yang mengalahkannya!," gaung para pendukung maupun pencacinya.

Benar sekali, pembual itu sampai saat ini masih duduk tenteram tak tergoyahkan. Mengalahkan masa bakti raja yang sudah lama _lengser keprabon_. Isu-isu yang ia mainkan di belakang adalah rahasia umum bagi para peserta pesta tua dan menjadi gosip baru buat pemula. Termasuk saya.

Itu tidak seberapa.

Sebetulnya sudah rahasia umum pesta bualan menjadi ajang membual keras-keras. Namanya juga pesta bualan. Bagai api yang panas dan air yang basah.

Saya pernah menjajal terjun langsung menjadi penjaga pintu ruangan pesta. Akibatnya, saya menjadi paham situasi bualan sebenarnya. Bayanganmu bagaimana? Para pembual berteriak-teriak sambil melempar ludah? Benar, mereka memang melakukan seperti itu, tapi sembari menyodorkan kotak tipis dengan lem pada ujung-ujungnya.

Kotak ini disebar tak hanya kepada peserta pesta lain, tetapi juga kepada penyelenggara pesta; penata meja, peletak taplak, pendekor panggung, dan pencatat peserta, yang sejatinya memiliki etika untuk tak boleh menerima apapun dari sang pembual. Namanya saja pembual di pesta bualan. Mereka akan memaksa yang diberi untuk menerima pemberiannya. Kalau pemberian ditolak, merajuk. Apa yang bisa kita harap dari mereka?

Tugas saya adalah menjaga pintu ruang dan menyimak apa yang pembual dan penyelenggara pesta lakukan. Mencatat apa yang keliru--teknis dan etis. Memberitahukan kekeliruan (kalau bisa) dan melaporkannya kepada kepala penjaga pintu pesta.

Namun itu tidak berjalan baik. Gerak penjaga pintu tak sebebas yang dipikirkan. Ada ruh misterius yang menaungi tindak-tanduk. Secara tak kasat mata, mereka mencegah mata penjaga pintu untuk memantau dan merekam proses pesta. Kecuali kalau mau direpotkan dengan birokrasi ala pesta bualan, penjaga pintu harus menahan diri dan tetap bekerja sesuai formalitas normal. Melaporkan yang baik-baik saja walau sekacau dan securang apapun pesta.

Keadaan penjaga pintu saat itu bagai kamu yang memegang ponsel di jalan lalu melihat perampokan. Kamu tahu jelas siapa perampoknya. Postur badan, wajah, bahkan alamat rumah dan namanya. Tapi kamu tahu, jika kamu memanggil polisi lewat ponselmu, kamulah yang justru akan dihantam dan ditusuk dari belakang.

Itu hampir terjadi dan itu sungguh menyakitkan.

Ruh misterius itu akan terus menggelayut. Ia akan menampilkan propaganda berupa sindiran atau olokan lewat media yang dibentangkan antara pohon atau tiang listrik. Memburuk-burukkan kompetitornya, agar pembual yang mereka usung menjadi pembual profesional, berkasta tinggi, bergaji berlebih.

Ruh misterius itu kemudian akan memilih siapapun yang pada pesta bualan berikutnya bertindak menjadi penyelenggara pesta dan penjaga pintunya.

Tidak perlu ada usaha untuk membenahi dan menjenamai ulang _pesta bualan_. Percuma. Kecuali jika pembual utama dan ruh misterius tidak lagi dipelihara dan sistem pembentuk mereka dipunahkan dari istana.

Maaf kalau hari ini penuh metafora. Siapa tahu, saya tiba-tiba didatangi grudukan massa berwajah garang dengan atau tanpa bendera dan lencana.

\[Gambar oleh [Ryan McGuire](https://pixabay.com/users/ryanmcguire-123690/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=238488) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=238488)\]
