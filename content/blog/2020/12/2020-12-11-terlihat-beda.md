---
title: "Terlihat Beda"
date: "2020-12-11"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-pragyan-bezbaruah-2029235-malu-rmdzn.jpg"
aliases:
  - /post/2020-12-11-terlihat-beda/
---

Dua anak kecil beradu malu ketika bertemu dengan seorang pengantin baru. "Halo!" teriaknya. Anak kecil itu langsung menunduk-mendongak sambil menyunggingkan senyum sipu.

Jauh hari sebelumnya, dua anak itu sungguh akrab dengan sang pengantin. Tentu sebelum pengantin ini belum menjadi pengantin. Akrab seakrab-akrabnya. Canda dan olokan yang saling terlontarkan dengan riang, atau setidaknya _nyambung_.

Ada satu titik ketika saya heran lalu berpikir, mengingat, dan berusaha menyimpulkan apa yang terjadi.

Bagi anak kecil, pernikahan adalah hal baru. Bukan "baru" seperti yang dibayangkan manusia remaja, dewasa, dan orang tua yang menyebut pernikahan penuh cinta dan tanggung jawab, melainkan cenderung pada karismanya. Pandangan orang terhadap apa yang tumbuh secara gaib dari dalam diri. Saya bingung saat memikirkan ini. Apa iya anak kecil (seusia TK/SD) bisa memandang karisma manusia?

Saya teringat dengan situasi saya saat masih TK. Saudara sepupu yang sering menggantikan ibu untuk mengasuh saya, menikah. Dari yang biasa akrab saat ketemu dia, saya malah jadi sering tersipu. Kalau ketemu suaminya, saya malu pakai banget. Untuk lewat depan rumahnya saja saya hampir tidak sanggup.

Saya melihat saudara sepupu saya seperti orang baru. Mungkin seperti saat melihat artis ibu kota yang pulang kampung sambil _da-daa-da-daa_ dan berjalan buru-buru. Kinclong gitu. Saya kurang yakin dan tahu apakah analogi saya cukup mendekati, tapi kurang lebih memang seperti itu.

Bisa jadi saya meremehkan pikiran polos mereka dan saya zaman dulu.

Siapa tahu, anak-anak kecil dapat membayangkan kehidupan calon orang tua baru yang akan merawat putra-putrinya hingga sebesar mereka. "_Siap-siap saja mereka akan lebih mudah marah karena anak-anaknya sebandel diriku_" katanya sambil menyipitkan mata. "_Bisa jadi, mereka justru cermat dan lebih sabar daripada orang tuaku yang lebih memilih membanding-bandingkan anaknya dengan anak orang lain yang begitu rajin._"

Jangan-jangan, anak-anak kecil ini aslinya merasa cemas alih-alih malu. Mereka lebih paham situasi tentang masa depan yang tak pasti. Apakah mereka akan menjadi tetua yang membawahi anak-anak pengantin baru itu di dunia yang hampir seperti dunia Mad Max atau Waterworld-nya Kevin Costner.

Mempersiapkan bekal menuju Valhalla dengan mobil besi tua berkarat dan komponen lancip yang dipasang di depan, samping, maupun belakang. Atau mempersiapkan perahu besar untuk berlayar mencari tanah kering yang didambakan orang-orang.

Bisa juga mereka mulai mempertimbangkan untuk mengajak adik-adiknya ke peradaban baru di Mars lewat program SpaceX yang sudah mulai menjamah galaksi Andromeda.

Anak-anak itu juga paham, generasi mereka dan generasi di bawahnya akan memiliki tingkat kecemasan yang lebih tinggi dibanding generasi-generasi sebelumnya. Pencapaian hidup masing-masing mereka akan tersampaikan dengan lebih mudah dan lebih nyata. Mereka merasa harus bertanggung jawab meningkatkan kesadaran orang lain bahwa kecemasan yang disebabkan _postingan-postingan_ itu dan usaha saling membandingkan nasib adik-adiknya adalah nyata bukan abal-abal.

Di negara yang orang-orangnya banyak tak paham tentang kesehatan mental, anak-anak kecil ini jadi merasa bertanggung jawab penuh terhadap mental adik-adiknya.

Apalagi didukung media sosial yang menyerbu dengan mode semakin _real time_, tidak cukup dengan tulisan dan gambar dua dimensi, tapi sudah seperti dunia empat dimensi Steven Spielberg, _Ready Player One_.

Namun bisa saja pikiran anak-anak ini di luar apa yang orang dewasa bisa percaya. Mereka mungkin tidak merasa malu, tapi gugup terhadap rasa percaya diri mereka atas tanggung jawab mengayomi dan mempertahankan permainan-permainan jadul agar tetap bertahan atau dapat terlahir kembali.

Gobak sodor, layang-layang, _delikan_ (sembunyi dan temukan alias _hide and seek_), _dingklik oklak-aklik_, _sombyong_, kelereng, bola bekel, _cublak-cublak suweng_, dll antara mati atau terlahir kembali dalam bentuk virtual. Dimainkan dengan kacamata ala Cyclope di kamar atau teras masing-masing rumah.

Namun itu hanya reka-reka. Padahal aslinya memang sesederhana anak yang merasa malu.

Anak kecil itu dipastikan akan kembali ramah dan akrab dengan mereka yang dituduh "menjadi baru". Seperti saya yang akhirnya tak perlu malu-malu lagi kepada saudara sepupu dan pasangannya. Sampai-sampai saya tak perlu ragu digendong bolak-balik dalam keadaan terjaga maupun tidur di dalam sarung yang ia kalungkan ke tubuhnya setiap pergi ke tempat tertentu.

\[Foto oleh **[Pragyan Bezbaruah](https://www.pexels.com/id-id/@pragyanbezbo?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/botol-cewek-dalam-ruangan-duduk-2029235/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
