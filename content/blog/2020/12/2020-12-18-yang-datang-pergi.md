---
title: "Yang Datang Pergi"
date: "2020-12-18"
categories: 
  - "cerpen"
tags: 
  - "cerpen"
coverImage: "pexels-tobi-631986-jalan-kaki-rmdzn.jpg"
aliases:
  - /post/2020-12-18-yang-datang-pergi/
---

"Assalamu'alaikum …"

Suara lembut itu mengagetkanku.

"Waalaikumsal--"

"Mas, saya mau pamit pulang," ujarnya dengan senyum kentara.

"Ke Bintan? Kok enggak kabar-kabar dari kemarin?"

"… Iya, Mas. Maaf. Ini ada bingkisan buat ibu."

Sebungkus kresek hitam dengan kotak besar di dalamnya telah pindah ke genggamanku.

Aku tidak tahu apa yang perlu kurasakan saat itu. Ada banyak getir yang menyelimuti. Baru pertama kali aku sedekat itu dengan seorang perempuan. Perempuan baru. Yang sejak awal tak kenal, menumbuhkan ketertarikan, dan mulai kudekati pelan-pelan.

…

Sepuluh tahun lebih masa balig yang kualami. Selama itulah aku tak menyangka akan merasakan hal seperti ini.

Ada rasa skeptis pada kehidupan berbunga-bunga. Bukan skeptis karena perjuangan yang selalu gagal, tetapi karena benar-benar tidak peduli. _Itu adalah rasa yang tak perlu kualami saat ini dan mungkin seterusnya_, pikirku. Ya meskipun semasa sekolah aku sering dikelilingi teman-teman yang sering bercakap bahasan serupa, ringan maupun agak "liar".

"Tentu saja, aku ingin mencari yang usianya jauh lebih muda dariku. Selisih lima sampai enam tahun sepertinya cukup," kata seorang teman yang terkenal cerdas seangkatan. "Biar istriku kelak lebih gampang nurut. Kalau seumuran, kita akan sering berdebat dan dia akan lebih sering membantah."

Aku mengangguk saja.

"Gimana denganmu?"

_Tidak tahu_, jawabku.

Untung saat itu aku masih polos. Kalau dia masih mengatakan hal yang sama kepadaku saat ini, langsung kutendang pantatnya keras-keras.

Aku menganggap segala hal yang berhubungan dengan rasa suka adalah kompleks, walau tampaknya sederhana. Dia tidak hanya berhenti menyeruak lewat pori-pori tapi langsung mengoyak organ dalam. Sebelah empedu mungil.

Rasa suka ialah sikap yang awalnya sekecap dan sepengelihatan lalu melebur menjadi kasmaran. Bibit-bibit lahirnya rasa kepemilikan. Tidak gampang menyederhanakan pengaruh rasa itu terhadap kehidupan di dunia nyata.

Empat bulan bertemu, satu bulan berkenalan. Perginya perempuan itu bukan sekadar pulang kampung lalu kembali pasca lebaran, tetapi pergi, kembali ke rumahnya untuk menikmati berkumpul bersama keluarga seterusnya. Lebih dekat, lebih lama. Setelah bertahun-tahun merantau di pulau seberang.

Siapa aku yang dapat melarangnya pulang? Siapa aku yang kecewa karena diberi pesan pamit sekenanya? Temu belum lama. Kenal juga belum lama.

Aku memandangnya memasukkan sekian pak kardus dan rak yang sempat kubantu turunkan dari kos lantai atas ke bagasi mobil MPV putih. Dari jauh. Tak ada perpisahan berarti, kecuali _tos_ dan lambaian tangan.

Baru kali itu. Setelah lambaian terakhir, kasur yang semasanya ringan dan menyenangkan jadi tempat rebah paling berat. Rasa sulit melepaskan itu timbul seketika. Segala sesal tumpah ruah dalam satu titik, pusat rasa itu diproduksi seenaknya oleh otak--datang dan pergi, pelan dan tiba-tiba.

"Coba kalau kemarin kenalan lebih cepat."

"Coba kalau kemarin enggak hujan, rencanaku untuk mengajaknya ke bakul dawet Simpang Lima pasti berjalan lancar."

"Coba kalau kemarin mengajak makan di warteg favoritnya dan berbincang banyak tentang hidup sambil menyelamatkan semut dari injakan pelanggan lain."

Baru kali itu. Aku benar-benar tidak bisa tidur. Mau menggerung tidak mungkin. Teriak? Apa lagi. Mau menangis tapi terhambat selimut yang kutempel rekat di muka. Dada panas dan terasa penuh beban. Kelebatan-kelebatan ingatan sesal terus muncul tanpa henti. Dan terus menghantui sehari, dua hari, tiga hari lagi.

_Kamu kenapa?_

Tidak. Tidak. Itu hanya pertanyaan halusinasi dari orang lain yang kubayangkan setelah hari perpisahan itu terjadi. Aku hanya sempat dipanggil "mata panda" oleh kasir minimarket langganan yang hanya berjarak dari rumah lima sampai sepuluh langkah kaki.

Tak banyak hal yang dapat kutuliskan untuk mengenang masa menyebalkan itu. Aku hanya terus berharap lekas beranjak.

Menutup komunikasi dan sibuk dengan hal lain. Menerima keadaan dan kenyataan lalu melupakan. Cara taktis terakhir, agar _plong_, aku menyalurkan kisah tak mengenakkan ini dalam lembar dokumen untuk dikirim sebagai cerpen ke media digital lokal.

TING!!

Nama tak asing itu muncul di kolom notifikasi monitor laptop. Sial, perempuan itu mengikuti akun media sosialku.

\[Foto oleh **[Tobi](https://www.pexels.com/id-id/@pripicart?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/alas-kaki-berjalan-di-luar-rumah-fashion-631986/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]

Kamu bisa mendukung saya sekaligus mendapatkan versi PDF cerpen ini di [KaryaKarsa](https://karyakarsa.com/rmdzn/yang-datang-pergi).
