---
title: "Bukan Kaleidoskop"
date: "2020-12-25"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-prashant-gautam-709626-desember-rmdzn.jpg"
aliases:
  - /post/2020-12-25-bukan-kaleidoskop/
---

Sejak dahulu, keluarga kecil kami--bapak, ibu, kakak laki-laki, dan saya--tak terbiasa dengan aksi seremonial. Merayakan sesuatu atas apapun yang dianggap layak dirayakan di rumah.

Di luar konteks, misal ada anggota keluarga sedikit iri karena keluarganya kuper, tak dapat melakukan kehebohan seperti keluarga para tetangga, aksi merayakan sesuatu cenderung bikin canggung. Dan itu secara langsung maupun tidak langsung mempengaruhi cara saya mengungkapkan perasaan terhadap sebuah perayaan.

Hari ulang tahun, Hari Ibu, Hari Ayah, maupun hari raya nonformal lain tak dapat menjadi pemantik untuk mengucapkan selamat atau kata romantis lainnya. Kecuali kalau memang situasi menuntut untuk bertindak demikian.

Proses mengenal diri sendiri terasa di sini. Saya dapat melanggar kenyamanan dengan mengatakan apa-apa tanpa banyak kerepotan. Terpaksa, mungkin. Sedikit kepikiran, tapi tidak sampai bikin hati canggung bermenit-menit bahkan berjam-jam.

Saya kembali ingin menantang diri, menuangkan aksi canggung. Merayakan akhir tahun. Mengikuti kehebohan ala-ala kaleidoskop, foto terbaik, _postingan_ terhebat, dan sebagainya. Tapi, tidak … tidak … saya tidak akan menulis hal-hal seperti itu. Saya ingin berbagi apa yang saya nikmati setahun terakhir ini.

## Buku Favorit

Saya masih melanjutkan novel berseri _Supernova_. Sampai tulisan ini terbit, baru sampai _[Gelombang](/blog/2020/11/2020-11-27-kecil-sebesar-partikel/)_. Ia jadi salah satu novel berseri yang saya baca tahun ini dan novel berseri kedua yang saya baca sepanjang hidup--setelah Harry Potter.

_Supernova_ menjadi bacaan yang menyenangkan. Jenis buku yang hampir tidak dapat dijeda kalau belum habis sampai akhir. Kecuali judul pertama [_Kesatria, Putri, dan Bintang Jatuh_](/blog/2019/09/2019-09-15-kesatria-putri-dan-bintang-jatuh-sering-rumit-sering-menarik/) yang bahasannya kebangetan rumit. Sedangkan kalau novel berseri favorit lain yang bikin geleng-geleng kepala, filosofis, terkadang menyakitkan, gelap dan marah adalah _[Bilangan Fu](/blog/2020/09/2020-09-18-bilangan-fu-bergelut-dengan-spritualisme-kritis/)_. Dua judul lanjutannya belum saya sentuh.

_[Waktu untuk Tidak Menikah](/blog/2020/02/2020-02-14-menyimak-isu-isu-perempuan-lewat-cerpen/)_ menjadi buku kumpulan cerpen favorit yang saya baca tahun ini. Agak gelap, tapi dapat dinikmati dengan senang hati karena penceritaannya yang enteng.

Buku nonfiksi terfavorit jatuh kepada _[Filosofi Teras](/blog/2020/10/2020-10-23-belajar-hidup-dari-emper-rumah-filosofi-teras/)_! Ia hampir mendekati sikap yang saya pikir dan terapkan beberapa bulan terakhir. Membaca ini bikin saya seperti berbincang dengan teman sendiri, tapi sambil membela diri saat sikap saya terasa disalahkan. Meski lalu merenung, merasa keliru. "Iyaa juga, yaa."

## Musik Favorit

Kalau ada istilah penggemar KPop jalur pandemi, saya mungkin masuk ke dalam barisan. Tahun kemarin sebetulnya sudah mengikuti "genre" musik ini, tapi hanya sebatas pada lagu-lagu Blackpink. Tahun ini, eksplorasi terhadap KPop lebih gencar dengan memasukkan lagu IU, Lee Hi, LeeSSang, Gary, TWICE, aespa, sedikit BTS, dan NCT ke dalam telinga.

Walaupun hanya lagu Lee Hi, LeeSSang, Gary, dan TWICE yang resmi masuk daftar putar.

Lagu-lagu Lee Hi di album _First Love (2013)_ dan dua EP (_Extended Play_); _Seoulite_ (sisi A dan B) serta _24°C_ semua oke punya. Termasuk juga lagu saat berduet dengan Epik High; _It's Cold_ dan _Can You Hear My Heart_, serta lagu saat berduet dengan Hi Suhyun _I'm Different_. Lagu favorit dari perempuan ini jatuh pada _Breathe_ dan _HOLO_.

Saya agak mangkel _sih_ karena baru tahu LeeSSang, duo hip hop yang sudah bubar, setelah mulai menikmati acara varietas Running Man. Satu personilnya adalah anggota Running Man yang juga sudah keluar sejak tahun 2017.

Ketukan dan komposisi lagu LeeSSang mudah dinikmati dan nyaman di kuping. Sebagai manusia yang tidak memfavoritkan lagu-lagu dengan model hip hop dan rap, LeeSSang terasa segar. Padahal mereka lahir tahun 2003 lalu. Ketukan lagu pelan, hentakan tiba-tiba dari _rapper_ Kang Gary, dan chorus yang mudah dinikmati melantun dari Gili.

_Asura Balbata_ adalah album looss! Hampir semua lagu di sana menjadi favorit. Terutama _Turned Off The TV_. Lagu di album lain atau _single_ favorit di antaranya _Girl Who Can't Break Up, Guy Who Can't Leave_, _Tears_, dan _Kaleidoscope_.

Tentu, karena saya menghobi Running Man dan LeeSSang, Kang Gary perlu diintip karya-karyanya. Di samping berduet, dia juga menelurkan karya solo. Saat grup duo LeeSSang bubar, ia sepenuhnya menjadi penyanyi solo.

_Single_ _Lonely Night_ jadi paling favorit. Selainnya ada _ZOTTO MOLA_ dari album _Mr. Gae_ dan _single_ _Your Scent_. Album _2020_ juga tidak kalah oke. Cocok buat leha-leha di teras rumah sambil mengisap pantat putik kembang sepatu.

Jangan tanyakan soal TWICE. Hampir semua lagu dari album _The Story Begins_ sampai _Eyes Wide Open_ dan dari EP _Page Two_ sampai _BETTER_ menempel di kuping semua. Dari yang _ballad_ sampai berketuk cepat. Halo NaJeongMoSaJiMiDaChaeTzu, _saranghae_!

## Tontonan Favorit

Sekali lagi. Saya sebarisan dengan penggemar acara-acara Korea jalur pandemi. Setelah merampungkan drama [Reply 1988](/blog/2020/06/2020-06-26-kala-saya-bingung-ingin-menulis-apa/), saya rehat terlebih dahulu dari kisah serupa.

Saya mulai menonton Running Man sejak episode pertama. Berlanjut sampai saat ini. Sekaligus menemukan harta karun berupa acara _Dogs are Incredible_ (개는 훌륭하다) dari kanal YouTube KBS World serta _We Like Zines!_ (냄비받침) dari kanal yang sama.

_Dogs are Incredible_ adalah acara yang awalnya dipandu Kang Hyungwook, Lee Gyeong-gyu, dan Lee Yu-bi, tapi di sekitar episode 30 tinggal dua yang pertama.

Hyungwook adalah pelatih anjing profesional. Tak sekadar fokus melatih anjing, di acara ini, dia juga mengajari pemilik anjing untuk bertanggung jawab terhadap piaraannya, sadar terhadap kemampuan dirinya sendiri, dan tanggap serta bijak terhadap lingkungan/tetangga tempat mereka membesarkan anjing.

Acara yang menampilkan begitu hebatnya binatang berbulu itu hingga saya menyadari, mereka lebih pantas mengumpat dengan cacian "manusia!" daripada manusia yang mengumpat dengan nama mereka.

Acara _We Like Zines!_ yang sebetulnya adalah acara tahun 2017 dan tidak ada musim lanjutan, merupakan acara yang menjembatani figur publik untuk menulis buku. Ada komedian Lee Gyeong-gyu (orang yang sama di _Dogs are Incredible_), aktor Ahn Jae-wook, atlet badminton Lee Yong-dae, anggota Super Junior yang sekarang kerap menjadi MC Kim Hee-chul, dan grup idol TWICE.

Banyak hal menarik di acara ini. Tentang politik, makanan, keluarga, ketenaran, dan kejutan dalam hidup keseharian.

Melihat bagaimana kanal televisi Korea menayangkan acara-acara seunik itu membuat saya berdecak remeh saat melihat acara televisi di negara ini. _Kepinding!_

Biarkan ini menjadi tulisan penutup tahun 2020 dan lagu di bawah ini menutup bulan Desember.

https://www.youtube.com/watch?v=CfUGjK6gGgs

Selamat mendengar lagu-lagu carol!

Selamat liburan dan selamat akhir tahunan!

\[Foto oleh **[Prashant Gautam](https://www.pexels.com/id-id/@pacific?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/alat-tulis-bahan-seni-beraneka-ragam-buku-agenda-709626/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
