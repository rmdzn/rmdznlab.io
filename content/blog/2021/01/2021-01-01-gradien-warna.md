---
title: "Gradien Warna"
date: "2021-01-01"
categories: 
  - "cuap"
tags: 
  - "cuap"
aliases:
  - /post/2021-01-01-gradien-warna/
---

![gambar warna-warni](/blog/2021/01/images/pexels-jan-van-der-wolf-11680885-29945184-rmdzn.jpg)

Kalau digambarkan dalam panel komik manga atau adegan anime, seseorang akan menyembulkan kepalanya dari dalam jendela yang dibuka pelan-pelan. Lubang hidungnya melebar, mata merem, bibir segaris melengkung atau datar, janggutnya sedikit mendongak. Suara hirupan dan hembusan nafas dengan gambar garis berkelok di depan hidung.

"Haaah …"

Pembaca maupun penontonnya diharap ikut melepaskan napas dan menikmati suasana sama-sama. Menyambut hari yang baru, pekan yang baru, tahun yang baru, dan dekade yang baru.

Banyak resolusi tertulis di lembar buku baru. Meski hanya menulis ulang resolusi 2020 yang telanjur remuk tak karuan. Atau resolusi baru, tetapi lebih sedikit dan ringan. Atau resolusi yang benar-benar baru, membalaskan dendam resolusi kacau tahun sebelumnya, menerabas apapun yang menghalangi di depannya.

Tidak ada yang salah.

Namun, hidup ini tidaklah biner. Yang dapat dibagi menjadi nol dan satu, atau hitam dan putih, lunak dan keras, gelap dan terang. Kalau dalam konteks waktu, hidup bukan portal yang membawa penerima nasib untuk merayakan keberhasilan atau merenungi kegagalan dalam waktu sekejap saja, seperti yang dialami Nobita.

Pada suatu sore, anak SD berbaju kuning itu merengek kepada Doraemon. Mengapa ia selalu dirundung dan dijadikan samsak Gian dengan Suneo menjadi pemandu sorak di belakang? Ditambah nilainya yang selalu jelek di sekolahan? Doraemon mengabulkan permintaan Nobita untuk mengintip masa depannya lewat lorong waktu. Cukup masuk ke lemari meja, beberapa detik hingga menit perjalanan, ia dapat langsung melihat nasibnya puluhan tahun kemudian.

Namun dunia tak sebegitu sederhana.

Waktu adalah ilusi, kata Carlo Rovelli di buku _The Order of Time_. Kata peneliti lain, waktu memang ada. Ia disimbolkan dalam detik jam dindingmu, kulitmu yang mengeriput, rambutmu yang memutih. Manusia yang menua lalu mati. Besi di sekelilingmu yang kuat dan bening lalu hilang terkorosi. Kasus sama seperti gravitasi yang tak terlihat tapi dapat dibuktikan dengan lemparan bola bekelmu ke atas, tubuhmu yang mengambruk ke kasur, dan air yang meresap ke sela-sela konblok.

Waktu berjalan bertahap. Soal dianggap cepat atau lambat, itu urusan lain, subjektif. Begitu juga pada apa itu yang disebut "tahun", satu dari sekian lambang waktu. Angka yang menyimbolkan peristiwa, zaman, dan era itu berjalan tak secara _mak bedunduk_.

Bagaimana dengan resolusimu? Adakah strategi menjadi lebih baik dalam sekejap yang bisa dijadikan contoh dan renungan? Kenapa memilih menunggu tahun tertentu untuk menjadi hebat dan baik?

Yang saya tahu. Perubahan terjadi langkah demi langkah. Yang instan biasanya lebih cepat pudar sedangkan yang bertahap umumnya lebih melekat dan hampir kekal. Seperti pohon jati yang tumbuh pelan tapi berakhir kokoh dibanding rumput yang cepat tumbuh dan lekas tercerabut.

Perubahan yang bertahap juga seperti penyediaan warna penyunting foto atau grafik. Penggunaan warna solid untuk mewarnai lebih rentan dan lebih cepat diurungkan ke warna sebelumnya. Beda jika menggunakan gradien untuk mengubah warna. Setiap "kontrol u" ditekan, warna akan diurungkan berurutan. Apa yang berubah secara singkat, akan berbalik ke semula lebih cepat. Apa yang berubah secara bertahap, akan lebih awet dan jika merasa ada kesalahan, proses balik juga tidak terlalu jomplang dengan hasil akhir yang diharapkan.

Jarang-jarang saya menerbitkan tulisan blog tepat saat perayaan "hari spesial". Namun dua hari dengan selisih seminggu ini kebetulan terjadi hari Jumat, tepat menutup 2020 dan membuka 2021, hari yang dalam setahun ini biasa saya pakai untuk menerbitkan tulisan baru.

Hari kebetulan di situasi "spesial" ini menjadi salah satu cara saya untuk berdamai dengan ucapan selamat dan rasa canggung. Menerapkan pewarnaan dengan penggeser gradien bukan dengan warna solid.

Bisa saja pada esok hari saya enggan mengucapkan selamat-selamat lagi, tapi setidaknya sikap itu hanya mundur per langkah dan bisa berubah melaju lagi dengan mudah. Bandingkan jika saya memilih naik ke atap gedung berlantai 30 memakai truk penderek dengan saya yang memilih naik lewat dalam gedung, tangga manual atau lift, yang setiap perjalanannya bertemu denganmu sambil menjabat tangan atau melambai da-daa-da-daa.

Selamat hari libur bagi yang libur!

Selamat awal tahunan!

[Foto oleh Jan van der Wolf](https://www.pexels.com/id-id/foto/patung-kontainer-pengiriman-berwarna-warni-di-le-havre-29945184/)
