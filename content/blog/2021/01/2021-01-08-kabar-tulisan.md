---
title: "Kabar Tulisan"
date: "2021-01-08"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-miguel-a-padrinan-1591056-blog-rmdzn.jpg"
aliases:
  - /post/2021-01-08-kabar-tulisan/
---

Mengabarkan pencapaian dan menyampaikan apa yang dilakukan adalah salah satu momok saya. Ada ketakutan terhadap situasi gagal dan menurunnya semangat. _Saya hari ini akan mulai begini begitu, agar saya besok menjadi sesuatu_. Kemudian otak saya memberi kesan palsu bahwa "mengabarkan" dianggap sebagai salah satu proses "menjadi sesuatu". Padahal tidak. Otak pun delusi, merasa berdaya, tapi aslinya tak lagi mau bekerja.

Namun untuk blog ini, saya ingin mencoba bercerita.

Baru tahun 2020 lalu, saya bisa mengisi blog ini dengan lebih rutin. Dalam artian, menerbitkan satu minggu sekali. Pada hari yang sama pula.

Sebelumnya, saya menulis di blog ini dengan cara sesuai _mood_ (_moody_) yang kadang kala seperti program rapelan--dalam satu minggu bisa saja _mood_ untuk menulis satu bahkan lebih artikel berkibar-kibar dan minggu setelahnya melempem bukan kepalang. Sekarang tetap menulis sesuai _mood_, tapi _mood_\-nya lebih teratur dan mau diajak bekerja sama.

Begini wawasan blog saya pada tahun 2020 dengan tangkapan layar yang sedikit telat.

![Tampilan wawasan (insight) blog](/blog/2021/01/images/2021-01-06-14.56.15-wordpress.com-ed1067c05610.png)

Tampilan wawasan

Bulan Januari 2020, saya dapat menayangkan empat tulisan dengan pola lima hari sekali. Mulai Februari, saya ajeg menerbitkan tulisan satu minggu sekali, setiap hari Jumat. Totalnya menjadi empat kali seminggu. Meskipun bulan Maret menjadi enam kali.

Kesan tertib kembali bergeser pada bulan April 2020, tetapi rapi lagi mulai Mei. Ia menggambarkan awal pandemi yang membuat saya gugup dan tidak nyaman, sekaligus dapat menuangkan segala bentuk keresahan dalam waktu yang sama. Pada saat itu saya belajar, begitu menyenangkannya melayangkan protes lewat tulisan, baik untuk disampaikan langsung kepada orang atau sekadar unek-unek belaka.

Trafik tidak sekonsisten tulisan yang terbit. Naik dan turun, terutama saat akhir pekan yang mendorong grafiknya jeblok. Kisaran 50-150 tampilan halaman per hari.

Iya, memang tidak banyak.

Haha~

Namun, untuk saat ini, saya tidak akan memikirkan terlalu berat hasil yang didapat. Agar tak terasa tercengkeram dengan angka, grafik garis, dan apa saja yang menyertainya.

Menulis adalah _coping mechanism_ yang saya jalani. Ia menawarkan jalan untuk bercerita. Bergerak aktif, menggubah kata menjadi frasa lalu kalimat, yang saya usahakan, runtut. Cara _go public_ setelah sebelumnya menuliskan pikiran ke dalam jurnal pribadi. "Memaksa" saya untuk merapikan gambaran bebas dari jurnal yang cenderung "asal bercerita yang penting lega".

Peluruh ketidaknyamanan lain yang dijalani adalah membaca buku. Terserah genrenya, yang penting nyaman dinikmati. Tidak perlu berjam-jam, bermenit-menit saja cukup. Saya bukan tipe orang yang menargetkan membaca sekian buku dalam satu tahun. Seperti tujuan yang saya pikirkan, membaca untuk meluruhkan rasa cemas, takut, dan pikiran-pikiran tak menyenangkan. Dan belajar darinya bagaimana bercerita dengan oke punya.

Dan, yaa, tahun 2020 menjadi tahun saya bergabung di KaryaKarsa. Selain di blog, saya juga mulai sering mengisi akun [KaryaKarsa](https://karyakarsa.com/rmdzn), tempat saya menerbitkan cerpen eksklusif dan mengabarkan ulasan buku terbaru yang sebelumnya sudah saya tulis di sini.

Cerpen yang saya tayangkan gratis berjudul "[Independensi Rasa](https://karyakarsa.com/rmdzn/independensi-rasa)". Silakan kunjungi dan nikmati di sana. Sedangkan cerpen eksklusif pertama yang saya tulis adalah "[Penjaga Penghuni](https://karyakarsa.com/rmdzn/penjaga-penghuni)". Karya lain adalah pengubahan cerpen-cerpen saya dari blog ini menjadi berkas PDF--sekadar menawarkan alternatif dukungan bagi yang ingin mendukung saya di sana.

Hari ini, saya sedang ingin bercerita pendek dahulu. Kalau mau [dukung](https://ramdziana.wordpress.com/dukung/) saya secara langsung, mampir akun saya di KaryaKarsa, ya.

Yoot~~

\[Foto oleh **[Miguel Á. Padriñán](https://www.pexels.com/id-id/@padrinan?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/internet-teknologi-komputer-tampilan-1591056/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
