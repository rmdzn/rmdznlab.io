---
title: "Bureau dan Kratia"
date: "2021-01-15"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-nothing-ahead-5009177-birokrasi-rmdzn.jpg"
aliases:
  - /post/2021-01-15-bureau-dan-kratia/
---

Menjadi anggota pemuda selalu mengingatkan pada pencapaian luar biasa generasi Karang Taruna di kampung saya. Dengan wajah-wajah baru, yang sempat terpenggal generasi, tapi mampu melakukan sesuatu yang oke punya, yang saya tak cukup yakin apakah generasi sebelumnya pernah melakukan hal yang sama.

Beberapa waktu sebelum pandemi menyerang, sang ketua yang belum lama dilantik mengusulkan penanaman pohon perindang di sepanjang Selokan Mataram yang ada di wilayah kampung. Tentang ini sebagian sudah terdokumentasi di [tulisan saya sebelumnya](/blog/2020/01/2020-01-14-soal-lingkungan-anak-muda-perlu-gerak/).

Ada niatan bagus untuk memperbaiki lingkungan yang mulai tak sehat, kering, dan panas. Begitu pula dengan laju air yang tidak tertahan untuk diserap tanah. Di lingkungan semi urban yang pembangunannya cukup gencar, \*_batuk_\* kampung saya \*_batuk_\*, cor-coran dan konblok semakin menghiasi rumah lama dan rumah baru. Sebagian warga bahkan mulai mengalami sumur kering kala kemarau.

Semakin banyak bangunan renovasi dan bangunan baru, baik dari kalangan bisnis mau pun individu, seharusnya sebanding pula dengan usaha untuk membuat sarana mencari ganti pohon yang ditebas habis dan menciptakan lubang-lubang baru untuk menyalurkan air ke naungan alaminya.

Apa yang saya anggap hebat dari kisah kumpulan pemuda ini? Mereka fokus pada solusi. Bagaimana cara biar lingkungan lebih rindang, ramah terhadap yang cuaca panas maupun hujan? Dan, meski terpaksa, mau menghubungi pengelola Selokan Mataram, Balai Besar Wilayah Sungai Serayu Opak (BBWSO).

Syaratnya memang begitu. Jika ada pihak yang ingin menanam pohon tepat di pinggir selokan, mereka harus meminta izin kepada BBWSO, dengan menjelaskan jenis pohon dan foto pohon saat masih bibit dan sudah besar.

Apakah misi utama tersebut terealisasi secara sukses?

Tidak.

Surat jawaban dikirim beberapa minggu kemudian. BBWSO tidak mengizinkan penanaman pohon di pinggiran Selokan Mataram karena bisa merusak pembatas aliran. Meski pemuda yang mengusulkan pertama kali telah melakukan riset kecil-kecilan untuk memastikan akar pohon saat besar hampir tidak mungkin merusak sisi selokan, tidak tetaplah tidak.

Frustrasi? Iya. Siapa yang tidak mau wilayahnya rindang?

Merasa ringan? Iya, kami tak perlu repot wira-wiri menyiram dan memupuk anak pohon setiap pagi dan sore.

Pandemi yang-pasti-kalian-tahu membawa saya ke pengalaman tak mengenakkan. Pada pertengahan tahun 2020, Juni-Juli, angka positif masih terus menanjak. Namun, rasa adanya krisis (_sense of crisis_) tak kunjung muncul dari benak otoritas dan orang-orang.

Sepanjang jalan. Tidak jauh dari rumah saya. Hampir semua ruang bisnis yang diharapkan tetap berjalan dengan pritikil kisihitin justru tidak mengindahkannya. Bisnis lanjut tanpa protokol. Burjo atau Warmindo, warung nasi padang, penatu, jasa fotokopi dan rental komputer, penjual jus, konter pulsa, dll.

Tempat langganan sekaligus tempat kesayangan saya, bukan minimarket dan bukan warung makan, adalah salah satunya. Saat awal pandemi terjadi, mereka cukup paham dengan memasang batas penghalang sekitar setengah meter dari etalase kaca. Berminggu-minggu berlalu, batas besi itu dilepas.

Saya yang merasa berprivilese karena melanggan sejak tahun 2014, menyarankan mereka untuk kembali memasang batas. Untuk mengamankan pelanggan mau pun penjual yang berjaga di balik etalase. Saran saya ditanggapi dengan baik. "Saya akan menyampaikan saran ke pihak admin \[pengelola -red\]," jawab mereka melalui aplikasi obrolan.

Saran saya yang saya sampaikan bulan Juli 2020 itu tak pernah diterapkan sampai sekarang. Tanpa masker, tanpa pembatas, _looss dol, ra rewel~~_

Bersamaan dengan itu, saya melaporkan lewat media sosial kepada humas Kabupaten yang kemudian diteruskan ke humas Kecamatan, agar sosialisasi pritikil kisihitin diadakan di wilayah itu. Setidaknya mengirim jajaran berwenang. Sidak dan sosialisasi.

Apa respon humas Kabupaten atas laporan saya? Saya diminta menyebutkan nama usaha, kronologi kejadian, alamat lengkap/titik koordinat lokasi, dan foto (bila ada) untuk kemudian ditindaklanjuti Dinas Perindustrian dan Perdagangan setempat.

Bayangkan, ada puluhan tempat usaha di wilayah itu yang tak taat pritikil kisihitin dan saya perlu menyebutkan apa yang mereka minta. _Mbahmu!!_

Oiya, saya melaporkan masalah itu bulan Agustus 2020 saat Jogja tidak begitu ketat (baca: tidak ada PSBB). Kalau sekarang, saya bodo amat. Meski Jogja sok ketat dengan kebijakan PPKM, selama otoritas tak turun tangan membantu UMKM/tempat usaha kecil-kecilan dengan uang tunai, saya akan mengatakan kebijakan itu tidak sensitif dengan realitas di luar sana. _Eit_, tapi saya tetep mendukung 100% pelaksanaan pritikil kisihitin pada setiap tempat usaha yang dijalankan.

Rasanya seperti ini: kalian tahu bahwa niat kalian baik. Kalian semangat untuk mencoba mencari tahu sendiri serta mematuhi aturan atau birokrasi agar niat itu tercapai secara legal. Tapi, ternyata birokrasi itu menghambat aksi ideal kalian. Kalian harus menerima itu, sambil berpikir rendah dengan ketentuan di bawah.

1. Kita memang tidak lebih tahu daripada mereka.
2. Jika kita lebih tahu daripada mereka, kembali ke ketentuan pertama.

\[Foto oleh **[Nothing Ahead](https://www.pexels.com/id-id/@ian-panelo?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/kayu-sekolah-penelitian-book-5009177/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
