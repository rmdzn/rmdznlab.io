---
title: "Seperlunya"
date: "2021-01-22"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "cat-2483826_1500-benci-rmdzn.jpg"
aliases:
  - /post/2021-01-22-seperlunya/
---

_Kapan terakhir kali kamu dapat tertidur tenang?_

Jika batinmu bernada, kamu kalah!

Saya sedang mengutip baris pertama lirik _Secukupnya_. Kalau Baskara menceritakan perasaan khawatir akan nasib hari esok, saya tidak berujung lelap karena memikirkan sisi benci mana yang lebih enak saya tuju.

Apakah saya memilih rasa benci terhadap perkataan Nadin Amizah soal miskin-kayanya? Apakah saya akan memilih benci kepada gerakan BLM karena satu bule kepinding yang dengan cerdas mengajak kawan-kawannya untuk pindah ke Bali dan menerabas regulasi imigrasi?

Apakah saya perlu benci terhadap pembelokan narasi dari media lokal yang justru fokus pada orientasi seksual sang bule alih-alih usahanya melanggar regulasi dan memakmurkan kesenjangan dengan gentrifikasinya?

Atau apakah saya juga memilih benci terhadap pelanggar pritikil kisihitin pendorong gerobak jualan, pengorek gorengan di wajan bawah terpal pinggir jalan, atau supir truk yang berpeluh deras dan _ngos-ngosan_?

Daftar itu hanya pertanyaan gambaran kekinian. Saya pernah di situasi seperti itu. Memilih untuk membenci atau tidak. Kalau pun akhirnya memilih yang pertama, mana yang harus dibenci terlebih dahulu--sambil mencatat prioritas daftar benci dalam pikiran dan mempertimbangkannya dengan penilaian 1-5 bintang berikut ulasan-ulasan dari orang lain.

Benar, Bung! Rasa benci kalau diperjualbelikan di pasar daring (_online_) mungkin akan terpampang kriteria-kriteria semirip Shopee dan Tokopedia. Ditambah tingkat dengan bentuk bilah ke atas, berbatas warna hijau-kuning-merah-hitam yang menandakan seberapa marah target jika kita meluapkan rasa benci yang dibeli dari sana.

Pada bagian kategori, pelanggan dapat memilih dasar rasa benci; agama, ras, gender, orientasi seksual, sifat, dan sikap (pemikiran, perkataan, opini, tingkah laku, tindakan).

Saya masih memiliki setumpuk majalah yang menawarkan lebih dari satu kategori dalam setiap lembarnya. Tapi bukan dalam bentuk _to the point_, lebih ke cara majalah itu menyampaikan pendapat tokoh dan menyimpulkan dalam liputan. Menggiring pembaca mulai dari judul kepala berita yang terpampang di sampul sampai isi-isinya.

Saya juga memiliki buku kecil yang memuat daftar orang-orang dengan pemikiran "berseberangan"; riwayat kehidupan dan karya tulis yang pernah diterbitkan. Penulis buku tersebut mungkin tidak bermaksud mengajak untuk membenci--cenderung mengajak menghindari pemikiran orang yang keluar dari mulut mereka. Namun hasilnya malah bermaksud menolak mentah-mentah semua opini yang datang dari orang-orang yang ditulisnya, berpotensi membiasakan bias dan memelihara ruang gema (_echo chamber_).

Rasa benci tidak muncul tiba-tiba seperti bau kembang pasir yang timbul dari balik pot, tetapi ia secara langsung maupun tidak terpupuk sejak bibit hingga besar berakar kuat.

Berapa kali sih kamu mendengar gosip dari tetangga atau menyaksikan kejadian sendiri, sekecil apa pun itu, yang tersebar dan membuat kamu atau tetangga lain ikut membenci? Dengan gairah pula.

Anak tetangga yang pulang malam yang bikin kamu berpikir tidak-tidak? Apa lagi? Tetangga yang tiap pagi berjalan bersama anjingnya lewat depan rumah? Anak-anak yang setiap siang berteriak bising dari gang samping? Mercon dan kembang api berisik yang selalu disulut setiap hari raya?

Benci adalah produk merakyat. Dari yang sederhana sampai yang rumit. Dari pinggiran sampai kaum tengah kota yang setiap saat bicara _ndakik-ndakik_.

Iprus mendapuk dirinya sebagai manusia maju, hidup di antara kota megah dan kota pinggiran. Bacaannya buku-buku tebal dan buku digital berukuran belasan mega. Perkataannya _thas thes_ seperti seorang juru bicara yang sedang menyingkapkan rencana revolusi negeri.

Iprus hanya karakter rekaan. Namun rasa bencinya terhadap orang yang tidak sepaham selalu kuat, terlihat nyata. Ia menampik hampir semua argumen personal mau pun yang berhubungan dengan publik dari orang lain yang membantah dirinya.

Begitu juga dengan Supri yang sehari-hari berpakaian paling agamis dibanding tetangga-tetangganya. Ucapannya menyudutkan, selalu menyalahkan manusia-manusia bebal di sekeliling atas segala hal buruk yang terjadi di dunia. Supri menutup telinganya dari pemikiran orang yang terlabeli sesat.

Agar kalian tahu, Supri pun rekaan.

Benci adalah produk universal. Ia produk manusiawi. Menjadi masalah ketika rasa benci dilempar secara membabi buta. Apa yang ada di pikiran langsung disembur dengan kencang, tidak memikirkan dampak ke diri sendiri dan ke sekitar.

Tentu, meski manusiawi, benci perlu dikendalikan. Ia perlu ditangkap, dipegang kepala dan ekornya agar tidak menyeruduk dan menyabet barang di sekelilingnya.

Seperti marah, benci dapat diuraikan dalam bentuk perasaan yang lebih akurat. Dengki, kah? Iri? Sombong? Sedih dan marah karena sesuatu? Kalau sudah terdeteksi, kini giliran mengakuinya sebagai perasaan yang tak sehat, lalu meredamnya. Pelan-pelan. Tak perlu terburu-buru. Hingga menjadi perasaan seperlunya kemudian lenyap. Jika pun rasa benci itu timbul lagi, ulangi dari awal.

Omong-omong, jangan anggap saya sebagai seorang profesional (karena memang bukan), anggap saja kawan yang juga sama-sama sedang belajar melakukannya.

\[Gambar oleh [Deedster](https://pixabay.com/users/deedster-2541644/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2483826) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2483826)\]
