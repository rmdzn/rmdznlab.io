---
title: "Menjadi Tua"
date: "2021-01-29"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-noelle-otto-906111-tua-rmdzn.jpg"
aliases:
  - /post/2021-01-29-menjadi-tua/
---

Muda pasti. Tua belum tentu. Kecuali situ belum lahir, keduanya jelas tidak pasti dan tidak mungkin terjadi.

Lalu, apabila manusia tidak dilahirkan, apakah dia juga bisa disebut _manusia_? Disebut Makhluk X mungkin? Kalau begitu jika dulu sampai sekarang Makhluk X tidak juga dilahirkan, apakah dirinya tetap dianggap tua dalam kehidupan misterius di luar sana?

Aah …

Jangan hiraukan. Ini hanya pikiran selintas saat mandi pagi lalu.

Tua adalah bukti waktu berjalan. Seperti bukti gaibnya bentuk enek yang tergambar pada pewangi mobil Stella dan bukti gaibnya bentuk harum menyeramkan yang terlihat pada kembang kantil. Tua itu gaib, tapi dapat dibuktikan dari kulit keriput, tubuh yang layu, rambut memutih, serta perubahan fisik lain karena waktu yang terus berjalan.

Titik tua setiap orang berbeda. Orang yang pola hidupnya sehat semasa muda, titik tua mungkin lebih lama. Sedangkan yang pola hidup semasa muda setingkat warga biasa; hidup dengan gorengan, kebanyakan konsumsi gula, begadang, jarang olahraga, dsb titik tuanya mungkin lebih cepat.

Itu baru soal fisik, belum jika membahas konsep tua yang ditandai dengan menurunnya ingatan. Orang yang tidak melatih ingatannya sejak muda, mungkin akan lebih cepat pikun. Kecuali jika masa mudanya sering bermain kuis, mengisi teka-teki silang, dan main catur.

Fisik dan ingatan. Dua simbol klasik penuaan. Tapi beberapa waktu terakhir saya baru menyadari, ada simbol lain yang terlewat. Simbol yang seharusnya mudah dimengerti dari pengalaman sendiri atau orang lain: teknologi dan [pengetahuan](/blog/2020/07/2020-07-17-gagap-medsos-sampai-ke-ubun-ubun/) yang mengiringinya.

Di tempat saya melayani jual beli pulsa, seorang anak kecil sekaligus tetangga generasi _[centennial](https://digitalcommons.kennesaw.edu/cgi/viewcontent.cgi?article=1194&context=ama_proceedings)_ membeli pulsa sebanyak 5 sampai 50 ribu. Tentu bukan untuk mengirim pesan SMS atau menelepon doi seperti kamu belasan tahun lalu, tapi membeli kulit dan senjata di gim perang kolosal maupun tembak-tembakan di ponsel.

"_Nggo tuku skin XXXX, apik, Le!_1" teriaknya kepada teman di samping.

Saking _de facto_\-nya WhatsApp sebagai aplikasi berkomunikasi, saya jadi tidak yakin anak-anak ini masih dapat menggunakan teknologi "jadul" SMS dan telepon biasa. Serta meragukan kemengertian kegunaan dasar dari angka-angka di belakang "Sisa pulsa Anda sebesar …" di ponselnya.

Saya juga tidak percaya bahwa mereka yakin tetap bisa menghubungi kawan-kawannya meskipun paket datanya habis atau sinyal 4G-nya buruk padahal pulsa masih tersisa banyak di sana. Jika ada sisa pulsa, ia lebih sering dipakai untuk membeli "perlengkapan" gim mau pun mengisi ulang paket data yang (hampir) habis.

Karena membayangkan majunya tenologi, saya jadi ingat hal lain. Dahulu, saya sempat mengalami komunikasi lewat surat tulisan tangan. Bukan pengalaman sendiri. Saya hanya menyaksikan bapak dan ibu yang mendapatkan kiriman surat dari adiknya di Jawa bagian barat, lalu bergegas menulis jawabannya beberapa hari kemudian untuk dimasukkan ke amplop dan dikirim lewat burung hantu kantor pos.

Anak milenial yang mengetik tulisan ini pun akhirnya mau tak mau harus mengakui bahwa manusia yang pernah menyaksikan surat-menyurat, menggunakan ponsel monokrom atau poliponik dan kini mampu beradaptasi cepat dengan ponsel pintar Android sudah dapat dianggap tua--tidak perlu menunggu punuk melengkuk, punggung nyeri setiap bangun pagi, dan lupa siapa nama putra keempatnya.

Namun, sungguh, meskipun anak milenial tampak hebat karena mampu melewati sebagian teknologi jadul dan mampu beradaptasi cepat dengan teknologi kekinian, tidak perlu ada penyombongan antar satu generasi dengan generasi lain.

Saya pun tak mungkin memongahkan diri sebagai "tua". Siapa saya dibandingkan eyang putri dan kakung yang masih sehat dan _sugeng_ yang berjalan dan bersepeda setiap kali pergi ke sawah atau pasar?

Generasi _centennial_ dan _alpha_ juga kedepannya mungkin akan mengalami hal sama. Pada awal kelahiran, dunia mereka sudah dilengkapi dengan internet dan teknologi komunikasi yang maju. Puluhan tahun kemudian, teknologi bisa saja semakin modern; tak perlu lagi ponsel untuk menelepon, tak perlu lagi menginjakkan roda atau kaki di tanah untuk pergi ke luar rumah, dsb, dan saat itu giliran mereka yang akan merasakan putaran hidup baru ...

Menjadi tua.

1: _"Buat beli skin XXXX, bagus, Coy!"_

\[Foto oleh **[Noelle Otto](https://www.pexels.com/id-id/@noellegracephotos?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/pasangan-tua-berjalan-sambil-berpegangan-tangan-906111/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
