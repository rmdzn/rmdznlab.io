---
title: "Menyemai Filsafat dari Dunia Sophie"
date: "2021-02-05"
categories: 
  - "buku"
tags: 
  - "buku"
coverImage: "dunia-shopie-unggulan-rmdzn.jpg"
aliases:
  - /post/2021-02-05-menyemai-filsafat-dari-dunia-sophie/
---

Ini adalah kali kedua saya membaca _Dunia Sophie_ dengan selisih waktu yang tidak jauh dari saat saya membacanya pertama kali. Dan inilah kali pertama saya melakukannya terhadap satu judul buku.

Buku ini sering disebut sebagai buku perkenalan filsafat yang ringan, yang ditumbuhkan dalam bentuk novel, tapi bagi orang yang baru pertama membaca topik seputar filsafat, _Dunia Sophie_ masih cocok dikatakan sulit. Titik ini yang membuat saya perlu membacanya kembali sebelum memutuskan untuk menulis ulasannya.

Sejak bahasan pertama, _Dunia Sophie_ menampar orang-orang dewasa yang selalu menganggap dunia ini biasa saja. Begitu normal. Berbanding terbalik ketika mereka masih bayi, masih kecil, yang selalu melihat dunia dengan kekaguman--karena si bayi yang baru merasakan kehidupan dan penasaran tentang apa saja yang dilihat.

Ini akibat dari pembiasaan hal-hal yang diasumsikan "biasa". Coba jika situasinya dibalik. Kira-kira, jika suatu hari ada orang melayang (terbang) di depan mata, orang dewasa atau bayikah yang lebih kagum dan tertarik terhadap peristiwa itu?

Bisa dipastikan orang dewasa yang akan bereaksi begitu. Karena terbang bukanlah kejadian biasa. Sedangkan bayi akan tetap kagum, tetapi dengan level kekaguman yang sama saat mereka melihat orang dewasa berjalan, berlari, dan menari. Dalam asumsi mungil bayi, terbang adalah aktivitas normal manusia.

Seorang pria yang secara tiba-tiba mengajarkan dunia filsafat kepada anak sekolahan bernama Sophie menyebutkan fenomena ini seperti kita yang tinggal di balik bulu-bulu kelinci yang sedang ditarik dari topi oleh pesulap. Semasa bayi, kita berusaha naik ke ujung bulu untuk melihat apa yang terjadi di luar, semakin dewasa, kita justru terus ditarik ke bawah. Filsafat lah ilmu yang akan kembali menarik ke atas orang-orang dewasa, untuk menemukan hal-hal misterius di luar badan kelinci.

Filsafat hadir sebagai pemikiran berdasar akal atau bukti. Sebelum ia ada, zaman itu orang Yunani percaya segala fenomena alam di dunia yang terjadi disebabkan oleh perbuatan dewa-dewa. Thor yang memukulkan palunya hingga menyebabkan hujan dan petir salah satunya. Filsafat datang lalu menjelaskan terjadinya fenomena tersebut dengan lebih alamiah.

Dunia filsafat bukan tanpa celah. Pada era filsafat pertama, seluruh pemikiran hanya keluar dari pria. Kaum wanita saat itu masih dibelenggu.

![](/blog/2021/02/images/dunia-sophie-rmdzn.jpg)

Sejarah terulang dan berputar. Ternyata pada masanya, di Yunani pernah muncul filsafat yang selalu menganggap semua kejadian di dunia sudah ditakdirkan dari sana (berhubungan dengan spiritual). Termasuk penyakit influenza yang ditimbulkan oleh efek jahat dari bintang-bintang. Zaman sekarang pun masih ada yang begitu dengan menganggap AIDS ada karena hukuman Tuhan (hal. 101).

_Dunia Sophie_ memperkenalkan banyak filsuf. Ada Socrates. Salah satu filsuf terbesar (terkenal) sepanjang masa yang diceritakan dengan apik. Socrates berwajah buruk. Tapi memiliki kemampuan unik untuk menyadarkan akal sehat dengan cara mengajak diskusi dan berpura-pura tidak tahu apa-apa. Hasilnya, lawan bicara menjadi malu dan menyadari kesalahannya. Sebuah cara diskusi yang dinamai "ironi Socrates".

Perkataan filsuf ini yang cukup populer adalah "_orang yang bijaksana adalah orang yang tahu bahwa dia tidak tahu._"

Ada juga murid Socrates, Plato, yang mengutarakan ihwal dunia ide, jiwa yang kekal, dan penjelasan perlunya orang untuk penasaran dan mencari tahu tentang asal muasal sesuatu alih-alih hanya menikmati bentuk akhirnya. Pun Plato lah filsuf pertama yang mendukung sekolah anak-anak yang diorganisasi oleh negara serta konsep pendidikan waktu penuh.

Berbeda dengan Socrates yang menganggap wanita adalah kaum lemah dan tidak berdaya, Plato justru melihat sisi positif dari mereka. "_Negara yang tidak melatih perempuan sama dengan orang yang hanya melatih tangan kanannya._"

Filsuf lain yang diperkenalkan adalah Democritus yang merumuskan konsep atom, Aristoteles, Diogenes dengan filsafat sinisme, Zeno dengan stoisisme, dan Epicurus. Lalu ke filsuf zaman berikutnya, Thomas Aquinas, Goethe, Giordano Bruno, dst. Dan ke yang zaman kini, Sigmund Freud dengan psikoanalisis populernya dan Friedrich Nietzsche, dst.

_Dunia Sophie_ juga dilengkapi dengan penceritaan tentang pengaruh Indo-Eropa terhadap kebudayaan, pemikiran filosofis. Termasuk kata "Dewa" dan salah satu namanya; Zeus (Yunani) yang sama dengan dewa Jupiter (Latin) dan dewa Tyr (Norwegia Kuno).

Teori dan penjelasan yang terlihat ramai mengenai filsafat dan tokohnya dari zaman ke zaman bisa dibundel dalam kisah penggambaran waktu yang apik. Pada bab Abad Pertengahan, Alberto (guru Sophie) menandai kelahiran Yesus sebagai permulaan hari, pukul 00.00, lalu setiap satu jam ke depan menyimbolkan 100 tahun kemudian. Pukul 01.00 sama dengan 100 tahun, pukul 06.00 sama dengan 600 tahun, pukul 14.00 sama dengan 1.400 tahun, dst.

Cerita di atas hanya secuil pelajaran filsafat. Masih banyak hal lain di belakangnya.

Kisah petualangan filsafat Sophie dengan guru bernama Alberto ini mengingatkan saya pada film _Inception_. Coba saja baca, kepala kalian akan meledak nantinya. Plotnya sederhana. Yang bikin _wow_ adalah siapa itu Alberto dan Sophie dan dunia apa yang mereka tinggali.

Mizan, sebagai penerbit, dalam pengantarnya menyebutkan _Dunia Sophie_ yang bias. Ia fokus pada dunia Kristen dan melewatkan segala kontribusi filsafat dari dunia Islam. Meski begitu, bukan berarti _Dunia Sophie_ isinya tak bisa jadi pegangan belajar filsafat. Ia layak menjadi rujukan pertama untuk mengenal lapisan atas ilmu ini.

Kurangnya buku ini ada pada terjemahannya. Cenderung kaku. Pada kalimat tertentu tidak begitu nyaman dibaca. Misalnya pada halaman 94 yang dalam satu paragraf terlalu banyak kata _bahwa_.

"_\[…\] Namun teringat juga olehnya bahwa banyak orang beranggapan bahwa berdoa dapat membantu penyembuhan. Jadi bagaimanapun, mereka pasti percaya bahwa Tuhan mempunyai kekuasaan atas kesehatan orang-orang._"

Bisa lah salah satu dari dua _bahwa_ di atas disingkirkan.

Pada bagian tertentu, terjemahan juga terkesan kaku. Di halaman 342, sang ibu berkata, "_Sophie--kamu harus menyatakan padaku mengapa kamu tampak kehilangan keseimbangan saat itu._"

Saya belum pernah melihat bahasa asli buku _Dunia Sophie_. Namun, melihat bagaimana frasa _kehilangan keseimbangan_ dipakai, sepertinya akan lebih nyaman dengan kata _limbung_, _linglung_, atau _bingung_.

Meski begitu, sekali lagi, saya merekomendasikan buku ini kepada kamu yang ingin mempelajari filsafat melalui kisah "ringan". Dan jangan ragu untuk membacanya ulang jika ingin lebih paham. Seperti kata Hilde, si pembaca _Dunia Sophie_, "_cerita itu tidak dapat dituntaskan dengan membaca hanya sekali_." (hal. 746).

Buku ini bisa bikin saya bilang, "_ternyata ilmu ini itu sudah dipikirkan para filsuf sejak lama. Lamaaa bangeeett._"

* * *

**Judul**: Dunia Sophie  
**Penulis**: Jostein Gaarder  
**Penerbit**: Penerbit Mizan  
**Tebal**: 800 halaman
