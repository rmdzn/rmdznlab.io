---
title: "Kitab Epitoerets"
date: "2021-02-12"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-pixabay-415071-buku-rmdzn.jpg"
aliases:
  - /post/2021-02-12-kitab-epitoerets/
---

"Tahan pintu itu!"

"Jangan biarkan mereka mendobrak!"

_BRAK!!_

_BAM!_

Gerombolan manusia menggelendoti pintu yang sudah dipalang besi sampai ambruk. Beruntung, kami berhasil naik ke pesawat Chinnok tidak lama setelah dua orang kru melemparkan tali tangganya ke bawah untuk kami naiki.

…

Jika suatu hari saya diberi mandat untuk mengoordinasi satu peleton infanteri amatir saat kiamat zombie terjadi, saya akan memilih generasi yang lahir di atas tahun 2010-an. Angkatan generasi Z terakhir menuju generasi alfa.

Di samping karena usia yang kala zombie menyerang cenderung lebih muda, mereka mempunyai kelebihan dibandingkan generasi sebelumnya, akibat berpengalaman "terjebak" di antara kultur orang-orang tua/dewasa yang, secara sederhana, konservatif dan progresif beserta teknologi pengalih perhatian di sekelilingnya. Lalu mau tak mau terombang-ambing dan memilih untuk bersikap seperti apa saat remaja dan ketika menuju dewasa.

Bagaimana dengan generasi di atasnya? Tidak bagaimana-bagaimana. Hanya saja pergulatan pikiran mereka sudah pernah mencapai klimaksnya, itu pun dengan distraksi yang minim, dan mereka lah yang kini menurunkan pikiran ke generasi kanak-kanak berikutnya.

Tak perlu basa-basi, _deh_.

Sina, anak perempuan yang kini sedang duduk di kelas 4 SD, bermain hujan-hujanan hari Jumat lalu. Bersama kawan-kawannya, berlarian kesana kemari dan tertawa~~

Seorang ibu yang juga tetangga lewat dan sore hari berikutnya mengolok dengan bahasa Jawanya yang kental. "Anak perempuan, kok, main hujan-hujanan. Memalukan dan tidak elok! Kalau hujan, tidur di rumah!"

Tahukah kamu apa yang terjadi dengan gestur dan raut muka Sina? Dia sedikit menundukkan muka lalu mendongak agak lesu. Gerakan tubuhnya terasa tidak natural, canggung.

Pada hari yang lain, Kundi, anak laki-laki yang usianya kurang lebih sama dengan Sina, berjalan bergerombol dengan teman-temannya. Bukan dengan geng yang biasa ia berkumpul, tetapi dengan anak-anak perempuan.

Kebetulan, ibu-ibu yang pernah menyemprot Sina kembali mengolok Kundi. "Teman-teman laki-lakimu mana? Anak laki-laki itu mainan layangan. Sana main layangan, bukan jalan-jalan sama perempuan!" Saya tidak tahu bagaimana wajah dan gestur Kundi karena saya sedang dalam jarak dan posisi yang tidak mungkin untuk melihatnya.

Saya tidak dapat membesarkan hati Kundi, tapi saya sempat berbicara langsung kepada Sina setelah ibu pengolok itu berjalan pergi.

"Anak perempuan hujan-hujanan tidak apa-apa. Kalau memang ibu \[ibu Sina\] mengizinkan, masa tidak boleh? Yang boleh hujan-hujanan tidak cuma laki-laki, kok."

Saya juga bercerita pendek bahwa dahulu ketika seumurannya saya bahkan ikut bermain masak-masakan bersama Tante Kiki.

"Bener, Om?"

"Iya. Tanya saja Tante Kiki."

Sina tersenyum.

Saya tidak tahu pasti apakah perkataan saya memang benar-benar melegakan hatinya. Tapi, ya, memang semoga begitu.

Dalam ensiklopedia fiktif berjudul Epitoerets (dibaca _epitorets_ atau _epiturets_). _Wanita (perempuan)_ dituliskan sebagai makhluk lemah suka merengek dan tidak tahan banting. Dia hanya boleh memasak dan mengurus kerja-kerja rumah. Pekerjaannya juga hanya melingkupi sisi feminitas. Tidak jauh dari rok, riasan, dan ruangan tertutup.

Begitu juga _pria (laki-laki)_ yang dituliskan sebagai makhluk kuat. Makhluk lengkap dan penuh energi. Tidak menangis, tidak boleh merasa lemah. Berkumpul dengan kawanan pria kuat lainnya. Bekerja di dunia maskulin. Selalu dekat dengan celana jins, celana gunung, baju kargo, dan ruangan terbuka. Penuh gertak dan gerak.

Hamba ensiklopedia _Epitoerets_ cukup, bahkan mungkin sangat banyak. Generasi tua adalah dedengkot pengiya si ensiklopedia lalu diteruskan kepada generasi termuda paling _mlethik_ saat ini. Milenial.

Karena begitu suksesnya penyebaran pengertian kata _Wanita (Perempuan)_ dan _Pria (Laki-laki)_ menurut Epitoerets, dalam taraf tertentu, ia menjadi kitab yang diamalkan secara naluriah. Tersebar secara alami tanpa perlu dibacakan lewat buku dengan pengeras suara atau ditulis besar-besar di spanduk. Meskipun untuk saat ini penyebaran lebih pesat justru melalui teks-teks yang dikirim lewat kabel bawah lautan dan gelombang sinyal.

Para pendengung ensiklopedia rasa kitab, Epitoerets, itu benar-benar membosankan. Mereka merasa paling mengerti dan berpengalaman lalu mendominasi segala narasi yang berkembang di lingkungan. Dalam bahasa Jawa, mereka bersikap _dumeh_. Merasa pernah kecil dan muda, lalu melibas praktik yang mereka anggap menyalahi kebiasaan lampau.

Saya cuma berharap tidak akan menjadi manusia tua seperti itu.

...

Sehingga pada waktunya saya bisa bebas memilih rekan yang saya ajak memegang senapan laras panjang dari atas gedung, siapa yang meluncurkan suar, siapa yang memegang senapan jarak pendek, siapa yang memegang tongkat bisbol, siapa yang menyediakan logistik, tanpa perlu repot memandang gendernya. Mereka lah yang sadar dengan kemampuan dan minatnya sendiri.

Bayangkan jika saya mengajak seorang putri yang larinya terkenal gesit untuk siaga memegang pistol, bertahan hidup sambil berlari menghindari kumpulan makhluk yang dulunya manusia, lalu mengumpulkan sisa-sisa senjata yang tergeletak di apartemen seberang jalan.

Suasana sungguh tegang. Kemudian dari belakang sepasang suami istri berteriak lantang, "anak perempuan bantu masak-masak di dapur! Malah masuk gedung reyot gak jelas, pas gerimis lagi, dasar malu-maluin keluarga. Pulang!!"
