---
title: "Rehat atau Pergi dari Media Sosial?"
date: "2021-03-05"
categories: 
  - "cuap"
tags: 
  - "media-sosial"
coverImage: "pexels-prateek-katyal-2694434-medsos-rmdzn.jpg"
aliases:
  - /post/2021-03-05-rehat-atau-pergi-dari-media-sosial/
---

Saya berjalan di taman. Melihat anak-anak bebek mengekor sang induk di rerumputan dekat sungai. Dua sampai tiga orang berdiam di masing-masing sudut taman, menunduk menatap buku. Duduk di kursi, bertikar di bawah pohon, dan di atas balok bata yang disusun rapi di tanjung kolam.

Dua insan bergandeng tangan membicarakan hal acak sambil terkikik, pemuda lain berkumpul sambil bermain _jenga_, domino, dan UNO. Tiba-tiba, seorang anak berlari lalu jatuh terjerembab dan sekelompok orang tua menolongnya sambil berteriak marah-marah.

Langkah kaki saya selalu ke sana, menyapa mereka dan membicarakan apa yang ada di kepala.

Saya ingin merekam semua keindahan dan keributan itu. Saya ingin memotret mereka lalu mencetaknya di album khusus, dengan teks yang menarik perhatian, sembari berpikir tulisan apa yang perlu saya tempelkan di poster depan rumah dua hari lagi.

Saya adalah saya yang sedang berkeliling di media sosial--tergambar sebagai taman. Segudang distraksi menunggu di sana. Keindahan. Kecantikan. Kebahagiaan. Ketertarikan. Kelambanan. Keterburu-buruan. Ketidaknyamanan. Keirian. Kemarahan. Dan mungkin kengerian.

Saya merespon sebagian situasi itu. Sementara, di sisi lain saya harus melakukan sesuatu yang lebih penting dalam tenggat waktu tertentu--termasuk menciptakan tulisan di blog ini. Otak ini kewalahan, meletup lalu berasap. Tangan pegal. Fokus hilang.

Saya selalu kagum dengan orang-orang yang mampu melepaskan media sosial secara total atau setidaknya sementara. Membuang candu darinya dan menyingkirkan residu-residu yang dihasilkannya. Maksud saya, bukan hanya yang mampu melepas media sosial secara fisik--mengerjakan suatu hal tetapi pikiran berputar ke sana--melainkan yang benar-benar menanggalkannya.

Dua "tokoh" menjadi lambang dari kisah seperti itu.

[Lana Blakely](https://www.youtube.com/user/brodernaho) bercerita tentang alasan dirinya memilih rehat sejenak dari media sosial (kecuali YouTube, kalau memang ada yang menganggapnya sebagai media sosial). Media sosial telah mendistraksi, mengubah minat dan niat dari semula yang ingin sekadar merekam suasana adiwarna menjadi hanya mengumpulkan kepopuleran belaka.

Dia tidak berada di kutub ekstrem; menetap atau meninggalkan selamanya. Dia hanya istirahat sambil menikmati hangatnya cuaca kota Stockholm yang kini sedang diguyur sinar mentari banyak-banyak, melihat pemandangan, dan berjalan dengan teman terdekat.

https://www.youtube.com/watch?v=7LvetOr4pPY

Beda dengan Muhammad Khan, aktor di sinema _Kucumbu Tubuh Indahku_. Dia tidak hanya menanggalkan media sosial, tetapi kembali menjadikan "ponsel bodoh" (_feature phone_ atau _dumbphone_) sebagai ponsel utama.

Dalam wawancaranya dengan Marissa Anita--[Greatmind.id](https://greatmind.id/article/bercakap-bersama-muhammad-khan-meninggalkan-smartphone), sang pemeran Juno mulai meninggalkan ponsel pintar bulan Desember 2020. Kini, dia hanya mengandalkan "ponsel bodoh" sebagai alat komunikasi sehari-hari dan tablet berukuran besar untuk melihat peta dan membaca surel. Tablet berukuran besar memang sengaja dipakai agar ada usaha lebih untuk mengeluarkan dan menggunakannya, beda dengan ponsel pintar mungil yang cenderung lebih mudah keluar masuk kantong.

"Aku merasa, mungkin karena _smartphone_ itu gampang di raih dari saku, sehingga kemampuan mendistraksi penggunanya sangat cepat," tuturnya.

Keluar dari media sosial adalah memori personal bagi saya. Saya pernah [keluar dari Facebook](https://ramdziana.wordpress.com/2020/09/25/pergi-dari-si-biru-bermuka-buku/), menghapus sebersih-bersihnya. Saya juga meninggalkan akun Twitter dengan nama asli saya (akun masih ada tetapi sudah tidak aktif). Bahkan juga akun Telegram yang terhapus secara otomatis karena lama tidak aktif, meskipun isi kontak, obrolan, dan grup sedang ramai-ramainya.

Saya baru menyadari, saya memang sedang ingin istirahat. Situasi kala itu kurang begitu saya pahami. Rasanya seperti ingin pergi begitu saja.

Saya memang pernah melakukan "puasa" media sosial, secara tak sengaja. Tapi karena itu, saya mengagumi orang-orang yang mampu melakukannya dengan niatan. Tahu apa yang memang diinginkan untuk pikiran dan jiwa sendiri.

Sebelum menghapus akun Facebook yang terbengkalai berbulan-bulan, saya membuka diri di tempat lain, pelan-pelan, dengan persona lain. Persona hampir autentik yang berbeda 170° dari media sosial saya bertahun-tahun sebelumnya. Sarana untuk berbagi dan berkomentar, lalu mengikuti akun yang memang saya ingin ikuti konten-kontennya.

Kemudian terbayang, jika saya ingin istirahat dari media sosial lagi, pikiran-pikiran menakutkan yang tidak pernah saya duga mulai bermunculan. "Saya sedang membangun citra, kalau saya pergi satu hari saja, bagaimana kalau nanti pengikut saya berkurang?", "saya sedang membangun audiens, kalau saya pergi, tidak ada potensi pengikut baru dari unggahan baru saya, _dong_."

> Aiss, pikiran _sok-sokan_, ketakutan yang tak berarti, padahal aku dan akunnya saja tidak besar.

Yang pasti dan jelas, kalaupun nanti saya ingin rehat lama atau sebentar dari media sosial, blog ini masih akan tetap ada untuk ajang berbagi dan menyapa.

_Sebentar, iring-iringan bebek tadi pergi kemana, ya?_

\[Foto oleh **[Prateek Katyal](https://www.pexels.com/id-id/@prateekkatyal?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/heart-and-zero-neon-light-signage-2694434/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
