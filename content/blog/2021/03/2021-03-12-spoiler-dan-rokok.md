---
title: "'Spoiler' dan Rokok"
date: "2021-03-12"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-luis-quintero-2339722-keran-bocor-rmdzn.jpg"
aliases:
  - /post/2021-03-12-spoiler-dan-rokok/
---

Ada Indomie vs Mie Sedaap.

Ada bubur diaduk vs tidak diaduk.

Ada makan nasi padang pakai sendok vs pakai tangan kosong.

Kalau di dunia hiburan, ada _pro-spoiler_ vs _anti-spoiler_. Namun, sepengelihatan saya sampai saat ini, kelompok kedualah yang lebih menggaung daripada yang pertama. Bahkan, saya akan dianggap mengada-ada kalau dunia ini dihidupi oleh keeksistensian kelompok pertama.

Nyatanya kelompok pertama benar-benar ada, hanya saja terkesan berkamuflase sebagai rakyat biasa karena tertutup oleh koar-koar kebencian orang terhadap para penyulut bahan bakar (penebar _spoiler_). _Pro-spoiler_ akan menikmati kontroversi itu dengan duduk tenang, mengonsumsi _spoiler_ yang tersebar dari para produsennya sembari menyantap keripik kentang di atas tikar atau sofa.

Saya pernah mempunyai teman daring. Panggil saja Dewi. Penggila sepak bola dan penyuka film animasi Jepang, anime. Dia mengaku penyuka _spoiler_. Saya sudah lama tidak berkontak dengannya, tapi saya ingat, _spoiler_ adalah alatnya untuk mempertimbangkan layak tidaknya judul anime ditonton.

Begitu juga seorang pengguna Twitter lain yang juga penikmat _spoiler_\--yang sempat saya balas cuitannya beberapa waktu lalu. Tuturnya mirip seperti Dewi. _Spoiler_ adalah upayanya untuk menilai tayangan--ingin menontonnya atau tidak.

Dari Dewi dkk, saya jadi memahami, beragam cara untuk menikmati sebuah tayangan. Ada yang ingin mengikuti alur cerita lalu dikejutkan tiba-tiba, ada yang menyiapkannya sejak lama; mungkin melihat siapa yang mati, siapa yang menjadi raja, siapa yang menjadi raksasa, siapa yang … dan seterusnya … serta, ada _plot twist_ atau tidak.

Jika para penebar _spoiler_ mempelajari materi pemasaran, Dewi dkk merupakan konsumen berdaya beli tinggi. Ceruk pasar. Target yang harus segera disasar dengan sedikit atau banyak strategi. Sehingga mereka tak perlu lagi membuang-buang hembusan nafasnya secara percuma kepada yang bukan peminatnya. Termasuk saya.

Malam itu adalah jadwal siskamling pemuda. Malam Minggu. Sebagai hiburan sebelum keliling kampung, kami biasa menonton film melalui _flashdisk_ yang dicolok ke TV layar datar di gardu siskamling. Di tengah mata menatap layar dengan serius, salah satu anggota pemuda mengisahkan alur cerita. Setelah ini, itu. Setelah begini, begitu. Si ini nanti begitu.

Suasana hati saya langsung anjlok. Saya yang menyukai kejutan dan analisis bebas tentang apa yang akan terjadi selanjutnya, langsung berhenti menonton. Memilih untuk menonton ulang pada hari berikutnya di rumah.

Di tempat lain. Seorang _YouTuber_ yang sehari-hari menampilkan dirinya bereaksi terhadap film (_reactor_), mendapati _spoiler_ beberapa hari sebelumnya melalui kolom komentar video dan kolom komentar akun Patreon yang tak sempat termoderasi.

Dalam video unggahan "terbaru", wajahnya datar, mimiknya tidak begitu menyenangkan. Penonton pun ikut tidak nyaman karena seharusnya mereka dapat melihat reaksi kaget dan terkejut, sebelum kemudian ia meminta maaf kepada penonton karena reaksinya tidak seperti yang diharapkan. "Ini karena _spoiler_ yang saya dapatkan kemarin," jelasnya.

Kalau saya menjadi sang _reactor_, saya juga pasti mangkel bukan kepalang. Menonton sebuah acara dengan segala plot yang tidak diduga adalah cara melayang tanpa narkotika, cara hangat tanpa anggur merah, cara melepas endorfin tanpa perlu menggunjingi tetangga.

Penebar _spoiler_ berucap dengan gaya paling tahu. Mengasumsikan si pendengar bahagia setelah mendengar celotehannya yang tanpa _consent_ itu.

Ya, mereka menyebalkan. Tapi penebar _spoiler_ tak perlu diusir dari negara, tak perlu juga dipendam sedalam-dalamnya di bawah ibu bumi, atau dijatuhkan ke neraka tingkat lima. Mereka hanya perlu diarahkan. Diberitahu bahwa mereka boleh melakukannya dalam ruang nonpublik.

Penebar _spoiler_ tidak mungkin disingkirkan, mereka masih akan tetap ada. Seperti perokok di dunia, terutama, di negeri ini.

Perokok yang tidak paham etika akan tetap menyulut batang rokoknya di sebelah ibu hamil di teras masjid atau mengepulkan asapnya di samping anak-anak di halaman sekolah. Penebar _spoiler_ yang tak beretika akan menyampaikan isi kepalanya di ruang publik atau di tengah teman-teman yang tidak memerlukannya.

Perokok pun masih jauh lebih baik. Meski tak sedikit orang yang membencinya, perokoklah yang membiayai anggaran kesehatan dengan cukai yang mereka bayar. Rokok juga hampir menjadi kebutuhan pokok; menghibur, melegakan pikiran dari beban hidup yang menjadi-jadi. Tidak dengan penebar _spoiler_ yang hanya bermanfaat untuk para penikmatnya.

Untuk menjaga lingkungan tetap sehat dan solusi menang-menang bagi perokok serta orang yang tidak menyukainya: pemegang kebijakan menyediakan lebih banyak ruang merokok. Mengisolasi mereka agar tidak sembarangan menyulut dan mengepulkan cincin asapnya di samping warga lain yang ingin menghirup udara tanpa cemar.

Begitu pun seharusnya dengan penebar _spoiler_. Mereka sebetulnya perlu disadarkan bahwa mereka mempengaruhi hajat banyak orang terhadap tontonan mengejutkan, sehingga mereka harus meredam keinginannya berbicara terlalu keras di ruang yang berpotensi didengar banyak orang.

Saat nongkrong bersama pemuda lain, dua orang berkerisik. Orang pertama menanyai bagaimana lanjutan film favoritnya (yang juga favorit saya), orang kedua menjawabnya secara bergairah. Saya yang sudah menjarak sekitar 1-2 meter, akhirnya mengalah untuk bergeser lebih jauh lagi.

_Spoiler_ mungkin tidak menjadikanmu sesak nafas atau kanker, tapi ia tetap menyakitkan.

Rokok dan _spoiler_ tidak mungkin dihentikan, ia selalu menyebar. Mereka hanya perlu diatur distribusinya dan diatur agar dapat dikonsumsi tanpa mengganggu pihak yang tidak menyukainya.

Bedanya. Keberadaan rokok adalah berkah bagi petani tembakau, penjual eceran, dan anggaran kesehatan. Sebaliknya, anggap saja _spoiler_ dan penebarnya adalah salah satu keragaman hayati yang tidak boleh dibebasliarkan.

\[Foto oleh **[Luis Quintero](https://www.pexels.com/id-id/@jibarofoto?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/foto-faucet-abu-abu-2339722/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
