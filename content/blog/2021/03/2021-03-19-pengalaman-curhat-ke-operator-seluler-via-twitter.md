---
title: "Pengalaman \"Curhat\" ke 6 Operator Seluler Via Twitter"
date: "2021-03-19"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-tyler-lastovich-699122-ponsel-smartphone-rmdzn.jpg"
aliases:
  - /post/2021-03-19-pengalaman-curhat-ke-operator-seluler-via-twitter/
---

Menengahi bulan Maret 2021 (tepatnya Sabtu, tanggal 13 lalu), saya berhasil mencapai pencapaian baru! Tidak besar-besar. Yakni, cukup pernah menghubungi semua layanan konsumen operator seluler secara daring. Lewat media sosial, terutama Twitter.

Pengalaman tak unik dan cukup umum. Tidak begitu spesial. Tetapi, begitulah hidup. Kadang spesial. Kadang biasa. Kadang saya-andaan. Kadang aku-kamuan. _Biyasaalaah_.

https://twitter.com/lilithkis/status/1356563571740561408

Saya mempunyai konter pulsa kecil-kecilan. Tidak semua pelanggan adalah pembeli yang transaksinya lancar jaya dan nomornya selalu tanpa kendala. Kadang, ada yang mengeluhkan satu dua masalah; pulsa yang tersedot oleh entah layanan apa, kode voucer yang tidak dapat dipakai, begitu bisa dipakai … _eh_ … tidak bisa untuk _internetan_.

Jika masalah yang mereka alami bisa langsung saya atasi, baik dengan intuisi atau mencari referensi di [mesin pencari](https://startpage.com), tentu akan saya lakukan sendiri. Kalau bisa _sat set bat bet_. Namun, kalau pikiran mentok, mau tidak mau saya laporkan ke kontak layanan konsumen secara langsung lewat Twitter.

Sebagian besar yang saya laporkan adalah masalah orang lain. Hanya segelintir kasus yang hanya berhubungan dengan nomor saya sendiri.

## Tri

Tri menjadi nomor kedua yang pernah saya pakai untuk kebutuhan _internetan_ tetapi menjadi yang pertama saya hubungi lewat Twitter. Di daerah saya tinggal, jaringan Tri sangat bagus. Bilah sinyal sering penuh. Sinyal data juga sering HSDPA dan 4G.

Pada Juli 2019, jaringan turun. Sinyal remuk. Sinyal biasa hanya 1-2 bilah, sinyal data mentok di E (Edge/2G).

Tri memiliki dua akun Twitter resmi: [@triindonesia](https://twitter.com/triindonesia) dan [@3CareIndonesia](https://twitter.com/3careindonesia). Akun pertama untuk mempromosikan produk-produk mereka, akun kedua sebagai akun layanan konsumen. Karena takut komplain saya tidak terkirim dengan baik. Saya putuskan untuk mengirimkan ke keduanya dengan selisih tidak lebih dari 10 menit (@triindonesia adalah yang pertama).

Anehnya, pesan komplain saya justru dibalas lewat akun @triindonesia, bukan @3CareIndonesia.

![DM ke akun@triindonesia](/blog/2021/03/images/photo_2021-03-18_11-38-30-edited.jpg)

Admin akun Tri menanyakan detail kartu Tri saya, serta merek ponsel, indikator sinyal, dan alamat saya tinggal. Setelah proses pengecekan, akhirnya pihak Tri menyimpulkan jaringan mereka tidak ada yang salah, kesalahan berada di perangkat saya sendiri. Padahal pengguna Tri di kampung saya mengeluhkan hal yang sama.

![DM ke akun @triindonesia](/blog/2021/03/images/photo_2021-03-17_22-47-54-edited.jpg)

Pahitnya, kiat yang mereka sarankan kepada saya untuk memulai ulang ponsel sudah saya coba berulang kali, bahkan ketika saya belum menghubungi mereka. Komplain lanjutan saya pun berakhir dengan status terkirim tanpa indikator telah dibaca.

Entah apa yang terjadi, beberapa hari kemudian, sinyal kembali normal. Tak bermasalah seperti sebelumnya. Tentu, perangkat saya masih ponsel yang sama, tanpa saya atur-atur koneksinya.

Saya justru cukup puas dengan layanan konsumen mereka pada tahun 2017 lalu, mereka jujur bahwa memang ada masalah pada jaringan mereka. Yang disayangkan, saya perlu menunggu sekitar 25 jam hanya untuk mendapatkan jawaban seperti itu.

![](/blog/2021/03/images/2021-03-16-12.23.17-twitter.com-960b7bc1befa.jpg)

## Indosat

Seumur-umur, saya hanya sekali menghubungi layanan konsumen Indosat lewat Twitter. Itu pun melalui _mention_ akun, bukan DM, dan berhubungan dengan masalah yang dialami orang lain.

Awal mulanya. Seorang pelanggan ingin memasukkan paket data ke nomornya, tapi selalu gagal. Usut punya usut, nomor Indosat yang dipakai masih terdaftar dengan paket BUNDLING--paket yang terdaftar secara otomatis saat pengguna membeli ponsel baru yang sudah dibundel dengan kartu Indosat.

Masalah saya justru selesai setelah utak-atik sendiri, bukan karena diarahkan oleh layanan konsumen Indosat.

## Smartfren

Smartfren menjadi layanan konsumen operator seluler pertama yang saya hubungi berulang-ulang dan semua masalah yang saya utarakan terpecahkan dengan baik atas panduan mereka.

![](/blog/2021/03/images/smartfren-1-rmdzn.jpg)

Bahkan, sekadar pertanyaan sepintas yang berakhir urung disampaikan, dijawab dengan apik dan menyenangkan.

![DM ke akun @smartfrencare](/blog/2021/03/images/photo_2021-03-18_11-51-58-edited.jpg)

## Telkomsel

Telkomsel adalah kartu pribadi utama yang saya pakai sejak tahun 2008. Dan akun Twitter mereka menjadi layanan konsumen operator seluler kedua (setelah Smartfren) yang saya hubungi banyak-banyak sejak saya memiliki akun media sosial berlogo burung terbang.

Uniknya, sejak belasan tahun lalu itu, saya tidak pernah sekali pun mengonsultasikan masalah nomor saya sendiri kepada mereka--kecuali saat saya datang langsung ke GraPARI untuk mengurus kartu SIM rusak--semua laporan masalah justru datang dari nomor orang lain.

Beda dengan layanan konsumen yang akan saya tulis setelah ini, Telkomsel menerapkan verifikasi terlalu ketat. Oke, verifikasi itu bagus. Untuk mencegah penyalahgunaan atau penipuan. Tapi untuk konteks masalah tertentu, verifikasi justru memberatkan, bahkan serasa tidak perlu.

"Kok, harus begini?" Batin saya berulang-ulang.

Seorang tetangga yang hanya memiliki _featured phone_ mengeluhkan pulsanya selalu tersedot habis meski baru diisi. Saya melaporkan kepada layanan konsumen Telkomsel tentang masalah tersebut, dengan informasi ponsel pengguna bukan jenis ponsel pintar sehingga tidak mungkin pulsa terpakai data secara otomatis.

![DM ke akun @telkomsel](/blog/2021/03/images/telkomsel-1-rmdzn.jpg)

![DM ke akun @telkomsel](/blog/2021/03/images/telkomsel-2-rmdzn.jpg)

Saya kemudian diminta untuk menyebutkan identitas pemilik nomor dan informasi lain yang berhubungan dengan pulsa. Karena verifikasi gagal, nama pemilik nomor tidak sesuai pangkalan data mereka, alhasil mereka meminta banyak informasi lain demi verifikasi mendetail.

Saya menyerah.

"Pak, saya sarankan _jenengan_ untuk pergi ke GraPARI," kata saya kepada pemilik nomor hari berikutnya.

Berkontak dengan layanan konsumen Telkomsel di Twitter membuat perasaan saya campur aduk. Senang dan sebal berbarengan. Saya senang ketika mereka melakukan verifikasi data, tapi sungguh berlebihan sampai saya memilih melepas masalah saya (baca: masalah orang lain) ke antah berantah.

Pun verifikasi data juga untuk apa ketika masalah yang dialami hanya tersedotnya pulsa. Bisa, dong, cukup verifikasi apakah nomor bersangkutan memang pulsanya hilang tiba-tiba lalu tentukan solusi berikutnya.

Saya juga memperhatikan, ketika ada pengguna Twitter yang mengeluhkan nomornya sering dikirimi spam, pihak Telkomsel selalu meminta mereka untuk mengirim nomor yang dipakai, nama lengkap pemilik, dan lokasi. Buat apa coba? Apa hubungan verifikasi data dengan pencegahan spam yang masuk ke nomor kita? Apakah mereka tidak memiliki mesin cerdas untuk mendeteksi lalu lintas data mencurigakan yang terkirim dari nomor-nomor tertentu?

Klik saja jawaban templat berikut ini. Dan silakan nikmati rasa bingung yang mencuat dari kepala.

https://twitter.com/Telkomsel/status/1369934656670375940

https://twitter.com/Telkomsel/status/1370265606843367426

Sampai sekarang, masalah yang saya konsultasikan ke akun Twitter Telkomsel tidak pernah selesai. Saya mungkin lebih memilih datang langsung ke GraPARI jika menghadapi masalah dengan kartu Telkomsel.

## Axis/XL

Saya akan menggabungkan mereka ke dalam satu daftar, karena memang berada di bawah naungan perusahaan yang sama.

Berbeda 180 derajat dibanding dengan layanan konsumen sebelumnya, layanan konsumen Twitter Axis ([@ask\_AXIS](https://twitter.com/ask_AXIS)) justru sangat membantu saya. Pertanyaan dan masalah seputar Axis yang saya cuitkan lewat DM, 99% terjawab dan terselesaikan.

Saya pernah membantu pelanggan saat voucer AIGO yang baru saja dibelinya tidak dapat digunakan, dengan keterangan "Nomor AXIS kamu tidak bisa untuk transaksi AIGO". Hanya berselang sebentar setelah saya tanyakan, voucer tersebut bisa digunakan dengan lancar.

![](images/axis-2-rmdzn.jpg)

Pernah juga ketika daerah saya yang selalu dilewati jaringan Axis bersinyal penuh, tapi selalu turun ketika mati listrik, mereka meminta saya untuk menyebutkan merek ponsel, lokasi, waktu sinyal turun, dsb.

Beberapa saat kemudian mereka membuatkan nomor tiket untuk diselidiki kemudian. Masalah pun selesai, meskipun beberapa waktu terakhir sering kumat.

![DM ke akun @ask_AXIS](/blog/2021/03/images/axis-1-rmdzn.jpg)

Sekitar satu minggu lalu, saya membantu tetangga yang pulsa XL-nya selalu tersedot setelah diisi. Dia mengaku belum pernah mendaftarkan ke layanan apa pun, termasuk RBT. Dan juga, ponselnya adalah _featured phone_ sehingga tidak mungkin tersedot sebagai data secara otomatis.

Setelah saya tanyakan kepada akun [@myXLCare](https://twitter.com/myXLCare), saya cukup diminta untuk menyebutkan nomor dan memastikan apakah saya pernah berlangganan konten atau mengetuk spanduk (_banner_) apapun yang muncul. Saya katakan "tidak".

Pihak XL langsung menemukan nomor yang bersangkutan memang pernah berlangganan layanan konten. Mereka menawarkan saya untuk memblokir layanan konten secara global yang berimbas nomor tidak bisa lagi dipakai untuk membeli konten lewat Google Play.

"Tidak apa-apa," ucap saya berikutnya. Toh, nomor pemilik tidak dipasang di ponsel pintar sehingga tidak mungkin dipakai untuk membeli konten di Google Play.

Hanya selisih sekitar 20 menit, pihak XL memblokir layanan konten nomor bersangkutan. Masalah pulsa yang tersedot secara tiba-tiba pun selesai.

![](/blog/2021/03/images/xl-1-edited-rmdzn.jpg)

![DM ke akun @myXLCare](/blog/2021/03/images/xl-2-edited-rmdzn.jpg?w=511)

## Kesimpulan

Bagi saya, berdasarkan pengalaman berinteraksi dengan layanan konsumen operator seluler di Twitter dalam hal penyelesaian masalah, saya menentukan urutan teratas (1) dan terbawah (4) seperti di bawah ini.

1. Axis, XL, Smartfren
2. Indosat
3. Telkomsel
4. Tri

Nomor 2 dan 3 bisa saja saling bertukar, tapi karena saya tidak pernah berkonsultasi secara detail dengan Indosat, saya putuskan untuk meletakannya di peringkat 2.

Bagaimana pengalamanmu?

\[Foto oleh **[Tyler Lastovich](https://www.pexels.com/id-id/@lastly?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/iphone-7-hitam-di-meja-coklat-699122/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
