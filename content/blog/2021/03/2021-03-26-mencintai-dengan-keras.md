---
title: "Mencintai dengan Keras"
date: "2021-03-26"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "sparkler-677774_1280-kembang-api-rmdzn.jpg"
aliases:
  - /post/2021-03-26-mencintai-dengan-keras/
---

GOL … GOL … GOL!!

Komentator berteriak kencang. Penonton di stadion meneriakkan _ola ole_ membahana, penonton di rumah mengikutinya dengan hal serupa sambil melolong dan memukul soblok berpantat gosong.

Piala dunia bertahun-tahun lalu membuktikan kepada saya bahwa orang tua saya (terutama ibu) memang penggila sepak bola. Walaupun bukti yang saya lihat sendiri tak seekstrem pengakuan mereka.

Saya melihat ibu menggunting satu lembar penuh jadwal piala dunia dari koran Kedaulatan Rakyat lalu menempelkannya di dinding samping televisi. Itu sekitar era Piala Dunia 2002 di Korsel dan Jepang, serta 2006 di Jerman. Sepertinya juga saat Piala Dunia 2010 di Afrika Selatan. Mungkin juga jauh sebelum itu, ketika saya belum tahu apa-apa.

Setiap pertandingan usai, ibu menuliskan skor di kolom yang disediakan dan memberi tanda pemenang untuk diturut ke dalam bagan turnamen demi melihat tim mana yang akan bertanding selanjutnya.

"Dahulu, saya _nggathok_ banget dengan sepak bola. Bahkan niat melek sampai malam demi menunggu pertandingan dimulai," tutur ibu saya ketika berbincang seputar hal-hal yang disukai dengan kerabat jauh atau tetangga.

Soal itu, saya tidak begitu paham. Karena batas yang saya tahu saat itu antara melek demi menonton sinetron dan sepak bola sangat tipis. Cerita pendek pada awal artikel ini pun hanya bayangan umum, orang tua saya tidak pernah melakukan hal itu. Hanya sedikit teriakan pendek.

Saya tidak tahu negara mana yang bapak dan ibu jagokan, yang saya lihat, mereka memang penggemar berat sepak bola. Tapi itu dahulu. Pada Piala Dunia yang belum lama digelar (2018 di Rusia), euforia tidak seramai sebelumnya. Ibu saya tak lagi menempelkan poster bagan turnamen dan tidak begitu menunggu jadwal pertandingan di televisi.

Menyukai sesuatu dengan keras dan detail adalah pengalaman menarik dalam kehidupan. Saya pernah mengikuti balap MotoGP. Getol sekali menonton mereka kebut-kebutan sambil memiringkan motor hingga lutut _nggeset-nggeset_ aspal. MotoGP biasa tayang selepas waktu Isya dan saya berusaha meluangkan waktu menonton Valentino Rossi dan kawan-kawan.

Karena kebiasaan itu, saya jadi cukup hapal beberapa nama pembalap dan timnya. Nama pembalap dan warna motornya. Bahkan juga beberapa pembalap dari kelas Moto2.

Ketika saya bergabung di forum daring MotoGP di Reddit, saya begitu kagum dengan anggota-anggota forum yang mampu berdiskusi terus menerus. Rinci pula. Membahas teknis; bagaimana komponen sayap (_winglet_) dipasang di Ducati dan efek yang dihasilkan. Serta analisis dan prediksi di arena tentang apa yang terjadi saat Marc Marquez rehat untuk memulihkan cederanya.

Saya juga menikmati olahraga badminton. Selain sebagai permainan, juga menikmati sebagai tontonan.

Saya ingin mencoba mengikuti perkembangan dunia badminton; lokal maupun internasional, di samping dengan cara menonton pertandingan secara langsung lewat televisi, saya juga mengikuti akun [@BadmintonTalk](https://twitter.com/badmintontalk) (satu-satunya media sosial badminton yang saya ikuti).

Saya ingin membuktikan diri, kira-kira bisakah saya menjadi penggemar berat olahraga pukul-pukul kok ini.

Ternyata tidak juga.

Mirip saat saya bergabung di forum MotoGP Reddit, saya menjadi paham begitu banyak hal-hal teknis maupun non-teknis yang belum saya mengerti di dunia perbadmintonan, terutama tentang hitung poin peringkat pemain dan seri turnamen (termasuk detail _super series_).

Rahang saya menganga ketika @BadmintonTalk mengadakan kuis prediksi pemenang pada setiap turnamen yang diselenggarakan. Pengikutnya pun ikut nimbrung dengan prediksi yang sedikit melenceng, agak mendekati, bahkan tepat. Poin demi poin yang diperoleh pada setiap tebakan dikumpulkan dan dirilis, memperlihatkan begitu banyak pecinta badminton yang ikut berpartisipasi.

Ternyata saya sadar, saya tak pernah dengan keras dan detail ketika mencintai sesuatu. Atau mungkin, saya tidak sadar bahwa saya sudah mencintai dengan keras kepala terhadap suatu hal, tapi terburu-buru meninggalkannya.

Situasi terakhir. Saya masih mengikuti perkembangan badminton, tapi tentu dengan detail-detail yang selalu terlewat. Tidak dengan MotoGP yang kini kurang saya nikmati dan tidak lagi saya tonton secara rutin.

Masih ada banyak objek (dan subjek) yang dapat saya cintai dan sukai. Yang membuktikan keragaman respon saya terhadap satu dan hal lainnya.

Yang penting, tidak perlu memaksakan perasaan diri sendiri. Kalau hanya dapat menyukai sekenanya sebenarnya tidak masalah. Kalau bisa menyukai totalitas anggap saja jadi nilai tambah.

_Hai~~_

\[Gambar oleh [Free-Photos](https://pixabay.com/photos/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=677774) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=677774)\]
