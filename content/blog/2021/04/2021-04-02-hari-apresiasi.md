---
title: "Hari Apresiasi"
date: "2021-04-02"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-wewe-yang-4508643-apresiasi-rmdzn.jpg"
aliases:
  - /post/2021-04-02-hari-apresiasi/
---

Pada kisaran waktu yang sama dengan hari ini--sekitar kuartal ketiga dan keempat tahun 2020 lalu, saya mengungkapkan banyak protes dan kemarahan. Saya merasa ingin damai, tetapi situasi tak mendukung. Dengan masalah yang menyentuh lingkungan terdekat, protes saya langsung bergemuruh lepas.

Setahun berlalu, negeri ini tidak pernah berhasil [meratakan kurva](/blog/2020/03/2020-03-20-meratakan-kurva/). Segala [hajatan](/blog/2020/03/2020-03-22-tangkap-cintamu-dengan-akad-tapi/) masih diadakan di awal bencana, dan mulai ramai lagi akhir tahun lalu. Segala hal dituntut berubah tapi sayangnya ia selalu dianggap asing, padahal seharusnya sudah menjadi [kewajaran](https://ramdziana.wordpress.com/2020/04/03/korona-dan-kewajaran/) yang baru.

Perlu ada hentakan berbentuk surat untuk mendorong kampung waspada. Surat yang saya anggap sebagai _[magnum opus](/blog/2020/04/2020-04-12-magnum-opus-bacotan-kami-didengar/)_. Kala itu.

Namun, hanya perlu beberapa hari sebelum kemudian kami menyemprotkan protes lagi. Kali itu kepada pengelola (takmir) masjid. Protes secara langsung maupun tidak langsung. Via aplikasi obrolan WhatsApp. Intinya, takmir masjid terlalu sibuk membahas sisi ritual--yang tentu saja baik--tapi [melewatkan masalah nyata di kanan kirinya](/blog/2020/04/2020-04-18-setop-debat-masjid-harus-fokus-bantu-warga-di-sekitarnya/).

Ya. Meskipun protestornya selalu ditentang. Dan ditanggapi secara pasif agresif dengan pernyataan pembatasan ibadah adalah pelanggaran hak asasi manusia.

Dengan julidan ala warga biasa. Dengan syak wasangka yang secara manusiawi tercipta. Saya menduga, kisah terakhir di atas bisa saja kembali terjelma. Karena memang situasi belum baik-baik saja.

Kata Puthut EA, yang kemudian dikonfirmasi oleh banyak berita di media hari berikutnya, terdapat puluhan warga yang terpapar penyakit setelah menghadiri takziah. Yang ditakziahi tidak meninggal karena penyakit yang sedang mewabah saat ini, tapi para pentakziah tertular dari pentakziah lainnya. Sang kepala suku Mojok menambahkan protes kecil dalam cuitannya.

"_Dan kemarin ada pejabat yg menargetkan 1,8 jt wisatawan dtg ke yogya. Ente sehat?_"

Prasangka saya tuliskan sebagai respon twit tersebut.

"_Sementara itu, banyak takmir masjid yang mulai gatal ingin mengadakan perayaan Ramadan seperti hari normal. Yaa rapat lah, buka bersama lah, dsb._" Termasuk takmir masjid di kampung saya.

Seru ketika saya diberitahu dugaan bahwa pembukaan masjid kala Ramadan, setidaknya, karena infak yang menurun, zakat fitrah yang menurun, dan sebagainya.

Jika infak masjid selama ini memberikan manfaat kepada warga di sekelilingnya, kekhawatiran menurunnya infak karena tak ada acara saat Ramadan layak terjadi. Namun, faktanya banyak masjid yang infaknya tak bergerak aktif, ia hanya menjadi dana macet.

Sedangkan tentang zakat fitrah. Kepanitiaan di bidang ini sebetulnya masih bisa dibentuk tanpa perlu didahului dengan perayaan-perayaan seperti Ramadan tahun-tahun sebelumnya. Zakat fitrah cenderung menurun pun bagi saya terbagi dalam tiga skenario.

1. Muzakki (orang yang berzakat) memang menurun. Di daerah saya, rata-rata per tahun muzakki selalu turun, bahkan masa pra-pandemi.
2. Terjadi disrupsi oleh aplikasi zakat secara daring. Mulai tahun lalu, platform-platform zakat daring menggencarkan iklan untuk menggunakan layanan mereka. Dari yang berbasis organisasi dan komunitas, sampai _marketplace_ raksasa.
3. Makin banyak muzakki (orang yang berzakat) yang mengantarkan zakatnya kepada mustahik (orang yang diberi zakat) secara langsung, tanpa perantara.

Aaah, tapi gara-gara ini, saya dianggap oleh pengguna Twitter lain terlalu bersudut pandang perkotaan. Setiap opini memang ada biasnya, dan bias saya disebabkan oleh ruang tinggal yang lokasinya tak sepenuhnya desa, tak sepenuhnya pula kota, tetapi gaya hidup warganya hampir menyerupai kota.

"Saya juga begitu mas, lingkungan mirip, tapi sikap keagamaannya cenderung pedesaan," tuturnya.

Saya pikir "cenderung pedesaan" serta "infak dan zakat fitrah" tidak ada hubungannya jika dikaitkan dengan tetap mengadakan ritual Ramadan seperti tahun-tahun sebelumnya atau tidak. Saya dapat lebih mengakui logika bahwa tradisi dan cara bermasyarakat memang sulit diubah, yang menjadi alasan kuat mengapa banyak takmir masjid kekeh untuk mengadakan ritual Ramadan seperti biasa.

Dengan senang hati saya menunggu sudut pandang yang sedikit kompleks. Tapi. Tak terjadi. Sepi. Saya benar-benar ingin mengerti lewat diskusi ini, tapi kebanyakan pengguna medsos begini. Kirim dan pergi. _Hais_.

…

Kabar terbaru, takmir masjid di kampung saya yang semula ingin mengadakan perayaan Ramadan seperti hari biasa, batal. Saya cukup mendengar sekali tentang ini untuk bersyukur berulang kali.

Saya tidak peduli apakah pembatalan memiliki motif berhati-hati demi menjaga kesehatan warga sekitar atau sekadar takut tak ada massa di masjid sehingga pekerjaan terasa percuma.

Saya tetap berterima kasih untuk hal ini.

Biarkan hari ini menjadi hari apresiasi positif setelah pada waktu yang sama setahun lalu saya tak dapat melakukannya. Terima kasih untuk semua yang mau berhati-hati dan terus menjaga keamanan diri dan sekitarnya.

_Clap clap_.

Menjura.

\[Foto oleh **[wewe yang](https://www.pexels.com/id-id/@wewe-yang-2383099?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/kafein-kopi-cangkir-gelap-4508643/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
