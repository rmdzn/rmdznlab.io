---
title: "Tersedot 30 Kali (Bag. 1)"
date: "2021-04-16"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "heart-2421218_1280-hati-jantung-rmdzn.jpg"
aliases:
  - /post/2021-04-16-tersedot-30-kali-1/
---

Dua bulan kemarin. Catatan donor darah saya sudah resmi mencapai 30 kali! Jumlah yang sudah melebihi usia sendiri. Jumlah yang bikin saya tidak menyangka, ternyata sudah sebanyak itu darah saya dikeluarkan secara sukarela.

Rutinitas yang akhirnya membawa banyak pengalaman tak terduga, baik pengalaman sendiri atau orang lain. Secara tak langsung, kisah ini juga mengubah cara pandang saya terhadap kontrol kesehatan diri sendiri.

## Awal mula

Saya masih ingat. Di masjid di jalan Waringin, barat kampus UPN, siang hari. Tempat pertama saya memutuskan untuk ikut mendonorkan darah, kisaran tahun 2013.

Keinginan yang selalu terbersit di kepala. Saya selalu ingin ikut donor darah, bahkan sejak masih SMA. Namun, ketika saya melihat peta lokasi PMI terdekat, ternyata jauh juga. Saya yang anak rumahan, cukup kuper, dan pernah ditabrak motor lain saat nekat ke Kota Yogya sendirian karena kebingungan jadi mengurungkan niat. _Ah besok aja, lah. Takut_.

Beberapa tahun kemudian. Akhirnya kesampaianlah keinginan saya. Teman saya yang adalah anggota marbot menawari saya dan teman lainnya untuk ikut donor darah di masjid yang ia urus.

"Datang pagi aja", ujarnya.

Saya datang bersama dua teman sekelas, tapi hanya dua dari kami yang ikut menderma darah--termasuk saya sendiri. Saat itu juga sepi, jadi kami tak perlu repot-repot untuk antre mengular.

Setelah mengisi satu lembar penuh pertanyaan, saya duduk di depan Bu Dokter. Diperiksalah tensi dan HB saya untuk memutuskan ideal tidaknya saya untuk disedot darahnya.

Sehat!

Cuss. Jarum disuntikkan.

Saya cukup biasa menghadapi jarum suntik. Jadi tak ada hal yang perlu saya khawatirkan. Kecuali cemas jika badan saya lemas setelahnya.

Selang diurut. Donor berhasil.

Lancar.

Bingkisan warna biru ditambah nasi Olive Chicken kami tenteng pulang bersama-sama dan kami lahap di kamar indekos teman sambil mengerjakan tugas harian.

Sedikit pegal, sih. Tapi, secara keseluruhan, saya sehat-sehat saja sepanjang hari. Beda dengan hari berikutnya. Badan saya demam. Flu menyerang.

Sial, batin saya. Satu-satunya doa saat itu: sakit saya memang berasal dari lemahnya badan pasca donor darah perdana, bukan karena virus yang keburu masuk sebelumnya dan telanjur terbawa oleh jarum-selang dan berakhir di incinerator.

Donor darah kala itu membuka niat saya untuk menjadikannya kebiasaan. Tiga bulan sekali.

## Rutinitas itu dimulai

Karena saya masih merasa tidak nyaman untuk pergi ke kantor PMI terdekat, saya putuskan untuk melakukan donor darah secara rutin melalui acara-acara khusus. Biasa diagendakan oleh _tenant_ di mal, warung makan, rumah sakit yang merayakan ulang tahun, dsb.

Dua kali--donor kedua dan ketiga--saya mendonor di acara khusus. Sayangnya saya sama sekali tidak mendokumentasikan kapan saya melakukan donor darah pertama, kedua, dan ketiga. Ya, sejak donor keempat, saya dengan rapi mendokumentasikannya, tapi penjelasannya nanti dulu.

Antara donor kedua dan ketiga, yang saya ingat hanya satu. Di Waroeng Steak & Shake Gejayan (Jl. Affandi). Saya datang hanya selisih sekitar satu jam dari acara dimulai, tapi saya sudah mendapatkan antrean ke-100 lebih.

Edan.

Datang dan daftar sekitar pukul 09.00. Saya baru dapat giliran di atas jam 12.00. Sungguh, berfaedah sekali saya tidak melakukan apa-apa selama tiga jam di sana. Di atas meja kursi makan yang disulap jadi tempat tunggu.

Tapi kebetulan juga saya bertemu kawan SMA yang tak begitu akrab. Duduk di ruang yang sama, berbincang sebentar, sambil saya melirik-lirik kartu bertabel yang ia genggam. Oh, ternyata itu adalah kartu dari PMI. Dengan tabel yang berisi waktu donor, untuk mengontrol kapan si pemilik kartu dapat mendonorkan darahnya lagi.

"Rutin donor, ya?"

"Gak, sih. Pas sempet aja. Haha."

Beda dari donor pertama. Donor darah saat itu benar-benar lancar, . Sedikit trauma karena sakit pasca donor perdana, tapi tak masalah, donor kedua dan ketiga sama sekali tidak mempengaruhi badan saya pada hari berikutnya.

Kepuasan saya terhadap respon tubuh sendiri tidak sejalan dengan kepuasan terhadap pengalaman saya ikut donor di acara-acara khusus. Antrenya itu, loh, bukan candaan. Keburu kisut di kursi tunggu. Kecuali kalau mau berpagi-pagi ria untuk mendaftarkan diri di urutan pertama.

Karena alasan itu, saya putuskan untuk menjadikan kantor PMI sebagai rujukan donor rutin berikutnya.

Berdasarkan survei lewat peta daring dan beberapa kali menelusuri mesin pencari, hati saya berlabuh di PMI Kota Yogyakarta.

Ada beberapa pertimbangan mengapa saya memilih PMI yang berada di Kotagede itu. Padahal dia di luar daerah administrasi tempat saya tinggal. Cukup jauh pula.

_Saya masih ingin cerita banyak, tapi biar tak kepanjangan, tulisan lanjutan akan terbit minggu depan. Sampai jumpa di bagian dua!_

\[Gambar oleh [Saveliy Morozov](https://pixabay.com/users/prographer_-217382/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2421218) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2421218)\]
