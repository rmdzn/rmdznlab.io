---
title: "Datang dan Memulai (Bag. 2)"
date: "2021-04-23"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "photo_2021-04-22_21-24-44-e1619103574708.jpg"
aliases:
  - /post/2021-04-23-datang-dan-memulai-2/
---

Saya keliru menyebut ini "ode". Menulis sajak saja tak _jedak_, apalagi menulis dendang dengan iramanya. Ini hanya ungkapan syukur berikut pengalaman saat saya memilih untuk memulai suatu aktivitas dan mampu konsisten sampai sekarang.

## Memilih PMI

Mengikuti donor darah di acara-acara khusus sungguh melelahkan. Ramai. Situasi yang membuat saya terlalu gugup untuk duduk _ndlombong_ begitu lama dan terlalu melelahkan untuk menyapa peserta lainnya.

Pengalaman merepotkan ini membuat saya berpikir ulang untuk mengikuti acara serupa. "Bagaimana kalau langsung ke PMI saja?"

Jauh setelah pikiran itu terbayang, saya mencari opsi lokasi PMI perwakilan kabupaten tempat saya tinggal. Kenapa kabupaten? Bukan kecamatan atau kelurahan? Berdasar hasil telusuran, kebanyakan Unit Donor Darah (UDD)/Unit Transfusi Darah (UTD) ada di PMI tingkat kabupaten atau kota madya.

_Kutak katik_ … akhirnya ketemu.

PMI Kabupaten Sleman.

Alamatnya di Triharjo. Kalau dicek lewat peta, lokasi PMI jauh di sana, masih ratusan bahkan ribuan meter utara Taman Denggung (atau lebih enaknya, utara Sleman City Hall). Itu pun masih perlu _bludas-bludus_ lewat jalan non-raya.

Bagi saya yang hidup di daerah Sleman Timur, pergi ke sana bagaikan mencari kitab suci. Perlu jalan ke barat cukup jauh, baru belok ke utara. Kalau beruntung, bertemu Siluman Kerbau lalu diangkat sebagai adik seperguruan, kalau tak beruntung, akan bertemu Siluman Tengkorak Putih lalu melawannya.

Ke sana? Sepertinya tidak mungkin.

Penelusuran pun dilanjutkan. Tapi dua pilihan pelik menunggu di depan. Ada PMI Kota Yogyakarta dan PMI DIY. PMI Kota Yogyakarta berada di Kotagede, barat Pasar Legi Kotagede. Sedangkan PMI DIY berada di perempatan Yogya-Wates (Ring Road Barat).

Pilihan yang sulit. Saya tak begitu suka Kota Yogyakarta, bukan karena romantisasinya yang berlebihan, tapi karena saya tak hafal jalanannya. Bahkan sampai sekarang. Sedangkan PMI DIY memang masih ada di Sleman, tapi ternyata juga cukup jauh--lebih jauh dari PMI Kota, meski cenderung lebih dekat daripada PMI Kabupaten Sleman.

Beruntung sekali ketika saya mendapati petunjuk lain. PMI DIY ternyata tidak memiliki UTD/UDD. Mantaplah saya untuk pergi ke PMI Kota Yogyakarta.

## Survei dulu, datang kemudian

PMI Kota Yogyakarta (selanjutnya saya hanya menyebutnya "PMI Kota") lumayan jauh sih, tapi setidaknya lebih mudah dijangkau dan jalan menuju ke sana juga tidak begitu ribet.

Saya tipikal orang yang survei jalan terlebih dahulu sebelum berangkat ke tempat asing. Dan memilih lewat jalan utama, yang mudah diingat dan dicapai, dibanding lewat jalan tikus yang … yaa lebih dekat sih, tapi rumit untuk ditelusuri.

Saya survei melalui Ring Road Selatan. Perempatan Ngipik belok ke barat. Jalan Karanglo lurus terus, mentok sedikit _mleyot_ melewati Pasar Legi Kotagede. Kata kunci yang saya ingat adalah "PMI ada di utara jalan setelah jembatan/kali". Ketemu!! Karena ini hanya survei, setelah letak tujuan terkonfirmasi, saya balik lagi. Pulang.

Tak jauh dari hari itu. Saya memberanikan diri untuk donor darah di PMI pertama kali. Berangkat sebelum waktu Zuhur (sepertinya sekitar satu sampai dua jam sebelumnya).

Sedikit kikuk, saya memarkirkan motor di tempat yang melajur ke arah barat itu.

"Tempat donor darah di mana ya, Pak?" Tanya saya kepada petugas jaga di pos samping.

"Di situ, Mas. Masuk aja ke sana lalu belok kanan," jawabnya sambil menunjuk ke arah pintu kaca hitam.

Di pintu itu memang terpasang tulisan "donor darah" dengan arah panah ke kanan. Ruang yang cukup besar dengan kursi dempet berjajar. Meja panjang berdiri teguh di depannya. Meja meliuk ala lobi dengan seseorang di baliknya.

Saya sedikit lupa susunan ruang tunggu donor darah PMI Kota saat pertama kali ke sana. Lebih dari tiga kali ruang tunggu berubah sejak tahun 2013 sampai masa pandemi ini, jadi harap maklum.

"Permisi, saya mau donor darah," ujar saya pelan kepada orang di balik meja lobi.

"Sudah punya kartu donor?"

"Belum, Pak."

Sambil menyodorkan lembar kertas, "kalau begitu, silakan mas, ini diisi dulu lalu tanda tangan di sini. Nanti saya buatkan kartu donornya."

Lembar kertas formulir penuh pertanyaan. Berat badan, riwayat kesehatan, dan segala pertanyaan bersifat personal. Lembar yang sama seperti saat saya donor darah di acara-acara sebelumnya. Oiya, ada timbangan badan panjang yang disediakan di sudut ruang tunggu, pakai saja kalau ingin cek berat badan sebelum dituliskan ke lembar itu.

Ruang periksa kesehatan ada di sebelahnya, ruang kecil dengan kursi deret tunggu yang juga kecil. Meja dengan alat tensi, pemeriksa kadar HB, serta selongsong dusgrip berdiri sebagai wadah pena--untuk dipakai pendonor yang ingin mengisi formulir.

Segalanya sehat, seperti yang sudah-sudah. Ruang donor di sebelahnya. Semirip bangsal di rumah sakit. Lengkap dengan lima _bed_ dan petugas donor dengan seragam PMI Kota yang sedang menangani pendonor lain.

"Kanan atau kiri, Mas?" Tanya seorang petugas kepada saya untuk menentukan _bed_ yang akan saya pakai.

"Bisa kanan atau kiri." Pada bulan-bulan donor darah awal, tangan kanan dan tangan kiri saya memang dicoblos bergantian. Jadi saya pun tak masalah. _Bed_ manapun yang kosong, siap saya rebahi.

"Silakan ambil bingkisan dan minum teh di sana," tutur petugas dengan gestur tangan menghadap ke atas setelah ia memotong dan mengerut selang, lalu menempelkan plester luka di lengan saya.

Ruang kecil berikutnya dipenuhi kursi dan meja bundar berwarna merah. Tatakan gelas tengkurap di sisi meja lain dengan jumbo berisi minuman. Pendonor diperkenankan untuk minum teh hangat, untuk menambah pasokan energi pasca disedot darahnya.

Saya siap pulang. Melewati lobi. Petugas yang sedari tadi berjaga di sana menyerahkan kartu donor baru saya.

"Terima kasih, Pak."

Saya selalu mengaggap hasil tes kesehatan saya wajar saja. Saya menerimanya sebagai hal yang memang harus begitu. Begitu juga proses donor yang selalu tanpa hambatan. Pola pikir yang sempat meremukkan saya beberapa tahun kemudian …

Tinggi.

Rendah.

Dan yang tak berhubungan langsung …

Lebam.
