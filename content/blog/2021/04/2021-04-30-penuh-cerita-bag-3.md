---
title: "Penuh Cerita (Bag. 3)"
date: "2021-04-30"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-puwadon-sangngern-5340267-donor-darah-hati-rmdzn.jpg"
aliases:
  - /post/2021-04-30-penuh-cerita-bag-3/
---

Donor darah ternyata kegiatan yang mengejutkan. Bukan mau berlebihan. Karena memang ada saja cerita baru pada setiap jengkalnya. Cerita yang saya alami sendiri maupun cerita yang datang dari orang lain.

## Kisah mengejutkan

Ruang donor darah di PMI Kota begitu medis sekali. Dominan warna putih dengan _bed_ khusus (bantalan mencuat ke samping pada setiap _bed_ untuk meletakkan tangan) berjumlah lima. Lengkap dengan meja di kanan/kirinya serta timbangan kantong darah di bawah sampingnya.

Perempuan berbaju khas ruang laboratorium terlihat wara-wiri di ruang samping yang dibatasi oleh kaca dengan lubang kecil seperti loket, untuk memasukkan kantong darah. Dan satu orang berada di depan komputer yang sepertinya sedang mendata sesuatu.

Begitulah ruang penuh cerita …

Perempuan muda, ibu-ibu, sampai bapak-bapak menusuk lengan saya selama delapan tahun terakhir. Tapi, dari sekian kali melakukannya, hanya seorang petugas yang dengan senang hati bercerita tentang apa saja. Kebetulan, beliau ibu-ibu. Sebagai orang yang irit bicara dan ogah menyapa orang asing hanya untuk berbincang terus-terusan, saya bahagia bertemu ibu itu. Sungguh klop dengan karakter saya, hampir selayak sobat lama.

"Donor darahnya AB, ya, Mas?," tanyanya.

"Iya, Bu."

"Wah, bagus, Mas."

"\[Golongan darah\] AB memang lagi sulit ya, Bu?" Tengok saya dengan selang menjulur ke bawah dari lengan kanan.

"Sulit banget."

"Apalagi kalau rhesus negatif ya?"

"Mirip. Tapi beda. Saking sedikitnya yang membutuhkan golongan darah rhesus negatif, pendonor dengan rhesus tersebut baru akan donor jika kita minta."

Saya mengangguk.

"Kemarin ada yang ingin donor, tapi rhesusnya ternyata negatif. Yaa, akhirnya kami tolak. Tapi kami menawarkan agar dapat menghubungi langsung, jika sewaktu-waktu ada yang membutuhkan golongan darah rhesus negatif," lanjut si ibu. "Kami bahkan siap mengunjungi rumahnya. Lengkap dengan ambulans dan _bed_ dari kami"

_Wow_.

Di pertengahan proses donor darah pada hari yang lain. Ibu yang sama mengisahkan seorang pendonor yang tak jujur kala mengisi formulir.

"Ada, lho, Mas, yang bohong."

"He? Gimana, Bu?"

"Setiap donor selesai, darah akan diperiksa lagi. Nah, ada satu kantong yang komponen darahnya menggumpal. Biasanya karena efek minum obat Bodrex atau Paramex. Kami langsung mengonfirmasi pemilik darah, dan ternyata benar. Dia sebelumnya minum obat dan tidak jujur saat mengisi formulir donor."

"Yang rugi sebetulnya juga calon penerima darah," lanjutnya. "Karena darah yang tidak layak dipakai akan dibuang \[dimusnahkan\]. Kasihan yang memang sedang membutuhkan."

Saat sejumlah darah yang dibutuhkan sudah masuk kantong, petugas donor akan menjapit selang kemudian memotongnya. Jarum yang menusuk lengan dicabut dan bekas tusukan akan ditempeli plester luka.

"Tangan tetap ditekuk ya, Mas. Sekitar 1 menit, lah," kata ibu itu. "Saran kami selalu begitu. Pernah ada pendonor syok lalu pingsan karena darah mengalir dari bekas tusukan. Ternyata, setelah donor tangannya tidak ditekuk, dan langsung dipakai aktivitas, digoyang-goyang."

_Yeah_, kisah yang hampir tak mungkin saya dengar jika saya tak bertemu ibu itu. Terima kasih atas ceritanya, lho, Bu.

## Lebam melebar

Beda lagi ketika petugas yang menusuk lengan adalah seorang perempuan muda. Lebih sering diam jika tak disapa duluan, sekali "menyapa" pengaruhnya luar biasa ke tubuh saya.

Sekitar donor kelima ke atas, lengan kanan saya menjadi lengan utama untuk donor darah. Petugas memang menyarankan pendonor untuk memakai lengan yang biasa dipakai donor. Sependek yang saya tahu, titik pembuluh yang biasa dipakai saat donor lebih mudah dicoblos, lebih lancar, dan cepat untuk mengalirkan darah.

Kala itu si perempuan muda berhati-hati menancapkan jarum ke lengan kanan saya. Entah karena masalah apa, jarum tak menancap tepat di pembuluh. Berulang kali jarum ditancap dan dicabut, sambil digeser sedikit ke kanan, kiri, atas, dan bawah. Saya menatap televisi, tapi fokus saya hanya pada rasa lengan yang dicoblos berulang kali.

"Pindah tangan kiri aja, ya, Mas?"

"Oke."

Saya bergeser ke _bed_ sebelah. Kini jarum mencari celah lain di lengan kiri. Berpindah sekitar dua kali, akhirnya ia tertusuk sempurna. Donor darah selesai seperti biasa, kecuali meninggalkan rasa pegal yang lumayan pada tangan.

Ternyata, area yang ditusuk-dicabut-ditusuk-dicabut itu sedikit lebam. Di hari-hari kemudian, lebam semakin melebar dan memanjang. Bahkan sampai kisaran 15cm. Karena saya tak sempat memotretnya, bayangkan saja pusat lengan dalammu menghitam, terus memanjang, hampir menyentuh pergelangan tangan. Menghitam sepanjang hari.

Namanya lebam, ia sakit jika dipencet. Bahkan saat dipencet pada bagian lebam paling bawah (hampir di pergelangan tangan). Perlu semingguan lebih sedikit, lebam itu baru hilang tak membekas.

Di luar masalah itu, saya suka ucapan mereka saat menusuk dan melepas jarum. Dulu mereka meminta kami untuk _nggeget_ (menggigit gigi dengan keras), sedangkan kali ini mereka meminta kami untuk menarik nafas. Supaya rasa _clekit_ pada kulit tersamar lewat gigitan keras atau tarikan nafas.

## Sempat lemah

Bukan berarti, donor darah yang saya lakukan selalu lancar. Pada [bagian pertama tulisan](https://ramdziana.wordpress.com/2021/04/16/tersedot-30-kali-1/), saya selalu menganggap hasil pengecekan kesehatan sebelum donor adalah hal wajar. _Take for granted_ alias berterima begitu saja.

Pikiran ini membuat saya "bunuh diri". Kala waktu, pikiran saya sedang kacau-kacaunya. Tekanan darah saya di atas batas normal. Itu tak hanya sekali, tapi berturut-turut pada beberapa hari berikutnya. Alhasil, berulang kali ke PMI, berulang kali pula saya gagal mendonorkan darah.

Tak hanya itu. Berbulan-bulan kemudian, saat saya sedang getol-getolnya [bersepeda](https://ramdziana.wordpress.com/2019/11/18/catatan-setahun-bersepeda/) ke mana saja (kecuali ke PMI), jumlah HB saya selalu rendah. Tak mencukupi. Jumlah HB minimal untuk donor darah adalah 12,5. Tapi, berkali-kali cek, HB saya berhenti di angka 11 koma. Saya pun mau tak mau berhenti donor darah sementara waktu untuk memulihkan badan yang mungkin keletihan.

Gara-gara itu, jika ditotal, saya tak melakukan donor darah selama setahun lebih.

Pengalaman ini yang membuat saya tak lagi santai saat proses pemeriksaan kesehatan sebelum donor. Selalu gugup dan khawatir.

"Habis lari, ya, Mas?" Tanya petugas yang memeriksa tensi saya suatu hari.

Degup jantung yang lumayan keras, rasa cemas yang sedikit menggila tak bisa saya atasi sepenuhnya. Bahkan untuk melontarkan candaan semacam, "iya, berlari-lari di ingatanmu", pun saya tak bisa.

Kali itu satu-satunya cara merespon adalah menarik nafas dan menghembuskan pelan-pelan.

## Sadar ...

Donor darah mengubah cara pandang saya terhadap kesehatan diri sendiri.

Sebelumnya, saya tak memiliki tujuan dan pandangan khusus kenapa saya harus minum air putih, menjeda minum teh, kopi, dan susu, serta tidak begadang, selain karena ancaman bahwa jika tidak melakukan itu saya akan menderita sakit blablabla. Dengan donor darah, saya jadi tahu alasan untuk melakukannya: setidaknya, agar saya dapat tetap rutin melakukan donor darah (2-3 bulan sekali) dan mengontrol berat badan.

Donor darah juga menjadi penghalang saya jika sewaktu-waktu saya ingin menindik kuping dan hidung atau mencoba menato tubuh dengan lambang bajak laut Whitebeard penuh di bagian punggung atau gambar tulisan ASE pada bagian lengan.

Omong-omong sudah 30 kali saya donor darah. Artinya, sekitar 9.000 cc (9 liter) darah saya disedot selama 8 tahun. Saya berharap dapat terus melakukannya di masa mendatang. Kalau bisa, sebanyak satu galon air penuh (19 liter), atau sebanyak tumpukan galon di atas truk besar.

\[Foto oleh **[Puwadon Sang-ngern](https://www.pexels.com/id-id/@puwadon-sang-ngern-2168173?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/sehat-cahaya-merah-wanita-5340267/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
