---
title: "Melihat Awal Pandemi dari Wuhan Diary"
date: "2021-05-07"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "photo_2021-05-06_21-04-33.jpg"
aliases:
  - /post/2021-05-07-melihat-awal-pandemi-dari-wuhan-diary/
---

Apa yang lebih horor dari melihat teke-teke\* menggelesot sepanjang gang setapak depan rumah? Atau kuchisake-onna yang menyapa tiba-tiba dari balik maskernya?

_Yak_, benar sekali. Membaca buku yang sangat berhubungan dengan situasi terkini. Bacaan yang membuat stresmu meningkat berkali-kali lipat dan degup sebal yang muncul secara tiba-tiba.

Perkenalkan: _Wuhan Diary_. Catatan harian sastrawan asal Tiongkok, Fang Fang. Sejak detik-detik menjelang kuncitara (_lockdown_) di kota Wuhan hingga dibukanya kembali pelan-pelan. Asumsi awal saya, buku ini menjadi buku kedua dengan genre memoar atau buku harian yang saya baca.

Omong-omong, sudahlah asumsinya, mari waktunya mengulas buku merah ini.

![Wuhan Diary](/blog/2021/05/images/wuhan-diary-rmdzn.jpg)

Pendahuluan yang dituliskan oleh Fang Fang terasa cukup merangkum semua. Sebentar … sebelum kamu merasa kena bocoran saat membaca Pendahuluan buku karena ulasan saya, tenang, "rangkuman" tersebut sangat ringkas. Terlalu ringkas untuk menampilkan rincian menarik dari kehidupan di Wuhan dan pengalaman pribadi Fang Fang yang hanya bisa diperoleh dengan membaca seluruh bukunya.

Mirip seperti di Indonesia, masker begitu penting--pada awalnya. Benar-benar penting. Sampai-sampai muncul selorohan komoditi masker lebih penting daripada daging babi yang merupakan makanan khas Imlek. Harga masker melonjak tajam. Berkali-kali lipat lebih mahal. Saking langka dan mahal, jika stok masker di rumah terbatas, penduduk Wuhan sebisa mungkin tidak keluar rumah. Berhemat.

Namun, situasi tersebut justru memunculkan solidaritas dengan gencar. Masker menjadi alat berbagi. Antar keluarga dan antar tetangga. Sebelum kemudian disusul oleh solidaritas lain. Berbagi bahan makanan, berbagi makanan jadi, sampai muncul jasa penitipan membeli bahan makanan pada setiap komplek hunian.

Salah satu solidaritas datang dari kaum muda. Fang Fang dengan menyejukkan berkata, orang lain tak perlu khawatir terhadap generasi muda. Meski mereka terkesan individual, kalau soal kemanusiaan mereka bisa gerak bersama-sama.

Itu menjadi penjelasan menenteramkan di tengah kisah buruk dari sikap pemerintah Tiongkok. Ketidaksensitifan mereka semakin memperparah endemi. Mirip seperti di sini. Pemerintah sana hobi dengan perayaan dan penghormatan. Menyambut dan menghormati pejabat lebih diutamakan, begitu juga ketika mereka mementingkan pencitraan dengan potret para relawan yang sedang mengibar-ngibarkan bendera, alih-alih fokus menangani endemi dengan tepat.

Begitu beraninya Fang Fang bercerita dan protes blak-blakan, membuatnya diserang bertubi-tubi. Siapa lagi kalau bukan diserang pendengung (_buzzer_). Akun Weibo dengan gambar profil seragam dan nama yang tidak jelas. Akun Weibo "profesional" dengan simbol centang di samping namanya.

Secara keseluruhan Wuhan Diary sungguh menyebalkan. Kisah kekecewaan yang tiada henti. Yang membuat pembacanya menyamakan dan membanding-bandingkan dengan negara yang sedang didiami.

Menariknya, Fang Fang menyertakan pengalaman sepositif mungkin (contohnya tentang solidaritas di atas), padahal saya dapat membayangkan situasi tanpa harap yang sedang menyelimuti Wuhan. Mirip kubah transparan yang terdiri dari benang-benang kuat yang melingkupi kota dan mengiris siapa saja yang melewatinya. Sementara itu Fang Fang sibuk menulis resep masakan sendiri, pengalaman tetangga yang mengirimkan bakpao di depan rumah untuk siap disantap, sambil memprotes kenapa harus ada kubah yang mengekang seperti itu dan seolah semua baik-baik saja, padahal di luar sana tak ada pejabat yang mau mengakui kesalahan atas dibangunnya kubah yang mematikan.

_Wuhan Diary_ secara tidak langsung juga memperkenalkan istilah, ritual, maupun budaya di Tiongkok. Baik lewat tulisan inti maupun lewat catatan kaki. Cocok menjadi pengetahuan tambahan. Termasuk _tou qi_ (Hari Ketujuh), sebuah ritus yang meyakini bahwa ruh orang meninggal akan kembali menyambangi rumahnya. Anggota keluarga mendiang diharapkan menyediakan sesaji dan berdiam di kamar masing-masing, agar ruh tidak merasa rindu sehingga menyulitkannya lahir kembali.

Pun ada istilah politik _zheng nengliang_ (energi positif) yang dicetuskan Xi Jinping kala masih menjabat sebagai Sekjen Partai Komunis Tiongkok tahun 2012. Sebaliknya, jika ada diskursus yang menentang partai akan dilabeli sebagai _fu nengliang_ (energi negatif).

Diksi yang menarik hati. Satu kata terjemahan mencolok yang menarik mata dan rasa saya, bahkan sebelum membuka halaman pertama: kuncitara. Kata yang selama ini, sejauh mata memandang, tak begitu umum dipakai untuk membahasaindonesiakan _lockdown_. Kebanyakan tulisan, berita sampai esai, pakai _karantina wilayah_. Di buku ini, _kuncitara_ dengan mudah ditemukan pada sinopsis.

Diksi menarik lain adalah _adikodrati_. Saya tak tahu sumber bahasa utama yang digambarkan dalam kata tersebut, tapi tetap saja ia membuat saya semakin hormat kepada Reni Indardini selaku penerjemah. _Sungkem ..._

Tapi ada yang mengganjal pada kata _corona_. Tebakan awal saya, ia bakal diterjemahkan sebagai _korona_. Ternyata tidak. Saya penasaran, alasan penerjemah tetap menggunakan kata bahasa Inggris _corona_. Tak dicetak miring pula.

Kalau ditanya bab paling favorit dari buku ini. Saya tak akan segan menjawab, bab saat Fang Fang melakukan tanya jawab dengan majalah sastra. Selain ada beberapa hal terkait kepenulisan yang dibahas, pola ceritanya juga segar. Tak seperti bab lain.

Saya dapat membayangkan, begitu serunya _Wuhan Diary_ saat ditulis Fang Fang secara langsung. Terbit harian. Cocok menjadi rujukan orang-orang yang ingin melepas ketegangan saat itu. Saya tak mengada-ngada. Para pembacanya di Tiongkok selalu berusaha tidur cukup malam demi menunggu tulisan Fang Fang terbit. Tapi, ketika diterbitkan sebagai buku, ia tak begitu seru.

Namun, bagi yang ingin mengetahui situasi Wuhan awal penyakit mewabah dan bagaimana pemerintah lokal menanganinya, serta _printilan_ pengetahuan tentang Tiongkok, jangan ragu untuk membaca buku ini.

\*) Konon, hantu merasa senang/risih kala disebut namanya, sehingga mereka akan mendatangi si penyebut nama. Jika saya menyebut nama hantu Jepang, kiranya mereka tak perlu repot-repot menyeberang lautan ratusan kilometer untuk terkikik di sebelah saya.

* * *

**Judul**: Wuhan Diary  
**Penulis**: Fang Fang  
**Penerbit**: Penerbit Bentang  
**Tebal**: xvi + 384 halaman
