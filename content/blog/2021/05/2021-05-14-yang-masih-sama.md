---
title: "Yang Masih Sama"
date: "2021-05-14"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-ahmed-aqtai-2233416-lentera-rmdzn.jpg"
aliases:
  - /post/2021-05-14-yang-masih-sama/
---

Hari ini. Hari kedua Idulfitri. 'Mat lebaran!

Kalau melihat kondisi sekarang, suasana lebaran masih belum semirip tahun 2020 ke atas. Ada kampung yang warganya sudah berkeliling saling menyambangi rumah tetangga, tapi ada juga yang warganya masih memilih untuk silaturahmi virtual. Melakukan panggilan telepon biasa maupun telepon video dari perangkat ponsel masing-masing.

Tahun lalu, di ruang keluarga dan kamar berukuran sekian petak. Kami bertelepon dengan keluarga yang tinggal di barat sana. Menyapa dan saling memberi salam seperti saat bertemu langsung di atap yang sama. Kadang, sambil bersuara meliuk, tercekat tiba-tiba.

Kemudian mengundang anggota keluarga lain. Dari yang sebelumnya panggilan tunggal, menjadi sebuah panggilan grup. Video pula. Melakukan hal seperti sebelumnya, sambil sebagian mengorbankan diri untuk mencabut sambungan agar keluarga lain dapat bergabung ke grup panggilan yang sama. Ingat, 'kan, saat itu, WhatsApp masih membatasi grup telepon video hanya sampai 4 orang jika tak lekas memperbarui versi?

Para tetangga yang sekaligus saudara (dengan garis keturunan yang cukup panjang) juga menghubungi lewat telepon video.

"_Sugeng riyadi nggih, Budhe/Mbak. Sedaya kalepatan nyuwun pangapunten_," kata masing-masing pemegang ponsel.

Kali itu, suasana tak normal. Jalanan kampung lengang. Sepi. Hanya satu dua orang lewat, sebagian kecil warga yang memutuskan untuk tetap berkunjung ke rumah-rumah tetangga.

Pada waktu yang sama, ponsel saya berdering. Situasi yang hampir tak saya sukai sepenuhnya. Bikin gugup. Jantung berdetak lebih kencang. Satu sampai dua hari sebelum Idulfitri, kawan-kawan lama ternyata berencana melakukan panggilan video grup. Dan deringan telepon itulah realisasinya.

Tapi, yaaa, maaf. Karena saya sedang ogah-ogahan menyapa dan berbincang dengan banyak orang, saya tak mengangkat panggilan mereka dan memilih untuk menyampaikan maaf kemudian.

Situasi pandemi mengobrak-abrik kebiasaan orang. Bagaimana cara berbincang? Bagaimana cara bersilaturahmi? Bagaimana cara berbasa-basi? Yang meskipun berhubungan dengan orang lain, itu justru cara untuk lebih memahami diri. Mengapa saya melakukannya? Apa yang dapat saya lakukan tanpa perlu menjalankan kegiatan seperti tahun sebelumnya?

_Lho_, bukannya memahami diri sudah dari dulu? Tentu saja. Memahami diri adalah sebuah proses. Bukan produk sekali jadi, lalu tinggalkan. Ia terus berjalan. Bagi yang sedang berproses memahami diri dan memutuskan untuk bertahan dengan aktivitas membatasi diri, tahun ini cenderung lebih berat daripada tahun kemarin. Tahun lalu, banyak orang sepemikiran, sama-sama saling menjaga dan berkomunikasi fisik sekenanya.

Sekarang? Banyak godaan untuk mengikuti orang lain dengan segala aktivitas normalnya. Itu hanya masalah kecil, masalah besarnya ketika ada orang yang justru jadi bahan olokan atau sindiran tetangganya karena masih menerapkan prinsipnya sendiri. Prinsip yang bahkan tak melukai orang lain.

"Terlalu takut."

"_Parnoan_."

"Keluar aja. Udah banyak yang keluar, lho."

"Ngapain takut?"

Dan semacam olokan/sindiran maupun ajakan menggoda lainnya.

Kamis (13/5) pagi, saya mengetik ini sambil mendengarkan belasan langkah kaki lewat jalan depan rumah. Satu keluarga besar, dengan baju necis, berkopiah, bergamis, dari semua usia. Anak SD sampai kakek nenek, berjalan beriringan dalam gerombol. Menyambangi tuan rumah yang sedang membukakan pintu dengan segala kaleng dan gelas minuman di meja.

Di sini. Sebaliknya. Merehatkan tubuh. Merasakan kebosanan yang sudah dimulai tahun lalu. Ya, kami juga manusia yang dapat merasa bosan. Tapi kami mencoba tetap berhati-hati.

Sembari menimbang, bagaimana kalau rasa bosan itu disalurkan pada pikiran untuk mengelolanya? Memahami diri apa yang dibutuhkan saat itu. Mencari alternatif untuk melakukan kegiatan lain yang diharap mampu membunuh perasaan itu.

Membaca. Menulis. Menonton film. Menonton video.

Coba dulu, _deh_, ulik diri sendiri. Seberapa besar kita mampu memahami diri sendiri? Semakin besar kita melakukannya, semakin kecil pula keinginan mengerecoki prinsip orang yang tak merugikan dirinya sekali pun. Fokus ke diri sendiri sebelum repot-repot memaksakan pahamnya pada orang lain.

"Hari raya. Semua bahagia. Kembali fitri. Kembali suci." Sambil menyantap opor beralas tikar dengan yang lain, lalu bersungut-sungut menyebut masih banyak orang yang lebih takut penyakit daripada hilangnya tradisi dan Tuhannya.

\[Foto oleh **[Ahmed Aqtai](https://www.pexels.com/id-id/@aqtai?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/lampu-meja-coklat-di-atas-meja-2233416/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
