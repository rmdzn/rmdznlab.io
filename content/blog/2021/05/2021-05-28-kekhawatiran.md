---
title: "Kekhawatiran"
date: "2021-05-28"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-keefe-tay-305810-bumi-resized-rmdzn.jpg"
aliases:
  - /post/2021-05-28-kekhawatiran/
---

Kamu sedang menggigit bakwan sambil menyeruput teh sebagai sajian cemilan sore di atas piring dan cangkir kecil. Enak-enak menggulir linimasi Twitter, sampai tiba pikiranmu ke titik potongan kata "anak" dan "bencana".

Bukan bacaan serampangan yang sekelebat mampir ke mata, tapi sebuah [cerita bergambar](https://twitter.com/gambarnana/status/1385554856727834626/photo/1). Mengisahkan perasaan seorang pria berkeluarga yang melihat semakin parahnya isu lingkungan. Dari berita laptop ke televisi, matanya beralih ke anaknya yang masih kecil lalu menangis membayangkan nasib si kecil. Apakah ia dapat menikmati bunga-bunga indah kala dewasa nanti?

https://twitter.com/gambarnana/status/1385554856727834626

Kehancuran alam dan dunia adalah keniscayaan, seperti yang dibentuk dalam kata "kiamat". Tapi seberapa besar keinginan orang-orang untuk "memperlambat" kehancuran tersebut dan memberi ruang bernafas generasi selanjutnya kelak?

Seberapa besar pula usahanya?

Mencoba mengikuti prinsip para pembela Tuhan dan rasulNya. Saat agama, Tuhan, dan Rasul diolok, kita dengan mudah reaktif merespon dan membela agama secara besar-besaran. Memarahi balik para penghina. Mengecam dengan sekeras-kerasnya.

Setiap kali narasi bertentangan muncul: "kenapa harus reaktif marah?", "Islam terlalu besar untuk dibela", "Allah pun, takkan ada sebiji olokan yang dapat meruntuhkan keagunganNya", timbullah respon secepat kilat bahwa kelak seorang muslim akan ditanyai posisi saat agama, Allah, dan Rasul diperlakukan dengan buruk. Apakah menjadi muslim yang membela agama, Allah, dan RasulNya? Atau hanya membiarkannya saja dan menjadi golongan munafik?

Lah, kok …

Ya memang begitu. Tindakan itu tidak salah. _Wong_ namanya _ghirah_. Pantaslah rasa marah menyeruak kalau agama, Allah, dan Rasul diolok. Bahkan kata Buya Hamka, seorang muslim yang kehilangan _ghirah_ akan serupa mayat. Membela adalah tanda hidup. Tentu, dengan cara [santun](https://news.detik.com/kolom/d-3520560/apa-itu-ghirah-islam).

Baguslah …

Apa iya berhenti di situ?

Bagaimana kalau _ghirah_\-nya diperluas lagi? Seperti paragraf-paragraf awal cuapan ini. Kesakitan dan kehancuran yang dialami bumi adalah kepastian yang sedang ditunda. Tapi, apa iya kita tidak malu jika kita tak bergabung dengan golongan yang berjuang menyehatkan dan merawat bumi, sesama makhluk ciptaan Allah?

Dalam [hukum perang](https://www.republika.co.id/berita/dunia-islam/khazanah/11/12/23/lwnebo-beginilah-aturan-islam-dalam-perang-dan-memperlakukan-tawanan), muslim tak boleh membunuh anak, perempuan, orang tua, dan orang yang sakit. Dan harus merawat tawanan perang dengan baik. Muslim pun tidak boleh merusak pohon berbuah, dan menyembelih domba, sapi, dan unta tapi tidak untuk dimakan.

Dalam situasi perang, menyakiti sembarang manusia berikut makhluk-makhluk lainnya tanpa tujuan saja tidak boleh, apalagi saat damai. Apa tak malu, selama ini kita ternyata sering menyakiti manusia lain dan merusak alam sekitar?

Pada hari perhitungan, kita ditanya posisi hati saat identitas dan sesembahan diolok. Kita dengan bangga berteriak keras di lapangan dan media sosial karena telah istikamah membela agama, Allah, dan Rasulullah.

Malaikat membalas, "bagaimana dengan dia?". Telunjuknya mengarah kepada seorang perempuan berkulit pucat. "Tuhan melarang manusia menyakiti sesama tanpa alasan kuat, dan kau menuduh perempuan itu sebagai pelacur karena selalu pulang malam."

"Tahukah kau? Dia tulang punggung keluarga. Kerja keras di sebuah kantor sebagai karyawan kontrak dan sorenya menjadi pencuci piring Rumah Makan Padang sampai larut malam."

"Dan kau!" teriak malaikat.

Pria di sebelah pria pertama terhentak kaget.

"Dengan baju lorengmu, kau selalu bertindak kasar dan menarik upeti setiap bulan dari pedagang sayur dan warung kelontong di pasar dengan alasan biaya keamanan. Tahukah kau? Pedagang itu seharusnya dapat hidup kecukupan tanpa berhutang besar di bank jika kau tak mengambil upeti itu."

"Kau juga! Pekerjaanmu yang seharusnya mendukung korban perkosaan untuk mendapatkan keadilan malah kau buang. Setiamu justru untuk pemerkosa dan keluarga mereka. Kau telah membebaskan makhluk keji yang pernah ada."

Pohon jati dan pohon kelapa di suatu sudut mengacungkan daun mereka yang lebar dan lentik. "Bagaimana dengan kami?"

"Orang-orang di sana membabat kami. Menindih bekas tempat berdiri kami dengan pohon baru yang lebih kerdil. Menjadikannya lebih gundul dan membuat rumah-rumah perawat kami tergusur air bah."

Pohon Aren menimpali.

"Lahan tempat kami pernah kukuh berdiri menjadi lubang besar. Mobil keruk rutin melewati. Truk-truk berbak besar membawa penuh batu-batu tambang. Air yang tersimpan rapat di bawah kami pun mengalir. Hilang. Kering. Bekas udara di atas kami penuh debu dan asap kendaraan."

Pria-pria yang awalnya meninggikan dada menjadi pusat pasi. Tertunduk. Sambil melirik takut ke arah rombongan pria dan wanita yang sedang digiring ke tempat buruk. Pimpinan dan tentara perang, yang bertahun-tahun merusak kota dan desa, meruntuhkan sekolah dan rumah sakit, serta menyengsarakan warga sipil yang tak bersalah.

Bertahun-tahun lalu, saat saya sedang hobi melompat-lompat tempat kajian. Ketika semangat saya masih sangat tinggi untuk mendengar kajian ala orasi. Berteriak keras. Menggebu-gebu. Sang penceramah berujar, _kalau kalian ingin selalu merasa damai, kalian telah terkena penyakit wahn_. _Wahn_ adalah cinta dunia, takut mati.

Saya tak tahu pasti maksud sang penceramah. Tak ada elaborasi, tak ada jemaah yang bertanya, juga saya yang masih terlalu lugu dan selalu mengiyakan dengan semangat.

Begitu marahnya saat saya mengingat peristiwa itu. Begitu menyebalkannya prinsip yang hanya fokus pada simbol. Tak peka dengan lingkungan. Tak peka dengan masalah nyata.

Bagaimana? Tertarikkah menjadi bagian kelompok penyayang makhluk Allah lainnya? Berhubungan baik dengan manusia lain dan alam sekitar? Atau sebaliknya, menjadi bagian kelompok perusak dan menyebalkan?

\[Foto oleh **[Keefe Tay](https://www.pexels.com/id-id/@keefe-tay-94966?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/langit-dan-awan-305810/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
