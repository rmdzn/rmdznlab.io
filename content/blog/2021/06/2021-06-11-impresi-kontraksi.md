---
title: "Impresi Kontraksi"
date: "2021-06-11"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-magda-ehlers-1301410-ban-rmdzn.jpg"
aliases:
  - /post/2021-06-11-impresi-kontraksi/
---

Pagi itu. Subuh.

Perempuan yang dikawini kakak saya, menjelang kuartal akhir tahun lalu, perutnya mulai bergejolak. Apa itu namanya? Mm … kontraksi?

Saya tak pernah membayangkan kehidupan ibu hamil dan bagaimana repotnya membopong bayi di rahim yang membesar setiap hari. Dari bintik kecil di balik perut normal, sampai janin dengan organ lengkapnya di balik perut gelembung.

Bayangan saya pun, prakelahiran selalu seperti adegan di film atau sinetron. Seorang calon ibu yang sudah hamil tua berjalan pelan di dalam rumah, tiba-tiba tubuh limbung, kepala _mentiung_, lalu menyebut perutnya yang sedang sakit sekali. Suami menawarkan rangkulan dan memapah istri ke dalam mobil menuju kantor bidan.

Calon ibu mengejan di atas kasur. Adegan berlanjut beberapa menit kemudian dan _oek_\-an bayi memotong suara kesakitan. Mengubah semuanya menjadi sebuah kelegaan.

Saya kira sekadar begitu.

Ternyata film atau sinetron memotong berjam-jam adegan. Beberapa kali, saya mendengar kenalan, masuk ruang bersalin pukul sekian, dan bayi baru keluar empat sampai enam jam kemudian.

Berbulan-bulan lalu, saudara sepupu masuk ke ruang bersalin bidan, tempatnya kontrol rutin saat hamil. Karena bukaan pintu rahim tak meningkat selama berjam-jam, akhirnya, ia dirujuk ke rumah sakit besar dan melahirkan di sana. Dengan proses berjam-jam lagi. Lagi.

Itu perkara waktu tunggu melahirkan di rumah bersalin atau rumah sakit. Belum jika membahas situasi yang sama tapi … di rumah. Kontraksi berulang. Dengan jarak yang semakin dekat. Dari beberapa jam sekali selama beberapa menit sampai hampir sesering menarik dan menghembuskan nafas.

Rasa sakit kontraksi jauh lebih sakit daripada sakit gigi. Saya kira begitu. Dan saya yakin memang begitu.

Omong-omong, pernahkah kamu emosi lantaran jalan depan rumah berisik karena suara motor berseliweran, suara pengeras suara konser dangdut dadakan, suara proses iris keramik tiba-tiba, suara teriakan anak-anak di rumah tetangga, padahal sedang sakit gigi?

Mangkel? Tentu. Makin berisik makin sakit.

Bagaimana kalau sakit gigi diganti kontraksi?

Apalagi itu. Saya tidak pernah dan tidak mungkin mengalami kontraksi kehamilan. Tapi, saya cukup yakin perbandingannya jomplang. Yang satu bikin mangkel, satunya lagi mangkel banget.

Mendekati Hari Perkiraan Lahir (HPL). Kontraksi makin sering terjadi. Hampir mencapai puncaknya saat dini hari, terus menerus sampai Subuh. Mbak Ipar berbaring miring dan kakak saya menemani di samping dipan.

"Mau ke Sadewa \[nama RSKIA -pen\]?"

"_Ho'o_."

Ternyata, karena masih pada tahap awal. Pihak Rumah Sakit menyarankan pulang. Katanya, mulut rahim belum cukup terbuka. Tunggu beberapa jam lagi dan silakan kembali ke Rumah Sakit nanti.

Satu dua cara pun rutin dilakukan di rumah untuk mendorong mulut rahim terus lebar membuka. Konsekuensinya, kontraksi makin menjadi. Saya yang mendengar di ruang depan sering ikut meringis membayangkan. Tapi nyatanya memang harus begitu. Semakin kontraksi terjadi, semakin besar pula potensi bayi _mbrojol_ sesegera mungkin.

Yang menyebalkan, pada jam saat bocah-bocah kecil berkeliaran, depan rumah berisik luar biasa. Saling berteriak, ramai bermain kartu "remi". Di sebelah mereka, anak lain sedang bermain lempar-lemparan dan tendang-tendangan toples blek wafer. Kontraksi yang menyakitkan ditambah suara-suara menyebalkan. _Arrgh_ … saya tak bisa lagi membayangkan.

Setelah melihat perjuangan kontraksi berjam-jam, saat masa prakelahiran. Dan mendengar cerita bagaimana Mbak Ipar saya berjuang melahirkan anak pertamanya, serta mendengar cerita ibu saya yang berangkat sendiri ke bidan tanpa ditemani satu pun orang (karena bapak saya sedang dinas) saat akan melahirkan saya. Maaf, jika saya belum dapat menyalurkan rasa syukur dan hormat dengan cara yang diharapkan. Saya juga tak dapat menuturkan rasa syukur secara ndakik-ndakik klasik dengan argumen perjuangan ibu yang merawat selama 9 bulan di kandungan hingga hari ini. Dan proses melahirkan yang bertaruh nyawa.

Saya mencoba bersyukur dan berterima kasih dengan tindakan yang saya lakukan.

…

Malam hari tiba. Waktunya lahiran. Saya hanya siaga di rumah. Menunggu jika ada panggilan darurat lain. Membawakan barang atau apa saja yang mungkin tertinggal di rumah--meski itu tidak pernah terjadi.

Singkat perjalanan. Bayi itu lahir. Keponakan terdekat pertama saya. Cucu pertama paklik-bulik saya. Anggota keluarga termuda klan Bejo dan klan Harun.

Calon anggota suku _Waterbander_ atau bahkan calon anggota infiltran Supernova.

Selamat datang, Pendekar!

![Tapak kaki bayi](/blog/2021/06/images/photo_2021-06-10_21-51-00.jpg)

Tapak Air.

\[Foto unggulan oleh **[Magda Ehlers](https://www.pexels.com/id-id/@magda-ehlers-pexels?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/ban-kendaraan-bertumpuk-banyak-1301410/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
