---
title: "Tergulung Gelombang"
date: "2021-06-18"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "gelombang-buku-rmdzn.jpg"
aliases:
  - /post/2021-06-18-tergulung-gelombang/
---

Kembali lagi. Berkelana di dunia _Supernova_. Dunia yang dibangun dua dekade lalu tapi baru saya sambangi dua tahun lalu. Dan kini, saya memasuki dunia salah satu tokoh lainnya--yang menggambarkan bentuk getaran, bergumul-gumul. Panggil saja, _Gelombang_!

Masih seperti pola judul _Supernova_ lain. Adegan dibuka dengan cuplikan kisah lain yang menjembatani cerita dari judul sebelumnya. Gio yang mencari-cari sang kekasih, Diva, ke mana-mana. Berakhir menyerah … pasrah.

Masih terliput misteri. Belum ada tanda-tanda ke mana arah cerita. Siapa mereka? Mau ke mana mereka? Tentang apa, sih? Dan lain-lain. Begitulah. Namanya saja masih cerita pembuka, saya mau berharap apa?

_Gelombang_ mengambil kisah seorang Batak, Alfa namanya, yang lahir di lingkungan dengan adat kental. Ini, salah satu hal favorit dari serial _Supernova_. Dee Lestari hampir selalu mengambil budaya nyata yang secara tidak langsung memperkenalkannya kepada pembaca. Ia mempersilakan saya mengenal tak sedikit pengetahuan baru, dan _Gelombang_ membawakannya lebih banyak.

Bukan _Supernova_ jika tak ada bahan tohokan. Sembari menceritakan secuplik adat dan budaya, _Gelombang_ menyindir fenomena sosial saat stigma melekat pada komunitas yang masih mempertahankan budaya hingga kepercayaan mereka. Salah satunya adalah stigma _sipek beju_ (penyembah berhala) yang disematkan kepada penganut agama asli Batak seperti yang dialami Alfa sendiri.

"Cap itu sudah lama distigmakan kepada penganut agama asli Batak oleh mereka yang merasa sudah menemukan Tuhan yang lebih benar dan digdaya."

Bukan berlebihan jika saya menyebut _Supernova_ sebagai salah satu novel yang sungguh-layak-dibaca. Paling tidak, dimulai dari kerumitan yang membingungkan pada _[Kesatria, Putri, dan Bintang Jatuh](https://ramdziana.wordpress.com/2019/09/15/kesatria-putri-dan-bintang-jatuh-sering-rumit-sering-menarik/)_ (KPBJ) sampai keseruan terus-terusan pada _Gelombang_.

![Buku Gelombang](/blog/2021/06/images/gelombang-buku-1-rmdzn.jpg)

Jauh, sebelum mencapai 100 halaman, _Gelombang_ sudah menawarkan keseruan. Penuh misteri, ditambah dengan bumbu-bumbu kisah lokal yang bikin merinding dan penasaran. Dan momen berkesan di awal adalah ketika Alfa, tokoh utama, disarankan untuk bersiap-siap dengan sebuah pertemuan (tentu dengan Akar, Petir, Partikel, dkk). _Crossover!_

Kalau dipikir-pikir, pola hidup tokoh antar judul _Supernova_ kurang lebih sama. Tinggal di Indonesia lalu memutuskan ke luar negeri, entah untuk bertualang, bekerja, atau sekadar mencari seseorang sembari melakukan dua tujuan sebelumnya. Karena situasi kondisi, tokoh kembali ke Indonesia.

Biasanya, cerita yang berpola terkesan membosankan, tapi tidak untuk _Supernova_. Pola tak lagi terlihat ketika konflik-konflik yang muncul bervariasi. Ditambah latar belakang yang berbeda dari setiap tokoh utama serta deskripsi lingkungan tempat ia berada.

Ada cerita, penggambaran wilayah, yang menarik pada setiap kisahnya. Pada _Gelombang_, terdapat penggambaran apartemen tua yang ditinggali geng-geng imigran. Saya dapat membayangkan berjalan di antara tembok-tembok dengan retakan hampir pada semua sisi, sarang laba-laba menggelantung di sudut-sudut eternit lorong, sebagian ruang diisi, sebagian lainnya kosong melompong. Tangga dengan debu yang tebal dan sebagian pegangan yang patah menghiasi tengah apartemen. Begitu juga dapat membayangkan penggambaran fisik lingkungan tempat tinggal Alfa maupun saat bersekelebat di mimpinya. Apakah pendeskripsiannya begitu spesial? Tidak juga, tapi bagus.

Nama lokasi, deskripsi tempat saat di Tibet seolah mengisahkan petualangan yang pernah terjadi. Saya pun cukup kaget dengan hasil [penelusuran](https://www.startpage.com/do/search?q=kalden+sakya), nama Kalden Sakya ternyata memang ada di dunia nyata. Bedanya, ia tak menulis buku berjudul _Milam Bardo_ (ada secara istilah, tapi tidak dengan bukunya), tapi tetap menulis buku yang berhubungan dengan Buddhisme.

Sayang, meski _Gelombang_ berkelana hingga Tibet, tak banyak bahasa lokal yang disisipkan di sana. Hanya sedikit sekali. Harapan saya untuk membaca cukup banyak Bahasa Tibet, pupus.

Namun, itu tidak begitu masalah. Kisah dalam _Gelombang_ berhasil menutupi harapan-harapan yang hilang. Ditambah fakta mengejutkan bahwa tokoh novel berasal dari Batak ternyata sudah direncanakan oleh Dee Lestari sejak tahun 2001. Oh iya, asal Dee Lestari sendiri memang Batak, sehingga tokoh Alfa dan lingkungannya menjadi sosok yang mewakili hasil kembalinya Dee Lestari ke tanah Batak untuk melakukan segala riset demi menulis cerita tentangnya. _Kudos, Bu Suri!_

_Gelombang_ adalah buku kedua sebelum judul terakhir, _Intelegensi Embun Pagi_, dan cerita mulai mengungkapkan apa dan siapa tokoh-tokohnya. Savara, Infiltran, dan Peretas.

_Les go, lanjut ke judul terakhir!!_

* * *

**Judul**: Gelombang  
**Penulis**: Dee Lestari  
**Penerbit**: Bentang  
**Tebal**: 492 halaman
