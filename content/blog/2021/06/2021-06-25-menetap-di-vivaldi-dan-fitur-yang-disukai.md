---
title: "Menetap di Vivaldi dan Fitur yang Disukai"
date: "2021-06-25"
categories: 
  - "cuap"
  - "tekno"
tags: 
  - "software"
coverImage: "vivaldi-kpl-rmdzn.jpg"
aliases:
  - /post/2021-06-25-menetap-di-vivaldi-dan-fitur-yang-disukai/
---

Menjadi pelompat bukan hanya tugas katak. Secara tidak langsung, manusia juga melakukannya. Entah pada hal apa saja.

Termasuk saat memilih perangkat lunak atau aplikasi komputasi harian. Saya pengguna sistem operasi [GNU/Linux](http://rafeyu.github.io/mengapa-saya-pakai-linux/) dan sudah berkali-kali pindah distribusi (distro), bahkan pernah sampai menjajal dua varian BSD populer; FreeBSD dan OpenBSD.

Dan, ya, saya sudah cukup dengan itu. Saya berhenti menjadi pelompat distro (_distrohoper_), menetap di distro yang sudah saya gunakan beberapa tahun terakhir.

Begitu juga dengan peramban, dulu saya pengguna berat Opera (Presto) tetapi beranjak dari sana saat ia mulai berganti mesin ke Chromium/Blink. Pindah ke Firefox, sambil sesekali mengintip Opera. Melihat apakah fitur sudah sejalan dengan Opera lama.

Karena alasan-alasan tertentu, saya pindah ke Pale Moon. Tapi kembali lagi ke Firefox. Cerita-cerita tentang saya sebagai pengguna garis keras Opera (Presto) dan pindah-pindah peramban boleh disimak di [Perjalanan Peramban Favorit, dari Opera sampai Pale Moon](/blog/2017/03/2017-03-26-perjalanan-peramban-favorit-dari-opera-sampai-pale-moon/).

Sungguh menyenangkan berganti markas, menetap menikmati semua ruang yang disajikan, menggunakan fitur-fitur yang tersedia. Apalagi kalau jalan pikir sesuai pemilik markas.

Jadi sekarang masih pakai Firefox?

Tidak.

Sudah sejak kuartal ketiga tahun 2020, saya menjadikan Vivaldi sebagai peramban baku di laptop saya.

![Vivaldi](/blog/2021/06/images/vivaldi-beranda-rmdzn.png)

Vivaldi

## Mengakrab Vivaldi

Jon Stephenson von Tetzchner, eks-mitra pendiri dan CEO Opera Software, muak saat perusahaan tempat ia pernah dirikan mengumumkan akan membunuh My Opera--yang benar-benar direalisasikan pada Maret 2014. My Opera merupakan paguyuban pengguna Opera. Di sana, kita dapat membuat rumah untuk menulis blog, mendapatkan alamat surel gratis, album foto, juga jejaring sosial (tempat para pengguna bisa saling bercengkerama).

Sebagai pengganti, Tetzchner berinisiatif, lalu meluncurkan layanan anyar Vivaldi.net sebagai tempat bernaung baru bagi pengguna My Opera, pada Desember 2013.

Tepat 27 Januari 2015, Tetzchner bersama tim yang ia bangun meluncurkan _Technical Preview_ pertama peramban Vivaldi. Versi belum matang, masih dalam proses kukus, tapi sudah bisa dipakai. Setahun kemudian, April 2016, mereka menerbitkan rilis stabil pertamanya, Vivaldi 1.0.

Saya menjadi bagian pengguna yang ikut mencoba versi awal Vivaldi. Dari _Technical Preview_, versi 1.x dan 2.x. Bolak-balik, dari Pale Moon ke Vivaldi dan sebaliknya, atau dari Firefox ke Vivaldi dan sebaliknya.

Sedikit alasan kenapa saya belum menggunakan Vivaldi sebagai peramban baku:

1. Masih cukup banyak _glitch_ pada versi-versi awal. Terutama saat Vivaldi dipakai di atas _tiling window manager_.
2. Bentrokan keyakinan. Saya tak begitu suka pasar peramban yang dikuasai mesin Chromium/Blink. Tak ada keragaman sama dengan merusak ekosistem web terbuka. Oleh karena itu, sebisa mungkin saya menggunakan Firefox yang sekarang satu-satunya rival besar Chromium/Blink dengan mesin Gecko-nya.

Berbeda saat Vivaldi menginjak versi 3.x, saya menyambut diri sendiri sebagai pengguna penuh mereka. _Yooott!!_

Alasan pindah ke Vivaldi:

1. Saat kembali mencoba beberapa pekan, Vivaldi sangat stabil.
2. Keyakinan sebelumnya saya buang jauh-jauh. Saya yang terbiasa dengan jalan pikir pengembangan Opera (Presto) seperti kembali ke rumah saat memakai Vivaldi. Jalan pikir yang tidak saya temukan pada Firefox, se-_open source_ apa pun itu pengembangannya.
3. Vivaldi sebisa mungkin membenamkan fitur ke dalam peramban. Mirip seperti yang dilakukan Opera. Efeknya, sumber daya memori yang dipakai bisa lebih hemat dibandingkan dengan Firefox dengan tambahan pengaya atau ekstensi.

## Fitur-fitur favorit di Vivaldi

Vivaldi punya buaaanyaaakk sekali fitur bawaan. Tak perlu menginstal ekstensi tambahan, cukup aktifkan melalui Pengaturan. Sebagian ada yang menjadi favorit saya.

1. **Catatan (Notes)**

_Catatan_ merupakan fitur yang pasti dan selalu saya pakai di Opera (Presto). Ia dapat dibuka di bilah samping atau dibuka secara penuh dalam satu tab.

_Catatan_ saya pakai untuk menulis ide sekelebat. Apa saja.

![Vivaldi Catatan](/blog/2021/06/images/vivaldi-catatan-rmdzn.png)

2. **Pemblokir iklan dan pelacak**

Pakai Vivaldi, saya tidak perlu memasang ekstensi pemblokir iklan macam uBlock Origin. Cukup aktifkan fitur tersebut, lalu pilih daftar sumber blokir iklan yang dimau, cuss iklan-iklan langsung tidak terlihat. Untuk blokir pelacak, Vivaldi menggunakan [DuckDuckGo Tracker Radar](https://github.com/duckduckgo/tracker-radar).

Sebagai catatan, Pemblokir Iklan dan Pelacak Vivaldi tidak sepowerful uBlock Origin, tapi cukup memberi kemampuan standar yang mumpuni.

3. **Tab Petak (_Tab Tiling_)**

Tab Petak dapat dipakai untuk melihat dua sampai empat tab sekaligus dalam satu jendela. Mirip _tiling window manager_, tapi ini di dalam peramban.

![Tab Petak di Vivaldi](/blog/2021/06/images/vivaldi-tab-petak-rmdzn.png)

Nonton YouTube sambil ngetik

4. **Hibernasi Tab**

_Nganu_ … memori perangkat saya tak terlalu besar, sehingga saat membuka banyak tab, memori lebih cepat tergerus dan kinerja melambat. Sementara itu, tab-tab yang saya buka cukup penting. _Eman-eman_ kalau sebagian tab tersebut saya tutup.

Untuk mengatasinya, saya tidurkan tab sementara, jika ingin membangunkan tab dan mengaksesnya, cukup klik tab tersebut.

5. **Pembaca Umpan (Feed Reader)**

Satu fitur lain yang sering saya pakai di Opera (Presto) adalah Pembaca Umpan alias _Feed Reader_. Fitur yang dapat dipakai untuk mengikuti pembaruan blog atau web lewat atom atau [RSS](https://ramdziana.wordpress.com/feed/). Beruntung, Vivaldi mulai mengenalkan Pembaca Umpan mulai versi 3.x sebagai fitur eksperimental dan masuk fase Beta pada versi 4.x.

![Pembaca Umpan di Vivaldi](/blog/2021/06/images/vivaldi-pembaca-umpan-rmdzn.png)

6. **Sinkronisasi (Sync)**

Terlihat sepele, ya? Tapi, fitur Sinkronisasi ini membantu saya menyimpan ratusan tautan _bookmark_ yang saya kumpulkan sejak beberapa tahun lalu. Dan berulang kali dipindah-pindah dari Pale Moon, Firefox, sampai Vivaldi.

Begitulah, Pemirsa, butuh beberapa bulan untuk memastikan saya benar-benar menetap di sini dan berbagi beberapa hal favorit. Kalau penasaran, coba saja unduh di [vivaldi.com](https://vivaldi.com).
