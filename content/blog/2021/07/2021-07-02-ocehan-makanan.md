---
title: "Ocehan Makanan"
date: "2021-07-02"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-ella-olsson-1640777-makanan-rmdzn.jpg"
aliases:
  - /post/2021-07-02-ocehan-makanan/
---

Dari sekian tumpuk perkakas, kebutuhan, produk, atau apalah itu--selain makhluk hidup dan uang, manusia pasti memiliki sesuatu yang dianggap berharga. Berharga yang tak selalu dicerminkan oleh nominal, tapi oleh nilai khusus, yang kadang dapat diungkapkan, kadang tidak. Dan bisa saja yang paling disukai sampai didukung dengan militan.

Setelah sedikit merenung sekenanya, berpikir sambil lalu, "makanan" ternyata menjadi sesuatu yang begitu.

Makanan jadi simbol pertemuan, keakraban, perdamaian, keberadaan, kemakmuran. Dalam kehidupan kampung, makanan menjadi penyangga guyub rukun. Sedang rapat, ada makanan. Sedang kerja bakti, ada makanan. Walaupun seadanya, cukup teh dan kacang-kacangan sampai gorengan. Hingga acara pentas seni, ada makanan dan tentu lebih lengkap. Mau lebih lengkap lagi, berikut jumlahnya yang lebih banyak? Datang saja ke pesta nikahan (tapi tidak saat pandemi, lho, ya, karena hidangannya pasti cenderung lebih sedikit).

Masih ingat, dulu saya sejak SD memang jarang jajan. Sekalipun jajan, uang saku tidak banyak. Kala teman sekelas uang saku kisaran 1.000, saya hanya separuhnya.

Era itu lumayan, lho. Tahun 2000 awal, 500 rupiah sudah bisa mendapatkan beberapa sendok nasi goreng atau mi di lapak depan SD. Atau beberapa butir salome kecil dengan satu salome bertelur puyuh. Tapi yaa begitu, dengan 500 rupiah harus lebih pintar mengelola duit, mau dipakai jajan sekali (istirahat pertama atau kedua) atau jajan dua kali, kemudian ingin jajan seberapa banyak.

Makanan bisa jadi tanda keakraban. Satu orang penjual telur gulung yang kadang saya beli seporsi kecil 100 atau 200, sekali waktu memberi saya dengan porsi 500 rupiah. Gulungannya lebih tebal. Cukup buat lauk sepiring nasi.

"Wuoh, Ram, gede banget. Beli berapa kamu?" Seorang teman menyapa dari belakang.

Mas Agung, penjual telur gulung itu membantu menjawab, "Lima ratus. Feri beli 500. Iya, kan, Fer?"

"Feri" adalah panggilan sirkel kampung dan saya mengangguk meski ketidaksinkronan panggilan membuat saya ingin mengumpat selayak _bangsat_.

Rumah sang penjual telur gulung memang tak jauh dari rumah nenek saya di kampung seberang. Kehidupannya tak hanya dekat secara tempat, tetapi juga komunikasinya. Telur gulung menjadi sarana bukti kedekatan, meski saat itu saya tak paham dan memilih untuk merasa tak enak.

Saya tak begitu paham, kenapa orang-orang memilih untuk makan di luar sementara di rumah sudah ada makanan, dimasak oleh orang tua sendiri atau siapa saja anggota keluarga. Tidak enakkah? Bosankah? Atau sekadar pengin makan di luar sambil nongki-nongki? Mending, uang sakunya ditabung untuk beli sesuatu. Ya, kan?

Semasa SMP, jajan di kantin bisa dihitung jari. Sekali jajan pun tidak lebih dari cemilan kecil-kecilan, tidak pernah apa saja yang dianggap mengenyangkan seperti Soto Ayam, yang saat itu adalah menu besar unggulan di kantin Bu Budi, sebelah lapangan voli.

Putra Bu Budi yang kadang ikut membantu melayani, seorang pemuda tanggung, cukup populer. Baik di kalangan murid laki-laki dan perempuan (tapi saya cukup yakin lebih banyak perempuan, sih). Setiap pergi ke kantin atau wilayah sekitar itu, ada saja orangnya. Kadang, bersama murid-murid lain dalam satu meja, _ngobrol_ lumayan ramai, terbahak-bahak, dengan mangkuk soto yang habis tak tersisa atau yang masih ada sedikit kuah di dalamnya. Sebagian murid menggenggam segelas es teh, sebagian lain memegang tempe goreng yang tergigit hampir habis.

Beranjak SMA, cara saya memandang kantin tak banyak berubah. Kenapa saya harus jajan kalau uangnya lebih baik buat beli bensin, beli majalah atau buku? Tapi, sekali dua kali boleh, lah. Beberapa kali juga boleh.

Jajan kala SMA hanya terjadi sepulang sekolah, bukan pada jam istirahat. Kala itu, saya hanya diajak teman sembari menunggu kegiatan ekstrakulikuler dimulai. Memesan menu populer, nasi dengan kering tempe dan sambal. Ternyata _weenak_, _Bre_! Meskipun pikiran bercabang karena muncul sesal keluar uang untuk tindakan yang tidak biasa dilakukan.

_Hess, malah jajan_, begitu kiranya.

Bertemu anak dari sirkel lain, dengan gaya lebih bebas setelah sebelumnya bertemu mereka dengan gaya religius di masjid sekolah. Bercakap-cakap semi intim. Mendengar kemajuan tentang proses apa saja, "bergosip" dengan penjual. Pertemuan dan makanan yang tersaji di mejalah yang membuka obrolan. Komunikasi terjadi, meski dengan level seadanya. Sambil bibir berkecap, semua bisa jadi bahan ocehan.

Saya menikmati makan di luar rumah dan menjadi tahu mengapa orang-orang melakukannya (di samping karena situasi kondisi memang tak ada makanan di rumah). Makanan dapat menjadi sekadar alat bertemu dan cengkerama, bahkan bisa sampai politik selevel negara.

Presiden Korsel dan Korut, Moon Jae-in dan Kim Jong-un menjalani pertemuan fenomenal 2018 lalu, yang berjuluk Korea Summit. Satu yang menjadi kehebohan adalah, apa lagi kalau bukan, makanan. Mi dingin yang dibawakan Pimpinan Korut kepada Presiden Korsel menjadi alat diplomatik yang bikin warung makan _naengmyeon_ (넹면) penuh antrean warga. Dalam beberapa hari, menu tersebut menjadi tren. Di Korut, menu yang sama bernama _raengmyeon_ (렝면).

Edan, tak?

Makanan menjadi tiang _gastro diplomasi_ sampai sesederhana rasa terima kasih.

Dalam tradisi pesta di Jawa (entah kalau di daerah lain), makanan menjadi alat syukur dan ungkapan terima kasih kepada tamu dan kepada tenaga masak (_rewang_). Yang pertama, terima kasih karena telah datang dan menyumbangkan uangnya. Yang kedua terima kasih karena merelakan waktunya seharian dalam dapur untuk memasak makanan para tamu.

Lihatlah begitu besar nilai makanan, sampai-sampai tak ada salahnya membenci setengah mati pada usaha untuk membuangnya begitu saja. Sikap itu yang juga membuat saya tak menyukai pesta-pesta--yang cenderung menjadi area bebas buang makanan. Pengunjung pesta yang menimbun nasi, sayur, dan lauk-lauk lalu menyisakan sebanyak-banyaknya di atas piring ceper. _Ater-ater_ untuk tenaga masak yang terlalu banyak di rumah sampai menjadi timbunan makanan membusuk sehari kemudian.

Menjadi pencinta makanan rumah, semonoton apa pun itu, jadi cukup menyenangkan. Tapi, kalau saya memberi diri kesempatan untuk menjadi penghobi kuliner di luar rumah berikut dana dan perut yang tak terbatas, saya akan menjadi penikmat total bakmi jawa, lotek, dan nasi telur burjoan belakang Direktorat Pajak.

\[Foto oleh **[Ella Olsson](https://www.pexels.com/id-id/@ella-olsson-572949?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/fotografi-flat-lay-dari-salad-sayuran-di-piring-1640777/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
