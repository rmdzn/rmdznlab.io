---
title: "Pesan Mati"
date: "2021-07-16"
categories: 
  - "cerpen"
tags:
  - "cerpen"
coverImage: "death-valley-89261_1280-rmdzn.jpg"
aliases:
  - /post/2021-07-16-pesan-mati/
---

Dalam sayup azan. Dengan langkah kakinya yang gontai, tangan itu sedikit gemetar ingin meraih sesuatu. Foto berpigura di atas meja dan cermin di baliknya yang ditujukan sebagai penghias ruang entah mengapa menjadi terasa menyebalkan. Tangan itu seperti siap membanting mereka satu persatu, sewaktu-waktu.

_Sreeet_! Tangan itu menyingkirkan tumpukan buku. Jarinya meniti lembaran-lembaran. Dari yang tebal sampai setipis brosur. Dari yang masih bersampul plastik, kertas putih jernih, sampai bentol-bentol berwarna kuning.

_Di mana kau?_ Ia bersungut-sungut.

Tiba mata di gagang laci. _Dasar tua!_ Ia menariknya dan mengangkat amplop coklat bercorak garis hitam-merah di tepinya.

Tulisan yang lumayan rapi menandainya telah ditulis dengan suasana tenang. Tak terburu-buru. Jernih dan juga terencana.

_Pagi, Ibu!_

_Maaf. Aku tak bermaksud lancang melangkahi ibu, bapak, dan Mas Deni. Hahaha. Bu, aku menulis ini dengan bahagia jadi tak usah repot-repot menitikkan air mata di atasnya. Ia tak berhak rusak, tapi jika itu terjadi, ya sudah, tidak apa-apa._

_Aku sudah membenamkan dalam pikiran ini akan terjadi kepada keluarga kita. Dan aku beruntung itu terjadi kepadaku. Aku bersyukur atas ini. Aku tak perlu melihat situasi menyakitkan yang mungkin akan dialami ibu, bapak, atau Mas Deni. Aku mungkin tak akan kuat._

_Maaf. Jika aku terlihat egois. Aku seperti tidak mementingkan perasaan ibu._

_Ibu dan Mas Deni tahu, Bapak adalah tokoh kampung hebat. Aku bersyukur karenanya. Tapi, di balik status itu, ada tanggung jawab yang sangat besar. Kata Paman Ben dari film Spiderman yang biasa aku dan mas Deni tonton sampai heboh itu, "dengan kekuatan yang besar, datang pula tanggung jawab yang besar"._

_Bapak punya kekuatan untuk menggerakkan warga. Saat musim banyaknya berita warga menolak tenaga kesehatan masuk kampung, tahun lalu, aku khawatir itu akan terjadi di kampung kita._

_Bapak mengiyakan segala bentuk berita palsu. Itu membahayakan. Karisma ketokohan bapak menjadi pisau bermata dua. Jika tentang keamanan dan kebaikan, karisma itu dapat menelan siapa saja yang tak patuh dan mengubahnya. Tapi, ketika karisma itu dipertontonkan pada topik yang berpotensi merusak, ia akan bergerak membunuh semena-mena._

_Aku tak menyesal pernah mengajak diskusi bapak tentang kerja keras nakes dan tuduhan-tuduhan bisnis yang dilancarkan kepada mereka. Itu satu-satunya kesempatan diskusi yang pernah kulakukan._

_Sedikit ragu pesan saya diterima. Tapi setidaknya, bapak tahu posisiku. Posisi anaknya dalam suasana pagebluk. Ketika ada ujaran hoaks di kampung, bapak tahu anaknya akan siap menerangkan bantahan kapan saja jika diperlukan._

_Itu tahun lalu. Harapan untuk situasi yang membaik tak juga terjadi. Ketidakpahaman dan ketidakteraturan masih saja dipelihara, ditambah sengkarut dari inkonsistensi pemerintah yang menjadi-jadi. Orang-orang makin banyak yang tak percaya adanya pandemi._

_Bapak masih hobi dengan berita-berita tidak jelas di YouTube maupun WhatsApp. Lebih menyukai mereka dan mengiyakannya, alih-alih mendengar ujaran langsung dari anggota keluarga._

_Maaf, Bu, ibu yang hampir paling tahu perasaanku, bagaimana pun aku selama ini tampak biasa-biasa saja saat ibu sedang ingin meluruskan pandangan bapak. Padahal saat mendengarkan situasi itu, sungguh tidak nyaman. Selalu ada rasa sesak dan emosi yang hampir meluap yang mengiringi. Di lain waktu, aku dengan kekeh selalu menolak usaha seperti itu. Maaf, Bu. Aku trauma dengan insiden-insiden lalu._

_Aku tidak tahu harus bagaimana lagi._

_Pekerjaanku yang kesana kemari sangat beresiko. Terprediksi. Dan aku hanya berharap kematianku menyadarkan keluarga yang suka meremehkan situasi dan tidak berhati-hati._ _Terutama bapak._

_Terima kasih, Bu. Telah melahirkan putra yang begini-begini saja, yang tak seperkasa namanya._

_Salam untuk semuanya. Aku hanya berharap matiku di kala jam favoritku. Pagi._

Kertas membasah. Perasaannya teracak-acak. Sesal yang tidak terduga membuncah. Kertas itu diremasnya kencang-kencang lalu dilemparkan ke sudut keranjang sampah.

Termenung.

Hari itu masih mencekam. Sedikit tenang dengan beberapa kali gangguan raungan sirine yang lewat dua arah. Namun, ia sama sekali tidak terganggu. Di atas kasur itu, ia hanya sibuk melamun dan memikirkan pesan yang baru dibacanya beberapa menit lalu.

_Ngeekk!_

Suara pelan pintu membuka menyadarkan lamunannya. Wajah sendu melongok, datang dari wanita paruh baya dengan baju terusan hitam.

"Pak, adek sudah datang."

"Ibu duluan saja. Aku susul."

Bapak berdiri pelan berjalan ke pintu. Air matanya masih merembes dan hampir membalut bengkak muka. Pipinya seperti menelurkan peluh-peluh kecil yang selalu tumbuh meski dihapus.

Remasan kertas yang teronggok di tumpukan paling atas kembali diambil dan disodokkannya ke saku.

Enam ambulans beriringan pelan. Ambulans yang pertama sampai sudah siaga di depan gapura makam. Petugas berbaju putih dari ujung kaki sampai ujung kepala, yang biasa ditonton di televisi kini selalu menyambangi tempat ini. Hampir setiap hari.

Peti mati turun pelan-pelan. Petugas menggotongnya ke dalam makam yang sudah dikeduk beberapa jam lalu. Lubang dipersiapkan tepat di pinggiran makam dan pelan-pelan peti masuk ke liang lahat.

Di teras rumah berwarna abu-abu itu, dengan pandangan yang jauh, satu keluarga memecahkan tangisnya.

"Maaf, dek."

\[Gambar oleh [Brigitte makes custom works from your photos, thanks a lot](https://pixabay.com/users/arttower-5337/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=89261) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=89261)\]
