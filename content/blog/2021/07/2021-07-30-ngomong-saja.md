---
title: "Ngomong Saja"
date: "2021-07-30"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-andrew-neel-5255996-mikir-rmdzn.jpg"
aliases:
  - /post/2021-07-30-ngomong-saja/
---

Kala waktu, kehidupan begitu normal. Berkumpul di balai sambil rapat menentukan tanggal lomba Agustus-an. Atau menentukan tanggal pentas seni dan menyiapkan pernak-pernik panggung dengan balon dan kertas krep berwarna-warni.

Seringnya--hidup yang begitu normal--ketika sekumpulan orang berteduh di bawah pohon atau baru saja menyelesaikan arisan rutin di pos siskamling bergosip membahas keputusan pejabat kampung yang hanya memikirkan udelnya sendiri. Sambil menunjuk tetangga lain yang begitu menyebalkan--di ranah urusan pribadi, bukan urusan publik (warga).

Saya suka dan rindu dengan suasana itu, begitu sempurnanya sikap manusiawinya. Tapi bukan berarti saya membenci setengah mati situasi seperti sekarang ini. Ya, suasana ini memang patut dibenci, tapi ada sedikit celah yang dapat saya sisipi dengan cara "abnormal", dan sedikit melegakan.

Baru kali ini, saya mampu menyampaikan pendapat dengan cukup bebas tanpa kebingungan. Cukup dengan satu-satunya motivasi: melindungi keluarga sendiri dengan segala celahnya dan melegakan diri.

Zaman sekolah. Pada mata pelajaran yang mana saja, kecuali tentu yang berhubungan dengan ilmu pasti (MIPA), saya selalu kebingungan saat ditugasi dengan soal esai. Mending kalau sekadar esai tentang sejarah atau ilmu pengetahuan yang sudah tercerita sejak lama, kalau esai tentang pendapat pribadi, _kok_ saya mabuk sendiri.

Hanya selisih beberapa hari setelah Masa Orientasi Sekolah (MOS) SMA, Bu Nuning, guru Bahasa Indonesia yang begitu tegas dan galak (stereotipe yang baru saya tahu setelah sekolah berminggu-minggu) menugasi anak didik barunya untuk bercerita panjang lebar tentang pengalaman sebelum masuk SMA. Dari alasan mengapa memilih sekolah ini dan bagaimana pengalaman saat MOS.

"Ceritakan pengalamanmu minimal 10 halaman," katanya.

Untung bukan 10 halaman folio bergaris, tetapi buku tulis kecil biasa. Saya? Mengarang bebas dengan ukuran huruf tangan di atas 13 _point_. Dengan tambahan ungkapan islami ala-ala alhamdulillah, insya Allah, bismillah.

Dengannya, bismillah, tugas lekas penuh 10 halaman.

Dan alhamdulillah-nya begitu.

Beberapa minggu kemudian setelah tugas dikumpulkan dan diberi "nilai", dengan muka lempeng dan seringai menyebalkan, sang guru mengkritik kami habis-habisan. Konten dan tulisan yang amburadul serta sindiran penggunaan huruf yang besarnya dan spasinya tak normal.

"Saya tahu, \[tulisannya diperbesar dan spasi lebar\] biar halamannya cepat penuh, kan?"

Pada tugas yang lain, hasilnya kurang lebih sama, meski tak separah tugas mata pelajaran Bahasa Indonesia, dan "sedikit bermutu" karena salin tempel sana sini dari Wikipedia dan situs web lain.

Ini tak cuma terjadi di dalam sekolah tapi juga di kehidupan ekstrakurikuler atau perkampungan. Di ekstrakurikuler atau organisasi luar sekolah, menyampaikan pendapat di depan teman-teman bukan hal gampang. Sekali berani _ngomong_, _cringe_ sendiri atau muncul respon tak menyenangkan dari forum, itu pun kalau direspon, kadang kala hanya menjadi angin lalu.

"Baik, terima kasih. Selanjutnya!". Begitu nadanya.

Saat rapat-rapat kampung, nuansanya penuh ketidakenakan--bapak, ibu, simbah-simbah, yang menatap mengelilingi kalian-kalian semua. Giliran bicara, yang keluar hanya gumaman. Dan malah seperti soal pelajaran bahasa Indonesia topik susun kata.

Intinya sama, bertahun-tahun, saya merasakan begitu sulitnya melampirkan pendapat sendiri. Seperti ada beban berat ketika akan mengungkapkan sesuatu. Takut diledek, dihakimi, dan dicueki.

Semakin ke sini ketakutan itu semakin luntur. Bukan hilang, _lho_, ya. Hanya teredam. Saya masih takut dengan ketiganya, apalagi di tengah budaya saklek saat orang-orang berlomba mengkritik orang yang berbeda pendapat.

Butuh bertahun-tahun hidup untuk menyadari kenapa ia bisa luntur. Kuncinya pada _respon_. Saya terlalu memikirkan respon yang saya dapat setelah berpendapat. Terlalu menimbang-nimbang hingga yang keluar hanya awang-awang, alias tak jadi berkata apa-apa.

Lalu bagaimana biar berani berpendapat? Cuek dengan respon yang mungkin akan didapat?

Tidak, dong. Potensi respon yang akan diperoleh tetap harus dipertimbangkan. Proses ini cukup sukar. Dan itulah salah satu cara melatih kepekaan. Menyadari topik yang dibicarakan dan menyadari rekan yang diajak bicara. Lalu menimbang bobot pengaruh jika pendapat dikeluarkan atau tidak dikeluarkan.

Saat cuapan ini ditulis, nomor WhatsApp saya diblokir tetangga dekat. Alasannya tersinggung karena saya mengingatkannya untuk menghapus status WhatsApp berisi video lama saat dirinya jalan-jalan di luar, padahal saat ini ia sedang isolasi mandiri di rumah.

Bobot pengaruh yang ditimbulkan jika saya mengingatkannya dengan jika saya tidak mengingatkannya, jomplang. Video lawas yang diunggah ulang ke WhatsApp dapat menjadi sumber salah paham. Bagaimana jika ada orang yang setelah melihatnya mengira bolehnya jalan-jalan walaupun sedang isolasi mandiri? Konyol.

Mengingatkannya berarti saya siap diputus jalur komunikasinya … _yep_, itu bukanlah masalah. Lebih baik begitu daripada terus-terusan berkontak dengan orang yang tidak mau mengerti, dan ceroboh kala pandemi, yang berpotensi membahayakan lingkungan jauh maupun dekat, secara langsung maupun tidak langsung.

Begitu.

Namun, sering kali kita tak perlu repot berhenti berpendapat jika tidak ada respon yang muncul sama sekali. Respon tidak selalu berbentuk lisan atau tulisan, kok. Dalam sebuah forum, pendapat yang kamu utarakan boleh jadi sepi dari tanggapan, tapi ternyata ia diingat kuat oleh beberapa orang yang mendengarkan. Saya percaya itu.

Tak perlu minder juga jika gaya pikirmu hanya diiyakan sedikit orang dan disebarkan secara "diam-diam" kepada sedikit orang lainnya. Tak perlu membayangkan diri seperti Bung Karno, Bung Tomo, Mao Zedong, atau Che Guevara.

Berpendapat atau beropini merupakan bagian dari komunikasi. Ia bukan ujung jalan tempat kita berkata, "hore, sampai!", tapi ia perjalanan itu sendiri. Tempat belajar mengurai pikiran, mengerti, peka, bersimpati dan berempati.

Jadi, ngomong saja, lah!

(sambil was-was kesamber UU ITE)

\[Foto oleh **[Andrew Neel](https://www.pexels.com/id-id/@andrew?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/pria-berkemeja-lengan-panjang-abu-abu-duduk-di-kursi-kayu-coklat-5255996/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
