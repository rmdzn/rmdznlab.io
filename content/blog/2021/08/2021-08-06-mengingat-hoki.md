---
title: "Mengingat Hoki"
date: "2021-08-06"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-fotografierende-1111597-luck-hoki-rmdzn.jpg"
aliases:
  - /post/2021-08-06-mengingat-hoki/
---

Si bayi lahir. Tengah malam. Rambutnya lebat.

Ia lahir sendiri, hanya ditemani ibu dan bapaknya di ruangan dominan putih, di bilangan Babarsari. Seperti ibu-ibu lain di rumah sakit Ibu dan Anak, hanya satu anggota keluarga yang boleh menemani sang ibu--suaminya, ibu bapaknya, atau yang lain. Sementara itu, keluarga lain hanya boleh menunggu di luar rumah sakit (kebanyakan justru tidak ikut ke rumah sakit, terlalu riskan untuk kesehatan).

Perlu berjam-jam ibunya berjuang supaya bukaan lekas lebih besar sambil beberapa kali meringis kesakitan dan melenguh. (Peluk jauh buat sang ibu).

_Oek … oek … oek!_

Saya tak tahu pasti bagaimana suara si bayi. Asumsi saya begitu, seperti narasi orang-orang pada umumnya. Siapa tahu si bayi melongokkan kepala sambil bilang, "halo, Mama," dengan bahasa Dunstan.

Seperti budaya perkampungan pada umumnya. _Njagong bayi_ tak bisa dihindari, bahkan masa pandemi. Kini giliran Aang kecil, panggil saja begitu, dengan wajah dan polah lucunya menjadi bayi baru yang dikunjungi puluhan orang dari banyak keluarga. Tetangga sampai keluarga dekat dan jauh. Rombongan jalan kaki, pesepeda motor, hingga rombongan dengan mobil Kijang atau Pajero.

Tamu-tamu ini datang seperti yang sudah diduga, tapi kadang juga tidak terduga. Suatu sore, rombongan ibu-ibu melangkahkan kaki sampai halaman rumah.

"_Assalamu'alaikum!_"

"_Wa'laikumsalam._"

Ada yang sekadar duduk sebentar lalu memberikan amplop putih, kemudian pamit. Ada pula yang bertamu seperti biasa. Membincangkan apa saja yang berhubungan dengan kelahiran si bayi, nama, dan kabar-kabar lain yang tak sempat tersampaikan sebelumnya.

Sebagian besar mengerti. Mereka hanya melihat Aang kecil dari pintu atau dari karpet yang digelar di ruang tengah. Kebetulan, Aang kecil yang sedang tidur memang mudah terlihat hanya dengan menengokkan muka ke kamar tidur.

Sebagian kecil saja yang tak dinyana masuk kamar tidur, lalu menyenggol pundak atau menyemol pipi Aang kecil yang sedang pulas bahkan saat si ibu tidak sempat dan sungkan menolak.

Itu sudah sekitar enam sampai tujuh minggu lalu. Dan selama itu pula keluarga Aang kecil, termasuk simbah dan pamannya--iya, saya--mendapatkan kabar tak enak berulang kali. Teman bapaknya yang pernah ikut menjenguk terinfeksi penyakit baru, beberapa tetangga juga melalui hal yang hal serupa. Atau minimal ikut isolasi mandiri setelah keluarga serumah terpapar. Kabar itu terjadi berhari-hari pasca waktu jenguk, untungnya.

Berulang kali sekeluarga kecil _greneng-greneng_: hoki Aang kecil ternyata besar banget.

Iya.

"Hoki" itu ada. Terpampang. Hoki tak selalu berupa harta segepok atau jodoh. Namanya juga peruntungan, terhindar dari penyakit sudah menjadi keberuntungan itu sendiri. Apalagi di situasi menyebarnya wabah seperti kini.

Faheem Younus, dokter yang sedang populer di Indonesia itu, November 2020 lalu [menceritakan](https://twitter.com/FaheemYounus/status/1327826911888879616) fakta bahwa 65% tidak terinfeksi flu spanyol dan 85% tidak terinfeksi H1N1 (Flu Babi). Twit bapak satu ini muncul dengan konteks melawan narasi bahwa semua orang akan terjangkit wabah Covid-19 sehingga tak perlu _tuh_ waspada ini itu, pakai masker, dll.

https://twitter.com/FaheemYounus/status/1327826911888879616

Laporan yang cukup menenangkan, tapi ingat, banyak manusia dalam persentase tersebut. Terkhusus flu spanyol yang memakan korban sangat banyak. Karena itu laporan justru dipakai untuk tetap berhati-hati, bukan waktunya untuk _leleh luweh_ (cuek bebek).

Hidup di masa wabah ini perlu menumbuhkan kesadaran penuh. Ketiadaan orang terinfeksi di lingkungan sendiri jangan menjadi titik kesombongan.

"Di tempat saya aman, _tuh_!"

"_Amaan, wes. Gak perlu khawatir,_" dalam konteks tidak perlu cemas dengan printilan protokol yang bikin repot.

_BIG NO!!_

Sebetulnya keamanan yang kita peroleh saat ini sekadar hoki. Hoki tidak tertular, tidak terinfeksi. Sadari itu. Lalu bekerjalah dengan hati-hati.

Ada garis tipis antara hoki atau memang usaha kita yang sudah begitu optimal. Namun, mengikuti prinsip orang Jawa, apa pun yang terjadi pasti ada (hoki) untungnya.

Saat berjalan hampir terserempet motor, "_untung tadi gak ketabrak._"

Saat tertabrak motor, "_untung cuma lecet. Gak perlu ke rumah sakit._"

Saat tertabrak motor dan opname di rumah sakit, "_untuk masih hidup._"

Untuk saat ini, plis _push your luck_. Bukan dalam artian idiom--melakukan dengan keras sampai mengorbankan apa yang telah diraih--tapi dalam arti harfiah. Dorong dengan keras kehokianmu, keberuntunganmu.

Seperti Aang kecil, jangan jadi sombong, tetap jatmika, dan hati-hati.

\[Foto oleh **[fotografierende](https://www.pexels.com/id-id/@fotografierende?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/orang-yang-akan-menangkap-empat-dadu-1111597/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
