---
title: "Ubah ke RSS Penuh"
date: "2021-08-20"
categories: 
  - "cuap"
tags: 
  - "blog"
coverImage: "message-in-a-bottle-3437294_1280-surat-rmdzn.jpg"
aliases:
  - /post/2021-08-20-ubah-ke-rss-penuh/
---

Menulis adalah bukti kehadiran. Bukti keberadaan. Saya dapat menyampaikan pikiran untuk saya simpan sendiri sebagai idealnya harta, atau saya sampaikan kepada orang-orang guna menyebarkan gagasan lebih luas.

Ada penulis, ada pembacanya. Hukumnya begitu. Meski tak selalu. Belum tentu gula yang kita ciptakan berhasil mengundang semut yang diharapkan. Sedih, _sih_, ini. Tapi itu urusan lain.

Tentang pembaca, semua penulis, atau lebih tepatnya platform untuk menulis, berlomba-lomba meningkatkan pengalaman proses baca. Bagaimana agar pembaca betah dan nyaman? Mudah diakses pula? Beragam cara dilakukan, dari yang paling teknis mengoptimalkan halaman supaya dapat diakses dengan cepat sampai mengubah tata letak halaman.

Namun, ada satu teknologi lawas yang hampir terlupakan, yang oleh sebagian orang masih menjadi unggulan … termasuk saya.

Perkenalkan RSS (RDF Site Summary atau lebih umum disebut Really Simple Sindication). Teknologi umpan (_feed_) web yang mengizinkan sebuah situs web untuk mengirim konten kepada pembacanya yang berlangganan. Jika media sosial punya istilah "notifikasi", dunia situs web atau blog sudah punya teknologi ini sejak lama--dalam bentuk RSS.

Baru tahu teknologi web RSS? Kalau tidak tahu, tak perlu merasa bersalah. Meski RSS sudah ada di dunia ini sejak tahun 1999, ia memang tak lagi populer. Atom yang diciptakan sebagai pembaru RSS pada tahun 2005 pun juga ikut-ikutan tak begitu populer.

Pemilik situs web atau blog kini lebih fokus menyebarkan konten melalui media sosial seperti Facebook atau Twitter. Banyak media daring terkenal seperti Detik, Tirto, dan Historia, bahkan setingkat platform UGC (_User Generated Content_) seperti Kompasiana, sama sekali tak tertarik mendukung RSS.

Beruntung sekali ketika Wordpress mendukung RSS sejak orok, versi Wordpress.com maupun Worpdress versi _self-host_. Blog saya pun akhirnya tak perlu repot-repot menambahkan fitur yang seharusnya ada pada setiap situs web.

Tapi ingat, ketidakpopuleran RSS bukan berarti kematian sang teknologi. Walau situs dengan fitur RSS cenderung [menurun](https://trends.builtwith.com/feeds/RSS), layanan atau aplikasi pembaca RSS justru banyak dan sebagiannya cukup terkenal. Feedly, apa lagi? InoReader juga ada. Kalau mau pilih tampilan dengan cita rasa jadul, ala Google Reader yang sudah mati itu, ada The Old Reader.

Itu baru aplikasi pembaca RSS daring, belum yang versi luring. Aplikasi desktop maupun _mobile_ untuk membaca RSS cukup banyak. Termasuk NewsFlow, Omea Reader, RSSOwl, Awasu, SharpReader, QuiteRSS, sampai yang se-_nerd_ Newsboat.

Baru-baru ini, [sebuah tulisan](https://ncase.me/rss/) menyadarkan saya. Mendukung RSS tak hanya sekadar menyediakan halaman berformat RSS atau Atom, tapi juga seharusnya menampilkan konten secara penuh.

Sebelum ke sana, omong-omong, setidaknya ada dua gaya tampilan RSS. Pertama, kutipan (_excerpt_), gaya yang mengharuskan pembaca mengklik tautan menuju situs web untuk membaca konten secara penuh. Kedua, konten penuh (_full_). Simpel, yakni gaya yang mengizinkan pembaca membaca konten langsung melalui aplikasi pembaca RSS.

Cara pertama menjebak pembaca mengunjungi situs web dan berputar-putar di sana. Cara kedua memanjakan pembaca dengan konten tanpa embel-embel komponen nirpenting.

Mengapa yang pertama yang terjadi? Dalam sudut pandang pengelola web, menjebak pembaca sama dengan meningkatkan trafik ke situs web. Apa itu salah? Tidak. Tapi, tak cukup menyenangkan kalau ternyata saat ingin keluar dari penjara (_walled garden_) media sosial malah masuk ke penjara (_walled garden_) yang lain.

Cara kedua membebaskan pengguna. Ia bukan lagi penjara dengan sistem pemantauan. Berapa lama penghuni penjara membaca cerita? Buku apa yang ia lempar, buku apa yang ia simak dengan cermat? Konten yang ditampilkan dalam RSS adalah murni konten tanpa embel-embel skrip iklan, pelacak, maupun tampilan yang dirasa tak perlu.

Hampir semua narablog yang saya ikuti menerapkan RSS dengan konten penuh. Termasuk [@segigitabaru](https://segigitabaru.wordpress.com/feed/), [@catatanwika](https://catatanwika.wordpress.com/feed/), bahkan juga [Bangwin](https://bangwin.net/feed/). Media yang cukup besar (yang cukup saya ikuti) dengan RSS konten penuh adalah [The Conversation Indonesia](https://theconversation.com/id). Begitu pun beberapa kanal YouTube (iya, platform sebesar YouTube saja mendukung RSS, lho).

Saya sendiri? Maju mundur. Padahal saya pendukung penuh teknologi RSS/Atom, tapi jalan pikir saya mengatakan dan mengaplikasikan cara pertama. Dan itu membuat dukungan saya kurang paripurna.

Setelah menimbang-nimbang, [mulai tulisan pekan lalu](/blog/2021/08/2021-08-13-pulsa/), blog saya mendukung RSS dengan konten penuh. Sila masukkan URL [https://ramdziana.wordpress.com/feed/](https://ramdziana.wordpress.com/feed/) ke aplikasi pembaca RSS untuk mendapatkan pembaruan konten langsung.

![RSS blog Ramdziana](/blog/2021/08/images/2021-08-20-11.04.56-ramdziana_wordpress_com-9a3f95a3f417.jpg)

Halaman RSS yang dibuka di Vivaldi

Pun jika ingin menikmati tulisan saya yang lain atau mendapatkan versi PDF dari cerpen-cerpen saya, sila kunjungi dan dukung saya di [KaryaKarsa](https://karyakarsa.com/rmdzn). Terima kasih selalu!

\[Gambar oleh [Antonios Ntoumas](https://pixabay.com/users/atlantios-4957810/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3437294) dari [Pixabay](https://pixabay.com/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=3437294)\]
