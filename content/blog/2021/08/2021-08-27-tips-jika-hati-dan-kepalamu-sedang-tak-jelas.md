---
title: "Tips Jika Hati dan Kepalamu Sedang Tak Jelas"
date: "2021-08-27"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-george-becker-374918-matematika-rmdzn.jpg"
aliases:
  - /post/2021-08-27-tips-jika-hati-dan-kepalamu-sedang-tak-jelas/
---

Apa yang kamu lakukan jika isi kepala dan hati sedang tidak bersahabat? Ya pusinglah, ya sedang tak termotivasilah, ya sedang suntuklah, ya sedang sedihlah, dan ya ya ya lainnya.

Menampik semuanya? Lalu bilang, "saya baik-baik saja"? Atau mendekam di kamar seharian, tidak melakukan apa-apa?

Respon pertama bukan respon yang sehat. Ucapan "saya baik-baik saja" cuma akal-akalan motivator kelas wahid. Yang saat naik ke atas panggung atau berhadapan dengan kamera, bibirnya selalu ditarik ke atas. Setelah itu, dunia seperti utopia dengan segala kebaikan dan kesuksesannya.

Cara kedua subjektif. Setiap orang memiliki caranya sendiri untuk memangkas apa saja yang bikin runyam hati dan pikiran. Dan kalau kamu memang tipe yang hanya ingin nongkrong di kamar, itu bukan masalah, asal jangan bertindak tidak-tidak alias membahayakan.

Ada opsi lain yang mungkin bisa kamu pertimbangkan demi tujuan yang sama:

1. **Mencuci piring**

_Yas_! Kalau ada piring atau gelas kotor yang sedang menganggur di ember atau wastafel, ini adalah saat yang tepat untuk menggosok mereka dengan sepenuh hati.

Tak perlu terpengaruh dengan sisa _blenyekan_ sambel, sisa saos yang tercocol-cocol tak habis, pecahan santan sayur lodeh bercampur kecap atau nasi kering yang berkerak di sisi-sisi piring. Kejijikan itu tak sebanding dengan kelegaan yang didapat.

Sunlight atau Mama Lemon berikut gerenjeng dan spon menjadi teman dekat. Kamu bisa menganggapnya sebagai teman duduk yang cukup menemani tanpa perlu diajak bicara. Mereka juga dapat menjadi rekan yang cakap untuk tak menghakimi kala diceritai. Coba saja, curhatkan saja pikiranmu kepada mereka. Jika mereka mampu merespon pun untung buatmu, pikiranmu akan teralihkan karena ingin segera lari tunggang langgang.

![Wastafel](/blog/2021/08/images/photo_2021-08-27_09-35-33.jpg)

Kalau diperhatikan, acap kali kegiatan mencuci piring menjadi hal melelahkan yang menenangkan. Ia menjadi kegiatan yang layak dipilih pasca pengajian atau pentas seni. Mencuci mangkok atau piring dan gelas atau jumbo teh daripada repot-repot mengepel lantai atau membongkar tenda. Namun ini kondisional, jika mencuci piring di kegiatan itu malah bikin tergesa-gesa, lakukan saja sebagai bentuk tugas, bukan untuk menenangkan diri. _Cus_, yang penting selesai.

Catatan, jangan mencuci piring kala marah. Potensi untuk membanting (menaruh dengan keras) piring ke rak lebih besar.

2. **Ke WC**

Ini mungkin tips yang cocok jika kamu tidak tinggal sendiri. Pada saat masuk kamar tidurmu tidak memungkinkan dan ke luar rumah bukan hal menjanjikan, pergi ke WC bisa menjadi salah satu jalan.

Kondisional. Bisa karena saat pengin benar-benar buang air atau cukup dengan pura-pura.

![Pintu WC](/blog/2021/08/images/photo_2021-08-27_09-35-57.jpg)

Duduk atau berjongkok di kloset sambil bengong atau menggulirkan layar ponsel dalam waktu cukup lama (tapi tetap hati-hati dengan konten yang kamu gulirkan, bisa jadi malah memperparah situasimu saat ini).

Bau menyengat dari buanganmu itu mungkin secara tak langsung menjadi pemantik ketenangan. Saya memang sok tahu. Tapi, siapa tahu kemunculan ide-ide segar atau konyol setiap kamu buang air justru didorong oleh itu? Di samping dorongan gelombang alfa dari lamunan tentu.

Bila buang air betulan, pikiranmu tak akan fokus pada masalah hidup, kecuali bagaimana barang bekas makanan dari perutmu keluar dengan lancar dan melegakan.

Namun, jangan lama-lama, lho, ya. Apalagi kalau hanya ada satu WC di rumah. Suatu waktu, kenyamananmu untuk bengong akan terganggu dengan gedoran pintu secara tiba-tiba. "WOE, GANTIAN!"

3. **Membaca**

Begitu klasik.

Memang.

Benar sekali, Bung.

Membaca dapat menekan perkembangan hormon kortisol penyebab stres. Kalau pikiran dan hati sedang tidak baik-baik saja, membaca seperti membuatmu keluar ke dimensi lain. Kamu tahu sedang punya masalah, tapi pikiranmu melayang sibuk memahami hal lain yang tak kasat mata.

![Buku Sherlock Holmes, "The Sign of The Four"](/blog/2021/08/images/photo_2021-08-27_09-44-06.jpg)

Tak perlu kaku. Pada titik ini, baca saja buku yang ringan, bukan buku Sapiens-nya Yuval Noah Harari atau Sejarah Dunia-nya Jonathan Black. Baca saja novela-novela, kumpulan cerita pendek, dan sejenisnya.

Tapi, "membaca" juga bukan berarti membaca buku, kok. Bisa juga buletin, koran dengan konten yang ringan-ringan. Bahkan juga buku manual produk apa saja. Apa pun. Asal, jangan membaca status _following_ di medsos, Kak, malah berpotensi bikin tak nyaman berkali-kali lipat.

Kegiatan membaca begitu fleksibel, bahkan dapat dikombinasikan dengan kegiatan lain. Kamu pun dapat membaca instruksi penggunaan deterjen dan pewangi--di WC--atau membaca bahan pasta gigi dan karbol. Baca saja tanpa berpikir apa-apa. _Los!_

4. **Bersih-bersih**

Mencuci piring adalah subkegiatan bersih-bersih. Subkegiatan lainnya termasuk menyapu lantai, mengepel, merapikan barang, mengibas debu, dan teman-temannya.

Setiap gerakan yang kamu lakukan mengalihkan pikiran dan hati yang sedang tidak karuan. Nikmati saja setiap prosesnya. Nikmati proses dan hasil menemukan gumpalan rambut rontok di bawah meja, gumpalan debu tersangkut remasan kertas bersama butiran kacang atom melempem di pojok dipan, menemukan sarang laba-laba bergelantungan di pojok eternit kamar, dll.

![](/blog/2021/08/images/photo_2021-08-27_09-58-26.jpg)

Bersih-bersih akan memaksamu untuk bergerak, cocok buat orang-orang yang kalau diam malah menyembulkan banyak pikiran.

Siapa tahu juga kan, selain menemukan kotoran kering normal sampai basah menjijikkan, kamu malah menemukan majalah Bobo dan Fantasi lawas yang terselip di balik kardus atau buku lain lawas yang selama ini dicari gila-gilaan.

5. **Tidur**

Ini, sih, tidak perlu dibahas, ya. Kalau situasi mendukung untuk tidur, mending segera tidur.

6. **Datang ke psikolog atau psikiater**

Tips-tips di atas adalah tindakan awal. Jika tak lekas membaik mungkin memang perlu ditangani secara profesional. Poin ini menjadi satu-satunya poin yang belum pernah saya lakukan. Tapi, untukmu yang perlu, semoga dikuatkan dan diberanikan!

\[Foto utama oleh **[George Becker](https://www.pexels.com/id-id/@eye4dtail?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/1-1-3-teks-di-papan-tulis-hitam-374918/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
