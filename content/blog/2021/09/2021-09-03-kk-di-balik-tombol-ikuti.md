---
title: "[KK] Di Balik Tombol IKUTI"
date: "2021-09-03"
tags: 
  - "cuap"
coverImage: "plang-gambar-rmdzn.jpg"
aliases:
  - /post/2021-09-03-kk-di-balik-tombol-ikuti/
---

Pekan ini sedang ingin menelurkan esai di KaryaKarsa. Salah satu jenis konten yang memang pernah saya niatkan untuk ditulis di platform tersebut.

**[Di Balik Tombol IKUTI](https://karyakarsa.com/rmdzn/di-balik-tom)** menceritakan tombol IKUTI yang ada di dalam KaryaKarsa itu sendiri, arti di baliknya yang saya tangkap. Bahasannya mungkin terlihat internal (lingkup platform KaryaKarsa), tapi sebetulnya umum banget.

Esai tersebut bisa diperoleh dengan harga satuan atau harga paket terendah. _Cao_, sampai jumpa di sana!

Terima kasih!

[![](/blog/2021/09/images/2021-09-03-12.08.22-karyakarsa.com-6c3d8573b05b.png)](https://karyakarsa.com/rmdzn/di-balik-tom)
