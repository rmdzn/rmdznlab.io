---
title: "Menaruh Ego"
date: "2021-09-10"
categories: 
  - "cuap"
tags: 
  - "cuap"
coverImage: "pexels-abbat-5595566-ego-rmdzn.jpg"
aliases:
  - /post/2021-09-10-menaruh-ego/
---

Saya menyukai satu grup musik luar negeri. Legendaris. Tahulah, Westlife itu. Cukup bikin "gila" setelah album _Coast to Coast_ meluncur.

Entah pernah dapat dari mana, lupa, keluarga saya memiliki _Compact Disc_ (CD) bajakan tur Westlife. Tepatnya tur album _Coast to Coast_. Komponen piringan kecil yang perlu diputar menggunakan _VCD Player_ itulah yang membuat saya menyukai album ini.

_My Love_ menjadi judul andalan. Judul lain seperti _I Lay My Love on You_ dan _When You Are Looking Like That_ menjadi pengiring adegan Shane dkk naik ke pesawat atau berjalan di lorong untuk siap-siap beraktivitas di acara berikutnya.

Bagian "menyayat" ketika _Close_ diputar sebagai latar adegan penggemar yang menangis mengelukan Westlife di terminal bandara atau saat jumpa penggemar, begitu juga dengan _Soledad_. Meskipun konteks kedua judul bagai Gunung Everest dan Palung Mariana, tapi nuansa baladanya sama.

Itu belasan tahun lalu. Waktu yang sama saat seseorang mengatakan grup musik itu sebagai idola para cewek. "Cowok, kok, suka Westlife," begitu ucapnya.

Hari itu, saya merasa malu, juga marah dengan sangat. _Mak prempeng_. Tersinggung. Sampai-sampai ingin menyelepet si pengolok dengan _srempat_ sandal bekas yang sudah terkikis-kikis bagian bawah.

Pada lain kesempatan, beberapa tahun lalu, seorang teman bercerita sambil bersungut-sungut.

"Dia itu cerewet banget."

Dari ekspresinya yang mengucapkannya sambil mengernyit, teman saya seperti sedang mengingat situasi dan muka menyebalkan pengoloknya. "Saat sedang menonton Detective Conan, dia nyinyir di belakang, 'iih, anime gak pernah tamat-tamat'."

Respon dalam hatinya mirip seperti saya. Bedanya mungkin ingin menampar pengolok dengan alas sandal Swallow yang masih baru kinyis-kinyis.

Sebetulnya, mereka hanya berkomentar, kenapa kita merasa terpantik dan terserang? Bahkan di sini, saya menyebut mereka sebagai "pengolok", padahal belum tentu label itu benar.

Sebentar.

Sesungguhnya kuncinya pada ego. Bukan Ego the Living Planet dari jagat film Marvel, bapaknya Peter Quill. Tapi ego yang secara harfiah berarti _diri pribadi_.

Lalu, ada apa dengan ego? Dia salah?

Ego tidak salah, kita yang salah. Selama hidup, ternyata kita repot-repot rajin menaruh ego pada apa saja. Pada barang, pada sesama manusia, pada nama tertentu, pada lokasi, dan pada lainnya. Repot, kan? Manusia telah diberi anugerah untuk sibuk dengan banyak hal--jangan salah, melamun itu juga kesibukan tersendiri--termasuk ego dalam diri mereka sendiri, tapi masih saja menyisihkan waktu untuk menyalin egonya lalu ditaruh di tempat lain dan menali sang ego di tempat itu dengan benang berutas-utas.

Ketika objek disenggol, ego yang tertempel di sana ikut mencak-mencak. Dengan cara dibatin atau disampaikan terang-terangan. Marah-marah di media sosial, misalnya.

Manusia sangat rapuh. Serapuh kayu gapuk di bawah dipan yang digerogoti rayap. Lalu pasrah? Ya, jangan. Justru perlu menguatkan diri dengan pengendalian diri yang lebih baik. Jangan malah menyangkal lalu berpura-pura menjadi sekuat baja dengan cara menaruh ego di tempat lain dan mengandalkannya.

Di Viranesia, suatu negara yang dibentuk dengan kebijakan berbasis viralitas, berdirilah komunitas besar yang masing-masing mempunyai spektrum politik ekstrem. Tapi prinsip mereka semua sama. Sama-sama saling menaruh ego pada simbol-simbol. Nama, lambang, mau pun identitas. Sekali simbol tersenggol, teriaknya kencang-kencang di banyak platform. Luring mau pun daring.

Itu di level gerombolan.

Balik ke level individu. Ego yang ditaruh di tempat lain ini sungguh menyebalkan. Saat ingin mencoba berdamai dengan situasi, si ego menyembulkan diri dan membuat rasa tidak nyaman muncul. Hati ini rasanya ingin ikut marah, jari-jarinya juga rasanya ingin ikut mengetik panjang lebar.

Saya masih belajar mencopot ego yang ada di tempat lain dan pelan-pelan mengubah respon jika sewaktu-waktu ada yang menyenggol tempat ego saya pernah bercokol.

Sadar. Bahasa nganunya _aware_. Kalau sudah _aware_ dengan pikiran dan tindak tanduk sendiri, akan lebih mudah melepaskan ego yang tidak perlu. Dan mampu mengembalikan kontrol kepada diri sendiri.

Saya masih belajar, dan semoga kamu juga. Biar kala waktu dapat menyukai dan menyayangi sesuatu tanpa perlu marah-marah saat ada yang menyenggol secara langsung mau pun tidak langsung. Menyenggol dengan sengaja mau pun tidak sengaja.

\[Foto oleh **[Abbat](https://www.pexels.com/id-id/@abbat-2342223?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)** dari **[Pexels](https://www.pexels.com/id-id/foto/mode-pria-orang-orang-orang-5595566/?utm_content=attributionCopyText&utm_medium=referral&utm_source=pexels)**\]
