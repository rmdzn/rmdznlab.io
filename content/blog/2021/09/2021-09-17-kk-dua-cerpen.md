---
title: "[KK] Dua Cerpen"
date: "2021-09-17"
categories: 
  - "cerpen"
tags: 
  - "cerpen"
coverImage: "pexels-thirdman-5407942-buku-hitam-rmdzn-e1631852642970.jpg"
aliases:
  - /post/2021-09-17-kk-dua-cerpen/
---

Baru sekali di minggu ini, saya menulis dua cerpen sekaligus. Cerpen yang pendek--sudah pendek, tapi lebih pendek lagi.

Bukan di blog ini, tetapi di KaryaKarsa. Dalam rangka memenuhi tantangan. Cerpennya dapat diakses secara gratis.

## [Temu Terakhir](https://karyakarsa.com/rmdzn/tantangan2u-45836)

![Jam](/blog/2021/09/images/pexels-olya-kobruseva-5556299-rmdzn.jpg)

## [Korban](https://karyakarsa.com/rmdzn/tantangan2u-korban-oleh-ramdziana-challenge-46370)

![Penyihir](/blog/2021/09/images/pexels-thirdman-5407942-buku-hitam-rmdzn-1.jpg)

Klik judul cerpen untuk langsung mengaksesnya.

Terima kasih!
