---
title: "Kepada Siapa Genta Berdentangan"
date: "2021-09-24"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "kepada-siapa-genta-berdentangan-rmdzn-2.jpg"
aliases:
  - /post/2021-09-24-merasakan-intrik-perang/
---

Hai, hai. Bertemu lagi di kategori ulasan buku 👋

Berminggu-minggu kemarin saya "sibuk" membaca novel _Kepada Siapa Genta Berdentangan_ alias _For Whom the Bell Tolls_ (judul aslinya). Salah satu karya besar seorang Ernest Hemingway.

_Kepada Siapa Genta Berdentangan_ (KSGB) menjadi novel Ernest Hemingway pertama yang saya baca. Sebuah kisah berdasarkan waktu nyata, tepatnya saat Perang Sipil Spanyol.

KSGB penuh dengan dialog. Setidaknya pada awal-awal bab. Bahkan saya sempat menyimpulkan KSGB sebagai cerita _dialogue driven_. Entah, apakah ada istilah resmi untuk itu. Tapi, tahu, kan, maksud saya? Cerita dibangun dari ucapan-ucapan, dari tutur para tokoh.

Ternyata tidak. Novel ini senormal novel pada umumnya. Hanya saja, beberapa adegan dikisahkan dengan obrolan yang panjang. Sehingga dalam batas tertentu bikin pusing. Pada sisi lain, terdapat paragraf panjang yang tidak nyaman untuk diikuti.

![](https://ramdziana.files.wordpress.com/2021/09/kepada-siapa-genta-berdentangan-rmdzn.jpg?w=555)

KSGB sebenarnya seru. Robert Jordan, dosen bahasa Spanyol yang juga sekaligus ahli ledak, diberi tugas untuk menghancurkan sebuah jembatan. Ia dibantu beberapa rekan, termasuk Anselmo. Kisah-kisahnya penuh interaksi dan rencana-rencana.

Sayangnya saya tidak merasakan kenikmatan pada setiap lembarnya. Terjemahannya tidak begitu ramah. Saya kurang cocok dengan penggunaan kata _banget_, _gitu_, dan _deh_. Tambah menyebalkan lagi, ketika saya menemukan kata _pada_ yang berarti _padha_ dalam Bahasa Jawa. Seperti, "_… yang pada mengenakan baju-baju usang mereka, …_" (hal. 142) dan "_… orang-orang pada mengistirahatkan cambuk-gebuk …_" (hal. 143).

Itu mungkin yang menjadikan saya tak semangat menghabiskan bacaan. Rasanya campur aduk. Saya pengin banget membaca buku Ernest Hemingway, di saat bersamaan, kok, malah bikin gedek kepala. Untuk menghabiskan 600 halaman, saya sampai perlu meminjam sebanyak 10 kali (bahkan lebih) lewat iPusnas.

Saya tadi mengatakan "seru", bukan hanya dalam arti cerita secara keseluruhan, tapi ke pesan-pesan eksplisit yang disampaikan.

Terfavorit dapat ditemukan pada halaman 34, "Robert Jordan bangkit dan mengangkat ransel itu dari pintu masuk gua dan menyenderkannya bersisian di sebuah batang pohon. Ia tahu apa isinya dan ia tidak pernah suka melihat keduanya berdekatan." Ransel Robert Jordan berisi dinamit yang menyimbolkan kerusakan dan kematian sedangkan pohon menyimbolkan kehidupan. Keren, Cuy.

Sisi menarik lainnya ada pada pemakaian bahasa Spanyol. Dalam teks asli, Hemingway memang sengaja tidak menerjemahkan ucapan Spanyol ke bahasa Inggris. Mending jika itu umpatan, saya tak perlu tahu artinya, tapi kalau ucapan lainnya? Saya cuma menebak dari aktivitas yang sedang dilakukan tokoh dan ucapan berikutnya. Salah satu contoh, _estoy muy borrucho_ (hal. 272) yang setelah saya _copas_ ke aplikasi penerjemah artinya _saya mabuk_.

_Kepada Siapa Genta Berdentangan_ bukan menjadi salah satu buku favorit. Tapi, kalau saya punya niat untuk membaca buku itu lagi, saya mungkin mencoba baca versi bahasa Inggrisnya.

* * *

**Judul**: Kepada Siapa Genta Berdentangan  
**Penulis**: Ernest Hemingway  
**Penerbit**: Diva Press  
**Tebal**: 600 halaman
