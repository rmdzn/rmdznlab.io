---
title: "Bertemu Rudy"
date: "2022-03-13"
categories: 
  - "buku"
  - "ulasan"
tags: 
  - "buku"
coverImage: "buku-rudy-featured.jpg"
aliases:
  - /post/2022-03-13-bertemu-rudy/
---

Waktunya buku lagi!

Satu buku yang saya baca kali ini malah belum pernah masuk daftar baca. Hanya pernah melihat selinting saja, mengingat penulis buku juga pernah menelurkan film yang menceritakan tokoh yang sama. Film yang begitu populer. Yang dibintangi Reza Rahadian itu, lho.

Beruntung, pembaca buku yang saya ikuti di Twitter mengiklankan buku _Rudy_ sebagai salah satu buku favoritnya. Dan bermaksud membelikan buku itu kepada pengikutnya yang beruntung.

_Wohoo_, sekali, dong. Saya tak pernah berharap apa-apa dari tawaran _giveaway_. Namun, kali ini saya mencoba ikut dan … _taraaa_ … saya mendapatkan buku _Rudy_. Terima kasih, Mbak [Zakia](https://twitter.com/optimustist)!

![Buku Rudy](/blog/2022/03/images/buku-rudy-img_20220228_171804.jpg)

_Rudy_ adalah buku biografi. Begitu, pandangan saya saat melihatnya berseliweran di linimasa medsos jauh sebelumnya. Oiya, satu kali pun saya belum pernah membaca buku biografi sehingga ketertarikan saya terhadapnya juga cukup besar.

Saya membayangkan _Rudy_ yang begitu "datar" layaknya biografi pada umumnya. Asumsi yang tak adil mengingat saya sendiri belum pernah membaca buku biografi. Namun, prolog buku menyadarkan saya. Akan banyak kisah menyenangkan yang disampaikan dengan cara seru. Mari kita coba!

Cukup membaca beberapa halaman bab awal, _Rudy_ disajikan sebagai biografi yang dibalut layaknya narasi cerita pendek atau novel. Terlihat di bagian percakapan tokoh dan penggambaran suasana yang renyah. Begini salah satunya:

_Tanpa pikir panjang, Rudy berjalan melintasi jalan dan langsung mendekat. Rudy berteriak, "Ainun, kamu jelek! Sudah hitam, gendut lagi!" Seketika semua orang diam. Hening. Mata Rudy bertatapan dengan mata Ainun… (dst)_ (hal. 90). Itu satu, ada juga kisah konyol yang tercerita dengan segar: ketika teman Rudy menemukan "balon" lalu ditiup Rudy sendiri dan bikin panik ibu bapaknya.

Saya membayangkan masa kecil Rudy seperti sosok pecinta garis keras pesawat sejak awal. Aslinya tidak begitu. Rudy justru mengenal pesawat pertama kali sebagai barang perusak. Pesawat yang bakal memisahkan dirinya dengan buku dan rumah. Iya … Rudy ternyata sering mengungsi karena perang dan itu sungguh traumatis.

Dalam setiap bab, buku _Rudy_ tak menampilkan ilustrasi ala kadarnya. Ada sisipan foto jadul bersejarah tentang masa kecil Rudy, keluarganya, dan apa saja yang berhubungan dengannya--foto yang menggambarkan garis besar bab yang disajikan. Dua foto yang paling saya sukai adalah foto anak-anak asuhan Papi Mami Rudy berjejer dan foto Alwi Habibie (papi Rudy) di atas sepeda motor Harley Davidson. Foto sang papi sangat kuat setelah disandingkan dengan cerita Rudy yang ternyata begitu tergantung dengannya--sebagai tempat sandaran maupun tempat diskusi.

_Rudy_ tidak dibuka dengan kisah-kisah penemuan dan kebahagiaan seperti yang saya bayangkan sebelumnya. Setelah kisah sedih Rudy yang selalu pindah tempat tinggal akibat perang, Rudy juga kehilangan sang adik. Yang berlanjut dengan meninggalnya sang papi beberapa tahun kemudian.

Sang papi yang sangat Rudy andalkan tak lagi terjangkau. Tidak berlebihkan ketika saya bilang sepenggal kisah itu bikin saya juga ikut resah--Rudy yang kehilangan pegangan.

Rudy adalah cerminan anak dengan pikiran terbuka dan terundung (yang lalu mampu mengubah diri dari asosial menjadi sosok mudah bergaul). Rudy juga merupakan sosok yang mampu memanfaatkan privilisenya dengan sangat baik. Tidak akan bosan saya menyatakan proses pengisahan tentang itu di buku ini begitu menyenangkan.

Banyak referensi kehidupan masa dulu. Termasuk saat Rudy diplonco kakak angkatan. Coba saja baca bab _Enam Bulan Kuliah_ yang gemas sekaligus menyebalkan.

Lagi-lagi, ini tak cuma soal keilmuan eksakta, ada sikap toleran Rudy muda yang patut ditiru dan layak menjadi buah bibir. Salah satunya, _nih_, Rudy yang terbiasa dengan gereja sejak kecil yang membuatnya nyaman ikut berdoa di sana saat berada di Jerman. Dan Rudy yang juga memiliki teman, Lim Keng Kie, seorang tionghoa-sunda yang baik hati. Tak memandang segala status, fisik, budaya … keduanya sungguh rekat.

_"Memang, lebih mudah melihat orang dari bungkusnya saja karena memahami manusia pada dasarnya memang melelahkan. Namun, semua tinggal masalah kemauan."_ (hal. 132)

Tidak berhenti di situ. Terdapat kisah heroik yang menarik berikut idealisme tinggi yang membesarkan hati. Dari perjuangan mami Rudy yang membuka hotel demi dapat mengirimkan uang secara rutin kepada sang putra di Jerman, sampai Rudy yang menolak bujukan permainan politik praktis kala akan mengadakan seminar PPI. Dan tentu saja, terdapat kisah-kasih _uwu_ yang ditunjukkan sepasang sejoli, Rudy dan Ainun, berikut beberapa drama yang mengitarinya.

Haduh.

"Haduh" yang memuaskan.

_Rudy_ dan kisah-kisah di dalamnya sungguh kaya. Tapi tak jelimet. Seperti yang saya bilang tadi-tadi. Narasi dalam buku ini dapat dinikmati dengan mudah, tak perlu pusing-pusing.

_Recommended?_

Tentu saja!

* * *

**Judul**: Rudy, Kisah Masa Muda Sang Visioner  
**Penulis**: Gina S. Noer  
**Penerbit**: Penerbit Bentang  
**Tebal**: xiv + 298 halaman
