+++
title = "Merangkul Luka-luka yang Hadir Sejak Kecil"
date = "2023-07-19T20:11:49+07:00"
tags = ["buku"]
+++

Pernah suatu kali saya teringat memori masa kecil. Situasi buruk yang bikin saya tidak nyaman, yang setelah dipikir-pikir lebih dalam, ternyata masih membayangi saya hingga saat ini. Bukan bayangan yang selalu muncul, setiap saat setiap waktu, tapi muncul saat dipicu oleh sesuatu. Ketidaknyamanan yang dihasilkan mempengaruhi *mood* dan tindakan saya, meski tidak lama kemudian reda.

Dalam bahasan "kekinian" terdapat istilah *inner child*. Arti gampangnya, suatu dampak emosi yang dirasakan saat kanak-kanak dan menetap hingga dewasa, baik menyenangkan atau menyakitkan, yang muncul secara tidak disadari.

Saya tidak begitu banyak mengulik buku tentang *inner child*, tetapi saya langsung kepincut saat Akal Buku mengadakan bedah buku *Si Kecil yang Terluka dalam Tubuh Orang Dewasa* bersama penulisnya, Patresia Kirnandita.

Tentang *inner child* negatif.

![Sampul buku "Si Kecil yang Terluka dalam Tubuh Orang Dewasa](/blog/2023/07/images/si-kecil-yang-terluka-dalam-tubuh-orang-dewasa.jpg)

Satu ucapan yang masih terngiang-ngiang hingga saat ini, Patres, begitu panggilannya, mengatakan jika perilaku buruk orang tua tidak terjadi kepadamu, bukan berarti hal tersebut tidak pernah terjadi kepada orang lain. Dan, ya, ternyata dari beragam orang tua, ada yang seseram itu. Dunia ini tidak hanya berisi orang tua yang harmonis, yang selalu lemah lembut terhadap anak-anaknya, yang dapat diajak ngobrol dengan nyaman. Buku ini menjadi catatan besar sikap orang tua yang memberi efek besar kepada Patres. Sangat besar.

Banyak hal yang membuat *inner child* negatif muncul. Salah satu yang begitu *relate* dengan saya adalah invalidasi. Pernah tidak, *sih*, perasaanmu diremehkan orang tua? Pasti pernah. Patresia mencontohkan kasus temannya, Ulfa, yang semasa kecilnya supergugup ketika tampil di depan umum. Bukannya menguatkan, ibunya justru bilang, "*ah, gitu doang enggak berani.*"

Tujuan si ibu mungkin agar Ulfa berpikir, "*ah, iya juga. Gass mah ini. Pasti berani.*" Tapi tidak. Anak-anak tidak berpikir sekeras itu. Yang ada, ketika anak tidak divalidasi, mereka bakal merasa tidak percaya diri, kesepian, kesulitan memproses dan mengekspresikan emosi, sampai minim kemampuan untuk menoleransi tekanan (hal. 28).

Sikap buruk orang tua terhadap anak sering mengalir begitu saja. Bukan sikap yang diniatkan dari awal untuk dilakukan. Ia turun temurun. Gaya asuh yang secara tidak sadar diturunkan dari orang tua sebelumnya. Patresia mengutip adanya 4 gaya pengasuhan: gaya otoriter, gaya permisif, gaya otoritatif, dan gaya pengabaian. Gaya ketiga yang paling ideal, orang tua sangat suportif terhadap anaknya dengan tetap menerapkan batasan-batasan. Gaya paling buruk adalah gaya otoriter yang terus mengekang dan gaya pengabaian, sebaliknya, membiarkan tanpa menerapkan batasan.

Benar kata banyak orang, menulis sama dengan membuka kerentanan. Begitu pun Patresia. Di sini, ia mengakui *inner child*\-nya betul-betul terluka. Ketidakpercayaan orang tua--sang relasi pertama--kepada dirinya membuatnya membangun tembok tinggi-tinggi. Pokoknya, apa-apa harus sendiri. Pokoknya kuat. Super mandiri. Hingga seorang kerabat menyebutkan "kemandiriannya itu mengerikan". Hal yang dikonfirmasi sendiri oleh Patresia saat konsultasi bersama psikolog beberapa waktu kemudian. Oiya, luka tersebut juga mempengaruhi relasi dengan pasangan dan anaknya sekarang.

Buku ini sangat penting, meskipun tidak menjadi buku favorit saya. Sebagai catatan, ia tidak mengajarkanmu marah-marah kepada orang tua. Justru sebaliknya, di balik kekecewaanmu terhadap sikap mereka, buku ini memberi sudut pandang lain. Bahwa, sikap buruk orang tua ada karena mereka mengalami hal buruk serupa pada masa kecilnya -- *inner child* orang tuamu terluka. Maka maklumlah.

Namun, bukan berarti kamu melanggengkan cara pengasuhan orang tuamu yang buruk. Jangan lagi diteruskan. Berhentilah di kamu, seperti Patresia yang masih berjuang untuk tidak meniru sikap orang tuanya saat mendidik sang anak, secara sadar maupun tidak sadar.

Begitu banyak referensi dalam buku, sayangnya catatan kaki kurang informatif. Seperti hanya menyantumkan sumber bacaan secara kasar, contoh "*Psychology Today"* atau hanya judul artikel dalam sebuah situs (tanpa ada keterangan URL-nya). *Like, whaaat? Ini gimana kalau tertarik ingin mengunjungi sumber kutipannya? Ke mana gitu?*

*Si Kecil yang Terluka dalam Tubuh Orang Dewasa* adalah buku nonfiksi kesekian yang saya rekomendasikan. Cocok untuk menambah wawasan bacaan topik pengasuhan anak (*parenting*).

---
**Judul**: Si Kecil yang Terluka dalam Tubuh Orang Dewasa

**Penulis**: Patresia Kirnandita

**Penerbit**: EA Books

**Tebal**: xviii+230 halaman

**Tahun terbit**: 2021

