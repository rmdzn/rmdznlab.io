+++
title = "Membaca Adalah Kesendirian"
date = "2023-07-23T22:03:20+07:00"

tags = ["cuap"]
+++

Membaca adalah kesendirian. Sejak dahulu saya berpikir seperti itu.

Kamu masuk ke rumah, membuka buku, lalu membacanya berlarut-larut. Diam. Sepi. Begitu lama. Situasi yang bisa dibayangkan dan sangat bisa dipahami saat sedang melakukannya (baik dengan objek buku, koran, maupun majalah). Yaa, sambil sesekali terdengar tegukan minuman atau gigitan cemilan.

Begitu juga jika kamu membaca di perpustakaan atau di ruang terbuka lain. Meskipun di tengah "keramaian", mata dan kepala hanya fokus pada rentetan huruf-kata-kalimat. Lalu otak dan hati lah yang sibuk pelan-pelan menyimpan dan mengolahnya.

Situasi tersebut mengiyakan apa kata banyak orang, bahwa membaca adalah aktivitas personal. Ia dapat menjadi salah satu solusi ampuh untuk mengatasi suntuk, *burn out*, dan kepenatan kepala lainnya.

Saat sudah (pura-pura) selesai dengan kesendirian, kamu akhirnya mencoba bertemu banyak kepala. Dengan syarat, harus tetap bertema perbukuan, baca membaca. Tentu saja kamu bisa datang ke acara diskusi. Diskusi tematik tentang judul buku tertentu hingga fenomena dunia perbukuan pada umumnya, termasuk tentang kepenulisan dan industri di baliknya. Bisa juga acara nondiskusi, seperti gelar wicara dengan penerbit atau penulis buku.

## Gelar wicara

Pertama kali saya mengikuti acara perbukuan adalah sesi jumpa dan sapa (*meet and greet*) di Gramedia dengan Nawang Nidlo Titisari (penulis buku *Hilang*) serta Brian Khrisna (penulis *The Book of Almost* dan *This is Why I Need You*) di Gramedia. Sedangkan acara kedua adalah gelar wicara dengan Amanatia Junda (penulis [*Waktu untuk Tidak Menikah*](/blog/2020-02-14-menyimak-isu-isu-perempuan-lewat-cerpen/) dan [*Kepergian Kedua*](/blog/2020-03-27-kepergian-kedua-sederhana-yang-kaya/)), serta Kalis Mardiasih dengan *Hijrah Jangan Jauh-Jauh, Nanti Nyasar!*\-nya di Togamas Kotabaru.

Sudah terbayangkan lah, ya? Tentang gelar wicara bekerja dan bayangan topik-topik di dalamnya. Karena acara tersebut tematik, tentu bahasan tidak jauh-jauh dari judul acara yang disampaikan.

## Diskusi ala-ala

Lalu, berikutnya!

Diskusi di toko buku independen: Akal Buku. Tentu saja, di sini juga bertemu dengan kedua mbak-mbak di atas.

Acara berjuluk *Nyore di Akal Buku* itu mengundang Wijaya Kusuma Eka Putra alias Eka Pocer, "sesepuh" toko buku independen dan pendiri Penerbit Pojok Cerpen. Banyak informasi dan pengetahuan baru yang saya dapatkan. Mulai dari pengalamannya menjajakan buku hingga mendirikan penerbitan sampai bubar.

Bahasan paling menarik bagi saya adalah "kenapa, sih, tidak ada acara perbukuan akbar lagi di Jogja?"

Dengan gaya *romantisasi-Jogja*\-nya (begitulah tuduhan yang disematkan Kalis kepada Eka), pria itu mengatakan Jogja adalah pasarnya orang penyuka buku. Begitu gampang mencari toko buku besar maupun independen di sini. Tapi, ide dan tindakan nyata untuk mengadakan acara akbar seputar perbukuan malah diambil kota lain.

*Wow, bahasan paling menarik ini!*

'Kan ada *Islamic Book Fair* yang sering diadakan di Mandala Bakti Wanitatama? Iya, benar. IBF adalah langganan tempat saya berkunjung sepulang sekolah dulu. Tapi, sesuai jenamanya, acara tersebut fokus pada satu hal: dunia perbukuan Islam. Buku-buku islam, gelar wicara dengan narasumber tokoh agama, dan acara yang berhubungan dengan ngaji-ngaji lainnya. Yang saya maksud dengan "acara perbukuan" adalah acara perbukuan umum, tidak merujuk pada identitas tertentu.

Dahulu, Jogja memiliki acara [Kampung Buku Jogja (KBJ)](https://www.instagram.com/kampungbukujogja/) \-- bagian dari *wishlist* acara yang ingin saya kunjungi :( Namun, karena beberapa alasan, ia tak dapat berlanjut. Justru, acara-acara serupa meledak dan sedang sering-seringnya diadakan di luar Jogja (*ehemmm ... Patjar Merah*).

![Berfoto-foto di Akal Buku](/blog/2023/07/images/nyore-di-akal-buku.jpeg "Foto oleh Akal Buku")

##### (foto oleh Akal Buku)

Begitulah. Kegiatan membaca dan mengunjungi gelaran pameran buku serta ajang diskusi adalah kegiatan terpisah. Ia tak dapat dilakukan berbarengan, atau setidaknya, membaca lalu membahasnya dalam satu tempat.

Memangnya ada kegiatan yang menyatukan kesemuanya dalam satu waktu dan satu tempat? Ada, ternyata. Tidak secara harfiah begitu, sih. Bukan membaca dan diskusi sekaligus, tetapi membaca apapun bersama, lalu mendiskusikannya -- secara spontan maupun terencana. Ketidakjelian saya memburamkan fakta bahwa begitu banyak acara seperti itu, tinggal kemauan untuk ikut bergabung atau bahkan membentuknya sendiri.

## Baca bareng

Hari itu, siang yang lumayan terik, saya membuka pengalaman baru dengan mengikuti aktivitas membaca buku yang diadakan oleh Juwita, dengan karakter bookstagram [@reviewgabut](https://instagram.com/reviewgabut/). #STREAD, julukannya, menjenamakan diri sebagai "membaca di mana saja" -- *street reading*.

Saya menyukai penjelasan @reviewgabut dalam [sebuah postingan](https://www.instagram.com/p/CphAWznMGFp/>), "salah satu misi #STREAD adalah menciptakan ruang personal dengan buku di mana aja. Bukan soal dunia memaklumi senyapnya kegiatan membaca, tapi pembaca membuat ruang buat diri mereka sendiri."

[*Merawat Luka Batin*](/blog/2023-01-09-merawat-luka-batin/) menjadi buku yang saya nikmati kala debut di sana. Di lapangan Klebengan, utara fakultas teknik UNY. Bertemu dengan lima orang. Dua di antaranya penggagas acara: sang sepasang, setali sepatu, Juwita dan Rekistra.

![5 orang masing-masing membawa buku menghadap kamera](/blog/2023/07/images/stread-desember-2022.jpeg)

*Vibe*\-nya memang beda dibandingkan membaca sendiri di teras rumah atau di sisi kamar. Dorongan untuk membaca lebih banyak. Halaman perhalaman. Asli. Yang membuat saya tidak menyangka sudah membaca sekitar 1 jam fokus tanpa gangguan.

Omong-omong, seperti memilih produk dalam etalase toko atau lokapasar, saya cukup selektif memilih komunitas baca buku yang memang ingin saya ikuti. Jatuhlah minat saya pada kegiatan #STREAD. Dari sekian linimasa kegiatan baca buku bareng, sepertinya #STREAD oleh @reviewgabut lebih santai (ssst ... ini ternyata dikonfirmasi sang empu kegiatan beberapa bulan kemudian). Saya justru sedang mencari yang seperti itu, bukan klub buku yang memiliki agenda diskusi terencana.

Tebaak selanjutnya?

Saat itu kegiatannya memang santai. Bonus sesi sampul buku pula! Setelah secara natural kawan-kawan baca menurunkan bukunya, obrolan mengalir alami. Entah membahas suatu kegiatan, skripsian, kuliah, maupun bercerita tentang buku-buku tertentu. Esai, novel, manga, dll.

![kawan baca di #STREAD berfoto bersama di Paperplane](/blog/2023/07/images/stread-bulan-ramadan.jpg)

Mengikuti kegiatan membaca buku bersama adalah salah satu hal yang patut saya syukuri dan rayakan. Begitu juga bisa mendapatkan komunitas atau kawan baca yang langsung sekali klik. Ia menjadi cara saya berlabuh ke suatu zona nyaman lain. Sebuah privilese yang tak bisa dihitung dengan sekali angkatan tangan.

Jadi, betulkah membaca adalah kesendirian?

##### (foto-foto oleh @reviewgabut)
