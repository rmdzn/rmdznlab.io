+++
title = "Eksil yang Berjuang, Eksil yang Pulang"
date = "2023-09-03T11:57:05+07:00"
tags = ["buku"]
+++

Saya sudah bilang kemarin, [*Laut Bercerita*](/blog/2023-06-11-laut-yang-bercerita/) ternyata adalah pembuka keinginan untuk membaca karya Leila Salikha Chudori lainnya. *Pulang* menjadi pilihan berikutnya. Di antara tumpukan buku-buku di toko buku, *Pulang* selalu menonjol dan menarik perhatian saya. Berulang kali. Ketika dompet sudah berisi sejumlah uang, *Pulang* pun lalu berada di tangan dengan hati senang.

![Kover buku Pulang](/blog/2023/09/images/pulang.jpg)

Kisah padanya terlihat tidak asing. *Pulang* merupakan fiksi sejarah yang mengambil waktu semasa Orde Baru -- Indonesia yang dipimpin oleh Presiden Soeharto -- seperti yang ada pada kisah *Laut Bercerita*. Bedanya di sini: fokus kisah *Pulang* ada pada eksil atau tahanan politik pasca tragedi 1965.

Buku ini membawa saya ke Prancis. Negara yang sejarahnya juga begitu kelam hingga sampai derajat revolusi. Memang, seperti ada pesan tersirat dari sana. Seperti kelamnya Prancis yang dapat diubah pesat dengan pemenggalan raja dan para borjuis, Indonesia mungkin juga bisa seperti itu. Mungkin, lho, yaa.

Itu hanyalah "pesan" berdasar dugaan saya. Yang pasti, *Pulang* menyinggung sejarah Mei 1968 di Prancis sebagai referensi. Saat itu, mahasiswa memulai gerakan protes, kemudian merembet ke kaum buruh dan masyarakat umum. Mereka meminta pemerintah De Gaulle yang berkuasa sejak tahun 1958 untuk turun. Ternyata Mei 1968 menjadi gerakan protes terbesar pada abad 20 di negeri L'Hexagone itu.

Cerita yang disampaikan dipotong ke dalam bab-bab. Utamanya berdasarkan tokoh, tapi ada selipan kisah berdasarkan waktu (disampaikan dalam bentuk surat atau catatan harian). Selipan cerita berdasarkan surat tak begitu saya sukai, seperti terkesan meloncat tiba-tiba. Kadang, saat membaca potongan kisah dalam bentuk surat (biasanya ditulis dengan huruf miring), saya perlu mengintip cerita sebelumnya karena lupa sedang membahas apa.

Ada dua kisah yang paling saya sukai dari *Pulang*. Pertama, tentang Ekalaya (Ekalawya). Kedua, tentang petualangan Lintang mencari konten wawancara.

Ekalaya adalah salah satu tokoh dalam kitab Mahabharata. Seorang yang ingin berguru sebagai pemanah kepada Drona tetapi ditolak. Drona selama ini lebih mengutamakan sang murid kesayangannya, Arjuna. Ekalaya yang lebih jago daripada Arjuna pun akhirnya mau "berkorban" atas permintaan Drona dan berakhirlah status Ekalaya sebagai yang lebih unggul daripada Arjuna. Ekalaya *be like*, "*its okay, gak papa*".

Kisah Ekalaya mengisi beberapa lembar dalam sebuah bab, dan itu cukup menarik. Paling tidak, dapat menjeda kisah sengsara yang dialami Dimas Suryo, yaa, meskipun sebenarnya Ekalaya adalah cerminan dari Dimas itu sendiri.

Petualangan Lintang yang sibuk mencari korban atau tokoh untuk diwawancarai cukup seru. Di sana tersirat protes terhadap sejarah yang dibelokkan, termasuk pada diorama G30S/PKI.

Juga betul-betul menguras emosi: tuduhan-tuduhan kepada para eksil yang didengar langsung oleh Lintang Utara. Rasa tegang dikuras habis-habisan saat adegan larinya putri Dimas Suryo dan kawan-kawannya itu dari kerusuhan 1998. Dag dig dug!

*Pulang* membawa sudut pandang baru untukmu yang selama ini hanya membaca sejarah dari satu sumber saja. Setidaknya, kisah di dalamnya menggambarkan satu sisi lain yang patut untuk disimak. Bagi yang asing dengan "alternatif" sudut pandang sejarah, *Pulang* bisa saja membawamu ke bacaan lain yang membahas hal serupa, yang lebih dalam.

Saya cukup puas dengan *Pulang*\-nya bu Leila ini, dan tentu sepadan juga buat kalian yang kepengin baca karya-karya beliau.

---

**Judul**: Pulang

**Penulis**: Leila S. Chudori

**Penerbit**: KPG (Kepustakaan Populer Gramedia)

**Tebal**: viii+461 halaman

**Tahun terbit**: 2012
