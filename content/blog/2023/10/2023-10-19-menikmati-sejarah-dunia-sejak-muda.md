+++
title = "Menikmati Sejarah Dunia Sejak Muda"
date = "2023-10-19T19:54:44+07:00"
tags = ["buku"]
+++

Pernah tidak, sih, menemukan bacaan pengetahuan yang populer sebagai "terlalu hafalan" tapi disampaikan dengan gaya lebih santai?

Saya menemukan itu. Sebetulnya, sering berseliweran di lini masa. Di akun-akun penjaja buku maupun akun-akun perbukuan umum. Berulang kali menjadi buku "yang direkomendasikan" sebagai buku yang layak banget untuk dibaca.

Perkenalkan: *Sejarah Dunia untuk Pembaca Muda*. Sebuah buku yang--tentu saja--berbicara tentang sejarah. Dari sebelum adanya negara (prasejarah). Hingga sejarah dari satu negara ke negara lain.

Ditulis oleh Ernst Hans Josef Gombrich, buku ini ternyata sangat sukses di dunia, yang kemudian mendorong beliau untuk menulis buku lain dan menjadi buku populer selanjutnya.

![buku Sejarah Dunia untuk Pembaca](/blog/2023/10/images/sejarah-dunia-utk-pembaca-muda.jpg)

Buku ini menjenamakan diri sebagai "untuk pembaca muda" dan itu berhasil. Sudah terlihat bahkan di bab pertama: misal pada penggunaan "kamu". Alasan saya terlalu simpel, ya? Baiklah, coba bayangkan jika teks yang sedang kamu baca menggunakan kata "Anda"?

Tentang ini, biarkan saya mengapresiasi tinggi kepada pengalih bahasa, Elisabeth Soeprapto-Hastrich. Terjemahannya sungguh menyenangkan. Ramah. Tak kaku. Pokoknya, enak betul untuk dibaca. Bahkan juga menyisipkan informasi tentang Indonesia, sehingga terasa natural. Misal, "... yaitu bahasa sastra Hindu kuno yang sangat mempengaruhi bahasa Jawa kuno, bahasa Indonesia modern yang kita pakai sekarang ..." (hal. 66).

Isi buku lumayan ramai. Pembahasan sejarah populer cukup lengkap. Sejarah yang tidak populer juga disampaikan dengan menarik. "Tidak populer" di sini bukan berarti tidak penting, cek saja pada penjelasan hieroglif dan cara membacanya (hal. 18) dan bagaimana bangsa Fenisia menciptakan abjad (hal. 36). Benar-benar pengetahuan yang bahkan saya tak ingat pernah dapatkan di sekolah.

Kisah sejarah dalam satu bab tidak selalu berdiri tunggal. Ada yang berlanjut atau menceritakan tokoh/tempat lain pada bab lainnya -- bab yang terkait. Contohnya pada kisah perseteruan Yunani versus Persia yang kemudian dilanjutkan dengan kisa dua kota di Yunani, Athena dan Sparta.

Pengetahuan-pengetahuan kecil dan terkesan trivia tapi justru membuktikan bahwa sejarah itu "berpola". Dan kadang terkesan ajaib! Seperti pada sejarah Roma yang merupakan salah satu penceritaan era yang cukup panjang di buku ini. Kaisar terakhir di Roma bernama Romulus Augustus. Jika nama tersebut dibagi menjadi 2 nama, maka Romulus sendiri adalah pendiri Roma sekaligus raja pertama. Sedangkan Augustus adalah kaisar pertama Romawi.

Bukan berarti bacaan selalu seru. Meskipun didukung oleh terjemahan yang ajib, paragraf yang begitu penuh dengan kisah yang bertele-tele justru membosankan. Terutama pada bab-bab terakhir seperti *Manusia dan Mesin* dan *Di Seberang Lautan*. Tergantung topiknya juga, sih, ini. Meskipun lumayan panjang, terfavorit jatuh pada topik Yunani versus Persia (bab 8) dan kisah Leonardo da Vinci di bab *Sebuah Zaman Baru* (bab 26).

Secara umum, *Sejarah Dunia untuk Pembaca Muda* cukup informatif. Layak menjadi bacaan penyuka sejarah, tapi tidak untuk pencari sejarah rinci. Seperti judulnya, ia mengajak kita untuk menikmati sejarah dengan bahasan yang mudah dicerna, bukan secara dakik-dakik menghafalkan abad dan tanggal-tahun kejadian tertentu.

Saya menutup ulasan buku kali ini dengan kutipan satu paragraf penuh dari bab penutup: *Secuil Sejarah Dunia yang Kualami Sendiri: Kilas Balik*. Kutipan yang sangat relevan, dahulu hingga sekarang.

> Aku mengenal seorang biksu Buddha bijaksana yang sudah sepuh. Dalam pidato kepada teman-teman sebangsanya ia pernah mengungkapkan rasa herannya mengapa kalau ada orang yang sesumbar tentang dirinya sendiri "aku ini orang paling pandai, paling kuat, paling berani, dan paling berbakat di dunia", semua orang pasti akan menertawainya dan menganggapnya konyol, sementara kalau orang yang sama mengganti kata "aku" dengan "kita" dan mengatakan bahwa "kita ini bangsa paling pandai, paling kuat, paling berani, dan paling berbakat di dunia", publik di tanah airnya bertepuk tangan meriah dan menyanjungnya sebagai patriot. Padahal sikap seperti itu sama sekali tak ada hubungannya dengan patriotisme. Orang tetap dapat mencintai tanah airnya tanpa perlu menganggap semua bagian dunia yang lain dihuni oleh cecunguk-cecunguk yang tak ada harganya. Sayangnya, semakin banyak orang yang tertarik untuk memercayai omong kosong macam itu, semakin besar pula bahaya yang mengancam perdamaian.

---

**Judul**: Sejarah Dunia untuk Pembaca Muda

**Penulis**: Ernst H. Gombrich

**Penerbit**: Marjin Kiri

**Tebal**: xxii+368 halaman

**Tahun terbit**: 2015

**Alih bahasa**: Elisabeth Soeprapto-Hastrich
