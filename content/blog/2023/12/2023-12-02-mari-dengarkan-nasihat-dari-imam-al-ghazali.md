+++
title = "Menikah atau Melajang? Mari Dengar Nasihat dari Imam Al-Ghazali"
date = "2023-12-02T22:38:11+07:00"
tags = ["buku"]
+++

Pernikahan adalah hal yang kompleks, "sakral". Bukan hal remeh atau sekadar fase kehidupan yang kudu dilalui. Banyak hal yang perlu dipertimbangkan. Banyak hal yang perlu dipelajari.

Menikah adalah ibadah. Kata banyak orang, sih, begitu. Dan, yaa, memang begitu. Tapi kehadiran konten-konten pernikahan yang beragam seperti membuat kesan bahwa ia hanya sekadar fase hidup. Yang sudah dan sedang melakukan dianggap fasenya terpenuhi. Yang belum, dianggap harus lekas mengejar.

Oke, itu bisa jadi benar. Tapi tahan dulu. Sekali seumur hidup, setidaknya membaca satu buku tentang pernikahan. Beruntungnya ada beberapa yang saya baca dan saya memutuskan untuk menuliskan reviu satu judul: *Nasihat Pernikahan Imam al-Ghazali*.

Buku ini sebetulnya berisi dua kitab karya Imam al-Ghazali, *Adab an-Nikah* dan *Kasr asy-Syahwatain*. Keduanya adalah bagian dari kitab *Ihya Ulumuddin.* Kitab *Ihya Ulumuddin* dibagi menjadi empat *rubu'*: *rubu' al-ibadat* (seperempat ibadah), *rubu' al-'adat* (seperempat adat), *rubu' al-muhlikat* (seperempat perkara yang menghancurkan), dan *rubu' al-munjiyat* (seperempat perkara yang menyelamatkan). Naah ... kitab *Adab an-Nikah* sendiri merupakan bagian dari *rubu' al-'adat* sedangkan kitab *Kasr asy-Syahwatain* merupakan bagian dari *rubu' al-muhlikat*.

![kover buku Nasihat Pernikahan](/blog/2023/12/images/nasihat-pernikahan.jpg)

Pembuka buku sekaligus menjadi salah satu bagian terfavorit yakni peta buku yang terdapat di halaman awal setelah pengantar penerbit. Isinya ringkasan tentang identitas penulis, alasan kenapa buku/kitab ditulis, keunggulan buku, serta tentang pernikahan.

Penuh. Isinya penuh. Meski hanya berisi "beberapa" lembar, setiap pertanyaan yang mungkin sekelebat kamu pikirkan ketika memilih untuk menikah kemungkinan besar terjawab di sini. Bukan tentang teknis kehidupan pernikahan, tentang mengelola amarah, mengelola finansial, dan sebagainya, tetapi lebih tentang kewajiban suami dan istri. Begitu juga dengan motivasi mengapa perlu menikah.

Bahasan yang cukup umum sebenarnya. Imam al-Ghazali menyebutkan beberapa ayat Al-Qur'an sebagai rujukan menikah, termasuk pada surah An-Nur ayat 32 sebagai perintah untuk menikah dan surah Al-Baqarah ayat 232 yang merupakan larangan menghalangi orang yang hendak menikah.

> "Dan nikahkanlah orang-orang yang sendirian di antara kamu." (QS an-Nur [24]: 32)
> 
> "Janganlah kau menghalangi mereka menikah lagi dengan bakal suaminya." (QS al-Baqarah [2]: 232)

Pun, beliau juga menyebutkan salah satu hadis populer.

> "Menikah adalah sunahku. Maka barang siapa yang suka pada fitrahku, hendaklah ia bersunah dengan sunahku." (HR Abu Ya'la)

*As expected*, seperti yang dibayangkan.

Namun ternyata ada bahasan lain yang di luar ekspektasi dan pantas dipahami. Tentang motivasi menjauhi pernikahan.

Apabila khawatir saat menikah atau berkeluarga malah jauh dari ibadah, hanya fokus pada duniawi, maka menjauhi pernikahan adalah pilihan yang tidak buruk. Bagi seorang muslim, kegiatan positif apa pun yang niatnya ibadah seharusnya tidak dihitung dalam konteks ini. Apakah jika suatu saat mempunyai istri kita justru malah bekerja keras hanya niat mencari uang tanpa menimbang kehalalan cara? Atau parahnya, bekerja hingga melupakan salat wajib lima waktu.

Ia mengajarkan kita akan kesadaran. Sadar dengan apa yang akan dan sedang dilakukan. Sadar dengan niatnya.

Tidak selalu tentang menyenangkan dan kemudahan, Imam al-Ghazali juga menyebutkan tentang kesulitan-kesulitan yang terjadi saat menikah. Dan, lagi-lagi, itu menuntut kesadaran kita. Soal kehalalan harta yang diperoleh, hak istri yang tidak terpenuhi, kelalaian yang timbul karena terlalu fokus pada mencari harta, dan lain sebagainya.

Mengenai kehalalan harta, bisa lah, ya, kita membayangkan maksudnya. Bagi diri sendiri saja diwajibkan mencari rezeki halal, apalagi saat menghidupi keluarga. Tanggung jawabnya ada di situ. Kelak anak istrimu akan mengadu kepada Allah swt., "Ya Tuhan kami, dahulu suamiku tidak memberikan nafkah halal". Lalu dihukumlah suami tersebut.

Bagi sebagian atau bahkan mungkin banyak orang, beberapa poin bisa jadi bikin hati tidak *sreg*, termasuk saya sendiri. Terutama bagi perempuan. Seperti dalam poin kelima dalam aspek sifat-sifat yang harus diperhatikan yang mengantarkan kepada tujuan-tujuan pernikahan: *subur/dapat melahirkan*. Jika laki-laki sudah tahu bahwa seorang perempuan mandul, maka laki-laki tersebut sebaiknya tidak menikahinya.

Baiklah, salah satu tujuan menikah adalah mencari keturunan. Tapi, ketika poin ini ditelan mentah tanpa pertimbangan, berapa banyak perempuan yang tak memiliki harapan untuk menikah karena didiagnosis mandul? Padahal mandul itu sendiri tak hanya bisa terjadi pada perempuan, tapi juga laki-laki.

Harus ada kehati-hatian dan keadilan di sini. Jangan sampai perempuan yang terdiagnosis tak dapat hamil terpojokkan. Sementara itu, laki-laki dengan aman melenggang ketika ternyata merekalah yang justru mengalami kemandulan.

Toh secara ideal, ketika menjadikan seorang perempuan sebagai istri, maka seharusnya haknya lah untuk dicintai dan disayangi sepenuh hati. (*Ceileh!*) Masih banyak sisi ibadah lain yang dapat diterapkan selain dari keturunan. Banyak sekali. Banyaaak sekali.

Untuk kamu yang ingin menikah, buku ini bukan buku bacaan wajib. Namun, tetap bisa menjadi tambahan wawasan penting. Sedangkan untuk kamu yang sedang tidak ingin (menunda) menikah, buku ini bisa menjadi buku bacaan wajib. Seperti bertolak belakang ya? Tidak sebetulnya, ia justru mengingatkan akan prinsip hidup: dengan niat yang benar, tepatkah mengejar pernikahan atau sementara tetap bertahan dengan kelajangan?

[Nilai: 3,9/5]

---

**Judul**: Nasihat Pernikahan Imam al-Ghazali

**Penulis**: Imam Abu Hamid Al-Ghazali

**Penerbit**: Turos Pustaka

**Tebal**: 324 halaman

**Tahun terbit**: 2020

**Alih bahasa**: Fuad Syaifudin Nur
