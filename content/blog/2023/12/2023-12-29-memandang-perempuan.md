+++
title = "Memandang Perempuan"
date = "2023-12-29T23:59:44+07:00"
tags = ["cuap"]
+++

Beberapa bulan lalu, saya membaca twit tumpahan-teh. Salah satu wartawan senior, yang pernah dipecat karena telah melakukan kekerasan seksual (KS) justru masih aman berpindah-pindah kantor. Bahkan, saat twit itu ditulis, masih berada di salah satu media besar dengan jabatan penting (kabar terakhir, ia disidang dan keluar dari kantor).

Raka Ibrahim, jurnalis muda yang tulisan-tulisannya sering saya ikuti, mengiyakan. Secara garis besar, "*begitulah, kenapa saya selama ini pilah-pilah tongkrongan, menjauh, keluar, dan melakukan seperti hal yang dilakukan sekarang.*" Bubuhnya, "*banyak banget pemerkosa, pelaku pelecehan, dan pembenci perempuan yang menyesaki ruang redaksi dan ranah intelektual kita.*"

Sikap saya, syok tidak syok. Syok karena sikap yang kerap saya temui di lingkungan kerja lapangan justru ada di lingkungan yang seharusnya aman darinya -- apalagi dengan konten dakik-dakik tentang kesetaraan yang sering terbit di sana.

Kamu tidak salah baca. *Kerap saya temui di lingkungan kerja lapangan* itu benar adanya. Kalau kamu belum *ngeh*, ada suatu stereotip yang diwajarkan bagi "orang lapangan": suka bicara sembarangan demi menghibur diri dari tekanan pekerjaan. Saya tak begitu masalah jika hal sembarangan itu candaan umum meski mungkin kadang agak gelap, tetapi yang sangat problematis adalah *saru*, candaan merendahkan perempuan, mengobjektifikasi perempuan.

Silakan ejek saya sebagai orang munafik, sok alim, dan tetek bengeknya.

Begini, *ngger*. Hidupmu seselo apa, sih, selalu melihat lalu mengomentari tubuh perempuan? Iya, iya, yang lewat depan rumah itu cantik. Terus kenapaaa? Jadi orang kok *gumunan* -- kamu sudah hidup belasan hingga puluhan tahun di dunia, lho. Kamu bukan Mihailo Tolotos (yang bahkan Tolotos tidak mengenal konsep kagum terhadap perempuan hingga ajalnya, ia hanya fokus melakukan selibat).

Masih jauh lebih baik perilaku kagum itu disimpan sendiri. *Lha*, kamu malah mengumbarnya ke dalam teriakan. Diselingi celotehan cabul, kotor, dan menjijikan. Menjadikannya kebiasaan lalu mengolok orang lain yang tak merespon dengan cara sama. "*Kamu homo, ya?*" begitu tuduhan favoritmu. Atau, dengan nada meremehkan, "*ssst ... dia sedang menjaga pandangan.*"

Kamu tidak hidup dalam dunia homogen. Begini, kata-kata saya terlalu berat? Oiya, maklum, kamu hanya suka melihat perempuan lalu berbicara *saru-saru*. Kaget tidak semua orang berpikiran sepertimu?

Rasa kagum adalah hal wajar. Ia manusiawi. Tapi, ketika itu diungkapkan dengan cara menganggu, tidak sopan, dan merendahkan, maka itu bukan lagi kewajaran. Mesum lebih tepatnya. Saya menyebutnya sebagai "memandang dengan jelalatan", gaya objektifikasi perempuan paling minimal.

Catat ini: objektifikasi.

Konsep yang seharusnya dimengerti oleh semua laki-laki. Di mana pun. Kapan pun. Menganggap perempuan hanya sebagai barang rebutan, barang tontonan, barang yang selalu harus dimiliki, mau pun secara "sederhana", dikomentari sesuka hati.

Tulisan ini sekaligus menjadi protes terhadap perilaku di sekitar saya. Entah di lingkup kampung mau pun pekerjaan. Sikap diam saya, sikap tidak merespon saya, adalah cara saya untuk menyatakan ketidaksetujuan terhadap perilaku tersebut. Capek memang, dan sering merasa menyesal ketika saya pun tidak berani mengonfrontasi celotehan mereka kecuali dengan kata-kata "*ngawur (kowe)*".

Seorang perempuan datang sebagai murid baru saat masih kelas 5 SD dulu. Devi namanya. Setelah beberapa minggu menjadi teman sekelas, murid yang cukup populer sebagai anak nakal mulai merundung murid baru itu dengan teriakan "LONTE!". Ya, orang yang sama yang selama itu juga merundung saya, mulai merundung siswa baru. Beruntungnya tidak terjadi masa kini dan terjadi di Indonesia, kalau terjadi di Amerika Serikat, pasti bakal beda cerita.

Oke, poinnya begini. Itu pengalaman pertama saya mendengar pelecehan langsung di depan muka. Ada traumanya sendiri. Bayangkan, seusia 10-11 tahun mendengar teriakan "LONTE!" dari teman sekelas dan ditujukan langsung kepada teman sekelas lain. Perempuan. Saya tahu itu kata olokan, tapi ketika itu saya tidak tahu persis maksudnya apa (yang baru saya tahu saat mulai usia remaja banget). Saya juga tahu, kala itu Devi sibuk menyembunyikan lukanya.

Ternyata semakin ke sini, semakin tahu. Banyak hal kotor yang dilakukan gender satu ini. Hingga muncul anggapan, laki-laki harus nakal, atau setidaknya pernah nakal. Jangan sampai nakalnya terlambat -- alim saat bujang, giliran sudah memiliki istri-anak baru melakukan hal-hal yang dikategorikan nakal (*madon, minum, main, merokok*).

Bagi mereka, nakal mungkin menjadi sebuah kunci kehidupan. Dan mengolok-olok perempuan adalah salah satu caranya.

Bukan.

Hal.

Baik.

Sangat tidak baik.

Memandang perempuan adalah satu hal. Memandang perempuan lalu mengomentari tubuhnya dan mengungkapkan kata cabul adalah hal lain.

Pada titik ini, saya sudah tidak peduli jika tulisan ini dibaca oleh teman dekat saya dan mungkin akan menjadi topik obrolan di lingkungan kantor.

Tidak peduli.
