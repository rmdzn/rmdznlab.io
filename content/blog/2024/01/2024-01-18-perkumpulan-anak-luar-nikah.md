+++
title = "Melihat Diskriminasi Terhadap Tionghoa dalam Kisah Anak Luar Nikah"
date = "2024-01-18T00:21:57+07:00"
tags = ["buku"]
+++

Tahun 2023 adalah tahun saya membaca banyak novel fiksi sejarah. Bagi saya, ia menjadi pencapaian tersendiri. Menjadi kepuasan tersendiri.

Melengkapi pencapaian tersebut, saya menemukan novel berjudul *Perkumpulan Anak Luar Nikah* karya Grace Tioso. Saya sebetulnya belum melirik buku ini hingga beberapa akun booktwt dan bookstagram memposting di lini masa dan si buku ternyata tersaji di rak paling depan toko.

> Jadi, tentang apakah buku tersebut? "Anak luar nikah" bukanlah merujuk pada cerita dewasa ala-ala, bahkan bukan stensil. Ia merupakan status yang diakibatkan oleh diskriminasi terhadap orang tionghoa.

![Buku Perkumpulan Anak Luar Nikah](/blog/2024/01/images/perkumpulan-anak-luar-nikah-dmg.jpeg)

Buku ini dibawakan Grace Tioso dengan gaya obrolan yang bagi saya terlalu santai. Saya sebetulnya tidak terbiasa dengan itu, seperti penggunaan "gue, elo" dan beberapa kata lain. Namun, ternyata saya dapat menikmati gaya tersebut, bahkan saat masih di awal-awal halaman. Saya dapat menikmati narasi yang menyenangkan serta istilah-istilah populer dalam bahasa Inggris yang juga ikut meramaikan.

Soal bahasa Inggris, nih, penulis mengasumsikan pembaca paham dengan bahasa tersebut. Konten buku terlihat menargetkan pembaca muda atau pembaca yang lebih melek bahasa asing. Meski itu wajar, penggunaan bahasa inggris tanpa menerjemahankannya justru membuat keterjangkauan buku ini tidak seluas jika bahasa Indonesia tetap diutamakan. Begitu juga pada beberapa penggunaan bahasa Jawa yang tidak diterjemahkan.

Sudut pandang kisah sungguh segar. Ia menceritakan kisah hidup orang di balik akun media sosial anonim yang sangat terkenal. Membahas politik, membongkar skandal, menolak dan mendukung kandidat calon legislatif. Kalau kamu ingat, beberapa tahun lalu muncul akun Triomacan2000 dan saya yakin ia lah yang menjadi inspirasi nama akun dalam buku ini.

Dibandingkan admin Triomacan2000, latar belakang akun duolion193 dalam *Perkumpulan Anak Luar Nikah* memberikan bayangan kepada saya sesuatu yang lebih bernilai. Di buku ini, Martha, sang tokoh utama, salah satu pengurus akun tersebut, adalah seorang Tionghoa. Namun, akibat peraturan diskriminatif masa lalu, urusan administrasi kehidupannya menjadi rumit. Kerumitan pun tidak hanya dirasakan Martha, tetapi juga dirasakan oleh banyak orang Indonesia dengan masa lalu serta keturunan yang sama. "Kerumitan" yang dimaksud ada dalam judul buku: status "anak luar nikah".

Buku ini memperkenalkan tionghoa dengan cukup kuat. Terutama pada budayanya. Bukan tentang Cina daratan, lho, ya, tetapi dari beragam negara, terkhusus tionghoa atau chindo (*chinese-indonesia*). Salah satunya terkait nama (hal. 311). Chindo memiliki ciri khas unik yang bikin kita bilang, "ini orang Indonesia, nih!". Ia gabungan dari dua nama. Nama pertama cenderung nama barat seperti Michael, Jessica, William, dsb. Nama kedua adalah nama keluarga tionghoa yang di-Indonesia-kan, seperti *Tan* menjadi Tanoto atau Tanuwijaya, *Lim* menjadi Halim atau Taslim, *Goe* menjadi Goenawan, dsb.

Mereka mengalami masa lalu yang buruk. Mereka dibungkam. Namun, para tionghoa justru kuat dan rekat saling mengenal. Mereka melawan Orba dengan identitas. *"Tidak ada propaganda pemerintah sekencang apa pun yang bisa menghapus sebuah identitas."*

Tak lupa dan ini sangat penting, *Perkumpulan Anak Luar Nikah* mengajarkan apa itu kebebasan bagi wanita sesungguhnya. Alih-alih harus berprofesi "tingkat tinggi" berdasarkan *passion*, pendidikan, dll, Martha justru ingin sekali menjadi ibu rumah tangga. Dan itu tidak masalah.

> Kalau perempuan yang memilih bekerja dibilang tetap bisa menjadi ibu sejati, kenapa perempuan yang memilih untuk tinggal di rumah dianggap pemalas dan enggak produktif? Kalau working mom enggak dipertanyakan rasa sayangnya terhadap anak-anak mereka, kenapa ibu rumah tangga perlu dipertanyakan kepintarannya? (hal. 323)

Selain itu, satu hal yang saya sukai lainnya adalah penjelasan seputar privasi dan kerahasiaan identitas berikut teknologi yang dipakai. Kehadiran akun-akun anonim serupa duolion163 di jagat media sosial untuk kegiatan sensitif dapat dipastikan menggunakan teknologi serupa.

Konsep penulisan buku ini segar. Kutipan akun media sosial ditulis layaknya fitur *embed*\--twit yang ditampilkan benar-benar seperti twit sebenarnya. Begitu juga notifikasi aplikasi atau pesan yang ditampilkan selayaknya notifikasi asli di ponsel. Yang lain, ada pula cuplikan berita yang ditampilkan seperti kliping. Tak ada hal yang membosankan saat membacanya.

Hal minus hanya terletak pada desain sampul. Terlalu ramai. Penuh sudut (yang sebetulnya dimaklumi karena menggambarkan jendela aplikasi dan laman web di layar monitor). Justru mirip seperti buku kuliah tentang perkomputeran, alih-alih novel fiksi sejarah.

Jika [*Laut Bercerita*](</blog/2023-06-11-laut-yang-bercerita/>) menjadi novel populer yang menyadarkan generasi terkini akan pelanggaran HAM terkhusus penculikan aktivis dan mahasiswa, saya berharap *Perkumpulan Anak Luar Nikah* menjadi novel dengan semangat sama yang menitikberatkan pada diskriminasi terhadap tionghoa.

[Nilai: 4,9/5]

---

**Judul**: Perkumpulan Anak Luar Nikah

**Penulis**: Grace Tioso

**Penerbit**: Penerbit Noura Books

**Tebal**: 390 halaman

**Tahun terbit**: 2023
