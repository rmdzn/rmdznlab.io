+++
title = "Menyaksikan Percintaan Dua Insan, Layla Majnun"
date = "2024-02-16T22:44:51+07:00"
tags = ["buku"]
+++

Setiap orang ada masanya. Setiap masa ada orangnya. Keduanya pun ada tempatnya sendiri. Ini tidak selalu tentang perpolitikan, kekuasaan, hal besar serupa negara, mau pun penemuan bersejarah, tapi juga tentang dunia romansa. Meskipun dalam nilai tertentu romansa itu merupakan hal besar luar biasa.

Pada kisaran tahun 1591 hingga 1595, William Shakespeare menuliskan kisah populer sepanjang masa: *Romeo dan Juliet*. Shakespeare menuliskannya berdasarkan cerita di Italia yang digubah dalam sajak The *Tragical History of Romeus and Juliet* oleh Arthur Brooke pada tahun 1562. Yang dilanjutkan oleh William Painter dalam bentuk prosa di *Palace of Pleasure* pada tahun 1582. Sulit dipastikan Romeo dan Juliet ditulis berdasarkan kisah nyata atau tidak, tapi ada teori yang menyebutkan ia terinspirasi dari kisah tragis yang lebih kuno yang ditulis pujangga Roma maupun yang lebih jadul lagi, ditulis oleh pujangga Yunani.[^1]

Terbukti pada masanya, di Eropa sana ada kisah cinta setragis itu--walau mungkin hanya fiksi, ia dapat menggambarkan hal yang mungkin memang bisa terjadi di sana. Lalu ke mana kita coba melangkah mencari tempat dan masa lain terkait romansa serupa?

Di Timur Tengah!

![Buku Layla Majnun](/blog/2024/02/images/layla-dan-majnun.jpg)

Atas rekomendasi seseorang, saya menuntaskan *Layla Majnun.* Sebuah novel, kisah cinta klasik dari negeri Timur. Berbeda dari *Romeo dan Juliet* yang lebih cenderung pada "sekadar kisah fiksi", *Layla Majnun* dipercaya sebagai kisah cinta yang benar-benar terjadi. Dia diutarakan dari mulut ke mulut, jauh sebelum William Shakespeare menulis *Romeo dan Juliet*.

Beragam versi dan varian cerita pun muncul. Namun, berterimakasihlah kepada Nizami asal Ganjavi (daerah Azerbaijan). Pada abad ke-12, dirinya menuangkan kisah percintaan Layla Majnun ke dalam tulisan hingga menyebar ke wilayah lainnya lalu diterjemahkan ke banyak bahasa.

Konsep cerita yang ditawarkan oleh *Layla Majnun* (LM) dan *Romeo dan Juliet* (RJ) seperti mirip, sekaligus bertolak belakang. Mengutip pengantar oleh Sholeh Gisymar, RJ berfokus pada materialisme barat (ganteng dan cantik), sedangkan LM memiliki fokus pada jiwa, perasaan, dan ketulusan. Sebaliknya, RJ bercerita tentang cinta yang penuh harapan, sedangkan LM penuh dengan rasa sakit dan derita (hal. xii).

Saya mengambil dua sudut pandang dalam kisah dua insan, Layla dan Qays (Majnun), ini.

Kesetiaan Qays sungguh di luar dugaan. Setia sekali. Penolakan keluarga Layla terhadapnya tidak menyurutkan isi hati Qays hingga di titik tertentu membuatnya dipanggil si gila alias Majnun. Bayangkan, kamu berjalan, penuh peluh, sambil rindu, menyiksa, menyakitkan tapi juga menyenangkan karena bayangan kasih sayang. Jika *Layla Majnun* menjadi bahan meme tentang percintaan, maka ia akan menjadi semacam, "mencintaimu secara ugal-ugalan layaknya Majnun kepada Layla."

Namun, ketika kisah mereka disandingkan gaya terkini, saya tidak yakin Majnun menjadi teladan bagi kaum Adam untuk mengungkapkan cinta-cintanya. Siapa yang mau dengan percaya diri membiarkan peperangan terjadi demi menikahi seorang perempuan? Siapa yang mau pula meninggalkan kehidupannya, menyendiri, hingga melupakan rasa berkomunikasi dengan sesamanya (manusia)? Saya tidak menafikan keinginan Majnun untuk mencari ketenangan dari dunia luar, saya pun bakal begitu kalau kanan kiri sudah terasa berisik dan melelahkan, minimal lima kali sehari dalam beberapa menit. Tetapi, kemudian wajib bangkit lagi, bukan?

Banyak titik kebahagiaan yang ada di dunia ini, tetapi Majnun tidak merasakannya, kecuali satu, saat bersama Layla. Seperti menggambarkan era saat ini, ketika kita ternyata sibuk mencari kebahagiaan sampai lupa untuk bahagia itu sendiri. Menikmati momen saat ini salah satunya.

Saya suka sekali dengan terjemahan *Layla Majnun*. Mulus. Tak kaku. Menyenangkan! Syair yang dialihbahasakan begitu cantik saat dibaca dan didengarkan. Pengalihbahasa terhitung sukses menjaga marwah syair dalam kisah klasik ini. Ia menjadi karya zaman dulu yang masih dapat dinikmati hingga sekarang. Tentu, sangat disarankan untuk penyuka kisah romantis dengan akhir yang dramatis.

---

**Judul**: Layla Majnun

**Penulis**: Nizami

**Penerbit**: Penerbit Gava Media

**Tebal**: xviii+226 halaman

**Tahun terbit**: 2019

**Alih bahasa**: Ust. Salim Bazmul

[^1]: https://id.quora.com/Apa-kisah-romeo-dan-juliet-benar-benar-nyata
