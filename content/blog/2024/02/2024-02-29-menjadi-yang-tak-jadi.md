+++
title = "Menjadi yang Tidak Jadi"
date = "2024-02-29T23:29:07+07:00"
tags = ["cuap"]
+++

Hidup itu rumit, bagi yang mampu melihat kerumitannya. Sebaliknya, hidup itu mudah, bagi yang mampu melihat kemudahannya. Namun, hidup tak sehitamputih itu, ada abu-abu di tengahnya yang secara pasti tentu tak tepat di tengah -- terkadang menjorok ke hitam dan kadang kala menjorok ke putih.

Pada suatu hari, saya terpilih menjadi orang nomor satu dalam organisasi tingkat kampung. Tidak bangga. Lebih banyak mangkelnya karena dugaan saya ternyata menjadi nyata. Iya, saya sudah was-was mayoritas suara saat itu akan memilih saya karena "kandidat" potensial lainnya tidak mungkin dipilih dengan alasan statusnya yang tidak lagi lajang.

Sebelum melanjutkan, izinkan saya menarasikan organisasi yang dimaksud. Organisasi tingkat kampung bukanlah lembaga pemerintahan tempat banyak orang saling sikut-sikutan, mengeluarkan biaya besar-besaran, demi mendapatkan satu kursinya. Lembaga yang sebetulnya adalah "perusahaan" dengan kursi berupa "lowongan pekerjaan". Setiap yang terpilih dan menduduki kursi akan memperoleh gaji besar, dengan tunjangan untuk wira-wiri dengan alasan bekerja. Sedangkan organisasi tingkat kampung merupakan lembaga murni nonprofit. Tidak ada kompensasi untuk pemegang jabatan di sana. Saat pemilihan, orang-orang justru *makakke liyane* (menyodorkan orang lain/temannya) alih-alih mengambil suara untuk dirinya sendiri.

Lagi-lagi, organisasi tingkat kampung pun berbeda dengan organisasi sosial pada umumnya. Anggota organisasi sosial ada karena kesamaan nilai. Ada sesuatu yang dipegang dan dituju. Masing-masing anggota tentu memiliki kegiatan yang mengarahkan mereka pada visi dan misi. Secara tidak langsung, rasa kepemilikan terhadap organisasi tersebut ada. Organisasi tingkat kampung tidak semua begitu. Ia terbentuk karena status "harus ada". Anggotanya pun terbentuk hanya karena keberadaannya di wilayah yang sama. Soal hati, siapa yang tahu. Soal target yang dituju, siapa yang tahu.

Saya membayangkan mampu menjalani takdir sebagai ketua organisasi. Saya mungkin mampu membentuk mental anggotanya untuk pelan-pelan menjadi anggota yang solid. Bukan hanya sekadar ada dan datang, tetapi memang tergerak dan bergerak. Fisik mau pun mentalnya. Idealis, ya? Memang. Tapi untuk menjaga kewarasan, saya tetap berusaha realistis. Ada kalanya situasi organisasi tidak seperti yang diinginkan. Baik perkembangannya, gaya komunikasinya, prinsipnya, maupun sikap anggota-anggotanya bakal berbeda tidak seperti bayangan awal-awal.

Semakin ke sini terlihat, alih-alih mengarahkan organisasi ke situasi ideal, cara berorganisasi anggota-anggotanya (termasuk senior mau pun junior) hanya ala kadarnya. Ketua diserahi sepenuhnya untuk *ngopyak-opyak* (menyuruh dengan sangat) semua anggota sebelum kegiatan, termasuk mereka yang senior-senior yang seharusnya *ngopyak-ngopyak*.

Situasi tersebut terjadi berulang kali.

Berulang.

Kali.

Pada mulanya, sisi realistis saya mencoba untuk menerima keadaan itu. Saya hanya perlu bertahan selama tiga tahun, tak perlu banyak perkembangan pun tak mengapa, begitu batin saya. Namun, lama-lama batin tertimpuk berulang kali hingga lebam membiru. Otak saya lelah. Mental saya lelah. Kemarin (sekitar hampir dua pekan sebelum tulisan ini diterbitkan), saya bahkan tidak bisa dan mau melakukan apa-apa, kecuali membaca. Entah, membaca saat itu merupakan kegiatan hiburan atau obat.

Ini mungkin hanya alasan. Tetapi, saya lelah luar biasa. Tidak adil ketika dalam organisasi tingkat kampung, ketua hanya dijadikan samsak tanggung jawab. Samsak yang terus dipukul sementara samsak lain tidak membantu dan merangkul. Apalagi, selama dua tahun terakhir, saya justru tidak dianggap siapa-siapa oleh sang ketua RW lama, kecuali jika beliau sedang butuh-butuhnya. Apabila tidak butuh, *boro-boro* diajak *ngobrol*, di banyak situasi diajak jabat tangan pun tidak.

Akhirnya, di titik ini saya melepaskan si beban. Keputusan yang cukup berat sebetulnya, saya ingin merampungkan tanggung jawab yang sudah diemban, tetapi demi diri sendiri sekaligus organisasi yang sehat, saya pikir mundur dari jabatan adalah keputusan tepat. Mengapa?

1. Hampir 3 tahun berjalan, gaya komunikasi tidak berubah secara signifikan. Saya tahu, ketua lah yang memiliki andil besar untuk memperbaiki komunikasi antar anggota dan antar seksi dalam organisasi. Namun, ketika perbaikan gaya komunikasi hanya selalu dilakukan oleh ketua dan beberapa anggota (itu pun tidak konsisten), hasilnya sama saja. Hal ini mungkin diakibatkan oleh poin dua.

2. Jarak sosial yang cukup jauh antar ketua dan anggotanya. Saya terlahir tahun 1993. Jarak usia terdekat dengan anggota adalah satu tahun (1994). Saya tidak begitu masalah saat berkomunikasi dengan para anggota, sejauh apa pun jarak usianya. Serius oke, bercanda pun oke. Tapi, ada satu yang tidak mungkin saya bisa pegang: satu sirkel pertemanan. Saya tidak tahu secara pasti tanggapan (gaya tongkrongan) mereka tentang organisasi (terutama organisasi kampung). Saya pernah menguliknya melalui forum resmi mau pun nonresmi, tapi hanya direspon secara normatif. Dari mana saya tahu bahwa saya bukan bagian dari sirkel? Ya, tahu, karena mereka memiliki grup pertemanan (berisi pemuda) nonresmi tanpa anggota karangtaruna lain, termasuk saya.

Saya tidak tahu, bagaimanakah kelanjutan organisasi ini nanti? Yang pasti, saya tidak akan lagi ambil pusing. Sejak lama, saya lebih suka bersosial dalam organisasi, jadi saya masih bakal bergerak, tapi tak lagi di sisi yang berat.

Dan, betul, ini adalah contoh hidup yang memang sedang rumit-rumitnya.
