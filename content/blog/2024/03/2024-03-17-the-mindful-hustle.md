+++
title = "The Mindful Hustle, Bekerja dan Bersosial Tanpa Stres"
date = "2024-03-17T13:27:56+07:00"
tags = ["buku"]
+++

Belum banyak buku improvisasi diri yang saya baca selama ini. Selain karena terlalu pilih-pilih--memilih topik yang benar-benar dibutuhkan--membaca buku dengan genre tersebut sama saja menabung janji kepada diri sendiri, bahwa saya harus menerapkan isi buku yang dibaca. Selalu ada beban tersendiri ketika buku improvisasi tidak memberikan efek apa pun kepada saya.

Jadi, ketika saya menemukan lagi buku bergenre itu untuk dibaca, ia spesial. Dan, ya, saya menemukan buku cakep lagi. Berjudul *The Mindful Hustle* oleh [Elisabet Guwanto](https://twitter.com/elisabetguwanto). Lewat media sosial, Elisabet Guwanto memang sering membagikan tips seputar produktivitas dan kehidupan pada umumnya.

![kover the mindful hustle](/blog/2024/03/images/the-mindful-hustle.jpg)

Subjudulnya menarik. *Produktif tanpa stres*. Yang begitu berhubungan, sesuai situasi yang sedang dialami kemudian dapat menuntut ke kiat-kiat dan solusi sesuai harapan.
  
Saya merekam hal penting dalam *The Mindful Hustle*. Ia merupakan buku pragmatis, bukan buku teori yang perlu banyak diawang-awang. Namun, bukan berarti tidak ada teori sama sekali. Setelah menjelaskan tentang sesuatu, Lisa, begitu nama panggilannya di media sosial akan membuatkan daftar penjelasan khusus disusul tabel yang dapat diisi pembacanya.

Terlihat sejak awal, pada bab pembukanya. Lisa menjelaskan tentang produktivitas yang tidak sehat (*unhealthy productivity*). Ada tiga, produktivitas beracun (*toxic productivity*), produktivitas palsu (*fake productivity*), dan rasa mempermalukan produktivitas (*productivity shame*). Yang pertama, adalah keinginan mendesak untuk terus-terusan produktif tanpa memperhatikan kesehatan fisik dan mental. Yang kedua, merasa harus terus sibuk, manganggap bahwa sibuk sama dengan produktif. Yang ketiga, perasaan bahwa kita tidak cukup produktif dan merasa bersalah saat menganggap aktivitas yang sedang dilakukan tidak produktif (istirahat, tidur, dsb). Penjelasan tersebut disusul dengan penjelasan lain kenapa hal itu terjadi. Yang kemudian disusul dengan daftar periksa atau tabel yang dapat diisi pembaca untuk mengidentifikasi masalah diri sendiri.

Ia tidak selalu berbentuk isian atau tabel, beberapa berbentuk pertanyaan yang bisa dijawab di kepalamu sendiri atau ditulis dalam kertas (ini lebih baik). Seperti pada halaman *Activity Page: How to Compare Yourself for Improvement* (hal. 64), penulis mengajak pembaca untuk membandingkan diri sendiri dengan masa lalu, membayangkan masa depan, lalu menyambungkannya dengan masa kini, apakah sesuai seperti yang diinginkan/diharapkan.

*The Mindful Hustle* juga mengajak mengelola emosi. Untuk menjaga rasa produktif sekaligus menghindari rasa tertekan, manajemen emosi memang sangat penting (hal. 140). Tarik napas dalam-dalam, tahan dan hitung selama 8 detik, kemudian hembuskan pelan-pelan. Jangka panjangnya dengan menerapkan *ART framework*. ART yang merupakan singkatan dari *Awareness*, *Regulation*, dan *Transformation*. Pembaca lagi-lagi secara praktis diminta untuk sadar terhadap diri sendiri.

*Begitu muda!* Budaya bergegas atau *hustle culture* yang melelahkan itu muncul di era kekinian, sehingga saya memandang buku ini menarget generasi Z. Dengan desain halaman lucu pada setiap jedanya serta kutipan-kutipan bijak yang pendek dengan bahasa Inggris. Pun jika saya melihat halaman Daftar Isi dan menutup kata "Daftar Isi" itu sendiri, buku ini bakal dianggap sebagai buku impor berbahasa Inggris--karena semua judul bab ditulis dalam bahasa Inggris.

Perasaan saya campur-campur soal ini, saya mendukung banyak orang muda yang aktif dengan pekerjaan pribadi dan sosial untuk membaca *The Mindful Hustle* dengan kontennya yang memang sangat muda--dari segi bahasa mau pun bahasan. Tapi, bahasa yang dipakai juga menjadikan buku ini terlalu *segmented*, terkesan hanya pemuda yang hidup di perkotaan dan memiliki pendidikan cukup yang dapat menikmati buku ini.

Saat membaca *The Mindful Hustle* pastikan kamu menyimak daftar isinya. Adanya ilustrasi (jeda bergambar) dan kutipan justru membuat saya bingung mana pembuka bab, mana subbab atau poin penjelasan. Sebetulnya itu tidak begitu penting, sih, tetapi demi menentukan lokasi menjeda bacaan kala istirahat atau melakukan sesuatu lainnya hal tersebut jelas perlu dilakukan.

Tentu saja, bagi kamu yang sibuk dengan pekerjaan, mau pun menyukai kegiatan sosial atau sukarela (*volunteering*) dan segalanya terasa membuat stres dan melelahkan, buku ini sangat direkomendasikan!

---

**Judul**: The Mindful Hustle

**Penulis**: Elisabet Guwanto

**Penerbit**: Bhumi Anoma

**Tebal**: 200 halaman

**Tahun terbit**: 2023
