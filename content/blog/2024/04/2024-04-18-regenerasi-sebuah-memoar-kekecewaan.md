+++
title = "Regenerasi, Sebuah Memoar Kekecewaan"
date = "2024-04-18T23:39:05+07:00"
tags = ["cuap"]
draft = false
+++

Pernah pada suatu masa, saya mengikuti organisasi kerohanian Islam di sekolah. Organisasi serius pertama yang saya selami. "Serius" dalam arti struktur organisasi paripurna, kepemimpinan juga jelas (meski sempat tumbuh drama-drama), dan kaderisasi yang berjalan tepat. Berbanding terbalik saat saya berkiprah di kampung, terutama di kepemudaan. Karena status organisasi baru mulai terbentuk lagi, ia lahir dengan gaya bebas--kalau ada kegiatan maka berkumpul, kalau tidak ada kegiatan *yaa* jangan diada-adakan.

Berdasarkan pengalaman tersebut, saya memperhatikan bahwa regenerasi memang hal yang kompleks, lengkap dengan kerumitan-kerumitannya sendiri. Sebelum agak jauh membahas tentang itu, istilah penting yang perlu diingat: kaderisasi merupakan proses untuk regenerasi, dengan kata lain regenerasi adalah hasil dari kaderisasi.

Regenerasi memang hal sangat penting. Kalau tidak ada regenerasi, siapa lagi yang akan menghidupkan komunitas mau pun organisasi? Ia akan terancam hilang, hanya akan menjadi sejarah dalam tulisan prasasti. Karena penting, regenerasi bukanlah hal yang mudah. Ia memerlukan proses pengaderan yang teliti dan sabar.

Teliti dan sabar. Kuncinya.

Mengikuti organisasi yang sudah berdiri kuat merupakan anugerah. Bertemu dengan orang yang sepemikiran. Bertemu dengan orang yang semangatnya sama. Bertemu dengan orang yang memang bergerak secara kuat sambil mencari penerusnya. Sesuatu yang sangat patut disyukuri sehingga tidak bisa dan tidak boleh dianggap biasa saja.

Karena ketahuilah, saat kamu berkecimpung di organisasi atau komunitas yang sedang tumbuh dan berkembang, situasinya lebih sangat campur aduk. Tergantung semangatmu sekaligus semangat rekan-rekanmu.

Di sebuah forum karangtaruna sepadukuhan, saya pernah mendengarkan pernyataan bijak: jangan sampai seorang ketua organisasi mutung. Ketika ketua mutung, pengelolaan organisasi akan berhenti. Pahitnya, ia mungkin rusak. Memang begitulah idealnya. Saya setuju. Namun, secara realistis saat apa saja masalah dilimpahkan kepada ketua, maka bukan hal tidak mungkin skenario tersebut terjadi.

Begini, dalam organisasi baru yang sedang tumbuh, tekanan untuk membaikkan organisasi tersebut sangat besar. Ada harapan banyak sesepuh atau orang tua kepada ketua yang sedang menjabatnya. Agar kampung maju karena (salah satunya) karangtaruna dan anaknya bisa berkecimpung ikut membesarkan situasi ideal yang dibayangkan.

Namun, kala itu dikembalikan ke realita, saya hanya dapat mengucapkan istigfar banyak-banyak. Oke, orang tua mendukung anak untuk berkecimpung di organisasi tetapi ketika sang anak diundang ke acara rapat rutin secara resmi, malah tidak datang. Saya tidak tahu di balik kata-kata dukungannya, apakah sudah nyata diterapkan dengan verbal atau tindakan kepada anak-anak mereka.

Itu satu masalah. Masalah lainnya adalah senior seperjuangan yang sama-sama tidak ingin berjuang. Mereka hanya mengandalkan satu dua sosok (terutama pada sosok ketua) agar organisasi sibuk dengan kegiatannya, agar pengaderan berjalan konsisten. Sungguh kenyataan yang membuat hancur hati dan semangat saya.

Sebagai pandangan, karangtaruna memiliki dua kategori kegiatan.

1. Kegiatan besar

2. Kegiatan kecil

Kegiatan besar adalah kegiatan yang memerlukan sumber daya banyak. Baik manusia, waktu persiapan, mau pun sumber daya lain. Dapat berbentuk sinoman pada acara pernikahan, lomba-lomba pada acara 17-an, acara takbiran, dan lain sebagainya. Sedangkan kegiatan kecil adalah kegiatan harian atau mingguan. Bisa berbentuk rapat, kegiatan kewirausahaan, mau pun kerja bakti.

Keduanya memerlukan kehadiran senior. Itu pun bukan sekadar hadir, tetapi ada dan berada. Datang tepat waktu, ada sampai acara selesai, dan ikut mengarahkan junior. Poin pertama dan kedua dapat ditoleransi ketika benar-benar ada urusan yang tidak dapat ditinggalkan, bukan karena lalai seperti ketiduran. Sikap tersebut harus ada di dua jenis kegiatan. Jangan tebang pilih.

Ini merupakan hal penting. Dari sudut pandang ketua, keberadaan senior dapat saya jadikan contoh. Saya bisa menanyakan kepada junior yang datangnya terlambat, "*loh, kok, terlambat? Udah ditunggu dari tadi, lho.*" Sebaliknya, jika para senior hadirnya terlambat tanpa keterangan jelas, saya pun secara tidak langsung hanya dapat memaklumi, "*wong seniornya aja terlambat, apalagi juniornya*".

Ini bukan tentang, "*loh, gantian, dong, dengan yang lebih muda (generasi baru)*", tetapi tentang bagaimana senior memberi contoh. Mereka dianggap memiliki pengalaman organisasi yang lebih banyak, maka tanggung jawabnya juga besar untuk membimbing yang muda-muda. Ini *common sense* alias akal sehat pada umumnya.

Intinya ini: saat terjun ke dalam kegiatan, fokus senior ada pada memberi contoh dan membimbing, bukan fokus ke dalam teknis. "Memberi contoh" mencakup hal apa pun, termasuk hadir saat kegiatan, izin jika berhalangan, menepati janji, menyisihkan waktu, dan sebagainya.

Ini kegiatan sosial. Maka, hanya dari partisipasi anggotanya lah ia dapat hidup dan tumbuh. Kesibukan seharusnya bukan menjadi alasan. Semua orang sibuk, saya tahu. Namun, dengan taraf kesibukan yang berbeda-beda, seharusnya ada pengelolaan diri agar tetap dapat ikut berpartisipasi dalam organisasi kepemudaan dengan lebih baik. Sebagai catatan, saya tidak menganggap nongkrong adalah bagian dari term *srawung*, sehingga tak pernah menyalahkan anggota pemuda yang tidak ikut nongkrong tak jelas. Saya justru sangat menghormati sikap *srawung* sebenarnya--anggota yang berusaha menyisipkan dirinya di antara kesibukan untuk mengikuti kegiatan karangtaruna (rutin mau pun nonrutin).

Ya. Saya cenderung fokus kepada peran senior karena saya tidak sepenuhnya setuju dengan klaim yang selalu menyalahkan gen-Z dan gen-alpha atas ketidakpekaan mereka terhadap kegiatan organisasi. Jangan *dumeh* sudah ada penerus, senior langsung menghilang lalu meremehkan kegiatan dengan sikap, "*ahh, santai, sudah ada bocah-bocah (penerus), kok.*"

Organisasi bukan hanya apa yang terlihat, tetapi juga apa yang ada di pikiran anggota-anggotanya. Agar ia tetap berjalan, jangan hanya melanjutkan kegiatan fisiknya tetapi juga harus melanjutkan prinsipnya, gaya berpikir anggotanya.

---

Kunci:

1. Untuk membaikkan organisasi, senior harus banyak berperan.

2. Tugas utama senior membimbing dan memberi contoh.

3. Jangan biarkan ketua berpikir dan berjuang sendirian.

4. Meskipun ada sosok penerus, senior jangan langsung lepas begitu saja. Ada tanggung jawab pengaderan di situ.
