+++
title = "Seperti Bjgn"
date = "2024-06-19T20:34:43+07:00"
tags = ["cuap"]
+++

Minggu, 12 Mei, saya mengikuti kegiatan senang-senang. Ia, sebuah komunitas bernama [Jalan-Jalan Jajan (JJJ)](https://www.instagram.com/_jalanjalanjajan/) yang fokus pada, secara harfiah, jalan-jalan lalu jajan, yang dibentuk oleh duo pasangan suami istri, Mas Azwar dan Mbak Manda. Pasangan yang sama, sang pendiri toko buku Paperplane itu. Ini adalah keikutsertaan saya kelima atau keenam, saya lupa. Karena waktu memang luang dan lokasi jalan-jalan berada di wilayah kerja saya, niat untuk mengikuti memang sungguh besar.
  
Niat yang besar tidak selalu mantap sejak awal. Saya memang cenderung ingin mengikuti Jalan-Jalan Jajan ke-52 itu tetapi maju mundur karena situasi kerjaan yang berpotensi repot-repotnya serta sempat ribut dengan pikiran sendiri, "*lah, ngapain jalan-jalan di jalan yang setiap hari dan setiap bulan jelas saya lalui*." Seperti kurang kerjaan. Piknik dan wisata itu di tempat yang asing sekalian, bukan yang di tempat yang sering ditemui. 
  
Pikiran yang dibantah oleh pikiran diri sendiri lainnya. Latar belakang kenapa pikiran bertolak belakang itu muncul: rute yang dilalui JJJ kali ini adalah rute yang minimal saya lalui sebulan sekali, jalan yang sama, kampung yang sama, hanya sebagian kecil yang belum pernah saya lewati sama sekali. Namun, saya kemudian tersadar, *oh*, ternyata saya selama ini berjalan dengan penuh tekanan pekerjaan. Saya tidak memperhatikan lucunya awan menggumpal di atas sana, hijaunya sawah kala musim panen belum tiba, dan belokan jalan, kambing dalam kandang, bunga matahari yang ternyata seru buat masuk ke dalam lensa kamera. 
  
Selama ini, saya tidak pernah benar-benar memperhatikan itu. Asal saja berjalan lalu melakukan rutinitas yang itu itu saja. 
  
Matahari mulai naik, mulai lebih panas daripada sebelumnya. Seperti yang sudah dijanjikan, hal spesial dari kegiatan pagi itu adalah melihat festival bajingan, lebih tepatnya festival gerobak sapi yang dikendarai oleh para bajingan. 
  
Festival diadakan di Pasar Wage Jangkang--pasar yang hanya buka saat pasaran wage--seperti Pasar Legi Kotagede yang hanya buka saat pasaran Legi (itu, lho, pasar yang terekam dalam AADC 2). Bedanya, festival bajingan di Jangkang hanya ada pada hari Minggu Wage, pada hari Minggu lain akan siaga di Prambanan, Godean, dan di beberapa pasar lain. 
  
Ada 10-an lebih gerobak sapi yang siaga, berkumpul di lapangan. Berdiri di sisi barat. Berbagi sisi dengan lapak jualan pakaian, alat kelistrikan, pipa-pipa ledeng bekas, dan aneka jajanan, batagor, es tung tung, dsb. Masih ada banyak gerobak sapi yang berdatangan saat kami menuju ke titik festival berlangsung. Satu ... dua ... dua sapi putih besar lewat bersama dua bajingannya ... tiga ... sapi yang sedikit kecil datang pelan-pelan. 
  
Rencana awal hanyalah melihat sapi beserta gerobak yang dicat warna-warni cantik. Namun, begitu sampai sana, terpikir, "bagaimana kalau mengendarai itu untuk pulang ke titik kumpul?" Semua peserta setuju. Tawar-menawar pun terjadi dan berhasil. Dengan modal 15.000-20.000 perorang, kami menyewa 5 gerobak untuk membawa kami ke titik akhir. Sayangnya, ada insiden kecil, saya "terusir" dari gerobak kelompok saya yang katanya hanya muat 5 orang padahal akadnya boleh 6 orang. Bagaimana perasaan ketika diusir bajingan? Bjgn sekali, bukan?

![Menjadi penumpang gerobak sapi](/blog/2024/06/images/naik-gerobak-sapi-jjj.jpg)
  
Jalanan di atas gerobak sapi kala itu cukup bisa dinikmati, kecuali saat melewati jalan raya utama yang terlalu datar, lempeng, dan penuh dengan motor-motor yang tersendat di belakang. Setelah ia belok ke utara, petualangan seru dimulai. 
  
*Bun, hidup berjalan seperti bajingan\~* 
  
Naik gerobak sapi tak semulus berkendara dengan mobil, motor, atau sepeda. Melewati sedikit lubang saja sudah seperti melewati jalan bergeronjal bermeter-meter. Saya yang cuma menaikinya sekitar 10 menit saja terasa sekali, apalagi para bajingan yang ternyata datang dari daerah Klaten--dua jam perjalanan hingga sampai daerah Ngemplak, Yogyakarta. Iya, kamu kaget? Saya pun. Saya kira, semua bajingan datang dari Yogyakarta itu sendiri, ternyata banyak juga yang datang dari luar kota.  
  
Nadin Amizah benar. 
  
Layaknya menumpang di sana, di perjalanan, saya bertemu dengan beragam situasi yang tak menyenangkan. Dari lingkup pertemanan, lingkup kerjaan, maupun komunitas. Bajingan dan penumpangnya dapat bertemu jalanan jelek, jembatan sempit, hingga tahi sapi yang tepat keluar di depan muka. Bahkan, bertemu bjgn sebenarnya. Tetangga, teman lama, atau kolega kantor. Bisa jadi, semoga tidak, kita yang menjadi bjgnnya. 
  
Perjalanan pagi dengan gerobak yang sangat menyenangkan itu, pertemuan dengan orang-orang baru yang sangat menyegarkan itu, membawa pikiran saya keluar sebentar dari hiruk pikuknya hidup. Mengizinkan saya melupakan sebentar begitu depresifnya genosida di Palestina, depresifnya situasi negara karena pemerintah yang seenaknya sendiri, serta begitu memusingkannya pekerjaan karena konsumen yang memaki, marah, dan kolega yang tidak tahu diri. Bjgn! 
  
*Gludak!* 
  
Gerobak sapi yang kami tunggangi menyerempet pinggir gapura jembatan. Kami hanya kaget. Kamu tidak perlu gelisah. Santai, ia tidak lecet dan tidak patah. "Tidak apa-apa," kata sang bajingan dari sisi depan, saat kami, penumpang, menengok kaget ke belakang. 
  
Kadang-kadang, hidup memang harus begitu. Berjalan mulus di suatu waktu, tetapi juga berjalan goyah di waktu yang lain. Satu hari bertemu petapa, hari lain bertemu bjgn. Bedanya, beruntunglah jika berjalan diiringi petapa, tapi belajar bersabarlah saat diiringi bjgn. Di tengahnya bisa saja bertemu hal-hal mengagetkan. Yang normal biasa, yang menyedihkan parah, sampai yang membahagiakan luar biasa. 
  
Kadang-kadang pula, kita harus sadar siapa yang bjgn. Kawan, rekan yang dipertemukan, atau pun saya atau kalian.

