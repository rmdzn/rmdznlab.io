+++
title = "Mengenal Lebih Dalam Kultur Kekerasan di Jogja"
date = "2024-08-08T19:24:03+07:00"
tags = ["buku"]
description = "Mengenal lebih dalam kultur kekerasan di Jogja dari buku Jogja Bab Getih dan Klitih"
+++

Jogja romantis. Mendiang Joko Pinurbo pernah berpuisi--yang terpampang di Teras Malioboro--*Jogja terbuat dari rindu, pulang, dan angkringan*. Kata Jokpin sudah mewakili semuanya. Mewakili jenama Jogja sebagai kota tenteram, tenang, dan medamaikan. Jenama yang sangat melekat, kuat.
  
Tapi, *sigh*, benarkah begitu? 
  
Ya dan tidak.
  
Jogja memang penuh dengan tempat yang menyenangkan dan pasti gampang dirindukan. Namun, bukan berarti ia tak memiliki sisi gelap yang begitu gelap. Mau tahu? Coba tanyakan kepada Gusti Aditya. Salah satu penulis yang esai-esainya pernah saya baca di Terminal Mojok dan Kumparan Plus, belum lama ini merilis buku berjudul *Jogja Bab Getih dan Klitih *dengan subjudul yang menambah kengerian, *Sebab Kami Belajar Kekerasan Sejak Dini*.

![sampul buku jogja bab getih dan klitih](/blog/2024/08/images/jogja-bab-getih-dan-klitih.jpeg)

Buku ini dibuka dengan cara "unik". Berupa pengantar oleh Paksi Raras yang dibuat bukan secara khusus untuk buku tetapi diambil dari tayangan monolog Rasan di kanal YouTube Mojok. Intinya, Paksi Raras menduga segala kekerasan yang dilakukan anak muda di Jogja disebabkan oleh jarak atau gap kualitas hidup. Di sebuah kota pelajar, warga aslinya justru kesulitan menggapai pendidikan tinggi yang sebegitu mahalnya. Akhirnya, anak-anak muda ini melampiaskan energinya dengan cara berbeda, yakni kekerasan.

Buku ini memberikan banyak wawasan. Info-info sederhana ala trivia hingga kompleks yang sangat bermanfaat buat saya. Dapat dilihat di awalnya yang menjelaskan garis waktu yang sangat informatif. Gaya penyampaiannya yang sederhana mirip dengan ala-ala "FYI" pada forum-forum daring.

Salah satu informasi yang begitu *wow* bagi saya adalah geng-geng di Jogja muncul dari poros, lalu terpecah-pecah. Sebagai orang Jogja tulen yang tidak tinggal di semesta geng-gengan, informasi itu meledakkan kepala saya.

Geng justru bermula pada sesuatu yang biasa--kumpulan pesepeda yang menjelajah kampung. Kemudian tumbuh menjadi kelompok yang lebih besar dan makin besar. Dengan terikutcampuran petinggi pemerintahan pada rezim Soeharto, geng mulai berafiliasi politik. Karena afiliasi ini lah ada jarak waktu yang memisahkan. Geng berikutnya lahir dengan motif berbeda sekaligus membingungkan.

Pembentukan geng tidak terlepas dari pembentukan organisasi masyarakat (ormas) zaman dahulu. Seperti Kotikam. Mulanya, RM Imam Kantoko diminta oleh pamannya, Mayjen Widodo (Pangdam IV Dipenogoro Jawa Tengah) mencari massa untuk Golkar. Namun Kintoko yang tidak berminat berurusan dengan politik menolak tanpa mengatakan apa pun. Kemudian ia dipanggil lagi di Hotel Ambarukmo. Di situ selain berhadapan dengan Widodo, Kintoko ternyata juga harus berhadapan dengan Sri Sultan Hamengkubuwono IX. Dari situ terbentuklah Angkatan Muda Diponegoro (AMD). Yang setahun kemudian, 1976, berubah menjadi Kotikam dengan tugasnya berlainan jauh daripada AMD (hal. 15).

Perkembangan motif dibentuknya geng ditulis dengan cermat, begitu juga dengan usaha pelemahan kuasa entitas yang membentuknya. Lagi-lagi, Gusti Aditya mengambilnya dari sumber yang dapat dipercaya dan dapat dicari bila memang ingin menguliknya lebih banyak.

Jauh setelah kehadiran geng-geng yang sangat politis, besar, dan menggemparkan, muncul lah era ketika geng sekolah lahir dengan tujuan tak begitu jelas. Yang memang sepertinya dapat disederhanakan dengan motif kebanggaan (*pride*). Begini, jika geng-geng "profesional" itu menandai wilayahnya berakibat dengan diperolehnya lahan parkir atau area dengan nilai ekonomis, maka geng-geng sekolah setelah menandai wilayahnya dapat apa? Saya tersenyum. Iya, juga, yaa ... satu-satunya keuntungan yang didapat hanya "kebanggaan".

Kebanggaan yang tak seberapa itu memiliki daya gesek sangat tinggi karena sekolah-sekolah di Kota Yogyakarta yang berdekatan sehingga wilayahnya cenderung tumpang tindih. Perebutan wilayah kekuasaan katanya. Karena itu, pada masanya *drop-dropan* sering terjadi, yang kemudian "diatasi" oleh Pemkot dengan kebijakan penggantian tullisan *badge* sekolah. Solusi yang tak solutif.

Tidak cukup tentang sejarah, buku ini juga membawa istilah-istilah yang kerap muncul yang, tentu saja, membenahi pengetahuan saya sebelumnya dan memberikan pengetahuan baru. Lumayan, laah, yaa. Perbendaharaan kata seputar geng-gengan Jogja bertambah dan terkoreksi. Misalnya saja penggunaan kata "klitih" dan "ngedrop" yang benar.

Seperti judulnya, ia tak fokus hanya pada geng, tetapi kekerasan secara keseluruhan. Salah satu yang menjadi favorit saya adalah penceritaan peristiwa pasca meninggalnya Gunjack. Termasuk tragedi Hugo's Cafe dan lapas Cebongan. Ada juga yang tak kalah menarik lainnya adalah kultur kekerasan yang terjadi di UGM. Terfavorit lain, yang tidak mungkin dilewatkan, kritik terhadap kondusivitas palsu dan jargon Istimewa yang digaungkan oleh pemerintah provinsi dan kepolisian.

Hanya ada satu minus, yakni penggunaan kata "aku". Entah mengapa, saya lebih dapat menikmati tulisan nonfiksi dengan penggunaan "saya". Tapi itu bukan masalah besar. Apresiasi tinggi untuk penulis. *Jogja Bab Getih dan Klitih* bukan hanya sekadar tulisan gosip, ia membawa banyak catatan kaki yang dapat menjadi rujukan kemudian. Ya, catatan kaki (*footnote*). Cara terfavorit saya dibandingkan dengan catatan akhir (*endnote*) seperti dalam buku karya Mark Manson. Saya hanya cukup fokus pada lembar yang dibaca, tanpa perlu membolak-balik buku ke halaman paling belakang. Referensinya penuh dengan jurnal atau penelitian. Kalau pun penulis membahas sesuatu hal penting tetapi tidak yakin dengan sumbernya, ia tetap menjelaskannya.

Buku *Jogja Bab Getih dan Klitih* sangat direkomendasikan untuk orang-orang yang ingin mengetahui kultur kekerasan di Jogja, terkhusus kamu yang mungkin selama ini tersihir dengan jargon tenteram, nyaman, dan istimewa soal Jogja.

---

**Judul**: Jogja Bab Getih dan Klitih

**Penulis**: Gusti Aditya

**Penerbit**: EA Books

**Tebal**: xxx + 184 halaman

**Tahun terbit**: 2024
